/*
FILENAME.cpp
James Watson, u1015536, YYYY MONTHNAME
A one-line description of the file

Dependencies:
Template Version: 2017-04-13
*/

/* MS Visual Studio Checklist (Ignore if not MSVS)
1. [ ] Copy/Create header and source folders under the directory that contains the main CPP
2. [ ] Copy/Create header and source files in their respective folders
3. [ ] (Right Click) --> Add the header files and the source files to their respective folders in the project
4. [ ] Project --> Properties --> C++ --> General : Add the above header folder to the list of include directories 
5. [ ] Project --> Properties --> C++ --> Preprocessor --> Definitions : Add "_CRT_SECURE_NO_WARNINGS" ( Voxelyze only ) 
6. [ ] Project --> Properties --> C++ : Reduce warning level to 2 ( Voxelyze only ) */

/* C++ Checklist 
1. [ ] Appropriate #include */

// == Init =======================================================================================================================

// ~~~ Includes ~~~

// ~~ Standard ~~ // **** Common includes not already in Cpp_Helpers.h
//#include <iomanip> // - Output precision and printing
//#include <exception> // error handling
//#include <stdexcept> // library provided error types like 'std::range_error'
//#include <tuple> // --- standard tuple datatype
//#include <cstddef> // - alias types names:  size_t, ptrdiff_t
//#include <cstring> // - C Strings: strcomp 
//#include <algorithm> // sort, search, min, max, sequence operations

// ~~ Special ~~

// ~~ Local ~~ 
#include "Cpp_Helpers.h" // Shortcuts and Aliases , and Functions of general use for C++ programming

// == End Init ===================================================================================================================

// === Program Vars ===


// === End Vars ===

// === Program Functions & Classes ===


// === End Functions & Classes ===

// == main ==

int main(){
	
	srand( time( 0 ) ); // Random seed based on the current clock time
	cout << "I EXIST!" << endl << endl; // Silly output test
	
	
	// DO STUFF HERE
	
	return 0; // I guess everything turned out alright at the end!
}

// == End Main ==


/* == Spare Parts ================================================================================================================

std::ostream& operator<<(std::ostream& os, const Gene& vec) {
	std::vector<float> codons = vec.copy_codons();
	os << "[ ";
	for (size_t i = 0; i < codons.size(); i++) {
		os << codons[i];
		if (i + 1 < codons.size()) { os << ", "; }
	}
	os << " ]";
	return os;
}

   == End Spare == */