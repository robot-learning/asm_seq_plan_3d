~~~ 2017-07-30 , NEW TIMES ARE UPON US ~~~
[Y] Check if the deprecated collision solver is still available - UNSUCCESSFUL , The name of the solver is still in the repo as a struct , 
    However neither importing the header that contains it nor importing the "deprecated.h" header restores this name to the namespace and
    compilation 
[Y] Make a first run at installing ASM3D - UNSUCCESSFUL , see above
    |Y| Troubleshoot the FCL finder file - COMPLETE , It was necessary to uninstall all default versions of FCL that came in with ROS
[Y] Contact library about missing papers
    |Y| Contact Mary Ann James about SagePub - COMPLETE , Sent 2017-07-30
    |Y| Contact April Love about ASME / VPN  - COMPLETE , Sent 2017-07-30
[ ] Read available papers --> Take notes
[ ] Check ICRA for ASP , VSM , Control , Trajectory ID , & Curiosity papers
{ } See what Wan and Harada are up to
[ ] Expand notes and revise plan
[ ] Read OSU thesis
    | | Mine for relevant citations & background
[ ] Mine read papers for models to use for assembly planning
[ ] Begin a list of schools to apply to
..........
[L] Re-write collision for the new version of FCL - DEFERRED , No need to become varied in details until you decide where your thesis is going!


~~~ 2017-07-27 , METHODICAL TROUBLESHOOTING ALWAYS WINS ~~~
[ ] Re-write collision for the new version of FCL
[ ] Make a first run at installing ASM3D
    | | Troubleshoot the FCL finder file 
[ ] Contact library about missing papers
[ ] Read available papers
[ ] Expand notes and revise plan
[ ] Read OSU thesis

!! TROUBLESHOOTING PLAN !!

ISSUE : COMPILER COMPLAINS THAT THE REFERENCE TO THE BOX-BOX SOLVER IS NOT VALID

[Y] Resolve Eigen complaints about deprecated finders . FCL depends on Eigen! - COMPLETE , Now linking against Eigen3 , but does not resolve
    the original issue about the FCL box-box solver
[Y] Print out all DIRS before and after they are set - COMPLETE , The file sets all of the vars that it claims to
[Y] Set up conditionals for local and not local
    |~| Test nonlocal - PARTIAL , Having some trouble getting all of the vars to point to /usr/include directories , Running 
        sudo make install
        from the "build" folder seems be installing everything in /usr/local/include so maybe this is the way to go. See "breadcrumbs.txt"
    |Y| Test local 
    


~~~ 2017-07-27 , READING IS FUNDAMENTAL ~~~
[ ] Make a first run at installing ASM3D
[Y] Retrieve Featherstone book : PARTIAL , Reserved the book I wanted , Checked out an older book , just in case I am able to use it
[ ] Contact library about missing papers
[Y] Lit List # 11 & 12 : COMPLETE
[ ] Read available papers
[ ] Expand notes and revise plan
[ ] Read OSU thesis

~~~ 2017-06-29 , BUILD INCREMENTALLY FOR GREAT JUSTICE ~~~
[Y] Collision detection at the world level
	- Pair-wise
	- How should the results be handled? - Populate a vector of pointer pairs and return
	- FCL does not have any obvious functions for handling a group of objects - so just do pairs for O(n^2) time , oh well
[L] Get face centers (corner?) - LATER
[L] Get face normals - LATER

~~~ 2017-06-24 , BUILD INCREMENTALLY FOR GREAT JUSTICE ~~~
[Y] Remove child markers

~~~ 2017-06-23 , BUILD INCREMENTALLY FOR GREAT JUSTICE ~~~
[Y] Have the node ask for a repaint at some refresh rate	
[Y] Display
[Y] Implement markers that are children of other markers 
[Y] Add child markers
[Y] Animate child frames

~~~ 2017-06-21 , BUILD INCREMENTALLY FOR GREAT JUSTICE ~~~
[Y] Add the marker to the constructor - COMPLETE
[N] Delete the marker in the Destructor? - This is not necessary , the marker was not created with 'new'
[Y] Have the world manager poll set the markers to their absolute poses
[Y] Have the world manager publish all of the part markers

~~~ 2017-06-17 ~~~
[%] Work on the version of ASP_Shape with tf2 , ADD INCREMENTALLY FOR GREAT JUSTICE

~~~ 2017-06-12 ~~~
[X] Get the updated repo to compile with context-dependent CMakeLists.txt
[X] Download and install the newest versions of libraries
[X] Compile with the latest versions - There apparently isn't any difference with FCL , something about my home environment made it different
[X] Note the versions and make a note to freeze them

~~~ 2017-03-30 ~~~
[X] rviz marker example
[X] Open new repo
[X] Move / Rotate marker
[N] Attach markers to a reference frame an move the frame with TF
