#!/usr/bin/env python

# ~~ Future First ~~
from __future__ import division # Future imports must be called before everything else, including triple-quote docs!

"""
joint_ctrl_interactive.py , Built on Spyder for Python 2.7
James Watson, 2016 July
Try to control the Gazebo model with a Tkinter interface
"""

# == Init Environment ==================================================================================================
import sys, os.path
SOURCEDIR = os.path.dirname(os.path.abspath(__file__)) # URL, dir containing source file: http://stackoverflow.com/a/7783326

def add_first_valid_dir_to_path(dirList):
    """ Add the first valid directory in 'dirList' to the system path """
    # In lieu of actually installing the library, just keep a list of all the places it could be in each environment
    loadedOne = False
    for drctry in dirList:
        if os.path.exists( drctry ):
            sys.path.append( drctry )
            print 'Loaded', str(drctry)
            loadedOne = True
            break
    if not loadedOne:
        print "None of the specified directories were loaded"
# List all the places where the research environment could be
add_first_valid_dir_to_path( [ '/home/jwatson/regrasp_planning/researchenv',
                               '/media/jwatson/FILEPILE/Python/ResearchEnv' ] )

# ~~ Imports ~~
# ~ Standard Libraries ~
from math import sin, cos, pi
import time
# ~ ROS Libraries ~
import rospy
from std_msgs.msg import Char
from sensor_msgs.msg import JointState
# ~ Other Special Libraries ~
import numpy as np
import matplotlib.pyplot as plotter
import scipy.optimize as opt
# ~ Local Libraries ~
from ResearchEnv import * # Load the custom environment
from ResearchUtils.Vector import *
from hand_ik_analysis import *
# == End Init ==========================================================================================================

# == State Helpers ==
           
def slice_joint_state(msg,bgn,end): # [4:11] to extract the arm from "/lbr4_allegro/joint_states" topic
    """ Return a slice of a message that contains only the indices that we want """
    rtnMsg = JointState()
    rtnMsg.header = msg.header
    rtnMsg.name = msg.name[bgn:end]
    rtnMsg.position = msg.position[bgn:end]
    rtnMsg.velocity = msg.velocity[bgn:end]
    rtnMsg.effort = msg.effort[bgn:end]
    return rtnMsg

def concat_joint_state(state1, state2):
    """ Return a JointState message that is the concatenation of 'state1' and 'state2' """
    rtnMsg = JointState()
    rtnMsg.header = state1.header # Assume that the header information still applies to the concatenated states
    #                  state1                                    state2
    rtnMsg.name.extend(state1.name) ;         rtnMsg.name.extend(state2.name) # URL, extend list: http://www.tutorialspoint.com/python/list_extend.htm
    rtnMsg.position.extend(state1.position) ; rtnMsg.position.extend(state2.position)
    rtnMsg.velocity.extend(state1.velocity) ; rtnMsg.velocity.extend(state2.velocity)
    rtnMsg.effort.extend(state1.effort) ;     rtnMsg.effort.extend(state2.effort)
    return rtnMsg
    
def joint_states_hand_only(input_joint_state):
    """ Take in joint states in the format of '/lbr4_allegro/joint_states' and return a message that is only composed of the Allegro states """
    return concat_joint_state( slice_joint_state(input_joint_state,0,4) , slice_joint_state(input_joint_state,11,23) )
    
def joint_states_arm_only(input_joint_state):
    """ Take in joint states in the format of '/lbr4_allegro/joint_states' and return a message that is only composed of the LBR4 states """
    return slice_joint_state(input_joint_state,4,11)
    
# == End State == 

_DISPLAY_RATE = 50
class JointController:
    """ A simple class to move the Gazebo models """

    def __init__(self, listen_prefix='/lbr4_allegro', publish_prefix='/lbr4_allegro'):
        """ Attach the publishers and the listener """
        # If the controllers don't work, try changing the prefixes?
        
        # , node_name='joint_controller_interactice', 
        rospy.init_node('joint_controller_interactice') # Start this node
        
        self.jointState = None

        # ~ Publishers ~
        self.joint_cmd_pub = rospy.Publisher('/lbr4_allegro/lbr4/joint_cmd', JointState, queue_size=5)
        self.ctrl_type_pub = rospy.Publisher('/lbr4_allegro/lbr4/control_type', Char, queue_size=5)      
        
        self.allegro_joint_cmd_pub = rospy.Publisher(publish_prefix+'/allegro_hand_right/joint_cmd', JointState, queue_size=100)        
        self.allegro_torque_cmd_pub = rospy.Publisher('/allegro_hand_right/torque_cmd',JointState, queue_size=100)
        self.allegro_ctrl_type_pub = rospy.Publisher(publish_prefix+'/allegro_hand_right/control_type', Char, queue_size=5)
                      
        # ~ Listeners ~                       
        rospy.Subscriber('/lbr4_allegro/joint_states', JointState, self.joint_state_cb)
        
        self.gotFirstState = False


    def joint_state_cb(self, input_joint_state):
        """ Store the current joint state of the robot """
        if input_joint_state.header.seq % _DISPLAY_RATE == 0:
            self.jointState = input_joint_state
            self.gotFirstState = True
            #print "stored a state!"               
        
    def run_allegro_to_state(self, fingerStateList):
        run_rate = rospy.Rate(100)
        control_type='p'
        self.allegro_ctrl_type_pub.publish(Char(data=ord(control_type)))
        run_rate.sleep()
        
        jc = JointState()
        jc.name = ['index_joint_0','index_joint_1','index_joint_2', 'index_joint_3',
                   'middle_joint_0','middle_joint_1','middle_joint_2', 'middle_joint_3',
                   'ring_joint_0','ring_joint_1','ring_joint_2', 'ring_joint_3',
                   'thumb_joint_0','thumb_joint_1','thumb_joint_2', 'thumb_joint_3']

        while not self.gotFirstState:
            run_rate.sleep()
        # Fetch the current state
        print self.jointState
        jc.position = joint_states_hand_only(self.jointState).position
        
        delta_position = 0.015
        
        posReached = False        
        
        # Note that this loop does not take into account the actual state of the hand as it moves
        while not posReached:
            self.allegro_joint_cmd_pub.publish(jc) # Send the current joint command
            posReached = True # flag the state is satisfactory until proven otherwise
            #jc.position = self.joint_states_hand_only(self.jointState).position # This seems to make it hang
            for i in xrange(len(jc.name)):
                stateDiff = fingerStateList[i] - jc.position[i]
                if abs( stateDiff ) > delta_position:
                    posReached = False
                    if stateDiff > 0:
                        jc.position[i] += delta_position
                    else:
                        jc.position[i] -= delta_position
            run_rate.sleep()
        
def send_hand_to_pos(thetaList):

    # Move the hand in Gazebo
    jc = JointController()
    jc.run_allegro_to_state( thetaList )

    # Apparently cannot start more than one ROS node from the main Python thread

#    # Init the KDL model for the hand
#    urdf_file_name = roslib.packages.get_pkg_dir('urlg_robots_gazebo') + '/robots/allegro.urdf'
#    model = HandSimpleModel(urdf_file_name)
#    finPoses = model.FK( [thetaList[:4],thetaList[4:8],thetaList[8:12],thetaList[12:]] ) # base is the "palm_link", relative to palm
#    
#    # Report the poses for the fingers
#    for pose in finPoses:
#        print pose, endl
        
