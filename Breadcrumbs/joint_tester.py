import rospy
from std_msgs.msg import Char
from sensor_msgs.msg import JointState

_DISPLAY_RATE = 50
class JointTester:

    def __init__(self, node_name='joint_tester', listen_prefix='/lbr4', listen=False, publish_prefix='/lbr4'):
        rospy.init_node(node_name)

        self.joint_cmd_pub = rospy.Publisher(publish_prefix+'/lbr4/joint_cmd', JointState,
                                             queue_size=5)

        self.ctrl_type_pub = rospy.Publisher(publish_prefix+'/lbr4/control_type', Char,
                                             queue_size=5)
        if listen:
            rospy.Subscriber(listen_prefix+'/joint_states', JointState,
                             self.joint_state_cb)

        self.allegro_joint_cmd_pub = rospy.Publisher(publish_prefix+'/allegro_hand_right/joint_cmd',
                                                     JointState, queue_size=100)
        self.allegro_torque_cmd_pub = rospy.Publisher('/allegro_hand_right/torque_cmd',
                                                      JointState, queue_size=100)

        self.allegro_ctrl_type_pub = rospy.Publisher(publish_prefix+'/allegro_hand_right/control_type', Char,
                                             queue_size=5)


    def send_joint_command(self):
        pass

    def joint_state_cb(self, cur_joint_state):
        if cur_joint_state.header.seq % _DISPLAY_RATE == 0:
            print cur_joint_state.name

    def run_lbr4_test(self, control_type='p'):
        run_rate = rospy.Rate(50)
        self.ctrl_type_pub.publish(Char(data=ord(control_type)))
        run_rate.sleep()

        jc = JointState()
        jc.name = ['lbr4_j0','lbr4_j1','lbr4_j2','lbr4_j3','lbr4_j4','lbr4_j5','lbr4_j6']
        # TODO: set this to work for velocity and effort
        jc.position = [1.0, 0.0, 0.0, 2.0, -2.0, 2.0, 0.0]

        for i in xrange(100):
            self.joint_cmd_pub.publish(jc)
            jc.position[0] -= 0.1
            jc.position[4] -= 0.3
            run_rate.sleep()

        jc.position = [1.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0]
        for i in xrange(100):
            self.joint_cmd_pub.publish(jc)
            jc.position[0] += 0.1
            jc.position[4] += 0.3
            run_rate.sleep()

    def run_allegro_test(self, control_type='p'):
        run_rate = rospy.Rate(100)
        self.allegro_ctrl_type_pub.publish(Char(data=ord(control_type)))
        run_rate.sleep()

        jc = JointState()
        jc.name = ['index_joint_0','index_joint_1','index_joint_2', 'index_joint_3',
                   'middle_joint_0','middle_joint_1','middle_joint_2', 'middle_joint_3',
                   'ring_joint_0','ring_joint_1','ring_joint_2', 'ring_joint_3',
                   'thumb_joint_0','thumb_joint_1','thumb_joint_2', 'thumb_joint_3']

        if(control_type=='p'):
            # TODO: set this to work for velocity
            jc.position = [ 0.0, 0.0, 0.0, 0.0,
                            0.0, 0.0, 0.0, 0.0,
                            0.0, 0.0, 0.0, 0.0,
                            0.0, 0.0, 0.0, 0.0]
            
            delta_position = 0.015
            
            for i in xrange(100):
                self.allegro_joint_cmd_pub.publish(jc)
                for i in xrange(len(jc.name)):
                    jc.position[i] += delta_position
                run_rate.sleep()

            for i in xrange(100):
                self.allegro_joint_cmd_pub.publish(jc)
                for i in xrange(len(jc.name)):
                    jc.position[i] -= delta_position
                run_rate.sleep()
        elif(control_type=='e'):
            jc.effort = [ 0.0, 0.0, 0.0, 0.0,
                            0.0, 0.0, 0.0, 0.0,
                            0.0, 0.0, 0.0, 0.0,
                            0.0, 0.0, 0.0, 0.0]
            
            delta_effort = 0.055
            
            for i in xrange(100):
                self.allegro_joint_cmd_pub.publish(jc)
                for i in xrange(len(jc.name)):
                    jc.effort[i] += delta_effort
                run_rate.sleep()

            jc.effort = [ 0.0, 0.0, 0.0, 0.0,
                          0.0, 0.0, 0.0, 0.0,
                          0.0, 0.0, 0.0, 0.0,
                          0.0, 0.0, 0.0, 0.0]

            for i in xrange(100):
                self.allegro_joint_cmd_pub.publish(jc)
                for i in xrange(len(jc.name)):
                    jc.effort[i] -= delta_effort
                run_rate.sleep()
                        
def test_lbr4_position_control(listen=False):
    jt = JointTester(listen=listen, listen_prefix='/lbr4', publish_prefix='')
    jt.run_lbr4_test('p')
    jt.run_lbr4_test('f')

def test_lbr4_allegro_position_control(listen=False):
    jt = JointTester(listen=listen, listen_prefix='/lbr4_allegro', publish_prefix='/lbr4_allegro')
    rospy.loginfo('Running lbr4 control test')
    jt.run_lbr4_test('p')
    rospy.loginfo('Running allegro control test')
    jt.run_allegro_test('p')

def test_allegro_position_control(listen=False, publish_prefix='', listen_prefix=''):
    jt = JointTester(listen=listen, listen_prefix='/allegro', publish_prefix=publish_prefix)
    jt.run_allegro_test('p')
    jt.run_allegro_test('f')
    
def test_allegro_effort_control(listen=False, publish_prefix='', listen_prefix=''):
    jt = JointTester(listen=listen, listen_prefix='/allegro', publish_prefix=publish_prefix)
    jt.run_allegro_test('e')
    
