#!/usr/bin/env python
# -*- coding: utf-8 -*-
# Template Version: 2016-07-23

# ~~ Future First ~~
from __future__ import division # Future imports must be called before everything else, including triple-quote docs!

"""
regrasp.py , Built on Spyder for Python 2.7
James Watson, 2016 July
Implement The Work, brainstorm node and edge costs and quality measures
"""

"""
  == TODO ==
* Check that the points of the grasp pair are not too far separated to grasp

"""

# == Init Environment ==================================================================================================
import sys, os.path
SOURCEDIR = os.path.dirname(os.path.abspath(__file__)) # URL, dir containing source file: http://stackoverflow.com/a/7783326

def first_valid_dir(dirList):
    """ Return the first valid directory in 'dirList', otherwise return False if no valid directories exist in the list """
    rtnDir = False
    for drctry in dirList:
        if os.path.exists( drctry ):
			rtnDir = drctry 
			break
    return rtnDir
        
def add_first_valid_dir_to_path(dirList):
    """ Add the first valid directory in 'dirList' to the system path """
    # In lieu of actually installing the library, just keep a list of all the places it could be in each environment
    validDir = first_valid_dir(dirList)
    if validDir:
        sys.path.append( validDir )
        print 'Loaded', str(validDir)
    else:
        raise ImportError("None of the specified directories were loaded") # Assume that not having this loaded is a bad thing
# List all the places where the research environment could be
add_first_valid_dir_to_path( [ '/home/jwatson/regrasp_planning/researchenv',
                               '/media/jwatson/FILEPILE/Python/ResearchEnv',
                               'F:\Python\ResearchEnv',
                               '/media/mawglin/FILEPILE/Python/ResearchEnv' ] )

# ~~ Imports ~~
# ~ Standard Libraries ~
from math import sin, cos, pi
import time
import random
#import warnings ; warnings.filterwarnings('always') # Mess with warnings some other time
# ~ ROS Libraries ~
import roslib
import rospy
import rospkg # for finding where ROS things are in the file system
rospack = rospkg.RosPack() # get an instance of RosPack with the default search paths
import tf # Transforms, for obtaining poses
from std_msgs.msg import Char # character message
from std_msgs.msg import String # String message
from std_msgs.msg import Bool # Boolean message
from sensor_msgs.msg import JointState # For getting the joint angles
from urdf_parser_py.urdf import Robot
from gazebo_msgs.srv import SpawnModel # For importing new models into the environment
from gazebo_msgs.srv import DeleteModel # For deleting models from the environment
from gazebo_msgs.srv import GetLinkState # get the state of robot links in Gazebo
from gazebo_msgs.srv import GetModelState # get the state of models loaded in Gazebo
from gazebo_msgs.srv import SetModelState # set the state of models loaded in Gazebo
from gazebo_msgs.msg import LinkState # For getting information about link states 
from gazebo_msgs.msg import ModelState # For getting information about model states
import geometry_msgs.msg # import Pose # This collides with Vector.Pose!
# ~ KDL Libraries ~
from pykdl_utils.kdl_parser import kdl_tree_from_urdf_model
from pykdl_utils.kdl_kinematics import KDLKinematics
from hrl_geom.pose_converter import PoseConv
# ~ Other Special Libraries ~
import numpy as np
import matplotlib.pyplot as plotter
import scipy.optimize as opt
import scipy.spatial as sci_spatial # convex hulls
# ~ Local Libraries ~
from ResearchEnv import * # Load the custom environment
from ResearchUtils.Vector import * # rotation and other geometry operations
from ResearchUtils.DebugLog import * # selective debug printing
from ResearchUtils.PointCloud import * # dealing with point collections
from ResearchUtils.Plotting import * # for 3D plotting
from regrasp_helpers import * # store of regrasp functions

# == End Init ==========================================================================================================


# === Script Globals ===
# == Node Globals ==
_DISPLAY_RATE = 100
_TEST_TORQUE_CONTROL = False
_TEST_IK_CONTROL =     True 

dummyPose = PoseConv.to_homo_mat( # KDL 0-pose at origin, for when a you need a stand-in KDL pose
    ( np.matrix( [ [0.0] , [0.0] , [0.0] ] ) , 
      np.matrix( [ [0.0 , 0.0 , 0.0] , [0.0 , 0.0 , 0.0] , [0.0 , 0.0 , 0.0] ] ) ) 
)
# == End Node ==

# == Arm Globals ==
_NUM_ARM_JOINTS = 7
_ARM_JNT_NAMES = ['lbr4_j0', 'lbr4_j1', 'lbr4_j2', 'lbr4_j3', 'lbr4_j4', 'lbr4_j5', 'lbr4_j6']
# == End Arm ==

# == Hand Globals ==
_EE_NAMES = ['index_tip', 'middle_tip', 'ring_tip', 'thumb_tip'] # Effector link names
_INDEX_IDX = 0
_MIDDLE_IDX = 1
_RING_IDX = 2
_THUMB_IDX = 3
_NUM_FINGERS = 4
_NUM_FINGER_JOINTS = 4
_HAND_JNT_NAMES = ['index_joint_0','index_joint_1','index_joint_2', 'index_joint_3',
                   'middle_joint_0','middle_joint_1','middle_joint_2', 'middle_joint_3',
                   'ring_joint_0','ring_joint_1','ring_joint_2', 'ring_joint_3',
                   'thumb_joint_0','thumb_joint_1','thumb_joint_2', 'thumb_joint_3']
# == End Hand ==
# === End Globals ===

# set_dbg_lvl(1) # Debug model init
# set_dbg_lvl(2) # Debug target init
set_dbg_lvl(3) # Debug IK and trajectories

dbgLog(1, "Interpreting file!")

# === Gazebo Model ===

class LBR_Allegro_Gazebo_Model:
    """ Representation of the KUKA LBR4 Arm + Wonik Allegro Hand """
    
    def __init__(self, pDisableGazebo=False):
        """ Constructor, init KDL solvers and control connections """        
        
        rospy.init_node('lbr4_allegro_model_node')
        dbgLog(1, "LBR_Allegro_Gazebo_Model:","Node started!")
        
        # == Model Vars & Utils ==
        self.DisableGazebo = pDisableGazebo
        self.heartBeatHz = 100
        self.jointState = None # Holds the joint state of the entire LBR4 + Allegro robot
        self.gotFirstState = False # Flag that tells whether the model can start reading state information
        self.idle = rospy.Rate( self.heartBeatHz ) # Best effort to maintain 'heartBeatHz' , URL: http://wiki.ros.org/rospy/Overview/Time
        self.trajTimeStep = 0.1 # seconds
        # == End Model Vars ==
        dbgLog(1, "LBR_Allegro_Gazebo_Model:","Utils loaded!")
        
        # == LBR4 Arm ==
        self.arm = ModelContainer()
        
        # = Arm Solver =
        urdf_file = file( roslib.packages.get_pkg_dir('urlg_robots_gazebo') + '/robots/lbr4.urdf' , 'r') 
        # urdf_file = file( roslib.packages.get_pkg_dir('urlg_robots_gazebo') + '/urdf/lbr/lbr4.urdf' , 'r') # open the URDF file for reading
        # urdf_file = file( roslib.packages.get_pkg_dir('urlg_robots_gazebo') + '/robots/lbr4.urdf.xacro' , 'r') # Errors parsing XACRO
        
        self.arm.robot = Robot.from_xml_string( urdf_file.read() ) # Attempt to parse URDF for KDL 
        self.arm.tree = kdl_tree_from_urdf_model( self.arm.robot ) # Attempt to build a tree 
        self.arm.base_link = self.arm.robot.get_root() # Fetch the base link
        self.arm.effector_link = 'lbr4_7_link' # last link in the chain, per the URDF below
        self.arm.chain = KDLKinematics( self.arm.robot , self.arm.base_link , self.arm.effector_link ) # Create a solver object based on the URDF
        dbgLog(3, "Arm Solver Link Names:", self.arm.chain.get_link_names() )
        # = End Solver =
        dbgLog(1, "LBR_Allegro_Gazebo_Model:","Arm solver loaded!")
        
        # = Arm Controller =
        self.arm.joint_cmd_pub = rospy.Publisher('/lbr4_allegro/lbr4/joint_cmd', JointState, queue_size=5)
        self.arm.ctrl_type_pub = rospy.Publisher('/lbr4_allegro/lbr4/control_type', Char, queue_size=5) # Is this used?
        self.arm.joint_limits = ( ( radians(-170) , radians( 170) ) , # Joint 1 , -/+ 170deg
                                  ( radians(-120) , radians( 120) ) , # Joint 2 , -/+ 120deg
                                  ( radians(-170) , radians( 170) ) , # Joint 3 , -/+ 170deg
                                  ( radians(-120) , radians( 120) ) , # Joint 4 , -/+ 120deg
                                  ( radians(-170) , radians( 170) ) , # Joint 5 , -/+ 170deg
                                  ( radians(-120) , radians( 120) ) , # Joint 6 , -/+ 120deg
                                  ( radians(-170) , radians( 170) ) ) # Joint 7 , -/+ 170deg
        # = End Controller =
        dbgLog(1, "LBR_Allegro_Gazebo_Model:","Arm controller loaded!")
        
        # == End Arm ==

    
        # == Allegro Hand ==
        self.hand = ModelContainer()
        
        # = Hand Solver = 
        urdf_file = file( roslib.packages.get_pkg_dir('urlg_robots_gazebo') + '/robots/allegro.urdf' , 'r') # open the URDF file for reading
        # urdf_file = file( roslib.packages.get_pkg_dir('urlg_robots_gazebo') + '/urdf/allegro_right/allegro_hand_description_right.urdf' , 'r') # open the URDF file for reading
        self.hand.robot = Robot.from_xml_string( urdf_file.read() ) # Attempt to parse URDF for KDL 
        self.hand.tree = kdl_tree_from_urdf_model( self.hand.robot ) # Attempt to build a tree 
        self.hand.base_link = 'palm_link' # positions given with respect to the palm frame
        self.hand.effector_link = _EE_NAMES # last link in the chain, per the URDF below
        self.hand.chain = [] #KDLKinematics( self.robot , self.base_link , self.effector_link ) # Create a solver object based on the URDF
        for i, ee_name in enumerate(_EE_NAMES): # for each of the finger effector link names
            fc = KDLKinematics(self.hand.robot, self.hand.base_link, ee_name) # Get a solver from the base of the hand to the tip of the current finger
            self.hand.chain.append(fc) # store the solver in the list
        # = End Solver =
        dbgLog(1, "LBR_Allegro_Gazebo_Model:","Hand Solver loaded!")
        
        
        # = Hand Controller =
        if not self.DisableGazebo:
            self.hand.joint_cmd_pub = rospy.Publisher('/lbr4_allegro/allegro_hand_right/joint_cmd', JointState, queue_size=100)
            self.hand.ctrl_type_pub = rospy.Publisher('/lbr4_allegro/allegro_hand_right/control_type', Char, queue_size=5) # Is this used?
        self.hand.joint_limits = ( ( # http://wiki.simlab.co.kr/AllegroHandWiki/index.php/Joint_Limits
                                        ( -0.5947 ,  0.5718 ), # Index ,  Joint 1
                                        ( -0.2969 ,  1.7367 ), # Index ,  Joint 2
                                        ( -0.2740 ,  1.8099 ), # Index ,  Joint 3
                                        ( -0.3275 ,  1.7185 )  # Index ,  Joint 4
                                   ), 
                                   (
                                        ( -0.5947 ,  0.5718 ), # Middle , Joint 1
                                        ( -0.2969 ,  1.7367 ), # Middle , Joint 2
                                        ( -0.2740 ,  1.8099 ), # Middle , Joint 3
                                        ( -0.3275 ,  1.7185 )  # Middle , Joint 4
                                   ),
                                   (
                                        ( -0.5947 ,  0.5718 ), # Ring ,   Joint 1
                                        ( -0.2969 ,  1.7367 ), # Ring ,   Joint 2
                                        ( -0.2740 ,  1.8099 ), # Ring ,   Joint 3
                                        ( -0.3275 ,  1.7185 )  # Ring ,   Joint 4
                                   ),
                                   (
                                        (  0.3636 ,  1.4968 ), # Thumb ,  Joint 1
                                        ( -0.2050 ,  1.2631 ), # Thumb ,  Joint 2
                                        ( -0.2897 ,  1.7440 ), # Thumb ,  Joint 3
                                        ( -0.2622 ,  1.8199 )  # Thumb ,  Joint 4
                                   ) )
        # = End Controller =
        dbgLog(1, "LBR_Allegro_Gazebo_Model:","Hand controller loaded!")

        # == End Hand ==


        # == Topic Publishers & Subscribers , Services (Model-Wide) == 
        if not self.DisableGazebo:
            rospy.Subscriber('/lbr4_allegro/joint_states', JointState, self.joint_state_cb) # Obtain JointState's from Gazebo
            self.tfListener = tf.TransformListener() # Listener for getting poses of the model
            self.grip_pub = rospy.Publisher('/regrasp/gripflag', Bool, queue_size=10) #std_msgs.msg.Bool, queue_size=10)
            # self.palmListener = tf.TransformListener() # Listener for getting the palm pose # I don't need another listener
            #                                              for another transform because the same command is used regardless of tf desired
            self.model_info_prox = rospy.ServiceProxy('/gazebo/get_model_state', GetModelState) # Get a handle to the Gazebo model spawner
            print "Waiting for /gazebo/get_model_state ...",
            rospy.wait_for_service('/gazebo/get_model_state') # Wait for the link state service to become available
            print "Model state ready!"
            self.spawn_model_prox = rospy.ServiceProxy('gazebo/spawn_sdf_model', SpawnModel) # Get a handle to the Gazebo model spawner
            print "Waiting for /gazebo/spawn_sdf_model ...",
            rospy.wait_for_service('gazebo/spawn_sdf_model') # Wait for the model loader to be ready
            print "Model spawn ready!"
            print "Waiting for /gazebo/set_model_state ...",
            self.modify_model_prox = rospy.ServiceProxy('gazebo/set_model_state', SetModelState) # Get a handle to the Gazebo model modifier
            rospy.wait_for_service('/gazebo/set_model_state') # Wait for the model state service to become available  
            print "Model modify ready!"
        # == End Topic ==
        dbgLog(1, "LBR_Allegro_Gazebo_Model:","Subscribers loaded!")
        
        
    # == State Retrieval ==        
        
    def joint_state_cb(self, input_joint_state):
        """ Joint state listener callback: Store the current joint state of the robot, let the components examine the joints they want """
        if input_joint_state.header.seq % _DISPLAY_RATE == 0:
            self.jointState = input_joint_state # store the state received without modification
            self.gotFirstState = True # Notify the model that states are available for reading now
            #print "stored a state!"    

    def arm_state_list(self):
        """ Return a list of joint angles for the arm """
        return list( joint_states_arm_only( self.jointState ).position ) # Fetch the current state and slice for arm angles

    def hand_state_list(self):
        """ Return a list of joint angles for the hand """
        return list( joint_states_hand_only( self.jointState ).position ) # Fetch the current state and slice for hand angles
     
    # == End Retrieval ==
    
    def sleep(self):
        """ Sleep for the remainder of a 100Hz period """
        self.idle.sleep()
        
    def sleep_for(self, s):
        """ Sleep for 's' (theoretical) seconds, although the actual pause is sometimes noticeably longer """
        for i in xrange( int( ceil(100 * s) ) ): 
            if i % 20 == 0:
                print "counting down" , i/100 , "of" , s , "...",
            self.sleep() # wait while the joint command exectutes
        print 
    
    def run(self):
        """ Keep things going """
        rospy.spin()

    # == Solver Wrappers ==

    # = Arm Solvers =

    def arm_FK(self, arm_q):
        """ Method to convert list of joint positions 'arm_q' to task coordinates """
        return PoseConv.to_pos_quat( self.arm.chain.FK(arm_q) ) # Convert before returning? # I must have had a reason for doing this?
        
    def arm_FK_full(self, arm_q):
        """ Given a list of joint positions 'arm_q', return a list of poses for each of the robot links """
        linkPoses = []
        dbgLog(3, self.arm.chain.get_link_names() )
        # world , base_link , lbr4_0_link , lbr4_1_link , lbr4_2_link , lbr4_3_link , lbr4_4_link , lbr4_5_link , lbr4_6_link , lbr4_7_link
        for linkDex , linkName in enumerate( self.arm.chain.get_link_names() ): 
            # linkPoses.append( PoseConv.to_pos_quat( self.arm.chain.FK( arm_q , link_number=linkDex ) ) )# It really seemed like 'FK' was achieving the same aim, but error
            linkPoses.append( PoseConv.to_pos_quat( self.arm.chain.forward( arm_q , end_link=linkName, base_link='world' ) ) ) 
        return linkPoses
        
    def arm_IK(self, x_d):
        """ Get inverse kinematics for arm given the KDL pose 'x_d' """
        # q = self.ac.inverse_search( x_d, timeout=15.0) # Let's forget about bias for now and search for a straight solution
        return self.arm.chain.inverse_search( x_d, timeout=0.25) # Somehow the function is not ticking real time #2.0) #15.0)
        
    def arm_IK_guess( self , x_d , guesState ):
        """ Search for IK starting from some 'guesState' """
        dbgLog(2, "MODEL.arm_IK_guess:", locals() )
        return self.arm.chain.inverse( x_d , q_guess=guesState )
        # TODO: If results are unfavorable, consider a timeout search if such makes sense
        
    def arm_nearby_search(self, pose, guesState):
        """ Repeats IK for different sets of random initial angles in a normal distribution near a guess until solution found or call times out. """
        timeout = 0.25        
        st_time = rospy.get_time()        
        while not rospy.is_shutdown() and rospy.get_time() - st_time < timeout:
            q_init = []
            for jointGuess in guesState:
                q_init.append( np.random.normal( loc=jointGuess , scale=0.175 ) )
            q_ik = self.arm.chain.inverse( x_d , q_guess=q_init ) 
            if is_vector( q_ik ):
                return q_ik
        return None

    # = End Arm =
    
    # = Hand Solvers =
    
    def hand_FK(self, finger_qs):
        """ Method to convert lists of finger joint positions 'finger_qs' to a per-finger list of task coordinates """
        # finger_qs = [ ['index_joint_0' , 'index_joint_1' , 'index_joint_2' , 'index_joint_3' ], 
        #               ['middle_joint_0', 'middle_joint_1', 'middle_joint_2', 'middle_joint_3'], 
        #               ['ring_joint_0'  , 'ring_joint_1'  , 'ring_joint_2'  , 'ring_joint_3'  ], 
        #               ['thumb_joint_0' , 'thumb_joint_1' , 'thumb_joint_2' , 'thumb_joint_3' ] ]
        X = []
        for i, fi_q in enumerate(finger_qs):
            fi_x = self.hand.chain[i].FK(fi_q)
            X.append(fi_x)
        return X    
    
    def hand_IK(self, fingers_desired, finger=None): # Position requests are in the palm frame, if specified in the world frame, change the root link
        """ Get inverse kinematics for desired finger poses of all fingers (palm frame)
        fingers_desired - a list of desired finger tip poses in order of finger_chains[], e.g. [pose1 , pose2, None, pose4]
        returns - a list of lists of pyhton arrays of finger joint configurations
        """
        # print "Got poses:", fingers_desired
        q_desired_fingers = [None for n in range(4)] # init soln's to 'None', only populate if a soln is requested for that finger
        for i, f_d in enumerate(fingers_desired): # for each of the desired frame specifications
            if f_d: # if the client code for this finger
                q_desired_fingers[i] = self.hand.chain[i].inverse_search( f_d, timeout=0.25)#2.0) # TODO: Increase wait time if not getting good results
        return q_desired_fingers    
    
    # = End Hand =

    # == End Solver ==

    # == Robot Control Functions ==
    
    # = Arm Control =
    def arm_JointCommand(self, thetaList):
        """ Send a JointCommand to the arm composed of the angles from 'thetaList' with only the position specified """
        jc = JointState()
        jc.name = _ARM_JNT_NAMES
        jc.position = thetaList[:]
        # for i in xrange(20):
        for i in xrange( int( ceil( self.trajTimeStep * self.heartBeatHz ) ) ): # Have to send this multiple times in order to be noticed, perhaps the controller's subscriber is not ready yet? Cmd topic not ready yet?  
            # Tried     20          
            self.arm.joint_cmd_pub.publish(jc) # Send the current joint command
            self.sleep() # wait # TODO: Observe this and make sure that the wait time does not do funny things to the trajectory execution
    # = End Arm =
            
    # = Hand Control =
    def hand_JointCommand(self, thetaList):
        """ Move the hand servos to the positions listed in 'fingerStateList' (all one list) """
        # NOTE: Angle specifications are different than for 'hand_FK', all finger angles are listed consecutively as seen below
        jc = JointState()
        jc.name = _HAND_JNT_NAMES
        jc.position = thetaList[:]
        for i in xrange(20): # Have to send this multiple times in order to be noticed, perhaps the controller's subscriber is not ready yet? Cmd topic not ready yet?            
            self.hand.joint_cmd_pub.publish(jc) # Send the current joint command
            self.sleep() # wait
    # = End Hand =

# === End Model ===
dbgLog(1, "Interpreted classes!")

MODEL = None

# == Simple, "Parallel" Two Finger Grasp ==

def print_gripper_tip_states():
    """ Print Poses and tfs for the end of the index finger and thumb """
    model_info_prox = rospy.ServiceProxy('/gazebo/get_link_state', GetLinkState) # Get a handle to the Gazebo model spawner
    rospy.wait_for_service('/gazebo/get_link_state') # Wait for the link state service to become available
    print
    # Get the final pose of the end of the index
    print "Index Link 3 Pose:"    , endl , model_info_prox( "lbr4_allegro::index_link_3" , "lbr4_7_link" )
    # print "Index Link 3 Pose:"    , endl , model_info_prox( "lbr4_allegro::index_link_3" , "lbr4_allegro::palm_link" ) # Cannot find this frame, though it is in tf
    try:
        indexPose = (trans,rot) = MODEL.tfListener.lookupTransform( 'palm_link' , 'index_link_3' , rospy.Time(0) )
        inTipPose = (trans,rot) = MODEL.tfListener.lookupTransform( 'palm_link' , 'index_tip'    , rospy.Time(0) )
        print "Index tf:" ,     indexPose
        print "Index Tip tf:" , inTipPose
    except (tf.LookupException, tf.ConnectivityException, tf.ExtrapolationException) as err:
        print "Could not obtain the transform:" , err
    # Get the final pose of the end of the thumb
    print "Thumb Link 3 Pose:"    , endl , model_info_prox( "lbr4_allegro::thumb_link_3" , "lbr4_7_link" )
    # print "Thumb Link 3 Pose:"    , endl , model_info_prox( "lbr4_allegro::thumb_link_3" , "lbr4_allegro::palm_link" ) # Cannot find this frame, though it is in tf
    try:
        thumbPose = (trans,rot) = MODEL.tfListener.lookupTransform( 'palm_link' , 'thumb_link_3' , rospy.Time(0) )
        tbTipPose = (trans,rot) = MODEL.tfListener.lookupTransform( 'palm_link' , 'thumb_tip'    , rospy.Time(0) )
        print "Thumb tf:"     , thumbPose
        print "Thumb Tip tf:" , tbTipPose
    except (tf.LookupException, tf.ConnectivityException, tf.ExtrapolationException) as err:
        print "Could not obtain the transform:" , err

def hand_open():
    """ Open thumb and forefinger """ 
    MODEL.hand_JointCommand( [   0,    0, pi/16, pi/2,    0,0,0,0,    0,0,0,0,    pi/2, 0, 0, pi/2] ) # Open
    MODEL.sleep_for(1)
    print_gripper_tip_states()
    
def hand_close():
    """ Close thumb and forefinger without respect to grasped object """ 
    MODEL.hand_JointCommand( [   0, pi/2, pi/16, pi/2,    0,0,0,0,    0,0,0,0,    pi/2, 0, 0, pi/2] ) # Open
    MODEL.sleep_for(1)
    print_gripper_tip_states()
    
# == End Parallel ==
        
dbgLog(1, "Interpreted functions!")        
 
def marker_handle( modelPath , modelName , mode='spawn' ): # requires model/regrasp_init
    """ Return a function that moves or creates a model of a certain name """
    modes = ('spawn','move')
    if mode not in modes: raise ValueError( "marker_handle: mode " + str(mode) + " is not recognized, expected one of " + str(modes) )
        
    def move_spawn_model( ROSPose ):
        """ Spawn a model at or move the model to a new location """
        if ROSPose.__class__ == Pose:
            ROSPose = ROS_geo_pose_from_Vector_Pose( ROSPose )
        if move_spawn_model.counter == 0: # If this is the first call, must instantiate the model in the world regardless of 'mode'
            MODEL.spawn_model_prox( move_spawn_model.mdlName , move_spawn_model.urdf , "robotos_name_space" , ROSPose , "world" ) 
        else: # not the first call, choose spawn or move original
            if mode == 'spawn':
                MODEL.spawn_model_prox( move_spawn_model.mdlName + "_" + str(move_spawn_model.counter) , move_spawn_model.urdf , "robotos_name_space" , ROSPose , "world" ) 
            elif mode == 'move':
                move_spawn_model.stateMsg.pose = ROSPose
                MODEL.modify_model_prox( move_spawn_model.stateMsg )
            # else no effect, not supposed to be here
        move_spawn_model.counter += 1 # Increment call count
    
    # set up functor vars that must be operated on, or that we want available outside the function     
    move_spawn_model.counter = 0 # functor counter to hold call count
    move_spawn_model.mdlName = modelName
    move_spawn_model.stateMsg = ModelState() # Set up message to target the bowl
    move_spawn_model.stateMsg.model_name = modelName
    # read and store URDF
    f = open(modelPath,'r') # Open the URDF for reading
    move_spawn_model.urdf = f.read() # and read it
    f.close() # close the file
    
    # Returned function will have the speficied behavior for the specified model
    return move_spawn_model
   
def model_init(pDisableGazebo=False):
    """ Start the rospy node, for some reason instantiating the model when the module load causes Python to hang """
    global MODEL
    MODEL = LBR_Allegro_Gazebo_Model(pDisableGazebo) # Start model, load URDF, init KDL
    dbgLog(1, "LBR_Allegro_Gazebo_Model:","Model init complete!")
    return MODEL

attachCounter = 0 
  
flag = False
  
def toggle_grip_flag(): 
    """ Flip the grip boolean to turn the grasp hack on and off """
    global flag
    flag = not flag # Toggle the flag
    newFlag = Bool() # compose a Bool msg to send to the grasp hack
    newFlag.data = flag
    MODEL.grip_pub.publish(newFlag)
    
def sleep_seconds(s):
    """ Idle the node for 's' seconds, probably more """ # I'm not sure why 'rospy.Rate' does such a poor job maintaining rate
    clock = rospy.Rate(100) # 'rospy.Rate' requires the node to be running!
    for i in xrange(100 * s): 
        if i % 20 == 0:
            print "counting down" , i/100 , "of" , s , "...",
        clock.sleep() # wait while the joint command exectutes

YCB_URDF_FLDR = os.path.join( rospack.get_path('ycb_urdf') , 'urdf' ) # Directory containing the URDF files
YCB_URDF_LIST = os.listdir( YCB_URDF_FLDR ) # Get all the URDF file names
YCB_FCET_FLDR = os.path.join( rospack.get_path('ycb_urdf') , 'facet_pkl' )
YCB_GRSP_FLDR = os.path.join( rospack.get_path('ycb_urdf') , 'grasp_pkl' )
YCB_GRPH_FLDR = os.path.join( rospack.get_path('ycb_urdf') , 'graph_pkl' )
    
modelList = [] # List of programmatically imported models

TARGETNAME = None # String representing object to be manipulated
MESHFCTP = None # Pickle structure with facet data, in the mesh frame
MESHGRSP = None # Pickle structure with grasp data, in the mesh frame
MESHGRPH = None # Graph structure, init fully connected, in the mesh frame
TARGETPATR = None # URDF physical attributes
TARGETPNTS = None # A points-only representation of the object, in the target frame

# GRASPOFFSET_P = [ 0.09 , 0.042 , -0.01 ] # palm frame # Thumb in the way
# GRASPOFFSET_P = [ 0.09 , 0.042 , 0.09 ] # palm frame # Thumb knocks over sugar box
GRASPOFFSET_P = [ 0.090 , 0.042 , 0.105 ] # palm frame

GRASPPAIRN = "block_X_mid.urdf" # Name of the grasp pair display model
GRASPPRPTH = os.path.join( rospack.get_path('grasp_anything') , 'urdf' , GRASPPAIRN ) # Path to the grasp pair model

AXES_SPWN = None # Spawn axes for each major wrist waypoint  # marker_handle( modelPath , modelName , mode='spawn' )
NORM_MOVE = None # Move a +z visual to places where you want to show a normal
PAIR_MOVE = None # Move a +/-y visual to places where an object will be grasped at two points
SPHR_SPWN = None # Spawn a sphere to show various points in the simulation
    
def regrasp_init(): # ( recomputeG = False , autoSaveG = True ): # Graph saving currently hits the recursion limit
    """ Get the world ready for running the algo """
    # 'recomputeG' : Graph compute flag, set to True to recompute regrasp graph and over-write previous one
    # 'autoSave'   : Graph pikcle flag, set to False to disable automatics pickling and saving of computed graphs
    # Default      - Use previous if exists, otherwise compute and save
    # True , True  - Recompute and overwrite
    # True , False - Recompute but only keep previous
    global TARGETNAME, MESHFCTP, MESHGRSP, MESHGRPH, TARGETPATR, TARGETPNTS , AXES_SPWN , NORM_MOVE , PAIR_MOVE, SPHR_SPWN
    
    if not MODEL: # If model has not been instantiated, then
        model_init() # do so
    
    # ~~ Designate the chosen object ~~    
    TARGETNAME = "DomiSuga1lb" # Chosen model for this demo
    
    # = Load Precomputed Data =
    MESHFCTP = load_pkl_struct( os.path.join( YCB_FCET_FLDR , TARGETNAME + ".pkl" ) ) # Load Precomputed Surface Data
    print "regrasp_init:",TARGETNAME,"surface loaded!"
    MESHGRSP = load_pkl_struct( os.path.join( YCB_GRSP_FLDR , TARGETNAME + "_stable-grasps.pkl" ) ) # Load Precomputed Grasp Data
    print "regrasp_init:",TARGETNAME,"grasps loaded!"
    # = End Load =    
    
    # = Load target STL model =
    fName = TARGETNAME + ".urdf"
    modelPath = os.path.join( YCB_URDF_FLDR , fName )
    TARGETPATR = get_urdf_phys_attr( modelPath ) # Fetch the physical attributes from the YRDF
    
    if TARGETNAME not in modelList: # If the model name not already registered
        modelList.append( TARGETNAME ) # Register the model that was spawned
    
    spawn_model_prox = rospy.ServiceProxy('gazebo/spawn_sdf_model', SpawnModel) # Get a handle to the Gazebo model spawner
    rospy.wait_for_service('gazebo/spawn_sdf_model') # Wait for the model loader to be ready

    f = open(modelPath,'r') # Open the URDF for reading
    sdff = f.read() # and read it
    
    spans = span_R3_pts( uniq_pts_from_mesh( MESHFCTP['mesh'] ) )
    dbgLog(2, "Target Extent  :" , spans )
    dbgLog(2, "Target Extent Z:" , spans[2] )
    
    # x 0.5 -y 0.5 -z 0.4 
    # tableCenter = [0.5 , 0.5] ; tableHeight = 0.3
    tableCenter = [0.35 , 0.35] ; tableHeight = 0.3
    dbgLog(2, "Setting Z pose for the target:" , spans[2]/2 + tableHeight + 0.02 )
    targetPose = ROS_geo_pose_from_Vector_Pose( Pose.pos_no_turn( [ tableCenter[0] , tableCenter[1] , spans[2]/2 + tableHeight + 0.02 ] ) ) 
    
    spawn_model_prox(TARGETNAME, sdff, "robotos_name_space", targetPose, "world") # gen model name and place the axes at the specfied pose
    print "regrasp_init:",TARGETNAME,"URDF + STL loaded!"
    
    TARGETPNTS = uniq_pts_from_mesh( MESHFCTP['mesh'] , TARGETPATR['origin'] ) # Get and store all the unique points from the model
    # = End STL =
    
    # = Generate / Load Graph =
    graphPath = os.path.join( YCB_GRPH_FLDR , TARGETNAME + "_connected-graph.pkl" )
    if True: # not os.path.exists( graphPath ) or recomputeG: # If the graph was not precomputed or we wish to recompute
        MESHGRPH = fully_connected_graph_from_struct( MESHFCTP , MESHGRSP ) # compute graph
        print "regrasp_init:",TARGETNAME,"graph computed!"
        if False: # autoSaveG: # If we wish to save or overwrite the pickled graph structure # DISABLED DUE TO RECURSION LIMIT
            print "regrasp_init: Saving",TARGETNAME,"graph ..."
            struct_to_pkl( MESHGRPH , graphPath ) # HITS RECURSION LIMIT
    else: # DISABLED DUE TO RECURSION LIMIT
        MESHGRPH = load_pkl_struct( graphPath )
        print "regrasp_init:",TARGETNAME,"graph loaded!"
    # = End Graph =   
        
    # = Init Markers =
    AXES_SPWN = marker_handle( rospack.get_path('grasp_anything') + '/urdf/block_axes.urdf'    , 'palm_axes'        , mode='spawn' )
    NORM_MOVE = marker_handle( rospack.get_path('grasp_anything') + '/urdf/block_Z.urdf'       , 'support_norm_neg' , mode='move'  )
    PAIR_MOVE = marker_handle( rospack.get_path('grasp_anything') + '/urdf/block_Y_mid.urdf'   , 'grasp_pair'       , mode='move'  )
    SPHR_SPWN = marker_handle( rospack.get_path('grasp_anything') + '/urdf/simple_sphere.urdf' , 'waypoint_sphere'  , mode='spawn' )
    # = End Markers =
    
def target_pose( name = None ): # Requires 'model_init'
    """ Return the Vector.Pose of the object being manipulated """
    name = name if name else TARGETNAME
    targetPose = Gazebo_pose_to_Vector_pose( MODEL.model_info_prox( name , "world" ) )
    print targetPose
    return targetPose

def id_target_support_surface():
    """ Identify the supporting face of the object, given an object pose """
    perceivedPose = target_pose() # 1. Fetch the pose of the target object
    # 2. Calculate the opposite of the table normal in the object frame, support table normal is just [0,0,1] in the world, so the inverse of the
    #    pose quaternion applied to [0,0,-1] in the target frame should give the normal of the perceived supporting face
    invRot = Quaternion.inverse( perceivedPose.orientation ) # Get the inverse of the pose rotation
    targetSupport = invRot.apply_to( [0.0 , 0.0 , -1.0] ) # Rotate the opposite of the z basis towards the supporing face
    # 3. Search for and choose the closest vector, dot product, push opposite of each dot product onto a priority queue and the least will be closest
    facetQueue = PriorityQueue()
    supportNorms = [ faceTuple[1] for faceTuple in MESHFCTP['clusterStability'] ]
    for nDex , normal in enumerate(supportNorms):
        facetQueue.push( nDex , -np.dot(normal , targetSupport) ) # Push an index of the normal onto the queue with its negated alignment with support vector
    selectDex = facetQueue.pop()
    # closestNorm = MESHFCTP['clusterNorms'][selectDex] # Fetch the normal we believe to be closest
    closestBases = MESHFCTP['clusterStability'][selectDex][4]
    displayQuat = Quaternion.serial_rots(
        Quaternion.principal_rot_Quat( closestBases['xVec'] , closestBases['yVec'] , closestBases['zVec'] ),
        Quaternion.k_rot_to_Quat( [1,0,0] , pi ) , 
        perceivedPose.orientation
    )
    displayPostn = np.subtract( perceivedPose.position , 
                                perceivedPose.orientation.apply_to( np.subtract( closestBases['origin'] , 
                                                                                 TARGETPATR['origin']   ) ) )
    displayPose = ROS_geo_pose_from_Vector_Pose( Pose( displayPostn , displayQuat ) )
    # 4. Display belief in Gazebo
    NORM_MOVE( displayPose ) # 5. Display belief in matplotlib # LATER
    
    return selectDex # Return the index of the subgraph of the pose

GOALTAG = None

def random_goal( superGraph , startTag=None ):
    """ Choose a random goal pose, avoiding 'startTag' if specified """
    gChoice = None 
    while True:
        gChoice = random.choice( superGraph )
        if gChoice.tag != startTag: # If we didn't choose the same as 'startTag' or none was passed
            break # This choice is fine, break and return
    return gChoice.tag
    
def plan( startSubGraph , goalTag ):
    """ Return a plan from the 'startSubGraph' to the goal subgraph with tag 'goalTag' """
    return uniformCostSearch( startSubGraph , goalTag ) # UCS in inappropriate 0 cost and does not return the shortest path

def target_pnt_world_frame( targetPose , targetPoint ): 
    """ Express the 'targetPoint' in the world frame """
    return np.add( targetPose.position , 
                   targetPose.orientation.apply_to( targetPoint ) )
                                                            
def mesh_to_trgt( meshCoords ): # Requires 'regrasp_init'
    """ Add the offset between from the YCB origin to the STL COM """
    return np.add( meshCoords , TARGETPATR['origin'] )
                                                            
def project_to_plane_and_hull( points , normal , xBasis, yBasis ):
    """ Project the target 'points' to a plane define by its 'normal' and return the convex hull on that plane """
    flatPoints = []
    for pnt in points:
        flatPoints.append( [ np.dot( vec_proj_to_plane(pnt, normal) , xBasis ) ,
                             np.dot( vec_proj_to_plane(pnt, normal) , yBasis ) ] )
    return sci_spatial.ConvexHull( flatPoints ) # Return the flat hull rotated to the new X-Y bases

def palm_object_relative():
    """ Report the relative pose between the wrist and the object """
    # URL, Transform Convention: http://wiki.ros.org/geometry/CoordinateFrameConventions#Transform_Direction
    # The direction of the transform returned will be from the target_frame to the source_frame. 
    #Which if applied to data, will transform data in the source_frame into the target_frame.
    palmPose = (trans,rot) = MODEL.tfListener.lookupTransform( 'world' , 'palm_link', rospy.Time(0) )
    #                                           target frame ---^         ^--- Source Frame
    objPose = MODEL.model_info_prox( TARGETNAME , "world" )
    palmVPose = tf_pose_to_Vector_pose( palmPose )
    objVPose = Gazebo_pose_to_Vector_pose( objPose )
    return Pose.diff_from_to( palmVPose , objVPose )

def object_palm_relative():
    """ Report the relative pose between the object and the wrist """
    # URL, Transform Convention: http://wiki.ros.org/geometry/CoordinateFrameConventions#Transform_Direction
    # The direction of the transform returned will be from the target_frame to the source_frame. 
    #Which if applied to data, will transform data in the source_frame into the target_frame.
    palmPose = (trans,rot) = MODEL.tfListener.lookupTransform( 'world' , 'palm_link', rospy.Time(0) )
    #                                           target frame ---^         ^--- Source Frame
    objPose = MODEL.model_info_prox( TARGETNAME , "world" )
    palmVPose = tf_pose_to_Vector_pose( palmPose )
    objVPose = Gazebo_pose_to_Vector_pose( objPose )
    return Pose.diff_from_to( objVPose , palmVPose )

def swept_cost( diffChain , perJntLen ):
    """ Calculate a cost assuming lower number joints take the arm through a greater arc, lower cost better """
    # NOTE: This function assumes that all angle differences are expressed in radians
    totalCost = 0
    for jntDex , angleDiff in enumerate(diffChain):
        totalCost += abs(angleDiff) / (2*pi) * pi * ((len(diffChain) - jntDex) * perJntLen) ** 2 # arc area = abs(angle)/(2pi) * (pi)r^2
    return totalCost
    
def tuned_joint_cost( diffChain , levels ):
    """ Return a total cost according to custom factors for each joint """
    totalCost = 0
    for jntDex , angleDiff in enumerate(diffChain):
        totalCost += abs(angleDiff) * levels[jntDex]
    return totalCost
    
def arm_link_high_score( armJointAngles ): # Requires model init
    """ Return a score that is the sum of all the z-components of link positions """
    fullFK = MODEL.arm_FK_full( armJointAngles )
    totalScore = 0
    for linkPose in fullFK:
        dbgLog(3, linkPose )
        totalScore += linkPose[0][2]
        #           position --^  ^-- z coord
    return totalScore

def arm_IK_move( targetKDLpose ):
    """ Attempt multiple IK solutions for a wrist pose, if solutions found choose the best and execut joint commands """
    # NOTE: This function expects poses in the KDL format, see "regrasp_helpers.py"
    # Attempt to obtain multiple IK solutions in hopes that one will not result in wild sweeps
    iterLimit = 8 # Maximum number of solutions to attempt
    IKsolns = PriorityQueue() # Queue to hold ranked IK solns
    beginState = MODEL.arm_state_list() # Get the present state to make calcs for gross movement
    for i in xrange(iterLimit): # Per attempt
        armSolnState = MODEL.arm_IK( targetKDLpose ) # Request a soln with timeout defined by 'arm_IK'
        if isinstance( armSolnState , ( list , np.ndarray ) ): # if the soln is non-empty
            # cost = sum_abs_diff_lists( beginState , armSolnState ) # Calc a movement score for this soln
            # cost = swept_cost( np.subtract( beginState , armSolnState ) , 0.20 ) # Calc a swept cost with link len 20cm
            # levels = [ 12 , 10 ,  8 ,  6 ,  4 ,  2 , 16 ] # Penalize large sweeps and wrist rotations
            levels = [  2 ,  4 ,  6 ,  8 , 10 , 12 , 16 ] # Penalize large flips and wrist rotations
            # cost = tuned_joint_cost( np.subtract( beginState , armSolnState ) , levels )
            cost = arm_link_high_score( armSolnState )
            IKsolns.push( armSolnState[:] , -cost ) # push the soln onto the queue with the calc'd score
            
    if not IKsolns.isEmpty(): # If there was at least 1 solution found
        solnState = IKsolns.pop() # Pop the IK soln with the lowest score
        MODEL.arm_JointCommand( solnState ) # Issue a command to the robot to move
        MODEL.sleep_for(1) # Give the simulated arm model a moment to settle    
        # Display results of the move
        finalState = MODEL.arm_state_list() #list( joint_states_arm_only( MODEL.jointState ).position )
        diffState = np.subtract( solnState , finalState )
        lists_as_columns_with_titles( [ 'final','desired','difference' ], [ finalState , solnState , diffState] )
    else: # no solns found, notify user
        print "IK did not return a solution for pose", targetKDLpose

def arm_IK_best( pPose ):
    """ Attempt multiple IK solutions for a wrist pose, if solutions found choose the best and execute joint commands """
    # NOTE: This function expects poses in the KDL format, see "regrasp_helpers.py"
    dbgLog(3, "arm_IK_best: Got" , pPose ) # list
    dbgLog(3, "arm_IK_best: Got" , pPose.__class__ ) # list
    dbgLog(3, "arm_IK_best: Got" , pPose.__class__.__name__ ) # list, why is it a list?
    if pPose.__class__ == Pose: # Convert to KDL pose for IK solns
        targetKDLpose = KDL_pose_from_Vector_Pose( pPose )
    else:
        targetKDLpose = pPose
    # Attempt to obtain multiple IK solutions in hopes that one will not result in wild sweeps
    iterLimit = 8 # Maximum number of solutions to attempt
    IKsolns = PriorityQueue() # Queue to hold ranked IK solns
    solnState = None
    for i in xrange(iterLimit): # Per attempt
        armSolnState = MODEL.arm_IK( targetKDLpose ) # Request a soln with timeout defined by 'arm_IK'
        if isinstance( armSolnState , ( list , np.ndarray ) ): # if the soln is non-empty
            cost = arm_link_high_score( armSolnState )
            IKsolns.push( armSolnState[:] , -cost ) # push the soln onto the queue with the calc'd score
    if not IKsolns.isEmpty(): # If there was at least 1 solution found
        solnState = IKsolns.pop() # Pop the IK soln with the lowest score
    else: # no solns found, notify user
        print "IK did not return a solution for pose", targetKDLpose
    return solnState
  
def arm_IK_least_change( lastJointState , targetPose ):
    if targetPose.__class__ == Pose: # Convert to KDL pose for IK solns
        targetPose = KDL_pose_from_Vector_Pose( targetPose )
    iterLimit = 16 # Maximum number of solutions to attempt
    IKsolns = PriorityQueue() # Queue to hold ranked IK solns
    solnState = None
    for i in xrange(iterLimit): # Per attempt
        # armSolnState = MODEL.arm_IK( targetPose ) # Request a soln with timeout defined by 'arm_IK'
        armSolnState = MODEL.arm_IK_guess( targetPose , lastJointState ) # Will this really result in different solutions?
        if isinstance( armSolnState , ( list , np.ndarray ) ): # if the soln is non-empty
            # cost = arm_link_high_score( armSolnState )
            cost = tuned_joint_cost( np.subtract( armSolnState , lastJointState ) , [1,1,1,1,1,1,1] )
            IKsolns.push( vec_copy( armSolnState ) , cost ) # push the soln onto the queue with the calc'd score
    if not IKsolns.isEmpty(): # If there was at least 1 solution found
        solnState = IKsolns.pop() # Pop the IK soln with the lowest score
    else: # no solns found, notify user
        print "arm_IK_least_change: IK did not return a solution for pose", targetPose
    return solnState   

def arm_IK_least_search( lastJointState , targetPose ):
    if targetPose.__class__ == Pose: # Convert to KDL pose for IK solns
        targetPose = KDL_pose_from_Vector_Pose( targetPose )
    iterLimit = 16 # Maximum number of solutions to attempt
    IKsolns = PriorityQueue() # Queue to hold ranked IK solns
    solnState = None
    for i in xrange(iterLimit): # Per attempt
        # armSolnState = MODEL.arm_IK( targetPose ) # Request a soln with timeout defined by 'arm_IK'
        # armSolnState = MODEL.arm_IK_guess( targetPose , lastJointState ) # Will this really result in different solutions?
        armSolnState = MODEL.arm_nearby_search( targetPose , lastJointState )
        if is_vector( armSolnState ): # if the soln is non-empty
            # cost = arm_link_high_score( armSolnState )
            cost = tuned_joint_cost( np.subtract( armSolnState , lastJointState ) , [1,1,1,1,1,1,1] )
            IKsolns.push( vec_copy( armSolnState ) , cost ) # push the soln onto the queue with the calc'd score
    if not IKsolns.isEmpty(): # If there was at least 1 solution found
        solnState = IKsolns.pop() # Pop the IK soln with the lowest score
    else: # no solns found, notify user
        print "arm_IK_least_change: IK did not return a solution for pose", targetPose
    return solnState
 
# TODO: Maybe this is completely overworked, and I can just use the IK with guess to arrive at an appropriate spline trajectory without large loops 
def gen_trajectory_from_waypoints( transTime , timeStep  , *waypoints , **kwargs ):
    """ Attempt to generate a trajectory that is a smooth blend between each waypoint """
    # NOTE: Each waypoint is a Vector.Pose
    
    # Space out the point the waypoints in time
    stepTimes = incr_steps( 0 , transTime , timeStep ) # Calc a list of steptimes for each of the points in the trajectory
    dbgLog(3, "gen_trajectory_from_waypoints:", len(stepTimes), "total points to generate" )
    spacing = trunc( ( len( stepTimes ) - 1 ) / ( len( waypoints ) - 1 ) ) # Calc the spacing among the points that is as even as possible
    wpDices = [0] # A list of waypoint locations, the first waypoint is the start of the trajectory
    nxtDex = spacing
    for i in xrange( 1 , len( waypoints ) - 1 ): # For each waypoint except the first and the last
        wpDices.append( nxtDex ) # Place the waypoint in the proper index along the trajectory
        nxtDex += spacing # Increment by the calculated spacing
    wpDices.append( len( stepTimes ) - 1 ) # The last waypoint is the end of the trajectory
    dbgLog(3, "gen_trajectory_from_waypoints: Waypoints at the following indices",wpDices)
    if get_dbg_lvl() == 3:
        dbgLog(3, "gen_trajectory_from_waypoints: Waypoints are ...")
        for wp in waypoints:
            dbgLog(3, wp)

    trajectory = [] # All the points in the trajectory
    
    # Get an acceptable IK soln for the first waypoint
    if 'initialState' in kwargs: # if an initial state was supplied, it is the first point in the trajectory
        trajectory.append( {'t': 0.00 , 'pos': kwargs['initialState'] } )
        dbgLog(3, "gen_trajectory_from_waypoints: Received initial state, appending", trajectory[0] )
    else: # else there was no initial state supplied, so calc an appropriate state for the first point as a seed for the rest of the trajectory
        dbgLog(3, "gen_trajectory_from_waypoints: No initial state recieved, calculating for", waypoints[0] )
        trajectory.append( {'t': 0.00 , 'pos': arm_IK_best( waypoints[0] ) } )
    
    lastPose = waypoints[0] # the last waypoint, the one we want to belnd from 
    stepNum = 1
    brkWarn = False
    for pntDex in range( 1 , len(waypoints) ): # For each waypoint after the first (first calc'd above)
        # Calculate the change between this pose and the next
        dbgLog(3, "gen_trajectory_from_waypoints: Blending from",lastPose,"to",waypoints[pntDex],"in",wpDices[pntDex] - wpDices[pntDex-1] + 1,"steps")
        pBlend = Pose.blend_poses( lastPose , waypoints[pntDex] , wpDices[pntDex] - wpDices[pntDex-1] + 1 ) # Blend poses between waypoints
        if get_dbg_lvl() == 3:
            dbgLog(3, "gen_trajectory_from_waypoints: printing all poses for wrist to assume")
            for pose in pBlend:
                dbgLog(3,  pose )
                
        lastPose = waypoints[pntDex] # store the current waypoint as the start of the blend for the next iteration
        
        # TODO: TEST THIS        
        
        for poseDex in range( 1 , len(pBlend) ): # For each blended pose between this one and the next, excluding the first
            dbgLog(3, "gen_trajectory_from_waypoints: Pose",poseDex,". Working on next state from " , trajectory[-1]['pos'] , "to" , pBlend[poseDex] )
            # 1. Try with the previous position as the guess
            nxtState = arm_IK_least_change( trajectory[-1]['pos'] , pBlend[poseDex] ) # Get an IK solution for this pose in the progression
            if check_solns_ok( nxtState ): # If the IK produced a jointState
                trajectory.append( {'t': stepTimes[stepNum] , 'pos': nxtState } ) # Append a joint command for this timestep
                dbgLog(3, "gen_trajectory_from_waypoints: Got a soln for step",stepNum,"jointState:", trajectory[-1]['pos'] )
            else:
                # 2. Try multiple guesses near the previous position
                nxtState = arm_IK_least_search( trajectory[-1]['pos'] , pBlend[poseDex] )
                if check_solns_ok( nxtState ): # If the IK produced a jointState
                    trajectory.append( {'t': stepTimes[stepNum] , 'pos': nxtState } ) # Append a joint command for this timestep
                    dbgLog(3, "gen_trajectory_from_waypoints: Got a soln for step",stepNum,"jointState:", trajectory[-1]['pos'] )
                else:
                    # 3. Guess randomly!
                    nxtState = arm_IK_best( pBlend[poseDex] )
                    if check_solns_ok( nxtState ): # If the IK produced a jointState
                        trajectory.append( {'t': stepTimes[stepNum] , 'pos': nxtState } ) # Append a joint command for this timestep
                        dbgLog(3, "gen_trajectory_from_waypoints: Got a soln for step",stepNum,"jointState:", trajectory[-1]['pos'] )
                    else: # else no valid IK returned, raise error, we cannot use this trajectory
                        # raise ValueError("gen_trajectory_from_waypoints: Failed to generate IK for " + str( pBlend[poseDex] ))
                        raise Warning("gen_trajectory_from_waypoints: Failed to generate IK for " + str( pBlend[poseDex] ))
                        brkWarn = True
                        break
            stepNum += 1
        if brkWarn:
            break
            
    return trajectory
  
def check_solns_ok( *solns ):
    """ Return true if all the 'solns' are joint states of the same length, otherwise return false """
    OK = True
    for soln in solns: # For each soln, check that it is indeed an array
        if not is_vector( soln ): # If one is not an array, bad batch, return false
            OK = False
            break
    if OK: # If all are arrays, check that they are the same len
        checkLen = len( solns[0] ) # inspect the first soln
        for soln in solns:
            if len(soln) != checkLen: # if the length of one of the solns does not match the first
                OK = False
                break
    if not OK:
        solnStr = ""
        for soln in solns: solnStr += str(soln) + endl
        print "check_solns_ok: One of {" , solnStr , "} was not a joint state or had the wrong dimensionality"
    return OK
            
def arm_approach_trajectory( bgnJointState , finalVecPose , approachVector , transTime , timeStep ): # Requires model init
    """ Return a trajectory 'bgnJointState' to 'finalPose', with a stop at 'approachVector' away from the final pose with total 'transTime' divided into 'timeStep' """
    # NOTE: This function builds a trajectory from the beginning joint state to the end of the 'approachVector' most distant from the object
    #       to grasp, then maintaining a constant velocity at the midpoint, moves from this point to the 'finalPose'
    # NOTE: 'approachVector' is defined from the most distant point at the tail and the end position at the head
    # NOTE: 'finalVecPose' is a Vector.Pose
    trajectory = None 
    # Get the pose of 'bgnJointState'
    bgnPose = Vector_Pose_from_KDL_pose( MODEL.arm_FK( bgnJointState ) )
    if not MODEL.DisableGazebo:
        AXES_SPWN( bgnPose )
    
    # Find the midpoint pose and obtain arm IK
    midPosn = np.subtract( finalVecPose.position , approachVector )
    overallTurn = Quaternion.diff_from_to( bgnPose.orientation , finalVecPose.orientation )
    k , rot = overallTurn.get_k_rot()
    midPose = Pose( midPosn , Quaternion.k_rot_to_Quat(k, rot/2.0) ) # Rotate halfway to the final pose
    if not MODEL.DisableGazebo:    
        AXES_SPWN( midPose ) 
    nn2Pose = Pose( vec_avg( bgnPose.position , midPosn ) , midPose.orientation )
    if not MODEL.DisableGazebo:    
        AXES_SPWN( nn2Pose )
    #nn2Soln = arm_IK_best( KDL_pose_from_Vector_Pose( nn2Pose ) )
    #midSoln = arm_IK_best( KDL_pose_from_Vector_Pose( midPose ) )
    nn4Posn = np.subtract( finalVecPose.position , np.divide( approachVector , 2 ) )
    nn4Pose = Pose( nn4Posn , finalVecPose.orientation ) 
    if not MODEL.DisableGazebo:    
        AXES_SPWN( nn4Pose )
    nn4Soln = arm_IK_best( KDL_pose_from_Vector_Pose( nn4Pose ) )
    # Obtain IK from the endpoint pose
    if not MODEL.DisableGazebo:
        AXES_SPWN( finalVecPose )
    #endSoln = arm_IK_best( KDL_pose_from_Vector_Pose( finalVecPose ) )
            
    # if is_vector( bgnJointState ) and is_vector( midSoln ) and is_vector( endSoln ) and len(bgnJointState) == len(midSoln) == len(endSoln):
    # if check_solns_ok( bgnJointState , nn2Soln , midSoln , nn4Soln , endSoln ):
        # If the beginning, 2nd, middle, 4th, and end states are valid vectors of equal length, proceed with construction of a trajectory
    # moveTo = cubic_spline_traj_smooth_accel( transTime , timeStep , bgnJointState , nn2Soln , midSoln )
    dbgLog(3, "arm_approach_trajectory: Calculating the move towards object")
    moveTo = gen_trajectory_from_waypoints( transTime , timeStep  , bgnPose , nn2Pose , midPose , initialState=bgnJointState )
    # apprch = cubic_spline_traj_smooth_accel( transTime , timeStep , midSoln       , nn4Soln , endSoln )
    dbgLog(3, "arm_approach_trajectory: Calculating the approach to the object")
    apprch = gen_trajectory_from_waypoints( transTime , timeStep  , midPose , nn4Pose , finalVecPose, initialState=moveTo[-1]['pos'] )
    trajectory = concat_trajectories( moveTo , apprch )
        
    return trajectory # Return a valid trajectory or None

def reorientation_trajectory( bgnJointState , center , finalVecPose , transTime , timeStep  ):
    """ Reorient the object in mid-air about a center """
    trajectory = None 
    # Get the pose of 'bgnJointState'
    bgnPose = Vector_Pose_from_KDL_pose( MODEL.arm_FK( bgnJointState ) )
    offsetFrmCtr = np.subtract( bgnPose.position , center )
    # Find the midpoint pose and obtain arm IK
    # midPosn = np.subtract( finalVecPose.position , approachVector )
    overallTurn = Quaternion.diff_from_to( bgnPose.orientation , finalVecPose.orientation )
    k , rot = overallTurn.get_k_rot()
    midPose = Pose( np.add( center , # Rotate halfway to the final pose about the center of the object
                            Quaternion.k_rot_to_Quat( k , rot/2.0 ).apply_to(offsetFrmCtr) ) , 
                    Quaternion.k_rot_to_Quat( k , rot/2.0 ) ) 
    midSoln = arm_IK_best( KDL_pose_from_Vector_Pose( midPose ) )
    # Obtain IK from the endpoint pose
    endSoln = arm_IK_best( KDL_pose_from_Vector_Pose( finalVecPose ) )
    
    if is_vector( bgnJointState ) and is_vector( midSoln ) and is_vector( endSoln ) and len(bgnJointState) == len(midSoln) == len(endSoln):
        # If the beginning, middle, and end states are valid vectors of equal length, proceed with construction of a trajectory
        trajectory = cubic_spline_traj_smooth_accel( transTime , timeStep , bgnJointState , midSoln , endSoln )
    else: # else the prereqs for a valid trajectory were not met, return none
        print "reorientation_trajectory: One of {" , bgnJointState , midSoln , endSoln , "} was not a joint state or had the wrong dimensionality"
        
    return trajectory # Return a valid trajectory or None

def diagnose_trajectory( trajectory ):
    """ Something is amiss: Arm swings wildly, knocks over the object, and results are much worse than simple joint commands overall """
    # Let's start by plotting all the joint angles across the trajectory to look for discontinuities
    colors = [ 'b' , 'g' , 'r' , 'c' , 'm' , 'y' , 'k' , 'w' ] # basic colors
    fig1 = plt.figure(num=1)
    jointSeries = []
    for jointNum in xrange( len(trajectory[0]['pos']) ): # build a time series for each of the robot joints
        jointSeries.append( joint_time_series(trajectory, jointNum) )
        plt.plot(jointSeries[-1][0], jointSeries[-1][1], c=colors[jointNum])
    plt.grid() # URL: http://stackoverflow.com/a/8210686
    plt.xlabel('Time [s]', fontsize=14) # URL: http://matplotlib.org/users/pyplot_tutorial.html
    plt.ylabel('Angle [rad]', fontsize=14)
    plt.title('Joint positions over time')
    plt.show()
    
    # Now plot the trajectory in space to better see what was asked for, was it correct
    spacePoints = []
    for point in trajectory:
        spacePoints.append( Vector_Pose_from_KDL_pose( MODEL.arm_FK( point['pos'] ) ).position )
    plot_chain(spacePoints)
    
def arm_execute_trajectory( trajectory ): # Requires model init
    """ Execute the given arm trajectory """
    lastTime = -500
    for jointState in trajectory:
        lastTime = jointState['t'] # This isn't so useful
        MODEL.arm_JointCommand( jointState['pos'] ) # 'arm_JointCommand' has a built-in delay in the nu,ber of times it repeats the joint command
   
def trajectory_length( trajectory ): # Requires model init
    """ Return the trajectory length (linear and angular, in that order) in the units of the URDF (normally meters and radians) """
    totalLen = 0 # Total linear path length for the trajectory
    totalAng = 0 # Total angular path length for the trajectory
    lastPose = Vector_Pose_from_KDL_pose( MODEL.arm_FK( trajectory[0]['pos'] ) )
    for pntDex in xrange( 1 , len(trajectory) ):
        pntPose = Vector_Pose_from_KDL_pose( MODEL.arm_FK( trajectory[pntDex]['pos'] ) )
        totalLen += vec_mag( np.subtract( pntPose.position , lastPose.position ) )
        k , rot = Quaternion.diff_from_to( pntPose.orientation , lastPose.orientation ).get_k_rot()
        totalAng += rot
        lastPose = pntPose # Store this pose for comparison to the next iteration
    return totalLen , totalAng 

def proximity_to_table( graspPairM , supportIndex ): # requires regrasp init
    # """ Given the OBJECT and GRASP_PAIR return the distance from the center of the GRASP_PAIR to the supporting surface of the POSE """
    pairPts = ( grasp_pnt_at_addr( MESHFCTP['trialGraspPoints'] , graspPairM[0] ) , grasp_pnt_at_addr( MESHFCTP['trialGraspPoints'] , graspPairM[1] ) ) # MESH FRAME
    pairCntr = vec_avg( *pairPts ) # MESH FRAME
    supprtingFacet = MESHFCTP['clusterStability'][supportIndex] # Fetch the supporting plane # MESH FRAME
    return vec_dist_to_plane( pairCntr , supprtingFacet[4]['origin'] , supprtingFacet[4]['zVec'] ) # Compute the distance from grasp pair to support plane # MESH FRAME

axesCounter = 0 # Don't really need this anymore                                                                     
def exec_plan():
    """ Execute an example plan """
    global axesCounter    
    
    supportDex = id_target_support_surface() # Try to identify the supporting surface
    trgtPose = target_pose()
    goalTag = MESHGRPH[4].tag # Choosing an (arbitary) goal surface, change if not a feasible goal
    print "Supported at index " , supportDex # 12
    startGrasp = random.choice( MESHGRPH[supportDex].nodes.item_list() ) # Choose a random starting grasp at the starting pose
    testPlan = plan( startGrasp , goalTag ) # Generate a plan to get to that surface
    # Each action in the plan takes the form:
    # transition/transfer , Node.tag: BGN-->END ,              Graph.tag: BGN-->END                         , target ref , target graph ref
    # ( edge.weight ,       (parent[0].tag , edgeTarget.tag) , (parent[0].graph.tag , edgeTarget.graph.tag) , edgeTarget , edgeTarget.graph )
    for action in testPlan:
        print action
    # ('transfer',   ('Node_3691', 'Node_4058'), ('Graph_13', 'Graph_13'), <ResearchUtils.Graph.Node object at 0x7f202f701410>, <ResearchUtils.Graph.Graph object at 0x7f2031342450>)
    # ('transition', ('Node_4058', 'Node_1210'), ('Graph_13', 'Graph_5' ), <ResearchUtils.Graph.Node object at 0x7f20403b8bd0>, <ResearchUtils.Graph.Graph object at 0x7f2040483550>)
    # The initial transition means we chose a bad starting point, let's focus on the transition since that is the hard work  
        
    # The two nodes of a transition should have the same alias, they represent the same grasp pair at different poses
    print testPlan[0][3].alias # get the grasp pair at the starting node so that we can approach the object
    pair = testPlan[0][3].alias # Fetch the grasp pair for the next action
    # Locate the grasp pair in the "world" frame
    pairPts = ( grasp_pnt_at_addr( MESHFCTP['trialGraspPoints'] , pair[0] ) , grasp_pnt_at_addr( MESHFCTP['trialGraspPoints'] , pair[1] ) )
    pairPtsW = ( target_pnt_world_frame( trgtPose , mesh_to_trgt( pairPts[0] ) ) , target_pnt_world_frame( trgtPose , mesh_to_trgt( pairPts[1] ) ) )
    # TODO: Check that the grasp pair are not too far apart for the gripper to grasp
    pairCntrW = vec_avg( *pairPtsW ) # Origin of the grasp pair visualization
    # Generate a display pose to paint the pair in Gazebo
    yGPr = vec_unit( np.subtract( pairPtsW[0] , pairCntrW ) )
    xGPr = np.cross( yGPr , [0.0 , 0.0 , 1.0] ) 
    zGPr = np.cross( xGPr , yGPr )
    pairQuat = Quaternion.principal_rot_Quat( xGPr , yGPr , zGPr ) 
    # Paint the pair in Gazebo
    displayPose = ROS_geo_pose_from_Vector_Pose( Pose( pairCntrW , pairQuat ) )
    # 4. Display belief in Gazebo
    PAIR_MOVE( displayPose )
    
    norm = vec_unit( np.subtract( pairPtsW[1] , pairCntrW ) ) # World frame
    hullX = vec_unit( np.cross( [0,0,1] , norm ) ) # World Frame
    if np.dot( hullX , pairCntrW ) > 0: # NOTE: This criterion will be formulated differently if the robot is not at the origin
        hullX = np.multiply( hullX , -1.0 ) # World frame
        norm  = np.multiply(  norm , -1.0 ) # World frame
    hullY = np.cross( norm , hullX ) # world frame
    # NOTE: The above will fail if the grasp pair is exactly aligned with the z-axis, for now assuming that this is rare enough to ignore
    
    # Put 'hullX' and 'hullY' in the target frame
    invTrgtQuat = Quaternion.inverse( trgtPose.orientation )
    hullXT = invTrgtQuat.apply_to( hullX ) # Target frame
    hullYT = invTrgtQuat.apply_to( hullY ) # Target frame
    
    # Flatten the model around the grasp pair
    try:
        graspPlaneProj = project_to_plane_and_hull( TARGETPNTS , norm , hullXT, hullYT ) 
    except Exception as err:
        print "exec_plan: Formation of the grasp plane projection failed" , str(err)
        
    testLen = 2.0 * max( span_R3_pts( TARGETPNTS ) ) # Return the largest extent along any of the principal axes, and assume a line twice this long intersects the hull
    N = 20 # Test at 20 angles around the circle
    testAngles = np.linspace(0 , 2*pi , N)
    radials = [ [ (0.0 , 0.0) , tuple( polr_2_cart( [ testLen , angle ] ) ) ] for angle in testAngles ] # Generate radials from the grasp center on the grasp plane
    outerMost = bounding_points_from_hull( graspPlaneProj ) # Get the projection's outermost points
    hullSegments = []
    for i in range(len(outerMost)): # Construct line segments from the outermost points of the convex hull
        hullSegments.append( [ tuple( elemw( i ,outerMost ) ) , tuple( elemw( i+1 ,outerMost ) ) ] )
    
    rankedRads = PriorityQueue()    
    for raDex , radl in enumerate(radials): # for each of the radials
        for segDex , segment in enumerate(hullSegments): # for each segment of the hull's bounding polygon
            intersection = intersect_pnt_2D( radl , segment ) # Calc the intersection point (False if no intersection)
            if intersection:
                rankedRads.push( raDex , vec_mag(intersection) ) # Radials are ranked by the distance of the intersection point from origin
                #                                                  the shorter the better
    # What is an acceptable approach angle?
    # A. Pop radials until while IK fails until distance becomes ungraspable # pop_with_priority
    # B. Accept an angle that is from 0 to 90deg above the table (vertical grasp plane) or +/-90deg from a line to the robot (horizontal grasp plane)
    
    # TODO: Define an infeasible gripper depth
    
    index , depth = rankedRads.pop_with_priority() # Get the radial index with the least depth and the associated depth
    radAngle = testAngles[index] # Get the angle of the radial index
    
    # Determine the desirability of the angle - NO, for now just try each approach and only pop the next if no IK sol'n returned
    
    # Get a vector in the world frame associated with the chosen radial
    chosenRadialGP = polr_2_cart( [1 , radAngle] ) # Generate a unit vector expressed in the grasp plain # Unit vector
    # Use 'hullX' and 'hullY' as basis vectors to get our chosen radial back in the world frame
    chosenRadialW = np.add( np.multiply( hullX , chosenRadialGP[0] ) , np.multiply( hullY , chosenRadialGP[1] ) ) # World frame # Unit Vector
    # Now we can construct basis vectors for the palm in the world frame, orientation part of the pose
    palmBasisXW = np.multiply( chosenRadialW , -1 ) # World Frame
    palmBasisZW = norm # World frame
    palmBasisYW = np.cross( palmBasisZW , palmBasisXW ) # World frame
    print "Palm vectors orthonormal:" , check_orthonormal( palmBasisXW , palmBasisYW , palmBasisZW ) # True # Nice!
    # Now define an offset from the grasp center that places the gripper favorably
    
    graspOffsetW = transform_by_bases( GRASPOFFSET_P , palmBasisXW , palmBasisYW , palmBasisZW )
    palmOrgn = np.subtract( pairCntrW , graspOffsetW )
    palmPose = Pose( palmOrgn , Quaternion.principal_rot_Quat( palmBasisXW , palmBasisYW , palmBasisZW ) )
    
    # Display an axis at the target location
    #axesCounter += 1
    displayPose = ROS_geo_pose_from_Vector_Pose( palmPose ) 
    #AXES_SPWN( displayPose ) # Spawn axes for palm target location 
    
    # Attempt IK to the designated location
    targetKDLpose = KDL_pose_from_Vector_Pose( palmPose )
    # arm_IK_move( targetKDLpose )   
    
    # approachVec = np.multiply( chosenRadialW , -0.1) # Direction OK, but too far from the arm, just descend from on high
    approachVec = [  0.0 ,  0.0 , -0.2 ]    
    
    # Calculate the approach trajectory
    approachTraj = arm_approach_trajectory( MODEL.arm_state_list() , palmPose   , approachVec , 4         , MODEL.trajTimeStep )
    #                                       current jointState     , final pose , approach    , exec time , timestep 
    if isinstance( approachTraj , list ):
        diagnose_trajectory( approachTraj ) # Find out why the arm goes all over the place!
        arm_execute_trajectory( approachTraj )
    else:
        print "exec_plan: Arm approach trajectory was not valid!"
        exit() # something is wrong, quit!
    
    
    # Grasp
    hand_open() # mock a grasp maneuver, closing the hand still causes major problems
    print "Preparing to grasp ... "
    MODEL.sleep_for(1)
    toggle_grip_flag() # "fix" the object to the palm frame
    
    # Lift the object straight up
    
    palmPose = Pose( np.add( palmOrgn , [0.0 , 0.0 , 0.2] ) , # move to a position far enough in the air to avoid collision that is still primary workspace
                     Quaternion.principal_rot_Quat( palmBasisXW , palmBasisYW , palmBasisZW ) )
    
    # Display an axis at the target location 
    #axesCounter += 1
    displayPose = ROS_geo_pose_from_Vector_Pose( palmPose ) 
    #AXES_SPWN( displayPose )
    
    # Lift
    # Attempt IK to the designated location
    targetKDLpose = KDL_pose_from_Vector_Pose( palmPose )
    arm_IK_move( targetKDLpose )
    
    MODEL.sleep_for(2) # Maybe the rotation pose is messing up because the last command hadn't finished?
    
    # Now find the putdown pose
    goalFaceIndex = testPlan[1][4].alias # This is the index of the putdown pose
    # goalNormM = MESHFCTP['clusterStability'][goalFaceIndex][1] # Get the normal of the goal # mesh frame
    
    # FIXME: The start and goal face appear to be on the same side of the box, for the sake of example pick a different goal manually
    #        The system thinks it has to make a transition to get to different lumps on the bottom of the box
    goalNormM = MESHFCTP['clusterStability'][6][1] # Get the normal of the goal # mesh frame # 12 is starting index
    #                                        ^--- Tried: 0 1 2 3 4 5

    trgtPose = target_pose() # refresh the target pose # world frame
    goalNormW = trgtPose.orientation.apply_to( goalNormM ) # target frame # Target frame and mesh frame have the same orientation
    pointGoalDown = Quaternion.shortest_btn_vecs( goalNormW , [  0.0 ,  0.0 , -1.0 ] ) # This will turn the goal normal facing the table
    target2palmW = object_palm_relative() # world frame
    # just rotate the wrist pose to point the goal down, keeping the origin of the target in place
    palmPose.orientation = Quaternion.compose_rots( palmPose.orientation , pointGoalDown )   
    palmPose.position = np.add( trgtPose.position , pointGoalDown.apply_to(target2palmW.position) )
    
    # TODO: Rotate about world z for a favorable arm pose, assume that pointing the palm z away from the robot is best
    
    # Display an axis at the target location to find out if it is reasonable # TODO: This really needs to be its own function!
    #axesCounter += 1
    displayPose = ROS_geo_pose_from_Vector_Pose( palmPose ) 
    #AXES_SPWN( displayPose )
    
    response = raw_input("Ok to continue? [y/n]: ")
    if response.lower() != 'y':
        exit() # The user disapproves, there's no point in going on
        
    # Attempt to execute the turn
        
    # Attempt IK to the designated location
    targetKDLpose = KDL_pose_from_Vector_Pose( palmPose )
    arm_IK_move( targetKDLpose )
        
    trgtPose = target_pose() # refresh the target pose # world frame
    
    # Calc the -z displacement that will bring the object acceptably close to the table for putdown

# == Development , Debug , and Extra ==
""" This was written for learning and testing purposes, kept here for reference, and just in case """
    
def wrist_pos_radial( XYZpos ):
    """ Send wrist link to an [x,y,z] position 'XYZpos', with plane of palm parallel to world plane """
    global axesCounter
    
    while not MODEL.gotFirstState: # While no state information is available
        time.sleep(1) # wait for information
    temp = XYZpos.copy() if isinstance( XYZpos , np.ndarray ) else XYZpos[:]
    temp[2] = 0.0 # Points out radially from the origin (where the robot is) and parallel to the world plane
    zPalm = vec_unit( temp ) # Z of the wrist frame is parallel to the world plane and points from the origin towards the goal position
    yPalm = vec_unit( np.cross( [0.0 , 0.0 , 1.0] , zPalm ) ) # The y basis vector is perpendicular to both the wrist and world z axes
    xPalm = np.cross( yPalm , zPalm ) # x asis is found easiliy from the first two
    poseQuat = Quaternion.principal_rot_Quat( xPalm , yPalm , zPalm ) # Find the rotation from the world basis to the wrist basis
    # 'poseQuat' seems to put the target model in the desired position
    
    # Spawn an axes model where the hand is going to go    
    targetPose = ROS_geo_pose_from_pos_quat( XYZpos , poseQuat ) # Generate a pose for the axes model from target position and orientation
    spawn_model_prox = rospy.ServiceProxy('gazebo/spawn_sdf_model', SpawnModel) # Get a handle to the Gazebo model spawner
    modelPath = rospack.get_path('grasp_anything') + '/urdf/block_axes.urdf' # Build a path to the model URDF
    f = open(modelPath,'r') # Open the URDF for reading
    sdff = f.read() # and read it
    rospy.wait_for_service('gazebo/spawn_sdf_model') # Wait for the model loader to be ready
    spawn_model_prox("axes" + str(axesCounter), sdff, "robotos_name_space", targetPose, "world") # gen model name and place the axes at the specfied pose
    axesCounter += 1 # Make sure each target axes have a unique model name, or Gazebo will refuse to spawn it
    
    # URL, Get model information from Gazebo: http://answers.ros.org/question/240371/waiting-for-gazebo_msgsgetlinkstate-causes-python-to-freeze/
    model_info_prox = rospy.ServiceProxy('/gazebo/get_link_state', GetLinkState) # Get a handle to the Gazebo model spawner
    rospy.wait_for_service('/gazebo/get_link_state') # Wait for the link state service to become available 
    
    # get a KDL pose, request IK solution, Determine if IK sol'n found
    targetKDLpose = KDL_pose_from_new_basis( XYZpos , xPalm , yPalm , zPalm ) # Generate a KDL-brand pose for the wrist link
    
    # Attempt to obtain multiple IK solutions in hopes that one will not result in wild sweeps
    # TODO: Make this pattern into a function
    iterLimit = 8 #4 
    IKsolns = PriorityQueue()
    beginState = MODEL.arm_state_list() # list( joint_states_arm_only( model.jointState ).position )
    for i in xrange(iterLimit):
        armSolnState = MODEL.arm_IK( targetKDLpose )
        if np.any( armSolnState ):
            diff = sum_abs_diff_lists( beginState , armSolnState )
            IKsolns.push( armSolnState[:] , diff )
            
    if not IKsolns.isEmpty(): # If there was a solution found
        
        solnState = IKsolns.pop() # Pop the IK soln with the lowest total angular distance
    
        MODEL.arm_JointCommand( solnState )

        MODEL.sleep_for(1)        
        
        finalState = MODEL.arm_state_list() #list( joint_states_arm_only( MODEL.jointState ).position )
        diffState = np.subtract( solnState , finalState )
        lists_as_columns_with_titles( ['final','desired','difference'], [finalState , solnState , diffState] )
        
        sep("Execution Results")
        print "Target Pose:" ,    endl , targetKDLpose
        print "FK(IK) Pose:" ,    endl , MODEL.arm_FK( solnState )
        print "FK(state) Pose:" , endl , MODEL.arm_FK( finalState )
        print "Link 7 Pose:"    , endl , model_info_prox( "lbr4_allegro::lbr4_7_link" , "world" )
    else:
        print "IK did not return a solution for position", XYZpos
        
def test_robot_attachment():
    """ Try attaching an axes model to a robot frame and see what happens """
    global attachCounter
    spawn_model_prox = rospy.ServiceProxy('gazebo/spawn_sdf_model', SpawnModel) # Get a handle to the Gazebo model spawner
    modelPath = rospack.get_path('grasp_anything') + '/urdf/block_axes.urdf' # Build a path to the model URDF
    f = open(modelPath,'r') # Open the URDF for reading
    sdff = f.read() # and read it
    rospy.wait_for_service('gazebo/spawn_sdf_model') # Wait for the model loader to be ready
    targetPose = ROS_geo_pose_from_lists( [ 0.5 , 0.5 , 0.5 ] , [0,0,0,1] )
    spawn_model_prox("axesAttached" + str(attachCounter), sdff, "robotos_name_space", targetPose, "lbr4_allegro::lbr4_7_link") # gen model name and place the axes at the specfied pose
    attachCounter += 1
    # This is not working as planned
    # URL: http://answers.ros.org/question/240737/gazebo-object-spawned-in-robot-link-frame-does-not-move-with-robot-link/
    
def test_bowl_position():
    """ Report the bowl position """
    # model_info_prox = rospy.ServiceProxy('/gazebo/get_model_state', GetModelState) # Get a handle to the Gazebo model spawner
    # rospy.wait_for_service('/gazebo/get_model_state') # Wait for the link state service to become available
    print "Bowl Pose:"    , endl , MODEL.model_info_prox( "bowl" , "world" ) # Works!

def bowl_teleport( xyz , k , rot ): # 2016-08-02: Works as expected
    """ Test giving the bowl a new pose a new 'xyz' location and an axis 'k' - angle 'rot' orientation """
    # NOTE: Express params in the "world" frame
    # ~/set_model_state : gazebo_msgs/SetModelState
    modify_model_prox = rospy.ServiceProxy('gazebo/set_model_state', SetModelState) # Get a handle to the Gazebo model spawner
    # URL, setting a Gazebo model state: http://docs.ros.org/electric/api/srs_user_tests/html/move__robot__sim_8py_source.html
    nuBowlState = ModelState()
    nuBowlState.model_name = "bowl"
    nuBowlState.pose = ROS_geo_pose_from_pos_quat( xyz , Quaternion.k_rot_to_Quat( k , rot ) )
    modify_model_prox( nuBowlState )

def random_model():
    """ Choose a random model and spawn in Gazebo """
    fName = random.choice( YCB_URDF_LIST )
    modelPath = os.path.join( YCB_URDF_FLDR , fName )
    nameStr = fName[:-5] # Trim ".urdf"
    modelList.append( nameStr ) # Register the model that was spawned
    
    spawn_model_prox = rospy.ServiceProxy('gazebo/spawn_sdf_model', SpawnModel) # Get a handle to the Gazebo model spawner
    rospy.wait_for_service('gazebo/spawn_sdf_model') # Wait for the model loader to be ready

    f = open(modelPath,'r') # Open the URDF for reading
    sdff = f.read() # and read it
    
    props = get_urdf_phys_attr( modelPath )

    randAngle = (random.random() - 0.5) * 2 * pi # any angle between -pi and pi
    randRdius = 1 + random.random() # any radius between 1 and 2
    xy = polr_2_cart( [ randRdius , randAngle ] )
    targetPose = ROS_geo_pose_from_Vector_Pose( Pose.pos_no_turn( [ xy[0] , xy[1] , props['COM'][2] + 0.02 ] ) )
    
    # sleep_seconds(2) # This probably doesn't matter
    
    spawn_model_prox(nameStr, sdff, "robotos_name_space", targetPose, "world") # gen model name and place the axes at the specfied pose
        
def del_model( modelName ): # FIXME: Freezes Python, DO NOT USE!
    """ Remove the model with 'modelName' from the Gazebo scene """
    # delete_model : gazebo_msgs/DeleteModel
    del_model_prox = rospy.ServiceProxy('gazebo/delete_model', DeleteModel) # Get a handle to the Gazebo model spawner
    # rospy.wait_for_service('gazebo/delete_model') # Wait for the model loader to be ready # FREEZES EITHER WAY
    del_model_prox(modelName) # Remove from Gazebo
    
def del_registered( modelName ): # FIXME: Freezes Python, DO NOT USE!
    """ Remove the registered model with 'modelName' from the Gazebo scene """
    if modelName in modelList: # If the model was previously registered
        del_model( modelName ) # Remove from Gazebo
        modelList.remove(modelName) # Unregister this name
    else: # model was not listed, notify user
        print "del_registered: No model with name", modelName ,"was registered with the simulation environment."

# == End Extra ==    
