#!/usr/bin/env python
# -*- coding: utf-8 -*-
# Template Version: 2016-08-01

# ~~ Future First ~~
from __future__ import division # Future imports must be called before everything else, including triple-quote docs!

"""
HWutil.py , Built on Spyder for Python 2.7
James Watson, 2016 August
Homework utility functions, shortened version of modules written for research, but not wanting to burden the grader with that entire library
"""

# ~~ Imports ~~
# ~ Standard ~
import os
from random import random
# ~ Special ~
import numpy as np

# ~~ Constants , Shortcuts , Aliases ~~
# URL, add global vars across modules: http://stackoverflow.com/a/15959638/893511
EPSILON = 1e-7
infty = 1e309 # URL: http://stackoverflow.com/questions/1628026/python-infinity-any-caveats#comment31860436_1628026
endl = os.linesep
  

# == Added for Project 2 ==  

def vec_random( dim ):
    """ Return a random vector in R-'dim' space with coordinates in [0,1) """
    rtnVec = []
    for i in range(dim):
        rtnVec.append( random() )
    return rtnVec
    
def vec_unit(vec):
    """ Return a unit vector in the direction of 'vec', using numpy """
    return np.divide( vec , np.linalg.norm(vec) )
 
def eq(op1, op2):
    """ Return true if op1 and op2 are close enough """
    return abs(op1 - op2) <= EPSILON
   
def vec_eq(vec1 , vec2):
    """ Return true if two vectors are equal enough, otherwise false """
    if len(vec1) == len(vec2):
        for i in range(len(vec1)):
            if not eq( vec1[i] , vec2[i] ):
                return False
    else:
        return False
    return True


    
def elemw(i, iterable):
    """ Return the 'i'th index of 'iterable', wrapping to index 0 at all integer multiples of 'len(iterable)' """
    return iterable[ i % ( len(iterable) ) ]

# == End Project 2 ==
  
  
# == Added for Project 1 ==
      
def vec_mag(vec):
    """ Return the magnitude of a vector """
    return np.linalg.norm(vec)
    
def is_vector( vec ):
    """ Return true if 'vec' is any of { list , numpy array } and thus may particpate in vector operations """
    return isinstance( vec , ( list , np.ndarray ) )
    
def taxi_diff(op1, op2):
    """ Return the relative Manhattan displacement from 'op1' to 'op2' """
    return [op2[0] - op1[0], op2[1] - op1[1]]

def taxi_dist(op1, op2):
    """ Return the absolute Manhattan distance between 'op1' and 'op2' """
    disp = taxi_diff(op1, op2)
    return abs(disp[0]) + abs(disp[1])

# == End Project 1 ==