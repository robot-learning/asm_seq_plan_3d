#!/usr/bin/env python
'''
Package providing helper classes and functions for performing graph search operations for planning.
'''
import numpy as np
import matplotlib.pyplot as plotter
#from math import pi
from collisions import PolygonEnvironment
import time , os

import cProfile
import re

from random import random , choice
from types import NoneType 
from HWutil import vec_eq , vec_unit , vec_mag , elemw
from UtahUtil import vec_random_limits , accum_print , accum_sep , accum_out_and_clear

from marchhare.marchhare import sep , PriorityQueue
from marchhare.Vector import AABB_span , is_vector , vec_dif_mag
from marchhare.MathKit import product , flip_weighted

_TRAPPED  = 'trapped'
_ADVANCED = 'advanced'
_REACHED  = 'reached'

# ~~ Constants , Shortcuts , Aliases ~~
# URL, add global vars across modules: http://stackoverflow.com/a/15959638/893511
EPSILON = 1e-7
infty = 1e309 # URL: http://stackoverflow.com/questions/1628026/python-infinity-any-caveats#comment31860436_1628026
endl = os.linesep

# === RRT classes and functions ===

class TreeNode:
    """ Search tree node for use with RRT """    
    
    @staticmethod
    def dist_between( n1 , n2 ):
        """ Return the distance between 'n1' and 'n2' in config space """
        return vec_mag(  np.subtract( n1.state , n2.state )  )
    
    def __init__( self , state , parent = None ):
        self.state         = state #- Configuration
        self.children      = [] # --- Successors , possibly leading to the goal configuration
        self.parent        = parent # Parent , for constructing path back to the start configuration
        self.cost_2_parent = 0 # ---- Path length (in config space) to parent node
        self.cost_2_root   = 0 # ---- Path length (in config space) to root   node

    def add_child( self , child ):
        """ Add a successor node """
        # NOTE: No correctness checks are made before adding
        self.children.append( child )
        
    def remove_child_by_ref( self , childRef ):
        """ Remove the object referred to by 'childRef' from 'self.children' , if 'childRef' DNE then fail gracefully """
        try:
            self.children.remove( childRef )
        except ValueError:
            print "WARN , TreeNode.remove_child_by_ref: Reference" , childRef , "DNE in list of successors!"
        
    def __str__( self ):
        """ Return a string representation of the TreeNode """
        return "TreeNode@" + str( self.state )

class RRTSearchTree:
    """ Search tree for use with RRT algorithms , Functions for searching and modifying the tree structure itself """    
    # NOTE: This class does not implement RRT algorithms , See class 'RRT'
    
    def __init__( self , init ):
        """ Create an empty tree with a root node at the initial state """
        self.root     = TreeNode( init ) # The initial state is the root node
        self.nodes    = [ self.root ] # -- Collection of all nodes
        self.edges    = [] # ------------- collection of all edges (Not used for searching)
        self.volume   = 0 # -------------- Volume occupied by the N-Dim bounding box
        self.hasGrown = False # ---------- Flag for whether the bounding box grew during the last volume computation
        self.newest   = self.root # ------ Points to the most-recently-added node

    def find_nearest( self , s_query ):
        """ Find the node nearest to 's_query' and return it with the distance between 's_query' and the node , Linear search """
        min_d = infty # Pick some arbitrarily large distance
        nn = self.root
        for n_i in self.nodes:
            if _DEBUG:
                print "DEBUG:" , s_query , n_i.state
            d = np.linalg.norm( np.subtract( s_query , n_i.state ) )
            if d < min_d:
                nn = n_i
                min_d = d
        return ( nn , min_d )
    
    def get_neighborhood( self , s_query , radius , NshortCut = None ):
        """ Return all of the nodes within 'radius' of 's_query' in a PriorityQueue sorted on cost , Optinally stop search at 'NshortCut' """
        checkShort = True if ( NshortCut != None and NshortCut > 0 ) else False
        count      = 0
        rtnQueue   = PriorityQueue()
        for n_i in self.nodes:
            if checkShort and ( count >= NshortCut ):
                break
            d = np.linalg.norm( np.subtract( s_query , n_i.state ) )
            if d <= radius:
                rtnQueue.push( n_i , # -------------- Node
                               n_i.cost_2_root , # -- Path length from node to tree root
                               tuple( n_i.state ) ) # Hash node by it's state for easy existence check
                count += 1
        return rtnQueue
        
    def compute_AABB_volume( self ):
        """ Compute and store the volume of the AABB enclosing all configurations in the tree """
        allQ = [ n.state for n in self.nodes ]
        span = AABB_span( allQ )
        self.volume = product( span )
        return self.volume
    
    def grew( self ):
        """ Compute and store the volume of the AABB , and Determine if the box has grown """
        oldVol = self.volume
        self.compute_AABB_volume()
        return self.volume > oldVol

    def add_node( self , node , parent ):
        """ Add a node with a designated parent , add node to the list of the parent's successors """
        # 1. Compute Costs
        if parent:
            node.cost_2_parent = TreeNode.dist_between( node , parent ) # ----- Calc the distance between this node and its parent
            node.cost_2_root   = node.cost_2_parent + parent.cost_2_root # Accumulate cost to root
        # 2. Add
        self.nodes.append( node ) # Add node to the master list of nodes
        self.edges.append( ( parent.state , node.state ) ) # Add a record of this edge
        node.parent = parent # Assign parentage to the node
        parent.add_child( node ) # Add the node to the parent's successors
        self.newest = node
        
    def remove_edge( self , state1 , state2 ):
        """ Remove the edge between the two states """
        SHOWDEBUG = True
        try:
            self.edges.remove( ( state1 , state2 ) )
            if SHOWDEBUG:
                print "Edge" , ( state1 , state2 ) , "was removed!"
        except:
            if SHOWDEBUG:
                print "No Edge was removed!"
        
    def add_by_state( self , q , parent ):
        """ Create a new TreeNode at state 'q' and assign it 'parent' """
        nuNode = TreeNode( q )
        self.add_node( nuNode , parent )
        return nuNode
        
    def add_state_seq( self , qLst , parent ):
        """ Add a sequence of states with 'parent' as the root """
        for q in qLst:
            parent = self.add_by_state( q , parent )
        return parent
        
    def add_node_disjoint( self , node ): 
        """ Add a node without parents or edges """
        self.nodes.append( node )
        self.newest = node

    def get_states_and_edges( self ):
        """ Return all states and all edges """
        states = np.array( [ n.state for n in self.nodes ] )
        return ( states , self.edges )

    def search_by_state( self , q_query ): 
        """ Return the first TreeNode that matches state 'q_query', otherwise return None , Linear Search """
        for n in self.nodes:
            if vec_eq( n.state , q_query ):
                return n
        return None

    def get_back_path( self , n ):
        """ Follow parent relationships from 'n' to the root node, Reverse the sequence so that it is a forward path """
        path = []
        # while n.parent is not None:
        while n is not None:
            path.append( n.state )
            n = n.parent
        path.reverse()
        return path
        
    def add_q_to_closest_parent( self , q ):
        """ Create a new TreeNode and assign it to the node with the closest config """
        closest = self.find_nearest( q )[0]
        self.add_node( TreeNode( q ) , closest )

class RRT:
    """ Rapidly-Exploring Random Tree Implementation """

    def __init__( self , num_samples , num_dimensions = 2 , step_length = 1 , lims = None , connect_prob = 0.05 , collision_func = None ):
        ''' Initialize an RRT planning instance '''
        self.K = num_samples # ----------- Number of samples to generate
        self.n = num_dimensions # -------- dimensionality of the space
        self.epsilon = step_length # ----- step size
        self.connect_prob = connect_prob # bias probability
        self.found_path = False # -------- Flag for if the problem has been solved
        self.newestNode = None # --------- Most recent node added by 'Connect' (Strandberg)
        self.newNodeDelta = 0 # ---------- Number of node added by a 'Connect' operation (Strandberg)

        self.in_collision = collision_func # function for checking collisions
        if collision_func is None: # If there was no collision function assigned, use the dummy function so that things don't crash
            self.in_collision = self.fake_in_collision

        # Setup range limits for config space (joint limits, etc)
        self.limits = lims
        if self.limits is None: # If there were no limits passed as params
            self.limits = []
            for n in xrange( num_dimensions ): # for each dimension
                self.limits.append( [ 0 , 100 ] ) # set default limits 0 - 100
            self.limits = np.array( self.limits ) # Convert limits to an np.array
        self.ranges = self.limits[ : , 1 ] - self.limits[ : , 0 ] # Calc the spans of the limits
        

    def get_random_state( self ): 
        """ Return a random state within the limits of the world """
        return vec_random_limits( len( self.limits ) , self.limits )

    def init_tree( self , init ):
        """ Instantiate the tree with one root node """
        self.T = RRTSearchTree( init ) # Start with initial state as the root node
        self.init = np.array( init ) # Store the config of the init state
        
    def build_rrt( self , init , goal ):
        ''' Build the rrt from init to goal , Returns path to goal or None '''
        # NOTE: This is the baseline RRT algorithm implementation
        self.goal = np.array( goal ) # store the config of the goal state
        self.init = np.array( init ) # store the config of the init state
        self.found_path = False # ---- Flag for reached goal

        # Build tree and search
        self.init_tree( init ) # Set up the RRT problem with the specified root node

        result = None
        samples = 0

        while isinstance( result , ( NoneType , bool ) ) and samples < self.K: # run continuously until either the goal or iteration limit is reached
            # 1. Sample either a random point or the goal with P(bias)
            target = self.sample()        
            # 2. Perform extension toward the target and collect result (  )
            result = self.extend( target )
            samples += 1
            if _DEBUG:
                print "Iteration" , samples , "with result" , result
            
        if not isinstance( result , ( NoneType , bool ) ):
            self.found_path = True
            return result
        else:
            return None
        
    # === RRT* =============================================================================================================================
    
    def build_rrt_star( self , init , goal ):
        ''' Build the rrt* from init to goal , Returns path to goal or None '''
        # NOTE: This is the baseline RRT algorithm implementation
        self.goal = np.array( goal ) # store the config of the goal state
        self.init = np.array( init ) # store the config of the init state
        self.found_path = False # ---- Flag for reached goal

        # Build tree and search
        self.init_tree( init ) # Set up the RRT problem with the specified root node

        result = None
        samples = 0

        while isinstance( result , ( NoneType , bool ) ) and samples < self.K: # run continuously until either the goal or iteration limit is reached
            # 1. Sample either a random point or the goal with P(bias)
            target = self.sample()        
            # 2. Perform extension toward the target and collect result (  )
            result = self.extend_star( target )
            samples += 1
            if _DEBUG:
                print "Iteration" , samples , "with result" , result
            
        if not isinstance( result , ( NoneType , bool ) ):
            self.found_path = True
            return result
        else:
            return None
        
    def extend_star( self , target ):
        ''' Perform rrt* extend operation.  q - new configuration to extend towards 
            ~~~~~~~~~~~~
            Return Codes:
            list - Goal found , return path back from start to goal
            True - A new node was added that extends towards the target from the nearest node
            None - Trapped , extension was not successful '''
        # NOTE: This function is the guts of the RRT* algorithm
        SHOWDEBUG  = False
        RADIUS     = self.epsilon * 2.5
        SHORTCOUNT = 7
        
        # 2. Find the closest node to the target
        q_near , dist = self.T.find_nearest( target ) # return ( NODE_REFERENCE , DISTANCE_TO_NODE )
        # 3. Gen an epsilon step towards the target
        q_new = np.add( np.multiply( vec_unit( np.subtract( target , q_near.state ) ) , self.epsilon ) , q_near.state )
        if SHOWDEBUG:
            print "q_new:" , q_new , endl
        # 4. Check the viability of the step
        if not self.in_collision( q_new ):
#            self.T.add_node( TreeNode( q_new ) , q_near )
            
            
#            self.add_node_star_ALT( TreeNode( q_new ) , RADIUS , SHORTCOUNT )
            self.add_node_star( TreeNode( q_new ) , RADIUS , SHORTCOUNT )
            
            
            # 5. Check if we have reached the goal
            if vec_mag( np.subtract( self.goal , q_new ) ) <= self.epsilon:
                # 4.a. If step is goal, gen path from parents and return it
                temp = TreeNode( self.goal )
                self.T.add_node( temp , q_near )
                return self.T.get_back_path( temp ) # Reached
            else:
                return True # Advanced
        
        return None # Trapped
    
    
    def add_node_star( self , node , radius , NshortCut = None ):
        """ Add node in the RRT* fashion """
        SHOWDEBUG = False
        MARGIN = 0.20
        
        # TODO: Test a rewire margin
        
        # ~ Add Phase ~
        #  1. Get the neighborhood around the new node
        neighborHoodQ = self.T.get_neighborhood( node.state , radius , NshortCut )
        #  A. Unspool queue
        if SHOWDEBUG:
            print "About to unspool ..." , 
        nghbr , costs = neighborHoodQ.unspool()
        if SHOWDEBUG:
            print "COMPLETE! There are " , len( nghbr ) , "neighbors"
        #  2. Find the node with the least cost to root
        leastCost =  infty
        i_least   = -infty
        leastDist =  infty
        leastNode =  None
        if SHOWDEBUG:
            print "Searching neighbors for cheapest ..." ,
        for i in xrange( len( nghbr ) ):
            distBtn = TreeNode.dist_between( nghbr[i] , node )
            totCost = distBtn + costs[i]
            if totCost < leastCost:
                leastCost = totCost
                i_least   = i
                leastDist = distBtn
                leastNode = nghbr[i]
        if SHOWDEBUG:
            print "Found!"
        #  3. If the distance to the cheapest node is more than the step size
        if leastDist > self.epsilon:
            locPln = self.local_planner( leastNode.state , node.state , incldBgn = False , incldEnd = True )
            #  4. If planning succeeds, then connect
            if len( locPln ) > 0:
                penultimateNode = self.T.add_state_seq( locPln[:-1] , leastNode )
                self.T.add_node( node , penultimateNode )
            #  A. Else plan fails, don't add node
        else:
            self.T.add_node( node , leastNode )
        # ~ Rewire Phase ~
        if SHOWDEBUG:
            print "About to rewire ..." ,
        #  4. For all other nodes in the neighborhood
        for i in xrange( len( nghbr )-1 , 0 , -1 ): # Start from highest-to-root and work towards lowest
            if i != i_least:
                leastCost =  infty
                j_least   = -infty
                leastDist =  infty
                leastNode =  None
                n_i       =  nghbr[i]
                rewire    =  False
                for j in xrange( i-1 , -1 , -1 ):
                    if j != i:
                        distBtn = TreeNode.dist_between( nghbr[j] , n_i )
                        totCost = distBtn + costs[j]
                        if totCost + ( totCost * MARGIN ) < costs[i]:
                            rewire = True
                            if totCost < leastCost:
                                leastCost = totCost
#                                j_least   = j
                                leastDist = distBtn
                                leastNode = nghbr[j]
                if rewire:
                    prevParent = n_i.parent
                    if leastDist > self.epsilon:
                        locPln = self.local_planner( leastNode.state , n_i.state , incldBgn = False , incldEnd = True )
                        #  4. If planning succeeds, then connect
                        if len( locPln ) > 0:
                            penultimateNode = self.T.add_state_seq( locPln[:-1] , leastNode )
    #                        self.T.add_node( node , penultimateNode )
                            n_i.parent = penultimateNode
                            penultimateNode.add_child( n_i )
                            self.T.edges.append( ( penultimateNode.state , n_i.state ) )
                            # A. Update cost for this node , Important!
                            n_i.cost_2_parent = TreeNode.dist_between( n_i , penultimateNode )
                            n_i.cost_2_root   = n_i.cost_2_parent + penultimateNode.cost_2_root
                        #  B. Else plan fails, don't add node
                    else:
#                        self.T.add_node( node , leastNode )
                        n_i.parent = leastNode
                        leastNode.add_child( n_i )
                        self.T.edges.append( ( leastNode.state , n_i.state ) )
                        # A. Update cost for this node , Important!
                        n_i.cost_2_parent = TreeNode.dist_between( n_i , leastNode )
                        n_i.cost_2_root   = n_i.cost_2_parent + leastNode.cost_2_root
                        
                    prevParent.remove_child_by_ref( n_i )
                    self.T.remove_edge( prevParent.state , n_i.state )
        if SHOWDEBUG:
            print "COMPLETE"
    
    def add_node_star_OLD( self , node , radius , NshortCut = None ):
        """ Add node in the RRT* fashion """
        
        SHOWDEBUG = True
        
        #  1. Get the neighborhood around the new node
        neighborHoodQ = self.T.get_neighborhood( node.state , radius , NshortCut )
        #  2. Unspool queue
        nghbr , costs = neighborHoodQ.unspool()
        if SHOWDEBUG:
            if len( nghbr ) > 3:
                sep( "Least Neighbor" )
                print nghbr[0].state , "," , costs[0]
                sep( "Most Neighbor" )
                print nghbr[-1].state , "," , costs[-1]
                exit()
        # ~ Add Phase ~
        #  3. While there are nodes
        for i in xrange( len( nghbr ) ):
            #  4. Get the next-cheapest node
            near = nghbr[i]
#            cost = costs[i]
            #  5. If the distance to q_cheap > step, then check local planner
            if TreeNode.dist_between( near , node ) > self.epsilon:
                #  6. If succeed, Connect to q_cheap  &&  break
                locPln = self.local_planner( near.state , node.state , incldBgn = False , incldEnd = True )
                if len( locPln ) > 0:
                    lastNode = self.T.add_state_seq( locPln[:-1] , near )
                    self.T.add_node( node , lastNode )
                    break
                else:
                    continue
            else:
                #  7. Else the distance <= step, Connect automatically
                self.T.add_node( node , near )
                break
        # ~ Rewire Phase ~
        #  8. For each node in neighborhood
#        for i in xrange( 0 ,len( nghbr )-1 ):
        for i in xrange( 0 , len( nghbr ) ):
#        for i in xrange( len( nghbr )-1 , -1 , -1 ):
#            for j in xrange( i+1 ,len( nghbr ) ):
            
            leastCost    = 1e7
            least_j      = 1000
            foundShorter = False
            
            for j in xrange( 0 ,len( nghbr ) ):
                # Attempt Rewire if Advantageous: i --to--> j
                if i != j:
                    node_i = nghbr[i];  node_j = nghbr[j];  
                    cost_i = costs[i];  
                    if node_i.parent: # If this is any node other than the root node, we can possibly rewire
                        #  9. Check if a neighbor has a cost-to-root closer than parent
                        ij_dist = TreeNode.dist_between( node_i , node_j )
                        if cost_i > ij_dist + node_j.cost_2_root:
                            foundShorter = True
                            if ij_dist + node_j.cost_2_root < leastCost:
                                leastCost = ij_dist + node_j.cost_2_root
                                least_j   = j
            if foundShorter:
                prevParent = node_i.parent
                node_j = nghbr[ least_j ];                  
                if ij_dist > self.epsilon:
                    # 10. If so, Check local planner  
                    locPln = self.local_planner( node_j.state , node_i.state , incldBgn = False , incldEnd = True )
                    # 11. If succeed, rewire
                    if len( locPln ) > 0:
                        lastNode = self.T.add_state_seq( locPln[:-1] , near )
    #                                    self.T.add_node( node_i , lastNode )
                        node_i.parent = lastNode
                        lastNode.add_child( node_i )
                else:
                    node_i.parent = node_j
                    node_j.add_child( node_i )
                ip_dist = TreeNode.dist_between( node_i , node_i.parent )
                node_i.cost_2_parent = ip_dist 
                node_i.cost_2_root   = ip_dist + node_i.parent.cost_2_root
                prevParent.remove_child_by_ref( node_i )
                self.T.remove_edge( prevParent.state , node_i.state )
                            
    
    # ___ End STAR _________________________________________________________________________________________________________________________

    def build_rrt_connect( self , init , goal ):
        ''' Build the rrt connect from init to goal , Returns path to goal or None '''
        # NOTE: This is the RRT-Connect algorithm
        
        self.goal = np.array( goal ) # store the config of the goal state
        self.init = np.array( init ) # store the config of the init state
        self.found_path = False # ---- Flag for reached goal

        # Set up the tree structure
        self.T = RRTSearchTree( init )
        
        result = None
        samples = 0

        while isinstance( result , ( NoneType , bool ) ) and samples < self.K: # Iterate until either the goal or the iteration limit is reached
            # 1. Sample either a random point or the goal with P(bias)
            target = self.sample()     
            
            # 2. Perform extension and collect result
            result = self.extend_connect( target )
            
            samples += 1
            if _DEBUG:
                print "Iteration" , samples , "with result" , result
            
        if not isinstance( result , ( NoneType , bool ) ):
            self.found_path = True
            return result
        else:
            return None
        return None

    def sample( self ):
        ''' Sample a new configuration and return '''
        # 1. Decide if the target is random or goal 
        if random() <= self.connect_prob: # Roll the dice and with P(bias) choose the goal as the target
            return self.goal
        else: # else, choose a random point in the config space
            temp = vec_random_limits( len( self.limits ) , self.limits )
            if _DEBUG:
                print "Random Sample:" , temp
            return temp

    def extend( self , target ):
        ''' Perform rrt extend operation.  q - new configuration to extend towards 
            ~~~~~~~~~~~~
            Return Codes:
            list - Goal found , return path back from start to goal
            True - A new node was added that extends towards the target from the nearest node
            None - Trapped , extension was not successful '''
        # NOTE: This function is the guts of the baseline RRT algorithm
        
        # 2. Find the closest node to the target
        q_near , dist = self.T.find_nearest( target ) # return ( NODE_REFERENCE , DISTANCE_TO_NODE )
        # 3. Gen an epsilon step towards the target
        q_new = np.add( np.multiply( vec_unit( np.subtract( target , q_near.state ) ) , self.epsilon ) , q_near.state )
        if _DEBUG:
            print "q_new:" , q_new , endl
        # 4. Check the viability of the step
        if not self.in_collision( q_new ):
            self.T.add_node( TreeNode( q_new ) , q_near )
            # 5. Check if we have reached the goal
            if vec_mag( np.subtract( self.goal , q_new ) ) <= self.epsilon:
                # 4.a. If step is goal, gen path from parents and return it
                temp = TreeNode( self.goal )
                self.T.add_node( temp , q_near )
                return self.T.get_back_path( temp ) # Reached
            else:
                return True # Advanced
        
        return None # Trapped
        
    def extend_connect( self , target ):
        ''' Perform rrt extend operation.  q - new configuration to extend towards 
            March towards the target for as long as the new step is neither in collision nor at the goal '''
        # NOTE: This is the guts of the RRT-Connect algorithm
            
        # 2. Find the closest node to the target
        q_near , dist = self.T.find_nearest( target ) # return ( NODE_REFERENCE , DISTANCE_TO_NODE )
        # 3. Gen an epsilon step towards the target
        q_new = np.add( np.multiply( vec_unit( np.subtract( target , q_near.state ) ) , self.epsilon ) , q_near.state )
        if _DEBUG:
            print "q_new:" , q_new , endl
        # 4. Check the viability of the step
        last = q_near
        if not self.in_collision( q_new ):
            # 5. As long as we have neither collided nor reached the target , Keep marching by 'epsilon' towards the target
            while not self.in_collision( q_new ) and vec_mag( np.subtract( target , q_new ) ) > self.epsilon:
                if _DEBUG:
                    print "Marching , q_new:" , q_new
                # 6. Add the validated node
                temp = TreeNode( q_new )
                self.T.add_node( temp , last )
                # 7. Check if we have reached the goal
                if vec_mag( np.subtract( self.goal , q_new ) ) <= self.epsilon:
                    # 7.a. If step is goal , make up the last little bit , and gen path from parents and return it
                    temp = TreeNode( self.goal )
                    self.T.add_node( temp , q_near )
                    return self.T.get_back_path( temp ) # Reached
                # 8. Else path not reached, march a step so that the new step can be evaluated at the top of the loop
                q_new = np.add( np.multiply( vec_unit( np.subtract( target , q_new ) ) , self.epsilon ) , q_new )
                last = temp
            return True # Advanced one step or many
        
        return None # Trapped, first step was in collision
    
    ## === 2004 _ Augmenting RRT w Local Trees _ Strandberg ================================================================================
    
    def get_root_node( self ):
        """ Return the root node """
        return self.T.root
    
    def get_closest( self , q ):
        """ Return the closest node to q """
        return self.T.find_nearest( q )[0]
    
    def is_node_in_tree( self , nodeRef ):
        """ Return true if 'nodeRef' is in the list of nodes """
        return nodeRef in self.T.nodes
    
    def is_q_in_tree( self , q ):
        """ Return True """
        for node in self.T.nodes:
            if vec_eq( node , q ):
                return True
        return False
    
    def sample_unbiased( self ):
        ''' Sample a new configuration and return '''
        
        SHOWDEBUG = True
        
        if SHOWDEBUG:
            sep( "Unbiased Free Sample" , 2 )
            print "Dimensions:" , len( self.limits )
            print "Limits: __ " , self.limits
        
        temp = vec_random_limits( len( self.limits ) , self.limits )
        
        if SHOWDEBUG:
            print "Returning:_" , temp
            
        return temp
    
    # === RRT* ===
    
    def local_planner( self , q_bgn , q_end , incldBgn = False , incldEnd = True ):
        """ If every point that is step size apart between 'q_bgn' and 'q_end' is free , then return them , Otherwise return [] """
        if self.in_collision( q_bgn ):
            # 4. If collide, then return []
            return []
        if vec_eq( q_bgn , q_end ):
            return [ q_bgn ]
        # 1. Build the list of q to check
        if incldBgn:
            rtnLst = [ q_bgn ]
        else:
            rtnLst = [ ]
        # 2. For every q
        q_new  = q_bgn
        dir_q  = vec_unit( np.subtract( q_end , q_bgn ) )
        deltaQ = np.multiply( dir_q , self.epsilon )
        q_new = np.add( q_new , deltaQ )
        while vec_dif_mag( q_new , q_end ) >= self.epsilon:
            # 3. Check q for collision
            if self.in_collision( q_new ):
                # 4. If collide, then return []
                return []
            else:
                # 5. else is not in collision, add to list
                rtnLst.append( q_new )
            q_new = np.add( q_new , deltaQ )
        if ( vec_dif_mag( q_new , q_end ) < self.epsilon ) and ( incldEnd ):
            # 3. Check q for collision
            if self.in_collision( q_end ):
                # 4. If collide, then return []
                return []
            else:
                # 5. else is not in collision, add to list
                rtnLst.append( q_end )
        # 5. Return q list
        return rtnLst
        
    

    def step_towards_star( self , q_trgt , radius , NshortCut = None ):
        """ Add a step towards 'q_trgt' , Return a reference to the new node, Return None if the step is in collision """
        q_NN = ((self.T.find_nearest( q_trgt ))[0]).state
        stepDir = vec_unit( np.subtract( q_trgt , q_NN ) )
        q_step = np.add( q_NN , 
                         np.multiply( stepDir , self.epsilon ) )
        if not self.in_collision( q_step ):
            temp = TreeNode( q_step )
            self.add_node_star( temp , radius , NshortCut )
    
    # ___ End* ___
    
    def Connect( self , target ):
        ''' Perform rrt extend operation.  q - new configuration to extend towards 
            March towards the target for as long as the new step is neither in collision nor at the goal 
            ~~~~~~
            Return Codes:
            True  , TreeNode - Reached the target   , Node that points to the target
            False , TreeNode - Did NOT reach target , Node that points to the last collision-free config
            False , None     - Did NOT reach target , There were no nodes in collision
            '''
        # NOTE: This is the Strandberg version of Connect
            
        SHOWDEBUG = True
        
        self.newNodeDelta = 0
            
        # 2. Find the closest node to the target
        q_near , dist = self.T.find_nearest( target ) # return ( NODE_REFERENCE , DISTANCE_TO_NODE )
        
        
        # 3. Gen an epsilon step towards the target
        if SHOWDEBUG:
            print "DEBUG:" , target , q_near.state
        q_new = np.add( np.multiply( vec_unit( np.subtract( target , q_near.state ) ) , self.epsilon ) , q_near.state )
        if SHOWDEBUG:
            print "q_new:" , q_new , endl
                
        if 0:
            # If we were asked to connect a target already in the tree, just return that node
            if vec_eq( target , q_near.state ):
                return True , q_near
        
        # 4. Check the viability of the step
        last = q_near
        if not self.in_collision( q_new ):
            
            if 1:
                temp = TreeNode( q_new )
                self.add_node_star( temp , self.epsilon * 5 , NshortCut = 10 )
                q_new = np.add( np.multiply( vec_unit( np.subtract( target , q_new ) ) , self.epsilon ) , q_new )
            
            # 5. As long as we have neither collided nor reached the target , Keep marching by 'epsilon' towards the target
            while not self.in_collision( q_new ) and vec_mag( np.subtract( target , q_new ) ) > self.epsilon:
                if SHOWDEBUG:
                    print "Marching , q_new:" , q_new , "Remaining:" , vec_mag( np.subtract( target , q_new ) )
                # 6. Add the validated node
                temp = TreeNode( q_new )
                self.T.add_node( temp , last )
                # 7. March a step so that the new step can be evaluated at the top of the loop
                q_new = np.add( np.multiply( vec_unit( np.subtract( target , q_new ) ) , self.epsilon ) , q_new )
                
                if SHOWDEBUG:
                    print "\tRemaining:" , vec_mag( np.subtract( target , q_new ) )
                
                last = temp
                self.newNodeDelta += 1
                
            if SHOWDEBUG:
                print "Done iterating, Check distance ..."
                
            # 8. Check if we have reached the goal
            if vec_mag( np.subtract( target , q_new ) ) <= self.epsilon:
                # 7.a. If step is goal , make up the last little bit , and gen path from parents and return it
                final = TreeNode( target )
                self.T.add_node( final , last )
                self.newestNode = self.T.newest
                return True , final
            # 9. Else there was a collision
            else:
                self.newestNode = self.T.newest
                return False , last # Advanced one step or many
        return False , q_near # Trapped, first step was in collision
        
    def count_nodes( self ):
        """ Return the number of nodes in this tree """
        return len( self.T.nodes )
    
    def Merge( self , node_self , T_other , node_othr ):
        """ Merge this tree with other via a connection from 'node_self' to 'node_othr' """
        # NOTE: This function does not delete 'T_other' as required by Strandberg , Discarding 'T_other' is the responsibility of client code
        # NOTE: This function assumes that both 'node_self' and 'node_othr' are properly connected 'TreeNode's
        # NOTE: This function assumes that 'node_self' and 'node_othr' are close enough in the problem to not need 'Connect'
        
        SHOWDEBUG = True
        
        visitSet = set([])
        
        if SHOWDEBUG:
            sep( "Pre-Merge Report ..." , 2 )
            
            print "This tree has " , self.count_nodes() , "nodes"
            print "Other tree has" , T_other.count_nodes() , "nodes"
            print "Does this tree have the arg node?:_" , self.is_node_in_tree( node_self )
            print "Does other tree have the arg node?:" , T_other.is_node_in_tree( node_othr )
            print "How close are the nodes?: ________ " , vec_mag( np.subtract( node_self.state , node_othr.state ) )
            print "Are they the same node?: _________ " , node_self == node_othr
            print "Are they the same tree?: _________ " , self == T_other
        
        first = True
        
        # 1. For each of the nodes on 'node_othr's branch, all the way up to the root
        # while node_othr.parent != None:
        while node_othr != None:
            
            # Save the current parent for the next iteration
            othrLastParent = node_othr.parent
            
            if othrLastParent and SHOWDEBUG:
                visitSet.add( tuple( node_othr.parent.state ) )
            
            # 2. Assign the nearest self node as the parent of the nearest other node
            node_othr.parent = node_self
            # 3. Assign the nearest other node as the successor of the nearest self node
            node_self.add_child( node_othr )
            
            self.T.edges.append( ( node_self.state , node_othr.state ) )
            
            # 4. Remove the merged node from the list of children of the nearest other node
            if not first:
                node_othr.remove_child_by_ref( node_self )
            first = False
            # 5. Move up the branch by 1 for the next iteration
            node_self = node_othr
            node_othr = othrLastParent
            
            if SHOWDEBUG and node_othr:
                if 0:
                    print '.' ,
                elif 1:
                    print "Set size:" , len( visitSet ) , 
                else:
                    print "Other node is  " , node_othr.__class__ ,  #type( node_othr ) ,
                    print "Other parent is" , node_othr.parent.__class__  , 
            
        # 6. Add the merged nodes to the merginf tree
        self.T.nodes.extend( T_other.T.nodes )
        # self.T.edges.extend( T_other.T.edges )
        
        if SHOWDEBUG:
            print
        
    def TryMerge( self , T_other , node_self ):
        """ Try to merge this tree with 'T_other' via 'node_self' on this tree """
        
        SHOWDEBUG = True
        
        if is_vector( node_self ): # If we got a config instead of a node, try to match to a node
            
            if SHOWDEBUG:
                print "Got a state , is the node in the tree?:" , self.is_q_in_tree( node_self )
            
            node_self = self.T.find_nearest( node_self )
            
        if isinstance( node_self , TreeNode ):
            
            if SHOWDEBUG:
                print "Is the node in the tree?:" , self.is_node_in_tree( node_self )
            
            reached , lastNode = T_other.Connect( node_self.state )
            
            if SHOWDEBUG:
                print "Did Connect succeed?:" , reached
                print "What was the last q?:" , lastNode.state
            
            if reached:
                self.Merge( node_self , T_other , lastNode )
                return True
            else:
                return False
        else:
            return False
            
    def TryMergeByNodes( self , node_self , T_other , node_othr ):
        """ Try to merge this tree with 'T_other' via 'node_self' on this tree and 'node_othr' and 'T_other' """
        # NOTE: This function assumes that 'node_self' and 'node_othr' exist in their respective trees and that neither are in collision
        
        SHOWDEBUG = True
        success   = False
        
        # 1. Local planner for the path between
        middlePlan = self.local_planner( node_self.state , node_othr.state , incldBgn = False , incldEnd = False )
        # 2. If the path is collision-free
        if len( middlePlan ) > 0:
			# 3. Add the nodes to this tree one at a time, until the middle tree is reached
			lastNode = node_self
			for i , q_i in enumerate( middlePlan ):
				temp = TreeNode( q_i )
				self.T.add_node( temp , lastNode )
				lastNode = temp
			# 4. Merge the other tree to the last node in the chain
			self.Merge( lastNode , T_other , node_othr )
			success = True
        # 5. Return success or failure
        return success
        
    ## ___ End Strandberg _________________________________________________________________________________________________________________
        
    def extend_connect_bidir( self , target ):
        ''' Perform rrt extend operation.  q - new configuration to extend towards '''
        # NOTE: This is the guts of the RRT-Bidirectional-Connect Algorithm
        
        RADIUS     = self.epsilon * 2
        SHORTCOUNT = 7
        
        # 1. Find the closest node to the target
        q_near , dist = self.T.find_nearest( target ) # return ( NODE_REFERENCE , DISTANCE_TO_NODE )
        
        # 2. Gen an epsilon step towards the target
        q_new = np.add( np.multiply( vec_unit( np.subtract( target , q_near.state ) ) , self.epsilon ) , q_near.state )
        if _DEBUG:
            print "q_new:" , q_new , endl
            
        # 3. Check the viability of the step
        last = q_near
        sampleReached = False # Did we connect all the way to target?
        temp = None
        # 4. If the sampled point was viable
        if not self.in_collision( q_new ):
            # 5. While the next march point is neither in collision nor at the goal
            while not self.in_collision( q_new ) and vec_mag( np.subtract( target , q_new ) ) > self.epsilon:
                if _DEBUG:
                    print "Marching , q_new:" , q_new
                temp = TreeNode( q_new )
                
                
#                self.T.add_node( temp , last )
                self.add_node_star( temp , RADIUS , SHORTCOUNT )
                
                
                # 6. Check if we have reached the goal # Our target is not the GOAL for bidir
                if vec_mag( np.subtract( target , q_new ) ) <= self.epsilon:
                    # 4.a. If step is goal, gen path from parents and return it
                    temp = TreeNode( target )
                    
                    
#                    self.T.add_node( temp , q_near )
                    self.add_node_star( temp , RADIUS , SHORTCOUNT )
                    
                    
                    # return self.T.get_back_path( temp ) , temp , True  # Reached
                    return True , temp , True  # Reached
                q_new = np.add( np.multiply( vec_unit( np.subtract( target , q_new ) ) , self.epsilon ) , q_new )
                last = temp
            if vec_mag( np.subtract( target , q_new ) ) <= self.epsilon:
                sampleReached = True # We connected all the way to target
                if _DEBUG:
                    print "REACHED!"
            return True , temp , sampleReached # Advanced
        
        return None , q_near , False # Trapped

    def fake_in_collision(self, q):
        ''' We never collide with this function! '''
        return False
    
    
    def set_goal( self , goal ):
        """ Set a new goal for the RRT """
        self.goal = np.array( goal ) # store the config of the goal state
        

## === 2004 _ Augmenting RRT w Local Trees _ Strandberg ====================================================================================
    
class RRTLocTrees( RRT ):
    """ RRT with Local Trees implementation """
    
    def __init__( self , 
                  num_samples , # ---------- Maximum allowed number of samples
                  num_dimensions = 2 , # --- Number of dimensions of this problem
                  step_length    = 1 , # --- Epsilon , length of march steps
                  lims           = None , #- Dimension limits
                  connect_prob   = 0.05 , #- Connection probability , NOT USED
                  collision_func = None , #- Collision checker 
                  N_loc_lim      = 20 , # -- Maximum allowed number of local trees
                  p_grow         = 0.12 ): # Probability of growing an existing tree
        """ Setup a collection of trees """
        
        RRT.__init__( self , num_samples , num_dimensions , step_length , lims , connect_prob , collision_func )
        
        self.N_loc    = N_loc_lim
        self.allTrees = []
        self.N_tree   = 0
        self.p_grow   = p_grow
        self.samples  = 0
        
    def get_all_nodes( self ):
        """ Get all the nodes that were generated while solving the problem """
        rtnNodes = []
        for T_i in self.allTrees:
            rtnNodes.extend( T_i.T.nodes )
        return rtnNodes
        
    def count_all_nodes( self ):
        """ Get all the nodes that were generated while solving the problem """
        count = 0
        for T_i in self.allTrees:
            count += len( T_i.T.nodes )
        return count
        
    def get_all_edges( self ):
        """ Get all the nodes that were generated while solving the problem """
        rtnEdges = []
        for T_i in self.allTrees:
            rtnEdges.extend( T_i.T.edges )
        return rtnEdges
        
    def get_states_and_edges( self ):
        """ Return all states and all edges """
        states = np.array( [ n.state for n in self.get_all_nodes() ] )
        return ( states , self.get_all_edges() )
        
    def RRT_from_self( self , q_init = None ):
        """ Create a vanilla RRT with the same specifications as the RRTLocTrees """
        rtnTree = RRT( self.K , # ---------- num_samples
                       self.n , # ---------- num_dimensions
                       self.epsilon , # ---- step_length
                       self.limits , # ----- lims
                       0.0 , # ------------- connect_prob == 0, connection managed by 'RRTLocTrees'
                       self.in_collision ) # collision_func
        if type( q_init ) != type( None ):
            rtnTree.init_tree( q_init )
        return rtnTree
        
    def init_trees( self , init , goal ):
        """ Create start and goal trees at indices 0 and 1, respectively """
        self.init = init 
        self.goal = goal
        self.allTrees = [] # Erase any old trees
        for q_init in [ goal , init ]:
            temp = self.RRT_from_self()
            temp.init_tree( q_init )
            self.allTrees.append( temp )
            
    def grow_roll( self ):
        """ Dice roll to grow """
        return flip_weighted( self.p_grow )
    
    def count_local( self ):
        """ Return the number of trees minus the Start and Goal trees """
        return len( self.allTrees ) - 2
    
    def get_order( self ):
        """ Return the indices indicating the proper order that trees should be iterated over """
        # NOTE: Always merge towards the goal tree
        order = [0]
        for i in xrange( self.count_local() ):
            order.append( i+2 )
        order.append( 1 )
        return order
    
    def grown_T_indices_in_merge_order( self ):
        """ Return the indices indicating the proper order that trees should be iterated over , Goal + Grown trees ONLY """
        # NOTE: Always merge towards the goal tree
        SHOWDEBUG = True
        order = [0]
        for i in xrange( self.count_local() + 1 ):
            if SHOWDEBUG:
                print self.allTrees[ i+1 ].__class__.__name__
                print self.allTrees[ i+1 ].T.__class__.__name__
            if self.allTrees[ i+1 ].T.grew():
                order.append( i+1 )
        return order
    
    def MergeTrees( self ):
        """ Merge all trees, if possible """
        # NOTE: Always merge towards the goal tree
        
        SHOWDEBUG = False
        
        order = self.get_order()        
        deleteList = []
        # For each tree T_i
        for m in xrange( 0 , len( self.allTrees ) - 1 ):
            i = order[m]
            T_i = self.allTrees[i]
            
            for n in xrange( m+1 , len( self.allTrees ) ):
                j = order[n]
                T_j = self.allTrees[j]
                
                if 0:
                    root_j = T_j.get_root_node()
                    close_i = T_i.get_closest( root_j.state )
                else:
                    close_i = choice( T_i.T.nodes )
                    
                if T_i.TryMerge( T_j , close_i ):
                    deleteList.append( T_j )
        for elem in deleteList:
            try:
                self.allTrees.remove( elem )
            except:
                if SHOWDEBUG:
                    print "Skipping delete..."
                    
    def SimpleMerge( self , init , goal ):
        """ Test RRTLocTrees components , always SHOWDEBUG """
        
        #SHOWDEBUG = True
        #ITERCOUNT = 0
        
        self.initState = init
        self.goalState = goal
        
        #  1. Start with 2 trees at the Start and the Goal
        self.init_trees( init , goal ) # Init trees
        self.T = self.allTrees[0] # So that we can plot it
        
        #  1.5. Init vars
        result       = None # ----------------------------------- Is either the path S-->G or None if node limit reached without success
        self.samples = 0 # -------------------------------------- Running count of all the nodes that have been added to the problem
        self.N_tree  = len( self.allTrees ) # ------------------- Count of all the trees in the problem
        
        print "How many trees are there total?:" , len( self.allTrees )
        print "How many local trees are there?:" , self.count_local()
        print "How many nodes are there?: _____" , self.count_all_nodes()
        
        willTheyOrWontThey = self.allTrees[0].TryMerge( self.allTrees[1] , self.allTrees[0].get_root_node() )
        
        print "Can I merge the start with the goal?:" , willTheyOrWontThey 
        
        startNode = self.allTrees[0].get_closest( self.initState )
        result    = self.allTrees[0].T.get_back_path( startNode ) # Reached
        
        print endl , endl , endl
        
        return result
        
        
    def RRTLocTreesF( self , init , goal ):
        """ 2004 _ Augmenting RRT-Planners with Local Trees _ Morten Strandberg """
        
        SHOWDEBUG = True
        ITERCOUNT = 0
        
        self.initState = init
        self.goalState = goal
        
        #  1. Start with 2 trees at the Start and the Goal
        self.init_trees( init , goal ) # Init trees
        self.T = self.allTrees[0] # So that we can plot it
        
        #  1.5. Init vars
        result       = None # ----------------------------------- Is either the path S-->G or None if node limit reached without success
        self.samples = 0 # -------------------------------------- Running count of all the nodes that have been added to the problem
        self.N_tree  = len( self.allTrees ) # ------------------- Count of all the trees in the problem
            
        def free_sample():
            """ Obtain a collision-free, unbiased sample """
            q_rand = self.allTrees[0].sample_unbiased()
            count = 1
            while self.allTrees[0].in_collision( q_rand ):
                q_rand = self.allTrees[0].sample_unbiased()
                count += 1
            if SHOWDEBUG:
                print "Obtained a collision-free sample" , q_rand , "after" , count , "attempts"
            return q_rand
                
        def goal_reached():
            """ Return True if the goal tree contains the start state , Otherwise return False """
            return ( self.allTrees[0].get_closest( self.initState ).state == self.initState ).all()
        
        #  2. Iterate until either the goal or the iteration limit is reached
        while isinstance( result , ( NoneType , bool ) ) and self.count_all_nodes() < self.K: 
            
            if SHOWDEBUG:
                ITERCOUNT += 1
                sep( "Iteration " + str( ITERCOUNT ) + " , There are " + str( self.count_all_nodes() ) + " samples" , 15 )
                print "There are" , self.count_local() , "local trees"
                print "Connection order:" , self.get_order()
            
            #  3. Get sample
            q_trgt = free_sample()
             
            noneReached  = True # ----------- True if none of the trees reached the target config
            deleteTrees  = [] # ------------- Trees to delete
            T_lastReachd = None # ----------- Last tree to reach the target config
            
            #  4. For each tree T_i
            for i in self.get_order():
                T_i          = self.allTrees[i] # Current tree
                
                
                #  5 &&  6. Try to connect the tree to the target config
                reached , q_new_i = T_i.Connect( q_trgt )
                
                #  7. If the tree reached the target config
                if reached:
                    noneReached = False # Set the flag
                    #  8. If there was another tree that got there first, merge
                    if T_lastReachd != None:
                        lastClosest = T_lastReachd.get_closest( q_trgt )
                        T_lastReachd.Merge( lastClosest , T_i , q_new_i )
                        #  9. Set this tree as the last one to reach the target config
                        T_lastReachd = T_i
                        deleteTrees.append( T_i )
                    else:
                        T_lastReachd = T_i
                        
            if SHOWDEBUG:
                print "There are" , len( deleteTrees ) , "trees to delete." , len( self.allTrees ) , "total trees"
                        
            for elem in deleteTrees:
                self.allTrees.remove( elem )
                
            if len( deleteTrees ):
                print "After deletion" , len( self.allTrees ) , "total trees"
            
            # 10. If none of the trees reached the target config
            if noneReached:
                # 11. Create a new tree
                # NOTE: N_loc is used only to limit the merge stage and not the actual number of trees
                temp = self.RRT_from_self()
                temp.init_tree( q_trgt )
                self.allTrees.append( temp )
                
            # 12. If the local tree "limit" has not been reached  -or-  We won the merge dice roll
            treeLimOK = self.count_local() <= self.N_loc
            mergeDice = self.grow_roll()
            
            if SHOWDEBUG:
                print "Decision to merge ..."
                print "\tBelow tree limit:" , treeLimOK
                print "\tMerge dice roll: " , mergeDice
                if treeLimOK or mergeDice:
                    print "\tYes , About to merge ..."
                else:
                    print "\tNo , Will not merge!"
            
#            if treeLimOK or mergeDice:
            if mergeDice:
                # 13. Then attempt to merge the trees
                self.MergeTrees()
                
                
            if SHOWDEBUG:
                print "Was the goal reached?:" , goal_reached()
                
            # 14. If the goal was reached Return the path to the goal
            if goal_reached():
                startNode = self.allTrees[0].get_closest( self.initState )
                result    = self.allTrees[0].T.get_back_path( startNode ) # Reached
                
        else:
            if SHOWDEBUG:
                nodeCount = self.count_all_nodes()
                if nodeCount > self.K:
                    print "Failed on node count:" , nodeCount , ">" , self.K
                
                
        return result
                
    def get_random_state( self ):
        """ Get a random state from the configuration space """
        return self.RRT_from_self().get_random_state()
            
    # *STAR* === RRTLocTrees* ======================================================================================================= *STAR*

    def free_sample( self ):
        """ Obtain a collision-free, unbiased sample """
        SHOWDEBUG = False
        q_rand = self.allTrees[0].sample_unbiased()
        count = 1
        while self.allTrees[0].in_collision( q_rand ):
            q_rand = self.allTrees[0].sample_unbiased()
            count += 1
        if SHOWDEBUG:
            print "Obtained a collision-free sample" , q_rand , "after" , count , "attempts"
        return q_rand
    
    def free_sample_biased( self , biasProb ):
        """ Obtain a collision-free, unbiased sample """
        # NOTE: Special care must be taken so that the goal tree does not seek out its own root
        SHOWDEBUG = False
        
        if flip_weighted( biasProb ):
            q_rand = self.goalState
        else:
            q_rand = self.allTrees[0].sample_unbiased()
            count = 1
            while self.allTrees[0].in_collision( q_rand ):
                q_rand = self.allTrees[0].sample_unbiased()
                count += 1
        if SHOWDEBUG:
            print "Obtained a collision-free sample" , q_rand , "after" , count , "attempts"
        return q_rand
    
    def goal_reached( self ):
        """ Return True if the goal tree contains the start state , Otherwise return False """
        return ( self.allTrees[0].get_closest( self.initState ).state == self.initState ).all()

    def MergeGrownTrees( self , radius , NshortCut = None ):
        # Attempt to merge all the trees that have grown since the last merge attempt
        SHOWDEBUG = True
#        deleteSet = set([]) # Set of merged trees that should be deleted
        deleteList = [] # List of merged trees that should be deleted
        # 1. Build a list of trees in merge order , goal + grown
        order = self.grown_T_indices_in_merge_order() 
        if SHOWDEBUG:
            print "Merge Order:" , order
        # 2. For each unique pair of trees
        for m in xrange( 0 , len( order ) - 1 ):
            i = order[m]
            T_i = self.allTrees[i]            
            for n in xrange( m+1 , len( order ) ):
                j = order[n]
                T_j = self.allTrees[j]
                # 3. Run the local planner between the newest node in each tree
                q_i = T_i.T.newest.state
                q_j = T_j.T.newest.state
                pathBtn = T_i.local_planner( q_i , q_j , incldBgn = False , incldEnd = True )
                # 4. If Success
                if len( pathBtn ) > 0:
                    # 5. Star-Connect by the newest nodes
                    n_i = T_i.step_towards_star( q_j , radius , NshortCut )
                    n_j = T_j.step_towards_star( q_i , radius , NshortCut )
                    if (  ( n_i != None )  and  ( n_i != None )  ):
                        # 6. Mark the merged tree T_j for deletion
                        if T_i.TryMergeByNodes( n_i , T_j , n_j ):
                            deleteList.append( T_j )
        # Delete merged trees
        for elem in deleteList:
            try:
                self.allTrees.remove( elem )
            except:
                if SHOWDEBUG:
                    print "Skipping delete..."

    def RRTLocTrees_STAR( self , init , goal ):
        # Star version of Strandberg 2004, Draft 0
        
        SHOWDEBUG = True # ------------ Flag to print debug info
        ITERCOUNT =  0 # -------------- Number of iterations that have been executed
        STARSHORT =  5 # -------------- Maximum number of nodes for local neighborhood
        BIASSAMPL =  0.15 # ----------- Bias towards picking the goal state as the target config
        SEARCHRAD = self.epsilon * 2 # Neighborhood to rewire
        ITERLIMIT = 6
        
        self.initState = init # Start config
        self.goalState = goal # Goal  config
        result         = None # Return the path from the start to the goal, or None if the node limit was reached
        
        #  1. Start with 2 trees at the Start and the Goal
        self.init_trees( init , goal ) # Init trees
        self.T = self.allTrees[0] # ---- So that we can plot it?
        
        #  2. Iterate until either the goal or the iteration limit is reached
        while isinstance( result , ( NoneType , bool ) ) and self.count_all_nodes() < self.K: 
            
            if SHOWDEBUG:
#                if ITERCOUNT > ITERLIMIT:
#                    break
                ITERCOUNT += 1
                sep( "Iteration " + str( ITERCOUNT ) + " , There are " + str( self.count_all_nodes() ) + " samples" , 15 )
                print "There are" , self.count_local() , "local trees"
                print "Connection order:" , self.get_order()
            
            #  3. Get sample
#            q_trgt = self.free_sample()
            q_trgt = self.free_sample_biased( BIASSAMPL )
            
            #  A. Iteration Init
            noneReached  = True # ----------- True if none of the trees reached the target config
            deleteTrees  = [] # ------------- Trees to delete
            T_lastReachd = None # ----------- Last tree to reach the target config
            
            #  4. For each tree T_i
            iterOrder = self.get_order()
            for i in iterOrder:
                
                if SHOWDEBUG:
                    print "Tree" , i , "of" , iterOrder
                
                if not ( ( i == 0 ) and vec_eq( q_trgt , self.goalState ) ):
                    T_i = self.allTrees[i] # Current tree
                    #  5. Obtain a step in the direction of the target sample
                    q_NN = ((T_i.T.find_nearest( q_trgt ))[0]).state
                    stepDir = vec_unit( np.subtract( q_trgt , q_NN ) )
                    q_step = np.add( q_NN , 
                                     np.multiply( stepDir , self.epsilon ) )
                    #  6. If the step is not in collision
                    if not T_i.in_collision( q_step ):
                        #  7. Add node (STAR*)
                        temp = TreeNode( q_step )
                        if SHOWDEBUG:
                            print "About to add node" , 
                        T_i.add_node_star( temp , SEARCHRAD , STARSHORT )
                        if SHOWDEBUG:
                            print "ADDED! About to determine reachability ..." ,
                        #  8. Determine if there is a clear path to the target sample.  &&  If so, set flag
                        if len( T_i.local_planner( q_step , q_trgt , incldBgn = False , incldEnd = False ) ) > 0:
                            noneReached = False
                        if SHOWDEBUG:
                            print "Complete! Is this point UNreachable?" , noneReached
            if SHOWDEBUG:
                print "Iteration complete!"
            #  9. If none of the trees made it to the target config
            if noneReached:
                if SHOWDEBUG:
                    print "New tree @" , q_trgt
#            if 0:
                # 10. Create a new tree with the target config as root
                tempT = self.RRT_from_self( q_trgt )
                # tempT.init_tree( q_trgt )
                self.allTrees.append( tempT )
                
            # 11. If roll for grow wins
            if( self.grow_roll() ):
#            if 1:
                # 12. Merge Trees
                self.MergeGrownTrees( SEARCHRAD , STARSHORT )
            # 13. If path found, Then return path to goal
            if SHOWDEBUG:
                print "About to perform goal check ..." ,
            if self.goal_reached():
                startNode = self.allTrees[0].get_closest( self.initState )
                result    = self.allTrees[0].T.get_back_path( startNode ) # Reached
            if SHOWDEBUG:
                print "COMPLETE" , result
        # 14. Return path or None
        return result
    
    # *STAR* ___ RRTLocTrees* _______________________________________________________________________________________________________ *STAR*    
        
                
            
## ___ End Strandberg _________________________________________________________________________________________________________________
        

def test_rrt_env( figPath , num_samples = 500 , step_length = 2 , env = 'env0.txt' , connect = False , connect_prob = 0.05 , STAR = False ):
    ''' create an instance of PolygonEnvironment from a description file and plan a path from start to goal on it using an RRT

    num_samples - number of samples to generate in RRT
    step_length - step size for growing in rrt (epsilon)
    env - path to the environment file to read
    connect - If True run rrt_connect

    returns plan, planner - plan is the set of configurations from start to goal, planner is the rrt used for building the plan '''
    
    print "STARTED"    
    
    pe = PolygonEnvironment()
    pe.read_env(env)

    dims = len(pe.start)
    start_time = time.time()

    rrt = RRT(num_samples,
              dims,
              step_length,
              pe.lims,
              connect_prob,
              collision_func=pe.test_collisions)
    
    if _RANDSTAT:
        pe.start = rrt.get_random_state()
        pe.goal  = rrt.get_random_state()
              
    if connect:
        plan = rrt.build_rrt_connect(pe.start, pe.goal)
    elif STAR:
        plan = rrt.build_rrt_star( pe.start , pe.goal )
    else:
        plan = rrt.build_rrt(pe.start, pe.goal)
    run_time = time.time() - start_time
    
    accum_print( "Init State:" , rrt.init )
    accum_print( "Goal State:" , rrt.goal )
    closest , dist = rrt.T.find_nearest( rrt.goal )
    accum_print( "Closest Node:" , closest , "at distance" , dist )
    accum_print( 'plan:', plan )
    if plan:
        accum_print( 'plan length:' , len( plan ) )
    accum_print( 'run_time =', run_time )
    accum_print( "Number of Nodes:" , len( rrt.T.nodes ) )
    if _GRAPHGEN:
        plotter.close( 'all' )
        pe.draw_plan( plan , rrt , figPath , dynamic_plan=False )
    accum_print( "END" )
    if _DEBUG:
        for node in rrt.T.nodes:
            print node

    return plan, rrt

def test_rrt_bidir( figPath , num_samples = 500 , step_length = 2 , env = 'env0.txt' , connect = False , connect_prob = 0.05 ):
    """ Search for a path with Bi-directional RRT Connect """
    
    print "STARTED"    
    
    # 0. Init environment variables
    pe = PolygonEnvironment()
    pe.read_env(env)
    dims = len(pe.start)
    start_time = time.time()
    
    # 1. Init RRT problems for both the start and goal states
    
    rrt_s = RRT( num_samples/2, # Tree growing from the initial state
                 dims,
                 step_length,
                 pe.lims,
                 connect_prob,
                 collision_func = pe.test_collisions )
                 
    rrt_g = RRT( num_samples/2, # Tree growing from the goal state
                 dims,
                 step_length,
                 pe.lims,
                 connect_prob,
                 collision_func = pe.test_collisions )
                 
    if _RANDSTAT:
        pe.start = rrt_s.get_random_state()
        pe.goal  = rrt_g.get_random_state()
    
    rrt_s.init_tree( pe.start )
    rrt_g.init_tree( pe.goal  )
        
    pathFound = False # Signal for when the two trees have become connected
    
    # Store references to the two trees in a list.  
    # We will be switching between them each time a tree attempts an extension without reaching the goal
    flipFlop = [ rrt_s , rrt_g ]
    
    index = 0
    samples = 0
    connecting = False
    reachBool = False
    
    lastAdded = rrt_g.T.nodes[0]
    if _DEBUG:
        print "Only node at goal" , rrt_g.T.nodes[0] , len( rrt_g.T.nodes )
    plan = None    


    while not pathFound and samples < num_samples:
        # Toggle the growing and goal trees
        flip = elemw( index   , flipFlop ) # Growing tree
        flop = elemw( index+1 , flipFlop ) # Goal tree
        
        if _DEBUG:
            print "Flip" , id( flip ) , "Flop" , id( flop )

        if random() < connect_prob:
            connecting = True
            target = lastAdded.state
        else:
            target = vec_random_limits( len( flip.limits ) , flip.limits )
            connecting = False
            samples += 1
            
        result , lastAdded , reachBool = flip.extend_connect_bidir( target )
        
        if _DEBUG:
            print "Last Added:", lastAdded
        
        if result: # if the result is True or a list
            if connecting and reachBool: # The result is true
                # If we are in a connecting phase and the target was reached, 
                pathFound = True # then the trees are connected
                
                if _DEBUG:
                    print "Last Added:" , lastAdded , ',' , "Target:" , target
                
                # The plan will consist of 
                # A. the path from the growing tree root to the target
                growPath = flip.T.get_back_path( lastAdded )
                if _DEBUG:
                    print "Grow Path:" , growPath
                # B. The path from the last node added to the goal tree root
                goalPath = flop.T.get_back_path( flop.T.search_by_state( target ) )
                if _DEBUG:
                    print "Goal Path:" , goalPath
                
                # One of these needs to be reversed
                if id(flip) == id(rrt_s): # flip is the start tree , flop is the goal tree
                    plan = growPath
                    goalPath.reverse()
                    plan.extend( goalPath )
                else: # flop is the start tree , flip is the goal tree
                    plan = goalPath
                    growPath.reverse()
                    plan.extend( growPath )
                    
            samples += 1
        
        index += 1 # flip / flop
     
    run_time = time.time() - start_time
        
    accum_print( "Init State:" , rrt_s.init )
    accum_print( "Goal State:" , rrt_g.init )
    accum_print( 'plan:', plan )
    if plan:
        accum_print( 'plan length:' , len( plan ) )
    accum_print( 'run_time =', run_time )
    accum_print( "Number of Nodes:" , len( rrt_s.T.nodes ) + len( rrt_g.T.nodes ) )
    if _GRAPHGEN:
        plotter.close( 'all' )
        pe.draw_plan_bidir( plan , rrt_s , rrt_g , figPath , dynamic_plan=False )
    accum_print( "END" )
        
    return plan, rrt_s , rrt_g

## === 2004 _ Augmenting RRT w Local Trees _ Strandberg ====================================================================================

def test_rrt_locTrees( figPath , num_samples = 500 , step_length = 2 , env = 'env0.txt' , connect = False , connect_prob = 0.05 ,
                       N_loc_lim = 4 , p_grow = 0.12 , STAR = True ):
    """ Search for a path with RRTLocTrees """
    
    print "STARTED"    
    
    # 0. Init environment variables
    
    pe = PolygonEnvironment()
    pe.read_env( env )
    dims = len( pe.start )
    start_time = time.time()
    
    # 1. Init RRT problems for both the start and goal states
    
    rrt_lt = RRTLocTrees( num_samples , # Tree growing from the initial state
                          dims,
                          step_length,
                          pe.lims,
                          connect_prob,
                          collision_func = pe.test_collisions ,
                          N_loc_lim = N_loc_lim , 
                          p_grow    = p_grow )
    
    if _RANDSTAT:
        pe.start = rrt_lt.get_random_state()
        pe.goal  = rrt_lt.get_random_state()
        
    if 1:       
        if STAR:
            
            cProfile.run('re.compile("RRTLocTrees_STAR|pe.start , pe.goal")')
            
            plan = rrt_lt.RRTLocTrees_STAR( pe.start , pe.goal )
        else:
            plan = rrt_lt.RRTLocTreesF( pe.start , pe.goal )
    else:
        plan = rrt_lt.SimpleMerge(  pe.start , pe.goal )
     
    run_time = time.time() - start_time
        
    accum_print( "Init State:" , rrt_lt.init )
    accum_print( "Goal State:" , rrt_lt.goal )
    accum_print( 'plan:', plan )
    if plan:
        accum_print( 'plan length:' , len( plan ) )
    accum_print( 'run_time =', run_time )
    allNodes = rrt_lt.get_all_nodes()
    accum_print( "Number of Nodes:" , len( allNodes ) )
    if _GRAPHGEN:
        plotter.close( 'all' )
        pe.draw_plan( plan , rrt_lt , figPath , dynamic_plan = False )
    accum_print( "END" )
        
    return plan , rrt_lt

## ___ End Strandberg ______________________________________________________________________________________________________________________

def merge_test():
    
    sep( "MERGE TEST" )
    
    R1 = RRT( 500 )
    R2 = RRT( 500 )
    
    R1.init_tree( [ 0,0 ] )
    R2.init_tree( [ 1,0 ] )
    
    R1.T.add_q_to_closest_parent( [ 0.25 , 0 ] )
    R1.T.add_q_to_closest_parent( [ 0.44 , 0 ] )
    
    R2.T.add_q_to_closest_parent( [ 0.75 , 0 ] )
    R2.T.add_q_to_closest_parent( [ 0.66 , 0 ] )
    
    print "R1 has" , len( R1.T.nodes ) , "nodes"
    print "R2 has" , len( R2.T.nodes ) , "nodes"
    
    goal = R2.T.root
    middle = [ 0.5 , 0.0 ]
    
    R1near = R1.get_closest( middle )
    R2near = R2.get_closest( middle )
    
    print "R1 nearest middle" , R1near.state
    print "R2 nearest middle" , R2near.state
    
    print "Merging ..."
    R1.Merge( R1near , R2 , R2near )
    print "R1 has" , len( R1.T.nodes ) , "nodes"
    print "R2 has" , len( R2.T.nodes ) , "nodes"
    
    print "R1 has" , len( R1.T.edges ) , "edges"
    print "R2 has" , len( R2.T.edges ) , "edges"
    
    print "Path back:" , endl , R1.T.get_back_path( goal )

    print "R1 edges" , endl , 
    for edge in R1.T.edges:
        print edge

# === End RRT ===
    



_GRAPHGEN = True # Set to true to generate graphics
_RANDSTAT = False # Set to true for random start and end states
_DEBUG    = False
_SIMALL   = False

if __name__ == '__main__':
    
    # === NEW ASM Tests ====================================================================================================================
    
    # Testing on Environment 1
    
    if 1:
    
        if 0 or _SIMALL:
            accum_sep("Problem 1.1.a")
            test_rrt_env( "STARtest.eps" , num_samples = 2000 , step_length = 4.0 , env = 'env0.txt' , connect = False , connect_prob = 0.15 , 
                          STAR = True )
            accum_out_and_clear( "Problem_1-1-a_Output.txt" )
        
        # Works SLOWLY
        if 0 or _SIMALL: 
            accum_sep( "Bidir Connect" )
            test_rrt_bidir( "ASM_Bidir_Connect.eps" , num_samples = 1000 , step_length = 0.15 , env = 'env3.txt' , connect = False , connect_prob = 0.05 )
            accum_out_and_clear( "ASM_Bidir_Connect_Output.txt" )
        
        # Works MOST TIMES , Up to 4X speedup vs Bidir-star
        if 1 or _SIMALL: 
            accum_sep( "RRTLocTrees" )
            test_rrt_locTrees( "ASM_LocTrees.eps" , num_samples = 2000 , step_length = 0.15 , env = 'env3.txt' , connect = False , connect_prob = 0.05 ,
                               N_loc_lim = 4 , p_grow = 0.75 , STAR = False )
            accum_out_and_clear( "ASM_LocTrees.txt" )
            
    else:
        merge_test() # Merge works, that's not the problem
    
    # ___ END ASM Tests ____________________________________________________________________________________________________________________
    
    
    # === OLD Homework Tests ===============================================================================================================
    
    _GRAPHGEN = False # Set to true to generate graphics
    _RANDSTAT = False # Set to true for random start and end states
    _DEBUG    = False
    _SIMALL   = False
    
    _RANDSTAT = False # Use the text file start and end states    
    
    if False or _SIMALL:
        accum_sep("Problem 1.1.a")
        test_rrt_env( "p_1-1-a.eps" , num_samples = 500 , step_length = 2 , env = 'env0.txt' , connect = False , connect_prob = 0.05 )
        accum_out_and_clear( "Problem_1-1-a_Output.txt" )
        
    if False or _SIMALL:
        accum_sep("Problem 1.1.b")
        test_rrt_env( "p_1-1-b.eps" , num_samples = 6000 , step_length = 0.15 , env = 'env1.txt' , connect = False , connect_prob = 0.05 )
        accum_out_and_clear( "Problem_1-1-b_Output.txt" )
      
    _RANDSTAT = True # Use random start and end states      
      
    if False or _SIMALL:
        for item in [ 'c' , 'd' , 'e' ]:
            accum_sep( "Problem 1.1." + item )
            test_rrt_env( "p_1-1-" + item + ".eps" , num_samples = 1000 , step_length = 2 , env = 'env0.txt' , connect = False , connect_prob = 0.05 )
            accum_out_and_clear( "Problem_1-1-" + item + "_Output.txt" )
            
    if False or _SIMALL:
        for item in [ 'f' , 'g' , 'h' ]:
            accum_sep( "Problem 1.1." + item )
            test_rrt_env( "p_1-1-" + item + ".eps" , num_samples = 12000 , step_length = 0.15 , env = 'env1.txt' , connect = False , connect_prob = 0.05 )
            accum_out_and_clear( "Problem_1-1-" + item + "_Output.txt" )
    
    _RANDSTAT = False # Use the text file start and end states    
    
    if False or _SIMALL:
        biases = [ 0.0 , 0.05 , 0.1 , 0.5 ]
        parts  = [ 'a' ,  'b' , 'c' , 'd' ]
        for i in range( len( parts ) ):
            accum_sep( "Problem 1.2." + parts[i] )
            test_rrt_env( "p_1-2-" + parts[i] + ".eps" , 
                          num_samples = 2000 , step_length = 2 , env = 'env0.txt' , connect = False , connect_prob = biases[i] )
            accum_out_and_clear( "Problem_1-2-" + parts[i] + "_Output.txt" )
            
    if False or _SIMALL:
        accum_sep( "Problem 1.3.a" )
        test_rrt_env( "p_1-3-a.eps" , num_samples = 500 , step_length = 2 , env = 'env0.txt' , connect = True , connect_prob = 0.05 )
        accum_out_and_clear( "Problem_1-3-a_Output.txt" )
        
    if False or _SIMALL:
        accum_sep( "Problem 1.3.b" )
        test_rrt_env( "p_1-3-b.eps" , num_samples = 5000 , step_length = 0.15 , env = 'env1.txt' , connect = True , connect_prob = 0.05 )
        accum_out_and_clear( "Problem_1-3-b_Output.txt" )
        
    if False or _SIMALL: # NOTE: Run this until it returns a plan
        accum_sep( "Problem 1.4.a" )
        test_rrt_bidir( "p_1-4-a.eps" , num_samples = 500 , step_length = 2 , env = 'env0.txt' , connect = False , connect_prob = 0.05 )
        accum_out_and_clear( "Problem_1-4-a_Output.txt" )
        
    if False or _SIMALL: # NOTE: Run this until it returns a plan
        accum_sep( "Problem 1.4.b" )
        test_rrt_bidir( "p_1-4-b.eps" , num_samples = 500 , step_length = 0.15 , env = 'env1.txt' , connect = False , connect_prob = 0.05 )
        accum_out_and_clear( "Problem_1-4-b_Output.txt" )
        
    # ___ END Homework Tests _______________________________________________________________________________________________________________
