#!/usr/bin/env python
# -*- coding: utf-8 -*-
# Template Version: 2016-07-08

# ~~ Future First ~~
from __future__ import division # Future imports must be called before everything else, including triple-quote docs!

"""
UtahUtil.py
James Watson, 2016 August
Utility functions, structures, and classes
"""

# ~~ Imports ~~
# ~ Standard ~
import os , heapq
endl = os.linesep
from random import random
# ~ Special ~


# == Added for Project 2 ==

def vec_random( dim ): # <<< resenv
    """ Return a random vector in R-'dim' space with coordinates in [0,1) """
    rtnVec = []
    for i in range(dim):
        rtnVec.append( random() )
    return rtnVec

def vec_random_limits( dim , limits ): # >>> resenv
    """ Return a vector in which each element takes on a random value between 'limits[i][0]' and 'limits[i][1]' with a uniform distribution """
    rtnVec = []
    randVec = vec_random( dim )
    for i , elem in enumerate( randVec ):
        span = abs( limits[i][1] - limits[i][0] )
        rtnVec.append( elem * span + limits[i][0] )
    return rtnVec

class PriorityQueue(list): # Requires heapq
    """ Implements a priority queue data structure. """ 
    # NOTE: PriorityQueue does not allow you to change the priority of an item. You may insert the same item multiple times with different priorities. 
        
    def __init__( self , *args ):
        """ Normal 'list' init """
        list.__init__( self , *args )   
        self.count = 0
        self.s = set([])    
        
    def push( self , item , priority , hashable=None ):
        """ Push an item on the queue and automatically order by priority , optionally provide 'hashable' version of item for set testing """
        entry = ( priority , self.count , item )
        heapq.heappush( self , entry )
        self.count += 1
        if hashable:
            self.s.add( hashable ) 
    
    def contains( self , hashable ): 
        ''' Test if 'node' is in the queue '''
        return hashable in self.s

    def pop( self ):
        """ Pop the lowest-priority item from the queue """
        priority , count , item = heapq.heappop( self )
        return item
        
    def pop_with_priority( self ):
        """ Pop the item and the priority associated with it """
        priority , count , item = heapq.heappop( self )
        return item , priority

    def isEmpty(self):
        """ Return True if the queue has no items, otherwise return False """
        return len( self ) == 0
        
    # __len__ is provided by 'list'
        
    def unspool(self):
        """ Pop all items as two sorted lists, one of increasing priorities and the other of the corresponding items """
        vals = []
        itms = []
        while not self.isEmpty():
            item , valu = self.pop_with_priority()
            vals.append( valu )
            itms.append( item )
        return itms , vals 
 
def accum_print(*args):
    """ Print args and store them in a string """
    for arg in args:
        accum_print.totalStr += str(arg) + " "
        print str(arg),
    print
    accum_print.totalStr += endl
accum_print.totalStr = ""

def accum_sep(title = "", char = '=', width = 6, strOut = False):
    """ Print a separating title card for debug """
    LINE = width*char
    if strOut:
        return LINE + ' ' + title + ' ' + LINE
    else:
        accum_print( LINE + ' ' + title + ' ' + LINE )

def accum_out_and_clear( outPath ):
    """ Write the contents of 'accum_print' to a file and clear """
    outFile = file( outPath , 'w' )
    outFile.write( accum_print.totalStr )
    outFile.close()
    accum_print.totalStr = ""
    
def accum_clear():
    """ Clear the contents of 'accum_print' """
    accum_print.totalStr = ""
   
# == End Project 2 ==


# == Added for Project 1 ==

class Stack(list): # >>> resenv
    """ LIFO container based on 'list' """    
    
    def __init__( self , *args ):
        """ Normal 'list' init """
        list.__init__( self , *args )
        
    def push( self , elem ):
        """ Push 'elem' onto the Stack """
        self.append( elem )
        
    # 'Stack.pop' is inherited from 'list'
        
    def is_empty(self):
        """ Returns true if the Stack has no elements """
        return len(self) == 0
        
class Queue(list): # >>> resenv
    """ FIFO container based on 'list' """ 
    
    def __init__( self , *args ):
        """ Normal 'list' init """
        list.__init__( self , *args )

    def push( self , elem ):
        """ Push an item (prepend) to the back of the Queue """
        self.insert( 0 , elem )

    # 'Queue.pop' is inherited from 'list'

    def is_empty(self):
        """ Returns true if the Queue has no elements """
        return len(self) == 0
        
def process_txt_for_LaTeX( TXTpath ): # >>> resenv
    """ Add appropriate line breaks to a TXT file and return as TEX """
    if os.path.isfile( TXTpath ):
        drctry , txtName = os.path.split( TXTpath )
        ltxName = txtName[:-4] + ".tex"
        txtFile = file( TXTpath , 'r' )
        txtLines = txtFile.readlines()
        txtFile.close()
        ltxPath = os.path.join( drctry , ltxName )
        ltxFile = file( ltxPath , 'w' )
        for line in txtLines:
            # print "Line is:", line
            if len(line) > 3:
                ltxFile.write( line.rstrip() + " \\\\ " + endl ) 
            else:
                ltxFile.write( " $\\ $ \\\\ " + endl ) 
        ltxFile.close()
    else:
        raise IOError( "process_txt_for_LaTeX: " +str(TXTpath)+ " did not point to a file!" )

# == End Project 1 ==