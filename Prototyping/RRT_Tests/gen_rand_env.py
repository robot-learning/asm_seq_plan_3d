#!/usr/bin/env python
# -*- coding: utf-8 -*-

# ~~ Future First ~~
from __future__ import division # Future imports must be called before everything else, including triple-quote docs!

__progname__ = "gen_rand_env.py"
__version__  = "2018.06" 
"""
James Watson , Template Version: 2018-05-30
Built on Spyder for Python 2.7

A ONE LINE DESCRIPTION OF THE FILE

Dependencies: numpy
"""


"""  
~~~ Developmnent Plan ~~~
[ ] ITEM1
[ ] ITEM2
"""

# === Init Environment =====================================================================================================================
# ~~~ Prepare Paths ~~~
import sys, os.path
SOURCEDIR = os.path.dirname( os.path.abspath( __file__ ) ) # URL, dir containing source file: http://stackoverflow.com/a/7783326
PARENTDIR = os.path.dirname( SOURCEDIR )
# ~~ Path Utilities ~~
def prepend_dir_to_path( pathName ): sys.path.insert( 0 , pathName ) # Might need this to fetch a lib in a parent directory
def rel_to_abs_path( relativePath ): return os.path.join( SOURCEDIR , relativePath ) # Return an absolute path , given the 'relativePath'

# ~~~ Imports ~~~
# ~~ Standard ~~
from math import pi , sqrt , cos , sin
from random import choice , random
# ~~ Special ~~
import numpy as np
# ~~ Local ~~
from marchhare.marchhare import accum

# ~~ Constants , Shortcuts , Aliases ~~
EPSILON = 1e-7
infty   = 1e309 # URL: http://stackoverflow.com/questions/1628026/python-infinity-any-caveats#comment31860436_1628026
endl    = os.linesep

# ~~ Script Signature ~~
def __prog_signature__(): return __progname__ + " , Version " + __version__ # Return a string representing program name and verions

# ___ End Init _____________________________________________________________________________________________________________________________


# === Main Application =====================================================================================================================

# = Program Vars =

N      = 20 # Number of obstacles to generate
boxL   = 40 # Maximum extent in any dimension
gonMin =  3 # Minimum number of sides
gonMax = 10 # Maximum number of sides
outNam = "env3.txt"

# _ End Vars _


# = Program Functions =



# _ End Func _


# = Program Classes =



# _ End Classes _

if __name__ == "__main__":
    print __prog_signature__()
    termArgs = sys.argv[1:] # Terminal arguments , if they exist
    
#    count = 0
    
    xBounds = [ -100 ,  100 ]
    yBounds = [  -50 ,  150 ]
    
    accum.write( "Bounds:" , xBounds[0] , xBounds[1] , yBounds[0] , yBounds[1] , endl )
    accum.write( "RobotLinks: 20 20 20 20 15" , endl )
    accum.write( "RobotBase: 0 50" , endl )
    
    allX = range( xBounds[0] , xBounds[1] + 1 )
    allY = range( yBounds[0] , yBounds[1] + 1 )
    sidesPossible = range( gonMin , gonMax + 1 )
    
    for i in xrange( N ):
        # 1. Choose the center
        y = choice( allY )
        while abs( y - 50 ) < ( boxL / 2 + 1 ):
            # A. Choose until the center does not allow y = 50 overlap
            y = choice( allY )
        x = choice( allX )
        # 2. Choose number of sides
        goN = choice( sidesPossible )
        # 3. Get the dTheta
        dTheta = 2 * pi / goN
        accum.write( "Obstacle:" )
        # 4. For each dTheta (gon)
        for j in xrange( goN ):
            theta = j * dTheta
            # 5. Choose a distance from center
            dist = random() * ( boxL / 2.0 )
            # 6. Place the point
            accum.write( " " + str(  x + int( dist * cos( theta ) )  ) )
            accum.write( " " + str(  y + int( dist * sin( theta ) )  ) )
        accum.write( endl )

    accum.write( "Start: 0 0 0 0 0" , endl )        
    accum.write( "Goal: 3.1416 0 0 0 0" , endl )    

    accum.out_and_clear( outNam )    
    

# ___ End Main _____________________________________________________________________________________________________________________________


# === Spare Parts ==========================================================================================================================



# ___ End Spare ____________________________________________________________________________________________________________________________
