#!/usr/bin/env python
'''
Package providing helper classes and functions for performing graph search operations for planning.
'''
import numpy as np
import matplotlib.pyplot as plotter
#from math import pi
from collisions import PolygonEnvironment
import time , os

from random import choice
#from types import NoneType 
from HWutil import vec_eq , vec_unit , vec_mag
from UtahUtil import vec_random_limits , accum_print , accum_sep , accum_out_and_clear , PriorityQueue

_DEBUG = False

_TRAPPED  = 'trapped'
_ADVANCED = 'advanced'
_REACHED  = 'reached'

# ~~ Constants , Shortcuts , Aliases ~~
# URL, add global vars across modules: http://stackoverflow.com/a/15959638/893511
EPSILON = 1e-7
infty = 1e309 # URL: http://stackoverflow.com/questions/1628026/python-infinity-any-caveats#comment31860436_1628026
endl = os.linesep


# === PRM functions and classes ===

class GraphNode: 
    """ Search graph node for use with PRM """    
    
    def __init__( self , state , *pNeighbors ):
        self.state = state
        self.neighbors = []
        for nghbr in pNeighbors:
            self.connect_neighbor( nghbr )

    def connect_neighbor(self, successor):
        """ Mutually connect neighbors in a graph , if the connection does not already exist return True , otherwise return False """
        if self not in successor.neighbors:
            self.neighbors.append( successor )
            successor.neighbors.append( self )
            return True
        return False
        
    def connect_many( self , *many ):
        """ Connect many neighbors """
        success = []
        for neighbor in many:
            success.append( self.connect_neighbor( neighbor ) )
        return success
            
    def __str__( self ):
        return "GraphNode@" + str( self.state )
        
class PRMSearchGraph:
    
    def __init__( self ):
        self.nodes = []
        self.edges = []

    def find_nearest( self , s_query , N ):
        """ Find the 'N' nodes nearest to 's_query' """
        pQ = PriorityQueue() # Sort the nodes
        for node in self.nodes:
            if not vec_eq( s_query , node.state ):
                pQ.push( node , vec_mag( np.subtract( s_query , node.state ) ) )
        rtnList = []
        for i in xrange( N ):
            rtnList.append( pQ.pop() ) # This function assumes there is at least one
            if pQ.isEmpty(): break # Cannot get any more
        return rtnList
        
    def add_node_from_state( self , state , *pNeighbors  ):
        """ Create a node from 'state', add it to the collection of nodes , and return it """
        temp = GraphNode( state , *pNeighbors )
        self.nodes.append( temp )
        return temp
        
    def add_node( self , node ):
        """ Add node to the collection """
        self.nodes.append( node )
        
    def connect_nodes( self , n1 , n2 ):
        """ Add an edge between the nodes """ # NOTE: This function assumes the nodes already belong to the graph
        if n1.connect_neighbor( n2 ):
            if n1.state[0] >= n2.state[0]:
                self.edges.append( ( n1.state , n2.state ) )
            else:
                self.edges.append( ( n2.state , n1.state ) )
        
    def get_states_and_edges(self):
        states = np.array( [n.state for n in self.nodes] )
        return (states, self.edges)

    def search_by_state( self , q_query ): # ADDED
        """ Return the first TreeNode that matches state 'q_query', otherwise return None """
        for n in self.nodes:
            if vec_eq( n.state , q_query ):
                return n
        return None
                
class PRM:
    """ Probabilistic Road Map """
    
    def __init__( self , num_samples , num_dimensions = 2 , step_length = 1 , num_neighbors = 5 , lims = None , collision_func = None ):
        ''' Initialize a PRM planning instance '''
        self.K = num_samples # Number of samples to generate
        self.n = num_dimensions # dimensionality of the space
        self.epsilon = step_length # step size
        self.nearest_N = num_neighbors

        self.in_collision = collision_func # function for checking collisions
        if collision_func is None: # If there was no collision function assigned, use the dummy function so that things don't crash
            self.in_collision = self.fake_in_collision

        # Setup range limits for config space (joint limits, etc)
        self.limits = lims
        if self.limits is None: # If there were no limits passed as params
            self.limits = []
            for n in xrange(num_dimensions): # for each dimension
                self.limits.append([0,100]) # set default limits 0 - 100
            self.limits = np.array(self.limits) # Convert limits to an np.array

        self.ranges = self.limits[:,1] - self.limits[:,0] # 
        self.found_path = False
        
        self.T = PRMSearchGraph()
        
    def fake_in_collision(self, q):
        ''' We never collide with this function! '''
        return False

    def get_random_state( self ): # ADDED
        """ Return a random state within the limits of the world """
        return vec_random_limits( len( self.limits ) , self.limits )
        
    def sample( self , noCollide=True ):
        """ Sample a non-colliding point """
        temp = vec_random_limits( len( self.limits ) , self.limits )
        if noCollide:
            while self.in_collision( temp ):
                temp = vec_random_limits( len( self.limits ) , self.limits )
        return temp
        
    def loc_plan_epsilon( self , q1 , q2 ):
        """ Local Planner: Marches from 'q1' to 'q2' in 'self.epsilon' steps , return True if free , otherwise return False """
        unit = vec_unit( np.subtract( q2 , q1 ) )
        i = 1
        temp = np.add( q1 , np.multiply( unit , self.epsilon * i ) )
        while vec_mag( np.subtract( q2 , temp ) ) > self.epsilon:
            if self.in_collision( temp ):
                return False
            i += 1
            temp = np.add( q1 , np.multiply( unit , self.epsilon * i ) )
        return True
            
    def build_prm( self , init , goal ):
        ''' Build the rrt from init to goal , Returns path to goal or None '''
        self.goal = np.array(goal) # store the config of the goal state
        self.init = np.array(init) # store the config of the init state
        self.init_n = self.T.add_node_from_state( init )        
        self.goal_n = self.T.add_node_from_state( goal )        
        
        self.found_path = False # Flag for reached goal

        # Build graph and search

#        result = None
        samples = 0
        
        # Generate samples
        for i in xrange( self.K ):
            self.T.add_node_from_state( self.sample() )
            samples += 1
            
        # Connect neighbors
        for node in self.T.nodes:
            for neighbor in self.T.find_nearest( node.state , self.nearest_N ):
                if self.loc_plan_epsilon( node.state , neighbor.state ):
                    self.T.connect_nodes( node , neighbor )
    
    def node_distance( self , n1 , n2 ):
        """ Return the distance between the two nodes """
        return vec_mag( np.subtract( n2.state , n1.state ) )
                
    def uniform_cost_search( self , init_node , goal_node ):
        """ Uniform Cost Search, takes path length into account by accumulating action costs on a path """
        frontier = PriorityQueue() # Search Priority Queue, always return the state with the least accumulated cost
        init_node.path = [] # Attach a sequence to each node to record the path to it, this may consume a lot of space for large problems
        init_node.cost = 0 # No cost for no action
        frontier.push( init_node , init_node.cost )
        visited = set([])    
        loop_count = 0 # Not used
        while len(frontier) > 0: # While there are still nodes on the frontier
            n_i = frontier.pop() # Pop the state with the least cumulative cost
            if tuple(n_i.state) not in visited: # If this state has not been visted before, then
                visited.add( tuple(n_i.state) ) # log the visit
                currentPath = n_i.path # Path to the current state
                # if is_goal( n_i.state ): # If this state is the goal, then return path and visit information
                if n_i == goal_node: # If this state is the goal, then return path and visit information
                    # return ( n_i.path + [n_i.state] , visited ) 
                    return n_i.path + [goal_node.state]
                else: # else this state is not the goal, add the successors to the frontier
                    # for aDex , a in enumerate(actions): # For each available action
                    for aDex , n_prime in enumerate( n_i.neighbors ): # For each available action
                        # s_prime = f( n_i.state , a ) # Generate the next state from the transition function
                        # Create a new node for the state, accumulate the path and the cost that it took to get there
                        # n_prime = SearchNode( s_prime , actions , n_i , a , n_i.cost + uniform_cost_search.cost[a] )
                        n_prime.cost = n_i.cost + self.node_distance( n_i , n_prime )
                        n_prime.path = currentPath[:] + [n_i.state] # Assemble a plan that is the path so far plus the current action
                        frontier.push( n_prime , n_prime.cost )
#                        if frontier.contains( n_prime ): # If the frontier already contains this state, then
#                            if frontier.get_cost( n_prime ) > n_prime.cost: # If the frontier's version is more costly, then replace
#                                frontier.push( n_prime , n_prime.cost ) # Alternative would have been to change PriorityQ's replacement 
#                        else: # else no conflict, push                    behavior so that only least costly version is saved
#                            frontier.push( n_prime , n_prime.cost )
                loop_count += 1 # Not used
        return None # The frontier was exhausted without finding the goal, return a sequence of excuses
            
def test_prm_env( figPath , num_samples=500 , step_length=0.5 , env='env0.txt' , num_neighbors = 3 , num_plans = 1 ):
    
    print "STARTED"    
    
    pe = PolygonEnvironment()
    pe.read_env(env)

    dims = len(pe.start)
    start_time = time.time()

    prm = PRM(num_samples,
              dims,
              step_length,
              num_neighbors,
              pe.lims,
              collision_func=pe.test_collisions)
    
    if _RANDSTAT:
        pass
#        pe.start = rrt.get_random_state()
#        pe.goal  = rrt.get_random_state()
              
    prm.build_prm( pe.start , pe.goal )
    
    plan = prm.uniform_cost_search( prm.init_n , prm.goal_n )
    
    if num_plans > 1:
        allPlans = [plan]
        for i in xrange( num_plans - 1 ):
            allPlans.append( prm.uniform_cost_search( choice( prm.T.nodes ) , choice( prm.T.nodes ) ) )
    
    run_time = time.time() - start_time
    
    accum_print( "Init State:" , prm.init )
    accum_print( "Goal State:" , prm.goal )
    accum_print( 'plan:', plan )
    if plan:
        accum_print( 'plan length:' , len( plan ) )
    accum_print( 'run_time =', run_time )
    accum_print( "Number of Nodes:" , len( prm.T.nodes ) )
    if _GRAPHGEN:
        plotter.close( 'all' )
        if num_plans > 1:
            pe.draw_plans( allPlans , prm , figPath , dynamic_plan=False )
        else:
            pe.draw_plan( plan , prm , figPath , dynamic_plan=False )
    accum_print( "END" )
    if _DEBUG:
        for node in prm.T.nodes:
            print node

    return plan, prm
        
        
# === End PRM ===
    
_GRAPHGEN = True # Set to true to generate graphics
_RANDSTAT = True # Set to true for random start and end states
_DEBUG    = True
_SIMALL   = True

if __name__ == '__main__':
    
    _RANDSTAT = False # Use the text file start and end states    
        
    if False or _SIMALL:
        accum_sep( "Problem 2.1.a" )
        test_prm_env( "p_2-1-a.eps" , num_samples=200 , step_length=0.5 , env='env0.txt' , num_neighbors = 4 )
        accum_out_and_clear( "Problem_2-1-a_Output.txt" )
        
    if False or _SIMALL:
        accum_sep( "Problem 2.1.b" )
        test_prm_env( "p_2-1-b.eps" , num_samples=500 , step_length=0.1 , env='env1.txt' , num_neighbors = 4 )
        accum_out_and_clear( "Problem_2-1-b_Output.txt" )
        
    if False or _SIMALL:
        accum_sep( "Problem 2.1.c" )
        test_prm_env( "p_2-1-c.eps" , num_samples=400 , step_length=0.5 , env='env0.txt' , num_neighbors = 6 , num_plans = 3 )
        accum_out_and_clear( "Problem_2-1-c_Output.txt" )
        
    if False or _SIMALL:
        accum_sep( "Problem 2.1.d" )
        test_prm_env( "p_2-1-d.eps" , num_samples=500 , step_length=0.1 , env='env1.txt' , num_neighbors = 6 , num_plans = 3 )
        accum_out_and_clear( "Problem_2-1-d_Output.txt" )