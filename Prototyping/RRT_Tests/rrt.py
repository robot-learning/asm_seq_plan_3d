#!/usr/bin/env python
'''
Package providing helper classes and functions for performing graph search operations for planning.
'''
import numpy as np
import matplotlib.pyplot as plotter
#from math import pi
from collisions import PolygonEnvironment
import time , os

from random import random 
from types import NoneType 
from HWutil import vec_eq , vec_unit , vec_mag , elemw
from UtahUtil import vec_random_limits , accum_print , accum_sep , accum_out_and_clear

from marchhare.marchhare import sep
from marchhare.Vector import AABB_span , is_vector
from marchhare.MathKit import product , flip_weighted

_TRAPPED  = 'trapped'
_ADVANCED = 'advanced'
_REACHED  = 'reached'

# ~~ Constants , Shortcuts , Aliases ~~
# URL, add global vars across modules: http://stackoverflow.com/a/15959638/893511
EPSILON = 1e-7
infty = 1e309 # URL: http://stackoverflow.com/questions/1628026/python-infinity-any-caveats#comment31860436_1628026
endl = os.linesep

# === RRT classes and functions ===

class TreeNode:
    """ Search tree node for use with RRT """    
    
    def __init__( self , state , parent = None ):
        self.state = state # - Configuration
        self.children = [] # - Successors , possibly leading to the goal configuration
        self.parent = parent # Parent , for constructing path back to the start configuration

    def add_child( self , child ):
        """ Add a successor node """
        # NOTE: No correctness checks are made before adding
        self.children.append( child )
        
    def remove_child_by_ref( self , childRef ):
        """ Remove the object referred to by 'childRef' from 'self.children' , if 'childRef' DNE then fail gracefully """
        try:
            self.children.remove( childRef )
        except ValueError:
            print "WARN , TreeNode.remove_child_by_ref: Reference" , childRef , "DNE in list of successors!"
        
    def __str__( self ):
        """ Return a string representation of the TreeNode """
        return "TreeNode@" + str( self.state )

class RRTSearchTree:
    """ Search tree for use with RRT algorithms , Functions for searching and modifying the tree structure itself """    
    # NOTE: This class does not implement RRT algorithms , See class 'RRT'
    
    def __init__( self , init ):
        """ Create an empty tree with a root node at the initial state """
        self.root     = TreeNode( init ) # The initial state is the root node
        self.nodes    = [ self.root ] # -- Collection of all nodes
        self.edges    = [] # ------------- collection of all edges (Not used for searching)
        self.volume   = 0 # -------------- Volume occupied by the N-Dim bounding box
        self.hasGrown = False # ---------- Flag for whether the bounding box grew during the last volume computation

    def find_nearest( self , s_query ):
        """ Find the node nearest to 's_query' and return it with the distance between 's_query' and the node , Linear search """
        min_d = infty # Pick some arbitrarily large distance
        nn = self.root
        for n_i in self.nodes:
            if _DEBUG:
                print "DEBUG:" , s_query , n_i.state
            d = np.linalg.norm( np.subtract( s_query , n_i.state ) )
            if d < min_d:
                nn = n_i
                min_d = d
        return ( nn, min_d )
    
    def compute_AABB_volume( self ):
        """ Compute and store the volume of the AABB enclosing all configurations in the tree """
        allQ = [ n.state for n in self.nodes ]
        span = AABB_span( allQ )
        self.volume = product( span )
        return self.volume
    
    def grew( self ):
        """ Compute and store the volume of the AABB , and Determine if the box has grown """
        oldVol = self.volume
        self.compute_AABB_volume()
        return self.volume > oldVol

    def add_node( self , node , parent ):
        """ Add a node with a designated parent , add node to the list of the parent's successors """
        self.nodes.append( node ) # Add node to the master list of nodes
        self.edges.append( ( parent.state , node.state ) ) # Add a record of this edge
        node.parent = parent # Assign parentage to the node
        parent.add_child( node ) # Add the node to the parent's successors
        
    def add_node_disjoint( self , node ): 
        """ Add a node without parents or edges """
        self.nodes.append( node )

    def get_states_and_edges( self ):
        """ Return all states and all edges """
        states = np.array( [ n.state for n in self.nodes ] )
        return ( states , self.edges )

    def search_by_state( self , q_query ): 
        """ Return the first TreeNode that matches state 'q_query', otherwise return None , Linear Search """
        for n in self.nodes:
            if vec_eq( n.state , q_query ):
                return n
        return None

    def get_back_path( self , n ):
        """ Follow parent relationships from 'n' to the root node, Reverse the sequence so that it is a forward path """
        path = []
        # while n.parent is not None:
        while n is not None:
            path.append( n.state )
            n = n.parent
        path.reverse()
        return path
        
    def add_q_to_closest_parent( self , q ):
        """ Create a new TreeNode and assign it to the node with the closest config """
        closest = self.find_nearest( q )[0]
        self.add_node( TreeNode( q ) , closest )

class RRT:
    """ Rapidly-Exploring Random Tree Implementation """

    def __init__( self , num_samples , num_dimensions = 2 , step_length = 1 , lims = None , connect_prob = 0.05 , collision_func = None ):
        ''' Initialize an RRT planning instance '''
        self.K = num_samples # ----------- Number of samples to generate
        self.n = num_dimensions # -------- dimensionality of the space
        self.epsilon = step_length # ----- step size
        self.connect_prob = connect_prob # bias probability
        self.found_path = False # -------- Flag for if the problem has been solved
        self.newestNode = None # --------- Most recent node added by 'Connect' (Strandberg)
        self.newNodeDelta = 0 # ---------- Number of node added by a 'Connect' operation (Strandberg)

        self.in_collision = collision_func # function for checking collisions
        if collision_func is None: # If there was no collision function assigned, use the dummy function so that things don't crash
            self.in_collision = self.fake_in_collision

        # Setup range limits for config space (joint limits, etc)
        self.limits = lims
        if self.limits is None: # If there were no limits passed as params
            self.limits = []
            for n in xrange( num_dimensions ): # for each dimension
                self.limits.append( [ 0 , 100 ] ) # set default limits 0 - 100
            self.limits = np.array( self.limits ) # Convert limits to an np.array
        self.ranges = self.limits[ : , 1 ] - self.limits[ : , 0 ] # Calc the spans of the limits
        

    def get_random_state( self ): 
        """ Return a random state within the limits of the world """
        return vec_random_limits( len( self.limits ) , self.limits )

    def init_tree( self , init ):
        """ Instantiate the tree with one root node """
        self.T = RRTSearchTree( init ) # Start with initial state as the root node
        self.init = np.array( init ) # Store the config of the init state
        
    def build_rrt( self , init , goal ):
        ''' Build the rrt from init to goal , Returns path to goal or None '''
        # NOTE: This is the baseline RRT algorithm implementation
        self.goal = np.array( goal ) # store the config of the goal state
        self.init = np.array( init ) # store the config of the init state
        self.found_path = False # ---- Flag for reached goal

        # Build tree and search
        self.init_tree( init ) # Set up the RRT problem with the specified root node

        result = None
        samples = 0

        while isinstance( result , ( NoneType , bool ) ) and samples < self.K: # run continuously until either the goal or iteration limit is reached
            # 1. Sample either a random point or the goal with P(bias)
            target = self.sample()        
            # 2. Perform extension toward the target and collect result (  )
            result = self.extend( target )
            samples += 1
            if _DEBUG:
                print "Iteration" , samples , "with result" , result
            
        if not isinstance( result , ( NoneType , bool ) ):
            self.found_path = True
            return result
        else:
            return None

    def build_rrt_connect( self , init , goal ):
        ''' Build the rrt connect from init to goal , Returns path to goal or None '''
        # NOTE: This is the RRT-Connect algorithm
        
        self.goal = np.array( goal ) # store the config of the goal state
        self.init = np.array( init ) # store the config of the init state
        self.found_path = False # ---- Flag for reached goal

        # Set up the tree structure
        self.T = RRTSearchTree( init )
        
        result = None
        samples = 0

        while isinstance( result , ( NoneType , bool ) ) and samples < self.K: # Iterate until either the goal or the iteration limit is reached
            # 1. Sample either a random point or the goal with P(bias)
            target = self.sample()     
            
            # 2. Perform extension and collect result
            result = self.extend_connect( target )
            
            samples += 1
            if _DEBUG:
                print "Iteration" , samples , "with result" , result
            
        if not isinstance( result , ( NoneType , bool ) ):
            self.found_path = True
            return result
        else:
            return None
        return None

    def sample( self ):
        ''' Sample a new configuration and return '''
        # 1. Decide if the target is random or goal 
        if random() <= self.connect_prob: # Roll the dice and with P(bias) choose the goal as the target
            return self.goal
        else: # else, choose a random point in the config space
            temp = vec_random_limits( len( self.limits ) , self.limits )
            if _DEBUG:
                print "Random Sample:" , temp
            return temp

    def extend( self , target ):
        ''' Perform rrt extend operation.  q - new configuration to extend towards 
            ~~~~~~~~~~~~
            Return Codes:
            list - Goal found , return path back from start to goal
            True - A new node was added that extends towards the target from the nearest node
            None - Trapped , extension was not successful '''
        # NOTE: This function is the guts of the baseline RRT algorithm
        
        # 2. Find the closest node to the target
        q_near , dist = self.T.find_nearest( target ) # return ( NODE_REFERENCE , DISTANCE_TO_NODE )
        # 3. Gen an epsilon step towards the target
        q_new = np.add( np.multiply( vec_unit( np.subtract( target , q_near.state ) ) , self.epsilon ) , q_near.state )
        if _DEBUG:
            print "q_new:" , q_new , endl
        # 4. Check the viability of the step
        if not self.in_collision( q_new ):
            self.T.add_node( TreeNode( q_new ) , q_near )
            # 5. Check if we have reached the goal
            if vec_mag( np.subtract( self.goal , q_new ) ) <= self.epsilon:
                # 4.a. If step is goal, gen path from parents and return it
                temp = TreeNode( self.goal )
                self.T.add_node( temp , q_near )
                return self.T.get_back_path( temp ) # Reached
            else:
                return True # Advanced
        
        return None # Trapped
        
    def extend_connect( self , target ):
        ''' Perform rrt extend operation.  q - new configuration to extend towards 
            March towards the target for as long as the new step is neither in collision nor at the goal '''
        # NOTE: This is the guts of the RRT-Connect algorithm
            
        # 2. Find the closest node to the target
        q_near , dist = self.T.find_nearest( target ) # return ( NODE_REFERENCE , DISTANCE_TO_NODE )
        # 3. Gen an epsilon step towards the target
        q_new = np.add( np.multiply( vec_unit( np.subtract( target , q_near.state ) ) , self.epsilon ) , q_near.state )
        if _DEBUG:
            print "q_new:" , q_new , endl
        # 4. Check the viability of the step
        last = q_near
        if not self.in_collision( q_new ):
            # 5. As long as we have neither collided nor reached the target , Keep marching by 'epsilon' towards the target
            while not self.in_collision( q_new ) and vec_mag( np.subtract( target , q_new ) ) > self.epsilon:
                if _DEBUG:
                    print "Marching , q_new:" , q_new
                # 6. Add the validated node
                temp = TreeNode( q_new )
                self.T.add_node( temp , last )
                # 7. Check if we have reached the goal
                if vec_mag( np.subtract( self.goal , q_new ) ) <= self.epsilon:
                    # 7.a. If step is goal , make up the last little bit , and gen path from parents and return it
                    temp = TreeNode( self.goal )
                    self.T.add_node( temp , q_near )
                    return self.T.get_back_path( temp ) # Reached
                # 8. Else path not reached, march a step so that the new step can be evaluated at the top of the loop
                q_new = np.add( np.multiply( vec_unit( np.subtract( target , q_new ) ) , self.epsilon ) , q_new )
                last = temp
            return True # Advanced one step or many
        
        return None # Trapped, first step was in collision
    
    ## === 2004 _ Augmenting RRT w Local Trees _ Strandberg ================================================================================
    
    def get_root_node( self ):
        """ Return the root node """
        return self.T.root
    
    def get_closest( self , q ):
        """ Return the closest node to q """
        return self.T.find_nearest( q )[0]
    
    def is_node_in_tree( self , nodeRef ):
        """ Return true if 'nodeRef' is in the list of nodes """
        return nodeRef in self.T.nodes
    
    def is_q_in_tree( self , q ):
        """ Return True """
        for node in self.T.nodes:
            if vec_eq( node , q ):
                return True
        return False
    
    def sample_unbiased( self ):
        ''' Sample a new configuration and return '''
        # 1. Decide if the target is random or goal 
        temp = vec_random_limits( len( self.limits ) , self.limits )
        if _DEBUG:
            print "Random Sample:" , temp
        return temp
    
    def Connect( self , target ):
        ''' Perform rrt extend operation.  q - new configuration to extend towards 
            March towards the target for as long as the new step is neither in collision nor at the goal 
            ~~~~~~
            Return Codes:
            True  , TreeNode - Reached the target   , Node that points to the target
            False , TreeNode - Did NOT reach target , Node that points to the last collision-free config
            False , None     - Did NOT reach target , There were no nodes in collision
            '''
        # NOTE: This is the Strandberg version of Connect
            
        SHOWDEBUG = True
        
        self.newNodeDelta = 0
            
        # 2. Find the closest node to the target
        q_near , dist = self.T.find_nearest( target ) # return ( NODE_REFERENCE , DISTANCE_TO_NODE )
        # 3. Gen an epsilon step towards the target
        if SHOWDEBUG:
            print "DEBUG:" , target , q_near.state
        q_new = np.add( np.multiply( vec_unit( np.subtract( target , q_near.state ) ) , self.epsilon ) , q_near.state )
        if SHOWDEBUG:
            print "q_new:" , q_new , endl
        # 4. Check the viability of the step
        last = q_near
        if not self.in_collision( q_new ):
            # 5. As long as we have neither collided nor reached the target , Keep marching by 'epsilon' towards the target
            while not self.in_collision( q_new ) and vec_mag( np.subtract( target , q_new ) ) > self.epsilon:
                if SHOWDEBUG:
                    print "Marching , q_new:" , q_new
                # 6. Add the validated node
                temp = TreeNode( q_new )
                self.T.add_node( temp , last )
                # 7. March a step so that the new step can be evaluated at the top of the loop
                q_new = np.add( np.multiply( vec_unit( np.subtract( target , q_new ) ) , self.epsilon ) , q_new )
                last = temp
                self.newNodeDelta += 1
            # 8. Check if we have reached the goal
            if vec_mag( np.subtract( target , q_new ) ) <= self.epsilon:
                # 7.a. If step is goal , make up the last little bit , and gen path from parents and return it
                final = TreeNode( target )
                self.T.add_node( final , last )
                self.newestNode = final
                return True , final
            # 9. Else there was a collision
            else:
                self.newestNode = last
                return False , last # Advanced one step or many
        return False , q_near # Trapped, first step was in collision
        
    def count_nodes( self ):
        """ Return the number of nodes in this tree """
        return len( self.T.nodes )
    
    def Merge( self , node_self , T_other , node_othr ):
        """ Merge this tree with other via a connection from 'node_self' to 'node_othr' """
        # NOTE: This function does not delete 'T_other' as required by Strandberg , Discarding 'T_other' is the responsibility of client code
        # NOTE: This function assumes that both 'node_self' and 'node_othr' are properly connected 'TreeNode's
        # NOTE: This function assumes that 'node_self' and 'node_othr' are close enough in the problem to not need 'Connect'
        
        SHOWDEBUG = True
        
        visitSet = set([])
        
        if SHOWDEBUG:
            sep( "Pre-Merge Report ..." , 2 )
            
            print "This tree has " , self.count_nodes() , "nodes"
            print "Other tree has" , T_other.count_nodes() , "nodes"
            print "Does this tree have the arg node?: " , self.is_node_in_tree( node_self )
            print "Does other tree have the arg node?:" , T_other.is_node_in_tree( node_othr )
            print "How close are the nodes?:" , vec_mag( np.subtract( node_self.state , node_othr.state ) )
            print "Are they the same node?:" , node_self == node_othr
        
        first = True
        
        # 1. For each of the nodes on 'node_othr's branch, all the way up to the root
        # while node_othr.parent != None:
        while node_othr != None:
            
            # Save the current parent for the next iteration
            othrLastParent = node_othr.parent
            
            if othrLastParent and SHOWDEBUG:
                visitSet.add( tuple( node_othr.parent.state ) )
            
            # 2. Assign the nearest self node as the parent of the nearest other node
            node_othr.parent = node_self
            # 3. Assign the nearest other node as the successor of the nearest self node
            node_self.add_child( node_othr )
            
            self.T.edges.append( ( node_self.state , node_othr.state ) )
            
            # 4. Remove the merged node from the list of children of the nearest other node
            if not first:
                node_othr.remove_child_by_ref( node_self )
            first = False
            # 5. Move up the branch by 1 for the next iteration
            node_self = node_othr
            node_othr = othrLastParent
            
            if SHOWDEBUG and node_othr:
                if 0:
                    print '.' ,
                elif 1:
                    print "Set size:" , len( visitSet ) , 
                else:
                    print "Other node is  " , node_othr.__class__ ,  #type( node_othr ) ,
                    print "Other parent is" , node_othr.parent.__class__  , 
            
        # 6. Add the merged nodes to the merginf tree
        self.T.nodes.extend( T_other.T.nodes )
        # self.T.edges.extend( T_other.T.edges )
        
        if SHOWDEBUG:
            print
        
    def TryMerge( self , T_other , node_self ):
        """ Try to merge this tree with 'T_other' via 'node_self' on this tree """
        
        SHOWDEBUG = True
        
        if is_vector( node_self ): # If we got a config instead of a node, try to match to a node
            
            if SHOWDEBUG:
                print "Got a state , is the node in the tree?:" , self.is_q_in_tree( node_self )
            
            node_self = self.T.find_nearest( node_self )
            
        if isinstance( node_self , TreeNode ):
            
            if SHOWDEBUG:
                print "Is the node in the tree?:" , self.is_node_in_tree( node_self )
            
            reached , lastNode = T_other.Connect( node_self.state )
            
            if SHOWDEBUG:
                print "Did Connect succeed?:" , reached
                print "What was the last q?:" , lastNode.state
            
            if reached:
                
                if SHOWDEBUG:
                    print "About to Merge ..."
                
                self.Merge( node_self , T_other , lastNode )
                return True
            else:
                return False
        else:
            return False
        
    ## ___ End Strandberg _________________________________________________________________________________________________________________
        
    def extend_connect_bidir( self , target ):
        ''' Perform rrt extend operation.  q - new configuration to extend towards '''
        # NOTE: This is the guts of the RRT-Bidirectional-Connect Algorithm
        
        # 1. Find the closest node to the target
        q_near , dist = self.T.find_nearest( target ) # return ( NODE_REFERENCE , DISTANCE_TO_NODE )
        
        # 2. Gen an epsilon step towards the target
        q_new = np.add( np.multiply( vec_unit( np.subtract( target , q_near.state ) ) , self.epsilon ) , q_near.state )
        if _DEBUG:
            print "q_new:" , q_new , endl
            
        # 3. Check the viability of the step
        last = q_near
        sampleReached = False # Did we connect all the way to target?
        temp = None
        # 4. If the sampled point was viable
        if not self.in_collision( q_new ):
            # 5. While the next march point is neither in collision nor at the goal
            while not self.in_collision( q_new ) and vec_mag( np.subtract( target , q_new ) ) > self.epsilon:
                if _DEBUG:
                    print "Marching , q_new:" , q_new
                temp = TreeNode( q_new )
                self.T.add_node( temp , last )
                
                # 6. Check if we have reached the goal # Our target is not the GOAL for bidir
                if vec_mag( np.subtract( target , q_new ) ) <= self.epsilon:
                    # 4.a. If step is goal, gen path from parents and return it
                    temp = TreeNode( target )
                    self.T.add_node( temp , q_near )
                    # return self.T.get_back_path( temp ) , temp , True  # Reached
                    return True , temp , True  # Reached
                q_new = np.add( np.multiply( vec_unit( np.subtract( target , q_new ) ) , self.epsilon ) , q_new )
                last = temp
            if vec_mag( np.subtract( target , q_new ) ) <= self.epsilon:
                sampleReached = True # We connected all the way to target
                if _DEBUG:
                    print "REACHED!"
            return True , temp , sampleReached # Advanced
        
        return None , q_near , False # Trapped

    def fake_in_collision(self, q):
        ''' We never collide with this function! '''
        return False
    
    
    def set_goal( self , goal ):
        """ Set a new goal for the RRT """
        self.goal = np.array( goal ) # store the config of the goal state
        

## === 2004 _ Augmenting RRT w Local Trees _ Strandberg ===============================================================================
    
class RRTLocTrees( RRT ):
    """ RRT with Local Trees implementation """
    
    def __init__( self , 
                  num_samples , # ---------- Maximum allowed number of samples
                  num_dimensions = 2 , # --- Number of dimensions of this problem
                  step_length    = 1 , # --- Epsilon , length of march steps
                  lims           = None , #- Dimension limits
                  connect_prob   = 0.05 , #- Connection probability , NOT USED
                  collision_func = None , #- Collision checker 
                  N_loc_lim      = 20 , # -- Maximum allowed number of local trees
                  p_grow         = 0.12 ): # Probability of growing an existing tree
        """ Setup a collection of trees """
        
        RRT.__init__( self , num_samples , num_dimensions , step_length , lims , connect_prob , collision_func )
        
        self.N_loc    = N_loc_lim
        self.allTrees = []
        self.N_tree   = 0
        self.p_grow   = p_grow
        self.samples  = 0
        
    def get_all_nodes( self ):
        """ Get all the nodes that were generated while solving the problem """
        rtnNodes = []
        for T_i in self.allTrees:
            rtnNodes.extend( T_i.T.nodes )
        return rtnNodes
        
    def count_all_nodes( self ):
        """ Get all the nodes that were generated while solving the problem """
        count = 0
        for T_i in self.allTrees:
            count += len( T_i.T.nodes )
        return count
        
    def get_all_edges( self ):
        """ Get all the nodes that were generated while solving the problem """
        rtnEdges = []
        for T_i in self.allTrees:
            rtnEdges.extend( T_i.T.edges )
        return rtnEdges
        
    def get_states_and_edges( self ):
        """ Return all states and all edges """
        states = np.array( [ n.state for n in self.get_all_nodes() ] )
        return ( states , self.get_all_edges() )
        
    def RRT_from_self( self ):
        """ Create a vanilla RRT with the same specifications as the RRTLocTrees """
        rtnTree = RRT( self.K , # ---------- num_samples
                       self.n , # ---------- num_dimensions
                       self.epsilon , # ---- step_length
                       self.limits , # ----- lims
                       0.0 , # ------------- connect_prob == 0, connection managed by 'RRTLocTrees'
                       self.in_collision ) # collision_func
        return rtnTree
        
    def init_trees( self , init , goal ):
        """ Create start and goal trees at indices 0 and 1, respectively """
        self.init = init 
        self.goal = goal
        self.allTrees = [] # Erase any old trees
        for q_init in [ init , goal ]:
            temp = self.RRT_from_self()
            temp.init_tree( q_init )
            self.allTrees.append( temp )
            
    def grow_roll( self ):
        """ Dice roll to grow """
        return flip_weighted( self.p_grow )
    
    def GrowLocalTrees( self , Ta_skipDex , q_trgt ):
        """ Grow all trees except that stored at 'Ta_skipDex' towards 'q_trgt' """
        
        SHOWDEBUG = True
        
        if SHOWDEBUG:
            print endl , "Growing local trees ..."
        
        # 1. If the target is collision-free
        if self.allTrees[ Ta_skipDex ].in_collision( q_trgt ):
            if SHOWDEBUG:
                print "Target in collision, Return False!" , endl
            return False
        else:
            
            if SHOWDEBUG:
                print "Target not in collision , growing trees ..." 
            
            # 2. For all trees except the skipped tree
            # for i , T_i in enumerate( self.allTrees ):
            for i in xrange( 0 , len( self.allTrees ) - 1 ):
                
                if SHOWDEBUG:
                    print "Tree" , i+1 , "of" , len( self.allTrees )
                
                T_i = self.allTrees[i]
                if i != Ta_skipDex:
                    # 3. Attempt to connect each tree to the given configuration
                    
                    if SHOWDEBUG:
                        print "Growing tree" , i , "towards the target , Success?:" , 
                    
                    reached , q_new_i = T_i.Connect( q_trgt )
                    
                    if SHOWDEBUG:
                        print reached , ", added" , T_i.newNodeDelta , "nodes"
                    
                    # 4. If T_i reached the config, or if it grew in the process of marching towards it
                    Tgrew = T_i.T.grew()
                    if SHOWDEBUG:
                        if Tgrew:
                            print "Tree" , i ,  "grew"
                    if reached or Tgrew:
                        # 5. For every tree after T_i , except for the active tree
                        deleteList = []
                        for j in xrange( i+1 , len( self.allTrees ) ):
                            if j != Ta_skipDex:
                                T_j = self.allTrees[j]
                                # 6. Try to merge every other T_j with T_i via the newly grown node
                                if T_i.TryMerge( T_j , q_new_i ): 
                                    self.N_tree -= 1
                                    deleteList.append( T_j )
                                self.samples +=  T_j.newNodeDelta
                        for delTree in deleteList:
                            self.allTrees.remove( delTree )
                    # 7. If the specified config was reached, Return
                    if reached:
                        if SHOWDEBUG:
                            print
                        return True
            # 8. Otherwise, no local tree could rach the given config. The config has been nominated as the new local tree because
            #   A. It is not in collision
            #   B. It cannot be reached by any tree
            if len( self.allTrees ) - 2 <= self.N_loc:
                
                if SHOWDEBUG:
                    print "Creating a new local tree"
                
                temp = self.RRT_from_self()
                temp.init_tree( q_trgt )
                self.allTrees.append( temp )
                self.N_tree += 1
                
            if SHOWDEBUG:
                print
        
            
    def RRTLocTreesF( self , init , goal ):
        """ 2004 _ Augmenting RRT-Planners with Local Trees _ Morten Strandberg """
        
        SHOWDEBUG = True
        ITERCOUNT = 0
        
        # 1. Start with 2 trees at the Start and the Goal
        self.init_trees( init , goal ) # Init trees
        self.T = self.allTrees[0] # So that we can plot it
        
        # 1.5. Init vars
        result       = None # ----------------------- Is either the path S-->G or None if node limit reached without success
        self.samples = 0 # -------------------------- Running count of all the nodes that have been added to the problem
        self.actDex  = 0 # -------------------------- Index of the active tree
        self.T_a     = self.allTrees[ self.actDex ] # Active tree
        self.N_tree  = len( self.allTrees ) # ------- Count of all the trees in the problem
        
        self.T_a.newestNode = self.T_a.get_root_node()
        
        def flip_bit( bit ):
            """ Return 1 for input 0 and 0 for input 1 """
            if bit == 1:
                return 0
            if bit == 0:
                return 1
            print bit , "is not a bit!"
        
        def swap_trees( self ):
            """ NOTE: You must pass the instance to this function """
            self.actDex = flip_bit( self.actDex )
            self.T_a = self.allTrees[ self.actDex ]
            
        def free_sample():
            """ Obtain a collision-free, unbiased sample """
            q_rand = self.T_a.sample_unbiased()
            while self.T_a.in_collision( q_rand ):
                q_rand = self.T_a.sample_unbiased()
            return q_rand
        
        # 2. Iterate until either the goal or the iteration limit is reached
        while isinstance( result , ( NoneType , bool ) ) and self.count_all_nodes() < self.K: 
            
            if SHOWDEBUG:
                ITERCOUNT += 1
                sep( "Iteration " + str( ITERCOUNT ) + " , There are " + str( self.count_all_nodes() ) + " samples" , 15 )
                print "There are" , len( self.allTrees ) - 2 , "local trees, counting" , self.N_tree
                
            
            # 3. Get sample
            q_rand = free_sample()
            
            if SHOWDEBUG:
                print "Got a random sample" , q_rand #, "," , [ type( elem ) for elem in q_rand ]
            
            # 4. Attempt to reach the new config, and if we did not reach it ...
            reached , q_new_A = self.T_a.Connect( q_rand )
            
            if SHOWDEBUG:
                print "Sample was reached" , reached , ", got as far as" , q_new_A.state , "which is" ,  \
                      np.linalg.norm( np.subtract( q_rand , q_new_A.state ) ), "away"
            
#            self.samples +=  self.T_a.newNodeDelta
            if not reached:
                #5. If we did not reach the tree limit  -or-  the p_grow dice roll lands, grow all trees except T_a
                treeLimOK = ( len( self.allTrees ) - 2 <= self.N_loc )
                growDice  = self.grow_roll()
                
                if SHOWDEBUG:
                    print "Decision to grow ..."
                    print "\tBelow tree limit:" , treeLimOK
                    print "\tGrow dice roll:  " , growDice
                    if treeLimOK or growDice:
                        print "\tYes , About to grow ..."
                    else:
                        print "\tNo , Will not grow!"
                
                if treeLimOK or growDice:
                    self.GrowLocalTrees( self.actDex , q_new_A.state )
                    
            # 6. If the active tree grew
            if self.T_a.T.grew():
                
                if SHOWDEBUG:
                    print "Active tree was expanded. Attempt merge"
                
                # 7. Try to merge all local trees with the active tree's new node
                for i , T_i in enumerate( self.allTrees ):                    
                    if i not in ( 0 , 1 ):
                        
                        if SHOWDEBUG:
                            print "Local Tree" , i , "attempt merge with" , self.actDex , ", Success?" ,
                        
                        # self.T_a.TryMerge( T_i , q_new_A )
                        mergeSuccess = self.T_a.TryMerge( T_i , self.T_a.newestNode )
                        
                        if SHOWDEBUG:
                            print mergeSuccess , "," , T_i.newNodeDelta , "added"
                        
                        self.samples +=  T_i.newNodeDelta
            else:
                if SHOWDEBUG:
                    print "Active tree was NOT expanded."
            
            if SHOWDEBUG:
                print "Attempting to reach goal, success?" , 
            
            # 8. Try to connect the start and goal trees via T_b's newest node
            # reached , q_new_B = self.allTrees[ flip_bit( self.actDex ) ].Connect( q_new_A.state ) 
            reached , q_new_B = self.allTrees[ flip_bit( self.actDex ) ].Connect( 
                self.T_a.newestNode.state 
            )
            
            if SHOWDEBUG:
                print reached
            
            if reached:
                # Sort out what nodes are on the merging side
                if self.actDex == 0:
                    q_new_0 = q_new_A
                    q_new_1 = q_new_B
                else:
                    q_new_0 = q_new_B
                    q_new_1 = q_new_A
                # A. Store the goal node
                goalNode = self.allTrees[1].T.root
                
                if _DEBUG:
                    sep( "MERGE CHECK" )
                    print "Found the correct parent node:" , q_new_0 , "," , q_new_0 in self.allTrees[0].T.nodes
                    print "Found the correct child node: " , q_new_1 , "," , q_new_1 in self.allTrees[1].T.nodes
                
                # B. Merge the goal tree into the start tree
                self.allTrees[0].Merge( q_new_0 , self.allTrees[1] , q_new_1 )
                # C. Get the path from the start to the goal
                result = self.allTrees[0].T.get_back_path( goalNode )
            # 9. If did not connect, Swap active trees and iterate
            if not reached:
                
                if SHOWDEBUG:
                    print "Swapping , skip index " , self.actDex , "is now" ,
                
                swap_trees( self )
                
                if SHOWDEBUG:
                    print  self.actDex 
                
                
            if SHOWDEBUG:
                print endl , endl
        else:
            print "Search failed, node number exceeded:" , self.count_all_nodes() , ">" , self.K
                
        return result
                
    def get_random_state( self ):
        """ Get a random state from the configuration space """
        return self.RRT_from_self().get_random_state()
            
                        
                
            
## ___ End Strandberg _________________________________________________________________________________________________________________
        

def test_rrt_env( figPath , num_samples = 500 , step_length = 2 , env = 'env0.txt' , connect = False , connect_prob = 0.05 ):
    ''' create an instance of PolygonEnvironment from a description file and plan a path from start to goal on it using an RRT

    num_samples - number of samples to generate in RRT
    step_length - step size for growing in rrt (epsilon)
    env - path to the environment file to read
    connect - If True run rrt_connect

    returns plan, planner - plan is the set of configurations from start to goal, planner is the rrt used for building the plan '''
    
    print "STARTED"    
    
    pe = PolygonEnvironment()
    pe.read_env(env)

    dims = len(pe.start)
    start_time = time.time()

    rrt = RRT(num_samples,
              dims,
              step_length,
              pe.lims,
              connect_prob,
              collision_func=pe.test_collisions)
    
    if _RANDSTAT:
        pe.start = rrt.get_random_state()
        pe.goal  = rrt.get_random_state()
              
    if connect:
        plan = rrt.build_rrt_connect(pe.start, pe.goal)
    else:
        plan = rrt.build_rrt(pe.start, pe.goal)
    run_time = time.time() - start_time
    
    accum_print( "Init State:" , rrt.init )
    accum_print( "Goal State:" , rrt.goal )
    closest , dist = rrt.T.find_nearest( rrt.goal )
    accum_print( "Closest Node:" , closest , "at distance" , dist )
    accum_print( 'plan:', plan )
    if plan:
        accum_print( 'plan length:' , len( plan ) )
    accum_print( 'run_time =', run_time )
    accum_print( "Number of Nodes:" , len( rrt.T.nodes ) )
    if _GRAPHGEN:
        plotter.close( 'all' )
        pe.draw_plan( plan , rrt , figPath , dynamic_plan=False )
    accum_print( "END" )
    if _DEBUG:
        for node in rrt.T.nodes:
            print node

    return plan, rrt

def test_rrt_bidir( figPath , num_samples = 500 , step_length = 2 , env = 'env0.txt' , connect = False , connect_prob = 0.05 ):
    """ Search for a path with Bi-directional RRT Connect """
    
    print "STARTED"    
    
    # 0. Init environment variables
    pe = PolygonEnvironment()
    pe.read_env(env)
    dims = len(pe.start)
    start_time = time.time()
    
    # 1. Init RRT problems for both the start and goal states
    
    rrt_s = RRT( num_samples/2, # Tree growing from the initial state
                 dims,
                 step_length,
                 pe.lims,
                 connect_prob,
                 collision_func = pe.test_collisions )
                 
    rrt_g = RRT( num_samples/2, # Tree growing from the goal state
                 dims,
                 step_length,
                 pe.lims,
                 connect_prob,
                 collision_func = pe.test_collisions )
                 
    if _RANDSTAT:
        pe.start = rrt_s.get_random_state()
        pe.goal  = rrt_g.get_random_state()
    
    rrt_s.init_tree( pe.start )
    rrt_g.init_tree( pe.goal  )
        
    pathFound = False # Signal for when the two trees have become connected
    
    # Store references to the two trees in a list.  
    # We will be switching between them each time a tree attempts an extension without reaching the goal
    flipFlop = [ rrt_s , rrt_g ]
    
    index = 0
    samples = 0
    connecting = False
    reachBool = False
    
    lastAdded = rrt_g.T.nodes[0]
    if _DEBUG:
        print "Only node at goal" , rrt_g.T.nodes[0] , len( rrt_g.T.nodes )
    plan = None    


    while not pathFound and samples < num_samples:
        # Toggle the growing and goal trees
        flip = elemw( index   , flipFlop ) # Growing tree
        flop = elemw( index+1 , flipFlop ) # Goal tree
        
        if _DEBUG:
            print "Flip" , id( flip ) , "Flop" , id( flop )

        if random() < connect_prob:
            connecting = True
            target = lastAdded.state
        else:
            target = vec_random_limits( len( flip.limits ) , flip.limits )
            connecting = False
            samples += 1
            
        result , lastAdded , reachBool = flip.extend_connect_bidir( target )
        
        if _DEBUG:
            print "Last Added:", lastAdded
        
        if result: # if the result is True or a list
            if connecting and reachBool: # The result is true
                # If we are in a connecting phase and the target was reached, 
                pathFound = True # then the trees are connected
                
                if _DEBUG:
                    print "Last Added:" , lastAdded , ',' , "Target:" , target
                
                # The plan will consist of 
                # A. the path from the growing tree root to the target
                growPath = flip.T.get_back_path( lastAdded )
                if _DEBUG:
                    print "Grow Path:" , growPath
                # B. The path from the last node added to the goal tree root
                goalPath = flop.T.get_back_path( flop.T.search_by_state( target ) )
                if _DEBUG:
                    print "Goal Path:" , goalPath
                
                # One of these needs to be reversed
                if id(flip) == id(rrt_s): # flip is the start tree , flop is the goal tree
                    plan = growPath
                    goalPath.reverse()
                    plan.extend( goalPath )
                else: # flop is the start tree , flip is the goal tree
                    plan = goalPath
                    growPath.reverse()
                    plan.extend( growPath )
                    
            samples += 1
        
        index += 1 # flip / flop
     
    run_time = time.time() - start_time
        
    accum_print( "Init State:" , rrt_s.init )
    accum_print( "Goal State:" , rrt_g.init )
    accum_print( 'plan:', plan )
    if plan:
        accum_print( 'plan length:' , len( plan ) )
    accum_print( 'run_time =', run_time )
    accum_print( "Number of Nodes:" , len( rrt_s.T.nodes ) + len( rrt_g.T.nodes ) )
    if _GRAPHGEN:
        plotter.close( 'all' )
        pe.draw_plan_bidir( plan , rrt_s , rrt_g , figPath , dynamic_plan=False )
    accum_print( "END" )
        
    return plan, rrt_s , rrt_g

## === 2004 _ Augmenting RRT w Local Trees _ Strandberg ====================================================================================

def test_rrt_locTrees( figPath , num_samples = 500 , step_length = 2 , env = 'env0.txt' , connect = False , connect_prob = 0.05 ,
                       N_loc_lim = 4 , p_grow = 0.12 ):
    """ Search for a path with RRTLocTrees """
    
    print "STARTED"    
    
    # 0. Init environment variables
    
    pe = PolygonEnvironment()
    pe.read_env( env )
    dims = len( pe.start )
    start_time = time.time()
    
    # 1. Init RRT problems for both the start and goal states
    
    rrt_lt = RRTLocTrees( num_samples , # Tree growing from the initial state
                          dims,
                          step_length,
                          pe.lims,
                          connect_prob,
                          collision_func = pe.test_collisions ,
                          N_loc_lim = N_loc_lim , 
                          p_grow    = p_grow )
    
    if _RANDSTAT:
        pe.start = rrt_lt.get_random_state()
        pe.goal  = rrt_lt.get_random_state()
        
    plan = rrt_lt.RRTLocTreesF( pe.start , pe.goal )
     
    run_time = time.time() - start_time
        
    accum_print( "Init State:" , rrt_lt.init )
    accum_print( "Goal State:" , rrt_lt.goal )
    accum_print( 'plan:', plan )
    if plan:
        accum_print( 'plan length:' , len( plan ) )
    accum_print( 'run_time =', run_time )
    allNodes = rrt_lt.get_all_nodes()
    accum_print( "Number of Nodes:" , len( allNodes ) )
    if _GRAPHGEN:
        plotter.close( 'all' )
        pe.draw_plan( plan , rrt_lt , figPath , dynamic_plan = False )
    accum_print( "END" )
        
    return plan , rrt_lt

## ___ End Strandberg ______________________________________________________________________________________________________________________

def merge_test():
    
    sep( "MERGE TEST" )
    
    R1 = RRT( 500 )
    R2 = RRT( 500 )
    
    R1.init_tree( [ 0,0 ] )
    R2.init_tree( [ 1,0 ] )
    
    R1.T.add_q_to_closest_parent( [ 0.25 , 0 ] )
    R1.T.add_q_to_closest_parent( [ 0.44 , 0 ] )
    
    R2.T.add_q_to_closest_parent( [ 0.75 , 0 ] )
    R2.T.add_q_to_closest_parent( [ 0.66 , 0 ] )
    
    print "R1 has" , len( R1.T.nodes ) , "nodes"
    print "R2 has" , len( R2.T.nodes ) , "nodes"
    
    goal = R2.T.root
    middle = [ 0.5 , 0.0 ]
    
    R1near = R1.get_closest( middle )
    R2near = R2.get_closest( middle )
    
    print "R1 nearest middle" , R1near.state
    print "R2 nearest middle" , R2near.state
    
    print "Merging ..."
    R1.Merge( R1near , R2 , R2near )
    print "R1 has" , len( R1.T.nodes ) , "nodes"
    print "R2 has" , len( R2.T.nodes ) , "nodes"
    
    print "R1 has" , len( R1.T.edges ) , "edges"
    print "R2 has" , len( R2.T.edges ) , "edges"
    
    print "Path back:" , endl , R1.T.get_back_path( goal )

    print "R1 edges" , endl , 
    for edge in R1.T.edges:
        print edge

# === End RRT ===
    



_GRAPHGEN = True # Set to true to generate graphics
_RANDSTAT = False # Set to true for random start and end states
_DEBUG    = False
_SIMALL   = False

if __name__ == '__main__':
    
    # === NEW ASM Tests ====================================================================================================================
    
    # Testing on Environment 1
    
    if 1:
    
        if 0 or _SIMALL: 
            accum_sep( "Bidir Connect" )
            test_rrt_bidir( "ASM_Bidir_Connect.eps" , num_samples = 500 , step_length = 0.15 , env = 'env1.txt' , connect = False , connect_prob = 0.05 )
            accum_out_and_clear( "ASM_Bidir_Connect_Output.txt" )
            
        if 1 or _SIMALL: 
            accum_sep( "RRTLocTrees" )
            test_rrt_locTrees( "ASM_LocTrees.eps" , num_samples = 750 , step_length = 0.15 , env = 'env1.txt' , connect = False , connect_prob = 0.05 ,
                               N_loc_lim = 6 , p_grow = 0.05 )
            accum_out_and_clear( "ASM_LocTrees.txt" )
            
    else:
        merge_test() # Merge works, that's not the problem
    
    # ___ END ASM Tests ____________________________________________________________________________________________________________________
    
    
    # === OLD Homework Tests ===============================================================================================================
    
    _GRAPHGEN = False # Set to true to generate graphics
    _RANDSTAT = False # Set to true for random start and end states
    _DEBUG    = False
    _SIMALL   = False
    
    _RANDSTAT = False # Use the text file start and end states    
    
    if False or _SIMALL:
        accum_sep("Problem 1.1.a")
        test_rrt_env( "p_1-1-a.eps" , num_samples = 500 , step_length = 2 , env = 'env0.txt' , connect = False , connect_prob = 0.05 )
        accum_out_and_clear( "Problem_1-1-a_Output.txt" )
        
    if False or _SIMALL:
        accum_sep("Problem 1.1.b")
        test_rrt_env( "p_1-1-b.eps" , num_samples = 6000 , step_length = 0.15 , env = 'env1.txt' , connect = False , connect_prob = 0.05 )
        accum_out_and_clear( "Problem_1-1-b_Output.txt" )
      
    _RANDSTAT = True # Use random start and end states      
      
    if False or _SIMALL:
        for item in [ 'c' , 'd' , 'e' ]:
            accum_sep( "Problem 1.1." + item )
            test_rrt_env( "p_1-1-" + item + ".eps" , num_samples = 1000 , step_length = 2 , env = 'env0.txt' , connect = False , connect_prob = 0.05 )
            accum_out_and_clear( "Problem_1-1-" + item + "_Output.txt" )
            
    if False or _SIMALL:
        for item in [ 'f' , 'g' , 'h' ]:
            accum_sep( "Problem 1.1." + item )
            test_rrt_env( "p_1-1-" + item + ".eps" , num_samples = 12000 , step_length = 0.15 , env = 'env1.txt' , connect = False , connect_prob = 0.05 )
            accum_out_and_clear( "Problem_1-1-" + item + "_Output.txt" )
    
    _RANDSTAT = False # Use the text file start and end states    
    
    if False or _SIMALL:
        biases = [ 0.0 , 0.05 , 0.1 , 0.5 ]
        parts  = [ 'a' ,  'b' , 'c' , 'd' ]
        for i in range( len( parts ) ):
            accum_sep( "Problem 1.2." + parts[i] )
            test_rrt_env( "p_1-2-" + parts[i] + ".eps" , 
                          num_samples = 2000 , step_length = 2 , env = 'env0.txt' , connect = False , connect_prob = biases[i] )
            accum_out_and_clear( "Problem_1-2-" + parts[i] + "_Output.txt" )
            
    if False or _SIMALL:
        accum_sep( "Problem 1.3.a" )
        test_rrt_env( "p_1-3-a.eps" , num_samples = 500 , step_length = 2 , env = 'env0.txt' , connect = True , connect_prob = 0.05 )
        accum_out_and_clear( "Problem_1-3-a_Output.txt" )
        
    if False or _SIMALL:
        accum_sep( "Problem 1.3.b" )
        test_rrt_env( "p_1-3-b.eps" , num_samples = 5000 , step_length = 0.15 , env = 'env1.txt' , connect = True , connect_prob = 0.05 )
        accum_out_and_clear( "Problem_1-3-b_Output.txt" )
        
    if False or _SIMALL: # NOTE: Run this until it returns a plan
        accum_sep( "Problem 1.4.a" )
        test_rrt_bidir( "p_1-4-a.eps" , num_samples = 500 , step_length = 2 , env = 'env0.txt' , connect = False , connect_prob = 0.05 )
        accum_out_and_clear( "Problem_1-4-a_Output.txt" )
        
    if False or _SIMALL: # NOTE: Run this until it returns a plan
        accum_sep( "Problem 1.4.b" )
        test_rrt_bidir( "p_1-4-b.eps" , num_samples = 500 , step_length = 0.15 , env = 'env1.txt' , connect = False , connect_prob = 0.05 )
        accum_out_and_clear( "Problem_1-4-b_Output.txt" )
        
    # ___ END Homework Tests _______________________________________________________________________________________________________________
