#!/usr/bin/env python
# -*- coding: utf-8 -*-

# ~~ Future First ~~
from __future__ import division # Future imports must be called before everything else, including triple-quote docs!

__progname__ = "circle_test_w_lbr4.py"
__version__  = "2018.06" 
"""
James Watson , Template Version: 2018-05-30
Built on Spyder for Python 2.7

Request IK for a circular path, then send that to a simulation of the LBR4

Dependencies: numpy
"""


"""  
~~~ Developmnent Plan ~~~
[ ] ITEM1
[ ] ITEM2
"""

# === Init Environment =====================================================================================================================
# ~~~ Prepare Paths ~~~
import sys, os.path
SOURCEDIR = os.path.dirname( os.path.abspath( __file__ ) ) # URL, dir containing source file: http://stackoverflow.com/a/7783326
PARENTDIR = os.path.dirname( SOURCEDIR )
# ~~ Path Utilities ~~
def prepend_dir_to_path( pathName ): sys.path.insert( 0 , pathName ) # Might need this to fetch a lib in a parent directory
def rel_to_abs_path( relativePath ): return os.path.join( SOURCEDIR , relativePath ) # Return an absolute path , given the 'relativePath'

# ~~~ Imports ~~~
# ~~ Standard ~~
from math import pi , sqrt , sin
from random import random
# ~~ Special ~~
import numpy as np
import rospy 
# ~ IK SOlver ~
from trac_ik_python.trac_ik import IK
# ~ ROS Messages ~
from geometry_msgs.msg import Pose # --- For receiving the effector pose
from sensor_msgs.msg import JointState # For sending the joint angles
from std_msgs.msg import Header
# ~~ Local ~~
# ~ Package Messages ~
from motion_cost.msg import IKrequest #- Message contains desired pose and possibly a seed
from motion_cost.msg import IKresponse # Message contains success code and solved joint angles
# ~ Package Services ~
from motion_cost.srv import qFromPose # IK Service
# ~ MARCHHARE ~
from marchhare.marchhare import sep , elemw
from marchhare.VectorMath.Vector3D import circle_arc_3D

# ~~ Constants , Shortcuts , Aliases ~~
EPSILON = 1e-7
infty   = 1e309 # URL: http://stackoverflow.com/questions/1628026/python-infinity-any-caveats#comment31860436_1628026
endl    = os.linesep

# ~~ Script Signature ~~
def __prog_signature__(): return __progname__ + " , Version " + __version__ # Return a string representing program name and verions

# ___ End Init _____________________________________________________________________________________________________________________________


# === Main Application =====================================================================================================================

# = Program Vars =

_ALL_JOINT_NAMES = [ 'lbr4_j0' , 'lbr4_j1' , 'lbr4_j2' , 'lbr4_j3' , 'lbr4_j4' , 'lbr4_j5' , 'lbr4_j6' , 
                     'r_gripper_l_finger_joint' , 'r_gripper_r_finger_joint' ]
_NUM_ALL_JOINTS = len( _ALL_JOINT_NAMES )

# _ End Vars _


# = Program Functions =

def lbr4_pos_only( q_0to6 ):
    """ Compose a joint command for the LBR4 """    
    SHOWDEBUG = False
    rtnMsg = JointState()
    # rtnMsg.header = # 2018-05-30: Ignoring the header for now, see if it works
    rtnMsg.name = _ALL_JOINT_NAMES
    rtnMsg.position = [0] * _NUM_ALL_JOINTS
    rtnMsg.velocity = [0] * _NUM_ALL_JOINTS
    rtnMsg.effort   = [0] * _NUM_ALL_JOINTS
    if len( q_0to6 ) > 7: # Truncate q if it has too many entries
        q_0to6 = list( q_0to6[0:6] )
    # If q was too short, elements will be assigned to joints in order starting from 0
    rtnMsg.position[ 0 : len( q_0to6 ) ] = list( q_0to6 ) # Assign the actual KUKA config
    if SHOWDEBUG:
        print "Sent: " , rtnMsg.position    
    return rtnMsg

def lbr4_assign_fingers( JointStateMessage , fingList ):
    """ Change only the finger state in the message """
    # NOTE: This function assumes that the parallel gripper fingers are the last two elements of the joint state
    JointStateMessage.position[ _NUM_ALL_JOINTS-2 : _NUM_ALL_JOINTS ] = fingList[0:2]
    return JointStateMessage

def q_solver_client( req ):
    """ Fetch an IK solution from the service """
    # NOTE: you don't have to call rospy.init_node() to make calls against
    # a service. This is because service clients do not have to be
    # nodes.

    # block until the add_two_ints service is available
    # you can optionally specify a timeout
    rospy.wait_for_service( 'q_from_pose' )
    
    try:
        # create a handle to the add_two_ints service
        qSolve = rospy.ServiceProxy( 'q_from_pose' , qFromPose )
        
        # simplified style
        response = qSolve( req )

        return response.response
    
    except rospy.ServiceException , e:
        print "Service call failed: %s" %e

# _ End Func _


# = Program Classes =

class LBR_Circle_Node:
    # Simplest node possible for talking to the robot model
    
    def __init__( self ):
        """ Constructor, init KDL solvers and control connections """        
        
        SHOWDEBUG = True
        
        # 1. Start Node
        rospy.init_node( 'LBR_Circle_Node' ) # ----- Start node with the given handle , Must be unique in the session or old will be stopped
        self.heartBeatHz = 30 # -------------------- Refresh rate for this node
        self.seqSat = False 
        self.idle = rospy.Rate( self.heartBeatHz ) # Best effort to maintain 'heartBeatHz' , URL: http://wiki.ros.org/rospy/Overview/Time
        self.repeat = 3
        self.rePause = ( 1.0 / ( self.heartBeatHz * self.repeat ) ) * 0.75 # Pause between repeats
        self.solnValid = False
        
        # 2. Robot vars
        self.qDes = [ 0 ] * 7 # Joint angles , ( Config space position )
        self.qSln = [ 0 ] * 7 # Joint angles , ( Config space position )
        self.theta = 0
        self.dTheta = ( 2*pi ) / ( self.heartBeatHz * 5 )
        
        # 2. Start Publishers
        self.joint_cmd_pub = rospy.Publisher( '/lbr4_teleop/joint_cmd' , JointState , queue_size = 5 )
        self.lbr_IKreq_pub = rospy.Publisher( '/ASM/LBR4_IK_reqs' , IKrequest , queue_size = 5 )
        # USAGE: self.joint_cmd_pub.publish( msg )  # where 'msg' is a 'JointState' message
        
        # 3. Calc waypoints
        self.waypoints = circle_arc_3D( [ 0.0 , 0.0 , 1.0 ] , # Axis
                                        [ 0.795 , 0.0 , 0.59 ] , # Center
                                        0.305 , # Radius
                                        [ 1.0 , 0.0 , 0.0 ] , # Theta = 0 line
                                        2*pi , # arc length
                                        int( 2*pi / self.dTheta ) ) # number of samples
        if SHOWDEBUG:
            print "Waypoints: " , endl , self.waypoints
        
    def send_q( self , q , repeat = 10 , storeQ = True ):
        """ Send a joint state to the sim robot, 'repeat' the command so that we know it gets the message """
        # NOTE: Setting 'repeat' <= 0 will result in no messages being sent
        if storeQ:
            self.q = q[:]
        for i in xrange( repeat ):
            self.joint_cmd_pub.publish(  lbr4_pos_only( q )  )
            
    def send_q_and_fingers( self , q , fingList , repeat = 10 , storeQ = True ):
        """ Send a joint state to the sim robot, 'repeat' the command so that we know it gets the message """
        # NOTE: Setting 'repeat' <= 0 will result in no messages being sent
        if storeQ:
            self.q = q[:]
        for i in xrange( repeat ):
            msg = lbr4_pos_only( q )
            lbr4_assign_fingers( msg , fingList )
            self.joint_cmd_pub.publish(  msg  )
        
    def run( self ):
        """ Try to maintain a circular path for the robot """
        
        SHOWDEBUG = True
        
        i = 0
        message = IKrequest()
        message.header = Header()
        message.header.frame_id = "world"
        message.seed.position = [ 0 ] * 7
        
        self.theta = 0
        
        while not rospy.is_shutdown():
            
            # 1. Increment
            i += 1
            pos = elemw( self.waypoints , i )
            message.sequence = i
            self.theta += self.dTheta
            
            # 2. Load the position
            message.worldPose.position.x    = pos[0]
            message.worldPose.position.y    = pos[1]
            message.worldPose.position.z    = pos[2]
            
            message.worldPose.orientation.w =  0.707
            message.worldPose.orientation.x =  0.0
            message.worldPose.orientation.y =  0.0
            message.worldPose.orientation.z = -0.707
            
            # 3. Request a solution
            soln = q_solver_client( message )
            
            if SHOWDEBUG:
                print "Solution:",endl
                print soln
                print endl
            
            # 4. If it is a valid solution, send to the robot
            if soln.valid:
                self.send_q_and_fingers( 
                    soln.qSoln.position ,
                    [ -0.006 + abs( 0.04 * sin( self.theta / 2.5 ) ) , 
                      -0.006 + abs( 0.04 * sin( self.theta / 2.5 ) ) ]
                )
                message.seed.position = soln.qSoln.position
            elif SHOWDEBUG:
                print "Solution was invalid!"
            
            self.idle.sleep()
        

# _ End Classes _

if __name__ == "__main__":
    print __prog_signature__()
    termArgs = sys.argv[1:] # Terminal arguments , if they exist
    
    circNode = LBR_Circle_Node()
    circNode.run()

# ___ End Main _____________________________________________________________________________________________________________________________


# === Spare Parts ==========================================================================================================================



# ___ End Spare ____________________________________________________________________________________________________________________________
