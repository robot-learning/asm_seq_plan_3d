#!/usr/bin/env python
# -*- coding: utf-8 -*-

# ~~ Future First ~~
from __future__ import division # Future imports must be called before everything else, including triple-quote docs!

__progname__ = "test_IK_serv.py"
__version__  = "2018.06"
"""
James Watson , Template Version: 2018-05-30
Built on Spyder for Python 2.7

Send a few IK requests to the service and see what happens!

Dependencies: numpy
"""


"""  
~~~ Developmnent Plan ~~~
[ ] ITEM1
[ ] ITEM2
"""

# === Init Environment =====================================================================================================================
# ~~~ Prepare Paths ~~~
import sys, os.path
SOURCEDIR = os.path.dirname( os.path.abspath( __file__ ) ) # URL, dir containing source file: http://stackoverflow.com/a/7783326
PARENTDIR = os.path.dirname( SOURCEDIR )
# ~~ Path Utilities ~~
def prepend_dir_to_path( pathName ): sys.path.insert( 0 , pathName ) # Might need this to fetch a lib in a parent directory
def rel_to_abs_path( relativePath ): return os.path.join( SOURCEDIR , relativePath ) # Return an absolute path , given the 'relativePath'

# ~~~ Imports ~~~
# ~~ Standard ~~
from math import pi , sqrt
from random import random
# ~~ Special ~~
import numpy as np
import rospy 
# ~ IK SOlver ~
from trac_ik_python.trac_ik import IK
# ~ ROS Messages ~
from geometry_msgs.msg import Pose # --- For receiving the effector pose
from sensor_msgs.msg import JointState # For sending the joint angles
from std_msgs.msg import Header
# ~~ Local ~~
# ~ Package Messages ~
from motion_cost.msg import IKrequest #- Message contains desired pose and possibly a seed
from motion_cost.msg import IKresponse # Message contains success code and solved joint angles
# ~ Package Services ~
from motion_cost.srv import qFromPose # IK Service
# ~ MARCHHARE ~
from marchhare.marchhare import sep

# ~~ Constants , Shortcuts , Aliases ~~
EPSILON = 1e-7
infty   = 1e309 # URL: http://stackoverflow.com/questions/1628026/python-infinity-any-caveats#comment31860436_1628026
endl    = os.linesep

# ~~ Script Signature ~~
def __prog_signature__(): return __progname__ + " , Version " + __version__ # Return a string representing program name and verions

# ___ End Init _____________________________________________________________________________________________________________________________


# === Main Application =====================================================================================================================

# = Program Vars =

_DISPLAY_RATE = 50
_ALL_JOINT_NAMES = [ 'lbr4_j0' , 'lbr4_j1' , 'lbr4_j2' , 'lbr4_j3' , 'lbr4_j4' , 'lbr4_j5' , 'lbr4_j6' , 
                     'r_gripper_l_finger_joint' , 'r_gripper_r_finger_joint' ]
_NUM_ALL_JOINTS = len( _ALL_JOINT_NAMES )

# _ End Vars _


# = Program Functions =

def print_joint_state( msg ):
    """ Display the contents of a joint message to the screen """
    print "Header:_ " , msg.header
    print "Names: _ " , msg.name
    print "Position:" , msg.position
    print "Velocity:" , msg.velocity
    print "Effort:_ " , msg.effort

def slice_joint_state(msg,bgn,end): # [4:11] to extract the arm from "/lbr4_allegro/joint_states" topic
    """ Return a slice of a message that contains only the indices that we want """
    rtnMsg = JointState()
    rtnMsg.header = msg.header
    rtnMsg.name = msg.name[bgn:end]
    rtnMsg.position = msg.position[bgn:end]
    rtnMsg.velocity = msg.velocity[bgn:end]
    rtnMsg.effort = msg.effort[bgn:end]
    return rtnMsg

def concat_joint_state(state1, state2):
    """ Return a JointState message that is the concatenation of 'state1' and 'state2' """
    rtnMsg = JointState()
    rtnMsg.header = state1.header # Assume that the header information still applies to the concatenated states
    #                  state1                                    state2
    rtnMsg.name.extend(state1.name) ;         rtnMsg.name.extend(state2.name) # URL, extend list: http://www.tutorialspoint.com/python/list_extend.htm
    rtnMsg.position.extend(state1.position) ; rtnMsg.position.extend(state2.position)
    rtnMsg.velocity.extend(state1.velocity) ; rtnMsg.velocity.extend(state2.velocity)
    rtnMsg.effort.extend(state1.effort) ;     rtnMsg.effort.extend(state2.effort)
    return rtnMsg

def lbr4_pos_only( q_0to6 ):
    """ Compose a joint command for the LBR4 """
    
    SHOWDEBUG = True
    
    rtnMsg = JointState()
    # rtnMsg.header = # 2018-05-30: Ignoring the header for now, see if it works
    rtnMsg.name = _ALL_JOINT_NAMES
    rtnMsg.position = [0] * _NUM_ALL_JOINTS
    rtnMsg.velocity = [0] * _NUM_ALL_JOINTS
    rtnMsg.effort   = [0] * _NUM_ALL_JOINTS
    if len( q_0to6 ) > 7: # Truncate q if it has too many entries
        q_0to6 = list( q_0to6[0:6] )
    # If q was too short, elements will be assigned to joints in order starting from 0
    rtnMsg.position[ 0 : len( q_0to6 ) ] = list( q_0to6 ) # Assign the actual KUKA config
    
    if SHOWDEBUG:
        print "Sent: " , rtnMsg.position
    
    return rtnMsg

def q_solver_client( req ):

    # NOTE: you don't have to call rospy.init_node() to make calls against
    # a service. This is because service clients do not have to be
    # nodes.

    # block until the add_two_ints service is available
    # you can optionally specify a timeout
    rospy.wait_for_service( 'q_from_pose' )
    
    try:
        # create a handle to the add_two_ints service
        qSolve = rospy.ServiceProxy( 'q_from_pose' , qFromPose )
        
        # simplified style
        response = qSolve( req )

        return response
    
    except rospy.ServiceException , e:
        print "Service call failed: %s" %e

# _ End Func _


# = Program Classes =

class LBR_Command_Node:
    # Simplest node possible for talking to the robot model
    
    def __init__( self ):
        """ Constructor, init KDL solvers and control connections """        
        
        # 1. Start Node
        rospy.init_node( 'LBR_CTRL_Node' )
        self.heartBeatHz = 100
        self.jointState = None # Holds the joint state of the entire LBR4 + Allegro robot
        self.gotFirstState = False # Flag that tells whether the model can start reading state information
        self.idle = rospy.Rate( self.heartBeatHz ) # Best effort to maintain 'heartBeatHz' , URL: http://wiki.ros.org/rospy/Overview/Time
        
        # 2. Robot vars
        self.q = [ 0 ] * 7
        self.dq = 0.01745 # 1 deg
        
        # 2. Start Publishers
        self.joint_cmd_pub = rospy.Publisher( '/lbr4_teleop/joint_cmd' , JointState , queue_size = 5 )
        # USAGE: self.joint_cmd_pub.publish( msg )  # where 'msg' is a 'JointState' message

    def increment( self ):
        """ Silly Demo: Move all of the joints by 'dq' """
        # NOTE: This function does not check joint limits
        for i in xrange( len( self.q ) ):
            self.q[i] += self.dq
        print "Sending:" , self.q
        self.joint_cmd_pub.publish(  lbr4_pos_only( self.q )  )
        
    def send_q( self , q , repeat = 10 , storeQ = True ):
        """ Send a joint state to the sim robot, 'repeat' the command so that we know it gets the message """
        # NOTE: Setting 'repeat' <= 0 will result in no messages being sent
        if storeQ:
            self.q = q[:]
        for i in xrange( repeat ):
            self.joint_cmd_pub.publish(  lbr4_pos_only( q )  )
                
    def run( self , q = [0,0,0,0,0,0,0] ):
        # Standalone demo , Infinitely spin all joints without regards to limits
        while not rospy.is_shutdown():
            if False: # Silly spin test
                self.increment() # Change all the LBR4 joints
            else: # Config manually copied from Trac_IK
                self.send_q( q , repeat = 1 )
            self.idle.sleep()

# _ End Classes _

if __name__ == "__main__":
    print __prog_signature__()
    termArgs = sys.argv[1:] # Terminal arguments , if they exist
    
    rospy.wait_for_service( 'q_from_pose' )
    
    SHOWDEBUG = True
    
    counter = 0
    requester = rospy.Publisher( '/ASM/LBR4_IK_reqs' , IKrequest , queue_size = 5 )
    
    message = IKrequest()
    
    
    sep( "1. Reachable" )
    message.header = Header()
    message.header.frame_id = "world"
    
    message.worldPose.position.x    =  1.1
    message.worldPose.position.y    = -0.456
    message.worldPose.position.z    =  0.59
    
    message.worldPose.orientation.w = 1.0
    message.worldPose.orientation.x = 0.0
    message.worldPose.orientation.y = 0.0
    message.worldPose.orientation.z = 0.0
    
    message.seed.position = [ 0 ] * 7

    counter += 1
    message.sequence = counter
    
    if SHOWDEBUG:
        print "Sending request ..."
        print message
        print
    print "Waiting on solution ..." , endl
    solnResp = q_solver_client( message ).response # It's just responses all the way down!
    print solnResp , endl 
    print "Solution returned!" , endl , endl
        
    print "Sending to robot" , endl , endl
    node = LBR_Command_Node()
    node.run( solnResp.qSoln.position )
    

# ___ End Main _____________________________________________________________________________________________________________________________


# === Spare Parts ==========================================================================================================================



# ___ End Spare ____________________________________________________________________________________________________________________________
