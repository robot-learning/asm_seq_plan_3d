#!/usr/bin/env python
# -*- coding: utf-8 -*-

# ~~ Future First ~~
from __future__ import division # Future imports must be called before everything else, including triple-quote docs!

__progname__ = "eval_seq_parallelism.py"
__version__  = "2018.08"
"""
James Watson , Template Version: 2018-05-30
Built on Spyder for Python 2.7

Try assigning assemblies to robots

Dependencies: numpy
"""


"""  
~~~ Developmnent Plan ~~~
[ ] Node class
    [ ] Move Node class to "Graph.py"
"""

# === Init Environment =====================================================================================================================
# ~~~ Prepare Paths ~~~
import sys, os.path
SOURCEDIR = os.path.dirname( os.path.abspath( __file__ ) ) # URL, dir containing source file: http://stackoverflow.com/a/7783326
PARENTDIR = os.path.dirname( SOURCEDIR )
# ~~ Path Utilities ~~
def prepend_dir_to_path( pathName ): sys.path.insert( 0 , pathName ) # Might need this to fetch a lib in a parent directory
def rel_to_abs_path( relativePath ): return os.path.join( SOURCEDIR , relativePath ) # Return an absolute path , given the 'relativePath'

# ~~~ Imports ~~~
# ~~ Standard ~~
#from math import pi , sqrt
# ~~ Special ~~
#import numpy as np
# ~~ Local ~~
from marchhare.marchhare import PriorityQueue , Stack , sep , accum , nowTimeStamp , lists_as_columns_with_titles
from marchhare.Graph import Graph , Node

# ~~ Constants , Shortcuts , Aliases ~~
EPSILON = 1e-7
infty   = 1e309 # URL: http://stackoverflow.com/questions/1628026/python-infinity-any-caveats#comment31860436_1628026
endl    = os.linesep

# ~~ Script Signature ~~
def __prog_signature__(): return __progname__ + " , Version " + __version__ # Return a string representing program name and verions

# ___ End Init _____________________________________________________________________________________________________________________________


# === Main Application =====================================================================================================================

# = Program Vars =



# _ End Vars _


# = Program Functions =

def deepest_goal_node( initNode , is_goal ):
    """ Exhaustive search of all nodes , Return the deepest node that satisfies 'goalFunc' """
    # NOTE: Search is exhaustive , Could be expensive depending on the problem
    # NOTE: This function assumes that a particular node cannot be reached at a depth deeper than it is first encountered
    frontier = Stack() # Search stack, pop from the top
    n0 = initNode
    n0.path = [] # Attach a sequence to each node to record the path to it, this may consume a lot of space for large problems
    n0.depth = 0
    frontier.push( n0 ) # Push the starting node on the frontier
    visited = set([]) 
    winners = PriorityQueue() # Used to sort goal nodes by depth
    while len( frontier ) > 0: # While states remain on the frontier
        n_i = frontier.pop() # Pop last element
        if n_i.tag not in visited: # If the state has not been previously popped, then
            visited.add( n_i.tag ) # Log the visit
            currentPath = n_i.path # Path to the current state
            if is_goal( n_i ): # If the goal has been reached, return path and visited information
                #return ( n_i , n_i.path + [ n_i.tag ] , visited )
                winners.push( n_i , - n_i.depth )
            else: # else is not goal
                for edge in n_i.edges:
                    s_prime = edge
                    s_prime.path = currentPath[:] + [ n_i.tag ] # Assemble a plan that is the path so far plus the current action
                    s_prime.depth = n_i.depth + 1
                    frontier.push( s_prime ) # Push onto top of the frontier                        
    #return ( None , None , visited ) # The frontier has been exhausted without finding the goal, return the sad story of the journey    
    if len( winners ) > 0:
        rtnNode , rtnDepth = winners.pop_with_priority()
        return ( rtnNode , -rtnDepth )
    else:
        return ( None , 0 )

def is_assembly_complete( design ):
    """ Return True if all of the nodes of 'design' have been completed , Otherwise return False """
    for node in design.nodes:
        if not node.complete:
            return False
    return True

def assembly_report( design ):
    """ Print the status of each of the subs """
    sep( "Assembly Report" , 3 , '*' )
    for node in design.nodes:
        if node.__class__.__name__ != "Part":
            print node.best_ID() , ", Complete?:" , node.complete , ", Assigned?:" , node.assigned

# _ End Func _


# = Program Classes =

class Part( Node ):
    """ Simplest possible part representation """
    
    def __init__( self , name , ID ):
        """ Create a part with name and ID """
        Node.__init__( self )
        self.give_alias( name ) 
        self.ID = ID
        self.complete = True # A part does not need work to be complete
        self.assigned = False
        
    def is_primary( self ):
        """ Return true if every member is of type 'Part' """
        return False
    
    def all_subs_completed( self ):
        """ Return True if all subs under this one have been completed and are available to assemble """
        return False
    
    def __str__( self ):
        """ String representation of the part """
        return "{ " + self.__class__.__name__ + ": " + self.alias + " }" 
    
    def print_status( self ):
        """ Print identification and status w.r.t. the assembly process """
        print self , ", Complete?:" , self.complete , ", Assigned?:" , self.assigned
        
    def reset_unbuilt( self ):
        """ This has not effect for a part """
        pass

class Subassembly( Node ):
    """ Simplest possible subasm representation 
    complete: Assembled and available to put in a parent sub
    assigned: Given to a robot and unavailable to assign or build """
    
    def __init__( self , design , name ):
        """ Creat an assembly with members and actions """
        Node.__init__( self , design , name )
        self.actions  = 0
        self.base     = None
        self.complete = False # A subassembly needs work to be complete
        self.assigned = False
        
    def __str__( self ):
        """ Return a string representation of the Graph """
        rtnStr = ""
        rtnStr += "Subassembly@" + str( id( self ) ) + " with " + str( len( self.edges ) ) + " edges , " + \
            str( self.alias if self.alias else self.tag ) + " , Contents: " + endl
        for edge in self.edges:
            rtnStr += "\t" + edge.__class__.__name__ + " ___ " + str( edge.alias if edge.alias else edge.tag ) + endl
        return rtnStr
        
    def add_base_part( self , name , ID ):
        """ Create and add part, without action """
        temp = Part( name , ID )
        self.graph.add_node_by_ref( temp )
        self.base = temp
        self.add_child( temp )
        
    def add_part( self , name , ID ):
        """ Create and add part, with action """
        temp = Part( name , ID )
        self.graph.add_node_by_ref( temp )
        self.actions += 1
        self.add_child( temp )
        
    def add_base_sub( self , subRef ):
        """ Add subassembly , without action """
        self.graph.add_node_by_ref( subRef )
        self.base = subRef 
        self.add_child( subRef )
        
    def add_sub( self , subRef ):
        """ Add subassembly , with action """
        self.graph.add_node_by_ref( subRef )
        self.actions += 1
        self.add_child( subRef )
        
    def is_primary( self ):
        """ Return true if every member is of type 'Part' """
        if self.edges > 0:
            for prt in self.edges:
                if prt.__class__.__name__ != "Part":
                    return False
            return True
        else: 
            return False
        
    def all_subs_completed( self ):
        """ Return True if all subs under this one have been completed and are available to assemble """
        if self.edges > 0:
            for prt in self.edges:
                if not prt.complete:
                    return False
            return True
        else: 
            return False     
        
    def print_status( self ):
        """ Print identification and status w.r.t. the assembly process """
        print self , ", Complete?:" , self.complete , ", Assigned?:" , self.assigned    

    def reset_unbuilt( self ):
        """ Set this sub and all constituent subs to unbuilt """
        self.complete = False # A subassembly needs work to be complete
        self.assigned = False
        for edge in self.edges:
            edge.reset_unbuilt()

class AsmRobot:
    """ Class to represent robot on an assembly line """
    
    totalInstance = 0 # Number of robots created
    
    def __init__( self ):
        """ Create a robot to consume assembly tasks """
        self.__class__.totalInstance += 1
        self.ID = self.__class__.totalInstance
        self.job = None
        self.remainingTicks = 0
        self.workStatus = 0 # 0: IDLE , 1: BUSY
        self.history = [] # Work history
        self.statusSeq = [] # Record of status at each timestep
        
    def give_job( self , subNode ):
        """ Assign a job to this robot """
        if ( not subNode.complete ) and ( not subNode.assigned ):
            # If the sub has not been completed , then
            subNode.assigned = True # Lock sub
            self.job = subNode # Assign job
            self.remainingTicks = subNode.num_successors() - 1 # Determine number of timesteps that robot will be busy
            self.workStatus = 1 # Set to busy
        else:
            # If the sub has been completed , then there is nothing for the robot to do
            self.job = None # No job was really assigned
            self.remainingTicks = 0 # There are no busy timesteps to count down from
            self.workStatus = 0 # 0: IDLE , 1: BUSY     
            
    def release_job( self ):
        """ Complete the sub currently assigned and return """
        if self.job:
            self.job.complete = True 
            self.job.assigned = False
            temp = self.job
            self.job = None
            return temp
        else:
            return None
            
    def tick( self ):
        """ Advance by one timestep """
        # NOTE: 'history' indicates the robot status at the END of each timestep
        #  0. Record whether the robot had a job at the beginning of the timestep
        self.statusSeq.append( self.workStatus )
        #  1. Create a history entry
        histEntry = { 'job': None , 'parts': 0 , 'remain': 0 , 'status': 0 }
        #  2. If there is a job in progress
        if self.workStatus:
            #  3. Set history data
            histEntry['job']    = self.job.best_ID()
            histEntry['parts']  = self.job.num_successors()
            #  4. Decrement remaining steps
            self.remainingTicks -= 1            
            histEntry['remain'] = self.remainingTicks
            #  5. If the last step was completed
            if self.remainingTicks <= 0:
                #  6. Release job
                self.release_job()
                #  7. Set idle
                self.workStatus = 0
        #  else there is no job in progress , Accept the default entry
        #  9. Record robot status
        histEntry['status'] = self.workStatus
        # 10. Append history
        self.history.append( histEntry )
        

class AsmDispatcher:
    """ Simulates an assembly process with finite robots and unit timesteps """
    
    """
    1. Every action takes unit time to accomplish: 1 tick
    2. Assigning / Switching jobs does not consume time
    3. Robot is idle if job ticks are consumed or there is no job assigned
    """
    
    def __init__( self , numBots = 1 ):
        self.workers = []
        self.task = None
        print "Creating robots ..."
        for i in xrange( numBots ):
            self.workers.append( AsmRobot() )
            print self.workers[i].ID
        print "There are" , len( self.workers ) , "workers"
        
    @staticmethod
    def is_deepest_unassigned( pNode ):
        """ Return True if 'pNode' represents a sub with no unbuilt children """
        #print "\t" , "IN 'is_deepest_unassigned'"
        #print "Working on node: " , pNode.tag , ":" , pNode.alias
        # 1. If Primary
        if pNode.is_primary():
            # 2. If Unassigned and Incomplete
            if ( not pNode.assigned ) and ( not pNode.complete ):
                # 3. Return True
                #print "Unassigned and incomplete"
                return True
            else:
                return False
        # 4. Else not primary
        else:
            # 5. If all subs completed
            if pNode.all_subs_completed():
                # 6. Return True
                return True
            # 7. Otherwise return false
            else:
                return False
        
    def fetch_deepest_unassigned( self , rootNode ):
        """ Fetch the deepest sub that has been neither built nor assgined """
        # Graph.dfs( self , initNode , is_goal ):
        #result = design.dfs( rootNode , self.is_deepest_unassigned )
        result = deepest_goal_node( rootNode , self.is_deepest_unassigned )
        #return result
        return result[0]
    
    def assign_design( self , design ):
        """ Assign an assembly task to the worker pool """
        self.task = design
        
    def get_worker_status( self ):
        """ Return a list for which each element is the working status of each worker """
        rtnLst = []
        for robot in self.workers:
            rtnLst.append( robot.workStatus )
        return rtnLst
        
    def assign_idle( self ):
        """ Assign jobs to idle robots """
        sep( "assign_idle" , 2 , '~' )
        # 1. For each robot
        for robDex , robot in enumerate( self.workers ):
            # 2. If the robot is idle (not busy)
            if not robot.workStatus:
                # 3. Find the deepest unassigned sub
                nextAvail = self.fetch_deepest_unassigned( self.task.get_root() )
                if nextAvail:
                    print robDex , "is idle, assign" , nextAvail.alias
                    # 4. Assign the sub to the idle robot
                    robot.give_job( nextAvail ) # This will also Set the assigned flag
                else:
                    print robDex , "is idle, but there is no available job to assign"
            else:
                print robDex , "has" , robot.remainingTicks , "steps to go"
        print
    
    def tick( self ):
        """ Advance sim by a unit time step """
        # 1. Assign jobs to idle robots
        self.assign_idle()        
        # 2. For each robot
        for robot in self.workers:
            # 3. Advance robot by one timestep
            robot.tick()

    def factory_reset( self ):
        """ Reset all robot states and erase history """
        # NOTE: This function will mark the currently-assigned job of each robot as completed
        # NOTE: Resetting the design in progress is the responsibility of the client code
        for robot in self.workers:
            robot.release_job() 
            robot.history = []
            robot.statusSeq = []
            
    def task_report_str( self ):
        """ Return a tabular report of robot work histories """
        headings = []
        workTabl = []
        # 0. For each robot
        for botDex , robot in enumerate( self.workers ):
            # 1. Accumulate job names that each robot worked for each timestep
            workTabl.append( [] )
            for entry in robot.history:
                workTabl[-1].append( entry['job'] )
            # 2. Generate heading
            headings.append( "Robot " + str( botDex + 1 ) )
        # 3. Submit table to the formatter and return
        return lists_as_columns_with_titles( headings , workTabl )
    
    def robot_utilization_str( self ):
        """ Report metrics on how the robots were utilized """
        idleTicks = []
        totlTicks = []
        rtnStr = ""
        # 1. For each robot
        for botDex , robot in enumerate( self.workers ):
            idleTicks.append( 0 )
            totlTicks.append( 0 )
            # 2. For each tick
            for entry in robot.statusSeq:        
                # 3. Accumulate total idle robot-ticks, per robot
                if not entry:
                    idleTicks[-1] += 1
                # 4. Accumulate total robot-ticks, per robot
                totlTicks[-1] += 1
            line = "Robot " + str( botDex + 1 ) + " : "
            # 5. Report total idle, per robot
            line += "Idle Ticks: " + str( idleTicks[-1] )
            line += " , Total Ticks: " + str( totlTicks[-1] )
            # 6. Report average idle, per robot
            line += " , Idle Fraction: " + str( idleTicks[-1] * 1.0 / totlTicks[-1] ) + endl
            print line ,
            rtnStr += line
        # 7. Report total idle
        totalIdle = sum( idleTicks )
        totalStep = sum( totlTicks )
        # 8. Report average idle
        line = "Total Idle Ticks: " + str( totalIdle ) + endl
        print line ,
        rtnStr += line
        line = "Total Idle Fraction: " + str( totalIdle * 1.0 / totalStep ) + endl
        print line ,
        rtnStr += line
        return rtnStr

# _ End Classes _

if __name__ == "__main__":
    print __prog_signature__()
    termArgs = sys.argv[1:] # Terminal arguments , if they exist
    
    # == Create Assembly ==
    
    
    
    # ### Phone , DIG ###############################################
    
    print "Creating assembly:" , "Simple Phone, DIG Result"
    plan_phone_DIG = Graph()
    
    # ~~ 1. Create Primary ~~
    
    modAsub = Subassembly( plan_phone_DIG , "ModuleA_sub" )
    modAsub.add_base_part( "Mod_A" , 9 )
    modAsub.add_part( "Chip_A1" , 4 )
    modAsub.add_part( "Chip_A2" , 5 )
    print "Is" , modAsub.alias , "primary?:" , modAsub.is_primary()
    
    modCsub = Subassembly( plan_phone_DIG , "ModuleC_sub" )
    modCsub.add_base_part( "Mod_C" , 11 )
    modCsub.add_part( "Chip_C1" , 8 )
    print "Is" , modCsub.alias , "primary?:" , modCsub.is_primary()
    
    modBsub = Subassembly( plan_phone_DIG , "ModuleB_sub" )
    modBsub.add_base_part( "Mod_B" , 10 )
    modBsub.add_part( "Chip_B1" , 6 )
    modBsub.add_part( "Chip_B2" , 7 )
    modBsub.add_part( "Bridge_AB" , 2 )
    modBsub.add_part( "Bridge_BC" , 3 )
    print "Is" , modBsub.alias , "primary?:" , modBsub.is_primary()
    
    # ~~ 2. Create Secondary ~~
    
    bodyACSub = Subassembly( plan_phone_DIG , "BodyAC_sub" )
    bodyACSub.add_base_part( "Body" , 0 )
    bodyACSub.add_sub( modAsub )
    bodyACSub.add_sub( modCsub )
    print "Is" , bodyACSub.alias , "primary?:" , bodyACSub.is_primary()
    
    root = Subassembly( plan_phone_DIG , "root" )
    root.add_base_sub( bodyACSub )
    root.add_sub( modBsub )
    root.add_part( "Antenna" , 1 )
    plan_phone_DIG.add_node_by_ref_safe( root ) # THIS STEP IS IMPORTANT
    print "Is" , root.alias , "primary?:" , root.is_primary()
    
    # ~~ 3. Assign Root ~~
    
    plan_phone_DIG.set_root( root )
    print
    
    
    # ### Phone , Morato ###############################################
    
    print "Creating assembly:" , "Simple Phone, Morato Result"
    plan_phone_Morato = Graph()   
    
    # ~~ 1. Create Primary ~~
    
    modABrdsub = Subassembly( plan_phone_Morato , "modABrd_sub" )
    modABrdsub.add_base_part( "Mod_A" , 9 )
    modABrdsub.add_part( "Chip_A1" , 4 )
    modABrdsub.add_part( "Chip_A2" , 5 )
    modABrdsub.add_part( "Bridge_AB" ,  2 )
    print "Is" , modABrdsub.alias , "primary?:" , modABrdsub.is_primary()  
    
    modCBrdsub = Subassembly( plan_phone_Morato , "ModCBrd_sub" )
    modCBrdsub.add_base_part( "Mod_C" , 11 )
    modCBrdsub.add_part( "Chip_C1" , 8 )
    modCBrdsub.add_part( "Bridge_BC" ,  3 )
    print "Is" , modCBrdsub.alias , "primary?:" , modCBrdsub.is_primary()    
    
    bodyBbasesub = Subassembly( plan_phone_Morato , "bodyBbase_sub" )
    bodyBbasesub.add_base_part( "Body" , 0 )
    bodyBbasesub.add_part( "Mod_B" , 10 )    
    print "Is" , bodyBbasesub.alias , "primary?:" , bodyBbasesub.is_primary()
    
    # ~~ 2. Create Secondary ~~
    
    bodyB1basesub = Subassembly( plan_phone_Morato , "bodyB1base_sub" )
    bodyB1basesub.add_base_sub( bodyBbasesub )
    bodyB1basesub.add_part( "Chip_B1" , 6 ) 
    print "Is" , bodyB1basesub.alias , "primary?:" , bodyB1basesub.is_primary()
    
    bodyB12basesub = Subassembly( plan_phone_Morato , "bodyB12base_sub" )
    bodyB12basesub.add_base_sub( bodyB1basesub )
    bodyB12basesub.add_part( "Chip_B2" , 7 ) 
    print "Is" , bodyB12basesub.alias , "primary?:" , bodyB12basesub.is_primary()
    
    bodyBCMorsub = Subassembly( plan_phone_Morato , "bodyBCMor_sub" )
    bodyBCMorsub.add_base_sub( bodyB12basesub )
    bodyBCMorsub.add_sub( modCBrdsub )
    print "Is" , bodyBCMorsub.alias , "primary?:" , bodyBCMorsub.is_primary()
    
    rootPhoneMor = Subassembly( plan_phone_Morato , "root" )
    rootPhoneMor.add_base_sub( bodyBCMorsub )
    rootPhoneMor.add_sub( modABrdsub )
    rootPhoneMor.add_part( "Antenna" , 1 ) 
    plan_phone_Morato.add_node_by_ref_safe( rootPhoneMor ) # THIS STEP IS IMPORTANT
    print "Is" , rootPhoneMor.alias , "primary?:" , rootPhoneMor.is_primary() 
    
    # ~~ 3. Assign Root ~~
    
    plan_phone_Morato.set_root( rootPhoneMor )
    print    
    
    
    # ### Phone , Belhadj ###############################################
    
    print "Creating assembly:" , "Simple Phone, Belhadj Result"
    plan_phone_Belhadj = Graph()   
    
    # ~~ 1. Create Primary ~~
    
    modBBrdgBelsub = Subassembly( plan_phone_Belhadj , "ModuleB_sub" )
    modBBrdgBelsub.add_base_part( "Mod_B" , 10 )
    modBBrdgBelsub.add_part( "Chip_B1" , 6 )
    modBBrdgBelsub.add_part( "Chip_B2" , 7 )
    modBBrdgBelsub.add_part( "Bridge_AB" , 2 )
    modBBrdgBelsub.add_part( "Bridge_BC" , 3 )
    print "Is" , modBBrdgBelsub.alias , "primary?:" , modBBrdgBelsub.is_primary()
    
    modABelsub = Subassembly( plan_phone_Belhadj , "ModuleA_sub" )
    modABelsub.add_base_part( "Mod_A" , 9 )
    modABelsub.add_part( "Chip_A1" , 4 )
    modABelsub.add_part( "Chip_A2" , 5 )
    print "Is" , modABelsub.alias , "primary?:" , modABelsub.is_primary()
    
    modCBelsub = Subassembly( plan_phone_Belhadj , "ModuleC_sub" )
    modCBelsub.add_base_part( "Mod_C" , 11 )
    modCBelsub.add_part( "Chip_C1" , 8 )
    print "Is" , modCBelsub.alias , "primary?:" , modCBelsub.is_primary()    
    
    bodyAntBelsub = Subassembly( plan_phone_Belhadj , "BodyAnt_sub" )
    bodyAntBelsub.add_base_part( "Body" , 0 )
    bodyAntBelsub.add_part( "Antenna" , 1 )
    print "Is" , bodyAntBelsub.alias , "primary?:" , bodyAntBelsub.is_primary()  
    
    # ~~ 2. Create Secondary ~~
    
    bodyAntCmodBelsub = Subassembly( plan_phone_Belhadj , "BodyAntCmod_sub" )
    bodyAntCmodBelsub.add_base_sub( bodyAntBelsub )
    bodyAntCmodBelsub.add_sub( modCBelsub )
    print "Is" , bodyAntCmodBelsub.alias , "primary?:" , bodyAntCmodBelsub.is_primary()  
    
    bodyAntCAmodBelsub = Subassembly( plan_phone_Belhadj , "BodyAntCAmod_sub" )
    bodyAntCAmodBelsub.add_base_sub( bodyAntCmodBelsub )
    bodyAntCAmodBelsub.add_sub( modABelsub )
    print "Is" , bodyAntCmodBelsub.alias , "primary?:" , bodyAntCmodBelsub.is_primary() 
    
    rootPhoneBel = Subassembly( plan_phone_Belhadj , "rootPhoneBel" )
    rootPhoneBel.add_base_sub( bodyAntCAmodBelsub )
    rootPhoneBel.add_sub( modBBrdgBelsub )  
    print "Is" , rootPhoneBel.alias , "primary?:" , rootPhoneBel.is_primary() 
    
    # ~~ 3. Assign Root ~~
    
    plan_phone_Belhadj.add_node_by_ref_safe( rootPhoneBel ) # THIS STEP IS IMPORTANT
    plan_phone_Belhadj.set_root( rootPhoneBel )
    print    
    
    
    # __ End Create __
    
    
    
    
    
    # == Work Test , Output for 1-3 Robots =================================================================================================
    
    # accum , nowTimeStamp
    
    if 1:
        
        # 0. Choose the task to plan
        #design = plan_phone_DIG
        design  = plan_phone_Belhadj
        dsnRoot = design.root
        
        # 1. for a number of workers 1 to 3
        for numBots in xrange( 1 , 4 ):
            # 2. Reset assembly
            dsnRoot.reset_unbuilt()
            # 3. Instantiate the workcell
            dispatch = AsmDispatcher( numBots )
            # 4. Assign the task to the workcell
            dispatch.assign_design( design )
            # 5. While there is work left
            totalTicks = 0
            while not is_assembly_complete( design ):
                # 3. Advance the sim by one timestep
                dispatch.tick()
                totalTicks += 1
                print "Timestep" , totalTicks , "complete"
                if totalTicks > 100:
                    break
                print
            print "Design built!"        
            # 7. Write robot records to file
            accum.sep( "Work Report for " + str( numBots ) + " Robots" )
            if 0:
                for botDex , robot in enumerate( dispatch.workers ):
                    accum.sep( "Robot " + str( botDex + 1 ) , 3 , '~' )
                    accum.prnt( "History:" , robot.history )
                    accum.prnt( "Record: " , robot.statusSeq )
            else:
                accum.prnt( dispatch.task_report_str() )
                accum.prnt( dispatch.robot_utilization_str() )
                
        accum.out_and_clear( "output/RobotReport_" + nowTimeStamp() + ".txt" )
    
    # __ End Work __________________________________________________________________________________________________________________________

# ___ End Main _____________________________________________________________________________________________________________________________


# === Spare Parts ==========================================================================================================================



# ___ End Spare ____________________________________________________________________________________________________________________________
