#!/usr/bin/env python2
# -*- coding: utf-8 -*-
"""
Created on Wed Oct 11 16:48:58 2017

@author: jwatson
"""

#    ,-- This does not appear to be necessary as long as "PACKAGENAME/setup.py" has been populated correctly , and that the ws "setup.bash"
# vv'    has been run       
# import roslib 
# roslib.load_manifest( "regrasp_processing" )
import os , operator
from math import radians , pi
import numpy as np

from mesh_pkg.mesh_helpers import * # well, we're going to test everything anyway , so ...
from mesh_pkg.mesh_plotters import *
from mesh_pkg.grasp_helpers import * 
from mesh_pkg.example_gripper import * 

from marchhare.marchhare import sep , same_contents_list , pretty_print_dict , iter_contains_None
from marchhare.MathKit import round_small
from marchhare.Vector import vec_rand_range_lst , vec_rand_prtrb_lst , matx_2D_pretty_print , vec_avg , vec_round_small
from marchhare.Plotting import plot_chain , fig_3d , show_3d , plot_pose_axes_mpl

# print dir( mesh_pkg.mesh_helpers ) # Look , it's there , I just had to run "setup.bash"
# ['STL_to_mesh_obj', '__builtins__', '__doc__', '__file__', '__name__', '__package__', 'division', 'mesh', 'mesh_verts_to_list']

# ~~ Constants , Shortcuts , Aliases ~~
EPSILON = 1e-7 # ------ Assume floating point errors below this level
infty   = 1e309 # ----- URL: http://stackoverflow.com/questions/1628026/python-infinity-any-caveats#comment31860436_1628026
endl    = os.linesep #- Line separator
pyEq    = operator.eq # Default python equality
piHalf  = pi/2

# ~~ FLAGS ~~
#_GENGRAPHICS = True
_GENGRAPHICS = False

# ..........................................................................................................................................

sep( "import mesh" , 2 , '~' )
meshObj = STL_to_mesh_obj( "CEE.stl" )
print meshObj , endl

# ..........................................................................................................................................

sep( "points from mesh" , 2 , '~' )
foo = mesh_verts_to_list( meshObj )
print "Loaded" , len( foo ) , "points" , endl

# ..........................................................................................................................................

sep( "number of facets" , 2 , '~' )
print "There are" , len_mesh( meshObj ) , "facets" , endl

# ..........................................................................................................................................

sep( "facet normal and indices" , 2 , '~' )
print "Facet 7 :" , tri_tuple_from_mesh( meshObj , 6 ) , endl

# ..........................................................................................................................................

sep( "facet center" , 2 , '~' )
print "Center 7:" , tri_centers_from_mesh( meshObj )[6] , endl

# ..........................................................................................................................................

sep( "points from STL" , 2 , '~' )
foo = STL_to_verts_list( "CEE.stl" )
print "Loaded" , len( foo ) , "points" , endl

# ..........................................................................................................................................

sep( "unique points from STL" , 2 , '~' )
foo = uniq_pts_from_STL( "CEE.stl" )
print "Loaded" , len( foo ) , "points" , endl

# ..........................................................................................................................................

sep( "AABB" , 2 , '~' )
lower =   -10
upper =    10
N     = 10000
foo = vec_rand_range_lst( 3 , lower , upper , N ) # Generate a whole bunch of points within known bounds
box = AABB_3D( foo )
for bDex , bounds in enumerate( box ):
    print [ 'x' , 'y' , 'z' ][ bDex ] , ":" , bounds
print "for" , N , "points and dim bounds " , ( lower , upper ) , endl

# ..........................................................................................................................................

sep( "span of AABB" , 2 , '~' )
print span_3D( box ) , endl

# ..........................................................................................................................................

sep( "span of 3D points" , 2 , '~' )
print span_3D_from_pts( foo ) , endl

# ..........................................................................................................................................

sep( "point mass centroid" , 2 , '~' )
massPoints = [ [ 2 , [ 0 , 0 ] ] ,
               [ 2 , [ 0 , 4 ] ] ,
               [ 4 , [ 4 , 4 ] ] ,
               [ 4 , [ 4 , 0 ] ] ]
print "Centroid , compute mass:" , centroid_discrete_masses( massPoints )
print "Centroid , given mass  :" , centroid_discrete_masses( massPoints , 12 ) , endl

# ..........................................................................................................................................

sep( "convex centroid of points" , 2 , '~' )
# 1. Generate a bunch of points in a sphere , centered on ( 1 , 1 , 1 ) , with height 4 and radius 4
N = 10000
spherePts = vec_rand_prtrb_lst( [ 1 , 1 , 1 ] , 4 , N )
print "Center of the sphere ( default slices ):" , volume_centroid_of_points_convex( spherePts ) 
print "Center of the sphere ( 500 slices )    :" , volume_centroid_of_points_convex( spherePts , 500 ) , endl

# ..........................................................................................................................................

sep( "compress mesh to V , F" , 2 , '~' )
V , F , N =  mesh_facets_to_matx_V_F_N( meshObj )
print "V = "
matx_2D_pretty_print( V ) 
print "F = "
matx_2D_pretty_print( F ) 

# ..........................................................................................................................................

sep( "facet adjacency matrix" , 2 , '~' )
matx_2D_pretty_print( facet_adjacency_matx( F ) ) 
print

# ..........................................................................................................................................

sep( "facet adjacency list" , 2 , '~' )
adj1 = facet_adjacency_list( F )
matx_2D_pretty_print( adj1 )
print

# ..........................................................................................................................................

sep( "facet adjacency list , ordered" , 2 , '~' )
adj2 = facet_adjacency_list_ordered( F )
matx_2D_pretty_print( adj2 )
bothListsSame = True
for nDex , neighbors in enumerate( adj1 ):
    if not same_contents_list( neighbors , adj2[ nDex ] ):
        bothListsSame = False
print "Did both adjacency methods result in the same neighbors?:" , bothListsSame
print

# ..........................................................................................................................................

sep( "cluster mesh" , 2 , '~' )
clusters , clstrNormals = cluster_mesh( meshObj , radians( 7 ) ) # returns clusters , clusterNorms
print "Clusters:"
matx_2D_pretty_print( clusters )
print "Cluster Normals:"
matx_2D_pretty_print( clstrNormals )

# ..........................................................................................................................................

sep( "cluster border" , 2 , '~' )
interestingIndex = 8
cluster = clusters[ interestingIndex ] # Pick an interesting cluster from the list
clsNorm = clstrNormals[ interestingIndex ]
borderSegs = get_outside_segments_from_mesh_subset( V , F , adj2 , cluster )
print borderSegs , endl

# ..........................................................................................................................................

sep( "cluster border polygon" , 2 , '~' )
borderPoly = polygon_from_RH_border_segments( borderSegs )
ptsList = [ elem[0] for elem in borderPoly ]
if _GENGRAPHICS:
    plot_chain( ptsList , makeCycle = True , color = 'red' , paintAxes = True )
print borderPoly , endl

# ..........................................................................................................................................

sep( "flatten border polygon" , 2 , '~' )
flattenPoly = flatten_cluster_border( borderPoly , clsNorm )
print flattenPoly['segments'] 
print "Same length as the 3D polygon?:" , len( flattenPoly['segments'] ) == len( borderPoly ) , endl

# ..........................................................................................................................................

sep( "polygons from clusters" , 2 , '~' )
clusterBorders = clusters_to_egdes( V , F , adj2 , clusters , clstrNormals )
print "Processed" , len( clusterBorders ) , "clusters."
pretty_print_dict( clusterBorders[0] )
print

# ..........................................................................................................................................

sep( "populate clusters with trial points" , 2 , '~' )
tile_clusters_with_trial_points( clusterBorders , 20 )
pretty_print_dict( clusterBorders[0] )
if _GENGRAPHICS:
    plot_clusters_and_grasp_points( clusterBorders )
print

# ..........................................................................................................................................

sep( "COM and volume" , 2 , '~' )
COM , vol = COM_volume_from_mesh( meshObj ) 
print "COM:" , COM , ", Volume:" , vol , endl

# ..........................................................................................................................................

sep( "simplify points" , 2 , '~' )
matx_2D_pretty_print( simplify_points( V ) )
print

# ..........................................................................................................................................

sep( "convex hull" , 2 , '~' )
hullFaces = hull_faces_from_points( simplify_points( V ) )
pretty_print_dict( hullFaces[0] )
if _GENGRAPHICS:
    plot_cluster_edges( hullFaces )
print

# ..........................................................................................................................................

sep( "support stability" , 2 , '~' )
hullFaces = identify_support_stability( hullFaces , COM )
pretty_print_dict( hullFaces[0] )
if _GENGRAPHICS:
    plot_supports_and_COM( hullFaces , COM )
print

# ..........................................................................................................................................

sep( "complete mesh processing" , 2 , '~' )
processedMesh = process_STL( "CEE.stl" )
print "V"
matx_2D_pretty_print( processedMesh.V )
print "F"
matx_2D_pretty_print( processedMesh.F )
print "N"
matx_2D_pretty_print( processedMesh.N )
print "adjacency"
matx_2D_pretty_print( processedMesh.adjacencyF )
print "clusters[0]"
pretty_print_dict( processedMesh.clusters[0] )
print "supports[0]"
pretty_print_dict( processedMesh.supports[0] )
print "volume" , processedMesh.volume
print "COM (" , processedMesh.COM[0] , " , " , processedMesh.COM[1] , " , " , processedMesh.COM[2] , " )"
print

# ..........................................................................................................................................

sep( "Candidate Grasp Pairs" , 2 , '~' )
candidatGraspPairs = generate_candidate_grasp_pairs( processedMesh , minSepDist = 100 )
print candidatGraspPairs
if _GENGRAPHICS:
    plot_opposing_grasp_pnts( processedMesh , extensionLength = 100 , divisor = 5 )
print 

# ..........................................................................................................................................
    
sep( "fully connected graph" , 2 , '~' )
fully_connected_graph_from_GraspMesh( processedMesh , 20 , radians( 45 ) )
if _GENGRAPHICS:
    plot_regrasp_supergraph( processedMesh , plotAllTransfers = True , plotAllTransitions = True )
    
print "Tags of the generated subgraphs"
for graph in processedMesh.superGraph:
    print graph.tag
    
print

# ..........................................................................................................................................
    
sep( "place the part on different supports" , 2 , '~' )
planMesh = PlanningMesh( processedMesh )
if _GENGRAPHICS:
    fig , ax = fig_3d() # Obtain a new 3D figure handle

    for i in xrange( 6 ):
        # print i ,
        planMesh.set_lab_pose_for_support_index( i , [ 500.0 * i , 0.0 ] )
        plot_3D_polygons_to_ax( ax , planMesh.get_lab_cluster_polygons() , lineColor = 'blue' , lineWidth = 1 )
        # print "Pose" , i , ":" , str( planMesh )
        plot_pose_axes_mpl( ax , planMesh , 50 , labelNum = i )
        
    ax.set_xlabel('X')
    ax.set_xlim( -100 , 3000 )
    ax.set_ylabel('Y')
    ax.set_ylim( -100 ,  500 )
    ax.set_zlabel('Z')
    ax.set_zlim(    0 ,  300 )
    
    # ax.axis('equal')
    ax.autoscale( enable = False )
    show_3d() # Show the 3D figure
else:
    print "Graphics generation skipped"
print

# ..........................................................................................................................................
    
sep( "Find supports from direction" , 2 , '~' )
upVec   = [  0 ,  0 ,  1 ]
downVec = [  0 ,  0 , -1 ]
bgnDex  = planMesh.get_support_nearest_vec( upVec   )
endDex  = planMesh.get_support_nearest_vec( downVec )
print "Desired indices are" , bgnDex , "and" , endDex , endl

if 0: # Set to 1 to debug graph generation
    sep( "Graph & UCS Debugging" , 2 , '~' )
    # 1. Print all nodes
    for subDex , subgraph in enumerate( planMesh.graspMesh.superGraph ):
        nodeList = subgraph.nodes.get_list()
        print "For support" , subDex , ", Found" , len( nodeList ) , "nodes"
        for node in nodeList:
            print '\t' , node.alias , '\t' , node.graph.alias
    # 2. Check that each node is present in every subgraph
    checkList = planMesh.graspMesh.superGraph[0].nodes.get_alias_list()
    allMatch  = True
    for subDex , subgraph in enumerate( planMesh.graspMesh.superGraph ):
        aliasList = subgraph.nodes.get_alias_list()
        print "Checking" , subDex
    
        if len( aliasList ) != len( checkList ):
            allMatch = False
            print "Length mismatch between 0 and" , subDex
            break
        if len( list( set( aliasList ) ) ) != len( checkList ):
            allMatch = False
            print "Non-unique nodes in " , subDex
            break
        for chkDex , chkAlias in enumerate( checkList ):
            if chkAlias != aliasList[ chkDex ]:
                allMatch = False
                print "At node[" , subDex , "][" , chkDex , "] , Alias mismatch:" ,  chkAlias , aliasList[ chkDex ]
                break
        if not allMatch:
            break
    if not allMatch:
        print "BAD:  Found a difference in nodes between supports for an unpruned graph!"
    else:
        print "GOOD: Found no difference in nodes between supports for an unpruned graph!"
    # 3. Examine all of the edges originating from one node
    for subNum in xrange( 6 ):
        print
        sep( "Subgraph " + str( subNum ) , 2 , '+' )
        print
        numNodes = len( planMesh.graspMesh.superGraph[ subNum ].nodes )
        for i in range(numNodes):
            successors = planMesh.graspMesh.superGraph[ subNum ].nodes[i].get_successors()
            print "Found" , len( successors ) , "successors to node" , i
            for edgeDex , successor in enumerate( successors ):
                print '\t' , successor.alias , '\t' , successor.graph.alias
    print

# ..........................................................................................................................................

sep( "Uniform Cost Search example" , 2 , '~' )
startNode = planMesh.graspMesh.superGraph[3].nodes[0] # Pick a particular node to start at
targtSprt = 5 # Pick a particular support to end at
examplePlan = uniformCostSearch( startNode , targtSprt )
print "Generated a plan with" , len( examplePlan ) , "actions"
for action in examplePlan:
    print "\t" , action
if _GENGRAPHICS:
    plot_regrasp_plan( planMesh.graspMesh , examplePlan , plotAllTransfers = True , plotAllTransitions = True )
    
# ..........................................................................................................................................

sep( "Outwards Facing Grasp" , 2 , '~' )
exGrip = ExampleGripper( [ 0.0 , 0.0 , 0.0 ] , Quaternion.no_turn_quat() , 300 )
sampleAlias = planMesh.graspMesh.superGraph[ targtSprt ].nodes[ 0 ].alias
#for targtSprt in xrange( 6 ):
if _GENGRAPHICS:
    targtSprt = 1
    print "DEBUG , Target support is:" , targtSprt
    print "DEBUG , Grasp pair before transformation:" , sampleAlias
    offsetXY = [ 0.0 , 0.0 ]
    planMesh.set_lab_pose_for_support_index( targtSprt , offsetXY )
    gripperPose , openState = exGrip.grasp_out_pose_lab( planMesh , targtSprt , sampleAlias , offsetXY )
    print "Gripper Pose:" , gripperPose , ", Open State:" , openState , endl
    gripperPose , openState = exGrip.set_grasp_out_pose_lab( planMesh , targtSprt , sampleAlias , offsetXY )
    print "Gripper Pose:" , gripperPose , ", Open State:" , openState , endl
    plot_part_and_gripper_state( planMesh , exGrip , sampleAlias )
print

# ..........................................................................................................................................

sep( "Display a sampling of grasps calculated from the part frame" , 2 , '~' )
# Can we trust all of the gripper poses that we calculated?
exGrip.calc_part_frame_grasps_for_PlanningMesh( planMesh )

if 0:
    sep( "TROBLESHOOTING GRIPPER TRANSFORMS" , 3 , '!' )
    
    targtSprt = 0
    offsetXY  = [ 0.0 , 0.0 ]
    skipN     = 5
    fig , ax = fig_3d() # Obtain a new 3D figure handle
    superGraph = planMesh.get_supergraph()
    # 1. Display the part in the untransformed state
    plot_3D_polygons_to_ax( ax , planMesh.get_part_cluster_polygons() )
    for noDex , node in enumerate( superGraph[ targtSprt ].get_node_list() ):
        # 2. Display the grasp pairs in the untransformed state
        plot_grasp_pair_to_ax( ax , node.alias , extensionLength = 85 , color = 'b' )
        # 3. Display the grasp poses in the untransformed state
        exGrip.set_gripper_pose( node.bag[ "ExGripperPose_part" ] )
        exGrip.set_gripper_position( node.bag[ "ExGripperState" ] ) 
        exGrip.render_gripper_to_ax( ax , gripColor = 'g' , lineWidth = 2 )
    ax.axis( 'equal' )
    show_3d() # Show the 3D figure

targtSprt  = 5
offsetXY   = [ 0.0 , 0.0 ]
skipN      = 0
bodyCrit   = 0.0 
fingerCrit = 75.0

if _GENGRAPHICS or 0:
    
    # A. Render the pose with all of the original grasps intact
    
    fig , ax = fig_3d() # Obtain a new 3D figure handle
    superGraph = planMesh.get_supergraph()
    # 0. Plot the supporting surface
    plot_XYgrid_to_ax( ax , [ 0 , 0 , 0 ] , [ 1 , 0 , 0 ] , [ 0 , 1 , 0 ] , 1000 , 1000 , 50 , lineColor = 'grey' , lineWidth = 1 )
    # 1. Generate a part pose for the specified support & and get the trimesh for collision check
    planMesh.set_lab_pose_for_support_index( targtSprt , offsetXY )
    labVFN = planMesh.get_lab_VFN()
    # 2. Render the part in the spefied pose
    plot_3D_polygons_to_ax( ax , planMesh.get_lab_cluster_polygons() )
    # 3. For each of the grasp pairs 
    for noDex , node in enumerate( superGraph[ targtSprt ].get_node_list() ):
        # 4. calc the gripper pose in the lab frame
        gripPoseLab = planMesh.transform_poses_to_lab( node.bag[ "ExGripperPose_part" ] )[0]
        # 5. Set the gripper pose & finger state
        exGrip.set_gripper_pose( gripPoseLab )
        exGrip.set_gripper_position( node.bag[ "ExGripperState" ] ) 
        # 6. Display the gripper in the lab frame
        #    Evaluate grasps for this support abd flag the invalid ones red
        collideRes = exGrip.collides_w_ground_or_VFN( labVFN.V , labVFN.F , labVFN.N , bodyCrit , fingerCrit )
        statusColor = 'g'
        if collideRes.result:
            print "Collision! , Codes:" , collideRes.errorCodes
            statusColor = 'r'
        exGrip.render_gripper_to_ax( ax , gripColor = statusColor , lineWidth = 2 )
        # 7. Calc the grasp pair in the lab frame
        labPair = planMesh.transform_points_to_lab( *node.alias )
        # 8. Display the grasp pair in the lab frame
        plot_grasp_pair_to_ax( ax , labPair , extensionLength = 85 , color = 'b' )
        
    ax.axis( 'equal' )
    show_3d() # Show the 3D figure
    
planMesh.supergraph_membership_report()

# B. Prune colliding grasps
print endl , "Pruning..." , endl
exGrip.eval_and_prune_colliding_grasps_for_PlanningMesh( planMesh , bodyCrit , fingerCrit )
# Now the supergraph is pruned and renumbered!

planMesh.supergraph_membership_report()
    
if _GENGRAPHICS or 0:
    # C. Render the pose with only the valid grasps showing
    
    fig , ax = fig_3d() # Obtain a new 3D figure handle
    superGraph = planMesh.get_supergraph()
    # 0. Plot the supporting surface
    plot_XYgrid_to_ax( ax , [ 0 , 0 , 0 ] , [ 1 , 0 , 0 ] , [ 0 , 1 , 0 ] , 1000 , 1000 , 50 , lineColor = 'grey' , lineWidth = 1 )
    # 1. Generate a part pose for the specified support & and get the trimesh for collision check
    planMesh.set_lab_pose_for_support_index( targtSprt , offsetXY )
    labVFN = planMesh.get_lab_VFN()
    # 2. Render the part in the spefied pose
    plot_3D_polygons_to_ax( ax , planMesh.get_lab_cluster_polygons() )
    # 3. For each of the grasp pairs 
    for noDex , node in enumerate( superGraph[ targtSprt ].get_node_list() ):
        # 4. calc the gripper pose in the lab frame
        gripPoseLab = planMesh.transform_poses_to_lab( node.bag[ "ExGripperPose_part" ] )[0]
        # 5. Set the gripper pose & finger state
        exGrip.set_gripper_pose( gripPoseLab )
        exGrip.set_gripper_position( node.bag[ "ExGripperState" ] ) 
        # 6. Display the gripper in the lab frame
        #    Evaluate grasps for this support abd flag the invalid ones red
        collideRes = exGrip.collides_w_ground_or_VFN( labVFN.V , labVFN.F , labVFN.N , bodyCrit , fingerCrit )
        statusColor = 'g'
        if collideRes.result:
            print "Collision! , Codes:" , collideRes.errorCodes
            statusColor = 'r'
        exGrip.render_gripper_to_ax( ax , gripColor = statusColor , lineWidth = 2 )
        # 7. Calc the grasp pair in the lab frame
        labPair = planMesh.transform_points_to_lab( *node.alias )
        # 8. Display the grasp pair in the lab frame
        plot_grasp_pair_to_ax( ax , labPair , extensionLength = 85 , color = 'b' )
        
    ax.axis( 'equal' )
    show_3d() # Show the 3D figure
    
# ..........................................................................................................................................
    
sep( "Uniform Cost Search on Pruned Supergraph" , 2 , '~' )
startNode = planMesh.graspMesh.superGraph[0].nodes[1] # Pick a particular node to start at
targtSprt = 5 # Pick a particular support to end at
examplePlan = uniformCostSearch( startNode , targtSprt )
print "Generated a plan with" , len( examplePlan ) , "actions"
for action in examplePlan:
    print "\t" , action
if _GENGRAPHICS or 0:
    plot_regrasp_plan( planMesh.graspMesh , examplePlan , plotAllTransfers = True , plotAllTransitions = True )
    plot_gripper_plan( exGrip , planMesh , examplePlan )
print
    
# ..........................................................................................................................................
    
sep( "Uniform Cost Search with Example Costs" , 2 , '~' )

assign_costs_for_example_gripper( planMesh ) # Assign new costs for all of the nodes and edges

startNode = planMesh.graspMesh.superGraph[0].nodes[1] # Pick a particular node to start at
targtSprt = 5 # Pick a particular support to end at
examplePlan = uniformCostSearch( startNode , targtSprt )
print "Generated a plan with" , len( examplePlan ) , "actions"
for action in examplePlan:
    print "\t" , action
if _GENGRAPHICS or 1:
    plot_regrasp_plan( planMesh.graspMesh , examplePlan , plotAllTransfers = True , plotAllTransitions = True )
    plot_gripper_plan( exGrip , planMesh , examplePlan )
print

# ..........................................................................................................................................