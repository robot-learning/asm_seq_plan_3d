#!/usr/bin/env python
# -*- coding: utf-8 -*-

# ~~ Future First ~~
from __future__ import division # Future imports must be called before everything else, including triple-quote docs!

__progname__ = "plot_vertex_normals.py"
__version__  = "2018.05" 
"""
James Watson , Template Version: 2018-05-14
Built on Spyder for Python 2.7

Plot the vertex normals to see if they are suitable to resize the VFN in a way that does not cause collisions

Dependencies: numpy
"""


"""  
~~~ Developmnent Plan ~~~
[ ] ITEM1
[ ] ITEM2
"""

# === Init Environment =====================================================================================================================
# ~~~ Prepare Paths ~~~
import sys, os.path
SOURCEDIR = os.path.dirname( os.path.abspath( __file__ ) ) # URL, dir containing source file: http://stackoverflow.com/a/7783326
PARENTDIR = os.path.dirname( SOURCEDIR )
# ~~ Path Utilities ~~
def prepend_dir_to_path( pathName ): sys.path.insert( 0 , pathName ) # Might need this to fetch a lib in a parent directory
def rel_to_abs_path( relativePath ): return os.path.join( SOURCEDIR , relativePath ) # Return an absolute path , given the 'relativePath'

# ~~~ Imports ~~~
# ~~ Standard ~~
import os , operator
from os import listdir
from math import radians , pi
# ~~ Special ~~
import numpy as np
# ~~ Local ~~
from marchhare.marchhare import sep , tokenize_with_separator 
from marchhare.Plotting import ( plot_chain_to_ax , fig_3d , show_3d , plot_VF_to_ax , axes_equal )

# ~~ Constants , Shortcuts , Aliases ~~
EPSILON = 1e-7
infty   = 1e309 # URL: http://stackoverflow.com/questions/1628026/python-infinity-any-caveats#comment31860436_1628026
endl    = os.linesep

# ~~ Script Signature ~~
def __prog_signature__(): return __progname__ + " , Version " + __version__ # Return a string representing program name and verions

# ___ End Init _____________________________________________________________________________________________________________________________


# === Main Application =====================================================================================================================

# = Program Functions =



# _ End Func _

# = Program Vars =

outDir  = '/home/jwatson/output/'
normScale = 0.005

# _ End Vars _

if __name__ == "__main__":
    print __prog_signature__()
    termArgs = sys.argv[1:] # Terminal arguments , if they exist
    
    # ~~~ Read Data ~~~
    
    #  1. List all of the files in the output directory
    outputList = listdir( outDir )
    #  2. For each of the files
    for fName in outputList:
        #  3. Init containers
        V  = [];  
        F  = [];  
        Nv = [];  
        readDex = 8
        matxReading = False
        matxRead = [ False , False , False ]
        target = None
        #  4. Open the file and read lines
        with open( os.path.join( outDir , fName ) ) as f:
            #  5. For each line
            for line in f:
                if "BGN:V" in line:
                    target = V
                    matxReading = True
                    readDex = 0
                elif "BGN:F" in line:
                    target = F
                    matxReading = True
                    readDex = 1
                elif "BGN:NV" in line:
                    target = Nv
                    matxReading = True    
                    readDex = 2
                elif "END" in line:
                    matxReading = False
                    if readDex == 0 and len( V ) > 0:
                        matxRead[ readDex ] = True
                    elif readDex == 1 and len( F ) > 0:
                        matxRead[ readDex ] = True
                    elif readDex == 2 and len( Nv ) > 0:
                        matxRead[ readDex ] = True
                    readDex = 8
                elif len( line ) > 2 and matxReading:
                    if readDex == 1:
                        target.append(  tokenize_with_separator( line , ',' , int )  )
                    else:
                        target.append(  tokenize_with_separator( line , ',' , float )  )
                else:
                    pass # empty line
               
        #  6. Input statistics
        print "Read all matrices?:" , matxRead
        sep( "V" )
        print len( V ) , "rows"
        print V
        sep( "F" )
        print len( F ) , "rows"
        print F
        sep( "Nv" )
        print len( Nv ) , "rows"
        print Nv
           
                 
        # ~~~ Plot ~~~
        
        #  7. New figure
        fig , ax = fig_3d()
        
        #  8. Plot faces
        plot_VF_to_ax( ax , V , F , color = 'blue' , width = 1 )
        
        #  9. Plot vertex normals
        if len( V ) == len( Nv ):
            print
            for i , N_i in enumerate( Nv ):
                print "V[i]:" , V[i] 
                print "N[i]:" , N_i 
                vec_i = [ V[i]                                             , 
                          np.add( V[i] , np.multiply( N_i , normScale ) )  ]
                print vec_i , endl
                plot_chain_to_ax( ax , vec_i , False , color = 'red' , width = 1 )
        else:
            print "The length of the Vertices (" , len( V ) , ") and the Vertex Normals (" , len( Nv ) , ") did NOT match!"
        
        # 10. Show
        axes_equal( ax )
        show_3d()

# ___ End Main _____________________________________________________________________________________________________________________________


# === Spare Parts ==========================================================================================================================



# ___ End Spare ____________________________________________________________________________________________________________________________
