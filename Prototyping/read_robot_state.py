#!/usr/bin/env python
# -*- coding: utf-8 -*-

# ~~ Future First ~~
from __future__ import division # Future imports must be called before everything else, including triple-quote docs!

__progname__ = "read_robot_state.py"
__version__  = "2018.05" 
"""
James Watson , Template Version: 2018-05-14
Built on Spyder for Python 2.7

Read the joint angles from a rostopic

Dependencies: numpy , roslib , rospy , rospkg , marchhare
"""


"""  
~~~ Developmnent Plan ~~~
[Y] Read the robot joints - COMPLETE , 2018-05-30
[ ] Read the effector pose
"""

# === Init Environment =====================================================================================================================
# ~~~ Prepare Paths ~~~
import sys, os.path
SOURCEDIR = os.path.dirname( os.path.abspath( __file__ ) ) # URL, dir containing source file: http://stackoverflow.com/a/7783326
PARENTDIR = os.path.dirname( SOURCEDIR )
# ~~ Path Utilities ~~
def prepend_dir_to_path( pathName ): sys.path.insert( 0 , pathName ) # Might need this to fetch a lib in a parent directory
def rel_to_abs_path( relativePath ): return os.path.join( SOURCEDIR , relativePath ) # Return an absolute path , given the 'relativePath'

# ~~~ Imports ~~~
# ~~ Standard ~~
from math import pi , sqrt
# ~~ Special ~~
import numpy as np
# ~ ROS Libraries ~
import roslib
import rospy
import rospkg # for finding where ROS things are in the file system
rospack = rospkg.RosPack() # get an instance of RosPack with the default search paths
# ~~ Local ~~
from marchhare.marchhare import sep
# ~ ROS Messages ~
from sensor_msgs.msg import JointState # ------------ For getting the joint angles
from ll4ma_robot_control_msgs.msg import RobotState # For getting the effector pose

# ~~ Constants , Shortcuts , Aliases ~~
EPSILON = 1e-7
infty   = 1e309 # URL: http://stackoverflow.com/questions/1628026/python-infinity-any-caveats#comment31860436_1628026
endl    = os.linesep

# ~~ Script Signature ~~
def __prog_signature__(): return __progname__ + " , Version " + __version__ # Return a string representing program name and verions

# ___ End Init _____________________________________________________________________________________________________________________________


# === Main Application =====================================================================================================================

# = Program Vars =

_DISPLAY_RATE = 50

# _ End Vars _


# = Program Functions =

def print_joint_state( msg ):
    """ Display the contents of a joint message to the screen """
    print "Header:_ " , msg.header
    print "Names: _ " , msg.name
    print "Position:" , msg.position
    print "Velocity:" , msg.velocity
    print "Effort:_ " , msg.effort
    
def print_robot_state( msg ):
    """ Display the contents of a robot state message to the screen """    
    # NOTE: There is a lot more info in this message ( rosmsg info ll4ma_robot_control_msgs/RobotState ) , but for now ignoring
    print "Header: ________ " , msg.header
    print "Position XYZ: __ " , [ msg.pose.position.x , msg.pose.position.y , msg.pose.position.z ]
    print "Orientation WXYZ:" , [ msg.pose.orientation.w , msg.pose.orientation.x , msg.pose.orientation.y , msg.pose.orientation.z ]

def slice_joint_state(msg,bgn,end): # [4:11] to extract the arm from "/lbr4_allegro/joint_states" topic
    """ Return a slice of a message that contains only the indices that we want """
    rtnMsg = JointState()
    rtnMsg.header = msg.header
    rtnMsg.name = msg.name[bgn:end]
    rtnMsg.position = msg.position[bgn:end]
    rtnMsg.velocity = msg.velocity[bgn:end]
    rtnMsg.effort = msg.effort[bgn:end]
    return rtnMsg

def concat_joint_state(state1, state2):
    """ Return a JointState message that is the concatenation of 'state1' and 'state2' """
    rtnMsg = JointState()
    rtnMsg.header = state1.header # Assume that the header information still applies to the concatenated states
    #                  state1                                    state2
    rtnMsg.name.extend(state1.name) ;         rtnMsg.name.extend(state2.name) # URL, extend list: http://www.tutorialspoint.com/python/list_extend.htm
    rtnMsg.position.extend(state1.position) ; rtnMsg.position.extend(state2.position)
    rtnMsg.velocity.extend(state1.velocity) ; rtnMsg.velocity.extend(state2.velocity)
    rtnMsg.effort.extend(state1.effort) ;     rtnMsg.effort.extend(state2.effort)
    return rtnMsg

# _ End Func _


# = Program Classes =

class LBR_Joint_Node:
    # Simplest node possible for talking to the robot model
    
    def __init__( self ):
        """ Constructor, init KDL solvers and control connections """        
        
        # 1. Start Node
        rospy.init_node( 'LBR_READ_Node' )
        self.heartBeatHz = 100 # ------------------- Node refresh rate
        self.jointState = None # ------------------- Joint state of the entire LBR4 + Allegro robot
        self.robotState = None # ------------------- Effector pose
        self.gotFirstState = False # --------------- Flag that tells whether the model can start reading state information ( old Gazebo flag )
        self.idle = rospy.Rate( self.heartBeatHz ) # Best effort to maintain 'heartBeatHz' , URL: http://wiki.ros.org/rospy/Overview/Time
        
        # 2. Start Subscribers  &&  Set callbacks
        # Robot Config
        rospy.Subscriber( '/lbr4_teleop/joint_states' , JointState , self.joint_state_cb )
        self.gotFirstState = False
        # Robot State
        rospy.Subscriber( '/lbr4_teleop/robot_state' , RobotState , self.robot_state_cb )
        
    def joint_state_cb( self , input_joint_state ):
        """ Store the current joint state of the robot """
        SHOWDEBUG = True
        if input_joint_state.header.seq % _DISPLAY_RATE == 0: # Not sure why states are filtered by timestamp ...
            self.jointState = input_joint_state
            if not self.gotFirstState:
                self.gotFirstState = True
                print "stored a state!"  
            if SHOWDEBUG:
                sep( "Incoming Joint Message" )
                print_joint_state( self.jointState )
                print
                
    def robot_state_cb( self , input_robot_state ):
        """ Store the current effector pose """
        SHOWDEBUG = False
        if input_robot_state.header.seq % _DISPLAY_RATE == 0: # Not sure why states are filtered by timestamp ...
            self.robotState = input_robot_state
            if SHOWDEBUG:
                sep( "Incoming Robot Message" )
                print_robot_state( self.robotState )
                print
                
    def run( self ):
        rospy.spin()
#        while not rospy.is_shutdown():
#            hello_str = "hello world %s" % rospy.get_time()
#            rospy.loginfo(hello_str)
#            pub.publish(hello_str)
#            self.idle.sleep()
        

# _ End Classes _





if __name__ == "__main__":
    print __prog_signature__()
    termArgs = sys.argv[1:] # Terminal arguments , if they exist
    
    node = LBR_Joint_Node()
    node.run()
    

# ___ End Main _____________________________________________________________________________________________________________________________


# === Spare Parts ==========================================================================================================================



# ___ End Spare ____________________________________________________________________________________________________________________________
