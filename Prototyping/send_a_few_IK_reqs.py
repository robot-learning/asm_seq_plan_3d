#!/usr/bin/env python
# -*- coding: utf-8 -*-

# ~~ Future First ~~
from __future__ import division # Future imports must be called before everything else, including triple-quote docs!

__progname__ = "send_a_few_IK_reqs.py"
__version__  = "2018.05"
"""
James Watson , Template Version: 2018-05-30
Built on Spyder for Python 2.7

Send a few IK requests and see what happens!

Dependencies: numpy
"""


"""  
~~~ Developmnent Plan ~~~
[ ] ITEM1
[ ] ITEM2
"""

# === Init Environment =====================================================================================================================
# ~~~ Prepare Paths ~~~
import sys, os.path
SOURCEDIR = os.path.dirname( os.path.abspath( __file__ ) ) # URL, dir containing source file: http://stackoverflow.com/a/7783326
PARENTDIR = os.path.dirname( SOURCEDIR )
# ~~ Path Utilities ~~
def prepend_dir_to_path( pathName ): sys.path.insert( 0 , pathName ) # Might need this to fetch a lib in a parent directory
def rel_to_abs_path( relativePath ): return os.path.join( SOURCEDIR , relativePath ) # Return an absolute path , given the 'relativePath'

# ~~~ Imports ~~~
# ~~ Standard ~~
from math import pi , sqrt
from time import sleep
from random import random
# ~~ Special ~~
import numpy as np
import roslib , rospy
import rospkg # for finding where ROS things are in the file system
rospack = rospkg.RosPack() # get an instance of RosPack with the default search paths
# ~~ Local ~~
# ~ Package Messages ~
from std_msgs.msg import Header
from motion_cost.msg import IKrequest #- Message contains desired pose and possibly a seed

# ~~ Constants , Shortcuts , Aliases ~~
EPSILON = 1e-7
infty   = 1e309 # URL: http://stackoverflow.com/questions/1628026/python-infinity-any-caveats#comment31860436_1628026
endl    = os.linesep

# ~~ Script Signature ~~
def __prog_signature__(): return __progname__ + " , Version " + __version__ # Return a string representing program name and verions

# ___ End Init _____________________________________________________________________________________________________________________________


# === Main Application =====================================================================================================================

# = Program Vars =



# _ End Vars _


# = Program Functions =



# _ End Func _


# = Program Classes =



# _ End Classes _

if __name__ == "__main__":
    print __prog_signature__()
    termArgs = sys.argv[1:] # Terminal arguments , if they exist
    
    rospy.init_node( 'IK_Test' )
    
    SHOWDEBUG = True
    
    repeat = 5
    counter = 0
    requester = rospy.Publisher( '/ASM/LBR4_IK_reqs' , IKrequest , queue_size = 5 )
    
    message = IKrequest()
    
    # 1. Reachable
    message.header = Header()
    message.header.frame_id = "world"
    
    message.worldPose.position.x    = 0.5
    message.worldPose.position.y    = 0.5
    message.worldPose.position.z    = 1.1
    
    message.worldPose.orientation.w = 1.0
    message.worldPose.orientation.x = 0.0
    message.worldPose.orientation.y = 0.0
    message.worldPose.orientation.z = 0.0
    
    message.seed.position = [ 0 ] * 7

    counter += 1
    message.sequence = counter
    
    for i in xrange( repeat ):
        requester.publish( message )
        if SHOWDEBUG:
            print message
            print
        sleep( 0.1 )
    
    # 2. Reachable
    message.worldPose.position.x    = -0.5
    message.worldPose.position.y    = -0.5
    message.worldPose.position.z    =  1.1
    
    message.worldPose.orientation.w =  1.0
    message.worldPose.orientation.x =  0.0
    message.worldPose.orientation.y =  0.0
    message.worldPose.orientation.z =  0.0
    
    message.seed.position = [ -pi + 2*pi*random() for i in xrange(7) ]

    counter += 1
    message.sequence = counter
    
    for i in xrange( repeat ):
        requester.publish( message )
        if SHOWDEBUG:
            print message
            print
        sleep( 0.1 )
    
    # 3. Unreachable
    message.worldPose.position.x    = -4.5
    message.worldPose.position.y    = -4.5
    message.worldPose.position.z    =  4.1
    
    message.worldPose.orientation.w =  1.0
    message.worldPose.orientation.x =  0.0
    message.worldPose.orientation.y =  0.0
    message.worldPose.orientation.z =  0.0
    
    message.seed.position = [ -pi + 2*pi*random() for i in xrange(7) ]

    counter += 1
    message.sequence = counter
    
    for i in xrange( repeat ):
        requester.publish( message )
        if SHOWDEBUG:
            print message
            print
        sleep( 0.1 )

# ___ End Main _____________________________________________________________________________________________________________________________


# === Spare Parts ==========================================================================================================================



# ___ End Spare ____________________________________________________________________________________________________________________________
