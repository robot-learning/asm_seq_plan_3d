#!/usr/bin/env python
# -*- coding: utf-8 -*-

# ~~ Future First ~~
from __future__ import division # Future imports must be called before everything else, including triple-quote docs!

__progname__ = "test_IK_serv.py"
__version__  = "2018.06"
"""
James Watson , Template Version: 2018-05-30
Built on Spyder for Python 2.7

Send a few IK requests to the service and see what happens!

Dependencies: numpy
"""


"""  
~~~ Developmnent Plan ~~~
[ ] ITEM1
[ ] ITEM2
"""

# === Init Environment =====================================================================================================================
# ~~~ Prepare Paths ~~~
import sys, os.path
SOURCEDIR = os.path.dirname( os.path.abspath( __file__ ) ) # URL, dir containing source file: http://stackoverflow.com/a/7783326
PARENTDIR = os.path.dirname( SOURCEDIR )
# ~~ Path Utilities ~~
def prepend_dir_to_path( pathName ): sys.path.insert( 0 , pathName ) # Might need this to fetch a lib in a parent directory
def rel_to_abs_path( relativePath ): return os.path.join( SOURCEDIR , relativePath ) # Return an absolute path , given the 'relativePath'

# ~~~ Imports ~~~
# ~~ Standard ~~
from math import pi , sqrt
from random import random
# ~~ Special ~~
import numpy as np
import rospy 
# ~ IK SOlver ~
from trac_ik_python.trac_ik import IK
# ~ ROS Messages ~
from geometry_msgs.msg import Pose # --- For receiving the effector pose
from sensor_msgs.msg import JointState # For sending the joint angles
from std_msgs.msg import Header
# ~~ Local ~~
# ~ Package Messages ~
from motion_cost.msg import IKrequest #- Message contains desired pose and possibly a seed
from motion_cost.msg import IKresponse # Message contains success code and solved joint angles
# ~ Package Services ~
from motion_cost.srv import qFromPose # IK Service
# ~ MARCHHARE ~
from marchhare.marchhare import sep

# ~~ Constants , Shortcuts , Aliases ~~
EPSILON = 1e-7
infty   = 1e309 # URL: http://stackoverflow.com/questions/1628026/python-infinity-any-caveats#comment31860436_1628026
endl    = os.linesep

# ~~ Script Signature ~~
def __prog_signature__(): return __progname__ + " , Version " + __version__ # Return a string representing program name and verions

# ___ End Init _____________________________________________________________________________________________________________________________


# === Main Application =====================================================================================================================

# = Program Vars =



# _ End Vars _


# = Program Functions =

def q_solver_client( req ):

    # NOTE: you don't have to call rospy.init_node() to make calls against
    # a service. This is because service clients do not have to be
    # nodes.

    # block until the add_two_ints service is available
    # you can optionally specify a timeout
    rospy.wait_for_service( 'q_from_pose' )
    
    try:
        # create a handle to the add_two_ints service
        qSolve = rospy.ServiceProxy( 'q_from_pose' , qFromPose )
        
        # simplified style
        response = qSolve( req )

        return response
    
    except rospy.ServiceException , e:
        print "Service call failed: %s" %e

# _ End Func _


# = Program Classes =



# _ End Classes _

if __name__ == "__main__":
    print __prog_signature__()
    termArgs = sys.argv[1:] # Terminal arguments , if they exist
    
    rospy.wait_for_service( 'q_from_pose' )
    
    SHOWDEBUG = True
    
    counter = 0
    requester = rospy.Publisher( '/ASM/LBR4_IK_reqs' , IKrequest , queue_size = 5 )
    
    message = IKrequest()
    
    # 1. Reachable
    sep( "1. Reachable" )
    message.header = Header()
    message.header.frame_id = "world"
    
    message.worldPose.position.x    = 0.8
    message.worldPose.position.y    = 0.0
    message.worldPose.position.z    = 0.59
    
    message.worldPose.orientation.w = 1.0
    message.worldPose.orientation.x = 0.0
    message.worldPose.orientation.y = 0.0
    message.worldPose.orientation.z = 0.0
    
    message.seed.position = [ 0 ] * 7

    counter += 1
    message.sequence = counter
    
    if SHOWDEBUG:
        print "Sending request ..."
        print message
        print
    print "Waiting on solution ..." , endl
    print q_solver_client( message ).response , endl # It's just responses all the way down!
    print "Solution returned!" , endl , endl
        
    
    # 2. Reachable
    sep( "2. Reachable" )
    message.worldPose.position.x    = -0.5
    message.worldPose.position.y    = -0.5
    message.worldPose.position.z    =  1.1
    
    message.worldPose.orientation.w =  1.0
    message.worldPose.orientation.x =  0.0
    message.worldPose.orientation.y =  0.0
    message.worldPose.orientation.z =  0.0
    
    message.seed.position = [ -pi + 2*pi*random() for i in xrange(7) ]

    counter += 1
    message.sequence = counter
    
    if SHOWDEBUG:
        print "Sending request ..."
        print message
        print
    print "Waiting on solution ..." , endl
    print q_solver_client( message ).response , endl # It's just responses all the way down!
    print "Solution returned!" , endl , endl
    
    # 3. Unreachable
    sep( "3. Unreachable" )
    message.worldPose.position.x    = -4.5
    message.worldPose.position.y    = -4.5
    message.worldPose.position.z    =  4.1
    
    message.worldPose.orientation.w =  1.0
    message.worldPose.orientation.x =  0.0
    message.worldPose.orientation.y =  0.0
    message.worldPose.orientation.z =  0.0
    
    message.seed.position = [ -pi + 2*pi*random() for i in xrange(7) ]

    counter += 1
    message.sequence = counter
    
    if SHOWDEBUG:
        print "Sending request ..."
        print message
        print
    print "Waiting on solution ..." , endl
    print q_solver_client( message ).response , endl # It's just responses all the way down!
    print "Solution returned!" , endl , endl

# ___ End Main _____________________________________________________________________________________________________________________________


# === Spare Parts ==========================================================================================================================



# ___ End Spare ____________________________________________________________________________________________________________________________
