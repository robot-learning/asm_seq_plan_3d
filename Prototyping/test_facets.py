#!/usr/bin/env python2
# -*- coding: utf-8 -*-
"""
Created on Wed Oct 11 16:48:58 2017

@author: jwatson
"""

#    ,-- This does not appear to be necessary as long as "PACKAGENAME/setup.py" has been populated correctly , and that the ws "setup.bash"
# vv'    has been run       
#import roslib 
#roslib.load_manifest( "regrasp_processing" )
import os , operator
from os import listdir
from math import radians , pi
import numpy as np

from marchhare.marchhare import sep , same_contents_list , pretty_print_dict , iter_contains_None , tokenize_with_separator , string_contains_any
from marchhare.MathKit import round_small
from marchhare.Vector import vec_rand_range_lst , vec_rand_prtrb_lst , matx_2D_pretty_print , vec_avg , vec_round_small
from marchhare.Plotting import ( plot_chain , fig_3d , show_3d , plot_pose_axes_mpl , plot_chain_to_ax , ax_view , plot_axes_3D_mpl , 
                                 plot2D_chain_to_ax , fig_2d , show_2d )

#from mesh_pkg.mesh_helpers import * # well, we're going to test everything anyway , so ...
#from mesh_pkg.mesh_plotters import *
#from mesh_pkg.grasp_helpers import * 
#from mesh_pkg.example_gripper import * 



# print dir( mesh_pkg.mesh_helpers ) # Look , it's there , I just had to run "setup.bash"
# ['STL_to_mesh_obj', '__builtins__', '__doc__', '__file__', '__name__', '__package__', 'division', 'mesh', 'mesh_verts_to_list']

# ~~ Constants , Shortcuts , Aliases ~~
EPSILON = 1e-7 # ------ Assume floating point errors below this level
infty   = 1e309 # ----- URL: http://stackoverflow.com/questions/1628026/python-infinity-any-caveats#comment31860436_1628026
endl    = os.linesep #- Line separator
pyEq    = operator.eq # Default python equality
piHalf  = pi/2

# ~~ FLAGS ~~
#_GENGRAPHICS = True
_GENGRAPHICS = False

# ..........................................................................................................................................

created = False
outDir  = '/home/jwatson/output/'
labels  = [ 'origin' , 'zBasis' , 'xBasis' ]
modeSep = '~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~'


# 1. List all of the files in the output directory
outputList = listdir( outDir )
# 2. For each of the files
for fName in outputList:
    # 3. Begin a new point collection
    polys = [[]] ; created = True
    polysFlat = [[]] ; createdFlat = True
    origin  = [ 0 , 0 , 0 ]
    zBasis  = [ 0 , 0 , 0 ]
    xBasis  = [ 0 , 0 , 0 ]
    mode1   = True
    # 4. Open the file and read lines
    with open( os.path.join( outDir , fName ) ) as f:
        # 5. For each line
        for line in f:
            # If we are reading 3D facets
            if mode1:
                # 6. If the line contains text , add a point to the collection , make sure that the created flag is False
                if len( line ) > 2:
                    if modeSep in line:
                        mode1 = False # Deactivate Mode 1 , Switch to plotting flat polys
                    elif not string_contains_any( line , labels ):
                        polys[-1].append( tokenize_with_separator( line , ',' , float ) )
                        if created:
                            created = False 
                    else:
                        label , coords = line.split( ':' )
                        if label == 'origin':
                            origin = tokenize_with_separator( coords , ',' , float )
                        elif label == 'zBasis':
                            zBasis = tokenize_with_separator( coords , ',' , float )
                        elif label == 'xBasis':
                            xBasis = tokenize_with_separator( coords , ',' , float )
                # 7. If the line does not contain text
                else:
                    # 9. If the created flag is not set , create new collection and set flag
                    if not created:
                        polys.append( [] )
                        created = True
                    # 10. If the created flag is set , then do nothing
            # If we are reading 2D facets
            else:
                if len( line ) > 2:
                    polysFlat[-1].append( tokenize_with_separator( line , ',' , float ) )
                    if createdFlat:
                        createdFlat = False 
                else:
                    if not createdFlat:
                        polysFlat.append( [] )
                        createdFlat = True
    # 11. Paint all of the polgons (closed) for the file
    fig , ax = fig_3d()
    plot_axes_3D_mpl( ax , scale = 0.05 )
    
    planeScale = 0.10
    plot_chain_to_ax( ax , [ origin , np.multiply( zBasis , planeScale ) ] , makeCycle = False , color = 'blue' , width = 1 )
    plot_chain_to_ax( ax , [ origin , np.multiply( xBasis , planeScale ) ] , makeCycle = False , color = 'red'  , width = 1 )
    
    for poly in polys:
        plot_chain_to_ax( ax , poly , makeCycle = False , color = 'black' , width = 1 )
    ax_view( ax , 45 , 45 )
    show_3d()
    
    # 12. Paint all the flat polys for the file
    fig , ax = fig_2d()
    for poly in polysFlat:
        plot2D_chain_to_ax( ax , poly , makeCycle = False , color = 'black' , width = 1 )
    show_2d()