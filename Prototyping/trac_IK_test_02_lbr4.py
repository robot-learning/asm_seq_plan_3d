#!/usr/bin/env python
# -*- coding: utf-8 -*-

# ~~ Future First ~~
from __future__ import division # Future imports must be called before everything else, including triple-quote docs!

__progname__ = "trac_IK_test_01_pr2.py"
__version__  = "2018.05" 
"""
James Watson , Template Version: 2018-05-30
Built on Spyder for Python 2.7

Test Trac_IK python wrapper

Dependencies: numpy
"""


"""  
~~~ Developmnent Plan ~~~
[Y] Trac_IK test pr2 verbatim - 2018-05-31 , Functional with the newest version of Trac_IK
[ ] Adapt test for KUKA
    [ ] Create a launch file to upload the KUKA xacro to the parameter server
    [ ] Test results
[ ] ROS service for IK results
"""

# === Init Environment =====================================================================================================================
# ~~~ Prepare Paths ~~~
import sys, os.path
SOURCEDIR = os.path.dirname( os.path.abspath( __file__ ) ) # URL, dir containing source file: http://stackoverflow.com/a/7783326
PARENTDIR = os.path.dirname( SOURCEDIR )
# ~~ Path Utilities ~~
def prepend_dir_to_path( pathName ): sys.path.insert( 0 , pathName ) # Might need this to fetch a lib in a parent directory
def rel_to_abs_path( relativePath ): return os.path.join( SOURCEDIR , relativePath ) # Return an absolute path , given the 'relativePath'

# ~~~ Imports ~~~
# ~~ Standard ~~
from math import pi , sqrt
# ~~ Special ~~
import numpy as np
from trac_ik_python.trac_ik import IK
# ~~ Local ~~

# ~~ Constants , Shortcuts , Aliases ~~
EPSILON = 1e-7
infty   = 1e309 # URL: http://stackoverflow.com/questions/1628026/python-infinity-any-caveats#comment31860436_1628026
endl    = os.linesep

# ~~ Script Signature ~~
def __prog_signature__(): return __progname__ + " , Version " + __version__ # Return a string representing program name and verions

# ___ End Init _____________________________________________________________________________________________________________________________


# === Main Application =====================================================================================================================

# = Program Vars =



# _ End Vars _


# = Program Functions =



# _ End Func _


# = Program Classes =



# _ End Classes _

if __name__ == "__main__":
    print __prog_signature__()
    termArgs = sys.argv[1:] # Terminal arguments , if they exist
    
    # Solver is relative from base to the end of the arm
    ik_solver = IK( "world"       ,   # world # base_link    # torso_lift_link
                    "lbr4_7_link" )   # lbr4_7_link  # r_wrist_roll_link 

    seed_state = [0.0] * ik_solver.number_of_joints
    
    print ik_solver.get_ik( seed_state ,
                            0.5 , 0.5 , 1.1 ,  # X, Y, Z
                            0.0 , 0.0 , 0.0 , 1.0 )  # QX, QY, QZ, QW
    
# ___ End Main _____________________________________________________________________________________________________________________________


# === Spare Parts ==========================================================================================================================



# ___ End Spare ____________________________________________________________________________________________________________________________
