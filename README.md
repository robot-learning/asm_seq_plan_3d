    git clone --recurse-submodules ABOVE_ADDRESS
    git submodule update --recursive
    
If you somehow did not clone recursively:

    git submodule update --init --recursive

If submodules remain stubbornly empty but also register as "up to date" after pulling

    git submodule foreach git stash
    git submodule foreach git pull --rebase

Last working LIBIGL commit:

commit e2aa034ab822d492ba74d736a8d823a317e8520e
Merge: 1e01eb2 5c04e63
Author: Daniele Panozzo <daniele.panozzo@gmail.com>
Date:   Tue Aug 29 02:47:45 2017 +0800

    Merge pull request #592 from josefgraus/matplotlib_colormaps
    
    Matplotlib colormaps

Copy { `tetgen.h` , `tetgen.cxx` } from libigl/external/tetgen/ to --> libigl/include/igl/copyleft/tetgen/
Setting the `CMakeLists.txt` flag for TETGEN seems to have no effect.