=== Package Commands ===

>> simple_render <<
1. roscore
2. rosrun rviz rviz
3. rosrun simple_render STL_test

>> move_markers <<
1. roscore
2. rosrun rviz rviz
3. rosrun using_markers marker_move

___ End Commands ___


=== RVIZ File Setup ===

1. "Fixed Frame" in RViz must be set to the same string that is assigned to 'MARKERNAME.header.frame_id' in the code
2. A marker visualization object must be added from the "Add" menu.  "Marker Topic" must be "visualization_marker", which is the same
   topic name that you must use in your code
   * You may have to uncheck and recheck the box in order to clear markers from a previous simulation if they share a topic name
3. MarkerArray is a separate RViz type from Marker.  It has its own visualization type in the RViz GUI. "Marker Topic" must be 
   "visualization_marker_array", which is the same topic name that you must use in your code

___ End RVIZ ___


=== OGV to MOV ===

0. Must have ffmpeg (Trusty 14.04): https://www.faqforge.com/linux/how-to-install-ffmpeg-on-ubuntu-14-04/
1. ffmpeg -i INPUT.ogv -vcodec libx264 "OUTPUT.mp4"

___ End Movie Conversion ___


=== Workspace Repair ===

If 'catkin_make' suddenly stops working when invoked from the root of the workspace, delete the CMakeLists from the root and src directories,
then call 'catkin_make' from the root of the workspace. This essentially resets the build 

___ End Workspace ___


=== Creating ROS Packages ===

http://wiki.ros.org/ROS/Tutorials/catkin/CreatingPackage

1. From the workspace src  dir , catkin_create_pkg PACKAGE_NAME INCLUDE1 INCLUDE2 ...
2. Write code , make sure that the CMakeLists.txt is correct
3. From the workspace root dir , catkin_make
4. From the workspace root dir , source devel/setup.bash

All of your ROS work should be in the same workspace for the same computer. There may be multiple packages in the 'src'
folder, each with their own packages and 'src' folders, but there should only be one workspace.

If there is a package that you do not want built with 'catkin_make', then place an empty file in the package folder named
'CATKIN_IGNORE', with no file extension.  'catkin_make' will skip over this package. This is useful if a package is
unfinished or otherwise broken.

~~ Declare all of your CPP sources! ~~
#                           v-- Main          v-- All others ...
add_executable( marker_move src/mrkr_move.cpp src/Cpp_Helpers.cpp )

    == Using External Libraries / Headers ==

    URL: http://docs.ros.org/api/catkin/html/howto/format2/building_libraries.html

    Find the directory that has the headers and store it in a variable

    __ End External __

___ End ROS Package __


~ Arduino for ROS ~
http://wiki.ros.org/rosserial_arduino/Tutorials/Arduino%20IDE%20Setup

~ rostopic Diagnostics ~
~ Record what is heard on a topic ~

unbuffer rostopic echo TOPICNAME

# Sometimes not everything gets printed unless you 'unbuffer', especially if you have to kill the process that you are saving the output of

source devel/setup.bash

You can have the above run each time you open a new terminal by adding it to the "~/.bashrc", but you should use the
full path in place of 'devel/'.


== Python in Packages ==

* trmnl> catkin_create_pkg beg_tuts2 std_msgs rospy roscpp roslib
  #                                           ^------------^------ These are important to for packages that will have Python nodes
* Create a 'scripts' folder to store Python scripts in
* Each script should have executable permission, trmnl> sudo chmod +x FILENAME.py
* 'rospy.loginfo(hello_str)' --> rosout is a handy for debugging: you can pull up messages using rqt_console instead of 
   having to find the console window with your Node's output
* The general rule of thumb is that constructor args are in the same order as in the .msg file
* Running scripts
  - trmnl> rosrun beginner_tutorials talker.py 
  - trmnl> rosrun beginner_tutorials listener.py
* Any time your package provides a service or custom message, there must be SRV and MSG files in the 'srv' and 'msg' 
  directories 
  - Use # for comments
  - http://wiki.ros.org/ROS/Tutorials/CreatingMsgAndSrv#Creating_a_srv
* Package changes to allow for services and 
  URL: http://wiki.ros.org/rospy_tutorials/Tutorials/Makefile

  - Your package.xml should contain the lines:
    <build_depend>message_generation</build_depend> <!-- For custom msg -->

  - Your CMakelists.txt should contain:

    find_package(catkin REQUIRED COMPONENTS message_generation rospy roslib)

    add_message_files(
      FILES  # e.g. Floats.msg HeaderString.msg
    )

    add_service_files(
      DIRECTORY srv 
      FILES AddTwoInts.srv
    )

	## Generate added messages and services with any dependencies
	generate_messages()

	catkin_package(
	  CATKIN_DEPENDS message_runtime
	)

__ End Py Pkg __


== Working with 'catkin_make' ==

* "package.xml" should have 
  <build_depend>PACKAGENAME</build_depend> # and a 
  <run_depend>PACKAGENAME</run_depend> 
  for every 'PACKAGENAME' called out under
  'CATKIN_DEPENDS' in 'catkin_package()' in "CMakeLists,txt"

gedit /home/jwatson/regrasp_planning/build/CMakeFiles/CMakeOutput.log
gedit /home/jwatson/regrasp_planning/build/CMakeFiles/CMakeError.log

__ End 'catkin_make' __


~~~~ FCL Installation Output ~~~~

-- Install configuration: "Release"
-- Up-to-date: /usr/local/include
-- Up-to-date: /usr/local/include/fcl
-- Up-to-date: /usr/local/include/fcl/narrowphase
-- Up-to-date: /usr/local/include/fcl/narrowphase/collision_result-inl.h
-- Up-to-date: /usr/local/include/fcl/narrowphase/distance.h
-- Up-to-date: /usr/local/include/fcl/narrowphase/cost_source-inl.h
-- Up-to-date: /usr/local/include/fcl/narrowphase/continuous_collision_object.h
-- Up-to-date: /usr/local/include/fcl/narrowphase/contact.h
-- Up-to-date: /usr/local/include/fcl/narrowphase/gjk_solver_type.h
-- Up-to-date: /usr/local/include/fcl/narrowphase/continuous_collision_request.h
-- Up-to-date: /usr/local/include/fcl/narrowphase/distance_result.h
-- Up-to-date: /usr/local/include/fcl/narrowphase/continuous_collision-inl.h
-- Up-to-date: /usr/local/include/fcl/narrowphase/collision.h
-- Up-to-date: /usr/local/include/fcl/narrowphase/collision_request.h
-- Up-to-date: /usr/local/include/fcl/narrowphase/distance_request.h
-- Up-to-date: /usr/local/include/fcl/narrowphase/cost_source.h
-- Up-to-date: /usr/local/include/fcl/narrowphase/continuous_collision.h
-- Up-to-date: /usr/local/include/fcl/narrowphase/detail
-- Up-to-date: /usr/local/include/fcl/narrowphase/detail/distance_func_matrix.h
-- Up-to-date: /usr/local/include/fcl/narrowphase/detail/conservative_advancement_func_matrix.h
-- Up-to-date: /usr/local/include/fcl/narrowphase/detail/convexity_based_algorithm
-- Up-to-date: /usr/local/include/fcl/narrowphase/detail/convexity_based_algorithm/gjk-inl.h
-- Up-to-date: /usr/local/include/fcl/narrowphase/detail/convexity_based_algorithm/epa-inl.h
-- Up-to-date: /usr/local/include/fcl/narrowphase/detail/convexity_based_algorithm/minkowski_diff.h
-- Up-to-date: /usr/local/include/fcl/narrowphase/detail/convexity_based_algorithm/minkowski_diff-inl.h
-- Up-to-date: /usr/local/include/fcl/narrowphase/detail/convexity_based_algorithm/alloc.h
-- Up-to-date: /usr/local/include/fcl/narrowphase/detail/convexity_based_algorithm/support.h
-- Up-to-date: /usr/local/include/fcl/narrowphase/detail/convexity_based_algorithm/list.h
-- Up-to-date: /usr/local/include/fcl/narrowphase/detail/convexity_based_algorithm/gjk_libccd.h
-- Up-to-date: /usr/local/include/fcl/narrowphase/detail/convexity_based_algorithm/polytope.h
-- Up-to-date: /usr/local/include/fcl/narrowphase/detail/convexity_based_algorithm/gjk.h
-- Up-to-date: /usr/local/include/fcl/narrowphase/detail/convexity_based_algorithm/epa.h
-- Up-to-date: /usr/local/include/fcl/narrowphase/detail/convexity_based_algorithm/simplex.h
-- Up-to-date: /usr/local/include/fcl/narrowphase/detail/convexity_based_algorithm/gjk_libccd-inl.h
-- Up-to-date: /usr/local/include/fcl/narrowphase/detail/gjk_solver_libccd-inl.h
-- Up-to-date: /usr/local/include/fcl/narrowphase/detail/collision_func_matrix-inl.h
-- Up-to-date: /usr/local/include/fcl/narrowphase/detail/collision_func_matrix.h
-- Up-to-date: /usr/local/include/fcl/narrowphase/detail/traversal
-- Up-to-date: /usr/local/include/fcl/narrowphase/detail/traversal/traversal_recurse.h
-- Up-to-date: /usr/local/include/fcl/narrowphase/detail/traversal/octree
-- Up-to-date: /usr/local/include/fcl/narrowphase/detail/traversal/octree/octree_solver-inl.h
-- Up-to-date: /usr/local/include/fcl/narrowphase/detail/traversal/octree/octree_solver.h
-- Up-to-date: /usr/local/include/fcl/narrowphase/detail/traversal/octree/collision
-- Up-to-date: /usr/local/include/fcl/narrowphase/detail/traversal/octree/collision/octree_collision_traversal_node-inl.h
-- Up-to-date: /usr/local/include/fcl/narrowphase/detail/traversal/octree/collision/shape_octree_collision_traversal_node-inl.h
-- Up-to-date: /usr/local/include/fcl/narrowphase/detail/traversal/octree/collision/octree_mesh_collision_traversal_node-inl.h
-- Up-to-date: /usr/local/include/fcl/narrowphase/detail/traversal/octree/collision/shape_octree_collision_traversal_node.h
-- Up-to-date: /usr/local/include/fcl/narrowphase/detail/traversal/octree/collision/octree_shape_collision_traversal_node-inl.h
-- Up-to-date: /usr/local/include/fcl/narrowphase/detail/traversal/octree/collision/mesh_octree_collision_traversal_node-inl.h
-- Up-to-date: /usr/local/include/fcl/narrowphase/detail/traversal/octree/collision/octree_collision_traversal_node.h
-- Up-to-date: /usr/local/include/fcl/narrowphase/detail/traversal/octree/collision/octree_mesh_collision_traversal_node.h
-- Up-to-date: /usr/local/include/fcl/narrowphase/detail/traversal/octree/collision/octree_shape_collision_traversal_node.h
-- Up-to-date: /usr/local/include/fcl/narrowphase/detail/traversal/octree/collision/mesh_octree_collision_traversal_node.h
-- Up-to-date: /usr/local/include/fcl/narrowphase/detail/traversal/octree/distance
-- Up-to-date: /usr/local/include/fcl/narrowphase/detail/traversal/octree/distance/mesh_octree_distance_traversal_node.h
-- Up-to-date: /usr/local/include/fcl/narrowphase/detail/traversal/octree/distance/octree_shape_distance_traversal_node.h
-- Up-to-date: /usr/local/include/fcl/narrowphase/detail/traversal/octree/distance/octree_mesh_distance_traversal_node.h
-- Up-to-date: /usr/local/include/fcl/narrowphase/detail/traversal/octree/distance/octree_shape_distance_traversal_node-inl.h
-- Up-to-date: /usr/local/include/fcl/narrowphase/detail/traversal/octree/distance/octree_distance_traversal_node.h
-- Up-to-date: /usr/local/include/fcl/narrowphase/detail/traversal/octree/distance/shape_octree_distance_traversal_node-inl.h
-- Up-to-date: /usr/local/include/fcl/narrowphase/detail/traversal/octree/distance/octree_distance_traversal_node-inl.h
-- Up-to-date: /usr/local/include/fcl/narrowphase/detail/traversal/octree/distance/mesh_octree_distance_traversal_node-inl.h
-- Up-to-date: /usr/local/include/fcl/narrowphase/detail/traversal/octree/distance/shape_octree_distance_traversal_node.h
-- Up-to-date: /usr/local/include/fcl/narrowphase/detail/traversal/octree/distance/octree_mesh_distance_traversal_node-inl.h
-- Up-to-date: /usr/local/include/fcl/narrowphase/detail/traversal/traversal_node_base-inl.h
-- Up-to-date: /usr/local/include/fcl/narrowphase/detail/traversal/collision
-- Up-to-date: /usr/local/include/fcl/narrowphase/detail/traversal/collision/mesh_continuous_collision_traversal_node-inl.h
-- Up-to-date: /usr/local/include/fcl/narrowphase/detail/traversal/collision/mesh_shape_collision_traversal_node.h
-- Up-to-date: /usr/local/include/fcl/narrowphase/detail/traversal/collision/bvh_collision_traversal_node.h
-- Up-to-date: /usr/local/include/fcl/narrowphase/detail/traversal/collision/shape_bvh_collision_traversal_node.h
-- Up-to-date: /usr/local/include/fcl/narrowphase/detail/traversal/collision/collision_traversal_node_base-inl.h
-- Up-to-date: /usr/local/include/fcl/narrowphase/detail/traversal/collision/shape_mesh_collision_traversal_node.h
-- Up-to-date: /usr/local/include/fcl/narrowphase/detail/traversal/collision/bvh_shape_collision_traversal_node-inl.h
-- Up-to-date: /usr/local/include/fcl/narrowphase/detail/traversal/collision/bvh_shape_collision_traversal_node.h
-- Up-to-date: /usr/local/include/fcl/narrowphase/detail/traversal/collision/mesh_collision_traversal_node.h
-- Up-to-date: /usr/local/include/fcl/narrowphase/detail/traversal/collision/shape_collision_traversal_node.h
-- Up-to-date: /usr/local/include/fcl/narrowphase/detail/traversal/collision/intersect.h
-- Up-to-date: /usr/local/include/fcl/narrowphase/detail/traversal/collision/mesh_shape_collision_traversal_node-inl.h
-- Up-to-date: /usr/local/include/fcl/narrowphase/detail/traversal/collision/collision_traversal_node_base.h
-- Up-to-date: /usr/local/include/fcl/narrowphase/detail/traversal/collision/mesh_continuous_collision_traversal_node.h
-- Up-to-date: /usr/local/include/fcl/narrowphase/detail/traversal/collision/mesh_collision_traversal_node-inl.h
-- Up-to-date: /usr/local/include/fcl/narrowphase/detail/traversal/collision/shape_collision_traversal_node-inl.h
-- Up-to-date: /usr/local/include/fcl/narrowphase/detail/traversal/collision/bvh_collision_traversal_node-inl.h
-- Up-to-date: /usr/local/include/fcl/narrowphase/detail/traversal/collision/shape_bvh_collision_traversal_node-inl.h
-- Up-to-date: /usr/local/include/fcl/narrowphase/detail/traversal/collision/intersect-inl.h
-- Up-to-date: /usr/local/include/fcl/narrowphase/detail/traversal/collision/shape_mesh_collision_traversal_node-inl.h
-- Up-to-date: /usr/local/include/fcl/narrowphase/detail/traversal/traversal_recurse-inl.h
-- Up-to-date: /usr/local/include/fcl/narrowphase/detail/traversal/traversal_node_base.h
-- Up-to-date: /usr/local/include/fcl/narrowphase/detail/traversal/collision_node-inl.h
-- Up-to-date: /usr/local/include/fcl/narrowphase/detail/traversal/collision_node.h
-- Up-to-date: /usr/local/include/fcl/narrowphase/detail/traversal/distance
-- Up-to-date: /usr/local/include/fcl/narrowphase/detail/traversal/distance/shape_mesh_distance_traversal_node-inl.h
-- Up-to-date: /usr/local/include/fcl/narrowphase/detail/traversal/distance/conservative_advancement_stack_data.h
-- Up-to-date: /usr/local/include/fcl/narrowphase/detail/traversal/distance/shape_mesh_conservative_advancement_traversal_node.h
-- Up-to-date: /usr/local/include/fcl/narrowphase/detail/traversal/distance/shape_mesh_distance_traversal_node.h
-- Up-to-date: /usr/local/include/fcl/narrowphase/detail/traversal/distance/shape_distance_traversal_node-inl.h
-- Up-to-date: /usr/local/include/fcl/narrowphase/detail/traversal/distance/distance_traversal_node_base.h
-- Up-to-date: /usr/local/include/fcl/narrowphase/detail/traversal/distance/bvh_distance_traversal_node-inl.h
-- Up-to-date: /usr/local/include/fcl/narrowphase/detail/traversal/distance/mesh_distance_traversal_node.h
-- Up-to-date: /usr/local/include/fcl/narrowphase/detail/traversal/distance/shape_bvh_distance_traversal_node.h
-- Up-to-date: /usr/local/include/fcl/narrowphase/detail/traversal/distance/bvh_shape_distance_traversal_node-inl.h
-- Up-to-date: /usr/local/include/fcl/narrowphase/detail/traversal/distance/shape_distance_traversal_node.h
-- Up-to-date: /usr/local/include/fcl/narrowphase/detail/traversal/distance/shape_conservative_advancement_traversal_node-inl.h
-- Up-to-date: /usr/local/include/fcl/narrowphase/detail/traversal/distance/bvh_distance_traversal_node.h
-- Up-to-date: /usr/local/include/fcl/narrowphase/detail/traversal/distance/mesh_shape_distance_traversal_node.h
-- Up-to-date: /usr/local/include/fcl/narrowphase/detail/traversal/distance/shape_bvh_distance_traversal_node-inl.h
-- Up-to-date: /usr/local/include/fcl/narrowphase/detail/traversal/distance/mesh_distance_traversal_node-inl.h
-- Up-to-date: /usr/local/include/fcl/narrowphase/detail/traversal/distance/shape_mesh_conservative_advancement_traversal_node-inl.h
-- Up-to-date: /usr/local/include/fcl/narrowphase/detail/traversal/distance/shape_conservative_advancement_traversal_node.h
-- Up-to-date: /usr/local/include/fcl/narrowphase/detail/traversal/distance/distance_traversal_node_base-inl.h
-- Up-to-date: /usr/local/include/fcl/narrowphase/detail/traversal/distance/mesh_conservative_advancement_traversal_node-inl.h
-- Up-to-date: /usr/local/include/fcl/narrowphase/detail/traversal/distance/bvh_shape_distance_traversal_node.h
-- Up-to-date: /usr/local/include/fcl/narrowphase/detail/traversal/distance/mesh_shape_conservative_advancement_traversal_node-inl.h
-- Up-to-date: /usr/local/include/fcl/narrowphase/detail/traversal/distance/conservative_advancement_stack_data-inl.h
-- Up-to-date: /usr/local/include/fcl/narrowphase/detail/traversal/distance/mesh_conservative_advancement_traversal_node.h
-- Up-to-date: /usr/local/include/fcl/narrowphase/detail/traversal/distance/mesh_shape_distance_traversal_node-inl.h
-- Up-to-date: /usr/local/include/fcl/narrowphase/detail/traversal/distance/mesh_shape_conservative_advancement_traversal_node.h
-- Up-to-date: /usr/local/include/fcl/narrowphase/detail/primitive_shape_algorithm
-- Up-to-date: /usr/local/include/fcl/narrowphase/detail/primitive_shape_algorithm/box_box-inl.h
-- Up-to-date: /usr/local/include/fcl/narrowphase/detail/primitive_shape_algorithm/halfspace.h
-- Up-to-date: /usr/local/include/fcl/narrowphase/detail/primitive_shape_algorithm/box_box.h
-- Up-to-date: /usr/local/include/fcl/narrowphase/detail/primitive_shape_algorithm/sphere_capsule.h
-- Up-to-date: /usr/local/include/fcl/narrowphase/detail/primitive_shape_algorithm/sphere_sphere-inl.h
-- Up-to-date: /usr/local/include/fcl/narrowphase/detail/primitive_shape_algorithm/halfspace-inl.h
-- Up-to-date: /usr/local/include/fcl/narrowphase/detail/primitive_shape_algorithm/capsule_capsule-inl.h
-- Up-to-date: /usr/local/include/fcl/narrowphase/detail/primitive_shape_algorithm/plane-inl.h
-- Up-to-date: /usr/local/include/fcl/narrowphase/detail/primitive_shape_algorithm/sphere_triangle.h
-- Up-to-date: /usr/local/include/fcl/narrowphase/detail/primitive_shape_algorithm/sphere_triangle-inl.h
-- Up-to-date: /usr/local/include/fcl/narrowphase/detail/primitive_shape_algorithm/triangle_distance-inl.h
-- Up-to-date: /usr/local/include/fcl/narrowphase/detail/primitive_shape_algorithm/triangle_distance.h
-- Up-to-date: /usr/local/include/fcl/narrowphase/detail/primitive_shape_algorithm/capsule_capsule.h
-- Up-to-date: /usr/local/include/fcl/narrowphase/detail/primitive_shape_algorithm/sphere_capsule-inl.h
-- Up-to-date: /usr/local/include/fcl/narrowphase/detail/primitive_shape_algorithm/sphere_sphere.h
-- Up-to-date: /usr/local/include/fcl/narrowphase/detail/primitive_shape_algorithm/plane.h
-- Up-to-date: /usr/local/include/fcl/narrowphase/detail/gjk_solver_indep.h
-- Up-to-date: /usr/local/include/fcl/narrowphase/detail/distance_func_matrix-inl.h
-- Up-to-date: /usr/local/include/fcl/narrowphase/detail/gjk_solver_libccd.h
-- Up-to-date: /usr/local/include/fcl/narrowphase/detail/conservative_advancement_func_matrix-inl.h
-- Up-to-date: /usr/local/include/fcl/narrowphase/detail/gjk_solver_indep-inl.h
-- Up-to-date: /usr/local/include/fcl/narrowphase/distance_request-inl.h
-- Up-to-date: /usr/local/include/fcl/narrowphase/collision_result.h
-- Up-to-date: /usr/local/include/fcl/narrowphase/continuous_collision_result.h
-- Up-to-date: /usr/local/include/fcl/narrowphase/continuous_collision_result-inl.h
-- Up-to-date: /usr/local/include/fcl/narrowphase/contact-inl.h
-- Up-to-date: /usr/local/include/fcl/narrowphase/contact_point.h
-- Up-to-date: /usr/local/include/fcl/narrowphase/collision_object-inl.h
-- Up-to-date: /usr/local/include/fcl/narrowphase/distance_result-inl.h
-- Up-to-date: /usr/local/include/fcl/narrowphase/collision_request-inl.h
-- Up-to-date: /usr/local/include/fcl/narrowphase/continuous_collision_object-inl.h
-- Up-to-date: /usr/local/include/fcl/narrowphase/distance-inl.h
-- Up-to-date: /usr/local/include/fcl/narrowphase/collision-inl.h
-- Up-to-date: /usr/local/include/fcl/narrowphase/continuous_collision_request-inl.h
-- Up-to-date: /usr/local/include/fcl/narrowphase/contact_point-inl.h
-- Up-to-date: /usr/local/include/fcl/narrowphase/collision_object.h
-- Up-to-date: /usr/local/include/fcl/broadphase
-- Up-to-date: /usr/local/include/fcl/broadphase/broadphase_spatialhash.h
-- Up-to-date: /usr/local/include/fcl/broadphase/broadphase_collision_manager.h
-- Up-to-date: /usr/local/include/fcl/broadphase/broadphase_continuous_collision_manager-inl.h
-- Up-to-date: /usr/local/include/fcl/broadphase/broadphase_interval_tree.h
-- Up-to-date: /usr/local/include/fcl/broadphase/broadphase_continuous_collision_manager.h
-- Up-to-date: /usr/local/include/fcl/broadphase/broadphase_spatialhash-inl.h
-- Up-to-date: /usr/local/include/fcl/broadphase/broadphase_dynamic_AABB_tree_array-inl.h
-- Up-to-date: /usr/local/include/fcl/broadphase/broadphase_bruteforce.h
-- Up-to-date: /usr/local/include/fcl/broadphase/broadphase_SaP.h
-- Up-to-date: /usr/local/include/fcl/broadphase/broadphase_SaP-inl.h
-- Up-to-date: /usr/local/include/fcl/broadphase/broadphase_SSaP.h
-- Up-to-date: /usr/local/include/fcl/broadphase/broadphase_dynamic_AABB_tree_array.h
-- Up-to-date: /usr/local/include/fcl/broadphase/detail
-- Up-to-date: /usr/local/include/fcl/broadphase/detail/sparse_hash_table-inl.h
-- Up-to-date: /usr/local/include/fcl/broadphase/detail/node_base_array-inl.h
-- Up-to-date: /usr/local/include/fcl/broadphase/detail/hierarchy_tree_array-inl.h
-- Up-to-date: /usr/local/include/fcl/broadphase/detail/hierarchy_tree.h
-- Up-to-date: /usr/local/include/fcl/broadphase/detail/node_base.h
-- Up-to-date: /usr/local/include/fcl/broadphase/detail/spatial_hash-inl.h
-- Up-to-date: /usr/local/include/fcl/broadphase/detail/interval_tree_node-inl.h
-- Up-to-date: /usr/local/include/fcl/broadphase/detail/morton.h
-- Up-to-date: /usr/local/include/fcl/broadphase/detail/simple_hash_table.h
-- Up-to-date: /usr/local/include/fcl/broadphase/detail/hierarchy_tree-inl.h
-- Up-to-date: /usr/local/include/fcl/broadphase/detail/simple_interval-inl.h
-- Up-to-date: /usr/local/include/fcl/broadphase/detail/morton-inl.h
-- Up-to-date: /usr/local/include/fcl/broadphase/detail/sparse_hash_table.h
-- Up-to-date: /usr/local/include/fcl/broadphase/detail/simple_interval.h
-- Up-to-date: /usr/local/include/fcl/broadphase/detail/interval_tree.h
-- Up-to-date: /usr/local/include/fcl/broadphase/detail/interval_tree_node.h
-- Up-to-date: /usr/local/include/fcl/broadphase/detail/interval_tree-inl.h
-- Up-to-date: /usr/local/include/fcl/broadphase/detail/node_base-inl.h
-- Up-to-date: /usr/local/include/fcl/broadphase/detail/node_base_array.h
-- Up-to-date: /usr/local/include/fcl/broadphase/detail/spatial_hash.h
-- Up-to-date: /usr/local/include/fcl/broadphase/detail/hierarchy_tree_array.h
-- Up-to-date: /usr/local/include/fcl/broadphase/detail/simple_hash_table-inl.h
-- Up-to-date: /usr/local/include/fcl/broadphase/broadphase_dynamic_AABB_tree-inl.h
-- Up-to-date: /usr/local/include/fcl/broadphase/broadphase_dynamic_AABB_tree.h
-- Up-to-date: /usr/local/include/fcl/broadphase/broadphase_SSaP-inl.h
-- Up-to-date: /usr/local/include/fcl/broadphase/broadphase_bruteforce-inl.h
-- Up-to-date: /usr/local/include/fcl/broadphase/broadphase_interval_tree-inl.h
-- Up-to-date: /usr/local/include/fcl/broadphase/broadphase_collision_manager-inl.h
-- Up-to-date: /usr/local/include/fcl/common
-- Up-to-date: /usr/local/include/fcl/common/types.h
-- Up-to-date: /usr/local/include/fcl/common/warning.h
-- Up-to-date: /usr/local/include/fcl/common/deprecated.h
-- Up-to-date: /usr/local/include/fcl/common/profiler.h
-- Up-to-date: /usr/local/include/fcl/common/detail
-- Up-to-date: /usr/local/include/fcl/common/detail/profiler.h
-- Up-to-date: /usr/local/include/fcl/common/unused.h
-- Up-to-date: /usr/local/include/fcl/common/exception.h
-- Up-to-date: /usr/local/include/fcl/common/time.h
-- Up-to-date: /usr/local/include/fcl/geometry
-- Up-to-date: /usr/local/include/fcl/geometry/octree
-- Up-to-date: /usr/local/include/fcl/geometry/octree/octree-inl.h
-- Up-to-date: /usr/local/include/fcl/geometry/octree/octree.h
-- Up-to-date: /usr/local/include/fcl/geometry/collision_geometry.h
-- Up-to-date: /usr/local/include/fcl/geometry/geometric_shape_to_BVH_model-inl.h
-- Up-to-date: /usr/local/include/fcl/geometry/shape
-- Up-to-date: /usr/local/include/fcl/geometry/shape/ellipsoid-inl.h
-- Up-to-date: /usr/local/include/fcl/geometry/shape/sphere.h
-- Up-to-date: /usr/local/include/fcl/geometry/shape/halfspace.h
-- Up-to-date: /usr/local/include/fcl/geometry/shape/cone.h
-- Up-to-date: /usr/local/include/fcl/geometry/shape/box-inl.h
-- Up-to-date: /usr/local/include/fcl/geometry/shape/utility.h
-- Up-to-date: /usr/local/include/fcl/geometry/shape/convex.h
-- Up-to-date: /usr/local/include/fcl/geometry/shape/cylinder-inl.h
-- Up-to-date: /usr/local/include/fcl/geometry/shape/halfspace-inl.h
-- Up-to-date: /usr/local/include/fcl/geometry/shape/shape_base-inl.h
-- Up-to-date: /usr/local/include/fcl/geometry/shape/cylinder.h
-- Up-to-date: /usr/local/include/fcl/geometry/shape/capsule.h
-- Up-to-date: /usr/local/include/fcl/geometry/shape/convex-inl.h
-- Up-to-date: /usr/local/include/fcl/geometry/shape/utility-inl.h
-- Up-to-date: /usr/local/include/fcl/geometry/shape/plane-inl.h
-- Up-to-date: /usr/local/include/fcl/geometry/shape/sphere-inl.h
-- Up-to-date: /usr/local/include/fcl/geometry/shape/capsule-inl.h
-- Up-to-date: /usr/local/include/fcl/geometry/shape/box.h
-- Up-to-date: /usr/local/include/fcl/geometry/shape/triangle_p-inl.h
-- Up-to-date: /usr/local/include/fcl/geometry/shape/triangle_p.h
-- Up-to-date: /usr/local/include/fcl/geometry/shape/cone-inl.h
-- Up-to-date: /usr/local/include/fcl/geometry/shape/shape_base.h
-- Up-to-date: /usr/local/include/fcl/geometry/shape/plane.h
-- Up-to-date: /usr/local/include/fcl/geometry/shape/ellipsoid.h
-- Up-to-date: /usr/local/include/fcl/geometry/collision_geometry-inl.h
-- Up-to-date: /usr/local/include/fcl/geometry/geometric_shape_to_BVH_model.h
-- Up-to-date: /usr/local/include/fcl/geometry/bvh
-- Up-to-date: /usr/local/include/fcl/geometry/bvh/BVH_internal.h
-- Up-to-date: /usr/local/include/fcl/geometry/bvh/BVH_utility.h
-- Up-to-date: /usr/local/include/fcl/geometry/bvh/BV_node_base.h
-- Up-to-date: /usr/local/include/fcl/geometry/bvh/BVH_utility-inl.h
-- Up-to-date: /usr/local/include/fcl/geometry/bvh/detail
-- Up-to-date: /usr/local/include/fcl/geometry/bvh/detail/BV_splitter_base.h
-- Up-to-date: /usr/local/include/fcl/geometry/bvh/detail/BV_splitter-inl.h
-- Up-to-date: /usr/local/include/fcl/geometry/bvh/detail/BV_fitter_base.h
-- Up-to-date: /usr/local/include/fcl/geometry/bvh/detail/BV_fitter.h
-- Up-to-date: /usr/local/include/fcl/geometry/bvh/detail/BVH_front.h
-- Up-to-date: /usr/local/include/fcl/geometry/bvh/detail/BV_splitter.h
-- Up-to-date: /usr/local/include/fcl/geometry/bvh/detail/BV_fitter-inl.h
-- Up-to-date: /usr/local/include/fcl/geometry/bvh/BV_node.h
-- Up-to-date: /usr/local/include/fcl/geometry/bvh/BVH_model.h
-- Up-to-date: /usr/local/include/fcl/geometry/bvh/BV_node-inl.h
-- Up-to-date: /usr/local/include/fcl/geometry/bvh/BVH_model-inl.h
-- Up-to-date: /usr/local/include/fcl/math
-- Up-to-date: /usr/local/include/fcl/math/geometry-inl.h
-- Up-to-date: /usr/local/include/fcl/math/geometry.h
-- Up-to-date: /usr/local/include/fcl/math/variance3.h
-- Up-to-date: /usr/local/include/fcl/math/variance3-inl.h
-- Up-to-date: /usr/local/include/fcl/math/rng-inl.h
-- Up-to-date: /usr/local/include/fcl/math/constants.h
-- Up-to-date: /usr/local/include/fcl/math/detail
-- Up-to-date: /usr/local/include/fcl/math/detail/project-inl.h
-- Up-to-date: /usr/local/include/fcl/math/detail/polysolver-inl.h
-- Up-to-date: /usr/local/include/fcl/math/detail/polysolver.h
-- Up-to-date: /usr/local/include/fcl/math/detail/project.h
-- Up-to-date: /usr/local/include/fcl/math/detail/seed.h
-- Up-to-date: /usr/local/include/fcl/math/triangle.h
-- Up-to-date: /usr/local/include/fcl/math/rng.h
-- Up-to-date: /usr/local/include/fcl/math/sampler
-- Up-to-date: /usr/local/include/fcl/math/sampler/sampler_se3_euler_ball.h
-- Up-to-date: /usr/local/include/fcl/math/sampler/sampler_se3_quat.h
-- Up-to-date: /usr/local/include/fcl/math/sampler/sampler_r-inl.h
-- Up-to-date: /usr/local/include/fcl/math/sampler/sampler_base.h
-- Up-to-date: /usr/local/include/fcl/math/sampler/sampler_se3_euler.h
-- Up-to-date: /usr/local/include/fcl/math/sampler/sampler_se2_disk.h
-- Up-to-date: /usr/local/include/fcl/math/sampler/sampler_r.h
-- Up-to-date: /usr/local/include/fcl/math/sampler/sampler_se3_quat_ball-inl.h
-- Up-to-date: /usr/local/include/fcl/math/sampler/sampler_se2_disk-inl.h
-- Up-to-date: /usr/local/include/fcl/math/sampler/sampler_se3_euler_ball-inl.h
-- Up-to-date: /usr/local/include/fcl/math/sampler/sampler_se2-inl.h
-- Up-to-date: /usr/local/include/fcl/math/sampler/sampler_se2.h
-- Up-to-date: /usr/local/include/fcl/math/sampler/sampler_se3_euler-inl.h
-- Up-to-date: /usr/local/include/fcl/math/sampler/sampler_se3_quat-inl.h
-- Up-to-date: /usr/local/include/fcl/math/sampler/sampler_se3_quat_ball.h
-- Up-to-date: /usr/local/include/fcl/math/bv
-- Up-to-date: /usr/local/include/fcl/math/bv/OBB-inl.h
-- Up-to-date: /usr/local/include/fcl/math/bv/AABB.h
-- Up-to-date: /usr/local/include/fcl/math/bv/RSS.h
-- Up-to-date: /usr/local/include/fcl/math/bv/AABB-inl.h
-- Up-to-date: /usr/local/include/fcl/math/bv/kDOP.h
-- Up-to-date: /usr/local/include/fcl/math/bv/kIOS.h
-- Up-to-date: /usr/local/include/fcl/math/bv/utility.h
-- Up-to-date: /usr/local/include/fcl/math/bv/OBB.h
-- Up-to-date: /usr/local/include/fcl/math/bv/OBBRSS-inl.h
-- Up-to-date: /usr/local/include/fcl/math/bv/RSS-inl.h
-- Up-to-date: /usr/local/include/fcl/math/bv/kDOP-inl.h
-- Up-to-date: /usr/local/include/fcl/math/bv/utility-inl.h
-- Up-to-date: /usr/local/include/fcl/math/bv/kIOS-inl.h
-- Up-to-date: /usr/local/include/fcl/math/bv/OBBRSS.h
-- Up-to-date: /usr/local/include/fcl/math/motion
-- Up-to-date: /usr/local/include/fcl/math/motion/motion_base.h
-- Up-to-date: /usr/local/include/fcl/math/motion/tbv_motion_bound_visitor.h
-- Up-to-date: /usr/local/include/fcl/math/motion/spline_motion.h
-- Up-to-date: /usr/local/include/fcl/math/motion/taylor_model
-- Up-to-date: /usr/local/include/fcl/math/motion/taylor_model/taylor_matrix-inl.h
-- Up-to-date: /usr/local/include/fcl/math/motion/taylor_model/taylor_vector-inl.h
-- Up-to-date: /usr/local/include/fcl/math/motion/taylor_model/interval_vector.h
-- Up-to-date: /usr/local/include/fcl/math/motion/taylor_model/taylor_model.h
-- Up-to-date: /usr/local/include/fcl/math/motion/taylor_model/time_interval-inl.h
-- Up-to-date: /usr/local/include/fcl/math/motion/taylor_model/taylor_model-inl.h
-- Up-to-date: /usr/local/include/fcl/math/motion/taylor_model/interval_vector-inl.h
-- Up-to-date: /usr/local/include/fcl/math/motion/taylor_model/taylor_vector.h
-- Up-to-date: /usr/local/include/fcl/math/motion/taylor_model/time_interval.h
-- Up-to-date: /usr/local/include/fcl/math/motion/taylor_model/interval-inl.h
-- Up-to-date: /usr/local/include/fcl/math/motion/taylor_model/interval_matrix-inl.h
-- Up-to-date: /usr/local/include/fcl/math/motion/taylor_model/taylor_matrix.h
-- Up-to-date: /usr/local/include/fcl/math/motion/taylor_model/interval_matrix.h
-- Up-to-date: /usr/local/include/fcl/math/motion/taylor_model/interval.h
-- Up-to-date: /usr/local/include/fcl/math/motion/translation_motion.h
-- Up-to-date: /usr/local/include/fcl/math/motion/interp_motion.h
-- Up-to-date: /usr/local/include/fcl/math/motion/bv_motion_bound_visitor.h
-- Up-to-date: /usr/local/include/fcl/math/motion/screw_motion.h
-- Up-to-date: /usr/local/include/fcl/math/motion/motion_base-inl.h
-- Up-to-date: /usr/local/include/fcl/math/motion/triangle_motion_bound_visitor-inl.h
-- Up-to-date: /usr/local/include/fcl/math/motion/spline_motion-inl.h
-- Up-to-date: /usr/local/include/fcl/math/motion/screw_motion-inl.h
-- Up-to-date: /usr/local/include/fcl/math/motion/interp_motion-inl.h
-- Up-to-date: /usr/local/include/fcl/math/motion/triangle_motion_bound_visitor.h
-- Up-to-date: /usr/local/include/fcl/math/motion/tbv_motion_bound_visitor-inl.h
-- Up-to-date: /usr/local/include/fcl/math/motion/translation_motion-inl.h
-- Up-to-date: /usr/local/lib/pkgconfig/fcl.pc
-- Up-to-date: /usr/local/include/fcl/config.h
-- Up-to-date: /usr/local/include/fcl/fcl.h
-- Up-to-date: /usr/local/lib/libfcl.so.0.6.0
-- Up-to-date: /usr/local/lib/libfcl.so.0.6
-- Up-to-date: /usr/local/lib/libfcl.so
-- Up-to-date: /usr/local/lib/cmake/fcl/fclConfig.cmake
-- Up-to-date: /usr/local/lib/cmake/fcl/fclConfig-release.cmake

