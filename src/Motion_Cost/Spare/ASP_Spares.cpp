
// LAB FRAME
bool Assembly_ASM::test_translation_sub_removal( std::vector<size_t> movedParts , std::vector<size_t> referenceParts , 
												 const Eigen::Vector3d& direction , double distance , usll samples ){
	// Test removing 'movedParts' along 'direction' in incremental steps , Each step checking for collision with 'referenceParts'
	// NOTE: This function assumes that the assembly as a whole has already updated the poses of all constituent parts
	
	bool SHOWDEBUG = false; // if( SHOWDEBUG ){  
	
	size_t len_move = movedParts.size()     , 
		   len_refc = referenceParts.size() ;
	Eigen::Vector3d  posIncr = direction.normalized() * ( distance / (double)samples );
	Eigen::Vector3d  savedPos;
	PartEntry_ASM*   movePartEntry = nullptr ; 
	PartEntry_ASM*   refcPartEntry = nullptr ;
	bool collisionFAIL = false;
	usll counter = 0;
	
	if( SHOWDEBUG ){  cout << "DEBUG , " << "There are " << partsList.size() << " parts in this assembly" << endl;  }
	
	// 1. For each of the parts to be translated
	for( size_t m = 0 ; m < len_move ; m++ ){
		// 2. Save the current position of the moved part
		
		if( SHOWDEBUG ){  cout << "DEBUG , " << "Attempting to fetch part " << movedParts[m] << " from the list of parts." << endl;  }
		
		movePartEntry = elem_i_from_list( partsList , movedParts[m] );
		if( movePartEntry ){
			savedPos = movePartEntry->part.get_position();
			counter = 0;
			// 3. For each incremental translation
			for( usll i = 1 ; i <= samples ; i++ ){
				movePartEntry->part.set_position( savedPos + posIncr * i );
				counter++;
				// 4. For each of the parts to be checked against
				for( size_t r = 0 ; r < len_refc ; r++ ){
					
					if( SHOWDEBUG ){  cout << "DEBUG , " << "Attempting to fetch part " << referenceParts[r] << " from the list of parts." << endl;  }
					
					refcPartEntry = elem_i_from_list( partsList , referenceParts[r] );
					if( refcPartEntry ){
						// 5. Check collision
						collisionFAIL = movePartEntry->part.collides_with( refcPartEntry->part );
						
						if( collisionFAIL ) 
							if( SHOWDEBUG ){  cout << "DEBUG , " 
												   << "Collision! Mover " << movedParts[m] << " with Reference " << referenceParts[r] 
												   << " after " << counter << " steps!" << endl;
						    }
						
						// 6. If there is a collision shortcut to FAILURE
						if( collisionFAIL ) break;
					}else{  cout << "WARN , Assembly_ASM::test_translation_sub_removal: FAILED TO RETRIEVE REFERENCE PART!" << endl;  }
				}
				if( collisionFAIL ) break;
			}
			// 8. Restore the part to its original position
			movePartEntry->part.set_position( savedPos );
			// 7. If there is a failure , Shortcut from outer loop
			if( collisionFAIL ) break;
		}else{  cout << "WARN , Assembly_ASM::test_translation_sub_removal: FAILED TO RETRIEVE MOVED PART!" << endl;  }
		if( collisionFAIL ) break;
	}
	
	// 9. Return result
	return !collisionFAIL;
}

//////////////////////////////////////////////////////////////////////////////////////////////////////////

void record_level_plan( LevelPlanNode* levelNode , PlanRecording& recording , size_t numRotDwellFrames ){
	
	bool SHOWDEBUG = true; // if( SHOWDEBUG ){  cout << "" << endl;  }
	
	if( SHOWDEBUG ){  sep( "record_level_plan" , 3 );  }
	
	size_t /*- */ len /*- */ = levelNode->levelPlan.size() , 
				  numFrames  = 0;
	ActionSpec*   currAction = nullptr;
	Assembly_ASM* movdAsm    = nullptr; // -- Lifted asm
	Assembly_ASM* refcAsm    = nullptr; // -- Remaining asm
	SuccessPath   stateSeq   = empty_SuccessPath();
	PlayerFrame*  currframe  = nullptr;
	std::vector<double> dummyJoints = {0,0,0,0,0,0,0};
	std::vector<double> dummyGrippr = {0,0};
	// 1. For each action in the level
	for( size_t i = 0 ; i < len ; i++ ){
		currAction = levelNode->levelPlan[i];
		// 2. Determine the action type
		switch( currAction->action ){
			// 3. If REMOVE
			case ACT_REMOVE:
				if( SHOWDEBUG ){  cout << "About to load remove action" << endl;  }
				// A. Load assemblies
				//~ movdAsm = currAction->movdAsm;
				//~ refcAsm = currAction->refcAsm;
				// B. Load sequence
				stateSeq  = currAction->liftAction;
				numFrames = stateSeq.path.size();
				// B. For each frame
				for( size_t j = 0 ; j < numFrames ; j++ ){
					clog << ".";
					// a. Instantiate frame 
					currframe = new PlayerFrame();
					// a. Set pose
					movdAsm->set_pose( stateSeq.path[j] );
					// b. Set markers
					//~ movdAsm->load_part_mrkrs_into_mngr( currframe->mrkrMngr );
					//~ refcAsm->load_part_mrkrs_into_mngr( currframe->mrkrMngr );
					// c. Set joint states
					pack_joint_and_gripper_states_into_msg( stateSeq.qSeq[j] , //- Joint positions of arm
											                stateSeq.gripState , // Joint positions of gripper
											                currframe->jnt_state_ );
					recording.push_back( currframe );
				}
				if( SHOWDEBUG ){  cout << "Complete" << endl;  }
				break;
			// 4. If ROTATE
			case ACT_ROTATE:
				if( SHOWDEBUG ){  cout << "About to load rotate action" << endl;  }
				// A. Load assembly
				//~ refcAsm = currAction->refcAsm;
				// B. For each frame
				for( size_t j = 0 ; j < numFrames ; j++ ){
					clog << ".";
					// a. Dwell for 'numRotDwellFrames' frames
					currframe = new PlayerFrame();
					//~ refcAsm->load_part_mrkrs_into_mngr( currframe->mrkrMngr );
					pack_joint_and_gripper_states_into_msg( dummyJoints , //- Joint positions of arm
											                dummyGrippr , // Joint positions of gripper
											                currframe->jnt_state_ );
					recording.push_back( currframe );
				}
				if( SHOWDEBUG ){  cout << "Complete" << endl;  }
				break;
			// 5. If NO_OPR , skip
			case ACT_NO_OPR:
			default:
				cout << "ACTION WAS BAD OR EMPTY" << endl;
		}
	}
	if( SHOWDEBUG ){  cout << "ACTION RECORD COMPLETE" << endl;  }
}

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

template< typename IK >
std::vector<ActionSpec*> tree_plan_exec_one_level( AsmStateNode_ASP* planRoot , Floor_ASM& floor , IK IK_clientCall , 
												   ValidationParams params ,
												   double CRIT_ANG = GEO_CRIT_ANG ){
	// Get all of the robot actions that will reduce the root node to each of its successors
	// NOTE: This function assumes that 'planRoot' has at least one outgoing edge
	
	bool SHOWDEBUG = true; // if( SHOWDEBUG ){  cout << "" << endl;  }
	
	std::vector<ActionSpec*> rtnActions;
	
	if( SHOWDEBUG ){  cout << "Init ..." << endl;  }
	
	// ~ Algo Vars ~
	Assembly_ASM* /*-*/ rootAsm      = planRoot->assembly; // Fetch the assembly from which subs will be removed
	if( SHOWDEBUG ){  cout << "Did we get a root assembly?: " << yesno( rootAsm ) << endl
						   << "About to replicate ..." << endl;  }
	if( !(rootAsm) ){  return rtnActions;  } // If the plan contained no assembly , Return empty
	Assembly_ASM* /*-*/ runningAsm   = rootAsm->replicate(); // The assembly will shrink as we work with it // NOTE: Pose copied
	std::vector<llin>   runningIDs   = runningAsm->get_part_ID_vec();
	size_t /* ------ */ numEdges     = 0 , // -------------- Number of edges below the root node
						chosenSupprt = 0 ; // ---------- Support that allows the present operation
	std::vector<size_t> edgeDices; // ------------- All of the edge indices
	std::queue<size_t>  execOrder; // ------------------ Controls the order in which we execute edges
	std::set<size_t>    plannedEdges;
	size_t /* ------ */ maxRetries   = numEdges + params.maxReplanFailures , 
						currPrec     = 0;
	
	// ~~~ LAMBDA FUNCS ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
	
	auto demote_edge = [&]( AsmStateNode_ASP* edgeNode ){
		edgeNode->actionData.precedence++;
		return edgeNode->actionData.precedence;
	};
	
	auto get_R_flag = [&]( AsmStateNode_ASP* edgeNode ){
		return edgeNode->actionData.rotateFlag;
	};
	
	auto set_R_flag = [&]( AsmStateNode_ASP* edgeNode , bool val ){
		edgeNode->actionData.rotateFlag = val;
	};
	
	auto reset_flags = [&](){
		for( size_t k = 0 ; k < numEdges ; k++ ){  set_R_flag( planRoot->outgoingEdges[k] , false );  }
	};
	
	auto append_rotate = [&]( Assembly_ASM* nuAsm ){
		// NOTE: This function assumes that 'nuAsm' has been posed and its support set
		rtnActions.push_back(
			new ActionSpec( nullptr , nuAsm , magic_success_rotate() , ACT_ROTATE )
		);
	};
	
	auto append_remove = [&]( Assembly_ASM* movdAsm , Assembly_ASM* refcAsm , SuccessPath actInfo ){
		// NOTE: This function assumes that 'nuAsm' has been posed and its support set
		rtnActions.push_back(
			new ActionSpec( movdAsm , refcAsm , actInfo , ACT_REMOVE )
		);
	};
	
	auto append_failed = [&]( const std::vector<llin>& edgeParts ){
		// NOTE: This function assumes that 'nuAsm' has been posed and its support set
		ActionSpec* failedAction = new ActionSpec( nullptr , nullptr , empty_SuccessPath() , ACT_NO_OPR );
		failedAction->IDvec = vec_copy( edgeParts );
		rtnActions.push_back( failedAction );
	};
	
	// ,,, END LAMBDA ,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,
	
	// ~~ Phase 0: Init ~~
	//  A. Get the number of edges
	numEdges = planRoot->outgoingEdges.size();
	if( numEdges > 0 ){
		//  B. Get a list of edge indices
		edgeDices = vec_range( (size_t)0 , numEdges-1 );
	}else{
		if( SHOWDEBUG ){  cout << "There were no edges!" << endl;  }
		return rtnActions;
	}
	
	if( SHOWDEBUG ){  cout << "There are " << numEdges << " edges" << endl 
						   << "Finding the best starting support ..." << endl;  }
	
	IndexMatches supportInfo = best_starting_support_for_level( planRoot , params );
	
	// ~~ Phase 1: Beginning Pose ~~
	
	//   1. Set the assembly to the requested pose
	runningAsm->set_lab_pose_for_index_3D( supportInfo.bestDex , params.setdownLoc );
	//   2. Enque Matches
	enqueue_vec( execOrder , supportInfo.indices );
	//   3. Enque Others
	enqueue_vec( execOrder , vec_copy_without_elem( edgeDices , supportInfo.indices ) );
	// NOTE: The asm will be changing over time, so we can't necessarily depend upon this order to be maintained as we go
	
	size_t counter = 0;
	
	
	// ~~ Phase 2: Plan Edges ~~
	
	size_t /* ---- */ currEdgeDex = 0; // --------- Index of the edge currently being planned
	AsmStateNode_ASP* currEdge    = nullptr; // --- Edge currently being planned
	Assembly_ASM*     currAsm     = nullptr; // --- Assembly currently being planned
	Eigen::Vector3d   remDir; // ------------------ Suggested removal direction
	bool /* ------ */ currFlag = false , // ------- Rotation flag of the inspected node
					  chkPass  = false , // ------- Flag 1
					  chekTwo  = false ; // ------- Flag 2  
	Assembly_ASM*     tempAsm; // ----------------- Temp asm
	std::vector<llin> subPartIDs , // ------------- Parts contained in the edge sub
					  currBalanc ; // ------------- Remainder parts for the assembly being looked at
	IndexSuccesLookup supportVote; // ------------- Info about which support is best for a certain action
	SuccessPath /*-*/ liftPath; // ---------------- Execution info for a single action
	ValidationCode    edgeCode; // ---------------- Result from the lift planner
	IndexSearchResult supportMatch; // ------------ Result from a support search (from pose)
	
	//  4. While there are edges to plan
	while( execOrder.size() > 0 ){
		if( SHOWDEBUG ){  sep( "Iteration " + to_string( counter + 1 ) , 1 );  counter++;  }  
		
		//  5. Pop Edge
		currEdgeDex = queue_get_pop( execOrder );
		currEdge    = planRoot->outgoingEdges[ currEdgeDex ];
		currAsm     = currEdge->assembly;
		subPartIDs  = currAsm->get_part_ID_vec();
		currBalanc  = runningAsm->get_balance_IDs_from_sub( subPartIDs );
		currFlag    = get_R_flag( currEdge );
		currPrec    = currEdge->actionData.precedence;
		
		//  6. Check that the balance is not zero
		if( currBalanc.size() == 0 ){  
			if( SHOWDEBUG ){  cout << "Balance is zero, Skipping loop ..." << endl;  }
			// A. We don't have to plan this edge if we have already created this state , Mark Planned
			plannedEdges.insert( currEdgeDex );
			// B. There shouldn't be other things , Just let the queue run out
			continue;  
		}
		
		//  7. Check that the max number of replans has not been exceeded for this sub
		if( currPrec > maxRetries ){
			// A. If so, let it fall off the queue , It will be noted when we scan for unplanned edges
			continue;  
		}
		
		//  8. Check that the current edge has not already been planned , If so skip and let it fall off of the queue
		if( is_arg_in_set( currEdgeDex , plannedEdges ) ){
			continue;
		}
		
		//  9. Obtain the suggested removal direction
		remDir = runningAsm->asm_center_of_freedom_direction( subPartIDs , currBalanc ); // Part ID
		if( SHOWDEBUG ){  cout << "Suggested removal direction: " << remDir << endl;  }
		// NOTE: This function assumes that 'remDir' is valid, otherwise it would not have been added to the precedence layer
	
	
		//  A. If the direction is downward-facing , Assume that it hits the floor and requeue
		if( remDir.dot( Eigen::Vector3d::UnitZ() ) < 0 ){
			if( SHOWDEBUG ){  cout << "Suggested removal direction points downward: Delay or Rotate? " << endl;  }
			// B. If the flag was not set , requeue without reprioritization
			if( !currFlag ){
				if( SHOWDEBUG ){  cout << "Flag was unset: Delay" << endl;  }
				set_R_flag( currEdge , true );
				execOrder.push( currEdgeDex );
				continue;
			}else{
			//  C. Else the flag was set , Must rotate!
				if( SHOWDEBUG ){  cout << "Flag was set: Rotate" << endl;  }
				//  D. Suggest a support to set the running assembly at
				supportVote = runningAsm->suggest_support_for_staightline_stability( subPartIDs , currBalanc , 
																		             params.distance , params.numSamples ); 
				//  E. Replicate the assembly
				tempAsm = runningAsm->replicate();
				//  F. Rotate the assembly
				tempAsm  -> set_lab_pose_for_index_3D( supportVote.bestDex , params.setdownLoc , true );
				runningAsm->set_lab_pose_for_index_3D( supportVote.bestDex , params.setdownLoc , true );
				//  G. Record action
				append_rotate( tempAsm );
				//  H. The assembly is in a new orientation and the info we have about each is old , Unset flags
				reset_flags();
			}
		}
		
		// ~ At this point assume that 'runningAsm' is in an orientation that supports the current edge ~
		
		// 10. Send the edge to the lift planner
		
		liftPath = plan_one_removal_action_multimode( runningAsm , subPartIDs , currBalanc ,
													  floor , params ,
													  IK_clientCall , 
											          CRIT_ANG );
		
		// 11. Handle success / failure cases
		edgeCode = liftPath.code;
		
		//~ enum ValidationCode{
			//~ // ~ FAIL ~
			//~ ERROR_INCOMPLETE , ok
			//~ NEW_SUB_UNSTABLE , ok
			//~ NEW_SUB_DISJOINT , ok
			//~ FLOATING_HAM_ERR , ok
			//~ REMDIR_NOT_FOUND , ok
			//~ GOALPOSE_NOT_FND , ok
			//~ RRTSTAR_NO_SOLTN , ok
			//~ ALL_GRASP_COLLID , ok
			//~ GRASP_IK_FAILURE , ok
			//~ // ~ PASS ~
			//~ PASS_STRAIGHTLIN , ok
			//~ PASS_RRTSTAR_PTH , ok
			//~ ROTATED_OK_MAGIC , See above
		//~ };
		
		switch( edgeCode ){
			
			// ~ A. PASS ~
			
			case PASS_STRAIGHTLIN:
			case PASS_RRTSTAR_PTH:
				// a. Place the removed asm in its pre-removal location , just so we know where it is , Should not affect playback
				currAsm->set_pose( runningAsm->get_pose() );
				// b. Generate a reference assembly
				tempAsm = runningAsm->sub_from_spec( currBalanc );
				// c. Situate the reference to match the stationary running assembly
				tempAsm->set_pose( runningAsm->get_pose() );
				supportMatch = tempAsm->set_support_from_current_pose();
				if( !supportMatch.result ){  if( SHOWDEBUG ){  cout << "Did not find a support to match the pose!" << endl;  }  }
				// d. Update the balance to reflect the removed sub
				runningAsm = tempAsm->replicate();
				// e. Record action
				append_remove( currAsm->replicate() , tempAsm , liftPath );
				// f. Flag planned
				plannedEdges.insert( currEdgeDex );
				break;
				
			// ~ B. UNSTABLE ~
			case NEW_SUB_UNSTABLE:
				if( SHOWDEBUG ){  cout << "Reference unstable , Requeue" << endl;  }
				set_R_flag( currEdge , true );
				execOrder.push( currEdgeDex );
				break;
			
			// ~ C. DISJOINT / FLOATING ~
			case NEW_SUB_DISJOINT:
			case FLOATING_HAM_ERR:
				if( SHOWDEBUG ){  cout << "Reference unstable and/or floating , Demote & Requeue" << endl;  }
				set_R_flag( currEdge , true );
				demote_edge( currEdge );
				execOrder.push( currEdgeDex );
				break;
				
			// ~ D. LIFT FAILURE ~ // Maybe there is a blockage?
			case ERROR_INCOMPLETE:
			case REMDIR_NOT_FOUND:
			case GOALPOSE_NOT_FND:
			case RRTSTAR_NO_SOLTN:
			case ALL_GRASP_COLLID:
			case GRASP_IK_FAILURE:
				if( SHOWDEBUG ){  cout << "Lift planner returned failure , Demote & Requeue" << endl;  }
				demote_edge( currEdge );
				execOrder.push( currEdgeDex );
				break;
			
				
			default:
				if( SHOWDEBUG ){  cout << "SHOULD NOT HAVE REACHED THIS BRANCH: " << edgeCode 
									   << " , " << interpret_ValidationCode( edgeCode ) << endl;  }
			
		}
	}	
	
	// 12. Report on unplanned edges
	if( edgeDices.size() != plannedEdges.size() ){
		std::vector<size_t> badEdges = vec_copy_without_elem( edgeDices , plannedEdges );
		if( SHOWDEBUG ){  cout << "ERROR: UNPLANNED EDGE ACTIONS: " << badEdges << endl;  }
		for( size_t i = 0 ; i < badEdges.size() ; i++ ){
			append_failed( planRoot->outgoingEdges[ badEdges[i] ]->assembly->get_part_ID_vec() );
		}
	}	
	
	// N. Return
	return rtnActions;
}

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

template< typename IK >
SuccessPath plan_one_removal_action_multimode( Assembly_ASM* original , std::vector<llin> movd , std::vector<llin> refc ,
											   Floor_ASM& floor , ValidationParams params ,
											   IK IK_clientCall , 
											   double CRIT_ANG = GEO_CRIT_ANG ){
	// Attempt to plan a removal action with a robot arm and the floor
	// NOTE: This function assumes that 'original' has been posed on 'floor' in a way that allows lifting
	// NOTE: This function assumes that the support index for 'original' has been properly set
	
	bool SHOWDEBUG = true; // if( SHOWDEBUG ){  cout << "" << endl;  }
	
	SuccessPath result = empty_SuccessPath(); // Return structure
	stringstream diagnosticStr; // ------------- Diagnostic string
	
	// ~~ Phase 0: Init ~~
	if( SHOWDEBUG ){  cout << "Init ..." << endl;  }
	
	// 0. Instantiate assemblies and pose
	Assembly_ASM* movdAsm = original->sub_from_spec( movd );	movdAsm->set_pose( original->get_pose() );
	Assembly_ASM* refcAsm = original->sub_from_spec( refc );	refcAsm->set_pose( original->get_pose() );
	
	std::vector<double> /* ---- */ q1; // ---------- Joint state 1
	std::vector<double> /* ---- */ q2; // ---------- Joint state 2
	std::vector<double> /* ---- */ q_i; // --------- Joint state iterate
	std::vector<double> /* ---- */ jointSeed = {0,0,0,0,0,0,0};
	std::vector<double> /* ---- */ crrntSeed;
	bool /* ------------------- */ chkPass = false ,
								   chekTwo = false ,
								   chkThr3 = false ;
	std::vector<std::vector<llin>> connSubGraphs;
	size_t /* ----------------- */ len;
	Pose_ASP /* ------ */ relHandPose;
	Pose_ASP /* ------ */ bgnPartPose = movdAsm->get_pose();
	Pose_ASP /* ------ */ endPartPose;
	Pose_ASP /* ------ */ endHandPose;
	std::vector<Pose_ASP> poseSeq;
	Eigen::Vector3d remDir; // ------------- Suggestion from the assembly
	Eigen::Vector3d curDir; // Used for direction check
	Eigen::MatrixXd labConstraints; // -- Constraints on a sub in the lab frame
	Eigen::MatrixXd labCorners; // ------ Contraint corners for a sub in the lab frame
	Eigen::MatrixXd freeConstrnts; // --- Constraint directions that do not violate the other contraints
	Eigen::MatrixXd trialDirs; // ------- List of directions to test
	size_t /* -- */ numDir  = 0 , // ---- Number of directions to test
					counter = 0 ; // ---- Number of sphere directions tested
	Sphere_d /*- */ dirSphere{ 1.0 , // - Unit sphere for straightline direction testing
							   Eigen::Vector3d( 0,0,0 ) };
	std::vector<size_t> shuffledDices; // Indices of directions to test , Randomized
	
	// A. Instantiate hand model
	hand_collision_model handModel;  handModel.init();
	
	// ~~~ LAMBDA FUNCS ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
	
	Pose_ASP firstFreePose; // -------- Enclose the first free final part pose
	bool     foundFirstFree = false; // Flag for whether 'firstFreePose' has been set
	
	auto end_straightline_part_pose = [&]( const Eigen::Vector3d& queryDir ){
		// Fetch the ending pose for a straightline removal
		return Pose_ASP{ bgnPartPose.position + queryDir * params.distance , bgnPartPose.orientation };
	};
	
	auto check_straightline_hand = [&]( const Eigen::Vector3d& queryDir ){
		bool check = false;
		// 1. Generate the ending part pose
		endPartPose = end_straightline_part_pose( queryDir );
		// 2. Generate the ending hand pose
		endHandPose = endPartPose * relHandPose;
		handModel.set_pose( endHandPose );
		// 4. Check for hand-refc  collision
		if(  refcAsm->collides_with( handModel.LBR4hand )  ){  return false;  }
		// 5. Check for hand-floor collision
		if(  floor.collides_with( handModel.LBR4hand )  ){  return false;  }
		// 6. Check IK soln
		q2 = IK_clientCall( endHandPose , q1 , params.IKattemptLimit );
		if(  is_err( q2 )  ){  return false;  }
		// 7. All checks passed, Return true
		return true;
	};
	
	auto check_hand_w_part_poses = [&]( const std::vector<Pose_ASP>& poseSeq ){
		bool   check = false;
		size_t len   = poseSeq.size();
		// 1. For ever
		for( size_t m = 0 ; m < len ; m++ ){ 
			handModel.set_pose( poseSeq[m] * relHandPose );
			// 2. Check for hand-refc  collision
			if(  refcAsm->collides_with( handModel.LBR4hand )  ){  return false;  }
			// 3. Check for hand-floor collision
			if(  floor.collides_with( handModel.LBR4hand )  ){  return false;  }
		}
		// 4. All checks passed, Return true
		return true;
	};
	
	auto check_straightline_part = [&]( const Eigen::Vector3d& queryDir ){
		// 1. Generate the ending part pose
		endPartPose = end_straightline_part_pose( queryDir );
		// 2. Check for mover collision with floor
		SuccessCode partTestRes = original->validate_removal_staightline_stability( movd , refc , // Validate for removal and stability 
																					queryDir , original->get_support() , params.distance , 
																					params.numSamples );
		return partTestRes.success;
	};
	
	auto part_pose_seq_from_dir = [&]( const Eigen::Vector3d& queryDir ){
		// Fetch all of the intermediate part poses for 
		std::vector<Pose_ASP> poseSeq;
		double dDist = params.distance / (double) params.numSamples;
		for( size_t m = 0 ; m < params.numSamples ; m++ ){
			poseSeq.push_back( Pose_ASP{ bgnPartPose.position + queryDir * ( dDist * (double) m+1 ) , bgnPartPose.orientation } );
		}
		return poseSeq;
	};
	
	auto hand_IK_seq_from_part_pose_seq = [&]( const std::vector<Pose_ASP>& poseSeq ){
		// Fetch the ending pose for a straightline removal
		std::vector<std::vector<double>> jntSeq;
		size_t len = poseSeq.size();
		for( size_t m = 0 ; m < len ; m++ ){
			if(  ( q_i.size() == 7 )  &&  ( !is_err( q_i ) )  ){  crrntSeed = q_i;  }
			else{  crrntSeed = randrange_vec( -M_PI , M_PI , 7 );  }
			q_i = IK_clientCall( poseSeq[m] * relHandPose , crrntSeed , params.IKattemptLimit );
			jntSeq.push_back( q_i ); // Stow it whether it was correct or not
		}
		return jntSeq;
	};
	
	auto store_first_free = [&]( const Eigen::Vector3d& queryDir ){
		// Fetch the ending pose for a straightline removal
		if( !foundFirstFree ){
			firstFreePose  = Pose_ASP{ bgnPartPose.position + queryDir * params.distance , bgnPartPose.orientation };
			foundFirstFree = true;
		}
	};
	
	// ,,, END LAMBDA ,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,
	
	
	
	// ~~ Phase 1: Stability Concerns ~~
	if( SHOWDEBUG ){  cout << "About to evaluate stability ..." << endl;  }
	
	//  1. Check that the reference parts will have the same supports  // This prevents dynamic events like moving to different supports 
	//  A. Check that the new sub has the same supports  &&  2. Check that the new assembly will not be unstable
	chkPass = original->validate_subasm_stability_for_support( refc , original->get_support() );
	if( !chkPass ){
		if( SHOWDEBUG ){  cout << "NEW_SUB_UNSTABLE: Reference " << refc
							   << " did not have a support that matches the present total assembly." << endl;  }
		result.success = false;
		result.code = NEW_SUB_UNSTABLE;
		diagnosticStr << "NEW_SUB_UNSTABLE: Reference " << refc << " did not have a support that matches the present total assembly.";
		result.desc = diagnosticStr.str();
		delif( movdAsm );
		delif( refcAsm );
		return result;
	}
	
	//  3. Check that there are no orphaned pieces up in the air
	if( SHOWDEBUG ){  cout << "About the evaluate connectivity ..." << endl;  }
	//  A. Check that the sub is connected
	connSubGraphs = connected_subgraphs_in_subgraph( original , movd );
	if( connSubGraphs.size() > 1 ){ 
		//  B. If not, FAIL
		if( SHOWDEBUG ){  cout << "NEW_SUB_DISJOINT: Subassembly " << movd 
							   << " contains disjoint connected subgraphs: " << connSubGraphs << endl;  }
		result.success = false;
		result.code = NEW_SUB_DISJOINT;
		diagnosticStr << "NEW_SUB_DISJOINT: Subassembly " << movd << " contains disjoint connected subgraphs:" << connSubGraphs << endl;
		result.desc += diagnosticStr.str();
		delif( movdAsm );
		delif( refcAsm );
		return result;
	}
	//  C. Check that the ref is connected
	connSubGraphs = connected_subgraphs_in_subgraph( original , refc ); //  E. generate connected chunks
	//  D. If not
	len = connSubGraphs.size();
	if( len > 1 ){ 
		//  F. Check that all of the chunks are supported
		for( size_t i = 0 ; i < len ; i++ ){
			//  G. If chunks are not supported, FAIL
			chkPass = original->validate_subasm_stability_for_support( connSubGraphs[i] , original->get_support() );
			if( !chkPass ){
				if( SHOWDEBUG ){  cout << "FLOATING_HAM_ERR: Reference " << refc << " is disjoint with unsupported portion " 
									   << connSubGraphs[i] << endl;  }
				result.success = false;
				result.code = FLOATING_HAM_ERR;
				diagnosticStr << "FLOATING_HAM_ERR: Reference " << refc << " is disjoint with unsupported portion " << connSubGraphs[i] << endl;
				result.desc += diagnosticStr.str();
				delif( movdAsm );
				delif( refcAsm );
				return result;
			}
		}
	}
	
	// ~~ Phase 2: Grasp ~~
	if( SHOWDEBUG ){  cout << "Generate and Evaluate grasps ..." << endl;  }
	
	remDir = original->asm_center_of_freedom_direction( movd , refc ); // Part ID
	
	// 1. Generate and rank grasps
	std::vector<ParallelGraspState> rankedPairs = 
		generate_and_rank_grasp_pairs( original , 
									   Eigen::Vector3d::UnitX() , Eigen::Vector3d::UnitY() , Eigen::Vector3d::UnitZ() , 
									   params.distance , params.graspTileSize ,
									   params.preferredWristDir , 
									   //~ params.preferredCenter , 
									   end_straightline_part_pose( remDir ).position , 
									   params.graspDTheta , CRIT_ANG );
	
	
	//  2. Select a collision-free grasp
	bool     graspFound = false ,
		     collision  = true  ,
		     allCollide = true ;
	size_t   numRanked  = rankedPairs.size() , 
		     winDex     = 0 , // Winning index
		     numGrasps  = 0;
	Pose_ASP winPose    = err_pose();
	ParallelGraspState currGrasp;
	std::vector<ParallelGraspState> validGrasps;
	
	
	//  A. For each grasp in decreasing order
	if( SHOWDEBUG ){  cout << "There are " << numRanked << " grasps to test ..." << endl;  }
	for( size_t i = 0 ; i < numRanked ; i++ ){
		currGrasp = rankedPairs[i];
		//~ if( SHOWDEBUG ){  cout << "Testing grasp " << i+1 << " of " << numRanked << ": \t" << currGrasp.graspTarget << endl;  }
		if( SHOWDEBUG ){  if( i % 1000 == 0 ){  cout << i << " , ";  }  }
		// B. Place the collision model according to the grasp state
		handModel.set_finger_state( currGrasp.q_fingers );
		//~ result.gripState = currGrasp.q_fingers;
		handModel.set_pose( currGrasp.graspTarget );
		// C. Check collision
		collision = ( refcAsm->collides_with( handModel.LBR4hand ) )  ||  ( floor.collides_with( handModel.LBR4hand ) );
		chkPass   = false;
		if( !collision ){
			//~ q1 = IK_clientCall( currGrasp.graspTarget , jointSeed , params.IKattemptLimit );
			q1 = IK_clientCall( currGrasp.graspTarget , randrange_vec( -M_PI , M_PI , 7 ) , params.IKsearchLimit );
			chkPass = !is_err( q1 );
			allCollide = false;
		}
		// D. If there is no collision , break
		//~ if( SHOWDEBUG ){  cout << "\tCollision Free?: " << yesno( !collision ) << " , IK OK?: " << yesno( chkPass ) << endl;  }
		if(  ( !collision )  &&  ( chkPass )  ){  
			winDex     = i;
			graspFound = true;
			winPose    = currGrasp.graspTarget;
			//~ break;  
			validGrasps.push_back( currGrasp );
		}
		// D. else there was a collision, continue
	}
	if( SHOWDEBUG ){  cout << endl << endl << "Was there a collision-free pose?: " << graspFound << " , Winning Pose: " << winPose << endl;  }
	numGrasps = validGrasps.size();
	
	
	if( !graspFound ){ 
		if( SHOWDEBUG ){  cout << "GRASP_IK_FAILURE: Unable to obtain IK for wrist pose " << winPose << endl;  }
		result.success = false;
		result.code = GRASP_IK_FAILURE;
		diagnosticStr << "GRASP_IK_FAILURE: Unable to obtain IK for wrist pose " << winPose << endl;
		result.desc = diagnosticStr.str();
		delif( movdAsm );
		delif( refcAsm );
		return result;
	}
	
	if( allCollide ){
		if( SHOWDEBUG ){  cout << "ALL_GRASP_COLLID: No grasp for " << movd << " could be found that does not collide with either " 
							   << refc << " or supporting floor" << endl;  }
		result.success = false;
		result.code = ALL_GRASP_COLLID;
		diagnosticStr << "ALL_GRASP_COLLID: No grasp for " << movd << " could be found that does not collide with either " 
					  << refc << " or supporting floor" << endl;
		result.desc = diagnosticStr.str();
		delif( movdAsm );
		delif( refcAsm );
		return result;
	}
	
	for( size_t G = 0 ; G < numGrasps ; G++ ){
	
		currGrasp   = validGrasps[G];
		winPose     = currGrasp.graspTarget;
		relHandPose = winPose / movdAsm->get_pose();
		result.gripState = currGrasp.q_fingers;

		
		
		// ~ At this point we have a grasp that collides with neither the reference or the floor, and a valid IK solution to match the initial pose ~
		//   NOTE: Also assuming that the correct finger state has already been set above
		
		remDir = err_vec3(); // ------------- Suggestion from the assembly
		curDir = Eigen::Vector3d::UnitZ(); // Used for direction check
		
		
		
		// ~~ Phase 3: Straightline Path ~~
		if( SHOWDEBUG ){  cout << "Evaluate straightline paths ..." << endl
							   << "About to evaluate suggested direction ..." << endl;  }
		
		
		//  3. Try the suggested direction
		remDir = original->asm_center_of_freedom_direction( movd , refc ); // Part ID
		
		chkPass = check_straightline_part( remDir );
		chekTwo = check_straightline_hand( remDir );
		if( chekTwo ){  store_first_free( remDir );  } 
		if( chkPass && chekTwo ){
			poseSeq = part_pose_seq_from_dir( remDir );
			chkThr3 = check_hand_w_part_poses( poseSeq );
		}
		
		if( SHOWDEBUG ){  cout << "For Removal direction: " << remDir << endl
							   << "Part Path OK: " << yesno( chkPass ) << endl
							   << "Hand End OK:_ " << yesno( chekTwo ) << endl
							   << "Hand Path OK: " << yesno( chkThr3 ) << endl;  }
		
		if( chkPass && chekTwo && chkThr3 ){
			if( SHOWDEBUG ){  cout << "PASS_STRAIGHTLIN: Straightline removal passed for " << remDir << endl;  }
			result.success = true;
			result.code    = PASS_STRAIGHTLIN;
			diagnosticStr << "PASS_STRAIGHTLIN: Straightline removal passed for " << remDir << endl;
			result.desc    = diagnosticStr.str();
			result.path    = poseSeq;
			result.qSeq    = hand_IK_seq_from_part_pose_seq( result.path );
			delif( movdAsm );
			delif( refcAsm );
			return result;
		}
		
		//  4. Choose from constraints and corners
		//  A. Obtain constraints
		labConstraints = original->asm_get_constraints_on_movd( movd , refc );
		//  B. Obtain corners
		labCorners     = original->asm_spherical_pyramid_corners_of_local_freedom( movd , refc );
		//  C. Test in non-violating constraint directions
		freeConstrnts = unblocked_constraints( labConstraints );
		//  D. Stack constraints and corners
		trialDirs = vstack( labCorners , freeConstrnts );
		//  E. Test each of the directions
		for( size_t i = 0 ; i < numDir ; i++ ){
			curDir = trialDirs.row(i);
			chkPass = check_straightline_part( curDir );
			chekTwo = check_straightline_hand( curDir );
			if( chekTwo ){  store_first_free( curDir );  } 
			if( chkPass && chekTwo ){
				poseSeq = part_pose_seq_from_dir( curDir );
				chkThr3 = check_hand_w_part_poses( poseSeq );
			}
			
			if( SHOWDEBUG ){  cout << "For Removal direction: " << curDir << endl
								   << "Part Path OK: " << yesno( chkPass ) << endl
								   << "Hand End OK:_ " << yesno( chekTwo ) << endl
								   << "Hand Path OK: " << yesno( chkThr3 ) << endl;  }
			
			if( chkPass && chekTwo && chkThr3 ){
				if( SHOWDEBUG ){  cout << "PASS_STRAIGHTLIN: Straightline removal passed for " << curDir << endl;  }
				result.success = true;
				result.code    = PASS_STRAIGHTLIN;
				diagnosticStr << "PASS_STRAIGHTLIN: Straightline removal passed for " << curDir << endl;
				result.desc    = diagnosticStr.str();
				result.path    = poseSeq;
				result.qSeq    = hand_IK_seq_from_part_pose_seq( result.path );
				delif( movdAsm );
				delif( refcAsm );
				return result;
			}
		}
		
		//  5. Sample on the sphere
		if( SHOWDEBUG ){  cout << "About to sample on the sphere ..." << endl;  }
		trialDirs = dirSphere.sample_directions_from_direction_constraints( labConstraints );
		numDir    = trialDirs.rows();
		if( numDir > 0 ){
			if( SHOWDEBUG ){  cout << "There are " << trialDirs.rows() << " directions to test" << endl;  }
			
			
			
			counter = 0;
			while(  ( (double) counter/numDir <= params.sphereFraction )  &&  ( counter < numDir )  ){
			
				curDir = trialDirs.row( shuffledDices[ counter ] );
				
				chkPass = check_straightline_part( curDir );
				chekTwo = check_straightline_hand( curDir );
				if( chekTwo ){  store_first_free( curDir );  } 
				if( chkPass && chekTwo ){
					poseSeq = part_pose_seq_from_dir( curDir );
					chkThr3 = check_hand_w_part_poses( poseSeq );
				}
				if( chkPass && chekTwo && chkThr3 ){
					if( SHOWDEBUG ){  cout << "PASS_STRAIGHTLIN: Straightline removal passed for " << curDir << endl;  }
					result.success = true;
					result.code    = PASS_STRAIGHTLIN;
					diagnosticStr << "PASS_STRAIGHTLIN: Straightline removal passed for " << curDir << endl;
					result.desc    = diagnosticStr.str();
					result.path    = poseSeq;
					result.qSeq    = hand_IK_seq_from_part_pose_seq( result.path );
					delif( movdAsm );
					delif( refcAsm );
					return result;
				}
				counter++;
			}
		}else if( SHOWDEBUG ){  cout << "There were no free spherical directions" << endl;  }
		
	}
	
	// ~~ Phase 3: RRT* ~~
	
	if( SHOWDEBUG ){  cout << "Constructing collision function ..." << endl;  }
	HandAsmFlrCollision collsn_check_part_pose{ movdAsm , refcAsm , &handModel , relHandPose , &floor };
	
	
	if( 0 ){
		//  5. Find a free goal pose
		if( !foundFirstFree ){
			
			if( SHOWDEBUG ){  cout << "There were no good end poses in the search so far.  Searching " << numDir << " directions ... " << endl;  }
			
			trialDirs = dirSphere.get_all_spherical_directions();
			numDir    = trialDirs.rows();
			
			chekTwo /*-*/ = false;
			if( numDir > 0 ){
				shuffledDices = vec_copy_shuffled( vec_range( (size_t)0 , (size_t)numDir-1 ) );
				counter /*-*/ = 0;
				chkThr3 = false;
				chekTwo = false;
				
				while(  ( !( chekTwo && chkThr3 ) )  &&  ( counter < numDir )  ){
					curDir = trialDirs.row( shuffledDices[ counter ] );
					chekTwo = check_straightline_hand( curDir );
					chkThr3 = collsn_check_part_pose( end_straightline_part_pose( curDir ) );
					counter++;
				}
			}
			// If a collision-free goal cannot be found, FAIL
			if( !chekTwo ){ 
				if( SHOWDEBUG ){  cout << "GOALPOSE_NOT_FND: No suitable goal pose was found for RRT*!" << endl;  }
				result.success = false;
				result.code = GOALPOSE_NOT_FND;
				diagnosticStr << "GOALPOSE_NOT_FND: No suitable goal pose was found for RRT*!" << endl;
				result.desc = diagnosticStr.str();
				// N-1. Delete allocated models
				delif( movdAsm );
				delif( refcAsm );
				return result;
			}else{
				if( SHOWDEBUG ){  cout << "Direction Chosen: " << curDir << endl;  }
				store_first_free( curDir );
			}
		}
	}else{
		curDir = original->asm_center_of_freedom_direction( movd , refc ); // Part ID
		if( SHOWDEBUG ){  cout << "Force Direction Chosen: " << curDir << endl;  }
	}
	
	
	
	if( SHOWDEBUG ){  cout << "Constructing a bounding box ..." << endl;  }
	
	//  6. Choose sampling bounds commensurate with the placement of the part
	//  A. Get bases
	Eigen::MatrixXd bases  = get_any_orthogBasis_for_Z( curDir , Eigen::Vector3d::UnitX() );
	Eigen::Vector3d xBasis = bases.row(0);
	Eigen::Vector3d yBasis = bases.row(1);
	Eigen::Vector3d zBasis = bases.row(2);
	
	if( SHOWDEBUG ){  cout << "xBasis: " << xBasis << endl
						   << "yBasis: " << yBasis << endl
						   << "zBasis: " << zBasis << endl;  }
	
	//  B. Construct a box that is a cube with sides that are the length of the lift distance times some factor
	double sideLen = params.distance * params.RRT_liftBoxFctr;
	std::vector<double> extents = { -sideLen/2.0 ,  sideLen/2.0 , -sideLen/2.0 ,  sideLen/2.0 , -sideLen/3.0 , sideLen };
	Eigen::MatrixXd bounds = AABB(  
		box_corners_from_bases_and_extents( movdAsm->get_pose().position , 
										    xBasis , yBasis , zBasis ,
										    extents )
	);
	
	bounds( 0 , 2 ) = max( bounds( 0 , 2 ) , params.setdownLoc(2) );  // Let's keep it above the table!
	
	if( SHOWDEBUG ){  cout << "Bounding Box for RRT* "<< endl << bounds << endl
						   << "Mover is at: ___ " << movdAsm->get_pose().position << endl
						   << "Reference is at: " << refcAsm->get_pose().position << endl;  }
	
	//  7. Init solver
	Pose_ASP strtPose = bgnPartPose;
	Pose_ASP goalPose = end_straightline_part_pose( curDir );
	RRT_Solver<HandAsmFlrCollision> rrtSolver( params.RRT_K , params.RRT_Epsilon , params.RRT_BiasProb , bounds , 
											   collsn_check_part_pose ,
											   strtPose , goalPose , params.RRT_WeightPosn , params.RRT_WeightOrnt );
											   
	if( SHOWDEBUG ){  cout << endl
						   << "Bounding Box for RRT* "<< endl << bounds << endl
						   << "Mover is at: ______ " << movdAsm->get_pose().position << endl
						   << "Reference is at: __ " << refcAsm->get_pose().position << endl
						   << "Start Pose: _______ " << strtPose << endl
						   << "Goal Pose: ________ " << goalPose << endl
						   << "Mover inside bbox:_ " << yesno( is_point_inside_AABB( movdAsm->get_pose().position , bounds ) ) << endl 
						   << "Start inside bbox:_ " << yesno( is_point_inside_AABB( strtPose.position , bounds ) ) << endl 
						   << "Goal inside bbox: _ " << yesno( is_point_inside_AABB( goalPose.position , bounds ) ) << endl 
						   << "Start in collision: " << yesno( collsn_check_part_pose( strtPose ) ) << endl
						   << "Goal in collision:_ " << yesno( collsn_check_part_pose( goalPose ) ) << endl
						   << "Table Center: _____ " << params.setdownLoc << endl << endl;
	}
	
	if( SHOWDEBUG ){  cout << "Action planner, About to RRT* ..." << endl;
					  waitkey();  }
	
	//  8. Solve
	std::vector<Pose_ASP> soln = rrtSolver.build_rrt_star( strtPose , goalPose , params.RRT_nghbrRadius , params.RRT_shortCutN );
	
	//  9. Return success
	if( soln.size() > 0 ){ 
		if( SHOWDEBUG ){  cout << "PASS_RRTSTAR_PTH: RRT* path returned from " << strtPose << " to " << goalPose << endl;  }
		result.success = true;
		result.code = PASS_RRTSTAR_PTH;
		diagnosticStr << "PASS_RRTSTAR_PTH: RRT* path returned from " << strtPose << " to " << goalPose << endl;
		result.desc = diagnosticStr.str();
		result.path = soln;
		result.qSeq = hand_IK_seq_from_part_pose_seq( result.path );
		// N-1. Delete allocated models
		delif( movdAsm );
		delif( refcAsm );
		return result;
	}else{ 
		if( SHOWDEBUG ){  cout << "RRTSTAR_NO_SOLTN: RRT* did not return a solution!" << endl;  }
		result.success = false;
		result.code = RRTSTAR_NO_SOLTN;
		diagnosticStr << "RRTSTAR_NO_SOLTN: RRT* did not return a solution!" << endl;
		result.desc = diagnosticStr.str();
		// N-1. Delete allocated models
		delif( movdAsm );
		delif( refcAsm );
		return result;
	}
	
	// N. Exit without plan
	if( SHOWDEBUG ){  cout << "No viable removal plan was generated, Exit!" << endl;  }
	delif( movdAsm );
	delif( refcAsm );
	return result;

} 

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

SuccessPath validate_removal_multimode_w_stability( Assembly_ASM* original , // Validate for removal and stability , Using Straightline and RRT*
												    std::vector<llin>& movedParts , std::vector<llin>& referenceParts , 
												    ValidationParams params  , 
												    const Eigen::Vector3d& direction ){
	// Validate for removal and stability , Using Straightline and RRT*
	// NOTE: The main idea is to generate an answer as soon as possible, so that all checks do not have to be done
	// NOTE: Weak Version
	//       This function will not rule on whether the assembly should be rotated to accomodate the operation
	
	
	// Distance: 1 assembly-width away
	// RRT*:    30k limit
		// Delete trees when done
	
	bool SHOWDEBUG = true; // if( SHOWDEBUG ){  cout << "" << endl;  }
	
	SuccessPath result = empty_SuccessPath();
	stringstream diagnosticStr;
	
	// ~~ Phase 0: Init ~~
	bool /* ------------------- */ chkPass = false , // - Flag for whether the most recent check has passed
								   chekTwo = false ; // - Flag for whether the most recent check has passed
	std::vector<std::vector<llin>> connSubGraphs; // ---- Connected subgraphs
	std::vector<llin> /* ------ */ temp; // ------------- Param for old functions
	size_t /* ----------------- */ len , // ------------- Number of elements in a loop
								   counter; // ---------- Iteration counter
	Eigen::Vector3d /* -------- */ remDir = direction; // Straighline removal direction
	Eigen::Vector3d /* -------- */ relDir; // ----------- Removal direction relative to the assembly
	double /* ----------------- */ floorScale = 2.0; // - Number of times wider the test Floor will be than the assembly
	Eigen::MatrixXd /* -------- */ labConstraints; // --- Constraints on a sub in the lab frame
	Eigen::MatrixXd /* -------- */ labCorners; // ------- Contraint corners for a sub in the lab frame
	Eigen::Vector3d /* -------- */ curDir; // ----------- Current removal direction being tested
	Eigen::MatrixXd /* -------- */ freeConstrnts; // ---- Constraint directions that do not violate the other contraints
	Sphere_d /* --------------- */ dirSphere{ 1.0 , // -- Unit sphere for straightline direction testing
											  Eigen::Vector3d( 0,0,0 ) };
	Eigen::MatrixXd /* -------- */ sampleDirs; // ------- Directions sampled from the unit sphere
	std::vector<size_t> /* ---- */ shuffledDices; // ---- Shuffled indices
	
	// = B. Load Params =
	//~ size_t vp_supportDex = params.support;		
	//~ double vp_liftDist   = params.distance;
	//~ usll   vp_Nsamples   = params.numSamples;
	//~ double vp_samplFract = params.sphereFraction;
	// _ Params Loaded _
	
	// ~~ Phase 1: Stability Concerns ~~
	chkPass = original->validate_partID_subset_does_not_float( referenceParts );
	if( !chkPass ){
		if( SHOWDEBUG ){  cout << "FLOATING_HAM_ERR: Reference " << referenceParts << " does not share any supports with the original assembly" << endl;  }
		result.success = false;
		result.code = FLOATING_HAM_ERR;
		diagnosticStr << "FLOATING_HAM_ERR: Reference " << referenceParts << " does not share any supports with the original assembly" << endl;
		result.desc += diagnosticStr.str();
		return result;
	}else if( SHOWDEBUG ){  cout << "Was not floating." << endl;  }
	
	// ~~ Phase 2: Straightline validation ~~
	
	//  4. Obtain the suggested removal direction
	if( SHOWDEBUG ){  cout << "About to evaluate suggested direction ..." << endl;  }
	//  A. If the client code suggested a direction, Use that
	if( !remDir.isApprox( DEFAULT_REMDIR , EPSILON ) ){
		remDir = remDir.normalized(); // Ensure that it is unit
		if( SHOWDEBUG ){  cout << "Normalized the given removal direction: " << remDir << endl;  }
	}else{
	//  B. Else generate the suggested direction
		remDir = original->asm_center_of_freedom_direction( movedParts , referenceParts ); // Part ID
		if( SHOWDEBUG ){  cout << "Client code suggested no direction, Suggesting: " << remDir << endl;  }
	}
	
	// DANGER: TRANSFORMATION
	//~ relDir = original->get_pose().orientation.inverse() * direction; // Store the relative direction so that we can rotate it for any support
	//~ if( SHOWDEBUG ){  cout << "Relative direction: " << relDir << endl
	if( SHOWDEBUG ){  cout 
						   << "Absolute direction: " << remDir << endl;  }
	
	if( is_err( remDir ) ){ 
		if( SHOWDEBUG ){  cout << "REMDIR_NOT_FOUND: No striaghtline center found for Moved: " << movedParts 
							   << " -vs- Reference: " << referenceParts << endl;;  }
		result.success = false;
		result.code = REMDIR_NOT_FOUND;
		diagnosticStr << "REMDIR_NOT_FOUND: No striaghtline center found for Moved: " << movedParts << " -vs- Reference: " << referenceParts << endl;
		result.desc = diagnosticStr.str();
		return result;
	}
	
	
	//~ chekTwo = original->test_translation_sub_removal( movedParts , referenceParts , ( original->get_pose().orientation * relDir ) , 
	chekTwo = original->test_translation_sub_removal( movedParts , referenceParts , remDir , 
													  params.distance , params.numSamples );
	
	if( chekTwo ){ 
		//~ if( SHOWDEBUG ){  cout << "PASS_STRAIGHTLIN: Straightline removal passed for " << ( original->get_pose().orientation * relDir ) << endl;  }
		if( SHOWDEBUG ){  cout << "PASS_STRAIGHTLIN: Straightline removal passed for " << remDir << endl;  }
		result.success = true;
		if( SHOWDEBUG ){  cout << "Assign code ... " << endl;  }
		result.code = PASS_STRAIGHTLIN;
		if( SHOWDEBUG ){  cout << "Diagnostic string ... " << endl;  }
		//~ diagnosticStr << "PASS_STRAIGHTLIN: Straightline removal passed for " << ( original->get_pose().orientation * relDir ) << endl;
		diagnosticStr << "PASS_STRAIGHTLIN: Straightline removal passed for " << remDir << endl;
		if( SHOWDEBUG ){  cout << "Unwind string ... " << endl;  }
		result.desc = diagnosticStr.str();
		if( SHOWDEBUG ){  cout << "Return ... " << endl;  }
		return result;
	}else if( SHOWDEBUG ){  cout << "Could not remove along Absolute direction: " << remDir << endl;  }
	
	//  6. Obtain constraints
	labConstraints = original->asm_get_constraints_on_movd( movedParts , referenceParts );
	//  7. Obtain corners
	labCorners     = original->asm_spherical_pyramid_corners_of_local_freedom( movedParts , referenceParts );
	//  8. Test in each corner directions
	if( SHOWDEBUG ){  cout << "About the evaluate corners ..." << endl;  }
	len = labCorners.rows();
	for( size_t i = 0 ; i < len ; i++ ){
		curDir = labCorners.row(i);
		//~ chkPass = original->test_sub_removal_against_floor( movedParts , tempFloor , curDir , 
													        //~ vp_liftDist , vp_Nsamples );
		chekTwo = original->test_translation_sub_removal( movedParts , referenceParts , curDir , 
														  params.distance , params.numSamples );
		//~ if( chkPass && chekTwo ){ 
		if( chekTwo ){ 
			if( SHOWDEBUG ){  cout << "PASS_STRAIGHTLIN: Straightline removal passed for " << curDir << endl;  }
			result.success = true;
			result.code = PASS_STRAIGHTLIN;
			diagnosticStr << "PASS_STRAIGHTLIN: Straightline removal passed for " << curDir << endl;
			result.desc = diagnosticStr.str();
			return result;
		}
	}
	
	//  9. Test in non-violating constraint directions
	if( SHOWDEBUG ){  cout << "About to evaluate free constraints ..." << endl;  }
	freeConstrnts = unblocked_constraints( labConstraints );
	len = freeConstrnts.rows();
	for( size_t i = 0 ; i < len ; i++ ){
		curDir = freeConstrnts.row(i);
		//~ chkPass = original->test_sub_removal_against_floor( movedParts , tempFloor , curDir , 
													        //~ vp_liftDist , vp_Nsamples );
		chekTwo = original->test_translation_sub_removal( movedParts , referenceParts , curDir , 
														  params.distance , params.numSamples );
		//~ if( chkPass && chekTwo ){ 
		if( chekTwo ){ 
			if( SHOWDEBUG ){  cout << "PASS_STRAIGHTLIN: Straightline removal passed for " << curDir << endl;  }
			result.success = true;
			result.code = PASS_STRAIGHTLIN;
			diagnosticStr << "PASS_STRAIGHTLIN: Straightline removal passed for " << curDir << endl;
			result.desc = diagnosticStr.str();
			return result;
		}
	}
	
	// 10. Get constrained sphere directions 
	if( SHOWDEBUG ){  cout << "About to sample on the sphere ..." << endl;  }
	sampleDirs    = dirSphere.sample_directions_from_direction_constraints( labConstraints );
	if( SHOWDEBUG ){  cout << "There are " << sampleDirs.rows() << " directions to test" << endl;  }
	len           = sampleDirs.rows();
	
	if( len > 0 ){
		shuffledDices = vec_copy_shuffled( vec_range( (size_t)0 , (size_t)sampleDirs.rows()-1 ) );
		
		
		if( SHOWDEBUG ){  cout << "There are " << len << " directions to test" << endl;  }
		
		// 11. Test in a sampling of the sphere directions
		counter = 0;
		while(  ( (double) counter/len <= params.sphereFraction )  &&  ( counter < len )  ){
			
			if( SHOWDEBUG ){  cerr << "\tCounter: __________ " << counter << endl
								   << "\tFraction: _________ " << (double) counter/len << endl
								   << "\tFraction Exceeded?: " << yesno( (double) counter/len <= params.sphereFraction ) << endl
								   << "\tCounter Exceeded?:_ " << yesno( counter < len ) << endl;  }
			
			curDir = sampleDirs.row( shuffledDices[ counter ] );
			
			if( SHOWDEBUG ){  cout << "Check direction " << curDir << " ..." << endl;  }
			
			//~ chkPass = original->test_sub_removal_against_floor( movedParts , tempFloor , curDir , 
																//~ vp_liftDist , vp_Nsamples );
			chekTwo = original->test_translation_sub_removal( movedParts , referenceParts , curDir , 
															  params.distance , params.numSamples );
			//~ if( chkPass && chekTwo ){ 
			if( chekTwo ){ 
				if( SHOWDEBUG ){  cout << "PASS_STRAIGHTLIN: Straightline removal passed for " << curDir << endl;  }
				result.success = true;
				result.code = PASS_STRAIGHTLIN;
				diagnosticStr << "PASS_STRAIGHTLIN: Straightline removal passed for " << curDir << endl;
				result.desc = diagnosticStr.str();
				return result;
			}
			counter++;
		}
	}else if( SHOWDEBUG ){  cout << "There were no valid sphere directions!" << endl;  }
	
	// ~~ Phase 3: RRT* ~~
	
	if( SHOWDEBUG ){  cout << "Constructing collision function ..." << endl;  }
	
	// 12. Construct collision function                                                 //  A. Set the subs at some arbitrary pose
	Assembly_ASM* moverAsm = original->sub_from_spec( movedParts     , (size_t)0 );		moverAsm->set_pose( params.planPose ); 
	Assembly_ASM* refrcAsm = original->sub_from_spec( referenceParts , (size_t)0 );		refrcAsm->set_pose( params.planPose );
	AsmCollision removalCollsnCheck{ moverAsm , refrcAsm };
	
	if( SHOWDEBUG ){  cout << "Mover is at: ___ " << moverAsm->get_pose() << endl
						   << "Reference is at: " << refrcAsm->get_pose() << endl;  }
	
	if( SHOWDEBUG ){  cout << "Choosing a goal pose ..." << endl;  }
	
	// 13. Choose a collision-free goal location
	//  A. Prefer center of freedom, lift distance away from the starting position
	
	//~ remDir = ( refrcAsm->get_pose().orientation * relDir ).normalized();
	
	Pose_ASP strtPose = params.planPose;
	Pose_ASP goalPose = Pose_ASP{ strtPose.position + remDir * params.distance , strtPose.orientation };
	//  B. Else preferred pose was in collision, sample on sphere until a collision-free location was found
	len     = sampleDirs.rows();
	chkPass = !( removalCollsnCheck( goalPose ) );
	if(  ( len > 0 )  &&  ( !chkPass )  ){
		shuffledDices = vec_copy_shuffled( vec_range( (size_t)0 , (size_t)sampleDirs.rows()-1 ) );
		counter = 0;
		while(  ( !chkPass )  &&  ( counter < len )  ){
			curDir = sampleDirs.row( shuffledDices[ counter ] );
			goalPose = Pose_ASP{ params.planPose.position + remDir * params.distance , params.planPose.orientation };
			chkPass = !( removalCollsnCheck( goalPose ) );
			counter++;
		}
	}
	// 14. If a collision-free goal cannot be found, FAIL
	if( !chkPass ){ 
		if( SHOWDEBUG ){  cout << "GOALPOSE_NOT_FND: No suitable goal pose was found for RRT*!" << endl;  }
		result.success = false;
		result.code = GOALPOSE_NOT_FND;
		diagnosticStr << "GOALPOSE_NOT_FND: No suitable goal pose was found for RRT*!" << endl;
		result.desc = diagnosticStr.str();
		// N-1. Delete allocated models
		delif( moverAsm );
		delif( refrcAsm );
		return result;
	}else if( SHOWDEBUG ){  cout << "Constructing a bounding box ..." << endl;  }
	
	if( SHOWDEBUG ){  cout << "Constructing a bounding box ..." << endl;  }
	
	// 15. Choose sampling bounds commensurate with the placement of the part
	//  A. Get bases
	Eigen::MatrixXd bases  = get_any_orthogBasis_for_Z( curDir , Eigen::Vector3d::UnitX() );
	Eigen::Vector3d xBasis = bases.row(0);
	Eigen::Vector3d yBasis = bases.row(1);
	Eigen::Vector3d zBasis = bases.row(2);
	//  B. Construct a box that is a cube with sides that are the length of the lift distance times some factor
	double sideLen = params.distance * params.RRT_liftBoxFctr;
	std::vector<double> extents = { -sideLen/2.0 ,  sideLen/2.0 , -sideLen/2.0 ,  sideLen/2.0 , -sideLen/3.0 , sideLen };
	Eigen::MatrixXd bounds = AABB(  
		box_corners_from_bases_and_extents( params.planPose.position , 
										    xBasis , yBasis , zBasis ,
										    extents )
	);
	
	if( SHOWDEBUG ){  cout << "Bounding Box for RRT* "<< endl << bounds << endl
						   << "Mover is at: ___ " << moverAsm->get_pose().position << endl
						   << "Reference is at: " << refrcAsm->get_pose().position << endl;  }
	
	// 13. Init solver
	RRT_Solver<AsmCollision> rrtSolver( params.RRT_K , params.RRT_Epsilon , params.RRT_BiasProb , bounds , 
										removalCollsnCheck ,
										strtPose , goalPose , params.RRT_WeightPosn , params.RRT_WeightOrnt );
	
	//~ if( SHOWDEBUG ){  cout << "Validator, About to RRT* ..." << endl;
					  //~ waitkey();  }
	
	// 14. Solve
	std::vector<Pose_ASP> soln = rrtSolver.build_rrt_star( strtPose , goalPose , params.RRT_nghbrRadius , params.RRT_shortCutN );
	
	// 15. Return success
	if( soln.size() > 0 ){ 
		if( SHOWDEBUG ){  cout << "PASS_RRTSTAR_PTH: RRT* path returned from " << strtPose << " to " << goalPose << endl;  }
		result.success = true;
		result.code = PASS_RRTSTAR_PTH;
		diagnosticStr << "PASS_RRTSTAR_PTH: Straightline removal passed for " << curDir << endl;
		result.desc = diagnosticStr.str();
		// N-1. Delete allocated models
		delif( moverAsm );
		delif( refrcAsm );
		return result;
	}else{ 
		if( SHOWDEBUG ){  cout << "RRTSTAR_NO_SOLTN: RRT* did not return a solution!" << endl;  }
		result.success = false;
		result.code = RRTSTAR_NO_SOLTN;
		diagnosticStr << "RRTSTAR_NO_SOLTN: RRT* did not return a solution!" << endl;
		result.desc = diagnosticStr.str();
		// N-1. Delete allocated models
		delif( moverAsm );
		delif( refrcAsm );
		return result;
	}
	
	// N-1. Delete allocated models
	delif( moverAsm );
	delif( refrcAsm );
	
	// ~~ Phase 4: Wrap-up and Reporting ~~
	
	// 16. Load path
	
	//  N. SHOULD NOT HAVE MADE IT HERE
	return result;
}


/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

SuccessPath validate_removal_multimode_w_stability( Assembly_ASM* original , // Validate for removal and stability , Using Straightline and RRT*
												    std::vector<llin>& movedParts , std::vector<llin>& referenceParts , 
												    ValidationParams params , 
												    const Eigen::Vector3d& direction = DEFAULT_REMDIR );
												    


/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

std::vector<LevelPlanNode*> tree_plan_exec_full_depth( AsmStateNode_ASP* planRoot , Floor_ASM& floor , IK IK_clientCall , 
													   ValidationParams params ,
													   double CRIT_ANG = GEO_CRIT_ANG ){
	
	bool SHOWDEBUG = true;

	std::vector<LevelPlanNode*>   rtnVec; // NOTE: Current implementation doesn't really use this to make a tree
	
	std::queue<AsmStateNode_ASP*> subNodes;
	std::vector<ActionSpec*>      levelPlan;
	AsmStateNode_ASP* /* ----- */ currNode = nullptr ,
								  tempNode = nullptr ;
	size_t /* ---------------- */ len      = 0 ,
								  depth    = 0 ;
	LevelPlanNode* /* -------- */ planNode = nullptr;
	
	// 0. Set the depth of the root node to 0
	planRoot->depth = 0;
	// 1. Enqueue original (node)
	subNodes.push( planRoot );
	
	// 2. While there are still assemblies in queue
	while( subNodes.size() > 0 ){
		// 3. Retrieve and Pop node
		currNode = queue_get_pop( subNodes );
		depth    = currNode->depth;
		// 4. If the asm has more than one part
		if( currNode->assembly->get_part_ID_vec().size() > 1 ){
			levelPlan = tree_plan_exec_one_level( currNode , floor , IK_clientCall , 
												  params ,
												  CRIT_ANG );
												  
			planNode = new LevelPlanNode( currNode->depth , 
										  !( failed_edges_in_level( levelPlan ) ) , 
										  levelPlan );
			len = currNode->outgoingEdges.size();
			if( len > 0 ){ // This should always be true if the asm clustered had more than one part
				// 6. For every parts list produced
				for( size_t i = 0 ; i < len ; i++ ){
					// 7. Produce a new assembly , load into Node  &&  8. Enqueue the new assembly
					tempNode = currNode->outgoingEdges[i];
					tempNode->depth = depth+1;
					subNodes.push( tempNode );
				}
			}
		}
	}
	
}

/////////////////////////////////////////////////////////////////////////////////////////////////////

AsmStateNode_ASP* Morato2013Modified_full_depth( Assembly_ASM* original , ValidationParams params ){
	
	AsmStateNode_ASP* rootNode = new AsmStateNode_ASP( original );
	std::queue<AsmStateNode_ASP*> subNodes;
	AsmStateNode_ASP* currNode = nullptr;
	std::vector<std::vector<llin>> asmClusterLvl1;
	size_t len = 0;
	bool SHOWDEBUG = true;
	
	// 1. Enqueue original (node)
	subNodes.push( rootNode );
	// 2. While there are still assemblies in queue
	while( subNodes.size() > 0 ){
		// 3. Retrieve and Pop node
		currNode = subNodes.front();  subNodes.pop();
		// 4. If the asm has more than one part
		//~ if( currNode->memParts.size() > 1 ){
		if( currNode->assembly->get_part_ID_vec().size() > 1 ){
			//~ asmClusterLvl1.clear();
			// 5. Execute 1st-level clustering
			asmClusterLvl1 = cluster_one_level_disasm( currNode->assembly , params );
			len = asmClusterLvl1.size();
			if( len > 0 ){ // This should always be true if the asm clustered had more than one part
				// 6. For every parts list produced
				for( size_t i = 0 ; i <  len ; i++ ){
					// 7. Produce a new assembly , load into Node  &&  8. Enqueue the new assembly
					subNodes.push( currNode->add_edge( asmClusterLvl1[i] ) );
				}
			}
		}
	}
	return rootNode;
}


/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

template< typename IK >
AsmStateNode_ASP* tree_plan_exec_one_level( AsmStateNode_ASP* planRoot , ValidationParams params ){
	// Get all of the robot actions that will reduce the root node to each of its successors
	// NOTE: This function assumes that 'planRoot' has at least one outgoing edge
	
	bool SHOWDEBUG = true; // if( SHOWDEBUG ){  cout << "" << endl;  }
	
	AsmStateNode_ASP* currState = new AsmStateNode_ASP( *planRoot ); // Pointer to the current state, used to build a chain
	AsmStateNode_ASP* initState = currState; // ----------------------- This is the root / init state to return
	
	if( SHOWDEBUG ){  cout << "Init ..." << endl;  }
	
	// ~ Algo Vars ~
	Assembly_ASM* /*-*/ rootAsm      = planRoot->assembly; // Fetch the assembly from which subs will be removed
	if( SHOWDEBUG ){  cout << "Did we get a root assembly?: " << yesno( rootAsm ) << endl
						   << "About to replicate ..." << endl;  }
	Assembly_ASM* /*-*/ runningAsm   = rootAsm->replicate(); // The assembly will shrink as we work with it
	std::vector<llin>   runningIDs   = runningAsm->get_part_ID_vec();
	size_t /* ------ */ numEdges     = 0 , // -------------- Number of edges below the root node
						chosenSupprt = 0 ; // ---------- Support that allows the present operation
	std::vector<size_t> edgeDices     , // ------------- All of the edge indices
						suggestSupprt , // ------------- The suggested parent support to allow the removal
						matches ; // ------------------- Support indices that match value
	std::queue<size_t>  execOrder; // ------------------ Controls the order in which we execute edges
	std::vector<llin>   subPartIDs , // ---------------- Parts contained in the edge sub
						currBalanc , // ---------------- Remainder parts for the assembly being looked at
						runningBal ; // ---------------- Current balance of parts from the parent asm
	IndexSuccesLookup   supportVote; // ---------------- Info about which support is best for a certain action
	double /* ------ */ floorScale = 2.0;
	
	// ~ Per Iteration ~
	size_t /* ---- */ currEdgeDex = 0; // ----- Index of the edge currently being planned
	AsmStateNode_ASP* currEdge    = nullptr; // Edge currently being planned
	Assembly_ASM*     currAsm     = nullptr; // Assembly currently being planned
	Eigen::Vector3d   remDir; // -------------- Suggested removal direction
	bool /* ------ */ currFlag = false , // ----------- Rotation flag of the inspected node
					  chkPass  = false ,
					  chekTwo  = false ;  
	Assembly_ASM*     tempAsm; // ------------- Temp asm
	
	// UNUSED
	std::set<size_t>    plannedEdges; // --------------- Set of edges with completed plans
	
	
	// ~~~ LAMBDA FUNCS ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
	
	auto demote_edge = [&]( AsmStateNode_ASP* edgeNode ){
		edgeNode->actionData.precedence++;
		return edgeNode->actionData.precedence;
	};
	
	auto get_R_flag = [&]( AsmStateNode_ASP* edgeNode ){
		return edgeNode->actionData.rotateFlag;
	};
	
	auto set_R_flag = [&]( AsmStateNode_ASP* edgeNode , bool val ){
		edgeNode->actionData.rotateFlag = val;
	};
	
	auto reset_flags = [&](){
		for( size_t k = 0 ; k < numEdges ; k++ ){  set_R_flag( planRoot->outgoingEdges[k] , false );  }
	};
	// ,,, END LAMBDA ,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,
	
	
	if( SHOWDEBUG ){  cout << "Locating edges ..." << endl;  }
	
	// ~~ Phase 0: Init ~~
	//  A. Get the number of edges
	numEdges = planRoot->outgoingEdges.size();
	if( numEdges > 0 ){
		//  B. Get a list of edge indices
		edgeDices = vec_range( (size_t)0 , numEdges-1 );
	}else{
		if( SHOWDEBUG ){  cout << "There were no edges!" << endl;  }
		return currState;
	}
	
	if( SHOWDEBUG ){  cout << "There are " << numEdges << " edges" << endl 
						   << "Finding the best starting support ..." << endl;  }
	
	
	// ~~ Phase 1: Beginning Pose ~~
	
	//  1. For each node
	for( size_t i = 0 ; i < numEdges ; i++ ){
		subPartIDs = planRoot->outgoingEdges[i]->assembly->get_part_ID_vec();
		currBalanc = rootAsm->get_balance_IDs_from_sub( subPartIDs );
		//  A. Suggest a support for edge sub removal and store
		supportVote = rootAsm->suggest_support_for_staightline_stability( subPartIDs , currBalanc , 
																		  params.distance , params.numSamples ); 
		suggestSupprt.push_back( supportVote.bestDex ); // Log the best support whether it was successful or not
	}
	//  B. Set the root node to the support with the most votes
	chosenSupprt = most_numerous_value_in( suggestSupprt );
	//  C. Situate the parent asm at the setdown location
	if( SHOWDEBUG ){  cout << "Setting parent asm to support " << chosenSupprt << endl;  }
	rootAsm->set_lab_pose_for_index_3D( chosenSupprt , params.setdownLoc , true );
	runningAsm->set_pose( rootAsm->get_pose() );
	runningAsm->set_support( chosenSupprt );
	matches = all_indices_equal_to_val( edgeDices , chosenSupprt );
	//  D. Enque Matches
	enqueue_vec( execOrder , matches );
	//  E. Enque Others
	enqueue_vec( execOrder , vec_copy_without_elem( edgeDices , matches ) );
	// NOTE: The asm will be changing over time, so we can't necessarily depend upon this order to be maintained as we go
	
	size_t counter = 0;
	
	// 2. While there are edges to plan
	while( execOrder.size() > 0 ){
		
		if( SHOWDEBUG ){  sep( "Iteration " + to_string( counter + 1 ) , 1 );  counter++;  }  
		
		// 3. Pop Edge
		currEdgeDex = queue_get_pop( execOrder );
		currEdge    = planRoot->outgoingEdges[ currEdgeDex ];
		currAsm     = currEdge->assembly;
		subPartIDs  = currAsm->get_part_ID_vec();
		currBalanc  = runningAsm->get_balance_IDs_from_sub( subPartIDs );
		currFlag    = get_R_flag( currEdge );
		
		// 4. Check that the balance is not zero
		if( currBalanc.size() == 0 ){  
			if( SHOWDEBUG ){  cout << "Balance is zero, Skipping loop ..." << endl;  }
			continue;  
		}
		
		//  4. Obtain the suggested removal direction
		remDir = runningAsm->asm_center_of_freedom_direction( subPartIDs , currBalanc ); // Part ID
		if( SHOWDEBUG ){  cout << "Suggested removal direction: " << remDir << endl;  }
		// NOTE: This function assumes that 'remDir' is valid, otherwise it would not have been added to the precedence layer
		
		
		//  A. If the direction is downward-facing , Assume that it hits the floor and requeue
		if( remDir.dot( Eigen::Vector3d::UnitZ() ) < 0 ){
			if( SHOWDEBUG ){  cout << "Suggested removal direction points downward: Delay or Rotate? " << endl;  }
			// B. If the flag was not set , requeue
			if( !currFlag ){
				if( SHOWDEBUG ){  cout << "Flag was unset: Delay" << endl;  }
				set_R_flag( currEdge , true );
				execOrder.push( currEdgeDex );
				continue;
			}else{
			//  C. Else the flag was set , Must rotate!
				if( SHOWDEBUG ){  cout << "Flag was set: Rotate" << endl;  }
				//  D. Suggest a support to set the running assembly at
				supportVote = runningAsm->suggest_support_for_staightline_stability( subPartIDs , currBalanc , 
																		             params.distance , params.numSamples ); 
				//  E. Replicate the assembly
				tempAsm = runningAsm->replicate();
				//  F. Rotate the assembly
				tempAsm  -> set_lab_pose_for_index_3D( supportVote.bestDex , params.setdownLoc , true );
				runningAsm->set_lab_pose_for_index_3D( supportVote.bestDex , params.setdownLoc , true );
				//  G. Append a new state with a rotation edge
				currState = currState->add_edge_rotate( supportVote.bestDex , tempAsm );
				//  H. Obtain a new removal direction
				remDir = runningAsm->asm_center_of_freedom_direction( subPartIDs , currBalanc ); // Part ID
			}
		}
		
		if( SHOWDEBUG ){  cout << "Creating floor" << endl;  }
		//  A. Instantiate a temp floor below the asm
		Eigen::MatrixXd span = span_from_AABB( runningAsm->calc_AABB() );
		Floor_ASM tempFloor{ params.setdownLoc , span(0,0) * floorScale , span(0,1) * floorScale };
		
		
		
		
	}
	
	// N-1. Clean Up
	delif( runningAsm );
	// N. Return
	return initState;
}
