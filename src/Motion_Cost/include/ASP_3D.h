#pragma once // This also helps things not to be loaded twice , but not always . See below

/***********  
ASP_3D.h
James Watson , 2017 September
Structures and functions to geometric processing and assembly planning

Template Version: 2017-09-23
***********/

#ifndef ASP3D_H // This pattern is to prevent symbols to be loaded multiple times
#define ASP3D_H // from multiple imports

// ~~ Includes ~~
// ~ Eigen ~
#include <Eigen/Core> // ---- The living heart of Eigen
#include <Eigen/StdVector> // This is REQUIRED to put an Eigen matrix in an std::vector // Dont do that , Use Eigen::MatrixXd and .row(i)
#include <Eigen/Dense> // --- Cross Product , etc.
#include <Eigen/Geometry> //- Quaternion , etc
// ~ ROS : Transforms & RVIZ ~
#include <ros/ros.h> // ---------------------------- ROS presides over all
#include <visualization_msgs/Marker.h> // ---------- Display geometry
#include <visualization_msgs/MarkerArray.h> // ----- Marker Array , Gets rid of the flicker problem
// ~ quickhull ~
#include <QuickHull.hpp> // Convex hull


// ~ Local ~
#include <Cpp_Helpers.h> // Utilities and Shortcuts
#include <MathGeo_ASP.h> // Math and geometry utilities

// ~~ Shortcuts and Aliases ~~
#define DEFAULTNUMPOINTS  200 // Default number of points to allocate for a polygon
#define ASP_COPY_OFFSET  1000 // ID number offset for copies of objects

/// ~~~ ASP Constants ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

const double /* -- */ ASP_SHRINKDISTANC = 0.0005; // --------------------------- Default distance to shrink models, in meters
const Eigen::Vector3d ASP_VECTORDEFAULT = Eigen::Vector3d( 1.0 , 1.0 , 1.0 ); // Choose a default vector
const double /* -- */ DEF_CRIT_ANG      = 0.0698d; // -------------------------- Default angle criterion for plane matching
const double /* -- */ DEF_CRIT_DST      = 0.00001d; // ------------------------- Default distance criterion for plane matching
const double /* -- */ DEF_NDBG_JIGGLE   = 0.0015d; // -------------------------- Default distance [m] to jiggle parts for NDBG enhancement

/// ~~~ End Constants ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

// === Classes and Structs =================================================================================================================

/// === VISUAL MARKERS =====================================================================================================================

// URL , RVIZ Markers:  http://docs.ros.org/diamondback/api/rviz/html/marker__test_8cpp_source.html

// == class Rainbow ==

// Simple class to cycle colors for display
class Rainbow{

public:
	EIGEN_MAKE_ALIGNED_OPERATOR_NEW 
	Rainbow(); // Default constructor
	~Rainbow(); // Default destructor
	// add_color( Eigen::Vector3d nuColor ); // For now sticking with 
	void advance();
	Eigen::Vector3d get_color();
	Eigen::Vector3d advance_and_get();
	
protected:
	
	size_t index;
	// vector<Eigen::Vector3d> colors;
	// URL , Vectors of Eigen Types https://eigen.tuxfamily.org/dox-devel/group__TopicStlContainers.html
	std::vector< Eigen::Vector3d , Eigen::aligned_allocator< Eigen::Vector3d > > colors; // Wonky as hell 
	
};

// __ End Rainbow __

// ~ Forward Declarations ~
class Part_ASM;

// == class RViz_MeshMarker ==

class RViz_MeshMarker{
public:
	EIGEN_MAKE_ALIGNED_OPERATOR_NEW
	
	// ~ Class Methods ~
	static RViz_MeshMarker* deletion_marker( RViz_MeshMarker& other );
	
	// ~ Constructors & Destructors ~
	void _init();
	RViz_MeshMarker(); // Default constructor
	RViz_MeshMarker( Part_ASM& meshPart );
	RViz_MeshMarker( const Eigen::MatrixXd& verticesDense , const Eigen::MatrixXi& facetsDense ); // Create the mesh from triangles
	RViz_MeshMarker( const TriMeshVFN_ASP& triMesh ); // Create the mesh from TriMesh
	RViz_MeshMarker( const RViz_MeshMarker& other ); // Copy constructor
	~RViz_MeshMarker();
	// ~ Getters ~
	Pose_ASP get_pose();
	Eigen::MatrixXd get_vertices();
	Eigen::MatrixXd get_colors();
	std::vector<float> get_uniform_color() const;
	// ~ Setters ~
	void set_color( float R , float G , float B , float alpha = 1.0 ); // Set the marker color RGBA , default alpah=1 opaque
	void set_color( const std::vector<float>& color );
    void rand_color(); // Set the marker to a random color
    void set_alpha( float alpha ); // Set the opacity for this shape
	void set_scale( float Xscale , float Yscale , float Zscale ); // Set the scale of the marker -- 1x1x1 here means 1m on a side
	void set_scale( float uniformScale ); // Set all the dimensions of the marker to 'uniformScale'
	void set_pose( float pX , float pY , float pZ , float oW , float oX , float oY , float oZ ); // Abso pose in RViz
	void set_pose_from_part( Part_ASM& meshPart );
	void set_pose_from_marker( const RViz_MeshMarker& other );
	void set_id( int itemID ); // Set the ID number for this marker
	void set_tags( string frame_id , ros::Time stamp , string nameSpace ); // Set frame , time , and namespace data
	void set_timestamp( ros::Time stamp );
	// ~ Set Geo ~
	void load_triangles( const Eigen::MatrixXd& verticesDense , const Eigen::MatrixXi& facetsDense ); // Create the mesh from triangles
	void load_triangles( const RViz_MeshMarker& other );
	// ~ Getters ~
	int /* ----------------- */ get_id(); //---- Get the ID number for this marker
	visualization_msgs::Marker& get_marker(); // Get a reference to the marker itself
	void /* ---------------- */ get_color( float colorArr[4] ); //- Get the color of the marker and load it into 'colorArr' {r,g,b,a}
	// ~ Info / Debug ~
	void /*- */ print_color(); // - Print the color for debug
	void /*- */ print_pose(); // -- Print the pose  for debug

protected:
	void reload_color();
	visualization_msgs::Marker visMarker; // RViz marker
	std::vector<float> uniformColor = { 1,1,1,1 };
};

SuccessCode check_marker_OK( RViz_MeshMarker& meshMrkr );

// __ End RViz_MeshMarker __



// == class Rviz_MarkerManager ==

/// For keeping track of all the markers
class RViz_MarkerManager{
public:
	EIGEN_MAKE_ALIGNED_OPERATOR_NEW
	
	// ~ Class Methods ~
	static RViz_MarkerManager anti_manager( RViz_MarkerManager& other ); // Create a manager that erases all the markers of 'other'
	
	// ~ Constructor / Destructor ~
	RViz_MarkerManager(); //- Default constructor
	~RViz_MarkerManager(); // Default destructor
	// ~ Getters ~
	size_t /* ------------------- */ get_len(); // ------------- Get the number of markers presently stored
	visualization_msgs::MarkerArray& get_arr(); // ------------- Get the marker array , used for RVis drawing
	int get_next_ID_incr(); // --------------------------------- Get the next ID and increment
	// ~ Marker Management ~
	int /* ---------------------- */ add_marker( RViz_MeshMarker& meshMrkr , bool suppressIncr = false ); // ------------ Add a mesh marker to the manager
	int /* ---------------------- */ add_marker( visualization_msgs::Marker genericMrkr , bool suppressIncr = false ); // Add a generic marker to the manager
	visualization_msgs::MarkerArray& rebuild_mrkr_array(); // -- Rebuild the marker array after a change to individual markers
	// ~ Debug ~
	std::vector<SuccessCode>   diagnose_markers_OK(); // Return true if there are no NaN or Infinity values , Print report
	void /* --------------- */ spam_cubes(); // -------- Spam a ton of markers to RViz for us to overwrite later
	
	// ~ Members ~
	std::list<RViz_MeshMarker*> /* --- */ mrkrList; // std::list of pointers to markers for easy add/remove , used to rebuild 'mrkrArry'
	std::list<visualization_msgs::Marker> basicMrkrs;
	visualization_msgs::MarkerArray /*-*/ mrkrArry; // Message that will actually get sent to Rviz
	int nextID; // ---------------------------- ID number to be assigned to the next marker that is added
	// ros::NodeHandle // No , This will be handled by the client program
};

// __ End Rviz_MarkerManager __

/// ___ END VISUAL _________________________________________________________________________________________________________________________


/// === ASM GEOMETRY =======================================================================================================================

// === Structs ===

struct FaceIndices{ // Holds the result of a search for flat faces over an STL mesh
	EIGEN_MAKE_ALIGNED_OPERATOR_NEW
	std::vector<std::vector<int>>  faceLists; //-- vectors of facet indices that comprise faces
	std::vector< Eigen::Vector3d , Eigen::aligned_allocator< Eigen::Vector3d > > faceNormals; // normals of the faces
};


struct part_face{ // Identifies a part on a face , this should be unique within the problem
	llin part;
	llin face;
};

struct part_face_pair{ // Holds a pairing of 'part_face's , to be used in keeping track of adjacency
	part_face a;
	part_face b;
};

struct interference_result{ // Holds the result of a NDBG query
	bool              result; // ----------- Interference or no
	std::vector<llin> interferingPartIDs; // All the parts that interfere
};

class Polygon_ASM;

struct SupportHull{ // Structure to hold the support information for some 3D object or collection of objects
	EIGEN_MAKE_ALIGNED_OPERATOR_NEW
	std::vector< Polygon_ASM > /* --- */ supports; // --- Vector of all of the support face objects
	Eigen::Vector3d /* -------------- */ COM; // -------- Center of mass _____________________________ , asm frame
	TriMeshVFN_ASP /* --------------- */ hullVFN; // ---- Convex hull ________________________________ , asm frame
	std::vector< std::vector< double > > spptEdgeDist; // Distance to each of the edges of the support , face frame
	std::vector< bool > /* ---------- */ stableFace; // - Flag for each support face , true == stable
	double /* ----------------------- */ volume; // ----- Volume of the contained objects
};

class LocalFreedom_ASM;

struct NDBGrelation{
	EIGEN_MAKE_ALIGNED_OPERATOR_NEW
	std::vector< std::vector< LocalFreedom_ASM > > NDBG; // ------ Non-Directional Blocking Graph
	std::vector< part_face_pair > /* ---------- */ adjacencies; // Part adjacency relationships
};

// ___ End Struct ___

// ~ Forward Declarations ~
class Part_ASM;
class Assembly_ASM;
class Floor_ASM;

// == class Polygon_ASM ==

class Polygon_ASM{
	
friend class Part_ASM;
friend class Assembly_ASM;
friend std::vector<Polygon_ASM> Polygon_ASM_vector_at_pose( std::vector<Polygon_ASM>& original , Pose_ASP labPose );
	
public:
	EIGEN_MAKE_ALIGNED_OPERATOR_NEW // REQUIRED MACRO FOR EIGEN

	// ~~ Constructors & Destructors ~~
	Polygon_ASM(); // ----------------------- Default constructor
	Polygon_ASM( const Eigen::MatrixXd& Vertices , const Eigen::MatrixXi& Facets , std::vector<int>& siDices , 
				 const Eigen::Vector3d& normalVec ); // Constructor from reduced model
	Polygon_ASM( Eigen::MatrixXd points ); // Points constructor
	Polygon_ASM( const Polygon_ASM& original ); // Copy constructor
	//~Polygon_ASM(); // ---------------------- Destructor , NOT NEEDED
	
	// ~~ Getters ~~
	// ~ Bookkeeping ~
	size_t /* -- */ get_len(); // -------------- NOT USED?
	int /* ----- */ get_num_facets(); // ------- Number of facets that make up the polygonal face
	part_face /*-*/ get_ID(); // --------------- Return { part , face } identifier corresponding to this face
	Eigen::MatrixXi get_facets(); // ----------- Return vertex indices corresponding to each of the facets ( same for all frames ) 
	size_t /* -- */ get_number_of_segments(); // Return the number of border segments
	double /* -- */ calc_area(); // ------------ Calc and Return the area in [unit^2] , Where unit is the same as the original mesh length
	double /* -- */ get_area(); // ------------- Return the area in [unit^2] , Where unit is the same as the original mesh length
	// ~ Local Frame (2D) ~
	Eigen::MatrixXd populate_CCW_polygon_verts_2D(); // Get the polygon so that we can do point-in-poly tests ( stability )
	Eigen::MatrixXd get_2D_vertices(); // ------------- Return VFN vertices that make up this face _ , local frame ( 2D )
	Eigen::Vector2d get_2D_COM_proj(); // ------------- Return the part COM projected onto this poly , local frame ( 2D )
	Eigen::MatrixXd get_2D_CCWpolyV(); // ------------- Get the vertices of the bounding polygon ___ , local frame ( 2D )
	// ~ Part Frame ~
	Eigen::Vector3d get_center(); // ---- Return center in the ____________________________ , part frame
	Eigen::Vector3d get_X_basis(); // --- X Basis _________________________________________ , part frame
	Eigen::Vector3d get_Y_basis(); // --- Y Basis _________________________________________ , part frame
	Eigen::Vector3d get_norm(); // ------ Z Basis , Return normal vector in the ___________ , part frame
	TriMeshVFN_ASP  get_VFN(); // ------- Return a trimesh of all of the facets of the face , part frame
	Eigen::Vector3d get_COM_proj_3D(); // Return the COM projected onto the polygon in the part frame (NOT recalculated!)
	
	// ~ Lab Frame ~
	Eigen::MatrixXd get_lab_vertices(); // ----------------- Return VFN vertices that make up this face  , lab frame
	Eigen::MatrixXd get_lab_bases(); // -------------------- Return basis vectors for this facet _______ , lab frame
	Eigen::Vector3d get_lab_center(); // ------------------- Return face center in the ___________________ lab frame
	Eigen::Vector3d get_lab_norm(); // --------------------- Return face normal in the ___________________ lab frame
	Eigen::MatrixXd get_lab_border( bool closed = true ); // Return vertices that make up polygon border , lab frame
	
	// ~~ Setters ~~
	// ~ Bookkeeping ~
	void set_ID( llin partID , llin faceID ); // Set { part , face } identifier corresponding to this face
	// ~ Local Frame ~
	void set_2D_COM_proj( Eigen::Vector2d projCOM2D );
	void set_2D_CCWpolyV( Eigen::MatrixXd borderVerts );
	void set_2D_CCWborderSegments( std::vector<Segment2D_ASP> borderSegments );
	// ~ Part Frame ~
	void set_part_COM_proj( Eigen::Vector3d projCOM3D );
	// ~ Lab Frame ~
	void set_lab_bases_from_part( Part_ASM& parentPart );
	
	// ~~ Geo Calcs ~~
	void calc_lab_vertices( Eigen::Vector3d& partCenterLab , // ---- Calc lab pose for face , bases , verts
	                        Eigen::Quaterniond& partOrientLab );
	void calc_lab_vertices( Pose_ASP labPose ); // ---- Calc lab pose for face , bases , verts
	void add_neighbor( part_face nuNeighbor ); // ------------------ Add a neighbor to the list
	void clear_neighbors(); // ------------------------------------- Erase all neighbors from the list
	std::vector<std::vector<int>> calc_ordered_facet_neighbors(); // Calculate , populate , and return ordered facet neighbors
	Eigen::MatrixXd project_lab_pnts_onto_3D_plane_2D( Eigen::Vector3d planeOrgn , Eigen::Vector3d planeNorm , Eigen::Vector3d planeXbas );
	Eigen::MatrixXd project_lab_brdr_onto_3D_plane_2D( Eigen::Vector3d planeOrgn , Eigen::Vector3d planeNorm , Eigen::Vector3d planeXbas , 
													   bool close = true );
	bool is_adjacent_to( Polygon_ASM& other , double CRIT_ANG , double CRIT_DST ); // Return true if one of poly's facets adjacent to any 'other' facet 
	double adjacent_area( Polygon_ASM& other , size_t lvl = 20 ); // Get the area shared between polygons, assuming that they are adjacent

protected:
	// ~ Bookkeeping ~
	part_face /* ------------- */ ID; // ------------------ Identifier for this face , unique within the part // { part , face }
	size_t /* ---------------- */ numPoints; // ----------- Number of point in the polygon
	std::vector< part_face >      neighbors; // ----------- List of all of the faces adjacent to this face
	Eigen::MatrixXi /* ------- */ facets; // -------------- Matrix of indices for both repeating 2D and repeating 3D vertices
	std::vector<std::vector<int>> facetNeighborsOrdered; // Ordered facet list 
	double /* ---------------- */ area; // ---------------- Surface area of all the facets
	// ~ Local frame (2D) ~
	Eigen::MatrixXd /* ---- */ points2D; // -------- Matrix of R2 points with respect to the polyon center , local frame , 2D
	Eigen::Vector2d /* ---- */ COMproj2D; // ------- Projection of the COM onto the support face _________ , local frame , 2D
	Eigen::MatrixXd /* ---- */ CCWpolyV; // -------- CCW vertices of the bounding polygon __________________ local frame , 2D
	std::vector<Segment2D_ASP> CCWborderSegments; // CCW vertices of the bounding polygon __________________ local frame , 2D
	// ~ Part Frame ~
	Eigen::Vector3d    center3D; // -- Center of the polygon in R3 ___________________ , part frame
	Eigen::Vector3d    xBasis; // ---- X basis for the polygon points , expressed in the part frame
	Eigen::Vector3d    yBasis; // ---- Y basis for the polygon points , expressed in the part frame
	Eigen::Vector3d    normal3D; // -- Z basis , Normal vector of the plane in R3 ____ , part frame
	Eigen::Quaterniond orientation; // Orientation in the ______________________________ part frame
	Eigen::MatrixXd    vertsPrt; // -- Matrix of R3 points with respect to _____________ part frame
	Eigen::Vector3d    COMproj3D; // - Projection of the COM onto the support face , ___ part frame
	
	// ~ Lab Frame ~
	Eigen::Transform< double , 3 , Eigen::Affine > faceTransform; // Transform from the face frame to the lab frame
	Eigen::MatrixXd /* ------------------------ */ vertcs3D; // ---- Matrix of R3 points with respect to lab frame
	Eigen::MatrixXd /* ------------------------ */ labBases; // ---- Basis vectors of the face , expressed in the lab frame
	Eigen::Vector3d /* ------------------------ */ labCentr; // ---- Center of the polygon in R3 , lab frame
};

std::vector<Polygon_ASM> copy_Polygon_ASM_vector(       std::vector<Polygon_ASM>& original );
std::vector<Polygon_ASM> copy_Polygon_ASM_vector( const std::vector<Polygon_ASM>& original );

bool area_A_lessThan_B( Polygon_ASM& A , Polygon_ASM& B ); // Return true if 'A' area less than 'B' area, Otherwise return false
bool area_A_grtrThan_B( Polygon_ASM& A , Polygon_ASM& B ); // Return true if 'A' area greater than 'B' area, Otherwise return false

std::vector<Polygon_ASM> copy_Poly_vec_decreasing_area( std::vector<Polygon_ASM>& original ); // Return a copy of the vector sorted by area

std::vector<Polygon_ASM> Polygon_ASM_vector_at_pose( std::vector<Polygon_ASM>& original , Pose_ASP labPose );

IndexSearchResult match_vec2_poly_w_vec1_index( std::vector<Polygon_ASM>& vec1 , std::vector<Polygon_ASM>& vec2 , // Find coplanar with index
												size_t index , double CRIT_ANG = DEF_CRIT_ANG , double CRIT_DST = DEF_CRIT_DST );
												
IndexSuccesLookup best_match_vec2_poly_w_vec1_index( std::vector<Polygon_ASM>& vec1 , std::vector<Polygon_ASM>& vec2 , 
													 size_t index , double CRIT_ANG = DEF_CRIT_ANG , double CRIT_DST = DEF_CRIT_DST );

IndexMatchResult best_match_vec1_poly_to_vec2_poly( std::vector<Polygon_ASM>& vec1 , std::vector<Polygon_ASM>& vec2 , 
													double CRIT_ANG = DEF_CRIT_ANG , double CRIT_DST = DEF_CRIT_DST );

std::vector<std::pair<size_t,size_t>> all_matches_vec1_poly_to_vec2_poly( std::vector<Polygon_ASM>& vec1 , std::vector<Polygon_ASM>& vec2 , 
													                      double CRIT_ANG = DEF_CRIT_ANG , double CRIT_DST = DEF_CRIT_DST );
													                      
bool is_arg_on_side_0_pair( size_t arg , const std::vector<std::pair<size_t,size_t>>& indexPairs );
bool is_arg_on_side_1_pair( size_t arg , const std::vector<std::pair<size_t,size_t>>& indexPairs );

void polygon_vec_report( std::vector<Polygon_ASM>& polyVec ); // Print information about the polygons

// __ End Polygon_ASM __


// == class LocalFreedom_ASM ==

class LocalFreedom_ASM{
	
public:
	EIGEN_MAKE_ALIGNED_OPERATOR_NEW // REQUIRED MACRO FOR EIGEN

	// ~ Constructor / Destructor ~
	LocalFreedom_ASM(); //- Default constrctor
	~LocalFreedom_ASM(); // Destructor
	
	// ~ Bookkeeping ~
	void clear(); // -------------------------------------------------- Remove all elements
	void overwrite_with_new( const Eigen::MatrixXd& nuConstraints ); // Remove all old constraints and add new
	
	// ~ Getters ~
	size_t get_number_of_constraints(); // Return the number of constraint normals contained by the structure
	
	// ~ Constraint Testing ~
	bool /* ---- */ test_direction( const Eigen::Vector3d& direction );
	Eigen::Vector3d suggest_safe_direction( const Eigen::Vector3d& direction );
	void /* ---- */ add_constraint( const Eigen::Vector3d& direction );
	Eigen::MatrixXd get_all_constraint_normals();
	// ~ Debug ~
	void /* ---- */ print_all_constraints();

protected:
	
	std::vector< Eigen::Vector3d , Eigen::aligned_allocator< Eigen::Vector3d > > constraintNormals;

};

// __ End LocalFreedom_ASM __


// == class Part_ASM ==

/**
For now, not storing the STL data inside of the object , only the path to it

The center of the part is the center of the STL

**/

class Part_ASM{
	
public:
	EIGEN_MAKE_ALIGNED_OPERATOR_NEW // REQUIRED MACRO FOR EIGEN
	
	// ~ Constructors and Destructors ~
	Part_ASM( llin partID ); // Default constructor
	Part_ASM( llin partID , string STLpath , // Manual constructor
			  Eigen::MatrixXd verticesDense   , Eigen::MatrixXi facetsDense   , 
			  Eigen::MatrixXd verticesReduced , Eigen::MatrixXi facetsReduced , 
			  std::vector< std::vector<int> > facesList ,
			  std::vector< Eigen::Vector3d , Eigen::aligned_allocator< Eigen::Vector3d > > faceNormals ); 
	Part_ASM( llin partID , string STLpath );  // Automatic constructor
	Part_ASM( const Part_ASM& original ); // Copy Constructor
	~Part_ASM(); // Default destructor
	
	// ~ Getters ~
	// VFN Mesh
	Eigen::MatrixXd& /* ---------- */ get_dense_vertices(); // -------------------- Part Frame
	const Eigen::MatrixXd& /* ---- */ get_dense_vertices() const; // -------------- Part Frame
	Eigen::MatrixXi& /* ---------- */ get_dense_facets(); 
	const Eigen::MatrixXi& /* ---- */ get_dense_facets() const; 
	Eigen::MatrixXi& /* ---------- */ get_reduc_facets();
	size_t /* -------------------- */ get_number_of_faces();
	TriMeshVFN_ASP /* ------------ */ get_dense_VFN(); // --- Fetch the dense trimesh , Part Frame
	double /* -------------------- */ calc_surface_area(); // Calculate the surface area of the part mesh
	double /* -------------------- */ get_surface_area(); //- Return the surface area of the part mesh
	// Pose
	Eigen::Vector3d& /* ---------- */ get_position(); // ----------- Lab Frame
	Eigen::Quaterniond& /* ------- */ get_orientation(); // -------- Lab Frame
	Pose_ASP /* ------------------ */ get_pose(); // --------------- Lab Frame
	Pose_ASP /* ------------------ */ get_pose_w_matx(); // -------- Lab Frame
	TriMeshVFN_ASP /* ------------ */ get_lab_VFN(); // ------------ Lab Frame
	Eigen::Vector3d /* ----------- */ get_shift(); // -------------- Part Frame
	// Flat Surfaces
	std::vector< Polygon_ASM >& /* */ get_all_faces(); // ---------------------- Part Frame
	const std::vector< Polygon_ASM >& get_all_faces() const; // ---------------- Part Frame
	Eigen::Vector3d /* ----------- */ get_face_lab_norm_by_ID( llin faceID ); // Lab Frame
	Polygon_ASM* /* -------------- */ get_face_by_ID( llin faceID );
	// Supports
	std::vector< Polygon_ASM >& /* */ get_all_supports();
	const std::vector< Polygon_ASM >& get_all_supports() const;
	// Collision
	RAPID_Geo& /* ---------------- */ get_collsn_geo_ref(); // Get a reference to the collision geometry
	// Misc 
	RViz_MeshMarker* /* ---------- */ get_mrkr_ptr();  
	llin /* ---------------------- */ get_ID();
	double /* -------------------- */ get_volume();
	double /* -------------------- */ get_radius(); // Get the furthest distance of any vertex from the COM
	string /* -------------------- */ get_STL_path();
	
	// ~ Markers ~ 
	// Part
	void load_part_mrkrs_into_mngr( RViz_MarkerManager& mngr ); // Fetch the PART marker and load it into the manager __ , Lab Frame
	void get_part_mrkr_color( float color[4] ); // Set the part marker color
	void set_part_mrkr_color( float R , float G , float B , float alpha = 1.0 ); // Set the part marker color
	void set_part_mrkr_color( const std::vector<float>& color ); // Set the part marker color
	void rand_part_mrkr_color();
	void update_part_mrkr_pose();  // ---------------------------- Use the lab pose to update poses of part marker _____ , Lab Frame
	void set_marker_opacity( float alpha );
	// Face
	void populate_face_markers(); // ----------------------------- Generate markers for all of the identified faces ____ , Lab Frame
	void load_face_mrkrs_into_mngr( RViz_MarkerManager& mngr ); // Fetch the FACE markers and load them into the manager , Lab Frame
	void update_all_face_mrkr_pose(); // ------------------------- Use the lab pose to update poses of faces ___________ , Lab Frame
	
	// ~ Info ~
	void status_report(); // Print some info to help troubleshoot whether the poly was properly formed
	
	// ~ Setters ~
	void set_ID( llin id );
	void set_position( const Eigen::Vector3d& pos , bool updateAuxGeo = true );
	void set_orientation( const Eigen::Quaterniond& orient , bool updateAuxGeo = true );
	void set_pose( const Eigen::Vector3d& pos , const Eigen::Quaterniond& orient , bool updateAuxGeo = true );
	void set_pose( double pX , double pY , double pZ , double oW , double oX , double oY , double oZ , bool updateAuxGeo = true );
	void set_pose( const Pose_ASP& partPose , bool updateAuxGeo = true );
	
	// ~ Geometry ~
	void calc_all_faces(); // Calculate the lab positions of all of faces
	
	// ~ Stability ~
	void determine_support_stability( double angleCritRad = DEF_CRIT_ANG ); // Calculate the part positions of all of supports
	//                                              3 deg = 0.0523 rad
	void /* ------ */ report_support_stability(); //  Print a verbose accounting of the supporting surfaces
	Pose_ASP /* -- */ get_lab_pose_for_support_index( size_t supportIndex , Eigen::Vector2d offsetXY );
	bool /* ------ */ is_support_i_stable( size_t supportIndex );
	std::vector<bool> get_stability_determination();
	void /* ------ */ move_COM_to_origin(); // Shift all of the vertices so that the COM is at (0,0,0) , part frame
	void /* ------ */ unshift_COM();  // Undo the above operation
	
	// ~ Collision ~
	void reload_collision_model( double shrinkDist = ASP_SHRINKDISTANC ); // Load the collision mesh with a slightly shrunken trimesh
	bool collides_with( Part_ASM& other ); // Determine if this part collides with 'other'
	bool collides_with( Floor_ASM& floor ); // Determine if this part collides with 'floor'
	
	// ~ Bookkeeping ~
	void align_polygon_ID_w_part(); // Make sure that all the faces recognize this Part_ASM as their container

protected:
	// ~ Bookkeeping ~
	string meshPath; // Path to the STL source used to generate
	llin ID; // Unique ID for each part
	// ~ Pose ~
	Eigen::Vector3d    position; // -- Center of the part in R3 , lab frame
	Eigen::Quaterniond orientation; // Orientation in the _____ , lab frame
	Eigen::Vector3d    shiftVec; // -- Shift applied to vertices at creation-time
	// ~ Faces / Surfaces ~
	std::vector< Polygon_ASM > faces; // ----- Vector of all of the face objects
	double /* ------------- */ surfaceArea; // Surface area of the mesh
	// ~ Geometry ~
	// Dense facets are for display ( RViz colors meshes with vertex shading so neighboring colors cannot share a vertex w/o blending )
	Eigen::MatrixXd vertcs3D; //- Dense points of the mesh
	Eigen::MatrixXi facets; // -- Facets corresponding to the dense points
	// Reduced facets are for calcs
	Eigen::MatrixXd reducdVtx; // Reduced points of the mesh
	Eigen::MatrixXi reducdFct; // Facets corresponding to the reduced points
	Eigen::MatrixXd reducdNrm; // Normals corresponding to the reduced facets // These should be the same as dense
	// ~ Support ~
	Eigen::Vector3d /* -------------- */ COM; // -------- Center of mass , part frame
	TriMeshVFN_ASP /* --------------- */ hullVFN; // ---- Convex hull , part frame
	std::vector< Polygon_ASM > /* --- */ supports; // --- Vector of all of the face objects
	std::vector< bool > /* ---------- */ stableFace; // - Flag for each support face , true == stable
	std::vector< std::vector< double > > spptEdgeDist; // Distance to each of the edges of the support
	double /* ----------------------- */ leastDist; // -- Least distance to a support edge
	double /* ----------------------- */ volume; // ----- Volume of the part , in the same units as the source STL
	// ~ Markers ~
	RViz_MeshMarker /* -------- */ partMrkr; //- Visual mesh for the part as a whole
	std::vector< RViz_MeshMarker > faceMrkrs; // List of visual meshes for each of the recovered sides
	float markerOpacity = 1.0;
	// ~ Planning ~
	RAPID_Geo /*- */ collsnGeo; // Collision geometry for RAPID
	LocalFreedom_ASM freedom; // - Local part freedom // What is the context?
	
};

// __ End Part_ASM __


// == class PartsCollection ==

class PartsCollection{
	// A grouping of loose parts for which intense geometric processing is not required
	

public:	
	EIGEN_MAKE_ALIGNED_OPERATOR_NEW // REQUIRED MACRO FOR EIGEN
	
	// ~ Con/Destructors ~
	PartsCollection(); // ----------------------------------------- Empty constructor
	PartsCollection( const std::vector<Part_ASM*>& collection ); // Constructor with a vector of part pointers
	~PartsCollection(); // ---------------------------------------- Destructor (erase all parts!)
	
	// ~ Bookkeeping ~
	void /*-*/ add_part_ptr( Part_ASM* prtPtr ); // ------------------ Add one part with no offset
	void /*-*/ add_part_ptr( Part_ASM* prtPtr , Pose_ASP& offset ); // Add one part with specified offset
	bool /*-*/ index_OK( size_t index ); // -------------------------- Check if the user specified a valid index
	Part_ASM*& operator[]( size_t index ); // ------------------------ Access the pointer at 'index'
	
	// ~ Getters ~
	size_t   size(); // -------------------------------------------- Return the number of parts stored
	Pose_ASP get_pose( size_t index );
	
	// ~ Setters ~
	void set_pose( const std::vector<Pose_ASP>& nuPoses ); // - Set the pose of every part
	void set_pose( const std::vector<Pose_ASP>& nuPoses , // -- Set the pose of selected parts
				   size_t bgn , size_t end ); 
	void set_pose( const Pose_ASP& nuPose , size_t index ); //- Set the pose of the selected part
	void set_pose( const Pose_ASP& nuPose ); // --------------- Set the pose of all the parts
	void set_offset( size_t index , Pose_ASP& offset ); // ---- Set the offset / relative pose for the specified part
	void unshift_all(); // ------------------------------------ Remove the COM shift from all constituent parts.  2018-06-25: No effect?
	
	// ~ Visualization ~
	void load_part_mrkrs_into_mngr( RViz_MarkerManager& mrkrMngr ); // ------------------------- Load part markers into marker manager
	void load_part_mrkrs_into_mngr( RViz_MarkerManager& mrkrMngr , size_t bgn , size_t end ); // Load selected part markers into marker manager
	
protected:	
	std::vector<Part_ASM*> partPtrVec; // Pointers to a collection of parts
	std::vector<Pose_ASP>  offsets; // -- Relative poses to which the  // FIXME : START HERE
};

// __ End PartsCollection __


// == class Floor_ASM ==

class Floor_ASM{ // Represents the perfectly flat assembly floor on which all parts must rest , normal is Z+
	friend class Part_ASM; // Allow parts access to the floor mesh
public:	
	EIGEN_MAKE_ALIGNED_OPERATOR_NEW // REQUIRED MACRO FOR EIGEN
	
	// ~ Con/Destructors ~
	Floor_ASM( Eigen::Vector3d centerLoc , double xWidth , double yLngth );
	
	// ~ Planning ~
	bool collides_with( PartsCollection& effector );
	
private:
	Eigen::Vector3d center; // -- Center of the rectangle
	TriMeshVFN_ASP  floorMesh; // flat rectangle is defined by 2 rectangles
	RAPID_Geo /*-*/ collsnGeo; // Collision geometry for RAPID
};

// __ End Floor_ASM __


// == class Assembly_ASM ==

struct PartEntry_ASM{ // Holds a part and its pose within the assembly
	EIGEN_MAKE_ALIGNED_OPERATOR_NEW
	Part_ASM part;
	Pose_ASP pose;
};

class Assembly_ASM{
public:
	EIGEN_MAKE_ALIGNED_OPERATOR_NEW // REQUIRED MACRO FOR EIGEN
	
	// ~ Constants & Class Variables ~
	static constexpr size_t DEFAULT_TRI_LVL = 30;
	
	// ~ Constructors and Destructors ~
	Assembly_ASM( llin asmID ); // ---------------------------------------- Empty constructor
	Assembly_ASM( llin asmID , std::list< PartEntry_ASM >& pPartsList ); // Parts list constructor
	~Assembly_ASM(); // --------------------------------------------------- Default destructor
	
	// ~ Bookkeeping ~
	std::vector<llin> /* -------- */ get_part_ID_vec(); // Return a vector of all the part IDs in the order that they appear in the list
	std::vector<llin> /* -------- */ get_balance_IDs_from_sub( const std::vector<llin>& subIDVec ); // Get the balance remaining after removing 'subIDVec'
	void /* --------------------- */ add_part_w_pose( Part_ASM& part , Pose_ASP pose ); // Add a part that has a relative pose in the asm frame
	bool /* --------------------- */ remove_part_w_ID( llin ID ); // Remove the part that has the 'partID' matching 'ID' , True on success and false otherwise
	IndexSearchResult /* -------- */ index_w_ID( llin ID ); // Return the index that holds the partID
	IDSearchResult /* ----------- */ index_to_partID( size_t ndx ); // Return the part ID corresponding to the structure index { liaison , NDBG }
	std::vector<size_t> /* ------ */ partID_vec_to_index_vec( const std::vector<llin>& partVec ); // Build a vector of corresponding to the indices that hold part IDs
	std::vector<llin> /* -------- */ index_vec_to_partID_vec( const std::vector<size_t>& indxVec ); // Build a vector of corresponding to the indices that hold part IDs
	std::vector<std::vector<size_t>> partID_vec_vec_to_index_vec_vec( const std::vector<std::vector<llin>>& partVecVec ); // Build a vector of corresponding to the indices that hold part IDs
	std::vector<std::vector<llin>>   index_vec_vec_to_partID_vec_vec( const std::vector<std::vector<size_t>>& indxVecVec ); // Build a vector of corresponding to the indices that hold part IDs
	
	// ~ Geometry ~
	VolumeCentroid      calc_COM( std::vector<llin> partsVec ); // --------- Calc volume centroid of some subset of parts
	void /* -------- */ calc_COM(); // ------------------------------------- Calculate the center of mass , Handy to do after part addition / removal
	Eigen::MatrixXd     calc_AABB(); // ------------------------------------ Calc, set, and get the AABB
	Eigen::MatrixXd     lab_AABB(); // ------------------------------------- Calc and get the AABB
	void /* -------- */ recalc_geo( double angleCritRad = DEF_CRIT_ANG ); // Recalculate the NDBG and support hull
	std::vector<double> get_part_volumes(); // ----------------------------- Return a vector of all the part volumes in the order that they appear in the list
	IDDbblResult /*- */ largest_part_ID_w_vol(); // ------------------------ Return the ID of the most voluminous part along with the volume
	Eigen::MatrixXd     get_part_centroids(); // --------------------------- Return a row-list matrix of all the part locations
	void /* -------- */ shift_part( llin ID , const Eigen::Vector3d& d ); // Shift the specified part within the assembly by 'd'
	TriMeshVFN_ASP      get_lab_VFN( const std::vector<llin>& partsVec ); // Get VFN for a sub
	TriMeshVFN_ASP      get_lab_VFN(); // ---------------------------------- Get VFN for the entire ASM
	void /* -------- */ report_liaison_and_self_collision_state(); // ------ Print out a report of what is adjacent and what is colliding
	IndexSearchResult   get_support_from_current_pose( double angleCritRad = DEF_CRIT_ANG );
	
	// ~ Getters ~
	size_t /* ------ */ how_many_parts(); // --------------------------------------- Return the number of parts that are contained in the assembly
	Part_ASM& /* --- */ get_part_ref_w_ID( llin ID ); // --------------------------- Return a reference to the part that has the 'partID' matching 'ID' 
	PartEntry_ASM&      get_entry_ref_w_ID( llin ID ); // -------------------------- Return a reference to the part entry that has the 'partID' matching 'ID' 
	Part_ASM* /* --- */ get_part_ptr_w_ID( llin ID ); // --------------------------- Return a pointer to the part that has the 'partID' matching 'ID' , or null
	Part_ASM& /* --- */ get_part_ref_w_index( size_t index ); // ------------------- Return a reference to the part at the specified index
	Pose_ASP /* ---- */ get_pose(); // --------------------------------------------- Get the assembly pose ______ , lab frame
	Eigen::Quaterniond  get_orientation(); // -------------------------------------- Get the assembly orientation , lab frame
	Pose_ASP /* ---- */ get_pose_w_ID( llin ID ); // ------------------------------- Return the asm pose of the part that has the 'partID' matching 'ID' 
	Pose_ASP /* ---- */ get_lab_pose_w_ID( llin ID ); // --------------------------- Return the lab pose of the part that has the 'partID' matching 'ID' 
	Eigen::MatrixXd     get_dense_vertices(); // ----------------------------------- Get all of the vertices of all of the parts , asm frame
	Eigen::MatrixXd     get_dense_vertices( Pose_ASP asmPose ); // ----------------- Get all of the vertices of all of the parts , given frame
	Eigen::MatrixXd     get_dense_vertices( std::vector<llin> partsVec ); // ------- Get the vertices of only the parts with IDs in 'partsVec' , asm frame
	Eigen::MatrixXd     get_dense_vertices( std::vector<llin> partsVec , // -------- Get the vertices of only the parts with IDs in 'partsVec' , given frame
											Pose_ASP asmPose ); 
	Eigen::MatrixXd     get_dense_lab_vertices(); // ------------------------------- Get all of the vertices of all of the parts , lab frame
	Eigen::MatrixXd     get_every_lab_center_except( llin excludeID ); // ---------- Return lab centers for all parts except that indicated
	Eigen::MatrixXd     get_part_lab_centers( std::vector<size_t>& selectDices ); // Return row-list of selected part locations , lab frame
	Eigen::MatrixXd     get_part_lab_centers( std::vector<llin>&   selectIDs   ); // Return row-list of selected part locations , lab frame
	Eigen::MatrixXd     get_part_asm_centers( std::vector<size_t>& selectDices ); // Return row-list of selected part locations , asm frame
	Eigen::MatrixXd     get_part_asm_centers( std::vector<llin>&   selectIDs   ); // Return row-list of selected part locations , asm frame
	std::vector<llin>&  get_NDBG_PartID_Lookup(); // ------------------------------- Return 'NDBG_PartID_Lookup' reference
	size_t /* ------ */ get_support(); // ------------------------------------------ Return the current support for this assembly 
	size_t /* ------ */ how_many_supports(); // ------------------------------------ Return the number of supports for this assembly 
	
	// ~ Setters ~
	void /* ---- */ set_pose( Pose_ASP asmPose ); // ------------------------ Set the 3D pose for the assembly ___________ , lab frame
	bool /* ---- */ set_support( size_t supportIndex ); // ------------------ Assign the current support for this assembly , Return true if successful
	// ---------------------------------------------------------------------- Assign the current support for this assembly based on its pose , no failure crit
	IndexSearchResult set_support_from_current_pose( double angleCritRad = DEF_CRIT_ANG ); 
	
	// ~ Liaisons ~
	std::vector<std::vector<bool>>   get_liaison_graph(); // ----------------- Return a copy of the liaison graph 
	std::vector<std::vector<double>> get_shared_area_graph(); // ------------- Return a copy of the shared area graph 
	std::vector<size_t> /* ------ */ get_liaison_neighbors( size_t query ); // Get the liaison neighbors of part with index 'query' 
	std::vector<llin> /* -------- */ get_liaison_neighbors( llin query ); // - Get the liaison neighbors of part with ID 'query' 
	
	// ~ Markers ~
	void set_marker_opacity( float alpha );
	void load_face_mrkrs_into_mngr( RViz_MarkerManager& mngr );
	void load_part_mrkrs_into_mngr( RViz_MarkerManager& mngr );
	
	// ~ Stability ~
	void determine_support_stability( double angleCritRad = DEF_CRIT_ANG ); // Calculate the asm positions of all of supports
	//                                              3 deg = 0.0523 rad
	SupportHull determine_subasm_stability( std::vector<llin> subAsm , // Calculate the asm positions of all of supports 
											double angleCritRad = DEF_CRIT_ANG ); // for some subset of current parts , part ID
	SupportHull determine_subasm_stability( std::vector<llin> subAsm , // Calculate the asm positions of all of supports 
											Pose_ASP labPose , //           for some subset of current parts , part ID
											double angleCritRad = DEF_CRIT_ANG ); // at the designated pose
	//                                                    3 deg = 0.0523 rad
	
	bool validate_subasm_stability_for_support( std::vector<llin> subAsm , size_t supportIndex , // Will sub support like 
												double CRIT_ANG = DEF_CRIT_ANG , double CRIT_DST = DEF_CRIT_DST ); // 'supportIndex' ?
												
	bool validate_partID_subset_does_not_float( std::vector<llin> subAsm , 
												double CRIT_ANG = DEF_CRIT_ANG , double CRIT_DST = DEF_CRIT_DST );
	bool validate_subasm_remainder_does_not_float( std::vector<llin> subAsm , 
												   double CRIT_ANG = DEF_CRIT_ANG , double CRIT_DST = DEF_CRIT_DST );
	bool validate_subasm_remainder_does_not_float( size_t originalSupport , std::vector<llin> subAsm , 
												   double CRIT_ANG = DEF_CRIT_ANG , double CRIT_DST = DEF_CRIT_DST );
												
	void /* ------ */ report_support_stability(); //  Print a verbose accounting of the supporting surfaces
	Pose_ASP /* -- */ get_lab_pose_for_support_index( size_t supportIndex , Eigen::Vector2d offsetXY );
	Pose_ASP /* -- */ set_lab_pose_for_support_index( size_t supportIndex , Eigen::Vector2d offsetXY , bool setSupportAlso = false );
	Pose_ASP /* -- */ get_lab_pose_for_index_3D( size_t supportIndex , Eigen::Vector3d offsetXYZ );
	Pose_ASP /* -- */ set_lab_pose_for_index_3D( size_t supportIndex , Eigen::Vector3d offsetXYZ , bool setSupportAlso = false  );
	bool /* ------ */ is_support_i_stable( size_t supportIndex );
	std::vector<bool> get_stability_determination();
	void /* ------ */ move_COM_to_origin(); // Shift all of the vertices so that the COM is at (0,0,0) , asm frame // NOT USED
	Eigen::Vector3d   get_support_normal( size_t supDex ); // Get the normal for specified support in the assembly frame
	Eigen::Vector3d   get_lab_support_normal( size_t supDex ); // Get the normal for specified support in the lab frame
	
	
	// ~ NDBG ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
	
	// Return a reference to the NDBG (NDBG order)
	std::vector<std::vector<LocalFreedom_ASM>>& get_NDBG(); 
	
	// Create the Non-Directional Blocking graph for the parts presently in the assembly
	void /* -------- */ calc_NDBG( double CRIT_ANG = DEF_CRIT_ANG , double CRIT_DST = DEF_CRIT_DST ); 
	void /* -------- */ enhance_NDBG( double dDist , double CRIT_ANG = DEF_CRIT_ANG , double CRIT_DST = DEF_CRIT_DST ); 
	
	// Get the NDBG indices for the associated Part IDs
	std::vector<size_t> NDBG_order_vec_from_Part_ID_vec( std::vector<llin> subAsm ); 
	
	// Return the interference of 'movedParts' against 'referenceParts' in the 'moveDir' // All part numbers NDBG order
	interference_result query_asm_NDBG( std::vector<size_t> movedParts , 
										std::vector<size_t> referenceParts ,
										const Eigen::Vector3d& moveDir ); 
	
	// Return the interference of 'movedParts' against 'referenceParts' in the 'moveDir' // Standardized on Part ID
	interference_result query_asm_NDBG( std::vector<llin> movedParts , 
										std::vector<llin> referenceParts , 
										const Eigen::Vector3d& moveDir ); 
	// LAB FRAME
	Eigen::Vector3d     asm_center_of_freedom_direction( std::vector<size_t> movedParts , std::vector<size_t> referenceParts ); // NDBG order
	// LAB FRAME
	Eigen::Vector3d     asm_center_of_freedom_direction( std::vector<llin> movedParts   , std::vector<llin> referenceParts   ); // Part ID
	
	Eigen::Vector3d     removal_dir_moved_vs_balance( std::vector<size_t> movedParts ); // Suggest center of freedom of moved parts -vs- balance , NDBG Order
	Eigen::Vector3d     removal_dir_moved_vs_balance( size_t              movedPart  ); // Suggest center of freedom of moved parts -vs- balance , NDBG Order
	Eigen::Vector3d     removal_dir_moved_vs_balance( std::vector<llin>   movedParts ); // Suggest center of freedom of moved parts -vs- balance , Part ID
	Eigen::Vector3d     removal_dir_moved_vs_balance( llin                movedPart  ); // Suggest center of freedom of moved parts -vs- balance , Part ID
	
	// Return the number of constraints between 'movedID' and 'referenceID'
	size_t /* ------ */ how_many_constraints( llin movedID , llin referenceID ); 
	
	// Return the number of connections with other parts
	size_t /* ------ */ how_many_liaisons( llin partID ); 
	
	// Return the total shared area with neighbors
	double /* ------ */ how_much_shared_area( llin partID ); 
	
	//how much area to slide past?
	double /* ------ */ removal_interface_area( std::vector<llin> movedParts , std::vector<llin> referenceParts ); 
	
	// ~ Validation ~
	
	bool test_translation_sub_removal( std::vector<size_t> movedParts , std::vector<size_t> referenceParts , // Test removing 'movedParts' 
							   		   const Eigen::Vector3d& direction , double distance , usll samples );   // along 'direction' in incremental 
							   		   
	SuccessPoints record_test_trans_sub_removal( std::vector<size_t> movedParts , std::vector<size_t> referenceParts , // Test removing 'movedParts' 
							   		             const Eigen::Vector3d& direction , double distance , usll samples );   // along 'direction' in incremental 
							   		   
	// All part numbers NDBG order                                          steps , Each step checking for collision with 'referenceParts'
	bool test_translation_sub_removal( std::vector<llin> movedParts , std::vector<llin> referenceParts , // Test removing 'movedParts' 
							   		   const Eigen::Vector3d& direction , double distance , usll samples );   // along 'direction' in incremental 
	// Part ID                                                              steps , Each step checking for collision with 'referenceParts'
	
	SuccessPoints record_test_trans_sub_removal( std::vector<llin> movedParts , std::vector<llin> referenceParts , // Test removing 'movedParts' 
							   		             const Eigen::Vector3d& direction , double distance , usll samples );   // along 'direction' in incremental 
	
	bool test_sub_removal_against_floor( std::vector<size_t> movedParts , Floor_ASM& floor , // Test removing 'movedParts' along 'direction' in incremental 
							   		     Eigen::Vector3d direction , double distance , usll samples ); // steps , Each step checking for 
	// All part numbers NDBG order                                                                        collision with 'floor'
	bool test_sub_removal_against_floor( std::vector<llin> movedParts , Floor_ASM& floor , // Test removing 'movedParts' along 'direction' in incremental 
							   		     Eigen::Vector3d direction , double distance , usll samples ); // steps , Each step checking for 
	// Part ID                                                                                            collision with 'floor'
	SuccessCode validate_removal_staightline_stability( std::vector<llin> movedParts , std::vector<llin> referenceParts , // Validate for removal and stability 
														Eigen::Vector3d direction , size_t supportDex , double distance = 1.0d , usll samples = 200 );
	IndexSuccesLookup suggest_support_for_staightline_stability( std::vector<llin> movedParts , std::vector<llin> referenceParts , 
																 double distance = 1.0d , usll samples = 200 );
														
	void populate_liaison_graph_from_NDBG(); // Use the NDBG as the basis for the Liaison Graph, which essentially already contains it
	
	void populate_shared_area_graph_from_adjacencies( size_t lvl = 20 ); // Calculate the adjacent areas between all of the parts
	
	// LAB FRAME
	Eigen::MatrixXd asm_get_constraints_on_movd( const std::vector<size_t>& movedParts , const std::vector<size_t>& referenceParts ); // NDBG Order
	// LAB FRAME
	Eigen::MatrixXd asm_get_constraints_on_movd( const std::vector<llin>& movedParts   , const std::vector<llin>& referenceParts   ); // PartID
    // LAB FRAME						
	Eigen::MatrixXd asm_spherical_pyramid_corners_of_local_freedom( const std::vector<llin>& movedParts , const std::vector<llin>& referenceParts );
	
														
	// ~ Planning ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
	
	Assembly_ASM* /* ------ */ sub_from_spec( const std::vector<llin>& partsVec , // Generate a sub from a subset of parts , Set to support
											  size_t supportNum );
	Assembly_ASM* /* ------ */ sub_from_spec( const std::vector<llin>& partsVec , // Generate a sub from a subset of parts , Attempt support match
											  double CRIT_ANG = DEF_CRIT_ANG , double CRIT_DST = DEF_CRIT_DST ); 
	Assembly_ASM* /* ------ */ replicate(); // ------------------------------------- Return a pointer to an identical assembly , With an identical pose and support
	std::vector<TargetVFN_ASP> lab_collision_targets_from_parts(); // -------------- Return a mesh target for each of the posed parts in the assembly
	bool /* --------------- */ collides_with( Assembly_ASM& other ); // ------------ Return true if any parts in this assembly collide with any parts in 'other'
	bool /* --------------- */ collides_with( Floor_ASM& floor ); // --------------- Return true if any of the parts in this assembly collide with 'floor'
	bool /* --------------- */ collides_with( PartsCollection& cllctn ); // -------- Return true if any parts in this assembly collide with any parts in 'cllctn'
	bool /* --------------- */ collides_with( PartsCollection& cllctn , // --------- Return true if any of the specified parts collide with any parts in 'cllctn'
											  const std::vector<llin>& pPartsVec ); 
	
	// ~ Debug ~
	void assembly_report(); // Print some info about the assembly
	void part_lab_pose_report(); // Print the lab pose for every part
	BoolScore check_Z_floor_OK( double zLevel , double margin = EPSILON ); // Return true if all parts of the assembly are above 'zLevel'
	
	

protected:
	// ~ Bookkeeping ~
	llin /* --------------- */ ID; // Unique ID for each assembly
	std::list< PartEntry_ASM > partsList;
	size_t currentSupportIndex;
	// ~ Markers ~
	float markerOpacity = 1.0;
	// ~ Pose ~
	Eigen::Vector3d    position; // -- Center of the part in R3 , lab frame
	Eigen::Quaterniond orientation; // Orientation in the _____ , lab frame
	Pose_ASP /* --- */ pose; // ------ Pose in the ____________ , lab frame
	// ~ Support ~
	std::vector< Polygon_ASM > /* --- */ supports; // --- Vector of all of the support face objects
	Eigen::Vector3d /* -------------- */ COM; // -------- Center of mass _____________________________ , asm frame
	TriMeshVFN_ASP /* --------------- */ hullVFN; // ---- Convex hull ________________________________ , asm frame
	std::vector< std::vector< double > > spptEdgeDist; // Distance to each of the edges of the support , face frame
	std::vector< bool > /* ---------- */ stableFace; // - Flag for each support face , true == stable
	double /* ----------------------- */ volume; // ----- Volume of the part , in the same units as the source STL
	Eigen::MatrixXd /* -------------- */ AABBasm; // ---- Axis-Aligned Bounding Box for all of the contained parts
	// ~ NDBG ~
	std::vector< std::vector< LocalFreedom_ASM > > NDBG; // Non-Directional Blocking Graph , asm frame
	std::vector<llin> /* ---------------------- */ NDBG_PartID_Lookup; // NDBG_PartID_Lookup[i] = 'Part_ASM.ID' for corresponding NDBG index
	std::vector< std::vector< bool > > /* ----- */ liaisonGraph;
	std::vector< part_face_pair > /* ---------- */ adjacentFaces; // Part adjacency relationships
	std::vector< std::vector< double > > /* --- */ sharedAreaGraph; // Shared Area between each part
};

IndexSearchResult match_sub_support_to_original( Assembly_ASM* original , size_t origSupport , Assembly_ASM* subAsm , 
												 double angleCritRad = DEF_CRIT_ANG ); // Match sub support

// __ End Assembly_ASM __

/// ___ END GEO ____________________________________________________________________________________________________________________________


// ___ End Classes _________________________________________________________________________________________________________________________


// === Functions ===========================================================================================================================

// == Assembly Planning ==

FaceIndices faces_from_facets( Eigen::MatrixXd V , Eigen::MatrixXi F , Eigen::MatrixXd N , double CRITRN_ANG );

std::vector< part_face_pair > adjacent_sides_from_parts( std::vector< Part_ASM* > partPtrList , double CRIT_ANG , double CRIT_DST );


/// ==== NDBG Functions ====================================================================================================================

NDBGrelation NDBG_from_posed_parts( std::vector< Part_ASM* > partPtrList , size_t totalProblemParts ,
									double CRIT_ANG , double CRIT_DST );
																	  
interference_result query_NDBG( std::vector< std::vector< LocalFreedom_ASM > >& NDBG , 
								const std::vector<size_t>& movedParts , const std::vector<size_t>& referenceParts , const Eigen::Vector3d& moveDir );

Eigen::Vector3d center_of_freedom_direction( std::vector< std::vector< LocalFreedom_ASM > >& NDBG , 
								             const std::vector<size_t>& movedParts , const std::vector<size_t>& referenceParts ,
								             const Eigen::Vector3d& nudge = ASP_VECTORDEFAULT );
								             
Eigen::Vector3d center_of_freedom_direction( const Eigen::MatrixXd& constraints , const Eigen::Vector3d& nudge = ASP_VECTORDEFAULT );
								             
Eigen::MatrixXd get_constraints_on_movd( std::vector< std::vector< LocalFreedom_ASM > >& NDBG , 
										 const std::vector<size_t>& movedParts , const std::vector<size_t>& referenceParts );

bool test_direction_against_constraints( const Eigen::Vector3d& direction , const Eigen::MatrixXd& constraints );

Eigen::MatrixXd self_conforming_constraints( const Eigen::MatrixXd& constraints );

size_t count_self_conforming_constraints( const Eigen::MatrixXd& constraints );

Eigen::MatrixXd corners_from_constraints( const Eigen::MatrixXd& constraints );
										 
Eigen::MatrixXd spherical_pyramid_corners_of_local_freedom( std::vector< std::vector< LocalFreedom_ASM > >& NDBG , 
														    const std::vector<size_t>& movedParts , const std::vector<size_t>& referenceParts );
			
// ~~~ FREEDOM CLASS ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
														    
enum FREEDOM_CLASS{ // Possible classes of freedom state in increasing order
    ERR_FREEDOM    , // Return code for bad freedom calc , 0 lowest value
	LOCKED         , // Least Free , 1
	PRISM_UNI      , 
	PRISM_BIDIR    ,
	PLANAR_CONSTR  ,
	PLANAR_FREE    ,
	SPHERE_PYRAMID ,
	DIHEDRAL       ,
	HALF_SPACE     ,
	FREE_SPHERE // ---- Most Free , 9
};


//  B. Function that returns freedom class designation from constraints
FREEDOM_CLASS freedom_class_from_constraints( const Eigen::MatrixXd& constraints );
string /*- */ freedom_string( FREEDOM_CLASS freeState ); // String representation of the freedom class

Eigen::MatrixXd sample_within_constraints( const Eigen::MatrixXd& constraints ,
										   double dTheta = M_PI / 100.0 , size_t sphereDiv = ICOS_SPHERE_DIVISN );

// ... END FREEDOM ......................................................................................................................... 

/// ____ End NDBG __________________________________________________________________________________________________________________________


// = Liaison Graph Functions =

std::vector<size_t> liaison_neighbor_indices( std::vector<std::vector<bool>>& liaisonGraph , size_t query );

void add_undir_edge( std::vector< std::vector< bool > >& liaison , size_t index1 , size_t index2 ); // DEBUG , Add undirected edge to graph

bool is_subgraph_connected( std::vector< std::vector< bool > >& liaison , std::vector< size_t >& subGdices );

bool is_subgraph_connected( Assembly_ASM* assembly , std::vector<llin>& prtIDs );

std::vector<std::vector<size_t>> connected_subgraphs_in_subgraph( std::vector<std::vector<bool>>& liaison   , 
																  std::vector<size_t>&            subGdices );
																  
std::vector<std::vector<llin>> connected_subgraphs_in_subgraph( Assembly_ASM* assembly , std::vector<llin>& prtIDs );

// _ End Liaison Graph _


void print_part_face_pair_vec( std::vector< part_face_pair > pair_vec );

void print_constraint_state_for_part( std::vector< std::vector< LocalFreedom_ASM > >& NDBG , size_t partID );

bool test_composite_freedom( std::vector< LocalFreedom_ASM* > freedomList , Eigen::Vector3d direction );

visualization_msgs::Marker get_axes_marker( float axsLen , float thickness = 0.01f );

visualization_msgs::Marker get_straightPath_marker( Eigen::Vector3d bgn , Eigen::Vector3d end , 
												    float thickness , float R , float G , float B , float alpha );

// = Marker Diagnostics =
Pose_ASP        get_mrkr_pose( visualization_msgs::Marker& vizMarker );
Eigen::MatrixXd get_mrkr_vertices( visualization_msgs::Marker& vizMarker );
Eigen::MatrixXd get_mrkr_colors( visualization_msgs::Marker& vizMarker );
SuccessCode     check_marker_OK( visualization_msgs::Marker& vizMarker );
// _ End Diagnostics _

// __ End ASP Func __

// == Struct Helpers ==

void ASP_init_colors();

std::ostream& operator<<( std::ostream& os , const part_face& pf );

// __ End Helpers __

// ___ End Func ____________________________________________________________________________________________________________________________


#endif

/* === Spare Parts =========================================================================================================================


   ___ End Parts ___________________________________________________________________________________________________________________________

*/

