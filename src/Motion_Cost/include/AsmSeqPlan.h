#pragma once // This also helps things not to be loaded twice , but not always . See below

/***********  
AsmSeqPlan.h
James Watson , 2018 February
Structures and functions to geometric processing and assembly planning

Template Version: 2017-09-23
***********/

#ifndef ASMSEQ_H // This pattern is to prevent symbols to be loaded multiple times
#define ASMSEQ_H // from multiple imports


// ~~ Includes ~~
// ~ Eigen ~
#include <Eigen/Core> // ---- The living heart of Eigen
#include <Eigen/StdVector> // This is REQUIRED to put an Eigen matrix in an std::vector
#include <Eigen/Dense> // --- Cross Product , etc.
#include <Eigen/Geometry> //- Quaternion , etc
// ~ ROS ~
#include <ros/ros.h> // -------------- ROS , Publishers , Subscribers
#include <sensor_msgs/JointState.h> // JointState , For plan replayer
#include <geometry_msgs/Pose.h> // --- Geometry Pose , For plan replayer

// ~ Local ~
#include <Cpp_Helpers.h> // C++ Utilities and Shortcuts
#include <ROS_Helpers.h> // ROS Utilities and Shortcuts
#include <ASP_3D.h> // ---- Assembly Geometry
//~ #include <Motion_Planning.h> // Path planning & Collision Detection

// ~~ Defines ~~
#define DEFAULT_EDGE_COST 1
#define DEFAULT_NODE_COST 1

// ~~ Constants ~~
const double /* ------ */ MAX_BAXTER_GRIP_SEP =   0.10; // [m]
const std::vector<double> LBR4_MAX_ANG_VEL    = { 1.920 , 1.920 , 2.234 , 2.234 , 3.560 , 3.211 , 3.211 }; // [rad/s]
const double /* ------ */ OFFSETNONZERO       = 0.0005;
const double /* ------ */ GRIDDIVISOR_DEFAULT = 15.0;

/// === GRAPH PLANNING =====================================================================================================================


// ~~ Forward Declarations ~~
class Forest_ASP;

// === Problem Components ===

enum ACTIONS{ ACT_ROTATE ,  // Rotate the entire assembly and place it on a new suppiort
			  ACT_REMOVE ,  // Remove one or more parts from an assembly, replacing that assembly with two new assemblies
			  ACT_NO_OPR }; // Placeholder action


string interpret_ACTIONS( const ACTIONS& action );

// == struct AsmStateRep ==

struct AsmStateRep{ // A structure to identify and compare the assembly state
				    // The assembly state is represented by the part membership and the supporting surface
	// ~ State Representation ~
	std::vector<llin> partsVec; //-- All the parts that are present in this assembly // 2018-02-28: Vector to account for repeat fasteners
	size_t /* ---- */ supportNum; // Support that the assembly is resting on
	// ~ Bookkeeping ~
	size_t numSupports; // The total number of possible supports 
};

bool operator==( const AsmStateRep& state1 , const AsmStateRep& state2 ); // Return true if the two states are equal , Otherwise return false

AsmStateRep copy_AsmStateRep( AsmStateRep& original );

std::vector<AsmStateRep> copy_state_vec( std::vector<AsmStateRep>& original );

void state_report( AsmStateRep& state );

// __ End AsmStateRep __


// == struct ActionRep ==

struct ActionRep{ // Structure that fully specifies the action that was taken
	ACTIONS /* --- */ action; //- Code for the action that was taken
	std::vector<llin> removed; // ACT_REMOVE : Vector of part IDs that were removed
	size_t /* ---- */ bgnSup; //- ACT_ROTATE : Support before the rotate operation
	size_t /* ---- */ endSup; //- ACT_ROTATE : Support after  the rotate operation
};

ActionRep rotate_action_spec( size_t bgn , size_t end ); // Return specification for a rotate action
ActionRep remove_action_spec( std::vector<llin> removeIDs ); // Return specification for a remove action
ActionRep remove_one_spec( llin removeID ); // Return specification for a single-part remove action
ActionRep no_op_asm(); // Return a no-op action

// __ End ActionRep __

// ___ End Components ___


// == struct AsmSearchResult ==

struct AsmSearchResult{
	EIGEN_MAKE_ALIGNED_OPERATOR_NEW
	std::vector<ActionRep>     plan; // ------ Sequence of actions
	std::vector<AsmStateRep>   path; // ------ Sequence of states
	std::vector<AsmStateRep>   visited; // --- Will be treating this like a set
	std::vector<Assembly_ASM*> geoSequence; // Sequence of assemly objects to match 'path'
	bool /* --------------- */ goalMet; // --- Did the planner reach the goal?
}; 

// __ End AsmSearchResult __


// == struct BasePartFitness ==

struct BasePartFitness{
	double w_alpha; // ---- Weight alpha
	double w_beta; // ----- Weight beta
	double w_gamma; // ---- Weight gamma
	double Si_bndrySurf; // Shared surface area with surrounding parts
	double St_totalSurf; // Total surface area of the assembly
	double Nr_Adjacency; // Number of connections between this part and other parts
	double Vi_partVolum; // Part volume
	double Vt_totVolume; // Total assembly volume
	// -----------------
	double baseFitness; //- Fitness for this part to be a Base Part
};

// __ End BasePartFitness __


// == struct AsmStateSuccessCode ==

class AsmStateNode_ASP;

struct AsmStateSuccessCode{ // An assembly state node and the data regarding the success of its construction
	EIGEN_MAKE_ALIGNED_OPERATOR_NEW
	AsmStateNode_ASP* stateNode; // ---- Resulting state after planning and verifying an action
	SuccessCode /*-*/ creationDetail; // Succeed or fail? Why?
};

void clear_ASM_node( AsmStateSuccessCode& node );

// __ End AsmStateSuccessCode __


/* class Graph_ASP // 2018-02-28: Delaying creating an explicit class for this until it is absolutely necessary. 
								  Write '~AsmStateNode_ASP' so that it erases the entire graph when called on the root node */


// == class AsmStateNode_ASP == 

enum ValidationCode{
	// ~ FAIL ~
	ERROR_INCOMPLETE , 
	NEW_SUB_UNSTABLE ,
	NEW_SUB_DISJOINT ,
	FLOATING_HAM_ERR , 
	REMDIR_NOT_FOUND ,
	GOALPOSE_NOT_FND , 
	RRTSTAR_NO_SOLTN ,
	ALL_GRASP_COLLID ,
	GRASP_IK_FAILURE ,
	MOVER_SUB_LOCKED ,
	VALIDATION_FAULT , 
	BAD_RESTING_POSE ,
	REFERNC_DISJOINT ,
	IK_PATH_INCOMPLT ,
	// ~ PASS ~
	PASS_STRAIGHTLIN ,
	PASS_RRTSTAR_PTH ,
	ROTATED_OK_MAGIC 
};

string interpret_ValidationCode( size_t index );

struct EdgeActionData{ // Container for data needed to plan one edge of a tree plan
	EIGEN_MAKE_ALIGNED_OPERATOR_NEW
	size_t /* - */ precedence   = 0 , // ------------- Order in which edge should be executed
				   failCount    = 0 , // ------------- Number of times that we have failed to execute this node
				   parentSupprt = 0 ; // ------------- Parent asm support index that will best allow the operation
	ValidationCode code /* - */ = ERROR_INCOMPLETE; // Code assigned to the action for success or failure
	bool /* --- */ rotateFlag   = false; // ---------- Flag was requeued to avoid a rotation action
};

class AsmStateNode_ASP{
	// Class to represent an assembly state in a graph search
	friend class Forest_ASP;
public:
	EIGEN_MAKE_ALIGNED_OPERATOR_NEW
	
	// ~ Con/Destructors ~
	AsmStateNode_ASP(); // --------------------------------- Default Constructor , Create a blank state node
	AsmStateNode_ASP( Assembly_ASM* pAsm ); // ------------- Populated with an assembly
	AsmStateNode_ASP( const AsmStateNode_ASP& original ); // Copy constructor
	~AsmStateNode_ASP(); // -------------------------------- Erase all of the connected nodes and the attached assemblies
	
	// ~ Getters ~
	bool /* ------- */ has_edge( AsmStateNode_ASP* queryPtr ); // Return true if 'queryPtr' is one of the outgoing edges, otherwise return false
	bool /* ------- */ has_part_ID( llin prtID ); // ------------ Return true if the part is present in the sub, otherwise return false
	bool /* ------- */ has_part_ID( std::vector<llin> prtID ); // Return true if all parts are present in the sub, otherwise return false
	size_t /* ----- */ count_edges(); // ------------------------ Return the number edges outgoing from this node
	AsmStateNode_ASP*& operator[]( size_t index ); // ----------- Return a reference to the pointer at the specified 'index'
	
	// ~ Graph Search ~
	void /* ------ */ add_edge( AsmStateRep nextState , // ----------- Create a new node according w/ 'nextState' and connect via outgoing edge
								ActionRep action , double edgeCost = 0.0 , 
								bool recalcGeo = true ); 
	AsmStateNode_ASP* add_edge( std::vector< llin >& partsVec , // --- Create a new node with specified parts and connect via outgoing edge
								bool recalcGeo = true ); 
	AsmStateNode_ASP* add_edge_remove( Assembly_ASM* contents  , // -- Create a new node with specified assembly and connect via outgoing edge
									   Assembly_ASM* subRemove );
	AsmStateNode_ASP* add_edge_remove( AsmStateNode_ASP* nuState ); // Create a new edge pointing to the specified state
	AsmStateNode_ASP* add_edge_rotate( size_t nuSupport ); // -------- Create a new node with same assembly and new support and connect via outgoing edge
	AsmStateNode_ASP* add_edge_rotate( size_t nuSupport , Assembly_ASM* posedAsm );
	AsmStateNode_ASP* add_edge_rotate( AsmStateNode_ASP* nuState ); // Connect a action pointing to the new state
	
	void /* ------ */ expand_single(); // ------------------------ Add edges for all rotations and single-part removals
	
	AsmStateNode_ASP* get_next(); // ----------------------------- Return the next node in sequence , or return 'nullptr' if there is no next
	
	// ~ Setters ~
	void set_state_rep_from_Assembly_ASM( Assembly_ASM* pAsm ); // Read the assembly state from assembly object
	void set_support( size_t supNum ); // ------------------------ Set the support in both the assembly and the state representation
	
	// ~ Node Contents ~
	AsmStateNode_ASP* /* ------ */ parent; // ------ Parent node
	AsmStateRep /* ------------ */ stateRep; // ---- Current state of the asm
	Assembly_ASM* /* ---------- */ assembly; // ---- Geo info of the asm (or action result)
	Assembly_ASM* /* ---------- */ removed; // ----- Geo info of the sub removed by the incoming edge (if applicable)
	std::vector<AsmStateNode_ASP*> outgoingEdges; // Pointers other nodes , each pointer is an outgoing edge
	std::vector<ActionRep> /* - */ edgeActions; // - Actions corresponding to each outgoing edge
	std::vector<double> /* ---- */ edgeCosts; // --- Costs corresponding to each outgoing edge
	Forest_ASP* /* ------------ */ forest; // ------ Forest that holds each tree
	double /* ----------------- */ cost; // -------- Cost of this node
	std::vector<AsmStateRep> /*-*/ path; // -------- Attach a sequence to each node to record the path to it
	std::vector<Assembly_ASM*>     history; // ----- Geometry representing the series of states
	std::vector< llin > /* ---- */ memParts; // ---- Vector of member parts, Shortcut for 'stateRep' if a full graph search isn't being done
	std::vector<Pose_ASP> /* -- */ seqPoses; // ---- Motion plan that produces the state represented by this node , parent --to-> this
	Pose_ASP /* --------------- */ staticPose; // -- Pose of the assembly while the sub is being removed 
	EdgeActionData /* --------- */ actionData; // -- Data about how this node will be executed
	size_t /* ----------------- */ depth; // ------- Depth of the node in the tree
	
protected:
	// 2018-02-28: Keeping everything public for now for the sake of brevity
};

// __ End AsmStateNode_ASP __


// == class Forest_ASP ==

class Forest_ASP{
	// Maintains a collection of assembly planning trees
	friend class AsmStateNode_ASP;
public:
	EIGEN_MAKE_ALIGNED_OPERATOR_NEW
	// ~ Con/Destructors ~
	// ~ Forest Contents ~
	std::vector<AsmStateNode_ASP*> roots; // Pointers to the root of each tree

protected:

};

// __ End Forest_ASP __


// == Cost Functions ==

double rotation_cost( Assembly_ASM* asm1 , size_t bgnSupport , size_t endSupport ); 
double removal_cost( Assembly_ASM* asm1 , std::vector<llin>& movedParts );
double edge_cost( AsmStateNode_ASP* state , ActionRep& action );
double heuristic( AsmStateNode_ASP* state );

// __ End Cost __


// == Graph Search ==

bool simple_goal( AsmStateRep& evalState ); // Evaluate whether a goal state has been reached, only one part remaining

bool validate_action_straight_simple( AsmStateNode_ASP* state , ActionRep& action ); // Validate a straight-line action

AsmSearchResult a_star_single( AsmStateNode_ASP* init_state , 
							   bool (*is_goal)(AsmStateRep&) , bool (*validator)(AsmStateNode_ASP*,ActionRep&) ); 

// __ End Search __

/// ___ END GRAPH __________________________________________________________________________________________________________________________


/// === PLAN PLAYBACK ======================================================================================================================

// == class PlayerFrame ==

class PlayerFrame{ // Holds the information to reconstruct one frame of a simulation
public:
	EIGEN_MAKE_ALIGNED_OPERATOR_NEW // REQUIRED MACRO FOR EIGEN
	
	// ~ Methods ~
	std::vector<double> get_q();
	
	// ~ Members ~
	
	// NOTE: 'RViz_MarkerManager.rebuild_mrkr_array' should not be used!
	RViz_MarkerManager      mrkrMngr; //-- Already have functions for interfacing with this, so may as well
	sensor_msgs::JointState jnt_state_; // For setting the robot state
};

// __ End PlayerFrame __


// == class PlanRecording ==

class PlanRecording{
public:
	EIGEN_MAKE_ALIGNED_OPERATOR_NEW // REQUIRED MACRO FOR EIGEN
	
	// ~ Con/Destructors ~
	~PlanRecording();
	
	// ~ Playback ~
	PlayerFrame* get_current_frame(); // --------------- Get the frame indicated by 'currDex'
	PlayerFrame* get_next_frame( bool loop = true ); //- Get the next frame in sequence, Optionally loop to the front
	PlayerFrame* get_frame( size_t framNum ); // ------- Get the specified frame at index framNum
	PlayerFrame* operator[]( size_t framNum ); // ------ Get the specified frame at index framNum
	void /* - */ erase_last_frame(); // ---------------- Create a frame that erases the markers of the last element of frames
	// ~ Control ~
	void rewind(); // ------------------ Set frame to 0
	bool set_frame( size_t framNum ); // Set frame to specified index
	// ~ Editing ~
	void push_back( PlayerFrame* nuFrame ); // Add an animation frame to the vector
	void clear(); // ------------------------ Erase all frames
	
protected:
 
	std::vector<PlayerFrame*> frames;
	size_t currDex = 0;
};

// __ End PlanRecording __


// == class PlanPlayer ==

class PlanPlayer{
	
public:
	EIGEN_MAKE_ALIGNED_OPERATOR_NEW

	// ~~ Functions ~~

	// ~ Con/Destructor ~
	PlanPlayer( double frameRateP , PlanRecording* recordingP );
	~PlanPlayer();
	
	// ~ ROS Node ~
	PlayerFrame* current_frame( bool loop = true ); // Return a pointer to the frame that is relevant to this time

	// ~~ Members ~~

	// ~ Animation Vars ~
	double frameRate; // ---- Frequency [Hz] of frame change
	
	// ~ Playback Data ~
	PlanRecording* recording; // ------ Marker and JointState data needed to reconstruct plan
	
	// ~ Bookkeeping ~
	StopWatch frameTimer;
};

// __ End PlanPlayer __

/// ___ END PLAYBACK _______________________________________________________________________________________________________________________


/// === PLAN VALIDATION & EVALUATION =======================================================================================================

struct ParallelGraspState{ // Contains gripper pose and finger opening config
	EIGEN_MAKE_ALIGNED_OPERATOR_NEW
	Pose_ASP /* ---- */ graspTarget; // Pose for planning gripper IK
	std::vector<double> q_fingers; // - Joint positions to send to the fingers { right , left }
	bool /* -------- */ feasible; // -- Flag for whether the manipulator / gripper can support this state
	double /* ------ */ score; // ----- Assign the grasp some score
	double /* ------ */ quality; // --- Assign the grasp some quality
	Eigen::Vector3d     A; // --------- Position of finger 1 in lab frame // Optional 
	Eigen::Vector3d     B; // --------- Position of finger 2 in lab frame // Optional 
};

std::vector<Pose_ASP> simulate_disasm_step_straight( Assembly_ASM* before_ASM , Assembly_ASM* after_ASM , 
													 Pose_ASP befPose , size_t befSupport , 
													 double removeDist , size_t numSteps ,
													 Eigen::Vector3d floorCenter , double floorXWidth , double floorYLngth );

std::vector<Pose_ASP> simulate_disasm_step_straight( AsmStateNode_ASP* before , AsmStateNode_ASP* after , 
													 Pose_ASP befPose , size_t befSupport , 
													 double removeDist , size_t numSteps ,
													 Eigen::Vector3d floorCenter , double floorXWidth , double floorYLngth );

// Return the gripper config to grasp
ParallelGraspState gripper_pose_from_grasp_pair( Eigen::MatrixXd pair , Eigen::Vector3d upZdir , double gripSepLim ); 

std::vector<llin> balance_ID_A_minus_B( Assembly_ASM* A , Assembly_ASM* B );

Assembly_ASM* Assembly_A_minus_B( Assembly_ASM* A , Assembly_ASM* B );

std::vector<std::vector<llin>> successor_membership( AsmStateNode_ASP* rootNode );

AsmStateNode_ASP* precedence_layer_root_to_action_sequence( AsmStateNode_ASP* rootNode , Eigen::Vector3d setdownCenter , 
														    double removeDist , size_t numSteps ,
														    double floorXWidth , double floorYLngth );
														    
AsmStateNode_ASP* precedence_layer_root_to_action_sequence_ALTERNATE( AsmStateNode_ASP* rootNode , Eigen::Vector3d setdownCenter , 
																	  double removeDist , size_t numSteps ,
																	  double floorXWidth , double floorYLngth );
																	  
AsmStateSuccessCode plan_straightline_REMOVE_action( Assembly_ASM* totalBeforeAction , Assembly_ASM* removedSub , 
												     Eigen::Vector3d setdownCenter , size_t initSupport ,
												     double removeDist , size_t numSteps ,
												     double floorXWidth , double floorYLngth );
												     
AsmStateSuccessCode plan_magic_ROTATE_action( Assembly_ASM* asmToRotate , 
											  Eigen::Vector3d setdownCenter , 
											  size_t initSupport , size_t finlSupport , 
											  double floorXWidth , double floorYLngth );
											  
AsmStateSuccessCode precedence_layer_ASP_simple_straightline( AsmStateNode_ASP* rootNode , Eigen::Vector3d setdownCenter , 
															  double removeDist , size_t numSteps ,
															  double floorXWidth , double floorYLngth );

void inspect_level_ASP( AsmStateNode_ASP* rootNode );

double manip_score_Yoshikawa85( Eigen::MatrixXd J );

std::vector<double> extract_arm_state_from_msg( const sensor_msgs::JointState& stateMsg );

double arm_traj_duration( const std::vector<sensor_msgs::JointState>& armTraj , const std::vector<double>& jointSpeedMax , double speedFactor );

/// ___ END VALIDATE _______________________________________________________________________________________________________________________


/// === HELPER FUNCTIONS ===================================================================================================================

Eigen::MatrixXd unblocked_constraints( const Eigen::MatrixXd& allConstraints );

/// ___ END HELPER _________________________________________________________________________________________________________________________


#endif

/* === Spare Parts =========================================================================================================================



   ___ End Parts ___________________________________________________________________________________________________________________________

*/

