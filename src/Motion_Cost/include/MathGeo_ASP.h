#pragma once // This also helps things not to be loaded twice , but not always . See below

/***********  
MathGeo_ASP.h
James Watson , 2018 January
Basic Math and 2D / 3D Geometry Utilities

Template Version: 2017-09-23
***********/

#ifndef MATHGEO_H // This pattern is to prevent symbols to be loaded multiple times
#define MATHGEO_H // from multiple imports

// ~~ Includes ~~
// ~ Eigen ~
#include <Eigen/Core> // ---- The living heart of Eigen
#include <Eigen/StdVector> // This is REQUIRED to put an Eigen matrix in an std::vector
#include <Eigen/Dense> // --- Cross Product , etc.
#include <Eigen/Geometry> //- Quaternion , etc
// ~ quickhull ~
#include <QuickHull.hpp> // Convex hull
// ~ LIBIGL ~
#include <igl/read_triangle_mesh.h> // ------ URL , Load STL : https://github.com/libigl/libigl/issues/273
#include <igl/remove_duplicate_vertices.h> // More compact representation of the model
#include <igl/per_face_normals.h> // -------- Needed for agglomerating facets into faces
#include <igl/per_vertex_normals.h> // ------ Needed for shrinking meshes
// ~ Local ~
#include <Cpp_Helpers.h> // Utilities and Shortcuts
#include <RAPID.H> // ----- Triangle-Triangle Collision detection for arbitrary meshes

// ~ Standard ~
//~ #include <iostream> // - standard input and output , istream
//~ #include <sstream> // -- Text streams 
//~ using std::ostream;

// ~~ Shortcuts and Aliases ~~
#define _USE_MATH_DEFINES    // Get the <cmath> constants
#define DEFAULTNUMPOINTS 200 // Default number of points to allocate for a polygon
// ~ Constants ~
const size_t /* ------ */ ICOS_SPHERE_DIVISN  =   7; // ---- Divide each icosahedron triangle into 28 triangles
const size_t /* ------ */ CIRCLE_DIVISION     = 180; // ---- Divide each circle into 300 Segments
const double /* ------ */ GEO_CRIT_ANG /*- */ =   0.0349; // Angle Criterion , 2deg

// ~ Type Aliases ~ // Use this for long type names and names that are unlikley to be shadowed
//~ typedef Eigen::Matrix< double , 7 , 1 > Vector7d;


// === Fundamental Structs ===

struct Pose_ASP{
	EIGEN_MAKE_ALIGNED_OPERATOR_NEW
	Eigen::Vector3d /* --------- */ position; // -- Position in R3
	Eigen::Quaterniond /* ------ */ orientation; // Quaternionion , double
	Eigen::Matrix< double , 4 , 4 > homogMatx; // - Equivalent Homogeneous transformation matrix for the position / orientation
};

enum MESHTYPE{ GENERIC   , //- Default mesh type
			   SPHERICAL , //- Facets surrounding a center of concern
			   REVOLUTE  , //- Facets surrounding an axis of concern
			   SHADOW    }; // Profile along a direction of concern, a distance from a point of concern

struct TriMeshVFN_ASP{
	EIGEN_MAKE_ALIGNED_OPERATOR_NEW
	Eigen::MatrixXd V; // ---- N x 3 matrix in which each row is a unique point in the mesh
	Eigen::MatrixXi F; // ---- M x 3 matrix in which each row is a list of indices of 'V' that comprise the facet
	Eigen::MatrixXd N; // ---- List of normal vectors corresponding to F
	Eigen::Vector3d center; // Center of the mesh, used for some expansion operations
	Eigen::Vector3d axis; // - Main axis, used for some expansion operations
	MESHTYPE /*- */ type;
};

// ___ End Fundamental ___


// === Geometry Classes ===

class RAPID_Geo{
public: // This is just a container, so everything is public
	
	// ~ Con/Destructors ~
	RAPID_Geo();
	~RAPID_Geo();
	
	// ~ Methods ~
	void /* -- */ load_RAPID_model( const Eigen::MatrixXd& V , const Eigen::MatrixXi& F );
	void /* -- */ load_RAPID_model( const TriMeshVFN_ASP& pMesh );
	void /* -- */ load_RAPID_transform( Pose_ASP partPose );
	bool /* -- */ collides_with( RAPID_Geo& other );
	std::set<int> collision_F( RAPID_Geo& other );
	
	// ~ Members ~
	RAPID_model *Rmodel;
	int /* -- */ numTris;
	double /*-*/ T[3];
	double /*-*/ R[3][3];
};

class CollisionVFN_ASP{
public:

	// ~ Con/Destructors ~
	CollisionVFN_ASP( const TriMeshVFN_ASP& pMesh );
	//~ ~CollisionVFN_ASP();

	// ~ Methods ~
	void init_for_collision( const TriMeshVFN_ASP& pMesh );
	
	// ~ Members ~
	TriMeshVFN_ASP  mesh;
	Eigen::MatrixXd aabb;
	RAPID_Geo /*-*/ collsn_geo;
};

// ___ End Classes ___


// === Geo Structs ===

struct Tetrahedron_ASP{
	EIGEN_MAKE_ALIGNED_OPERATOR_NEW
	Eigen::Vector3d v0; 
	Eigen::Vector3d v1;
	Eigen::Vector3d v2;
	Eigen::Vector3d v3;
};

struct VolumeCentroid{
	EIGEN_MAKE_ALIGNED_OPERATOR_NEW
	double          volume; // Note that the volume is allowed to be negative!
	Eigen::Vector3d centroid; 
};

struct TargetVFN_ASP{ // TriMesh and its bounding box , AABB is to make collision checking faster
	EIGEN_MAKE_ALIGNED_OPERATOR_NEW
	TriMeshVFN_ASP  mesh;
	Eigen::MatrixXd aabb;
};

struct Segment2D_ASP{
	EIGEN_MAKE_ALIGNED_OPERATOR_NEW
	Eigen::Vector2d pnt1; 
	Eigen::Vector2d pnt2; 
};

struct PointsClassfd{
	EIGEN_MAKE_ALIGNED_OPERATOR_NEW
	Eigen::MatrixXd points; // ---- Row-list matrix of points
	Eigen::ArrayXXi assignments; // Assigned classes
};

struct NumsClassfd{
	std::vector<double> points; // ---- Vector of points on the number line
	std::vector<size_t> assignments; // Assigned classes
};

struct RayHits{
	EIGEN_MAKE_ALIGNED_OPERATOR_NEW
	bool /* ---- */ anyHits; //- Were there any intersections recorded?
	Eigen::MatrixXd enter; // -- Row-list of entry points
	Eigen::MatrixXd exit; // --- Row-list of exit points
	Eigen::MatrixXd n_Metric; // Generic entry metrics to be populated by client code (e.g. grasp pair angles)
	Eigen::MatrixXd x_Metric; // Generic exit  metrics to be populated by client code (e.g. grasp pair angles)
};

struct SuccessPoints{
	EIGEN_MAKE_ALIGNED_OPERATOR_NEW
	bool /* ---- */ success; // -- Was the goal reached?
	Eigen::MatrixXd goodSeqPos; // Positions that meet the requirements
	double /* -- */ travelDist; // Furthest distance along a path we have traveled
	Eigen::Vector3d bestDir; // -- Direction of success , I am headed there
};

// == Struct Helpers ==

Pose_ASP pose_from_position_orient( const Eigen::Vector3d& position , const Eigen::Quaterniond& orientation );

Pose_ASP pose_from_pXYZ_oWXYZ( double pX , double pY , double pZ , double oW , double oX , double oY , double oZ );

Pose_ASP pose_from_origin_bases( const Eigen::Vector3d& origin ,
								 const Eigen::Vector3d& xBasis , const Eigen::Vector3d& yBasis , const Eigen::Vector3d& zBasis );

// Return the relative orientation Quaternion that will turn 'rght_base' onto 'left_target'
Eigen::Quaterniond operator/( const Eigen::Quaterniond& left_target , const Eigen::Quaterniond& rght_base );

double mag_rad( Eigen::Quaterniond turn );

// Return the lab frame pose of 'rght_op' as though it were a expressed in the frame 'left_op'
Pose_ASP operator*( const Pose_ASP& left_op     , const Pose_ASP& rght_op   ); 
// Return the Pose that will move 'rght_base' onto 'left_target'
Pose_ASP operator/( const Pose_ASP& left_target , const Pose_ASP& rght_base ); 

Pose_ASP origin_pose();

Pose_ASP copy_pose( const Pose_ASP& original );

bool check_vec3_OK( Eigen::Vector3d vec );
bool check_quat_OK( Eigen::Quaterniond quat );
bool check_dbbl_OK( double number );
bool check_matx_OK( Eigen::MatrixXd matx );

bool check_pose_OK( Pose_ASP pose ); // Return true if there are no { NaN , Infinity } in the pose

Eigen::MatrixXd closest_pair_in_hits_to( const RayHits& graspPairs , const Eigen::Vector3d& point ); // Find the closest grasp pair to the given point

double least_dist_from_pnt_to_row( const Eigen::Vector3d& query , const Eigen::MatrixXd& pnts ); 

double greatest_dist_from_pnt_to_row( const Eigen::Vector3d& query , const Eigen::MatrixXd& pnts ); 

double least_dist_from_pnt_to_hit( const Eigen::Vector3d& query , const RayHits& hits ); 

double greatest_dist_from_pnt_to_hit( const Eigen::Vector3d& query , const RayHits& hits ); 

RayHits concat_hits( const RayHits& op1 , const RayHits& op2 ); // Collect hits info from two response objects and collect them into one

std::vector<Pose_ASP> translate_pose_by_sequence( const Pose_ASP& original , const Eigen::MatrixXd& seqPos );

SuccessPoints empty_SuccessPoints();

// __ End Helpers __

// ___ End Structs ___





// === General Mathematics ===

size_t triangular_num( size_t N ); // Return the Nth triangular number

double selected_average_vec( std::vector<double>& vec , std::vector<size_t>& selection ); // Return the average of the selected points , R1

double vec_L2_norm( std::vector<double>& vec ); // Return the Euclidean Norm of a standard vector as though it were an n-dim vector
double vec_L2_diff( std::vector<double>& v1 , std::vector<double>& v2 ); // Return the Euclidean Norm of the difference between standard vectors

std::vector<double> perturb_vec( std::vector<double>& vec , double oneSideRange ); // Perturb each element of 'vec' by up to +/- 'oneSideRange'

// ___ End General ___


// === Geometry Functions ===

// == Printing Helpers ==

void print_vec3d_inline( Eigen::Vector3d vec );
void print_vec2d_inline( Eigen::Vector2d vec );

string Eig_vec3d_to_str( Eigen::Vector3d& vec );
string Eig_Quatd_to_str( Eigen::Quaterniond& quat );

std::ostream& operator<<( std::ostream& os , const Eigen::Vector3d& vec );
std::ostream& operator<<( std::ostream& os , const Eigen::Vector2d& vec );
std::ostream& operator<<( std::ostream& os , const Eigen::Quaterniond& quat );

std::ostream& operator<<( std::ostream& os , const Segment2D_ASP& segment );

std::ostream& operator<<( std::ostream& os , const Pose_ASP& pose );

string matx_1x3_to_string( double rowVec[3]  );
string matx_3x3_to_string( double matx[3][3] );

// __ End Printing __


// == Memory Helpers ==

Eigen::Vector3d Vec3D_from_aligned_stdvec( std::vector< Eigen::Vector3d , Eigen::aligned_allocator< Eigen::Vector3d > > vectorVector , 
										   size_t index );

Eigen::MatrixXd vstack( const Eigen::MatrixXd& A , const Eigen::MatrixXd& B ); // Stack two matrices vertically
Eigen::MatrixXi vstack( const Eigen::MatrixXi& A , const Eigen::MatrixXi& B ); // Stack two matrices vertically

Eigen::MatrixXd copy_V_plus_row( const Eigen::MatrixXd& pMatx , const Eigen::Vector3d& nuVec ); // Extend vertices list by 1 R3 vector , return copy
Eigen::MatrixXd copy_V_plus_row( const Eigen::MatrixXd& pMatx , const Eigen::Vector2d& nuVec ); // Extend vertices list by 1 R2 vector , return copy
Eigen::MatrixXi copy_F_plus_row( const Eigen::MatrixXi& pMatx , const Eigen::Vector3i& nuVec ); // Extend vertices list by 1 I3 vector , return copy

Eigen::MatrixXd copy_V_minus_row( const Eigen::MatrixXd& pMatx , size_t rowNum );

Eigen::MatrixXd copy_column_plus_dbbl( const Eigen::MatrixXd& columnMatx , double nuNum ); // Extend column by 1 number , return copy

Segment2D_ASP copy_Segment2D_ASP( Segment2D_ASP original );

void copy_Segment2D_vec_from_A_to_B(       std::vector<Segment2D_ASP>& A , std::vector<Segment2D_ASP>& B );
void copy_Segment2D_vec_from_A_to_B( const std::vector<Segment2D_ASP>& A , std::vector<Segment2D_ASP>& B );

TriMeshVFN_ASP copy_TriMeshVFN_ASP(       TriMeshVFN_ASP& original );
TriMeshVFN_ASP copy_TriMeshVFN_ASP( const TriMeshVFN_ASP& original );

Eigen::MatrixXd load_vec_vec_into_matx( std::vector<std::vector<double>>& vecVec );

Eigen::MatrixXd repeat_vector3d( Eigen::Vector3d origVec , size_t N );

std::vector<double> vec_nan( size_t N ); // Return a double vector of NaN with specified length

// __ End Memory __


// == Geo 2D ==

double d_point_to_segment_2D( Eigen::Vector2d  point , Segment2D_ASP  segment );

bool vec2d_eq( Eigen::Vector2d& v1 , Eigen::Vector2d& v2 ); // Test if Eigen 2D vectors are "about equal"

double winding_num( Eigen::Vector2d& point , Eigen::MatrixXd& polygon ); // Return the number of times polygon winds around point

bool point_in_poly_w( Eigen::Vector2d& point , Eigen::MatrixXd& polygon , bool makeCycle = false ); // Return true for a nonzero winding number

bool point_in_triangle_2D( Eigen::Vector2d pt , Eigen::Vector2d v0 , Eigen::Vector2d v1 , Eigen::Vector2d v2 ); // Return true if pnt in tri

size_t index_of_segment_with_pnt1( std::vector<Segment2D_ASP> segmentList , Eigen::Vector2d& queryPnt1 ); // Find the point in seg vector

std::vector<Segment2D_ASP> segment_list_to_segment_vector( std::list<Segment2D_ASP> segLst );

std::list<Segment2D_ASP> order_RH_border_segments( std::vector<Segment2D_ASP> segmentList ); // Attempt to order segments end-to-end

Eigen::MatrixXd Matx2D_points_to_Matx3D_flat( Eigen::MatrixXd& pnts2D ); // Place the R2 pnts vec in R3 with z_i = 0

bool intersect_seg_2D( Segment2D_ASP seg1 , Segment2D_ASP seg2 , bool includeEndpoints = true ); // Return true if the segments intersect

std::vector<Segment2D_ASP> segments_from_poly2D_matx( Eigen::MatrixXd& polyPts2D , bool closed = true ); // Convert point matx to segment vec

bool polygon_collide_2D( Eigen::MatrixXd& polyPtsA , Eigen::MatrixXd& polyPtsB , 
						 bool endpoints = false , bool makeCycle = false ); // Return true if one polygon is overlapped or contained

string verts2D_to_string( Eigen::MatrixXd& V );

Eigen::MatrixXd tile_triangle_2D( Eigen::Vector2d v0 , Eigen::Vector2d v1 , Eigen::Vector2d v2 , size_t N );
Eigen::MatrixXd tile_triangle_2D( Eigen::MatrixXd CCWverts , size_t N );

double tri_area_2D( Eigen::Vector2d p0 , Eigen::Vector2d p1 , Eigen::Vector2d p2 ); // Return area of CCW triangle in R2
double tri_area_2D( Eigen::MatrixXd CCWverts ); // Return area of CCW triangle in R2

double overlapping_tri_area( Eigen::MatrixXd tri1 , Eigen::MatrixXd tri2 , size_t discretLvl = 20 ); // Estimate area shared by two triangles

// __ End 2D __


// == Geo 3D ==

Eigen::Vector3d vec3d_random();
Eigen::Vector3d vec3d_rand_corners( const Eigen::Vector3d& corner1 , const Eigen::Vector3d& corner2 );

double angle_between( const Eigen::Vector3d& vec1     , const Eigen::Vector3d& vec2     );
double angle_between( const Eigen::Quaterniond& quat1 , const Eigen::Quaterniond& quat2 );
double angle_between( const Pose_ASP& pose1           , const Pose_ASP& pose2           );

double vec3_project_mag( const Eigen::Vector3d& vector , const Eigen::Vector3d& basis );

Eigen::Vector3d pnt_proj_onto_ray( const Eigen::Vector3d& point , const Eigen::Vector3d& rayOrg , const Eigen::Vector3d& rayDir );

double dist_to_plane( Eigen::Vector3d planeNorm , Eigen::Vector3d planePoint , Eigen::Vector3d queryPoint );

Eigen::Vector3d vec_proj_to_plane( Eigen::Vector3d vec , Eigen::Vector3d planeNorm );

Eigen::Vector3d pnt_proj_to_plane( Eigen::Vector3d queryPnt , Eigen::Vector3d planePnt , Eigen::Vector3d normal );

Eigen::Vector2d pnt_proj_onto_plane_2D( Eigen::Vector3d queryPnt , 
										Eigen::Vector3d planePnt , Eigen::Vector3d normal , Eigen::Vector3d xBasis );

Eigen::Vector3d get_average_V( const Eigen::MatrixXd& V ); // Return the average of a matrix of R3 points , each row is one point

Eigen::Vector3d selected_average_V( Eigen::MatrixXd& V , std::vector<size_t>& selection ); // Return the average of the selected points , R3

void shift_V_inplace( Eigen::MatrixXd& V , const Eigen::Vector3d& shiftVec );

void rotate_directions_inplace( Eigen::MatrixXd& dirs , const Eigen::Quaterniond& rotQuat );

Eigen::Vector3d Eig_vec3d_round_zero( const Eigen::Vector3d& vec );

bool vec3d_eq( const Eigen::Vector3d& v1 , const Eigen::Vector3d& v2 ); // Test if Eigen 3D vectors are "about equal"

Eigen::Vector3d vec_avg( const Eigen::MatrixXd& vertices );

Eigen::Vector3d    err_vec3(); // Return a 3D vec populated with NaN
Eigen::Quaterniond err_quat(); // Return a quaternion populated with NaN
Pose_ASP /* --- */ err_pose(); // Return a 3D pose populated with NaN
Eigen::MatrixXd    err_matx(); // Return a matrix populated with NaN

bool is_err( const Eigen::Vector3d&    query ); // Test if the error vector     was returned
bool is_err( const Eigen::Quaterniond& query ); // Test if the error quaternion was returned
bool is_err( const Pose_ASP& /* --- */ query ); // Test if the error pose       was returned
bool is_err( const Eigen::MatrixXd&    query ); // Test if the error matrix     was returned

Eigen::Vector3d    origin_vec(); // Return a vector that represents no translation
Eigen::Quaterniond no_turn_quat(); // Return a quaternion that represents no rotation

string vertices_to_string( Eigen::MatrixXd& V );
string facets_to_string( Eigen::MatrixXi& F );

bool perp_w_margin( Eigen::Vector3d v1 , Eigen::Vector3d v2 , double CRIT_ANG = 0.0698d );

// = Transformation / Pose =

Eigen::Matrix3d basis_vecs_to_rot_matx( const Eigen::Vector3d& xBasis_B , 
										const Eigen::Vector3d& yBasis_B , 
										const Eigen::Vector3d& zBasis_B );
										
Eigen::Quaterniond basis_vecs_to_quat( const Eigen::Vector3d& xBasis_B , 
									   const Eigen::Vector3d& yBasis_B , 
									   const Eigen::Vector3d& zBasis_B );

Eigen::Vector3d basis_change( Eigen::Vector3d& vec_A , 
							  Eigen::Vector3d& xBasis_B , Eigen::Vector3d& yBasis_B , Eigen::Vector3d& zBasis_B );
							  
Eigen::Vector3d basis_change( Eigen::Vector3d& vec_A , Pose_ASP basisPose );
							  
Eigen::Vector3d point_basis_change( Eigen::Vector3d point_A  , Eigen::Vector3d origin_B , 
									Eigen::Vector3d xBasis_B , Eigen::Vector3d yBasis_B , Eigen::Vector3d zBasis_B );
									
Eigen::Vector3d point_basis_change( Eigen::Vector3d point_A  , Pose_ASP basisPose );
									
Eigen::Vector3d vec3d_from_arbitrary_2D_basis( double x , double y , Eigen::Vector3d xBasis , Eigen::Vector3d yBasis );

Eigen::Vector3d transform_point( Eigen::Vector3d point_A , Pose_ASP basisPose );

Eigen::Vector3d transform_vector( Eigen::Vector3d vec_A  , Pose_ASP basisPose );

bool eq( const Eigen::Quaterniond& left_target , const Eigen::Quaterniond& rght_base );

Eigen::Quaterniond RPY_to_Quat( double Roll , double Pitch , double Yaw );
										   
// _ End Transform _

Eigen::Vector3d get_CCW_tri_norm( const Eigen::Vector3d& v0 , const Eigen::Vector3d& v1 , const Eigen::Vector3d& v2 );
Eigen::Vector3d get_CCW_tri_norm( const Eigen::MatrixXd& V );

Eigen::MatrixXd N_from_VF( const Eigen::MatrixXd& V , const Eigen::MatrixXi& F );

Eigen::MatrixXd vertex_norms_from_reduced_VF( const Eigen::MatrixXd& V , const Eigen::MatrixXi& F );

Eigen::MatrixXd AABB( const Eigen::MatrixXd& V ); // Return the minimum and maximum corners of an AA Bounding Box of arbitrary dimension
Eigen::MatrixXd AABB( const TriMeshVFN_ASP& mesh ); // Return the minimum and maximum corners of an AA Bounding Box of 'mesh'

bool is_point_inside_AABB( const Eigen::Vector3d& query , const Eigen::MatrixXd& bbox );

bool do_AABBs_intersect( const Eigen::MatrixXd& bbox1 , const Eigen::MatrixXd& bbox2 );

Eigen::MatrixXd arbitrary_BB_parent_frame( const Eigen::MatrixXd& V ,
										   const Eigen::Vector3d& origin , 
										   const Eigen::Vector3d& xBasis , const Eigen::Vector3d& yBasis , const Eigen::Vector3d& zBasis );
				
Eigen::MatrixXd box_corners_from_bases_and_extents( const Eigen::Vector3d& origin , 
										            const Eigen::Vector3d& xBasis , const Eigen::Vector3d& yBasis , const Eigen::Vector3d& zBasis ,
										            const std::vector<double>& extents );

double extent_in_direction( const Eigen::MatrixXd& V , const Eigen::Vector3d& direction );

Eigen::MatrixXd sample_from_AABB( size_t N , const Eigen::MatrixXd& aabb ); // Return 'N' uniform, random samples from AABB
Eigen::Vector3d sample_from_AABB( const Eigen::MatrixXd& aabb ); // Return a uniform, random samples from AABB

Eigen::MatrixXd span_from_AABB( Eigen::MatrixXd aabb ); // Return the extent of each dimension of an AABB as a row vector

Eigen::Vector3d tri_normal( Eigen::Vector3d p0 , Eigen::Vector3d p1 , Eigen::Vector3d p2 ); // Return normal of CCW triangle in R3
Eigen::Vector3d tri_normal( Eigen::MatrixXd CCWverts ); // Return normal of CCW triangle in R3

double tri_area( Eigen::Vector3d p0 , Eigen::Vector3d p1 , Eigen::Vector3d p2 ); // Return area of CCW triangle in R3
double tri_area( Eigen::MatrixXd CCWverts ); // Return area of CCW triangle in R3

bool are_those_triangles_adjacent( Eigen::MatrixXd triA , Eigen::MatrixXd triB , double CRIT_ANG , double CRIT_DST ); // Return true if close

Eigen::Vector3d closest_point_to( Eigen::MatrixXd points , Eigen::Vector3d queryPnt );
Eigen::Vector3d farthest_point_from( Eigen::MatrixXd points , Eigen::Vector3d queryPnt );

IndexDbblResult farthest_point_from_sq( Eigen::MatrixXd points , Eigen::Vector3d queryPnt );
IndexDbblResult closest_point_to_sq( Eigen::MatrixXd points , Eigen::Vector3d queryPnt );
IndexDbblResult closest_point_to_sq( std::vector<double> points , double queryPnt );

Eigen::Vector3d line_intersect_plane( Eigen::Vector3d rayOrg , Eigen::Vector3d rayDir , 
									  Eigen::Vector3d planePnt , Eigen::Vector3d planeNrm ,
									  bool pntParallel = true );

Eigen::MatrixXd circle_arc_3D( const Eigen::Vector3d& axis , const Eigen::Vector3d& center , 
							   double radius , const Eigen::Vector3d& beginMeasureVec , double theta , size_t N );
							   
Eigen::MatrixXd linspace_pts_3D( Eigen::Vector3d bgn , Eigen::Vector3d end , size_t N );

Eigen::Quaterniond unif_rand_quat(); // Generate a uniformly-distributed random unit quaternion

// == Direction Counting ==

// Copy the rows of 'original' that represent unique directions
Eigen::MatrixXd uniqify_directions( const Eigen::MatrixXd& directions , double CRIT_ANG = GEO_CRIT_ANG );

// Count the number of unique directions
size_t count_unique_dirs(   const Eigen::MatrixXd& directions , double CRIT_ANG = GEO_CRIT_ANG );

// Count all of the directions that do not have an opposing direction
size_t count_unopposed(     const Eigen::MatrixXd& directions , double CRIT_ANG = GEO_CRIT_ANG ); 

// Return a row list of all of the unique directions that opposing vectors are colinear with
Eigen::MatrixXd get_unopposed_dirs( const Eigen::MatrixXd& directions , double CRIT_ANG = GEO_CRIT_ANG ); 

// Count all of the unique directions that opposing vectors are colinear with
size_t count_opposing_dirs( const Eigen::MatrixXd& directions , double CRIT_ANG = GEO_CRIT_ANG ); 

// Return a row list of all of the unique directions that opposing vectors are colinear with
Eigen::MatrixXd get_opposing_dirs( const Eigen::MatrixXd& directions , double CRIT_ANG = GEO_CRIT_ANG ); 

// __ End Counting __

Eigen::Vector3d get_any_perpendicular( const Eigen::Vector3d& query , double CRIT_ANG = GEO_CRIT_ANG );
Eigen::Vector3d get_any_perpendicular( const Eigen::Vector3d& query , const Eigen::Vector3d& perpSuggest , double CRIT_ANG = GEO_CRIT_ANG );

Eigen::MatrixXd get_any_orthogBasis_for_X( const Eigen::Vector3d& Xquery , double CRIT_ANG = GEO_CRIT_ANG );
Eigen::MatrixXd get_any_orthogBasis_for_Z( const Eigen::Vector3d& Zquery , double CRIT_ANG = GEO_CRIT_ANG );
Eigen::MatrixXd get_any_orthogBasis_for_Z( const Eigen::Vector3d& Zquery , const Eigen::Vector3d& Xsuggest , double CRIT_ANG = GEO_CRIT_ANG );

Eigen::MatrixXd cardinal_directions_R3(); // Return positive and negative directions along each principal axis

// ___ End Geometry ___


// === Mesh Operations ===

Eigen::MatrixXd get_facet_centers( const Eigen::MatrixXd& vertices , const Eigen::MatrixXi& facetIndices );

Eigen::MatrixXi get_adjacency_matx( const Eigen::MatrixXi& facetIndices );

std::vector<std::vector<int>> get_adjacency_list( const Eigen::MatrixXi& facetIndices );

std::vector<std::vector<int>> facet_adjacency_list_ordered( const Eigen::MatrixXi& F );

TriMeshVFN_ASP remove_duplicate_vertices_from_VF( const Eigen::MatrixXd& OV , const Eigen::MatrixXi& OF );

std::vector<std::vector<int>> facet_adjacency_list_ordered_dense( const Eigen::MatrixXd& V , const Eigen::MatrixXi& F );

std::list< Segment2D_ASP > get_outside_CCW_poly_from_mesh_subset( const Eigen::MatrixXd& V2D , const Eigen::MatrixXi& F , 
																  const std::vector<std::vector<int>>& orderedNeighborsF , 
																  const std::vector<int>& facetSublist );
																  
Eigen::MatrixXd get_closed_poly_verts_from_CCW_segments( const std::vector< Segment2D_ASP >& segments );
Eigen::MatrixXd get_closed_poly_verts_from_CCW_segments( const std::list< Segment2D_ASP >& segments   );

Eigen::Vector3d vec_from_pnt_to_plane( const Eigen::Vector3d& queryPnt , const Eigen::Vector3d& planePnt , const Eigen::Vector3d& normal );

Eigen::MatrixXd transform_V( const Eigen::MatrixXd& V , const Eigen::Vector3d& position , const Eigen::Quaterniond& orientation );

Eigen::MatrixXd V_in_child_frame( const Eigen::MatrixXd& V , 
							      const Eigen::Vector3d& origin , 
							      const Eigen::Vector3d& xBasis , const Eigen::Vector3d& yBasis , const Eigen::Vector3d& zBasis );
							      
Eigen::MatrixXd V_in_parent_frame( const Eigen::MatrixXd& V , 
							       const Eigen::Vector3d& origin , 
							       const Eigen::Vector3d& xBasis , const Eigen::Vector3d& yBasis , const Eigen::Vector3d& zBasis );

Eigen::MatrixXd transform_Dir( const Eigen::MatrixXd& directions , const Eigen::Quaterniond& orientation );

// == Mesh Transformations ==

TriMeshVFN_ASP shrink_dense_VFN_along_normals( const TriMeshVFN_ASP& original , double shrinkDistance ); // Shrink a dense mesh along normals

TriMeshVFN_ASP shrink_dense_VFN_by_Vtx_normals( const TriMeshVFN_ASP& original , double shrinkDistance ); // Shrink a dense mesh along vertex normals

TriMeshVFN_ASP expand_VFN_from_center( const TriMeshVFN_ASP& original , double factor );

TriMeshVFN_ASP expand_VFN_from_axis( const TriMeshVFN_ASP& original , double factor );

TriMeshVFN_ASP shift_VFN_along_axis( const TriMeshVFN_ASP& original , double distance );

// __ End Mesh Transform __

double furthest_extent_from_radius_in_dir( const TriMeshVFN_ASP& original , const Eigen::Vector3d& direction ); // Return the furthest from 'center'

double V_radius_from_center( const Eigen::MatrixXd& V , const Eigen::Vector3d& center );

Eigen::MatrixXd get_triangle_i( const Eigen::MatrixXd& V , const Eigen::MatrixXi& F , size_t i ); // Get the 'V' of 'i'th triangle in 'F'

bool F_indices_OK( Eigen::MatrixXd& V , Eigen::MatrixXi& F ); // Check that F does not refer to nonexistent rows

double surface_area( const TriMeshVFN_ASP& mesh ); // Return the sum of the areas of all the triangles in the mesh

RayHits ray_intersect_VFN( const Eigen::Vector3d& rayOrg , const Eigen::Vector3d& rayDir , const TriMeshVFN_ASP& mesh );

Eigen::Vector3d ray_intersect_AABB( const Eigen::Vector3d& origin , const Eigen::Vector3d& dir , const Eigen::MatrixXd& aabb );

RayHits ray_intersect_TargetVFN( const Eigen::Vector3d& rayOrg , const Eigen::Vector3d& rayDir , const TargetVFN_ASP& target );

RayHits ray_intersect_CollsnVFN( const Eigen::Vector3d& rayOrg , const Eigen::Vector3d& rayDir , const CollisionVFN_ASP& target );

RayHits perforate_meshes_and_obtain_FILO_pairs( std::vector<TargetVFN_ASP>& meshTargets , 
												Eigen::MatrixXd rayOrgs , Eigen::MatrixXd rayDirs );
												
Eigen::MatrixXd extract_V_from_TargetVFN( std::vector<TargetVFN_ASP>& partTargets );

TriMeshVFN_ASP merge_meshes( const TriMeshVFN_ASP& op1 , const TriMeshVFN_ASP& op2 ); // Naively combine traingles from two different meshes

// == Centroid Operations ==

VolumeCentroid get_VolumeCentroid_from_V_F( Eigen::MatrixXd& V , Eigen::MatrixXi& F );
VolumeCentroid get_VolumeCentroid_from_V_F( Eigen::MatrixXd& V , const Eigen::MatrixXi& F ); // NOT SURE IF USED?

VolumeCentroid get_VolumeCentroid_from_discrete_volume_centroids( std::vector<VolumeCentroid>& discretePointVolumes );

double get_tetrahedron_volume( Eigen::Vector3d& v0 , 
							   Eigen::Vector3d& v1 ,
							   Eigen::Vector3d& v2 ,
							   Eigen::Vector3d& v3 );
							   
double get_tetrahedron_volume( Tetrahedron_ASP tet );

Eigen::Vector3d get_tetrahedron_centroid( Eigen::Vector3d& v0 , 
										  Eigen::Vector3d& v1 ,
										  Eigen::Vector3d& v2 ,
										  Eigen::Vector3d& v3 );
										  
Eigen::Vector3d get_tetrahedron_centroid( Tetrahedron_ASP tet );

VolumeCentroid get_tetrahedron_VolumeCentroid( Eigen::Vector3d& v0 , 
											   Eigen::Vector3d& v1 ,
											   Eigen::Vector3d& v2 ,
											   Eigen::Vector3d& v3 );
											   
VolumeCentroid get_tetrahedron_VolumeCentroid( Tetrahedron_ASP tet );

// __ End Centroid __


// == Convex Hull ==

TriMeshVFN_ASP V_to_ConvexHull_VFN( const Eigen::MatrixXd& Vertices );

// Convert an Eigen R3 vector to a quickhull R3 vector
quickhull::Vector3<double> eigV3d_to_hullV3d( Eigen::Vector3d& eigVec );

// Convert an quickhull R3 vector to a Eigen R3 vector
Eigen::Vector3d hullV3d_to_eigV3d( quickhull::Vector3<double>& hullVec );

// 'const' version of the above
Eigen::Vector3d hullV3d_to_eigV3d( const quickhull::Vector3<double>& hullVec );

bool check_VFN_convexity( const TriMeshVFN_ASP& mesh );

void repair_convex_VFN( TriMeshVFN_ASP& mesh );

// __ End Hull __


// == class Icosahedron_d ==

// Geometry based on Paul Bourke's excellent article:
//   Platonic Solids (Regular polytopes in 3D)
//   http://astronomy.swin.edu.au/~pbourke/polyhedra/platonic/

class Icosahedron_d{
public:
	// ~ Constants ~
	double sqrt5 = sqrt( 5.0d ); // ------------------------------------ Square root of 5
	double phi   = ( 1.0d + sqrt5 ) * 0.5d; // ------------------------- The Golden Ratio
	double ratio = sqrt( 10.0d + ( 2.0d * sqrt5 ) ) / ( 4.0d * phi ); // ratio of edge length to radius
	
	// ~ Variables ~
	Eigen::Vector3d center;
	double /* -- */ radius;
	double /* -- */ a; 
	double /* -- */ b; 
	Eigen::MatrixXd V; // Points of the mesh
	Eigen::MatrixXi F; // Facets corresponding to the points V
	
	// ~ Constructors & Destructors ~
	Icosahedron_d(); // ---------------------------------- Default constructor
	Icosahedron_d( double rad , const Eigen::Vector3d& cntr ); // Parameter constructor
	~Icosahedron_d(); // --------------------------------- Destructor
	
	// ~ Getters ~
	Eigen::MatrixXd& get_vertices();
	Eigen::MatrixXi& get_facets();
	
protected:
	// ~ Init ~
	void _init( double rad , const Eigen::Vector3d& cntr );
};

// __ End Icosahedron_d __


// == class Sphere_d ==

// = Sphere Helpers =

bool test_against_constraint_norms(  const Eigen::MatrixXd& constraintNorms , const Eigen::Vector3d& rayDir ); // Return true if ray does not violate constraints

bool test_against_constraint_planes( const Eigen::MatrixXd& constraintNorms , const Eigen::MatrixXd& constraintPnts , const Eigen::Vector3d& queryPnt );

// _ End Helpers _

class Sphere_d{
public:
	
	// ~ Variables ~
	Eigen::Vector3d center;
	double /* -- */ radius;
	Icosahedron_d   baseIcos; // Icosahedron from which the sphere will be generated
	size_t /* -- */ sub; // ---- Number of rows to divide each face into
	Eigen::MatrixXd V; // ------ Points of the mesh
	Eigen::MatrixXi F; // ------ Facets corresponding to the points V
	
	// ~ Constructors & Destructors ~
	Sphere_d(); // ---------------------------------- Default constructor
	Sphere_d( double rad , const Eigen::Vector3d& cntr , size_t subdivision = ICOS_SPHERE_DIVISN ); // Parameter constructor
	Sphere_d( Icosahedron_d icos , size_t subdivision = ICOS_SPHERE_DIVISN );
	~Sphere_d(); // --------------------------------- Destructor
	
	// ~ Getters ~
	Eigen::MatrixXd& get_vertices();
	Eigen::MatrixXi& get_facets();
	TriMeshVFN_ASP   get_mesh();
	
	// ~ Surface slicing ~
	TriMeshVFN_ASP get_surface_w_direction_constraints( const Eigen::MatrixXd& constraintNorms );
	TriMeshVFN_ASP get_surface_w_planar_constraints( const Eigen::MatrixXd& constraintNorms , const Eigen::MatrixXd& constraintPnts );
	
	// ~ Direction Query ~
	Eigen::MatrixXd sample_directions_from_direction_constraints( const Eigen::MatrixXd& constraintNorms );
	Eigen::MatrixXd get_all_spherical_directions();
	
protected:
	// ~ Init ~
	void _init( Icosahedron_d icos , size_t subdivision );
	void _transform_V();
};

// __ End Sphere_d __


// == Special Meshes ==

// Return a mesh that represents the surface of revolution of line segment with 'height' that is parallel to the axis of revolution at 'radius'
TriMeshVFN_ASP revolved_segment( const Eigen::Vector3d& center , const Eigen::Vector3d& axis , double radius , double height , 
								 size_t divisions = CIRCLE_DIVISION );

// __ End Special __


TriMeshVFN_ASP mesh_shadow_along_Z( const TriMeshVFN_ASP& original , double squareSide ,
									const Eigen::Vector3d& origin ,
									const Eigen::Vector3d& xBasis , const Eigen::Vector3d& yBasis , const Eigen::Vector3d& zBasis );
									
TriMeshVFN_ASP get_mesh_w_direction_constraints( const TriMeshVFN_ASP& original , const Eigen::MatrixXd& constraintNorms );


// == Mesh Collision ==

// Return true if two triangles intersect , Otherwise return false
bool triangle_collision( Eigen::Vector3d V1_0 , Eigen::Vector3d V1_1 , Eigen::Vector3d V1_2 , Eigen::Vector3d norm1 ,
						 Eigen::Vector3d V2_0 , Eigen::Vector3d V2_1 , Eigen::Vector3d V2_2 , Eigen::Vector3d norm2 );

// __ End Collision __


// ___ End Mesh ___


#endif

/* === Spare Parts =========================================================================================================================



   ___ End Parts ___________________________________________________________________________________________________________________________

*/

