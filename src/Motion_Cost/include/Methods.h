#pragma once // This also helps things not to be loaded twice , but not always . See below

/***********  
Methods.h
James Watson , 2018 July
Sequencing and Motion Planning, Together Forever

Template Version: 2018-06-06
***********/

#ifndef METHODS_H // This pattern is to prevent symbols to be loaded multiple times
#define METHODS_H // from multiple imports

// ~~ Includes ~~
// ~ LIBNAME_i ~
// ~ Local ~
#include <MathGeo_ASP.h>
#include <ASP_3D.h> // ---- Assembly Geometry
#include <AsmSeqPlan.h> //- Planning
#include <Motion_Planning.h> // Path planning & Collision Detection


// ~~ Shortcuts and Aliases ~~

// ~~ Constants ~~
const Eigen::Vector3d DEFAULT_REMDIR = Eigen::Vector3d( 0.0 , 0.0 , 1.0 );

// === Classes and Structs =================================================================================================================

//~ enum ACTIONS{ ACT_ROTATE ,  // Rotate the entire assembly and place it on a new suppiort
			  //~ ACT_REMOVE ,  // Remove one or more parts from an assembly, replacing that assembly with two new assemblies
			  //~ ACT_NO_OPR }; // Placeholder action

struct SuccessPath{ 
	/* Container of action planning info, with 2 purposes: 
		1) Flag whether action planning succeeded or failed 
		2) Store enough information to reconstruct the action */
	EIGEN_MAKE_ALIGNED_OPERATOR_NEW
	bool /* --------------------- */ success , // ------- Succeed or Fail?
									 escapedRefc ; // --- Did the mover move outside of the bounding box of the reference?
	ValidationCode /* ----------- */ code; // ----------- Status code
	string /* ------------------- */ desc = ""; // ------ Description of { disposition , failure reason , results , etc }
	std::vector<Pose_ASP> /* ---- */ path; // ----------- Winning path, if requested, optional
	std::vector<std::vector<double>> qSeq; // ----------- Sequence of IK solns matching 'path'
	std::vector<double> /* ------ */ gripState; // ------ Position of the gripper fingers for this operation
	double /* ------------------- */ travelDist; // ----- Furthest along a path that we have traveled
	Eigen::Vector3d /* ---------- */ bestDir; // -------- First winning direction
	Eigen::MatrixXd /* ---------- */ goodDirs; // ------- ALL straightline winning directions
	std::vector<Pose_ASP> /* ---- */ posesFree; // ------ ALL mover poses that collide with neither the reference nor the floor , Possible RRT seed
	Pose_ASP /* ----------------- */ initPose; // ------- Init  pose for mover + reference
	Pose_ASP /* ----------------- */ finlPose; // ------- Final pose for mover
	ValidationCode /* ----------- */ code_cached; // ---- Status code from inner check
	string /* ------------------- */ desc_cached = ""; // Description of { disposition , failure reason , results , etc } from inner check
	std::vector<llin> /* -------- */ movdIDs; // -------- Parts removed by the action 
	std::vector<llin> /* -------- */ refcIDs; // -------- Parts stationary during the action
	std::vector<Pose_ASP> /* ---- */ handPath; // ------- Hand path (abs), if requested, optional
	Pose_ASP /* ----------------- */ relHandPose; // ---- Best relative hand pose
	ParallelGraspState /* ------- */ bestGrasp; // ------ Best grasp target to execute
};

SuccessPath empty_SuccessPath();

SuccessPath magic_success_rotate();

// ___ End Classes _________________________________________________________________________________________________________________________



// === Functions ===========================================================================================================================

/// === VALIDATION =========================================================================================================================

struct ValidationParams{ // This is to save mixups with parameters 
	EIGEN_MAKE_ALIGNED_OPERATOR_NEW
	// ~ Validation & Execution ~
	size_t   support; // -------- Index of the support on which the part is resting
	Pose_ASP planPose; // ------- Default placement for assembly
	double   distance; // ------- Maximum extent to pull the part out along a straight line
	usll     numSamples; // ----- Number of locations to test along the removal direction from 0 to 'distance'
	double   sphereFraction; // - Fraction of the sphere directions to test
	size_t   RRT_K; // ---------- Max number of RRT* samples
	double   RRT_Epsilon     , // Max distance between configurations for RRT*
		     RRT_BiasProb    , // Probability with which RRT chooses the goal as the target sample
		     RRT_WeightPosn  , // Cost weight on position    for calculating configuration distance
		     RRT_WeightOrnt  , // Cost weight on orientation for calculating configuration distance
		     RRT_nghbrRadius , // Max radius around a new sample for rewiring
		     RRT_liftBoxFctr ; // Multiple of 'distance' for the bounding box
	size_t   RRT_shortCutN; // -- Stop rewiring after this many neighbors
	// ~ Execution ~
	Eigen::Vector3d setdownLoc; // ------- Place on a flat table where the operation is taking place
	Eigen::Vector3d ROBOT_BASE; // ------- Base of the robot
	double /* -- */ graspTileSize , // --- Spacing between grasps
				    graspDTheta   ; // --- Angles between grasps , Rotating about a pair of intersection points
	Eigen::Vector3d preferredWristDir , // Prefer to point the wrist in this direction
					preferredCenter   ; // Prefer to grasp at this location in space
	size_t /* -- */ IKsearchLimit     , // Number of times to retry IK during grasp search
					IKattemptLimit    , // Number of times to retry IK for a grasp that we care about
					maxReplanFailures , // Max number of replans for a lift operation after all other options have been exhausted
					bel_minNumSeeds   ; // Min number of bases to look for (Belhadj)
};

ValidationParams validation_defaults();
ValidationParams copy_params( const ValidationParams& original );

SuccessPath validate_removal_multimode_w_stability( Assembly_ASM* original , // Validate for removal and stability , Using Straightline and RRT*
												    std::vector<llin>& movedParts , std::vector<llin>& referenceParts , 
												    ValidationParams params , 
												    const Eigen::Vector3d& direction = DEFAULT_REMDIR );

/// ___ END VALIDATE _______________________________________________________________________________________________________________________


/// === PLAN EXECUTION =====================================================================================================================

// ~~~ LIFT PLANNER ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

bool any_poses_below_Z( const std::vector<Pose_ASP>& allPoses , double Zlvl );

template< typename IK >
SuccessPath plan_one_removal_action_multimode( Assembly_ASM* original , std::vector<llin> movd , std::vector<llin> refc ,
											   Floor_ASM& floor , ValidationParams params ,
											   IK IK_clientCall , 
											   double CRIT_ANG = GEO_CRIT_ANG ){
	// Attempt to plan a removal action with a robot arm and the floor
	// NOTE: This function assumes that 'original' has been posed on 'floor' in a way that allows lifting
	// NOTE: This function assumes that the support index for 'original' has been properly set
	// NOTE: This function makes a copy of the assembly and works with it in the 'original' pose
	
	
	bool SHOWDEBUG = true  , // if( SHOWDEBUG ){  cout << "" << endl;  }
		 BREAKPNTS = true  , // if( BREAKPNTS ){  waitkey();  }
		 POSDETAIL = false ;
	
	if( SHOWDEBUG ){  sep( "plan_one_removal_action_multimode" );
					  cerr << "Plan for a parade of happy thoughts!" << endl;  }
	
	SuccessPath result = empty_SuccessPath(); // Return structure
	result.movdIDs = vec_copy( movd );
	result.refcIDs = vec_copy( refc );
	
	// ~~ Init ~~
	// A. Vars
	stringstream diagnosticStr; // Diagnostic string
	bool /* - */ geoChk = false;
	bool /* - */ GOchk2 = false;
	
	// B. Instantiate a test asm to manipulate
	Assembly_ASM* testAsm  = original->replicate();
	
	
	TriMeshVFN_ASP  testMesh;
	Eigen::MatrixXd asmAABB;
	
	Pose_ASP strtPose = testAsm->get_pose();
	
	if( BREAKPNTS ){  
		if( original->get_pose().position(2) < 0.5 ||
			testAsm->get_pose().position(2) < 0.5 ||
		    strtPose.position(2) < 0.5 ){ 
			cerr << "Original Assembly Pose: " << original->get_pose() << " , Bounding box: " << endl
				 << AABB( original->get_lab_VFN() ) << endl
				 << "Replicant Asm Pose: ___ " << testAsm->get_pose() << " , Bounding box: " << endl
				 << AABB( testAsm->get_lab_VFN() ) << endl
				 << "Stored 'strtPose': ____ " << strtPose << endl;  
			waitkey();  
		}
	}
	
	// C. Instantiate assemblies and pose
	Assembly_ASM* movdAsm = testAsm->sub_from_spec( movd );		movdAsm->set_pose( strtPose );
	Assembly_ASM* refcAsm = testAsm->sub_from_spec( refc );		refcAsm->set_pose( strtPose );
	
	// D. Instantiate hand model
	hand_collision_model handModel;		handModel.init();
	
	// ~~~ LAMBDA FUNCS ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
	
	auto cache_free_mover_pose = [&]( const Pose_ASP& freePose ){  result.posesFree.push_back( freePose );  };
	
	auto part_pose_seq_from_dir = [&]( const Eigen::Vector3d& queryDir ){
		// Fetch all of the intermediate part poses for 
		std::vector<Pose_ASP> poseSeq;
		double dDist = params.distance / (double) params.numSamples;
		
		for( size_t m = 0 ; m <= params.numSamples ; m++ ){
			Pose_ASP temp{ strtPose.position + queryDir * ( dDist * (double) m ) , strtPose.orientation };
			poseSeq.push_back( temp );
			if(  POSDETAIL  &&  temp.position(2) < 0.5  ){  
				cerr << "Components" << endl
					 << "Iteration: ____ " << m << endl
					 << "Start Pose: ___ " << strtPose << endl
					 << "Start Position: " << strtPose.position << endl
					 << "Query Dir: ____ " << queryDir << endl
					 << "Distance: _____ " << ( dDist * (double) m ) << endl
					 << "Orientation: __ " << strtPose.orientation << endl
				     << temp << endl;  
			}
		}
		
		if( SHOWDEBUG ){  cerr << endl
							   << "Calculating a path for direction " << queryDir << endl
							   << "Start Position: " << strtPose.position << endl
							   << "Delta distance: " << dDist << endl
							   << "Num Samples: __ " << params.numSamples << endl;  }
		
		// This isn't necessarily bad
		if( false && any_poses_below_Z( poseSeq , 0.5 ) ){
			cerr << "About to return a bad sequence ..." << endl;
			waitkey();
		};
		
		return poseSeq;
	};
	
	auto log_good_dir = [&]( const Eigen::Vector3d& queryDir ){  result.goodDirs = copy_V_plus_row( result.goodDirs , queryDir );  };
	
	auto end_straightline_part_pose = [&]( const Eigen::Vector3d& queryDir ){
		// Fetch the ending pose for a straightline removal
		return Pose_ASP{ strtPose.position + queryDir * params.distance , strtPose.orientation };
	};
	
	auto gen_poses_for_hand_from_part_seq = [&]( const std::vector<Pose_ASP>& seqPoses , const Pose_ASP& relPoseHand ){
		// Return true if none of 'seqPoses' put 'handModel' in collision given 'relPoseHand' , Otherwise return false
		size_t len = seqPoses.size();
		std::vector<Pose_ASP> rtnSeq;
		// 1. For each of the part poses
		for( size_t i = 0 ; i < len ; i++ ){  rtnSeq.push_back( seqPoses[i] * relPoseHand );  }
		return rtnSeq;
	};
	
	auto check_part_poses_for_hand = [&]( const std::vector<Pose_ASP>& seqPoses , const Pose_ASP& relPoseHand ){
		// Return true if none of 'seqPoses' put 'handModel' in collision given 'relPoseHand' , Otherwise return false
		bool     check = true;
		size_t   len   = seqPoses.size();
		Pose_ASP handAbsPose;
		Pose_ASP savedPose = handModel.get_pose();
		// 1. For each of the part poses
		for( size_t i = 0 ; i < len ; i++ ){
			// 2. Set the absolute hand pose
			handAbsPose = seqPoses[i] * relPoseHand;
			handModel.set_pose( handAbsPose );
			// 4. Check for hand-refc  collision , Break on first collision
			if(  refcAsm->collides_with( handModel.LBR4hand )  ){  check = false;  break;  }
			// 5. Check for hand-floor collision , Break on first collision
			if(  floor.collides_with( handModel.LBR4hand )  ){  check = false;  break;  }
		}
		// N-1. Restore pose
		handModel.set_pose( savedPose );
		// N. Return
		return check;
	};
	
	auto clean_asm = [&](){
		delif( testAsm );
		delif( movdAsm );
		delif( refcAsm );
	};
	
	// ,,, END LAMBDA ,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,
	
	
	/// ~~ Check 0 : Re-validate and Cache Path Data ~~
	SuccessPath validationCache = empty_SuccessPath(); 
	
	validationCache = validate_removal_multimode_w_stability( testAsm , // Validate for removal and stability , Using Straightline and (eventually) RRT*
															  movd , refc , 
															  params );
	result.code_cached = validationCache.code; // Status code from inner check
	result.desc_cached = validationCache.desc; // Description of { disposition , failure reason , results , etc } from inner check
	if( !(validationCache.success) ){
		if( SHOWDEBUG ){  cerr << "VALIDATION_FAULT: Re-validation failed with code " << interpret_ValidationCode( validationCache.code ) << endl 
							   << "VALIDATOR MESSAGE: " << validationCache.desc << endl;  }
		result.success = false;
		result.code = VALIDATION_FAULT;
		diagnosticStr << "VALIDATION_FAULT: Re-validation failed with code " << interpret_ValidationCode( validationCache.code ) << endl 
					  << "VALIDATOR MESSAGE: " << validationCache.desc << endl;
		result.desc = diagnosticStr.str();
		clean_asm();
		return result;
	}else if( SHOWDEBUG ){  
		cerr << "Inner validation passed" << endl  
			 << "Points Recorded: _ " << validationCache.path.size() << " out of " << params.numSamples << " required" << endl
			 << "Distance Traveled: " << validationCache.travelDist << " [m] out of " << params.distance << " required" << endl;
	}
	
	/// ~~ Check 1 : Above Table ~~
	//~ TriMeshVFN_ASP  asmLabMesh;
	testMesh = testAsm->get_lab_VFN();
	asmAABB = AABB( testMesh );
	
	geoChk = (  abs( asmAABB(0,2) - params.setdownLoc(2) ) < EPSILON  &&  abs( asmAABB(1,2) >= params.setdownLoc(2) )  );
	if( !(geoChk) ){
		if(1){
			if( SHOWDEBUG ){  cerr << "BAD_RESTING_POSE: Assembly has bounding box " << endl << asmAABB << endl 
								   << "which lies AWAY FROM the setdown location " << params.setdownLoc << endl;  }
			result.success = false;
			result.code = BAD_RESTING_POSE;
			diagnosticStr << "BAD_RESTING_POSE: Assembly has bounding box " << endl << asmAABB << endl 
						  << "which lies AWAY FROM the setdown location " << params.setdownLoc << endl;
			result.desc = diagnosticStr.str();
			clean_asm();
			return result;
		}
		
		if( BREAKPNTS ){  waitkey();  }
		
	}else if( SHOWDEBUG ){  cerr << "Assembly above table , AABB:" << endl << asmAABB << endl;  }
	
	
	/// ~~ Check 2 : Stability Concerns ~~
	
	bool chkPass = false;
	GOchk2 = false;
	IndexSearchResult suggestSpprt;
	
	// Check if pose is on the table
	suggestSpprt = testAsm->get_support_from_current_pose();
	if( !(suggestSpprt.result) ){
		if( SHOWDEBUG ){  cerr << "BAD_RESTING_POSE: Pose does not seem to be on any support " << endl;  }
		result.success = false;
		result.code = BAD_RESTING_POSE;
		diagnosticStr << "BAD_RESTING_POSE: Pose does not seem to be on any support " << endl;
		result.desc = diagnosticStr.str();
		clean_asm();
		return result;
	}else if( SHOWDEBUG ){  cerr << "Suggested Support: " << suggestSpprt.index << " , ASM Support: " << testAsm->get_support() << endl;  }
	
	// Check if reference will be left hanging
	chkPass = testAsm->validate_subasm_remainder_does_not_float( testAsm->get_support() , movd );
	if( !chkPass ){
		if( SHOWDEBUG ){  cout << "FLOATING_HAM_ERR: Reference " << refc << " does not share any supports with the original assembly" << endl;  }
		result.success = false;
		result.code = FLOATING_HAM_ERR;
		diagnosticStr << "FLOATING_HAM_ERR: Reference " << refc << " does not share any supports with the original assembly" << endl;
		result.desc = diagnosticStr.str();
		clean_asm();
		return result;
	}else if( SHOWDEBUG ){  cout << "Was not floating." << endl;  }
	
	
	/// ~~ Check 3 : Cached Path Clear & Save ~~
	
	size_t /* -------- */ numPoses = validationCache.path.size();
	Pose_ASP /* ------ */ currPose;
	std::vector<Pose_ASP> winPath;
	bool /* ---------- */ pathFailed         = false ,
						  moverOnlyPathFound = false ;
	std::vector<Pose_ASP> bestMoverPath;
	
	// 1. If there is a path stored
	if( numPoses > 0 ){
		// 2. For each of the positions in the winning path
		for( size_t i = 0 ; i < numPoses ; i++ ){
			// 3. Move the mover to the specified offset
			currPose = strtPose * validationCache.path[i];
			if( POSDETAIL ){  cerr << "Cached Pose" << validationCache.path[i] << endl
								   << "\t---> Mover Pose " << currPose << endl;  }
								   
			if( BREAKPNTS ){
				double diffDist = (currPose.position - strtPose.position).norm(); 
				if( diffDist > 0.5 ){
					cerr << "Calculated a move pose very far away " << diffDist << " [m]" << endl;
					waitkey();
				}
			}
								   
			movdAsm->set_pose( currPose );
			// 4. Check for Reference collision
			geoChk = !( movdAsm->collides_with( *refcAsm ) );
			// 5. Check for floor     collision
			GOchk2 = !( movdAsm->collides_with( floor ) );
			// 6. If the pose is free , cache it
			if( geoChk && GOchk2 ){  
				cache_free_mover_pose( currPose );  
				// 7. If we are on a supposedly winning path , append to the winning path
				if(  ( !pathFailed )  &&  ( validationCache.success )  ){  winPath.push_back( currPose );  }
			}else{  
				pathFailed = true;  
				if( SHOWDEBUG ){  cerr << "Suggested straightline failed at pose:" << currPose << endl;  }
			}
		}
	}else if( SHOWDEBUG ){  cout << "Validator did not populate a pose sequence!" << endl;  }
	if( winPath.size() == 0 ){  pathFailed = true;  }
	
	// If the validator and the planner both liked the path , Flag it found and store it
	if(  ( !pathFailed )  &&  ( validationCache.success )  ){  
		if( SHOWDEBUG ){  cout << "Stored a winning path!" << endl;  }
		moverOnlyPathFound = true;  
		bestMoverPath      = winPath;
		result.bestDir     = validationCache.bestDir;
		log_good_dir( validationCache.bestDir );
	}
	
	/* ~ At this point we have evaluated the path that the validator thinks should be a success , 
		 However the mover may have hit the floor , and there are other paths to try 
		 Get as many successful paths as possible to try with the hand when we add it ~ */
	
	
	/// ~~ Check 4 : Evaluate Cached Candidate Directions & Save ~~
	// There are multiple winners, but only one is best
	
	Eigen::Vector3d /*-*/ curDir;
	std::vector<Pose_ASP> testPath;
	size_t /* -------- */ numDirs = 0;
	
	// 1. See if the validator logged any candidate directions based on freedom
	numDirs = validationCache.goodDirs.rows();
	// 2. If there are directions to tru
	if( numDirs > 0 ){
		// 3. For each of the logged directions
		for( size_t i = 0 ; i < numDirs ; i++ ){
			// 4. Generate a path to test
			curDir   = validationCache.goodDirs.row(i); // These were stored in the relative frame
			curDir   = Eig_vec3d_round_zero( testAsm->get_orientation() * curDir );
			testPath = part_pose_seq_from_dir( curDir );
			
			if( false && BREAKPNTS ){
				if( any_poses_below_Z( testPath , 0.5 ) ){
					cout << "While testing direction " << curDir << " encountered a bad mover pose from 'part_pose_seq_from_dir'" << endl
						 << "Start Pose: " << strtPose << endl;
					waitkey();
				}
			}
			
			numPoses   = testPath.size();
			pathFailed = false;
			// 5. For each of the poses on that path
			for( size_t j = 0 ; j < numPoses ; j++ ){
				// 3. Move the mover to the specified offset
				currPose = testPath[j];
				movdAsm->set_pose( currPose );
				// 4. Check for Reference collision
				geoChk = !( movdAsm->collides_with( *refcAsm ) );
				// 5. Check for floor     collision
				GOchk2 = !( movdAsm->collides_with( floor ) );
				// 6. If the pose is free , cache it
				if( geoChk && GOchk2 ){  
					cache_free_mover_pose( currPose );  
				}else{  pathFailed = true;  }
			}
			// 7. If the path won
			if( !pathFailed ){  
				// 8. Log winning path
				log_good_dir( curDir );
				// 9. If there was not a previously identified best path
				if( !moverOnlyPathFound ){
					moverOnlyPathFound = true;  
					bestMoverPath      = testPath;
					result.bestDir     = curDir;
				}
			}
		}
	}
	
	/* ~ At this point we have either identified the best (first winning) path for Mover -vs- { Reference , Floor } or not ,
		 We may have also identified many candidate directions that are ok against { Reference , Floor } ,
		 We are keeping these in our back pocket for grasp planning and IK ~ */ 
	
	/* ~ 2018-07-28: For now, making the assumption that a robot motion plan can be generated for the already-chosen part motion plan.  
	So, only proceed if we arlready have a winning translation path ~ */
	if( moverOnlyPathFound ){
		
		std::vector<ParallelGraspState> noncollidingGrsps;
		std::vector<Pose_ASP> /* --- */ noncollidingRelPoses;
		bool     graspFound = false ,
				 collision  = true  ,
				 allCollide = true  ,
				 pathClear  = false ,
				 UPDATEOUTP = false ;
		size_t   numRanked  = 0 , 
				 winDex     = 0 , // Winning index
				 numGrasps  = 0;
		size_t /* ------ */ counter     = 0 , 
							pathLen     = 0 ,
							lastIKindex = 0 ;
	
		if( 1 ){
			
			/// ~~ Check 5 : Grasp Planning and Hand Collision Check ~~
			if( SHOWDEBUG ){  cout << "Generate and Evaluate grasps ..." << endl;  }
			
			// LATER: Recover cached grasps if we are replanning this lift action
			
			// 1. Generate and rank grasps
			std::vector<ParallelGraspState> rankedPairs = 
				//~ generate_and_rank_grasp_pairs( testAsm , 
				generate_and_rank_grasp_pairs( movdAsm , 
											   Eigen::Vector3d::UnitX() , Eigen::Vector3d::UnitY() , Eigen::Vector3d::UnitZ() , 
											   params.distance , params.graspTileSize ,
											   params.preferredWristDir , 
											   //~ params.preferredCenter , 
											   end_straightline_part_pose( result.bestDir ).position , 
											   params.graspDTheta , CRIT_ANG );
			
			
			//  2. Select a collision-free grasp
			graspFound = false ;
			collision  = true  ;
			allCollide = true  ;
			pathClear  = false ;
			UPDATEOUTP = false ;
			numRanked  = rankedPairs.size() ;
			winDex     = 0 ; // Winning index
			numGrasps  = 0 ;
			
			Pose_ASP winPose    = err_pose();
			Pose_ASP relPose    = err_pose();
			ParallelGraspState currGrasp;
			std::vector<size_t> shuffledDices; // Indices of directions to test , Randomized
			
			shuffledDices = vec_copy_shuffled( vec_range( (size_t)0 , (size_t)rankedPairs.size()-1 ) );
			
			//  A. For each grasp in decreasing order
			if( SHOWDEBUG ){  cerr << "There are " << numRanked << " grasps to test ..." << endl;  }
			for( size_t i = 0 ; i < numRanked ; i++ ){
				if( SHOWDEBUG ){  if( i % 1000 == 0 ){  cerr << i << " , " << endl;  UPDATEOUTP = true;  }else{  UPDATEOUTP = false;  }  }
				// 1. Fetch the grasp
				
				//~ currGrasp = rankedPairs[i]; // Evaluate grasps in sorted order
				currGrasp = rankedPairs[ shuffledDices[i] ]; // Evaluate grasps in random order
				
				// 2. Place the collision model according to the grasp state
				handModel.set_finger_state( currGrasp.q_fingers );
				handModel.set_pose( currGrasp.graspTarget );
				// 3. Calc the relative pose
				//~ relPose = currGrasp.graspTarget / strtPose;
				relPose = currGrasp.graspTarget / movdAsm->get_pose();
				
				// 4. Check collision
				//~ collision  = ( refcAsm->collides_with( handModel.LBR4hand ) )  ||  ( floor.collides_with( handModel.LBR4hand ) );
				collision  = ( movdAsm->collides_with( handModel.LBR4hand ) )  ||  ( refcAsm->collides_with( handModel.LBR4hand ) )   
						 ||  ( floor.collides_with( handModel.LBR4hand ) );
				
				if( SHOWDEBUG && UPDATEOUTP ){  cerr << "Initial collision?: " << yesno( collision ) << endl;  }
				// 4.5. If there was not a collision at the start pose
				if( !collision ){
					
					//~ noncollidingGrsps.push_back( currGrasp );
					
					// 5. Check path for collisions
					pathClear = check_part_poses_for_hand( bestMoverPath , relPose );
					if( SHOWDEBUG && UPDATEOUTP ){  cerr << "Path clear?: " << yesno( pathClear ) << endl;  }
				}
				if(  ( !collision )  &&  pathClear  ){
					if( SHOWDEBUG ){  cerr << "Grasp planning succeeded!  Storing winning grasp ..." << endl;  }
					noncollidingGrsps.push_back( currGrasp );
					noncollidingRelPoses.push_back( relPose );
					//~ result.handPath    = gen_poses_for_hand_from_part_seq( bestMoverPath , relPose );
					//~ result.relHandPose = relPose;
					//~ result.bestGrasp   = currGrasp;
					graspFound /* - */ = true;
					//~ break;
				}
			}
			
			// LATER: Cache grasps // noncollidingGrsps
			
			if( !graspFound ){
				diagnosticStr << "ALL_GRASP_COLLID: No non-colliding grasp found for mover " << movd << " and reference " << refc << endl;
				if( SHOWDEBUG ){  cout << diagnosticStr.str() << endl;  }
				result.success = false;
				result.code = ALL_GRASP_COLLID;
				result.desc = diagnosticStr.str();
				clean_asm();
				return result;
			}
		}
		
		numGrasps = noncollidingGrsps.size();
		if( SHOWDEBUG ){  cerr << "There are " << numGrasps << " out of " << numRanked << " valid grasps" << endl;  }
		
		/* ~ At this point, assume:
			 1. A valid lift motions is planned 
			 2. Path of the hand is free from collision 
			 So, all that needs to be done is to run IK for the path of the hand ~ */
		
		
		/// ~~ Check 6 : IK Begin / Path Check ~~
		
		
		if( SHOWDEBUG ){  cerr << "About to calc robot motion ..." << endl 
							   << "There are " << numGrasps << " grasps to test for motion plans" << endl;  }
		
		//~ noncollidingGrsps.push_back( currGrasp );
		//~ noncollidingRelPoses.push_back( relPose );
		
		// 1. For each grasp
		for( size_t j = 0 ; j < numGrasps ; j++ ){
		
			// 2. Retrieve grasp info and load path
			result.relHandPose = noncollidingRelPoses[j];
			result.handPath    = gen_poses_for_hand_from_part_seq( bestMoverPath , result.relHandPose );
			result.bestGrasp   = noncollidingGrsps[j];
		
			// 3. Init vars
			std::vector<double> q1; //- Joint state 1
			std::vector<double> q2; //- Joint state 2
			std::vector<double> q_i; // Joint state iterate
			counter     = 0 , 
			pathLen     = result.handPath.size() ,
			lastIKindex = 0 ;
			
			if( SHOWDEBUG ){  cout << "There are " << pathLen << " hand poses to solve for" << endl
								   << "Fetching first IK solution ... " << endl;  }
			// 2. Fetch IK for first pose
			q1 = err_vec( 7 );
			while(  is_err( q1 )  &&  ( counter < params.IKsearchLimit )  ){
				q1 = IK_clientCall( result.handPath[0] , randrange_vec( -M_PI , M_PI , 7 ) , 1 );
				counter++;
			}
			
			if( is_err( q1 ) ){
				if( SHOWDEBUG ){  cerr << "GRASP_IK_FAILURE: No IK solution found for action with mover " << movd << " and reference " << refc << endl;  }
				graspFound = false;
				continue;
			}else{
				graspFound = true;
				result.qSeq.push_back( q1 );
				lastIKindex = 0;
			}
			
			// 2. For every following pose
			for( size_t i = 1 ; i < pathLen ; i++ ){
				// 3. Try for as many times as we are allowed, using the last soln as a seed
				q2 = IK_clientCall( result.handPath[i] , q1 , params.IKattemptLimit );
				// 4. If we got a soln back
				if( !is_err( q2 ) ){
					// 5. Mark the last successful index
					lastIKindex = i;
					// 6. Store the joint solution
					result.qSeq.push_back( q2 );
					// 7. Set the seed for the next point
					q1 = q2;
					
				// 8. else store a bad soln and deal with it later
				//~ }else{  result.qSeq.push_back( err_vec( 7 ) );  }
				
				// 8. else fall back on random seed
				}else{
					q2 = IK_clientCall( result.handPath[i] , randrange_vec( -M_PI , M_PI , 7 ) , params.IKattemptLimit );
				}
				
				// 9. If we are still failing, break
				if( is_err( q2 ) ){  break;  }
			}
			
			// 9. Determine if the part made it outside of the AABB of the reference
			Pose_ASP savedPose = movdAsm->get_pose();
			// A. Place the mover at the last successful IK
			movdAsm->set_pose( bestMoverPath[ lastIKindex ] );
			// B. Fetch AABB
			Eigen::MatrixXd movdBox = AABB( movdAsm->get_lab_VFN() );
			Eigen::MatrixXd refcBox = AABB( refcAsm->get_lab_VFN() );
			// C. Determine overlap
			result.escapedRefc = !( do_AABBs_intersect( movdBox , refcBox ) );
			// D. Restore pose
			movdAsm->set_pose( savedPose );
			// E. Return error if failed to escape (optional)
			if( !( result.escapedRefc ) ){
				pathClear = false;
			}else{
				pathClear = true;
				break;
			}
		}
		
		if( !graspFound ){
			diagnosticStr << "GRASP_IK_FAILURE: No IK solution found for action with mover " << movd << " and reference " << refc << endl;
			if( SHOWDEBUG ){  cerr << diagnosticStr.str() << endl;  }
			result.success = false;
			result.code = GRASP_IK_FAILURE;
			result.desc = diagnosticStr.str();
			clean_asm();
			return result;
		}
		
		if( !pathClear ){
			diagnosticStr << "IK_PATH_INCOMPLT: Could not find IK solutions to separate " << movd << " and reference " << refc << endl
						  << "Completed " << result.qSeq.size() << " of " << pathLen << " requested poses " << endl;
			if( SHOWDEBUG ){  cerr << diagnosticStr.str() << endl;  }
			result.success = false;
			result.code = IK_PATH_INCOMPLT;
			result.desc = diagnosticStr.str();
			clean_asm();
			return result;
		}
		
		///|////////////////////
		/// FIXME: WORK AREA ///
		///|////////////////////
		
		/// ~~ Check N : Package and Return Plan Data ~~
		
		if( SHOWDEBUG ){  cout << "PASS_STRAIGHTLIN: Straightline removal planned for direction " << result.bestDir << endl;  }
		result.success = true;
		result.code = PASS_STRAIGHTLIN;
		diagnosticStr << "PASS_STRAIGHTLIN: Straightline removal planned for direction " << result.bestDir << endl;
		result.desc = diagnosticStr.str();
		
		// DANGER : DIFFERENT PATHS FOR PHASES 2 AND 3
		result.path     = bestMoverPath;
		if( result.path.size() > 0 ){
			result.initPose = bestMoverPath[0];
			result.finlPose = last_elem( bestMoverPath );
			result.travelDist = ( result.finlPose.position - result.initPose.position ).norm();
			
			if( SHOWDEBUG ){  cout << "First mover pose: ______ " << bestMoverPath[0] << endl
							       << "Last mover pose: _______ " << last_elem( bestMoverPath ) << endl;  }
			
		}
		
		if( SHOWDEBUG ){  cout << "Storing a starting pose: " << result.initPose << endl
							   << "Storing a final    pose: " << result.finlPose << endl;  }
		
		
		if( BREAKPNTS ){  
			if(  ( result.initPose.position(2) < 0.5 )  ||  ( result.finlPose.position(2) < 0.5 )  ){
				cout << "One of the above poses is bad!" << endl;
				waitkey();  
			}
		}
		
		return result;
	}
	
	// N. Clean up and return
	
	return result;

} 

// ~~~ LEVEL PLANNER ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

// === class ActionSpec ===

class ActionSpec{ // Storage container for the minimal representation of an action
	// NOTE: This object has a destructor that erases the assemblies assigned to it
public:
	EIGEN_MAKE_ALIGNED_OPERATOR_NEW
	
	// Con/Destructors
	ActionSpec( Assembly_ASM* pAssembly , SuccessPath pLiftAction , ACTIONS pAction );
	~ActionSpec();
	
	// ~ Members ~
	Assembly_ASM*     assembly; // - Yes
	SuccessPath /*-*/ liftAction; // Data to reconstruct lift action , From the lift planner
	ACTIONS /* --- */ action; // --- Action type
	std::vector<llin> IDvec; // ---- Parts from a failed operation ( optional )
};

// ___ End ActionSpec ___

// Find the support that will allow the most actions from the beginning
IndexMatches best_starting_support_for_level( AsmStateNode_ASP* planRoot , ValidationParams params ); 

template< typename IK >
std::vector<ActionSpec*> tree_plan_exec_one_level( AsmStateNode_ASP* planRoot , 
												   Floor_ASM& floor , IK IK_clientCall , ValidationParams params ,
												   double CRIT_ANG = GEO_CRIT_ANG ){
	// Get all of the robot actions that will reduce the root node to each of its successors
	// NOTE: This function assumes that 'planRoot' has at least one outgoing edge
	
	bool SHOWDEBUG = true  , // if( SHOWDEBUG ){  cout << "" << endl;  }
		 BREAKPNTS = true ; // if( BREAKPNTS ){  waitkey();  }
	
	std::vector<ActionSpec*> rtnActions;
	
	if( SHOWDEBUG ){ sep( "tree_plan_exec_one_level" );
					 cout << "Init ..." << endl;  }
	
	/// ~~ Phase 0: Init ~~
	
	// ~ Tree Plan ~
	Assembly_ASM* /*-*/ rootAsm      = planRoot->assembly; // Fetch the assembly from which subs will be removed
	if( SHOWDEBUG ){  cout << "Did we get a root assembly?: " << yesno( rootAsm ) << endl
						   << "About to replicate ..." << endl;  }
	if( !(rootAsm) ){  return rtnActions;  } // If the plan contained no assembly , Return empty
	
	// ~ Algo Vars ~
	Assembly_ASM* /*-*/ runningAsm  = rootAsm->replicate(); // -------- The assembly will shrink as we work with it // NOTE: Pose copied
	std::vector<llin>   runningIDs  = runningAsm->get_part_ID_vec(); // Part IDs remaining
	size_t /* ------ */ numEdges = planRoot->outgoingEdges.size(); // - Number of edges below the root node
	bool /* -------- */ edgeSuccess = false; // ----------------------- Did the edge succeed?
	std::vector<size_t> edgeDices; // --------------------------------- All of the edge indices
	std::queue<size_t>  execOrder; // --------------------------------- Controls the order in which we execute edges
	std::set<size_t>    plannedEdges; // ------------------------------ Set of edges for which planning is complete
	
	//  A. Shortcut out if there is nothing to plan
	if( numEdges > 0 ){
		//  B. Get a list of edge indices
		edgeDices = vec_range( (size_t)0 , numEdges-1 );
	}else{
		if( SHOWDEBUG ){  cout << "There were no edges!" << endl;  }
		return rtnActions;
	}
	
	// ~~~ LAMBDA FUNCS ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
	
	auto get_R_flag = [&]( AsmStateNode_ASP* edgeNode ){
		return edgeNode->actionData.rotateFlag;
	};
	
	auto set_R_flag = [&]( AsmStateNode_ASP* edgeNode , bool val ){
		edgeNode->actionData.rotateFlag = val;
	};
	
	auto reset_flags = [&](){
		for( size_t k = 0 ; k < numEdges ; k++ ){  set_R_flag( planRoot->outgoingEdges[k] , false );  }
	};
	
	auto append_remove = [&]( Assembly_ASM* currAsm , SuccessPath actInfo ){
		// NOTE: This function assumes that 'nuAsm' has been posed and its support set
		rtnActions.push_back(  new ActionSpec( currAsm , actInfo , ACT_REMOVE )  ); // ActionSpec constructor will take care of replication
	};
	
	auto append_failed = [&]( const std::vector<llin>& edgeParts ){
		// NOTE: This function assumes that 'nuAsm' has been posed and its support set
		ActionSpec* failedAction = new ActionSpec( nullptr , empty_SuccessPath() , ACT_NO_OPR );
		failedAction->IDvec = vec_copy( edgeParts );
		rtnActions.push_back( failedAction );
	};
	
	// ,,, END LAMBDA ,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,
	
	
	// ~ Iteration Vars ~
	size_t /* ---- */ currEdgeDex = 0; // ----- Index of the edge currently being planned
	AsmStateNode_ASP* currEdge    = nullptr; // Edge currently being planned
	Assembly_ASM*     currAsm     = nullptr; // Assembly currently being planned
	bool /* ------ */ currFlag    = false , //- Rotation flag of the inspected node
					  chkPass     = false , //- Flag 1
					  chekTwo     = false ; //- Flag 2  
	size_t /* ---- */ counter     = 0; // ----- Iteration counter
	Eigen::Vector3d   remDir; // -------------- Suggested removal direction
	Assembly_ASM*     tempAsm; // ------------- Temp asm
	std::vector<llin> subPartIDs , // --------- Parts contained in the edge sub
					  currBalanc ; // --------- Remainder parts for the assembly being looked at
	IndexSuccesLookup supportVote; // --------- Info about which support is best for a certain action
	SuccessPath /*-*/ liftPlan; // ------------ Execution info for a single action
	ValidationCode    edgeCode; // ------------ Result from the lift planner
	IndexSearchResult supportMatch; // -------- Result from a support search (from pose)
	
	
	// 1. Situate the assembly on an initial support
	if( 1 ){
		// A. Situate the assembly on support 0
		runningAsm->set_lab_pose_for_index_3D( 0 , params.setdownLoc , true );
		
	}else{ // This branch highlights unstable operations
		
		currEdge    = planRoot->outgoingEdges[ 0 ];
		currAsm     = currEdge->assembly;
		subPartIDs  = currAsm->get_part_ID_vec();
		currBalanc  = runningAsm->get_balance_IDs_from_sub( subPartIDs );
		
		// B. Situate the assembly on the best support for the first action , If the support can be found
		supportVote = runningAsm->suggest_support_for_staightline_stability( subPartIDs , currBalanc , 
																		     params.distance , params.numSamples ); 
		if( supportVote.result ){
			runningAsm->set_lab_pose_for_index_3D( supportVote.bestDex , params.setdownLoc , true );
		}else{
			runningAsm->set_lab_pose_for_index_3D( 0 , params.setdownLoc , true );
		}
	}
	
	// 2. Enqueue
	enqueue_vec( execOrder , edgeDices );
	
	
	/// ~~ Phase 1: Plan Edges ~~
	
	//  3. While there are edges to plan
	while( execOrder.size() > 0 ){
		if( SHOWDEBUG ){  sep( "Iteration " + to_string( counter + 1 ) , 1 );  counter++;  }  
		
		//  4. Pop Edge
		currEdgeDex = queue_get_pop( execOrder );
		currEdge    = planRoot->outgoingEdges[ currEdgeDex ];
		currAsm     = currEdge->assembly;
		subPartIDs  = currAsm->get_part_ID_vec();
		currBalanc  = runningAsm->get_balance_IDs_from_sub( subPartIDs );
		currFlag    = get_R_flag( currEdge );
		
		if( SHOWDEBUG ){  cerr << "\tAbout to plan edge: ____ " << currEdgeDex << endl
							   << "\tValid edge pointer?: ___ " << yesno( currEdge ) << endl
							   << "\tValid assembly pointer?: " << yesno( currAsm ) << endl
							   << "\tMoved IDs: _____________ " << subPartIDs << endl
							   << "\tReference IDs: _________ " << currBalanc << endl
							   << "\tRunning Asm Pose: ______ " << runningAsm->get_pose() << endl
							   << "\tRoot Asm Pose: _________ " << rootAsm->get_pose() << endl
							   << "\tWas flag set?: _________ " << yesno( currFlag ) << endl;  }
							   
							   
		if( BREAKPNTS && false ){  waitkey();  }
		
		//  5. Check that the balance is not zero
		if( currBalanc.size() == 0 ){  
			if( SHOWDEBUG ){  cout << "Balance is zero, Skipping loop ..." << endl;  }
			// A. We don't have to plan this edge if we have already created this state , Mark Planned
			plannedEdges.insert( currEdgeDex );
			// B. There shouldn't be other things , Just let the queue run out
			continue;  
		}
		
		//  6. Check that the current edge has not already been planned , If so skip and let it fall off of the queue
		if( is_arg_in_set( currEdgeDex , plannedEdges ) ){
			continue;
		}
		
		// NOTE: At this time not considering how many times we have replanned an edge
		
		// 7. If the flag was set, must rotate!
		if( currFlag ){
			if( SHOWDEBUG ){  cout << "Flag was set: Rotate" << endl;  }
			//  D. Suggest a support to set the running assembly at
			supportVote = runningAsm->suggest_support_for_staightline_stability( subPartIDs , currBalanc , 
																				 params.distance , params.numSamples ); 
			if( supportVote.result ){
				//  F. Rotate the assembly
				runningAsm->set_lab_pose_for_index_3D( supportVote.bestDex , params.setdownLoc , true );
			}
			
			// DANGER: HACK FOR MOTOR
			//~ Pose_ASP temPose = runningAsm->get_pose();
			//~ runningAsm->set_pose(
				//~ Pose_ASP{ temPose.position , Eigen::AngleAxisd( M_PI / 2.0 , Eigen::Vector3d::UnitZ() ) * temPose.orientation }
			//~ );
			
			//  H. The assembly is in a new orientation and the info we have about each is old , Unset flags
			reset_flags();
			
			// NOTE: For now, not considering whether we have tried this this support before.  Keeping track would add complexity
		}
		
		// 7. Attempt to plan the edge , Assume that the 'runningAsm' has been properly posed
		liftPlan = plan_one_removal_action_multimode( runningAsm , subPartIDs , currBalanc ,
													  floor , params ,
													  IK_clientCall );
		
		// 8. If successful, Store and Mark success
		if( liftPlan.success ){
			// A. Record action
			append_remove( runningAsm , liftPlan );
			// B. Flag planned
			plannedEdges.insert( currEdgeDex );
			// C. Generate a reference assembly
			tempAsm = runningAsm->sub_from_spec( currBalanc );
			// D. Situate the reference to match the stationary running assembly
			
			if( SHOWDEBUG ){  cout << "About to copy the running asm ..." << endl
								   << "Before copy: Running assembly has the parts " << runningAsm->get_part_ID_vec() << endl
								   << "Before copy: Running assembly has the pose  " << runningAsm->get_pose() << endl;  }  
			
			// THIS COULD BE THE SOURCE OF PREVIOUS NONSENSE POSES
			tempAsm->set_pose( runningAsm->get_pose() );
			
			if( SHOWDEBUG ){  cout << "Before copy: Setting the copy pose to _____ " << tempAsm->get_pose() << endl;  }  
			
			supportMatch = tempAsm->get_support_from_current_pose();
			if( !supportMatch.result ){  
				if( SHOWDEBUG ){  cout << "Did not find a support to match the pose!" << endl;  }  
			}else{
				// DANGER: THIS IS SETTING THE ASM BELOW THE TABLE
				if( SHOWDEBUG ){  cout << "Before copy: Found a support that matches:  " << supportMatch.index << endl;  }  
				tempAsm->set_lab_pose_for_index_3D( supportMatch.index , params.setdownLoc , true );
			}
			//~ delif( runningAsm ); // otherwise prev version leaks!
			// E. Update the balance to reflect the removed sub
			runningAsm = tempAsm->replicate();
			
			if( SHOWDEBUG ){  cout << "After copy : Running assembly has the parts " << runningAsm->get_part_ID_vec() << endl
								   << "After copy : Running assembly has the pose  " << runningAsm->get_pose() << endl;  }  
								   
			
			
		}else{
		// 9. If failed, Set rotation flag and Requeue
			if( BREAKPNTS ){  waitkey();  }
			set_R_flag( currEdge , true );
			execOrder.push( currEdgeDex );
		}
	}
	
	std::vector<size_t> badEdges;
	
	// 10. Report on unplanned edges
	if( edgeDices.size() != plannedEdges.size() ){
		badEdges = vec_copy_without_elem( edgeDices , plannedEdges );
		if( SHOWDEBUG ){  cout << "ERROR: UNPLANNED EDGE ACTIONS: " << badEdges << endl;  }
		for( size_t i = 0 ; i < badEdges.size() ; i++ ){
			append_failed( planRoot->outgoingEdges[ badEdges[i] ]->assembly->get_part_ID_vec() );
		}
	}	
	
	if( SHOWDEBUG ){  cout << "Finished planning level with " << rtnActions.size() << " planned actions and " << badEdges 
						   << " unplanned actions." << endl;  }
	
	// N-1. Clean Up
	delif( runningAsm );
	
	// N. Return
	return rtnActions;
}


// ~~~ TREE PLANNER ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

// === class LevelPlanNode ===

class LevelPlanNode{
	// Container for a level plan with pointers to sub-plans
public:
	EIGEN_MAKE_ALIGNED_OPERATOR_NEW
	
	// ~ Con/Destructors ~
	LevelPlanNode( AsmStateNode_ASP* linkedState , size_t pDepth ); // Create a node linked to an assembly state
	void _init( bool pAllPassed , std::vector<ActionSpec*>& pLevelPlan ); // Load precedence level execution data
	~LevelPlanNode(); // erase pointers
	
	// ~ Methods ~
	void /* --- */ add_child( LevelPlanNode* successor ); 
	LevelPlanNode* get_root(); 
	
	// ~ Members ~
	AsmStateNode_ASP* /* --- */ stateLink; //- 
	size_t /* -------------- */ depth; // ---- Depth of this node in the overall plan
	bool /* ---------------- */ allPassed; //- Did all of the actions succeed?
	std::vector<ActionSpec*>    levelPlan; //- Series of actions for this level
	std::vector<LevelPlanNode*> successors; // Deeper plan levels 
	LevelPlanNode* /* ------ */ parent; // --- Pointer to parent, Used to find root
	double /* -------------- */ timeInSec;
};

// ___ End LevelPlanNode ___

bool failed_edges_in_level( const std::vector<ActionSpec*>& levelPlan ); // Scan the level plan for failed actions


template< typename IK >
LevelPlanNode* tree_plan_exec_full_depth( AsmStateNode_ASP* planRoot , Floor_ASM& floor , IK IK_clientCall , 
										  ValidationParams params ,
										  double CRIT_ANG = GEO_CRIT_ANG ){
	bool SHOWDEBUG = true;

	std::queue<LevelPlanNode*> subNodes;
	LevelPlanNode* /* ----- */ currNode  = nullptr ;
	LevelPlanNode* /* ----- */ tempNode  = nullptr ;
	AsmStateNode_ASP* /* -- */ currState = nullptr;
	size_t /* ------------- */ currDepth = 0 ,
							   len       = 0 ;
	std::vector<ActionSpec*>   levelPlan;
	
	// 0. Create a new 'LevelPlanNode'
	currNode = new LevelPlanNode( planRoot , 0 );
	// 1. Enqueue the root node
	subNodes.push( currNode );
	
	// 2. While there are still assemblies in queue
	while( subNodes.size() > 0 ){
		// 3. Retrieve and Pop node
		currNode  = queue_get_pop( subNodes ); // Build on this node
		currState = currNode->stateLink; // ---- Plan on this assembly level
		currDepth = currNode->depth;
		
		// 4. If the asm has more than one part
		if( currState->assembly->get_part_ID_vec().size() > 1 ){
			
			// 5. Plan actions for this precedence level
			levelPlan = tree_plan_exec_one_level( currState , floor , IK_clientCall , 
												  params ,
												  CRIT_ANG );
			
			currNode->_init(  !( failed_edges_in_level( levelPlan ) )  ,  levelPlan  );
			
			len = currState->outgoingEdges.size();
			
			if( len > 0 ){ // This should always be true if the asm clustered had more than one part
				// 6. For every parts list produced
				for( size_t i = 0 ; i < len ; i++ ){
					// 7. If the successor has at least another level to go
					if( currState->outgoingEdges[i]->assembly->get_part_ID_vec().size() > 1 ){
						// 8. Produce a new node linked to the successor to be unpacked and planned later  &&  8. Enqueue 
						tempNode = new LevelPlanNode( currState->outgoingEdges[i] , currDepth+1 );
						currNode->add_child( tempNode );
						subNodes.push( tempNode );
					}
				}
			}
		}
	}
	// N. Return
	return currNode->get_root();
}
												   
/// ___ END EXEC ___________________________________________________________________________________________________________________________


/// === Competing Methods ==================================================================================================================
	
// ~~ Part Interaction Clusters , 2013 Morato ~~

PointsClassfd kMeans_clusters_3D( size_t k , Eigen::MatrixXd points );

std::vector< std::vector< llin > > kMeans_part_clusters_ID( Assembly_ASM* original , size_t k );

std::vector< std::vector< llin > > cluster_one_level_disasm( Assembly_ASM* original , ValidationParams params );

AsmStateNode_ASP* Morato2013Modified_full_depth( Assembly_ASM* original , ValidationParams params );

// ~~ Base Part Fitness & Force Fits , 2017 Belhadj ~~

NumsClassfd kMeans_clusters_1D( size_t k , std::vector<double> points );

bool weights_sum_to_one( BasePartFitness& components );

void normalize_weights( BasePartFitness& components );

double base_fitness_from_components( BasePartFitness& components );

std::map< llin , BasePartFitness > calc_base_fitness_for_parts( Assembly_ASM& assembly , double alpha , double beta , double gamma );

llin ID_w_max_base_fitness( std::map< llin , BasePartFitness >& baseScores );

IDScoreLookup assoc_IDs_scores( std::map< llin , BasePartFitness >& baseScores );

std::vector<std::vector<bool>> dissociate_indices( std::vector<std::vector<bool>>& liaisonG , std::vector<size_t> indices ); // remove edges

double lo_bound_for_fraction_geq( std::vector<double>& unorderedVec , double fraction );

double divider_for_largest_gap( std::vector<double>& unorderedVec );

// Return a list of subassemblies that can be identified at a single level
std::vector< std::vector< llin > > Belhadj2017_one_level( Assembly_ASM* original , 
														  double alpha , double beta , double gamma , 
														  double initFrac , size_t numIter , 
														  ValidationParams params );

AsmStateNode_ASP* Belhadj2017_full_depth( Assembly_ASM* original  , 
										  double alpha , double beta , double gamma , 
										  double initFrac , size_t numIter ,
										  ValidationParams params );

/// ___ End Competing ______________________________________________________________________________________________________________________


/// === DISASSEMBLY INTERFERENCE GRAPH =====================================================================================================

//  Version of DIG calculator that allows the parts that go into the constraint calculation to be determined by the client code
std::vector<TriMeshVFN_ASP> get_intersection_surfaces( Assembly_ASM* assembly , llin projectPartID , const std::vector<llin>& refcID , 
													   double dRadiusInterval , size_t numSteps ,
													   bool interior = true  );
													   
//  Version of the DIG calculator for subs
std::vector<TriMeshVFN_ASP> get_intersection_surfaces( Assembly_ASM* assembly , 
													   const std::vector<llin>& projectPartID , const std::vector<llin>& refcID , 
													   double dRadiusInterval , size_t numSteps ,
													   bool interior = true );

std::vector<TriMeshVFN_ASP> get_intersection_surfaces( Assembly_ASM* assembly , llin projectPartID , 
													   double dRadiusInterval , size_t numSteps ,
													   bool interior = true  );
													   
std::vector<CollisionVFN_ASP*> prep_meshes_for_collision( const std::vector<TriMeshVFN_ASP>& meshSeq );

double collision_fraction( CollisionVFN_ASP* shell , Part_ASM* part ); // Return the fraction of 'CollisionVFN_ASP' that intersects 'part'
double collision_fraction( CollisionVFN_ASP* shell , const TriMeshVFN_ASP& mesh ); // Return the fraction of 'CollisionVFN_ASP' that intersects 'mesh'

std::vector<std::vector<double>> DIG_from_asm_V1( Assembly_ASM* assembly , double dRadiusInterval , size_t numSteps );

std::vector<std::vector<double>> DIG_from_asm_V2( Assembly_ASM* assembly , double dRadiusInterval , size_t numSteps , 
												  double stuckInfluenceFactor );
	
/* Disassembly Interference Graph V3: Every mover vs every obstruction considering limited constraints for shell computation */
std::vector<std::vector<double>> DIG_from_asm_V3( Assembly_ASM* assembly , double dRadiusInterval , size_t numSteps ,
												  const std::vector<llin>& limitedRefc ,
												  double stuckInfluenceFactor );

double assembled_NRG_at_index_from_DIG( size_t j , const std::vector<std::vector<double>>& DIG ); // NRG --> Part

double assembled_NRG_at_indices_from_DIG( const std::vector<size_t>& jDices , const std::vector<std::vector<double>>& DIG ); // NRG --> Part

double donated_NRG_from_index_from_DIG( size_t i , const std::vector<std::vector<double>>& DIG ); // NRG <-- Part 

double donated_NRG_from_indices_from_DIG( const std::vector<size_t>& iDices , const std::vector<std::vector<double>>& DIG ); // NRG <-- Part 

double NRG_donated_by_i_to_j( size_t i , size_t j , const std::vector<std::vector<double>>& DIG );

double NRG_receivd_by_j_from_i( size_t j , size_t i , const std::vector<std::vector<double>>& DIG );


enum NRGTYPE{ V1 , V2 };

double max_blocking_fraction_sub1_vs_sub2( Assembly_ASM* assembly , // Create projection of one sub and collide it with another
										   const std::vector<llin>& sub1 , const std::vector<llin>& sub2 , // sub1 moves "against" sub2
										   const std::vector<llin>& refcID , // Reference parts for determining freedom
										   double dRadiusInterval , size_t numSteps );

// Blocking calculator: Return a mapping (pair vector) from all parts not in the sub to the fraction that each blocks the sub
std::vector<std::pair<llin,double>> sub_vs_parts_blockage_map( Assembly_ASM* assembly , // Create projection of one sub and collide it with another
															   const std::vector<llin>& sub , const std::vector<llin>& opponents , // sub moves "against" opponents
															   const std::vector<llin>& refcID , // Reference parts for determining freedom
															   double dRadiusInterval , size_t numSteps );

double sum_blockage_across_parts( const std::vector<std::pair<llin,double>>& IDblockagePairs );

double sum_blockage_across_parts_excluding( const std::vector<std::pair<llin,double>>& IDblockagePairs , const std::vector<llin>& excluded );

std::vector<std::vector<llin>> sub_ID_blockage_reduction_one_level( Assembly_ASM* original , 
																	double alpha , double beta , double gamma ,
																	double dRadiusInterval , size_t numSteps ,
																	double stuckInfluenceFactor , 
																	double blockThresh , 
																	ValidationParams params  );
																	
AsmStateNode_ASP* sub_ID_blockage_reduction_full_depth( Assembly_ASM* original , 
														double alpha , double beta , double gamma ,
														double dRadiusInterval , size_t numSteps ,
														double stuckInfluenceFactor , 
														double blockThresh , 
														ValidationParams params  );

/// ___ END DIG ____________________________________________________________________________________________________________________________


/// === VISUALIZATION ======================================================================================================================

void visualize_search_plan( AsmSearchResult& searchResult , RViz_MarkerManager& mrkrMngr , 
							Eigen::Vector2d displayOrigin , Eigen::Vector2d offsetPerAction );
							
void visualize_Morato2013_Level_1( RViz_MarkerManager& mrkrMngr , 
								   Eigen::Vector2d displayOrigin , Eigen::Vector2d offsetPerLayer , Eigen::Vector2d offsetPerSub , 
								   Assembly_ASM* original , std::vector< std::vector< llin > > clusterVector ); // Modified

// 2018-05-23 , RENAMED: This will break Morato tests
void visualize_tree_structure_plan( RViz_MarkerManager& mrkrMngr , 
									Eigen::Vector2d displayOrigin , Eigen::Vector2d offsetPerLayer , Eigen::Vector2d offsetPerSub , 
									AsmStateNode_ASP* rootNode );

struct ShellVizMap{
	std::vector<std::pair<llin,double>> mapping;
	std::vector<TriMeshVFN_ASP> /* - */ subShells;
};

ShellVizMap sub_vs_parts_blockage_viz( Assembly_ASM* assembly , // Create projection of one sub and collide it with another
									   const std::vector<llin>& sub , const std::vector<llin>& opponents , // sub moves "against" opponents
									   const std::vector<llin>& refcID , // Reference parts for determining freedom
									   double dRadiusInterval , size_t numSteps );

void load_shells_into_mrkrMngr( const std::vector<TriMeshVFN_ASP>& shells , RViz_MarkerManager& mrkrMngr );

/// ___ END VIZ ____________________________________________________________________________________________________________________________

// ___ End Func ____________________________________________________________________________________________________________________________


#endif

/* === Spare Parts =========================================================================================================================



   ___ End Parts ___________________________________________________________________________________________________________________________

*/

