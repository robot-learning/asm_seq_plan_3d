#pragma once // This also helps things not to be loaded twice , but not always . See below

/***********  
Model_Factory.h
James Watson , 2018 June
Load ASP models with an optional subset of parts

Template Version: 2018-06-06
***********/

#ifndef MODEL_FACTORY_H // This pattern is to prevent symbols to be loaded multiple times
#define MODEL_FACTORY_H // from multiple imports

// ~~ Includes ~~
// ~ Eigen ~
#include <Eigen/Core> // ---- The living heart of Eigen
#include <Eigen/StdVector> // This is REQUIRED to put an Eigen matrix in an std::vector // Dont do that , Use Eigen::MatrixXd and .row(i)
#include <Eigen/Dense> // --- Cross Product , etc.
#include <Eigen/Geometry> //- Quaternion , etc
// ~ ROS : Transforms & RVIZ ~
#include <ros/ros.h> // ---------------------- ROS presides over all
#include <ros/package.h> // ------------------ Where are we? // http://wiki.ros.org/Packages#C.2B-.2B-
#include <visualization_msgs/Marker.h> // ---- Display geometry
#include <visualization_msgs/MarkerArray.h> // Marker Array , Gets rid of the flicker problem
// ~ Local ~
#include <Cpp_Helpers.h> // Utilities and Shortcuts
#include <MathGeo_ASP.h> // Math and geometry utilities
#include <ASP_3D.h> // ---- Assembly model classes


// ~~ Shortcuts and Aliases ~~



// === Assemblies ==========================================================================================================================

// ~ Simple Phone ~
Assembly_ASM* simple_phone();
Assembly_ASM* simple_phone( std::vector<llin>& partIDs );

// ~ Simple Cube ~
Assembly_ASM* simple_cube();

// ~ Simple Cube Obstructed ~
Assembly_ASM* simple_cube_obstr();

// ~ Drive Motor, Wang ~
Assembly_ASM* drive_motor( std::vector<llin>& partIDs );
Assembly_ASM* drive_motor();

// ~ Validator, Kheder ~
Assembly_ASM* validator( std::vector<llin>& partIDs );
Assembly_ASM* validator();

// ~ Toy Plane, Lee ~
Assembly_ASM* toy_plane( std::vector<llin>& partIDs );
Assembly_ASM* toy_plane();

// ___ End Assembly ________________________________________________________________________________________________________________________



#endif

/* === Spare Parts =========================================================================================================================



   ___ End Parts ___________________________________________________________________________________________________________________________

*/
