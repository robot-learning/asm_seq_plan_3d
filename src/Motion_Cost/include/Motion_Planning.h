#pragma once // This also helps things not to be loaded twice , but not always . See below

/***********  
Motion_Planning.h
James Watson , 2018 June
Grasp Selection , Grasp Planning , and Arm Motion

Template Version: 2018-06-06
***********/

#ifndef MOTION_PLAN_ASM_H // This pattern is to prevent symbols to be loaded multiple times
#define MOTION_PLAN_ASM_H // from multiple imports

// ~~ Includes ~~
// ~ Eigen ~
#include <Eigen/Core> // ---- The living heart of Eigen
#include <Eigen/StdVector> // This is REQUIRED to put an Eigen matrix in an std::vector
#include <Eigen/Dense> // --- Cross Product , etc.
#include <Eigen/Geometry> //- Quaternion , etc
// ~ ROS ~
#include <ros/ros.h> // -------------- ROS , Publishers , Subscribers
#include <ros/package.h> // ---------- Where are we? // http://wiki.ros.org/Packages#C.2B-.2B-
#include <sensor_msgs/JointState.h> // JointState , For plan replayer
#include <geometry_msgs/Pose.h> // --- Geometry Pose , For plan replayer
#include <ll4ma_teleop/Joints2Jac.h>
#include <ll4ma_teleop/LinkPoses.h>
#include <ll4ma_teleop/FngrPoses.h>
#include <motion_cost/qFromPose.h>

// ~ Local ~
#include <Cpp_Helpers.h> // C++ Utilities and Shortcuts
#include <ROS_Helpers.h> // ROS Utilities and Shortcuts
#include <ASP_3D.h> // ---- Assembly Geometry
#include <comm_ASM.h>
//~ #include <AsmSeqPlan.h> //- Planning



// ~~ Shortcuts and Aliases ~~

/// === ARM SERVICE WRAPPERS ===============================================================================================================

struct request_IK_solution{
	ros::ServiceClient& IKclient;
	std::vector<double> operator()( Pose_ASP X_desired , const std::vector<double>& jointSeed , size_t attemptLimit = 25 );
};

struct request_Jacobian{
	ros::ServiceClient& Jclient;
	Eigen::MatrixXd operator()( const std::vector<double>& q_joints7 );
};

struct request_FK_poses{
	ros::ServiceClient& FKclient;
	std::vector<Pose_ASP> operator()( const std::vector<double>& q_joints7 );
};

struct request_FNG_poses{
	ros::ServiceClient& FNGclient;
	std::vector<Pose_ASP> operator()( const std::vector<double>& q_joints9 );
};

/// ___ END SERVICES _______________________________________________________________________________________________________________________


/// === GRASP PLANNING FUNCTIONS ===========================================================================================================

struct priorityQ_less_than_ParGrasp{ // Compare the costs of two 'ParallelGraspState's
	bool operator()( const std::pair<double,ParallelGraspState>& op1 , const std::pair<double,ParallelGraspState>& op2 ) const;
};

struct priorityQ_greater_than_ParGrasp{ // Compare the costs of two 'ParallelGraspState's
	bool operator()( const std::pair<double,ParallelGraspState>& op1 , const std::pair<double,ParallelGraspState>& op2 ) const;
};

// SILLY VERSION , OLD
std::vector<ParallelGraspState> rank_grasp_pairs( Assembly_ASM* targetAsm , 
												  Eigen::Vector3d approachFrom , Eigen::Vector3d robotBase , Eigen::Vector3d workCenter , 
												  Eigen::MatrixXd approaches , double tileSize );

// BETTER VERSION , NEW						  
std::vector<ParallelGraspState> rank_grasp_pairs( const RayHits& allPairs , 
												  const Eigen::Vector3d& preferredWristDir , const Eigen::Vector3d& preferredCenter , 
												  double dTheta , double CRIT_ANG = GEO_CRIT_ANG );

// ~~ Manipulation Planning ~~

// COMPONENT
RayHits get_opposite_grasp_pairs_for_ASM_and_direction( Assembly_ASM* assembly , 
														const Eigen::Vector3d& direction , const Eigen::Vector3d& xDir , double distance ,
														double tileSize );

// COMPLETE
RayHits tile_ASM_grasps_orthogonal( Assembly_ASM* assembly , 
									const Eigen::Vector3d& xBasis , const Eigen::Vector3d& yBasis , const Eigen::Vector3d& zBasis , 
									double distance , double tileSize );
									
// COMBINED
std::vector<ParallelGraspState> generate_and_rank_grasp_pairs( Assembly_ASM* assembly , 
															   const Eigen::Vector3d& xBasis , const Eigen::Vector3d& yBasis , const Eigen::Vector3d& zBasis , 
															   double distance , double tileSize ,
															   const Eigen::Vector3d& preferredWristDir , const Eigen::Vector3d& preferredCenter , 
															   double dTheta , double CRIT_ANG = GEO_CRIT_ANG );

/// ___ END GRASP FUNC _____________________________________________________________________________________________________________________


/// === ARM IK AND MOTION ==================================================================================================================

template < typename F > 
std::vector<sensor_msgs::JointState> arm_motion_for_trajectory( Assembly_ASM* liftAsm , std::vector<Pose_ASP>& sequence ,
																Eigen::Vector3d approachFrom , Eigen::Vector3d robotBase , Eigen::Vector3d workCenter , 
																Eigen::MatrixXd approaches , double tileSize , F serviceCallFunc ){
	// Return a sequence of joint states that will exectute the given trajectory
	
	bool SHOWDEBUG = false; // if( SHOWDEBUG ){  cout << "" << endl;  }
	
	//  1. Generate candidate grasps for the assembly to be lifted
	std::vector<ParallelGraspState> rankedPairs = rank_grasp_pairs( liftAsm , 
																	approachFrom , robotBase , workCenter , 
																	approaches , tileSize );
	size_t numGrasps = rankedPairs.size();																
	if( SHOWDEBUG ){  cout << "Obtained " << numGrasps << " ranked grasp pairs" << endl;  }
																	
	std::vector<sensor_msgs::JointState> rtnStates;
	Pose_ASP relativeTarget;
	Pose_ASP /* ---- */ bgnTrgt;   Pose_ASP /* ---- */ endTrgt;
	std::vector<double> bgnSoln;   std::vector<double> endSoln;
	Pose_ASP /* ---- */ currTrgt;
	std::vector<double> currSoln;
	ParallelGraspState currGrasp;
	std::vector<double> jointSeed = { 0 , 0 , 0 , 0 , 0 , 0 , 0 };
	
	size_t numPoses  = sequence.size();
	size_t attemptLimit = 25;
	//  2. For ranked grasp
	for( size_t i = 0 ; i < numGrasps ; i ++ ){
		if( SHOWDEBUG ){  cout << "Grasp " << i+1 << " of " << numGrasps << endl;  }
		currGrasp = rankedPairs[i];
		//  3. Calc relative pose
		relativeTarget = currGrasp.graspTarget / sequence[0];
		//  4. Fetch the beginning and ending part poses
		bgnTrgt = sequence[0] * relativeTarget;
		endTrgt = sequence[ last_index( sequence ) ] * relativeTarget;
		//  5. Determine IK feasibility for the beginning and ending grasps
		bgnSoln = serviceCallFunc( bgnTrgt , jointSeed , attemptLimit );
		endSoln = serviceCallFunc( endTrgt , jointSeed , attemptLimit );
		//  6. If both feasible
		if(  !is_err( bgnSoln )  &&  !is_err( endSoln )  ){
			if( SHOWDEBUG ){  cout << "Beginning and end are feasible! Calculating joint states for trajectory ..." << endl;  }
			//  7. For every pose in the trajectory
			for( size_t j = 0 ; j < numPoses ; j++ ){
				if( SHOWDEBUG ){  cout << "Pose " << j+1 << " of " << numPoses << endl;  }
				//  8. Calc absolute grasp target
				currTrgt = sequence[j] * relativeTarget;
				//  9. Get IK solution
				currSoln = serviceCallFunc( currTrgt , jointSeed , attemptLimit );
				// 10. Create a joint state
				sensor_msgs::JointState temp;
				//  A. Use this joint state, whether it is valid or not
				pack_joint_and_gripper_states_into_msg( currSoln , //- Joint positions of arm
														rankedPairs[i].q_fingers , // Joint positions of gripper
											            temp ); // Load state data here
				// 11. Pack joint state into vector
				rtnStates.push_back( temp );
			}
			break;
		}else if( SHOWDEBUG ){  cout << "Beginning or end INfeasible! Skipping to next grasp ..." << endl;  }
	}
	// 12. Return vector
	return rtnStates;
}

/// ___ END ARM MOTION _____________________________________________________________________________________________________________________


/// === COLLISION WRAPPERS =================================================================================================================

struct DummyCollision{
	bool operator()( const Pose_ASP& pose );
};

struct AsmCollision{
	Assembly_ASM* movd;
	Assembly_ASM* refc;
	bool operator()( const Pose_ASP& pose );
};

struct hand_collision_model; // Forward declaration , See below

struct HandAsmCollision{
	Assembly_ASM* /* - */ movd;
	Assembly_ASM* /* - */ refc;
	hand_collision_model* handModel;
	Pose_ASP /* ------ */ relHandPose;
	bool operator()( const Pose_ASP& pose );
};

struct HandAsmFlrCollision{
	Assembly_ASM* /* - */ movd;
	Assembly_ASM* /* - */ refc;
	hand_collision_model* handModel;
	Pose_ASP /* ------ */ relHandPose;
	Floor_ASM* /* ---- */ floor;
	bool operator()( const Pose_ASP& pose );
};

/// ___ END WRAPPERS _______________________________________________________________________________________________________________________


/// === ARM COLLISION GEOMETRY =============================================================================================================

// === struct LBR4_collision_model ===

template< typename FKA , typename FNG >
struct LBR4_collision_model{
	EIGEN_MAKE_ALIGNED_OPERATOR_NEW
	
	FKA& armFrameCall;
	FNG& fngFrameCall;
	PartsCollection LBR4arm;
	
	//~ void init( FKA& pArmFrameCall , FNG& pFngFrameCall , llin partIDoffset = 1000 ){
	void init( llin partIDoffset = 1000 ){
		
		bool SHOWDEBUG = false; // if( SHOWDEBUG ){  cout << "" << endl;  }
		
		if( SHOWDEBUG ){  cout << "Assign services ..." << endl;  }
		
		Incrementer assign_part_ID{ partIDoffset };
		
		if( SHOWDEBUG ){  cout << "About to load arm parts ..." << endl;  }
		
		string robotURDFpkg = ros::package::getPath( "ll4ma_robots_description" );
		
		for( size_t i = 1 ; i <= 7 ; i++ ){
			string fullPath = robotURDFpkg + "/meshes/lbr4/link" + to_string( i ) + ".stl";
			if( SHOWDEBUG ){  cout << "Adding part " << fullPath << " ...";  }
			LBR4arm.add_part_ptr( new Part_ASM( assign_part_ID() , fullPath ) );
			if( SHOWDEBUG ){  cout << "Success!" << endl;  }
		}
		
		if( SHOWDEBUG ){  cout << "Arm parts loaded!" << endl;  }
		
		cout << "About to offset arm ..." << endl;
		// ~ Arm Links ~
		Pose_ASP link1pose{  Eigen::Vector3d( 0.0 ,  0.023 ,  0.123 )  ,  RPY_to_Quat( 0.0 , 0.0 , M_PI )  };  LBR4arm.set_offset(  0 , link1pose );
		Pose_ASP link2pose{  Eigen::Vector3d( 0.0 , -0.023 ,  0.080 )  ,  RPY_to_Quat( 0.0 , 0.0 , M_PI )  };  LBR4arm.set_offset(  1 , link2pose );
		Pose_ASP link3pose{  Eigen::Vector3d( 0.0 , -0.023 ,  0.123 )  ,  RPY_to_Quat( 0.0 , 0.0 , M_PI )  };  LBR4arm.set_offset(  2 , link3pose );
		Pose_ASP link4pose{  Eigen::Vector3d( 0.0 ,  0.023 ,  0.080 )  ,  RPY_to_Quat( 0.0 , 0.0 , M_PI )  };  LBR4arm.set_offset(  3 , link4pose );
		Pose_ASP link5pose{  Eigen::Vector3d( 0.0 ,  0.023 ,  0.085 )  ,  RPY_to_Quat( 0.0 , 0.0 , M_PI )  };  LBR4arm.set_offset(  4 , link5pose );
		Pose_ASP link6pose{  Eigen::Vector3d( 0.0 , -0.005 , -0.002 )  ,  RPY_to_Quat( 0.0 , 0.0 , M_PI )  };  LBR4arm.set_offset(  5 , link6pose );
		Pose_ASP link7pose{  Eigen::Vector3d( 0.0 ,  0.000 , -0.015 )  ,  RPY_to_Quat( 0.0 , 0.0 , M_PI )  };  LBR4arm.set_offset(  6 , link7pose );
		
		if( SHOWDEBUG ){  cout << "About to load hand parts ..." << endl;  }
		
		string robotPartPkg = ros::package::getPath( "motion_cost" );
		
		std::vector<string> baxterNames = { "baxterPalm_collsn.stl" , // Palm
											"baxterFing_collsn.stl" , // Right Finger
											"baxterFPad_collsn.stl" ,
											"baxterFing_collsn.stl" , // Left Finger
											"baxterFPad_collsn.stl" };
		for( size_t i = 0 ; i < baxterNames.size() ; i++ ){
			string fullPath = ( robotPartPkg ) + "/Resources/Assembly_CAD/BxtrHandCollsn/" + ( baxterNames[i] );
			if( SHOWDEBUG ){  cout << "Adding part " << fullPath << " ...";  }
			LBR4arm.add_part_ptr( new Part_ASM( assign_part_ID() , fullPath ) );
			if( SHOWDEBUG ){  cout << "Success!" << endl;  }
			if( SHOWDEBUG ){  cout << "Loaded " << LBR4arm.size() << " parts" << endl;  }
		}
	
		if( SHOWDEBUG ){  cout << "Hand parts loaded!" << endl
							   << "Loaded " << LBR4arm.size() << " parts" << endl;  }
							   
		cout << "About to offset gripper ..." << endl;
		
		double RFnAdjY = -0.0025 ,
			   tipAdjY =  0.0025 ,
			   fingerZ = -0.050 - 0.01  ,
			   fngrTpZ = -0.015 - 0.001  ,
			   fingrRY =  0.023 + RFnAdjY ,
			   fngRTpY =  0.018 + RFnAdjY + tipAdjY ,
			   fingrLY =  0.000 ,
			   fngLTpY = -0.005 + tipAdjY ;
		
		// ~ Baxter Collision ~
		Pose_ASP baxPalmPose{  Eigen::Vector3d( -0.005 ,  0.000  ,  0.050  )  ,  RPY_to_Quat( M_PI/2.0 , 0.0 , 0.0 )  };
		LBR4arm.set_offset(  7 , baxPalmPose );
		Pose_ASP baxFngRPose{  Eigen::Vector3d(  0.000 , fingrRY , fingerZ )  ,  RPY_to_Quat( M_PI/2.0 , 0.0 , M_PI/2.0 )  };
		LBR4arm.set_offset(  8 , baxFngRPose );
		Pose_ASP baxFnRTPose{  Eigen::Vector3d(  0.000 , fngRTpY , fngrTpZ )  ,  no_turn_quat()  };
		LBR4arm.set_offset(  9 , baxFnRTPose );
		Pose_ASP baxFngLPose{  Eigen::Vector3d(  0.000 , fingrLY , fingerZ )  ,  RPY_to_Quat( M_PI/2.0 , 0.0 , M_PI/2.0 )  }; 
		LBR4arm.set_offset( 10 , baxFngLPose );
		Pose_ASP baxFnLTPose{  Eigen::Vector3d(  0.000 , fngLTpY , fngrTpZ )  ,  no_turn_quat()  };
		LBR4arm.set_offset( 11 , baxFnLTPose );
		
		if( SHOWDEBUG ){  cout << "INIT COMPLETE!" << endl;  }
	}
	
	void set_poses_from_joints9( const std::vector<double>& jntPos , const std::vector<double>& fngPos ){
		
		bool SHOWDEBUG = true; // if( SHOWDEBUG ){  cout << "" << endl;  }
		
		if( SHOWDEBUG ){  cout << "Setting joints to:_ " << jntPos << endl 
							   << "Setting fingers to: " << fngPos << endl;  }
							   
		// 2. Request poses
		
		if( SHOWDEBUG ){  cout << "About to request link poses for " << jntPos << endl;  }
		std::vector<Pose_ASP> linkPoses = armFrameCall(  jntPos  );
		
		if( SHOWDEBUG ){  cout << "About to request finger poses for " << fngPos << endl;  }
		std::vector<Pose_ASP> fngrPoses = fngFrameCall(  vec_join( jntPos , fngPos )  );
		
		if( SHOWDEBUG ){  
			cout << "Got the following " << fngrPoses.size() << " finger poses: " << endl;
			for( size_t j = 0 ; j < fngrPoses.size() ; j++ ){
				cout << fngrPoses[j] << endl;
			}
		}
		
		LBR4arm.set_pose( linkPoses , 0 , 6 );
		// Fingers
		LBR4arm.set_pose( linkPoses[6] ,  7 );
		LBR4arm.set_pose( fngrPoses[0] ,  8 );
		LBR4arm.set_pose( fngrPoses[0] ,  9 );
		
		double spread = fngPos[0] + fngPos[1];
		
		Pose_ASP hackLeftFing{  
			linkPoses[7].orientation * Eigen::Vector3d( -spread , 0 , 0 ) + fngrPoses[0].position ,
			linkPoses[7].orientation * RPY_to_Quat( 0.0 , M_PI , -M_PI/2.0 )
		};
		if( SHOWDEBUG ){  cout << "Hacked Left Finger Pose: " << hackLeftFing << endl;  }
		
		LBR4arm.set_pose( hackLeftFing , 10 );
		LBR4arm.set_pose( hackLeftFing , 11 );
		
	}
	
	Pose_ASP get_pose( size_t index ){  return LBR4arm.get_pose( index );  }
};

// ___ End LBR4_collision_model ___


// === struct hand_collision_model ===

struct hand_collision_model{
    EIGEN_MAKE_ALIGNED_OPERATOR_NEW
    // ~ Members ~
    PartsCollection LBR4hand;
    double R_FngrState ,
           L_FngrState ;
    Pose_ASP R_FngrOffset;
    Pose_ASP L_FngrOffset;
    Pose_ASP absPose;
    // ~ Methods ~
    void     init( llin partIDoffset = 2000 );
    void     set_finger_state( const std::vector<double>& states );
    Pose_ASP get_pose();
    void     set_pose( const Pose_ASP& nuPose );
    void     load_part_mrkrs_into_mngr( RViz_MarkerManager& mrkrMngr ); // Load part markers into marker manager
};

// ___ End hand_collision_model ___

/// ___ END COLLISION GEO __________________________________________________________________________________________________________________


/// === RRT Removal Path Planning ==========================================================================================================

double dot( const Eigen::Quaterniond op1 , const Eigen::Quaterniond op2 ); // Compute the inner product of the quaternion elements

double distance_between( const Eigen::Vector3d op1 , Eigen::Vector3d op2 , double w_posn );

double distance_between( const Eigen::Quaterniond op1 , Eigen::Quaterniond op2 , double w_ornt );

double distance_between( const Pose_ASP& op1 , const Pose_ASP& op2 , double w_posn , double w_ornt ); // Compute the weighted distance between poses

// Return the pose that is as close to 1*epsilon as possible away from 'opFrom' towards 'opTo'
Pose_ASP one_step_from_to( const Pose_ASP& opFrom , const Pose_ASP& opTo , double w_posn , double w_ornt , double stepMag );

// Sample on a compass around 'nearbyValid'
std::vector<Pose_ASP> all_compass_samples( const Pose_ASP& nearbyValid , double w_posn , double w_ornt , double stepMag );

// === class NodePose ===

class NodePose{
public:
	EIGEN_MAKE_ALIGNED_OPERATOR_NEW
	
	NodePose( const Pose_ASP& nodeState );
	~NodePose(); // Delete this node and all successors
	
	// ~~~ Methods ~~~
	void add_child( NodePose* nuChild );
	void remove_child_by_ptr( NodePose* nodePtr );
	void reassign_parent( NodePose* nuParent , double w_posn , double w_ornt );
	void set_state( const Pose_ASP& nodeState );
	// ~ Paths ~
	std::vector<Pose_ASP> path_to_node();
	std::vector<std::pair<Pose_ASP,Pose_ASP>> get_distal_edges();
	
	// ~~~ Members ~~~
	Pose_ASP /* ----- */ state;
	NodePose* /* ---- */ parent = nullptr;
	std::list<NodePose*> edges;
	double /* ------- */ cost_2_parent = 0.0; // The root node must start with 0 cost or it will be rewired!
	double /* ------- */ cost_2_root   = 0.0;
	bool /* --------- */ isRoot = false;
	bool /* --------- */ compassFlag = false;
};

// ___ End NodePose ___


// === class TreePose ===

class TreePose{ // Bookkeeping for a single tree
	// 2018-06-28: Functions that require collision detection will be offloaded onto the solver, This class is for tree node accounding
public:
	EIGEN_MAKE_ALIGNED_OPERATOR_NEW
	
	// ~~~ Methods ~~~
	
	// ~ Con/Destructors ~
	TreePose( const Pose_ASP& q_root );
	TreePose( const TreePose& other );
	~TreePose();
	
	// ~ Bookkeeping ~
	size_t size();
	
	// ~ Add Nodes ~
	void      add_node( NodePose* nuNode , NodePose* parent , double w_posn , double w_ornt );
	NodePose* add_by_state( const Pose_ASP& q , NodePose* parent , double w_posn , double w_ornt ); // Create a new TreeNode at state 'q' and assign it 'parent'
	NodePose* add_state_seq( const std::vector<Pose_ASP>& qVec , NodePose* parent , double w_posn , double w_ornt );
	
	// ~ Nearest Neighbor ~
	std::pair<NodePose*,double> /* ------ */ find_nearest( const Pose_ASP& q_query , double w_posn , double w_ornt );
	std::pair<NodePose*,double> /* ------ */ find_nearest_unmarked( const Pose_ASP& q_query , double w_posn , double w_ornt );
	std::vector<std::pair<NodePose*,double>> get_neighborhood( const Pose_ASP& q_query , double radius , size_t NshortCut , 
															   double w_posn , double w_ornt );
															   
	// ~ Display ~
	std::vector<std::pair<Pose_ASP,Pose_ASP>> get_distal_edges(); // Return a list of all edges distal to this node
	
	// ~~~ Members ~~~
	NodePose* /* ---- */ rootNode;
	std::list<NodePose*> allNodes;
};

// ___ End TreePose ___


// === class RRT_Solver ====================================================================================================================

template< typename T >
class RRT_Solver{
public:
	EIGEN_MAKE_ALIGNED_OPERATOR_NEW

	// ~~~ Methods ~~~
	
	RRT_Solver( size_t pK /*, size_t pN*/ , double pEpsilon , double pBiasProb , const Eigen::Matrix<double, 2, 3>& pXbounds , 
			    T chkCollsnFnc ,
			    const Pose_ASP& pStart , const Pose_ASP& pGoal , double pWeightPosn , double pWeightOrnt ){
				   
		bool SHOWDEBUG = false; // if( SHOWDEBUG ){  cout << "" << endl;  }
				   
		// ~ Set Parameters ~
		K /* ------- */ = pK;
		epsilon /* - */ = pEpsilon;
		biasProb /*- */ = pBiasProb;
		
		Xbounds /* - */ = pXbounds;
		
		if( SHOWDEBUG ){  cout << "Init with position bounds:" << endl << Xbounds << endl;  }
		
		is_in_collision = chkCollsnFnc;
		
		// DANGER: COPY POSE
		start /* --- */ = copy_pose( pStart );
		goal /* ---- */ = copy_pose( pGoal );
		//~ start /* --- */ = pStart;
		//~ goal /* ---- */ = pGoal;
		
		weightPosn      = pWeightPosn;
		weightOrnt      = pWeightOrnt;
		// ~ Default Parameters ~
		rewireMargin    = 0.025; // Default Margin: 20%
	}
	
	
	Pose_ASP sample_random_state( bool biasEnable = true ){
		// Sample a random state from the task space that is collision-free
		
		bool SHOWDEBUG = false; // if( SHOWDEBUG ){  cout << "" << endl;  }
		
		if(  biasEnable  &&  dice_roll( biasProb )  ){
			
			if( SHOWDEBUG ){  cout << "Biased Step ..." << endl;  }
			
			// DANGER: COPY POSE
			//~ return copy_pose( goal );
			return goal;
			
		}else{
			
			if( SHOWDEBUG ){  cout << "Random Step ..." << endl;
							  cout << "Sample from bounds:" << endl << Xbounds << endl; }
			
			Eigen::Vector3d    rtnPos = sample_from_AABB( Xbounds );
			if( SHOWDEBUG ){  cout << "Sample Position: __ " << rtnPos << endl;  }
			Eigen::Quaterniond rtnOrn = unif_rand_quat(); 
			if( SHOWDEBUG ){  cout << "Sample Orientation: " << rtnOrn << endl;  }
			Pose_ASP tempState = Pose_ASP{ rtnPos , rtnOrn };
			
			
			if( SHOWDEBUG ){  cout << "Sampled: " << tempState << endl;  }
			while( is_in_collision( tempState ) ){  
				tempState = Pose_ASP{ sample_from_AABB( Xbounds ) , unif_rand_quat() };  
				if( SHOWDEBUG ){  cout << "Sampled: " << tempState << endl;  }
			}
			return tempState;
		}
	};
	
	
	std::vector<Pose_ASP> local_planner( Pose_ASP& q_bgn , Pose_ASP& q_end , bool incldBgn = false , bool incldEnd = true ){
		
		bool SHOWDEBUG = false; // if( SHOWDEBUG ){  cout << "" << endl;  }
		
		if( SHOWDEBUG ){  
			sep( "local_planner" , 3 );  
			cout << "From: " << q_bgn << " , Include?: " << incldBgn << endl
				 << "To: _ " << q_end << " , Include?: " << incldEnd << endl;
		}
		if( SHOWDEBUG ){  cout << "Init Vars ..." << endl;  }
		std::vector<Pose_ASP> retrnVec;
		std::vector<Pose_ASP> emptyVec;
		Pose_ASP nextPose;
		
		if( incldBgn ){
			if( SHOWDEBUG ){  cout << "Evaluate beginning ..." << endl;  }
			if( is_in_collision( q_bgn ) ){  return emptyVec;  }
			
			// DANGER: COPY POSE
			//~ retrnVec.push_back( copy_pose( q_bgn ) );
			retrnVec.push_back( q_bgn );
			
		}
		if( eq( distance_between( q_bgn , q_end , weightPosn , weightOrnt ) , 0.0 ) ){
			if( SHOWDEBUG ){  cout << "States were equal, return immediately ..." << endl;  }
			
			// DANGER: COPY POSE
			//~ retrnVec.push_back( copy_pose( q_end ) );
			retrnVec.push_back( q_end );
			
			return retrnVec;
		}
		if( SHOWDEBUG ){  cout << "About to iterate ..." << endl;  }
		nextPose = one_step_from_to( q_bgn , q_end , weightPosn , weightOrnt , epsilon );
		while(  distance_between( nextPose , q_end , weightPosn , weightOrnt ) > epsilon ){
			if( is_in_collision( nextPose ) ){  return emptyVec;  }
			retrnVec.push_back( nextPose );
			nextPose = one_step_from_to( nextPose , q_end , weightPosn , weightOrnt , epsilon );
		}
		if( incldEnd ){
			if( SHOWDEBUG ){  cout << "Evaluate end ..." << endl;  }
			if( is_in_collision( q_end ) ){  return emptyVec;  }
			
			// DANGER: COPY POSE
			//~ retrnVec.push_back( copy_pose( q_end ) );
			retrnVec.push_back( q_end );
			
		}
		if( SHOWDEBUG ){  cout << "About to return ..." << endl;  }
		return retrnVec;
	}
	
	
	bool add_node_star( TreePose* tree , NodePose* nuNode , double radius , size_t shortN ){
		// NOTE: This function assumes that 'nuNode' is collision-free
		
		bool SHOWDEBUG = false; // if( SHOWDEBUG ){  cout << "" << endl;  }
		
		if( SHOWDEBUG ){  sep( "add_node_star" , 4 );  }
		
		if( SHOWDEBUG ){  cout << "About to init vars ..." << endl;  }
		// ~ Algo Vars ~
		std::vector<Pose_ASP>  locPln; // -- Local planner results
		double    leastCost  = BILLION_D , // Least total cost to the root
			      leastDist  = BILLION_D , // Least distance between nodes , Used to determine if local planner is needed for connection
			      distBtn    = 0.0       , // Distance between 2 nodes
			      totCost    = 0.0       ; // Total cost to the root
		size_t    i_least    = 0 , // ------- Index of the cheapest parent for ~ Add ~
				  j_least    = 0 ; // ------- Index of the cheapest parent for ~ Rewire ~
        NodePose* leastNode  = nullptr; // -- Best parental candidate so far
        NodePose* currNghbr  = nullptr; // -- Neighbor under scrutiny
        NodePose* node_i     = nullptr; // -- Outer loop neighbor
        NodePose* node_j     = nullptr; // -- Inner loop neighbor
        NodePose* prevParent = nullptr; // -- Parent before rewire // NOT USED?
        bool      rewire     = false , // --- Flag to indicate rewire
				  wasAdded   = false ; // --- Flag for wether the ~ Add ~ phase was successful
        
        // ~ Add Phase ~
        //  1. Get the neighborhood around the new node
        if( SHOWDEBUG ){  cout << "About to fetch neighbors ... ";  }
		std::vector<std::pair<NodePose*,double>> neighbors = tree->get_neighborhood( nuNode->state , radius , shortN , weightPosn , weightOrnt );
		size_t /* --------------------------- */ len       = neighbors.size();
		if( SHOWDEBUG ){  cout << "Found " << len << " neighbors" << endl;  }
		
		if( SHOWDEBUG ){  cout << "Searching for least cost ..." << endl;  }
		
        //  2. Find the node with the least cost to root
        for( size_t i = 0 ; i < len ; i++ ){
			currNghbr = std::get<0>( neighbors[i] );
			distBtn   = distance_between( currNghbr->state , nuNode->state , weightPosn , weightOrnt );
			totCost   = distBtn + currNghbr->cost_2_root;
			if( totCost < leastCost ){
				leastCost = totCost;
                i_least   = i;
                leastDist = distBtn;
                leastNode = currNghbr;
			}
		}
		
		if( SHOWDEBUG ){  cout << "About to connect new node ..." << endl;  }
		//  3. If the distance to the cheapest node is more than the step size
		if( leastDist > epsilon ){
			
			if( SHOWDEBUG ){  
				cout << "Is there a 'leastNode'?: " << ( leastNode ? "Yes" : "No" ) << endl;
				cout << "Is there a 'nuNode'?: __ " << ( nuNode    ? "Yes" : "No" ) << endl;
				cout << "Node is very far, Run local planner from " << leastNode->state << " to " << nuNode->state << endl;  
			}
			locPln = local_planner( leastNode->state , nuNode->state , false , false );
			if( SHOWDEBUG ){  cout << "Plan has " << locPln.size() << " states" << endl;  }
			//  4. If planning succeeds, then connect
			if( locPln.size() > 0 ){  
				if( SHOWDEBUG ){  cout << "Plan succeeds, Adding nodes ..." << endl;  }
				currNghbr = tree->add_state_seq( locPln , leastNode , weightPosn , weightOrnt );
				if( SHOWDEBUG ){  cout << "Nodes added, connecting last ..." << endl;  }
				tree->add_node( nuNode , currNghbr , weightPosn , weightOrnt );
				wasAdded = true;
			}else{  wasAdded = false;  } //  A. Else plan fails, don't add node
		}else{  
			if( SHOWDEBUG ){  cout << "Node is close, add directly ..." << endl;  }
			tree->add_node( nuNode , leastNode , weightPosn , weightOrnt );  
			wasAdded = true;
		}
		if( SHOWDEBUG ){  cout << "Add phase complete!" << endl;  }
		
		if( SHOWDEBUG ){  cout << "About to rewire ..." << endl;  }
		// ~ Rewire Phase ~
		//  A. Sort the nodes
		std::sort( neighbors.begin() , neighbors.end() , sort_nodes_by_decr_cost );
		//  4. For all other nodes in the neighborhood, See if it is advantageous to rewire 'node_i' to have 'node_j' as it's parent
		//~ for( size_t i = len-1 ; i >= 0  ; i-- ){
		for( size_t i = 0 ; i < len-1 ; i++ ){
			if( i != i_least ){
				leastCost = BILLION_D;
				leastDist = BILLION_D;
				leastNode = nullptr;
				node_i    = std::get<0>( neighbors[i] );
				rewire    = false;
				if( !node_i->isRoot ){
				for( size_t j = i+1 ; j < len ; j++ ){
					node_j  = std::get<0>( neighbors[j] );
					distBtn = distance_between( node_i->state , node_j->state , weightPosn , weightOrnt );
					totCost = distBtn + node_j->cost_2_root;
					if( totCost + ( totCost * rewireMargin ) < node_i->cost_2_root ){
						rewire = true;
						if( totCost < leastCost ){
							leastCost = totCost;
							leastDist = distBtn;
							leastNode = node_j;
						}
					}
				}
				if( rewire ){
					if( leastDist > epsilon ){
						locPln = local_planner( leastNode->state , node_i->state , false , false );
						if( locPln.size() > 0 ){  
							currNghbr = tree->add_state_seq( locPln , leastNode , weightPosn , weightOrnt );
							if( node_i != currNghbr ){
								node_i->reassign_parent( currNghbr , weightPosn , weightOrnt );
							}
						} //  A. Else plan fails, don't add node
					}else{
						if( node_i != leastNode ){
							node_i->reassign_parent( leastNode , weightPosn , weightOrnt );
						}
					}
				}
				}
			}
		}
		return wasAdded;
	}
	
	Pose_ASP sample_compass_nearby( TreePose* tree , const Pose_ASP& nearbyValid , bool biasEnable = true ){
		// Return a collision-free compass sample around 'nearbyValid'
		std::vector<Pose_ASP> samples = all_compass_samples( nearbyValid , weightPosn , weightOrnt , epsilon );
		size_t lenSmpl = samples.size() , 
			   bestDex = BILLION        ;
		std::vector<size_t> freeDices;
		double distBetwn = BILLION_D ,
			   leastDist = BILLION_D ;
		Pose_ASP leastPose;
		Pose_ASP currPose;
		//  5. For each pose
		for( size_t i = 0 ; i < lenSmpl ; i++ ){
			currPose = samples[i];
			//  6. Check if the pose is free
			if( !is_in_collision( currPose ) ){
				//  7. If free, check flag and compute distance from goal
				distBetwn = distance_between( goal , currPose , weightOrnt , epsilon );
				//  8. If it is nearest goal, then store pose
				if( distBetwn < leastDist ){
					leastDist = distBetwn;
					leastPose = currPose;
				}
				//  9. If the pose is free then mark it
				freeDices.push_back( i );
			}
		}
		// 10. If none of the samples were free , Return the error pose 
		if( freeDices.size() < 1 ){  return err_pose();  }
		else{
			// 11. Roll to see if the pose should be biased
			if(  biasEnable  &&  dice_roll( biasProb )  ){
				// 12. If there was a free nearest pose return that
				return leastPose;
			// 13. Else chose a random free pose to return
			}else{ 
				//~ return samples[ freeDices[ randrange( (size_t) freeDices.size() ) ] ];
				return samples[ rand_choice( freeDices ) ];
			}
		}
	}
	
	std::vector<Pose_ASP> extend_star( const Pose_ASP& q_trgt , NodePose*& rtnPtr , double radius , size_t shortN ){
		
		bool SHOWDEBUG = false; // if( SHOWDEBUG ){  cout << "" << endl;  }
		
		if( SHOWDEBUG ){  sep( "extend_star" , 5 );  }
		
		std::vector<Pose_ASP> rtnVec;
		
		TreePose* startTree = allTrees[0]; // Will be using this tree throughout
		if( SHOWDEBUG ){  cout << "Finding nearest node ..." << endl;  }
		
		// DANGER: NEW NEAREST
		//~ std::pair<NodePose*,double> nearPair = startTree->find_nearest( q_trgt , weightPosn , weightOrnt );
		std::pair<NodePose*,double> nearPair = startTree->find_nearest_unmarked( q_trgt , weightPosn , weightOrnt );
		
		NodePose* /* ----------- */ n_near = std::get<0>( nearPair );
		NodePose* /* ----------- */ n_temp = nullptr;
		NodePose* /* ----------- */ n_goal = nullptr;
		if( SHOWDEBUG ){  cout << "Stepping towards node ..." << endl;  }
		Pose_ASP /* ------------ */ q_new = one_step_from_to( n_near->state , q_trgt , weightPosn , weightOrnt , epsilon );		
		
		if( SHOWDEBUG ){  cout << "Step Result: " << q_new << endl;
						  double btnDist = distance_between( q_new , n_near->state , weightPosn , weightOrnt );
						  cout << "Distance from nearest: " << btnDist << " , " << btnDist << "/epsilon = " << btnDist/epsilon << endl;  }
		
		bool /* ---------------- */ wasAdded = false , 
									collided = true  ;
		
		if( SHOWDEBUG ){  cout << "About to run collision checker ...";  }
		collided = is_in_collision( q_new );
		if( SHOWDEBUG ){  cout << "Collision?: " << collided << endl;  }
		
		// If we collided, try to recover with a compass sample
		if( collided ){
			if( SHOWDEBUG ){  cout << "Attempt recovery ... "  << endl;  }
			q_new = sample_compass_nearby( startTree , n_near->state , true );
			if( !is_err( q_new ) ){
				if( SHOWDEBUG ){  cout << "Recovery SUCCESS!"  << endl;  }
				n_near->compassFlag = true;
				startTree->add_by_state( q_new , n_near , weightPosn , weightOrnt );
			}else if( SHOWDEBUG ){  cout << "Recovery FAILURE!"  << endl;  }
		}else{
			n_temp   = new NodePose( q_new );
			
			if( SHOWDEBUG ){  cout << "About to run 'add_node_star' ...";  }
			wasAdded = add_node_star( startTree , n_temp , radius , shortN );
			if( SHOWDEBUG ){  cout << "Added node?: " << wasAdded << endl;  }
			
			if( !wasAdded ){  
				delete n_temp;  
				if( SHOWDEBUG ){  cout << "New node was rejected by 'add_node_star'!" << endl;  }
			} // If the add was not successful, delete the node that was created
			else{
				if( SHOWDEBUG ){  cout << "New node was accepted by 'add_node_star'! Performing goal check ..." << endl;  }
				if( distance_between( q_new , goal , weightPosn , weightOrnt ) <= epsilon ){
					n_goal = new NodePose( goal );
					startTree->add_node( n_goal , n_temp , weightPosn , weightOrnt );
					rtnVec = n_goal->path_to_node();
					if( SHOWDEBUG ){  cout << "GOAL REACHED!" << endl;  }
				}else{  if( SHOWDEBUG ){  cout << "Goal not reached" << endl;  }  }
			}
		}
		return rtnVec;
	}
	
	std::vector<Pose_ASP> build_rrt_star( const Pose_ASP& pStart , const Pose_ASP& pGoal , double radius , size_t shortN ){
		
		bool SHOWDEBUG = true , // if( SHOWDEBUG ){  cout << "" << endl;  }
			 BREAKOUT  = false ,
			 DETAIL    = false ; 
		
		std::vector<Pose_ASP> result;
		Pose_ASP /* ------ */ target;
		NodePose* /* ----- */ addedNode = nullptr;
		size_t /* -------- */ counter   = 0;
		
		if( SHOWDEBUG ){  sep( "RRT* Solver" );  }
		
		// 0. Init Tree
		
		// DANGER: COPY POSE // NOTE: ALWAYS PASS EIGEN BY REFERENCE
		//~ start = copy_pose( pStart );  
		//~ goal  = copy_pose( pGoal  );  
		start = pStart;  
		goal  = pGoal;  
		
		if( SHOWDEBUG ){  cout << "Start: " << start << endl;  }
		if( SHOWDEBUG ){  cout << "Goal:_ " << goal  << endl;  }
		
		// If either the start or the goal is in collision
		if( is_in_collision( start ) || is_in_collision( goal ) ){
			if( SHOWDEBUG ){  cout << "Either the start or the goal was in collision!"  << endl;  }
			return result;
		}
		
		allTrees.clear();
		allTrees.push_back( new TreePose( start ) );
		TreePose* startTree = allTrees[0]; // Will be using this tree throughout
		
		// DANGER: COPY POSE // NOTE: ALWAYS PASS EIGEN BY REFERENCE
		//~ startTree.rootNode->state = copy_pose( start ); // THIS IS A HACK I DO NOT KNOW WHY X AND Y COMPONENTS ARE DROPPED!
		//~ startTree.rootNode->state = start; // THIS IS A HACK I DO NOT KNOW WHY X AND Y COMPONENTS ARE DROPPED!
		
		//~ startTree.rootNode->set_state( start ); // THIS IS A HACK I DO NOT KNOW WHY X AND Y COMPONENTS ARE DROPPED!
		
		if( SHOWDEBUG ){  
			cout << "Started a new tree at" << startTree->rootNode->state << endl;  
			if( BREAKOUT ){  return result;  }
		}
		
		if( SHOWDEBUG ){  cout << "About to solve ..." << endl;  }
		while(  ( result.size() < 1 )  &&  ( startTree->size() <= K )  ){
			counter++;
			if( SHOWDEBUG && DETAIL ){  cout << "Iteration: " << counter; 
										cout << " , Nodes: " << startTree->size() << endl;  }
			
			// 1. Sample either a random point or the goal with P(bias)
			if( SHOWDEBUG && DETAIL ){  cout << "About to sample ... "; }
			target = sample_random_state();
			if( SHOWDEBUG && DETAIL ){  cout << "got: " << target << endl; }
			
			// 2. Perform extension toward the target and collect result
			if( SHOWDEBUG && DETAIL ){  cout << "About add node ..." << endl;  }
			result = extend_star( target , addedNode , radius , shortN );
		}
		if( SHOWDEBUG ){  cout << "Solver finished! Returning a path with " << result.size() << " states." << endl;  }
		delete_all_trees(); // Clean up before exiting.  There will have been MANY nodes!
		return result; // This either holds a path or we exceeded the sample limit
	}
	
	void delete_all_trees(){  clearif( allTrees );  }
	
	// ~~~ Members ~~~
	std::vector<TreePose*> allTrees;
	Pose_ASP /* ------- */ start;
	Pose_ASP /* ------- */ goal;
	size_t /* --------- */ K , 
					       N ;
	double /* --------- */ epsilon      ,
						   biasProb     ,
						   weightPosn   , 
						   weightOrnt   ,
						   rewireMargin ;
	Eigen::MatrixXd /*- */ Xbounds;
	bool /* ----------- */ foundPath;
	T /* -------------- */ is_in_collision;
	struct nodeSorter{
		bool operator()( std::pair<NodePose*,double>& opLeft , std::pair<NodePose*,double>& opRght ){ 
			return  (  std::get<1>( opLeft )  >  std::get<1>( opRght )  );
		}
	} sort_nodes_by_decr_cost;
};

// ___ RRT_Solver __________________________________________________________________________________________________________________________

void load_RRT_soln_into_mrkrMngr( RViz_MarkerManager& mngr , 
								  const std::vector<std::pair<Pose_ASP,Pose_ASP>>& allEdges , 
								  const std::vector<Pose_ASP>& solnPath , 
								  double epsilonSize = 0.1 );

/// ___ End RRT ____________________________________________________________________________________________________________________________

#endif

/* === Spare Parts =========================================================================================================================



   ___ End Parts ___________________________________________________________________________________________________________________________

*/
