// == class PlanPlayer ==

/*
	USAGE:
		1. Create a 'PlanRecording' with per-frame markers and configurations
		2. Instantiate 'PlanPlayer' with proper topic names for the demo, passing the recording to constructor
		3. 'run()'
		NOTE: This is not a node, this manages publishers for the node
*/

/*
	TODO:
		[ ] Find out if ROS freaks out about having ROS member vars before 
*/

class PlanPlayer{
	
public:

	// ~~ Functions ~~

	// ~ Constructor ~
	PlanPlayer( int rateHzP , string nsP , size_t repeatP , PlanRecording recordingP , 
				string markerTopicP , string jointTopicP );
	
	// ~ ROS Node ~
	bool init(); // Do the appropriate setup work
	bool render_frame(); // Render a new frame when the time comes, otherwise publish nothing

	// ~~ Members ~~

	// ~ ROS Node Vars ~
	ros::Publisher  mrkr_pub; // ----- Marker publisher
	string /* -- */ mrkr_pub_topic; // Marker topic
	double /* -- */ frameRate; // ---- Frequency [Hz] of frame change
	ros::Publisher  jnt_cmd_pub; // -- Joint publisher
	string /* -- */ jnt_cmd_topic; //- Joint topic
	size_t /* -- */ repeat; // ------- Number of times to repeat messages // NOT USED: See ROS::Rate -vs- 'frameRate'
	
	// ~ Playback Data ~
	PlanRecording recording; // ------ Marker and JointState data needed to reconstruct plan
	
};

// __ End PlanPlayer __
