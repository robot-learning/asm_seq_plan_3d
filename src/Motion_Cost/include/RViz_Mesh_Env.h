#pragma once // This also helps things not to be loaded twice , but not always . See below

/***********  
RViz_Mesh_Env.h
James Watson , 2017 October
Classes and functions for managing a collection of meshes in RViz

Template Version: 2017-09-23
***********/

#ifndef RVIZ_MESH_ENV_H // This pattern is to prevent symbols to be loaded multiple times
#define RVIZ_MESH_ENV_H // from multiple imports

// ~~ Includes ~~
// ~ Standard ~
#include <stdio.h>
// ~ Local ~
#include <Cpp_Helpers.h>
// ~ ROS : Transforms & RVIZ ~
#include <ros/ros.h> // ---------------------------- ROS presides over all
#include <visualization_msgs/Marker.h> // ---------- Display geometry
#include <visualization_msgs/MarkerArray.h>


// ~~ Shortcuts and Aliases ~~



// === Classes and Structs =================================================================================================================





// ___ End Classes _________________________________________________________________________________________________________________________



// === Functions ===========================================================================================================================



// ___ End Func ____________________________________________________________________________________________________________________________


#endif

/* === Spare Parts =========================================================================================================================



   ___ End Parts ___________________________________________________________________________________________________________________________

*/
