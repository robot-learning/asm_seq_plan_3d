#pragma once // This also helps things not to be loaded twice , but not always . See below

/***********  
comm_ASM.h
James Watson , 2018 June
Messages, Communication, and Visualization for Assembly Planning

Template Version: 2018-06-06
***********/

#ifndef COMM_ASM_H // This pattern is to prevent symbols to be loaded multiple times
#define COMM_ASM_H // from multiple imports

// ~~ Includes ~~
// ~ Eigen ~
#include <Eigen/Core> // ---- The living heart of Eigen
#include <Eigen/StdVector> // This is REQUIRED to put an Eigen matrix in an std::vector
#include <Eigen/Dense> // --- Cross Product , etc.
#include <Eigen/Geometry> //- Quaternion , etc
// ~ ROS ~
#include <ros/ros.h> // -------------- ROS , Publishers , Subscribers
#include <sensor_msgs/JointState.h> // JointState , For plan replayer
#include <geometry_msgs/Pose.h> // --- Geometry Pose , For plan replayer
#include <ll4ma_teleop/Joints2Jac.h>

// ~ Local ~
#include <Cpp_Helpers.h> // C++ Utilities and Shortcuts
#include <ROS_Helpers.h> // ROS Utilities and Shortcuts
#include <ASP_3D.h> // ---- Assembly Geometry
#include <AsmSeqPlan.h> //- Planning

// ~~ Constants ~~
const std::vector<string> ALL_JOINT_NAMES = { "lbr4_j0" , "lbr4_j1" , "lbr4_j2" , "lbr4_j3" , "lbr4_j4" , "lbr4_j5" , "lbr4_j6" , 
										      "r_gripper_l_finger_joint" , "r_gripper_r_finger_joint" };
const size_t NUM_ALL_JOINTS = ALL_JOINT_NAMES.size();

/// ==== MESSAGES ==========================================================================================================================

geometry_msgs::Pose ROS_Pose_from_Pose_ASP( const Pose_ASP& pose ); // Load an Pose_ASP into a ROS Pose message and return

Pose_ASP ASP_Pose_from_Pose_ROS( const geometry_msgs::Pose& pose ); // Return the Pose_ASP that corresponds to the ROS pose

std::vector<Pose_ASP> load_ASP_Poses_from_Poses_ROS( const std::vector<geometry_msgs::Pose>& poses );

void /* -------- */ pack_joint_and_gripper_states_into_msg( const std::vector<double>&     armState , //- Joint positions of arm
											                const std::vector<double>&     fingState , // Joint positions of gripper
											                sensor_msgs::JointState& stateMsg ); // Load state data here

Eigen::MatrixXd     extract_Jacobian( const ll4ma_teleop::Joints2Jac& msg );

/// ____ END MESSAGE _______________________________________________________________________________________________________________________


/// === ARM PUBLISHER WRAPPERS =============================================================================================================

struct send_joint_command{
	ros::Publisher& jntCmdPublisher;
	void operator()( const std::vector<double>& q_joints7 , const std::vector<double>& fing_pos2 = std::vector<double>() , size_t repeat = 5 );
};

/// ___ END PUBLISHER ______________________________________________________________________________________________________________________


/// ==== VISUALIZATION =====================================================================================================================

// Diagnose a disassembly sequence for a single level
void display_sequence_without_animation( AsmStateNode_ASP* rootNode , RViz_MarkerManager& nodeMrkrMngr , 
										 Eigen::Vector2d planStart , Eigen::Vector2d actionIncr , Eigen::Vector2d remvOffset );

// Diagnose a disassembly sequence for a single level
void display_sequence_without_animation_ALTERNATE( AsmStateNode_ASP* rootNode , RViz_MarkerManager& nodeMrkrMngr ,
												   Eigen::Vector2d planStart , Eigen::Vector2d actionIncr , Eigen::Vector2d remvOffset );

PlayerFrame* spam_frame(); // Return a frame that is just cubes

/// ____ END VIZ ___________________________________________________________________________________________________________________________

#endif

/* === Spare Parts =========================================================================================================================



   ___ End Parts ___________________________________________________________________________________________________________________________

*/
