#!/usr/bin/env python
# -*- coding: utf-8 -*-

# ~~ Future First ~~
from __future__ import division # Future imports must be called before everything else, including triple-quote docs!

__progname__ = "get_q_from_pose_serv.py"
__version__  = "2018.06"
"""
James Watson , Template Version: 2018-05-30
Built on Spyder for Python 2.7

Get an IK solution for the given effector pose

Dependencies: numpy

#####################################################################################################
#                                                                                                   #
#   NOTE: You must have loaded a xacro to the parameter server under the name "robot_description"   #
#                                                                                                   #
#####################################################################################################

"""


"""  
~~~ Developmnent Plan ~~~
[ ] ITEM1
[ ] ITEM2
"""

# === Init Environment =====================================================================================================================
# ~~~ Prepare Paths ~~~
import sys, os.path
SOURCEDIR = os.path.dirname( os.path.abspath( __file__ ) ) # URL, dir containing source file: http://stackoverflow.com/a/7783326
PARENTDIR = os.path.dirname( SOURCEDIR )
# ~~ Path Utilities ~~
def prepend_dir_to_path( pathName ): sys.path.insert( 0 , pathName ) # Might need this to fetch a lib in a parent directory
def rel_to_abs_path( relativePath ): return os.path.join( SOURCEDIR , relativePath ) # Return an absolute path , given the 'relativePath'

# ~~~ Imports ~~~
# ~~ Standard ~~
from math import pi , sqrt , radians
from random import random
# ~~ Special ~~
import numpy as np
import rospy 
# ~ IK SOlver ~
from trac_ik_python.trac_ik import IK
# ~ ROS Messages ~
from geometry_msgs.msg import Pose # --- For receiving the effector pose
from sensor_msgs.msg import JointState # For sending the joint angles
# ~~ Local ~~
# ~ Package Messages ~
from motion_cost.msg import IKrequest #- Message contains desired pose and possibly a seed
from motion_cost.msg import IKresponse # Message contains success code and solved joint angles
# ~ Package Services ~
from motion_cost.srv import qFromPose # IK Service


# ~~ Constants , Shortcuts , Aliases ~~
EPSILON = 1e-7
infty   = 1e309 # URL: http://stackoverflow.com/questions/1628026/python-infinity-any-caveats#comment31860436_1628026
endl    = os.linesep

# ~~ Script Signature ~~
def __prog_signature__(): return __progname__ + " , Version " + __version__ # Return a string representing program name and verions

# ___ End Init _____________________________________________________________________________________________________________________________


# === Main Application =====================================================================================================================

# = Init Solver =

# USAGE: 1. You must have loaded a xacro to the parameter server under the name "robot_description"
#        2. The first argument is the base frame, in which the request is expressed
#        3. The second argument is the target frame that the desired pose describes

# Solver is relative from base to the end of the arm
#ik_solver = IK( "world"       ,   # world # base_link    # torso_lift_link
#                "lbr4_7_link" )   # lbr4_7_link  # r_wrist_roll_link 
ik_solver = IK( "world"       ,   # world # base_link    # torso_lift_link
                "grasp_target" )   # lbr4_7_link  # r_wrist_roll_link 

_DEFAULT_SEED = [ 0.0 ] * ik_solver.number_of_joints

_SOLVER_FREE = True # Hacked mutex flag

FUZZMAG = radians( 5 )

# _ End Solver _


# = Program Functions =

def get_soln_from_solver( desX , seed ):
    """ Get a solution from the solver """
    fuzz = [ FUZZMAG * random() for i in xrange( ik_solver.number_of_joints ) ] # Move the seed off the given so that we don't repeat queries
    return ik_solver.get_ik( np.add( seed.position , fuzz ) , # -------------------------------------------------- q  (JointState.position)
                             desX.position.x    , desX.position.y    , desX.position.z    , # -------------------- X  , Y  , Z
                             desX.orientation.x , desX.orientation.y , desX.orientation.z , desX.orientation.w ) # QX , QY , QZ , QW
# _ End Func _


# = Program Classes =

class LBR4_IK_Solver:
    """ From the world frame to the end of link 7 """
    
    def __init__( self ):
        """ Start the node , with a subscriber for  """
        
        # 1. Init Node
        rospy.init_node( 'LBR4_IK_Solver' ) # ------ Start the node
        self.heartBeatHz = 300 # ------------------- Node refresh rate [Hz]
        self.idle = rospy.Rate( self.heartBeatHz ) # Best effort to maintain 'heartBeatHz' , URL: http://wiki.ros.org/rospy/Overview/Time
        self.repeat = 1 # -------------------------- Default number of times to transmit the response
        
        # 2. Set up the service
        self.service = rospy.Service( 'q_from_pose'   , qFromPose , self.ik_req_cb )
        #              rospy.Service( advertised name , SRV type  , handler func   )
        
    def ik_req_cb( self , req ):
        """ Process the request """
        request = req.request # It's just requests all the way down!
        
        SHOWDEBUG = True
        
        if SHOWDEBUG:
            print "Got a request:" , endl , request
        
        rtnMsg = IKresponse()

        soln = get_soln_from_solver( request.worldPose , request.seed )
        rtnMsg.sequence = request.sequence # Match the response with the request, in case the client cares
        # If there was no solution found , set flag and return NaN
        if soln == None:
            rtnMsg.valid = 0
            rtnMsg.qSoln.position = [ float('NaN') ] * ik_solver.number_of_joints
        # else found a soluton , set flag and return the solved joint angles
        else:
            rtnMsg.valid = 1
            rtnMsg.qSoln.position = soln
            
        if SHOWDEBUG:
            print "Sent a response:" , endl , rtnMsg
            
        return rtnMsg
        
    def run( self ):
        """ Idle until a request arrives """
        rospy.spin()

# _ End Classes _

if __name__ == "__main__":
    print __prog_signature__()
    termArgs = sys.argv[1:] # Terminal arguments , if they exist
    
    node = LBR4_IK_Solver() # Create a solver node
    node.run() # ------------ Start node and wait for requests   

# ___ End Main _____________________________________________________________________________________________________________________________


# === Spare Parts ==========================================================================================================================



# ___ End Spare ____________________________________________________________________________________________________________________________
