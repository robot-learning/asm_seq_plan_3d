/***********  
ASP_3D.cpp
James Watson , 2017 September
Structures and functions to geometric processing and assembly planning

Template Version: 2017-09-23
***********/

#include <ASP_3D.h>

Eigen::Vector3d _RED( 1.0 , 0.0 , 0.0 ); // Colors
Eigen::Vector3d _YLW( 1.0 , 1.0 , 0.0 );
Eigen::Vector3d _GRN( 0.0 , 1.0 , 0.0 );
Eigen::Vector3d _AQU( 0.0 , 1.0 , 1.0 );
Eigen::Vector3d _BLU( 0.0 , 0.0 , 1.0 );
Eigen::Vector3d _PNK( 1.0 , 0.0 , 1.0 );
// static const vector<Eigen::Vector3d> COLORS_UNITY = { _RED , _YLW , _GRN , _AQU , _BLU , _PNK }; // doesn't work

Eigen::Vector3d _WHT( 1.0 , 1.0 , 1.0 ); // Values
Eigen::Vector3d _GRY( 0.5 , 0.5 , 0.5 );
Eigen::Vector3d _BLK( 0.0 , 0.0 , 0.0 );

// === Classes and Structs =================================================================================================================

/// === VISUAL MARKERS =====================================================================================================================

// == class Rainbow ==

Rainbow::Rainbow(){
	 // Default constructor
	 index = 0;
	 colors.push_back( _RED );
	 colors.push_back( _YLW );
	 colors.push_back( _GRN );
	 colors.push_back( _AQU );
	 colors.push_back( _BLU );
	 colors.push_back( _PNK );
	 //cout << "COLORS_UNITY has " << COLORS_UNITY.size() << " colors" << endl;
	 cout << "Created a Rainbow with " << colors.size() << " colors , at index " << index << endl;
	 cout << "colors[2]:" << endl << colors[2] << endl;
	 cout << "_RED" << _RED  << endl;
}
 
Rainbow::~Rainbow(){
	// Default destructor
	colors.clear(); 
}

void Rainbow::advance(){
	// cout << "DEBUG , Rainbow::advance - About to advance index " << index << " ..." << endl;
	index = (size_t) ( index + 1 ) % colors.size();
}

Eigen::Vector3d Rainbow::get_color(){
	// cout << "DEBUG , Rainbow::get_color - About to get color " << index << " ..." << endl;
	return colors[ index ];
}

Eigen::Vector3d Rainbow::advance_and_get(){
	advance();
	return get_color();
}

// __ End Rainbow __


// == class RViz_MeshMarker ==

void RViz_MeshMarker::_init(){ // Default constructor guts
	
	bool SHOWDEBUG = false; // if( SHOWDEBUG ){  cout << "" << endl;  }
    
    if( SHOWDEBUG ){ sep( "RViz_MeshMarker::_init" );
					 cout << "Init ..." << endl;  }
	
	visMarker.header.frame_id = "world"; // ---------- Set the default world frame
 	visMarker.ns              = "AssemblyPlanning"; // Set the default marker namespace
	set_id( 1000 ); // ------------------------------- Set to a dummy number in the absence of a real number
	if( SHOWDEBUG ){  cout << "About to set color ..." ;  }
	set_color( 0.0f , 0.0f , 1.0f , 1.0f ); // ------- Set tht default color to blue
	if( SHOWDEBUG ){  cout << " Color set!" << endl ;  }
	set_scale( 1.0f ); // -------------------- Set the default scale to 1
	if( SHOWDEBUG ){  cout << "Scale set!" << endl ;  }
	set_pose( 0.0f , 0.0f , 0.0f , // -------- Set the default pose to origin
	          1.0f , 0.0f , 0.0f , 0.0f );
	if( SHOWDEBUG ){  cout << "Pose set!" << endl ;  }
	visMarker.type            = visualization_msgs::Marker::TRIANGLE_LIST; // HARD CODED
	if( SHOWDEBUG ){  cout << "Type set!" << endl ;  }
	visMarker.action          = visualization_msgs::Marker::ADD; // HARD CODED
	if( SHOWDEBUG ){  cout << "Action set! , COMPLETE" << endl ;  }
}

RViz_MeshMarker::RViz_MeshMarker(){ 
	bool SHOWDEBUG = false; // if( SHOWDEBUG ){  cout << "" << endl;  }
    if( SHOWDEBUG ){ sep( "RViz_MeshMarker::RViz_MeshMarker()" ); }
	_init(); 
} // Default constructor

RViz_MeshMarker::RViz_MeshMarker( Part_ASM& meshPart ){ // Extract the mesh infomation from the Part_ASM
	bool SHOWDEBUG = false; // if( SHOWDEBUG ){  cout << "" << endl;  }
    if( SHOWDEBUG ){ sep( "RViz_MeshMarker::RViz_MeshMarker( Part_ASM& meshPart )" ); }
	// 1. Populate ID info
	visMarker.header.frame_id = "world";
	visMarker.ns              = "AssemblyPlanning";
	visMarker.type            = visualization_msgs::Marker::TRIANGLE_LIST; // HARD CODED
	visMarker.action          = visualization_msgs::Marker::ADD; // HARD CODED
	set_id( 1000 ); // Set to a dummy number in the absence of a real number
	// 2. Set color and scale
	set_color( 0.0f , 0.0f , 1.0f , 1.0f );
	set_scale( 1.0f );
	// 3. Load pose data from the part
	set_pose_from_part( meshPart );
	// 4. Load the triangle data from the mesh
	load_triangles( meshPart.get_dense_vertices() , meshPart.get_dense_facets() );
}

RViz_MeshMarker::RViz_MeshMarker( const RViz_MeshMarker& other ){ 
	// Copy constructor
	bool SHOWDEBUG = false; // if( SHOWDEBUG ){  cout << "" << endl;  }
    if( SHOWDEBUG ){ sep( "RViz_MeshMarker::RViz_MeshMarker( const RViz_MeshMarker& other )" ); }
    if( SHOWDEBUG ){  cout << "Copy header info ..." << endl;  }
	visMarker.header.frame_id = other.visMarker.header.frame_id;
	visMarker.ns              = other.visMarker.ns;
	visMarker.type            = other.visMarker.type;
	visMarker.action          = other.visMarker.action;
	if( SHOWDEBUG ){  cout << "Set ID ..." << endl;  }
	set_id( other.visMarker.id ); // Set to a dummy number in the absence of a real number
	// 2. Set color and scale
	if( SHOWDEBUG ){  cout << "Get color ..." << endl;  }
	set_color( other.get_uniform_color() );
	if( SHOWDEBUG ){  cout << "Set scale ..." << endl;  }
	set_scale( 1.0f );
	// 3. Load pose data from the part
	if( SHOWDEBUG ){  cout << "Set pose ..." << endl;  }
	set_pose_from_marker( other );
	// 4. Load the triangle data from the mesh
	if( SHOWDEBUG ){  cout << "Load triangles ..." << endl;  }
	load_triangles( other );
	if( SHOWDEBUG ){  cout << "COMPLETE!" << endl;  }
}

RViz_MeshMarker* RViz_MeshMarker::deletion_marker( RViz_MeshMarker& other ){
	// Create a marker that will perform a REMOVE action
	RViz_MeshMarker* rtnMarker = new RViz_MeshMarker( other );
	rtnMarker->visMarker.action = visualization_msgs::Marker::DELETE; // HARD CODED
	return rtnMarker;
}

RViz_MeshMarker::RViz_MeshMarker( const Eigen::MatrixXd& verticesDense , const Eigen::MatrixXi& facetsDense ){ // Create the mesh from triangles
	_init(); // Default constructor
	load_triangles( verticesDense , facetsDense );
}

RViz_MeshMarker::RViz_MeshMarker( const TriMeshVFN_ASP& triMesh ){
	// Create the mesh from TriMesh
	_init(); // Default constructor
	load_triangles( triMesh.V , triMesh.F );
}

RViz_MeshMarker::~RViz_MeshMarker(){  uniformColor.clear();  }

// ~ Getters ~
Pose_ASP RViz_MeshMarker::get_pose(){
	// Get the pose as stored in the marker
	return get_mrkr_pose( visMarker );
}

Eigen::MatrixXd RViz_MeshMarker::get_vertices(){
	// Get the vertices as stored in the marker
	return get_mrkr_vertices( visMarker );
}

Eigen::MatrixXd RViz_MeshMarker::get_colors(){ // Get the colors as stored in the marker
	return get_mrkr_colors( visMarker );
}

std::vector<float> RViz_MeshMarker::get_uniform_color() const {  
	return vec_copy( uniformColor );
}

void RViz_MeshMarker::set_color( float R , float G , float B , float alpha ){ // Set the marker color RGBA , default alpah=1 opaque
    visMarker.color.r = R;		
    visMarker.color.g = G;		
    visMarker.color.b = B;		
    visMarker.color.a = alpha;	
    if( uniformColor.size() != 4 ){  uniformColor = {1,1,1,1};  }
    uniformColor[0] = R;
    uniformColor[1] = G;
    uniformColor[2] = B;
    uniformColor[3] = alpha;
    reload_color(); // Remember that all the vertices are as you left them!
}

void RViz_MeshMarker::set_color( const std::vector<float>& color ){ // Set the marker color RGBA , default alpah=1 opaque
	if( color.size() == 4 ){
		set_color( color[0] , color[1] , color[2] , color[3] );
	}
    reload_color(); // Remember that all the vertices are as you left them!
}

void RViz_MeshMarker::print_color(){ // Print the color for debug
	cout << "[ " << visMarker.color.r << " , " 
	             << visMarker.color.g << " , " 
	             << visMarker.color.b << " , " 
	             << visMarker.color.a << " ]"  << endl;
}

void RViz_MeshMarker::print_pose(){ // Print the pose for debug
	cout <<  "position:    [ x: " << visMarker.pose.position.x
	                  << " , y: " << visMarker.pose.position.y 
	                  << " , z: " << visMarker.pose.position.z << " ]"  << endl
		 <<  "orientation: [ w: " << visMarker.pose.orientation.w 
	                  << " , x: " << visMarker.pose.orientation.x
	                  << " , y: " << visMarker.pose.orientation.y 
	                  << " , z: " << visMarker.pose.orientation.z << " ]"  << endl;
}

void RViz_MeshMarker::rand_color(){ // Set the marker to a random color
    set_color( rand_float() , rand_float() , rand_float() , 1.0f ); // Set the color -- be sure to set alpha to something non-zero!
}

// Set the opacity for this shape
void RViz_MeshMarker::set_alpha( float alpha ){ visMarker.color.a = alpha; }

void RViz_MeshMarker::set_scale( float Xscale , float Yscale , float Zscale ){
    visMarker.scale.x = Xscale; // Set the scale of the marker -- 1x1x1 here means 1m on a side
    visMarker.scale.y = Yscale;
    visMarker.scale.z = Zscale;
}

void RViz_MeshMarker::set_scale( float uniformScale ){ // Set all the dimensions of the marker to 'uniformScale'
    set_scale( uniformScale , uniformScale , uniformScale );
}

void RViz_MeshMarker::set_pose( float pX , float pY , float pZ , float oW , float oX , float oY , float oZ ){ // Abso pose in RViz
	// ~ Position ~
	visMarker.pose.position.x    = pX;
	visMarker.pose.position.y    = pY;
	visMarker.pose.position.z    = pZ;
	// ~ Orientation ~
	visMarker.pose.orientation.w = oW;
	visMarker.pose.orientation.x = oX;
	visMarker.pose.orientation.y = oY;
	visMarker.pose.orientation.z = oZ;
}

void RViz_MeshMarker::set_pose_from_marker( const RViz_MeshMarker& other ){ // Abso pose in RViz
	// ~ Position ~
	visMarker.pose.position.x    = other.visMarker.pose.position.x;
	visMarker.pose.position.y    = other.visMarker.pose.position.y;
	visMarker.pose.position.z    = other.visMarker.pose.position.z;
	// ~ Orientation ~
	visMarker.pose.orientation.w = other.visMarker.pose.orientation.w;
	visMarker.pose.orientation.x = other.visMarker.pose.orientation.x;
	visMarker.pose.orientation.y = other.visMarker.pose.orientation.y;
	visMarker.pose.orientation.z = other.visMarker.pose.orientation.z;
}

void RViz_MeshMarker::set_pose_from_part( Part_ASM& meshPart ){ // Load pose data from the part
	Eigen::Vector3d    pos    = meshPart.get_position();
	Eigen::Quaterniond orient = meshPart.get_orientation();
	set_pose( pos(0)     , pos(1)     , pos(2)     , 
	          orient.w() , orient.x() , orient.y() , orient.z() );
	
}

void RViz_MeshMarker::set_id( int itemID ){ visMarker.id = itemID; } // Set the ID num
int  RViz_MeshMarker::get_id(            ){ return visMarker.id;   } // Get the ID num

void RViz_MeshMarker::set_tags( string frame_id , ros::Time stamp , string nameSpace ){ // Set the tag information for this marker
	visMarker.header.frame_id = frame_id;
	visMarker.header.stamp    = stamp;
	visMarker.ns              = nameSpace;
	visMarker.type            = visualization_msgs::Marker::TRIANGLE_LIST; // HARD CODED
	visMarker.action          = visualization_msgs::Marker::ADD; // HARD CODED
}

void RViz_MeshMarker::set_timestamp( ros::Time stamp ){ visMarker.header.stamp = stamp; }

visualization_msgs::Marker& RViz_MeshMarker::get_marker(){ return visMarker; }

void RViz_MeshMarker::get_color( float colorArr[4] ){ 
	// Get the color of the marker and load it into 'colorArr' {r,g,b,a}
	colorArr[0] = visMarker.color.r; // Red
	colorArr[1] = visMarker.color.g; // Green 
	colorArr[2] = visMarker.color.b; // Blue
	colorArr[3] = visMarker.color.a; // Alpha
}

void RViz_MeshMarker::reload_color(){ // This is a private function to recolor all of the vertices according to the global marker color
	// URL: marker.points and marker.colors are just 'std::vector', so you can use all of the associated functions!
	//      https://answers.ros.org/question/272487/length-of-the-ranges-array-in-a-laserscan-msg/?answer=272488#post-id-272488
	
	int i       = 0                       , 
		j       = 0                       ,
	    numRows = visMarker.points.size() ;
	
	visMarker.colors.clear();
	
	for( i = 0 ; i < numRows ; i++ ){ // For each of the facets listed
		for( j = 0 ; j < 3 ; j++ ){ // For each of the vertices associated with the facet
			std_msgs::ColorRGBA c;
			c.r = visMarker.color.r;
			c.g = visMarker.color.g;
			c.b = visMarker.color.b;
			c.a = visMarker.color.a; // Don't forget to set the alpha!
			visMarker.colors.push_back( c ); // <-- This is vertex shading
			//                                   One color for each vertex , to be blended within the triangle
		}
	}
}

void RViz_MeshMarker::load_triangles( const Eigen::MatrixXd& verticesDense , const Eigen::MatrixXi& facetsDense ){ // Create the mesh from triangles
	
	int i       = 0                  , 
		j       = 0                  ,
	    numRows = facetsDense.rows() ;
	
	for( i = 0 ; i < numRows ; i++ ){ // For each of the facets listed
		for( j = 0 ; j < 3 ; j++ ){ // For each of the vertices associated with the facet
			
			geometry_msgs::Point p;
			p.x = verticesDense( facetsDense( i , j ) , 0 );
			p.y = verticesDense( facetsDense( i , j ) , 1 );
			p.z = verticesDense( facetsDense( i , j ) , 2 );
			visMarker.points.push_back( p );
			
			std_msgs::ColorRGBA c;
			c.r = visMarker.color.r;
			c.g = visMarker.color.g;
			c.b = visMarker.color.b;
			c.a = visMarker.color.a; // Don't forget to set the alpha!
			visMarker.colors.push_back( c ); // <-- This is vertex shading
			//                                   One color for each vertex , to be blended within the triangle
		}
	}
}

void RViz_MeshMarker::load_triangles( const RViz_MeshMarker& other ){ // Create the mesh from triangles
	
	int i       = 0                  , 
		j       = 0                  ,
	    numRows = other.visMarker.points.size() ;
	
	for( i = 0 ; i < numRows ; i++ ){ // For each of the facets listed
		visMarker.points.push_back( other.visMarker.points[i] );
		visMarker.colors.push_back( other.visMarker.colors[i] );
	}
}

SuccessCode check_marker_OK( RViz_MeshMarker& meshMrkr ){ 
	// Check that there are no { NaN , Infinity } in the marker
	SuccessCode rtnSuccess;
	bool poseOK = check_pose_OK( meshMrkr.get_pose()     );
	bool vertOK = check_matx_OK( meshMrkr.get_vertices() );
	bool colrOK = check_matx_OK( meshMrkr.get_colors()   );
	rtnSuccess.success = poseOK && vertOK && colrOK;
	rtnSuccess.code = 0;
	rtnSuccess.desc = "";
	cout.flush();
	if( !poseOK ){
		rtnSuccess.code += 001;
		rtnSuccess.desc += "BAD POSE , ";
		cerr << "BAD POSE" << endl;
		cerr << meshMrkr.get_pose() << endl;
	}
	if( !vertOK ){
		rtnSuccess.code += 010;
		rtnSuccess.desc += "BAD VERTICES , ";
		cerr << "BAD VERTICES" << endl;
		cerr << meshMrkr.get_vertices() << endl;
	}
	if( !colrOK ){
		rtnSuccess.code += 100;
		rtnSuccess.desc += "BAD COLORS";
		cerr << "BAD COLORS" << endl;
		cerr << meshMrkr.get_colors() << endl;
	}
	if( rtnSuccess.success ){  rtnSuccess.desc = "OK";  }
	return rtnSuccess;
}

// __ End RViz_MeshMarker __


// == class Rviz_MarkerManager ==

RViz_MarkerManager::RViz_MarkerManager(){  //- Default constructor
	bool SHOWDEBUG = false;
	nextID = 0; // The next marker will be assigned this ID
	if( SHOWDEBUG ){  cout << "Created a RViz_MarkerManager!" << endl;  }
}

RViz_MarkerManager::~RViz_MarkerManager(){ // Default destructor
	//~ mrkrList.clear(); // Clear all of the markers from the list
	clearif( mrkrList ); // Clear all of the markers from the list
}

RViz_MarkerManager RViz_MarkerManager::anti_manager( RViz_MarkerManager& other ){
	// Create a manager that erases all the markers of 'other'
	RViz_MarkerManager rtnMngr;
	std::list<RViz_MeshMarker*>::iterator it;
	RViz_MeshMarker* temp;
	for( it = other.mrkrList.begin() ; it != other.mrkrList.end() ; ++it ){
		temp = RViz_MeshMarker::deletion_marker( *(*it) );
		rtnMngr.add_marker( *temp , true );
	}
	return rtnMngr;
}

// get the number of markers per list
size_t RViz_MarkerManager::get_len(){ return mrkrList.size(); }

// Return a reference to the marker array
visualization_msgs::MarkerArray& RViz_MarkerManager::get_arr(){ return mrkrArry; }

int RViz_MarkerManager::add_marker( RViz_MeshMarker& meshMrkr , bool suppressIncr ){ // Add a marker to the manager
	if( !suppressIncr ){
		meshMrkr.set_id( nextID ); // --------- Give the marker a non-conflicting ID
		nextID++; // ------------------------- Increment ID so that markers remain unique
	}
	mrkrList.push_back( &meshMrkr ); // Push onto the list
	mrkrArry.markers.push_back( meshMrkr.get_marker() ); // Push onto the marker array message
	return meshMrkr.get_id();
}

int RViz_MarkerManager::get_next_ID_incr(){  int temp = nextID;  nextID++;  return temp;  }

int RViz_MarkerManager::add_marker( visualization_msgs::Marker genericMrkr , bool suppressIncr  ){ // -------- Add a generic marker to the manager
	if( !suppressIncr  ){
		genericMrkr.id = get_next_ID_incr();
	}
	basicMrkrs.push_back( genericMrkr );
	mrkrArry.markers.push_back( genericMrkr ); // Push onto the marker array message
}

// 2017-10-09 : At this time not supporting removal of markers

visualization_msgs::MarkerArray& RViz_MarkerManager::rebuild_mrkr_array(){
	size_t i   = 0                 ,
	       len = mrkrList.size()   ,
	       j   = 0                 ,
	       lnB = basicMrkrs.size() ;
	
	mrkrArry.markers.clear(); // Erase all of the old markers
	// Add mesh markers
	std::list<RViz_MeshMarker*>::iterator it;
	for( it = mrkrList.begin(); it != mrkrList.end(); ++it ) {
		mrkrArry.markers.push_back( (*it)->get_marker() ); // Push onto the marker array message
	}
	// Add basic markers
	std::list<visualization_msgs::Marker>::iterator iterBasic;
	for( iterBasic = basicMrkrs.begin(); iterBasic != basicMrkrs.end(); ++iterBasic ) {
		mrkrArry.markers.push_back( (*iterBasic) ); // Push onto the marker array message
	}
	return mrkrArry;
}

std::vector<SuccessCode> RViz_MarkerManager::diagnose_markers_OK(){
	// Return true if there are no NaN or Infinity values , Print report
	std::vector<SuccessCode> rtnVec;
	sep( "Marker Diagnosis" , 2 , '*' );
	size_t len = mrkrArry.markers.size();
	cout << "There are " << len << " markers." << endl;
	for( size_t i = 0 ; i < len ; i++ ){  
		rtnVec.push_back( check_marker_OK( mrkrArry.markers[i] ) );  
		if( rtnVec[i].success ){
			cout << "\t" << "Marker " << i << " OK" << endl;
		}else{
			cout << "\t" << "Marker " << i << " BAD! : " << rtnVec[i].desc << endl;
		}
	}
	return rtnVec;
}

void RViz_MarkerManager::spam_cubes(){ 
	// Spam a ton of markers to RViz for us to overwrite later
	
	float xOffset = 100.0f; // Way out in the distance
	int   count   = 9000 ,
		  num     = 5    ; // Number of cubes = pow( num , 3 )
	
	for( int x = 0 ; x < num ; ++x ){
		for( int y = 0 ; y < num ; ++y ){
			for( int z = 0 ; z < num ; ++z ){

				visualization_msgs::Marker visMarker;
				visMarker.header.frame_id = "world";
				visMarker.ns              = "AssemblyPlanning";
				visMarker.type            = visualization_msgs::Marker::CUBE; // HARD CODED
				visMarker.action          = visualization_msgs::Marker::ADD; // HARD CODED
				visMarker.id              = count;
				
				visMarker.pose.position.x = x * 0.1f + xOffset;
				visMarker.pose.position.y = y * 0.1f;
				visMarker.pose.position.z = z * 0.1f;
				
				visMarker.pose.orientation.x = 0.0;
				visMarker.pose.orientation.y = 0.0;
				visMarker.pose.orientation.z = 0.0;
				visMarker.pose.orientation.w = 1.0;
				
				visMarker.scale.x = 0.05;
				visMarker.scale.y = 0.05;
				visMarker.scale.z = 0.05;
				
				visMarker.color.r = 1.0;
				visMarker.color.g = 1.0;
				visMarker.color.b = 0.0;
				visMarker.color.a = 1.0;
				
				mrkrArry.markers.push_back( visMarker );
					
				count++;
			}
		}
	}
	
	
    
    
    
    
}

// __ End Rviz_MarkerManager __

visualization_msgs::Marker get_axes_marker( float axsLen , float thickness ){
 
	float axesPts[3][3] = { { axsLen , 0.0    , 0.0    } ,   
							{ 0.0    , axsLen , 0.0    } ,   
							{ 0.0    , 0.0    , axsLen } };
	
	float axesClrs[3][3] = { { 1.0 , 0.0 , 0.0 } ,   
							 { 0.0 , 1.0 , 0.0 } ,   
							 { 0.0 , 0.0 , 1.0 } };
	
	visualization_msgs::Marker axes;
	axes.header.frame_id = "world";
	axes.header.stamp = ros::Time();
	axes.ns = "my_namespace";
	axes.id = 0;
	axes.type = visualization_msgs::Marker::LINE_LIST;
	axes.action = visualization_msgs::Marker::ADD;
	axes.pose.position.x = 0.0;
	axes.pose.position.y = 0.0;
	axes.pose.position.z = 0.0;
	axes.pose.orientation.x = 0.0;
	axes.pose.orientation.y = 0.0;
	axes.pose.orientation.z = 0.0;
	axes.pose.orientation.w = 1.0;
	
	axes.scale.x = thickness; // <-- Line Thickness
	
	axes.color.r = 1.0;
	axes.color.a = 1.0;
	for( int i = 0 ; i < 3 ; ++i ){
		// Set Points
		geometry_msgs::Point p1, p2;
		p1.x = 0;				p1.y = 0;				p1.z = 0;
		p2.x = axesPts[i][0];	p2.y = axesPts[i][1];	p2.z = axesPts[i][2];
		axes.points.push_back( p1 );
		axes.points.push_back( p2 );
		// Set Colors
		std_msgs::ColorRGBA c;
		
		c.r = axesClrs[i][0];	c.g = axesClrs[i][1];	c.b = axesClrs[i][2];
		c.a = 1.0;

		axes.colors.push_back( c );
		axes.colors.push_back( c );
	}
	return axes;
}

visualization_msgs::Marker get_straightPath_marker( Eigen::Vector3d bgn , Eigen::Vector3d end , 
												    float thickness , float R , float G , float B , float alpha ){
	
	Eigen::Vector3d offset = end - bgn;
	double crssFrac = 0.1; // fraction of the path equal to size of cross
	double pathLen  = offset.norm();
	double segLenH  = pathLen * crssFrac / 2.0;
	
	visualization_msgs::Marker path;
	path.header.frame_id = "world";
	path.header.stamp = ros::Time();
	path.ns = "my_namespace";
	path.id = 0;
	path.type = visualization_msgs::Marker::LINE_LIST;
	path.action = visualization_msgs::Marker::ADD;
	path.pose.position.x = (float) bgn(0);
	path.pose.position.y = (float) bgn(1);
	path.pose.position.z = (float) bgn(2);
	path.pose.orientation.x = 0.0;
	path.pose.orientation.y = 0.0;
	path.pose.orientation.z = 0.0;
	path.pose.orientation.w = 1.0;
	
	path.scale.x = thickness; // <-- Line Thickness
	
	path.color.r = 1.0;
	path.color.a = 1.0;
	geometry_msgs::Point p1, p2;
	
	std_msgs::ColorRGBA c;
	c.r = R;	c.g = G;	c.b = B;	c.a = alpha;
	
	// Path
	p1.x = 0;			p1.y = 0;			p1.z = 0;
	p2.x = offset(0);	p2.y = offset(1);	p2.z = offset(2);
	path.points.push_back( p1 );
	path.points.push_back( p2 );
	path.colors.push_back( c );
	path.colors.push_back( c );
	// Cross X
	p1.x = offset(0) - segLenH;  p1.y = offset(1);  p1.z = offset(2);
	p2.x = offset(0) + segLenH;  p2.y = offset(1);  p2.z = offset(2);
	path.points.push_back( p1 );
	path.points.push_back( p2 );
	path.colors.push_back( c );
	path.colors.push_back( c );
	// Cross Y
	p1.x = offset(0);  p1.y = offset(1) - segLenH;  p1.z = offset(2);
	p2.x = offset(0);  p2.y = offset(1) + segLenH;  p2.z = offset(2);
	path.points.push_back( p1 );
	path.points.push_back( p2 );
	path.colors.push_back( c );
	path.colors.push_back( c );
	// Cross Z
	p1.x = offset(0);  p1.y = offset(1);  p1.z = offset(2) - segLenH;
	p2.x = offset(0);  p2.y = offset(1);  p2.z = offset(2) + segLenH;
	path.points.push_back( p1 );
	path.points.push_back( p2 );
	path.colors.push_back( c );
	path.colors.push_back( c );
	
	return path;
}

// = Marker Diagnostics =

Pose_ASP get_mrkr_pose( visualization_msgs::Marker& vizMarker ){
	return pose_from_pXYZ_oWXYZ( vizMarker.pose.position.x    ,
								 vizMarker.pose.position.y    ,
								 vizMarker.pose.position.z    ,
								 vizMarker.pose.orientation.w , 
								 vizMarker.pose.orientation.x ,
								 vizMarker.pose.orientation.y ,
								 vizMarker.pose.orientation.z );
}

Eigen::MatrixXd get_mrkr_vertices( visualization_msgs::Marker& vizMarker ){
	size_t len = vizMarker.points.size();
	Eigen::MatrixXd rtnMatx = Eigen::MatrixXd::Zero( len , 3 );
	for( size_t i = 0 ; i < len ; i++ ){
		rtnMatx.row(i) << vizMarker.points[i].x , vizMarker.points[i].y , vizMarker.points[i].z;
	}
	return rtnMatx;
}

Eigen::MatrixXd get_mrkr_colors( visualization_msgs::Marker& vizMarker ){
	size_t len = vizMarker.colors.size();
	Eigen::MatrixXd rtnMatx = Eigen::MatrixXd::Zero( len , 4 );
	for( size_t i = 0 ; i < len ; i++ ){
		rtnMatx.row(i) << vizMarker.colors[i].r , vizMarker.colors[i].g , vizMarker.colors[i].b , vizMarker.colors[i].a ;
	}
	return rtnMatx;
}

SuccessCode check_marker_OK( visualization_msgs::Marker& vizMarker ){ 
	// Check that there are no { NaN , Infinity } in the marker
	SuccessCode rtnSuccess;
	bool poseOK = check_pose_OK( get_mrkr_pose( vizMarker )     );
	bool vertOK = check_matx_OK( get_mrkr_vertices( vizMarker ) );
	bool colrOK = check_matx_OK( get_mrkr_colors( vizMarker )   );
	rtnSuccess.success = poseOK && vertOK && colrOK;
	rtnSuccess.code = 0;
	rtnSuccess.desc = "";
	if( !poseOK ){
		rtnSuccess.code += 001;
		rtnSuccess.desc += "BAD POSE , ";
		cerr << "BAD POSE" << endl;
		cerr << get_mrkr_pose( vizMarker ) << endl;
	}
	if( !vertOK ){
		rtnSuccess.code += 010;
		rtnSuccess.desc += "BAD VERTICES , ";
		cerr << "BAD VERTICES" << endl;
		cerr << get_mrkr_vertices( vizMarker ) << endl;
	}
	if( !colrOK ){
		rtnSuccess.code += 100;
		rtnSuccess.desc += "BAD COLORS";
		cerr << "BAD COLORS" << endl;
		cerr << get_mrkr_colors( vizMarker ) << endl;
	}
	if( rtnSuccess.success ){  rtnSuccess.desc = "OK";  }
	return rtnSuccess;
}

// _ End Diagnostics _

/// ___ END VISUAL _________________________________________________________________________________________________________________________


/// === ASM GEOMETRY =======================================================================================================================

// == Struct Helpers ==

std::ostream& operator<<(std::ostream& os, const part_face& pf){
	os << "( part: " << pf.part << " , face: " << pf.face << " )";
	return os; // You must return a reference to the stream!
}

// __ End Helpers __


// == class Polygon_ASM ====================================================================================================================

// ~ Forward Declarations ~
//class RViz_MeshMarker;

// ~ Constructors & Destructors ~

Polygon_ASM::Polygon_ASM(){
	points2D = Eigen::MatrixXd( DEFAULTNUMPOINTS , 2 ); // Matrix of R2 points with respect to the polyon center
	normal3D << 0.0d , 0.0d , 0.0d; // - Normal vector of the plane in R3
	center3D << 0.0d , 0.0d , 0.0d; // - Center of the polygon in R3
	xBasis   << 0.0d , 0.0d , 0.0d; // - Center of the polygon in R3
	yBasis   << 0.0d , 0.0d , 0.0d; // - Center of the polygon in R3
	numPoints = 0; // -------------------------- Number of point in the polygon
}

// Vertices : Array of reduced vertices from the STL         , each row is a vertex
// Facets   : Array of vertex indices of each facet from STL , each row is a facet
// siDices  : vector of facet indices that comprise the side

typedef std::pair< int , int > index_match; // Not sure this is even needed!


Polygon_ASM::Polygon_ASM( const Eigen::MatrixXd& Vertices , const Eigen::MatrixXi& Facets , std::vector<int>& siDices , 
						  const Eigen::Vector3d& normalVec ){
	// NOTE: This function assumes that the number of vertices has already been reduced
	// NOTE: This function assumes that 'normalVec' has unit length
	
	// 0. Create a an index<-->index map for face vertices
	//~ std::map< int , int > vertexMap; // Mapping STL vertex indices --> face vertex indices
	
	//~ int nuDex = 0;
	
	bool SHOWDEBUG = false;
	
	size_t i      = 0 ,
		   j      = 0 ;
	llin   vCount = 0 ;
	
	if( SHOWDEBUG ){  
		if( siDices.size() < 1 ){  cout << "Polygon_ASM::Polygon_ASM: WARNING , ZERO-FACET FACE!" << endl;  }
		else{  cout << "Polygon_ASM::Polygon_ASM: Facet count OK!" << endl;  }
	}
		   
	//~ std::list<int> storedV; // List of vertices we have encountered before
	
	// Allocate an array for facets that is 'siDices.size()' rows long , 3 columns
	facets = Eigen::MatrixXi::Zero( siDices.size() , 3 );
	
	// Allocate an array for vertices that is 3X 'siDices.size()' rows long ( No Repeat vertices ) , 2 columns
	points2D = Eigen::MatrixXd::Zero( siDices.size() * 3 , 2 );
	
	// Allocate an array for vertices that is 3X 'siDices.size()' rows long ( No Repeat vertices ) , 3 columns
	vertcs3D = Eigen::MatrixXd::Zero( siDices.size() * 3 , 3 );
	
	// Allocate the part frame vertices
	vertsPrt = Eigen::MatrixXd::Zero( siDices.size() * 3 , 3 );
	
	// Determine bases from normal and first triangle and store
	Eigen::Vector3d zBasis , origin , scratch , pt1 , pt2; 
	zBasis = normalVec; // Normal is the Z basis vector , naturally
	pt1     = Vertices.row( Facets( siDices[0] , 0 ) );
	pt2     = Vertices.row( Facets( siDices[0] , 1 ) );
	scratch = Vertices.row( Facets( siDices[0] , 2 ) );
	yBasis = ( pt2 - pt1 ).normalized();
	xBasis = yBasis.cross( zBasis );
	origin = ( pt1 + pt2 + scratch ) / 3.0d; // The center of the first triangle is the center of the face
	// Now we have all the basis vectors and we are ready to flatten all of the vertices onto a plane
	
	// 1. Store repeat vertices 
	for( i = 0 ; i < siDices.size() ; i++ ){ // For each facet in the face
		for( j = 0 ; j < 3 ; j++ ){ // For each vertex
			
			// log the vertex in the facets list
			facets( i , j ) = vCount;
			
			if( SHOWDEBUG ){  
				if( vCount > 1000 || facets( i , j ) > 1000 ){  cout << "vCount is very high! " 
																	 << vCount << " , " << facets( i , j ) << endl; }
				else{ cout << "vCount is OK! " << vCount << " , " << facets( i , j ) << endl; }
			}
			
			// Store the 3D vertex
			vertcs3D.row( vCount ) = Vertices.row( Facets( siDices[i] , j ) ); // Lab frame
			vertsPrt.row( vCount ) = Vertices.row( Facets( siDices[i] , j ) ); // Part Frame
			
			// Project to 2D
			pt1 = vertcs3D.row( vCount );
			scratch = point_basis_change( pt1 , origin , xBasis , yBasis , zBasis );
			
			// Store the 2D Vertex
			points2D( vCount , 0 ) = scratch( 0 ); points2D( vCount , 1 ) = scratch( 1 ); 
			
			vCount++;
		}
	}
	
	// 2. Store pose information
	normal3D    = zBasis;
	center3D    = origin;
	orientation = basis_vecs_to_quat( xBasis , yBasis , zBasis );
	
	// 3. Allocate lab bases
	labBases = Eigen::MatrixXd::Zero( 3 , 3 );
	
	// 4. Generate border
	populate_CCW_polygon_verts_2D();
	
	// 5. Calculate area
	calc_area();
	
	// N. Report on what was created
	if( SHOWDEBUG ){ // Set to 'true' to show contents of the polygon
		cout << "Created a Polygon_ASM" << endl;
		cout << "\t2D Points" << endl << points2D << endl;
		cout << "\t3D Points" << endl << vertcs3D << endl; 
		cout << "\tFacets"    << endl << facets   << endl;
	}
}

Polygon_ASM::Polygon_ASM( const Polygon_ASM& original ){ 
	// Copy constructor
	ID /* ------------ */ = part_face{ original.ID.part , original.ID.face };
	numPoints /* ----- */ = original.numPoints;
	neighbors.clear(); // Not sure if this is needed
	facets /* -------- */ = original.facets; // Eigen assignment is copy
	
	if( facets.maxCoeff() != original.facets.maxCoeff() ){  
		cout << "Polygon_ASM COPY ERROR , MAX: " << facets.maxCoeff() << " , " << original.facets.maxCoeff() << endl;
	}
	
	facetNeighborsOrdered = vec_vec_copy( original.facetNeighborsOrdered );
	points2D /* ------ */ = original.points2D; // Eigen assignment is copy
	COMproj2D /* ----- */ = original.COMproj2D;
	CCWpolyV /* ------ */ = original.CCWpolyV;
	copy_Segment2D_vec_from_A_to_B( original.CCWborderSegments , CCWborderSegments );
	center3D /* ------ */ = original.center3D;
	xBasis /* -------- */ = original.xBasis;
	yBasis /* -------- */ = original.yBasis;
	normal3D /* ------ */ = original.normal3D;
	orientation /* --- */ = original.orientation;
	vertsPrt /* ------ */ = original.vertsPrt;
	COMproj3D /* ----- */ = original.COMproj3D;
	faceTransform /* - */ = original.faceTransform;
	vertcs3D /* ------ */ = original.vertcs3D;
	labBases /* ------ */ = original.labBases;
	labCentr /* ------ */ = original.labCentr;
	area /* ---------- */ = original.area;
}

// ~ Getters ~
size_t Polygon_ASM::get_len(){  return numPoints;  }
int Polygon_ASM::get_num_facets(){  return (int) facets.rows();  } // - Number of facets that make up the polygonal face
part_face Polygon_ASM::get_ID(){  return ID;  }
size_t Polygon_ASM::get_number_of_segments(){  return CCWborderSegments.size();  } // Return the number of border segments

// Return a trimesh of all of the facets of the face , part frame
TriMeshVFN_ASP Polygon_ASM::get_VFN(){  return TriMeshVFN_ASP{ vertsPrt , facets , repeat_vector3d( normal3D , facets.rows() ) };  }

double Polygon_ASM::calc_area(){  
	area = surface_area( get_VFN() );  
	return area;
}

// Return the area in [unit^2] , Where unit is the same as the original mesh length
double Polygon_ASM::get_area(){  return area;  }
	
Eigen::Vector3d Polygon_ASM::get_center(){ return center3D; } // --- Origin  , part frame
Eigen::Vector3d Polygon_ASM::get_X_basis(){ return xBasis; } // ---- X Basis , part frame
Eigen::Vector3d Polygon_ASM::get_Y_basis(){ return yBasis; } // ---- Y Basis , part frame
Eigen::Vector3d Polygon_ASM::get_norm(){ return normal3D; } // ----- Z Basis , part frame
Eigen::Vector3d Polygon_ASM::get_COM_proj_3D(){ return COMproj3D; } 

// ~ Setters ~
void Polygon_ASM::set_ID( llin partID , llin faceID ){ 
	ID.part = partID;
	ID.face = faceID;
}

void Polygon_ASM::set_2D_COM_proj( Eigen::Vector2d projCOM2D ){ COMproj2D = projCOM2D; } // ~ Local Frame ~
void Polygon_ASM::set_part_COM_proj( Eigen::Vector3d projCOM3D ){ COMproj3D = projCOM3D; } // ~ Part Frame ~

// ~ Lab Frame ~
void Polygon_ASM::set_lab_bases_from_part( Part_ASM& parentPart ){
	// Rotate each of the lab bases so that it is in the correct lab orientation
	Eigen::Vector3d xB;  Eigen::Vector3d yB;  Eigen::Vector3d zB;  
	Eigen::Quaterniond partOrient = parentPart.get_orientation();
	// Rotate part frame bases		&&	Assign to lab bases	
	xB = partOrient * get_X_basis();	labBases.row(0) = xB;
	yB = partOrient * get_Y_basis();	labBases.row(1) = yB;
	zB = partOrient * get_norm();		labBases.row(2) = zB;
}

Eigen::MatrixXd Polygon_ASM::get_2D_vertices(){ return points2D; } // Return the VFN vertices in 2D local frame
Eigen::Vector2d Polygon_ASM::get_2D_COM_proj(){ return COMproj2D; } // Return the part COM projected onto this poly , local frame ( 2D )
Eigen::MatrixXd Polygon_ASM::get_2D_CCWpolyV(){ return CCWpolyV; } // Get the vertices of the bounding polygon ___ , local frame ( 2D )

// ~ Geo Calcs ~
void Polygon_ASM::calc_lab_vertices( Eigen::Vector3d& partCenterLab , Eigen::Quaterniond& partOrientLab ){
	// Calculate the positions of all of the vertices in the lab frame
	// This is done in a way that the 'F' facet lookup will still be valid
	
	size_t i       = 0               , 
		   numRows = points2D.rows() ;

	Eigen::Vector3d temp;
	
	// 1. Set up the transformation for the part
	Eigen::Translation< double , 3 > partTrans( partCenterLab );
	
	// 2. Set up the transformation for the face
	Eigen::Translation< double , 3 > faceTrans( center3D );
	
	// 3. Compose the transformations
	// URL , Compose transformations from vectors and quaternions: https://stackoverflow.com/a/18790084/893511
	// URL , Apply transformation to vectors : https://eigen.tuxfamily.org/dox/group__TutorialGeometry.html#TutorialGeoTransform
	Eigen::Transform< double , 3 , Eigen::Affine > faceXform =  partTrans * partOrientLab * faceTrans * orientation;
	faceTransform = faceXform;
	
	// 4. Multiply each point by the composite transformation and populate the matrix holding vertices in the lab frame
	for( i = 0 ; i < numRows ; i++ ){
		temp << points2D( i , 0 ) , points2D( i , 1 ) , 0.0d ;
		vertcs3D.row( i ) = faceXform * temp;
	}
	
	// Eigen::Transform< double , 3 , Eigen::Affine > partXform = partOrientLab * partTrans;
	Eigen::Transform< double , 3 , Eigen::Affine > partXform = partTrans * partOrientLab;
	labBases.row( 0 ) = partOrientLab * xBasis;
	labBases.row( 1 ) = partOrientLab * yBasis;
	labBases.row( 2 ) = partOrientLab * normal3D; // zBasis
	//~ cout << "Transformed the polygon bases" << endl << labBases << endl;
	labCentr          = partXform * center3D; // face center in the lab frame
}

void Polygon_ASM::calc_lab_vertices( Pose_ASP labPose ){
	calc_lab_vertices( labPose.position , labPose.orientation );
}

Eigen::MatrixXd Polygon_ASM::get_lab_vertices(){ return vertcs3D; }

Eigen::Vector3d Polygon_ASM::get_lab_center(){ return labCentr; } // Return face center in the lab frame

Eigen::Vector3d Polygon_ASM::get_lab_norm(){ // - Return face normal vector in the lab frame
	Eigen::Vector3d rtnCenter = labBases.row( 2 );
	return rtnCenter;
} 

Eigen::MatrixXd Polygon_ASM::get_lab_border( bool closed ){ 
	// Return vertices that make up polygon border , lab frame
	size_t numSegs = CCWborderSegments.size();
	
	cout << "Number of segments: " << numSegs << endl;
	
	Eigen::MatrixXd rtnMatx;
	if( !closed ){  rtnMatx = Eigen::MatrixXd::Zero( numSegs   , 3 );  }
	else{           rtnMatx = Eigen::MatrixXd::Zero( numSegs+1 , 3 );  }
	size_t len = rtnMatx.rows();
	// 0. Fetch the basis vectors for the lab frame polygon
	Eigen::Vector3d xBasis = labBases.row(0);
	Eigen::Vector3d yBasis = labBases.row(1);
	for( size_t i = 0 ; i < numSegs ; i++ ){ // For each of the segments comprising the border
		// 1. Fetch the first point in the segment  &&  2. Make the point R3  &&  3. Store in the appropriate row 
		rtnMatx.row(i) = vec3d_from_arbitrary_2D_basis( CCWborderSegments[i].pnt1(0) , CCWborderSegments[i].pnt1(1) , xBasis , yBasis );
	}
	if( closed ){  rtnMatx.row( numSegs ) = rtnMatx.row( 0 );  } // Close the loop , if specified
	return rtnMatx;
}

Eigen::MatrixXi Polygon_ASM::get_facets(){ return facets; }

// Add a neighbor to the list
void Polygon_ASM::add_neighbor( part_face nuNeighbor ){ neighbors.push_back( nuNeighbor ); }

// Erase all neighbors from the list
void Polygon_ASM::clear_neighbors(){ neighbors.clear(); }

std::vector<std::vector<int>> Polygon_ASM::calc_ordered_facet_neighbors(){ 
	// Calculate , populate , and return ordered facet neighbors
	// facetNeighborsOrdered = facet_adjacency_list_ordered( facets );
	facetNeighborsOrdered = facet_adjacency_list_ordered_dense( 
		Matx2D_points_to_Matx3D_flat( points2D ) , 
		facets
	);
	return facetNeighborsOrdered;
}

Eigen::MatrixXd Polygon_ASM::project_lab_pnts_onto_3D_plane_2D( Eigen::Vector3d planeOrgn , 
																Eigen::Vector3d planeNorm , Eigen::Vector3d planeXbas ){
	// NOTE: 'planeXbas' does not have to be strictly in-plane
	// NOTE: This function assumes that the lab points have already been transformed
	Eigen::Vector3d queryPnt;
	size_t len = vertcs3D.rows();
	Eigen::MatrixXd rtnPts = Eigen::MatrixXd::Zero( len , 2 );
	for( size_t i = 0 ; i < len ; i++ ){
		queryPnt = vertcs3D.row(i);
		rtnPts.row(i) = pnt_proj_onto_plane_2D( queryPnt , planeOrgn , planeNorm , planeXbas );
	}
	return rtnPts;
}

Eigen::MatrixXd Polygon_ASM::project_lab_brdr_onto_3D_plane_2D( Eigen::Vector3d planeOrgn , Eigen::Vector3d planeNorm , Eigen::Vector3d planeXbas , 
													            bool close ){
	// NOTE: 'planeXbas' does not have to be strictly in-plane
	// NOTE: This function assumes that the lab points have already been transformed
	Eigen::Vector3d queryPnt;
	Eigen::MatrixXd vertcs = get_lab_border( close );
	
	cout << "Retrieved a border with " << vertcs.rows() << " vertices." << endl;
	
	size_t len = vertcs.rows();
	Eigen::MatrixXd rtnPts = Eigen::MatrixXd::Zero( len , 2 );
	for( size_t i = 0 ; i < len ; i++ ){
		queryPnt = vertcs.row(i);
		rtnPts.row(i) = pnt_proj_onto_plane_2D( queryPnt , planeOrgn , planeNorm , planeXbas );
	}
	return rtnPts;
}

bool Polygon_ASM::is_adjacent_to( Polygon_ASM& other , double CRIT_ANG , double CRIT_DST ){ 
	// Return true if one of this poly's facets are adjacent to any facet of 'other' , Otherwise return false
	// NOTE: This function assumes that the lab frame facents have been properly transformed
	// NOTE: This function assumes that if one of the traingles of this polygon is touching and opposing any triangle of 'other', 
	//       then the polygons can be considered adjacent to each other
	
	size_t polyLen = get_num_facets();
	size_t othrLen = other.get_num_facets();
	Eigen::MatrixXd currPolyTri( 3 , 3 );
	Eigen::MatrixXd currOthrTri( 3 , 3 );
	bool SHOWDEBUG = false , 
		 CHECKINDX = false ;
		 
	if( CHECKINDX ){
		cout << "This polygon indices OK?:  " << F_indices_OK( vertcs3D , facets ) << " , How many?: " << polyLen 
			 << " , Max Index: " << facets.maxCoeff() << endl;
		cout << facets << endl;
		cout << "Other polygon indices OK?: " << F_indices_OK( other.vertcs3D , other.facets ) << " , How many?: " << othrLen 
			 << " , Max Index: " << other.facets.maxCoeff() << endl << endl;
		cout << other.facets << endl;
	}
	
	// 1. For each triangle in this
	for( size_t i = 0 ; i < polyLen ; i++ ){ 
		
		if( SHOWDEBUG ){  cout << "About to fetch the first triangle ..." << endl;  }
		
		// 2. Fetch triangle
		currPolyTri = get_triangle_i( vertcs3D , facets , i );
		// 3. For each triangle in other
		for( size_t j = 0 ; j < othrLen ; j++ ){ 
			
			if( SHOWDEBUG ){  cout << "About to fetch the second triangle ..." << endl;  }
			
			// 4. Fetch triangle
			currOthrTri = get_triangle_i( other.vertcs3D , other.facets , j );
			
			if( SHOWDEBUG ){  cout << "About to run the adjacency check ..." << endl;  }
			
			// 5. Check if the triangles are adjacent
			if( are_those_triangles_adjacent( currPolyTri , currOthrTri , CRIT_ANG , CRIT_DST ) ){  return true;  }
		}
	}
	return false;
}

Eigen::MatrixXd Polygon_ASM::get_lab_bases(){  return labBases;  }

double Polygon_ASM::adjacent_area( Polygon_ASM& other , size_t lvl ){ 
	// Get the area shared between polygons, assuming that they are adjacent
	// NOTE: This function assumes that this polygon and the 'other' are adjacent
	
	size_t numTris = get_num_facets()       , 
		   numOthr = other.get_num_facets() ; 
	double A_share = 0.0;
	Eigen::MatrixXd triA;  Eigen::MatrixXd triB;
	
	// 1. Project the triangles of each onto this polygon, and store them
	Eigen::Vector3d xB = labBases.row(0);
	Eigen::Vector3d zB = labBases.row(2);
	Eigen::Vector3d cn = get_lab_center();
	
	Eigen::MatrixXd flatTris = project_lab_pnts_onto_3D_plane_2D( cn , zB , xB );
	Eigen::MatrixXd othrTris = other.project_lab_pnts_onto_3D_plane_2D( cn , zB , xB );
	Eigen::MatrixXi trisF    = get_facets();
	Eigen::MatrixXi othrF    = other.get_facets();
	
	// 2. For each triangle in this polygon
	for( size_t i = 0 ; i < numTris ; i++ ){
		triA = get_triangle_i( flatTris , trisF , i );  
		// 3. For each triangle in other polygon
		for( size_t j = 0 ; j < numOthr ; j++ ){
			triB = get_triangle_i( othrTris , othrF , j );  
			// 4. Accumulate the shared area
			A_share += overlapping_tri_area( triA , triB , lvl );
		}
	}
	return A_share;
}

Eigen::MatrixXd Polygon_ASM::populate_CCW_polygon_verts_2D(){
	// Return a matrix of CCW vertices associated with this polygon
	
	// 1. Get the adjacency list for all of the facets that belong to this face
	std::vector<std::vector<int>> orderedNeighbors = facet_adjacency_list_ordered( facets );
	
	// 2. Get the edge segments
	std::list<Segment2D_ASP> segLst = get_outside_CCW_poly_from_mesh_subset( points2D , facets , 
							  	  						                     orderedNeighbors , 
																             vec_range( (int) 0 , (int) facets.rows() ) );
	// 3. Store the segments
	CCWborderSegments = list_to_vec( segLst );

	// 4. Package the border verts as a matrix and return
	size_t numPts = segLst.size() , 
	       i      = 0             ;
	CCWpolyV = Eigen::MatrixXd::Zero( numPts , 2 );
	CCWpolyV.row( i ) = segLst.front().pnt1;
	std::list<Segment2D_ASP>::const_iterator iterator;
	for (iterator = segLst.begin(); iterator != segLst.end(); ++iterator) {
		i++;
		CCWpolyV.row( i ) = (*iterator).pnt2;
	}
	
	return CCWpolyV;
}

void Polygon_ASM::set_2D_CCWpolyV( Eigen::MatrixXd borderVerts ){ CCWpolyV = borderVerts; }

void Polygon_ASM::set_2D_CCWborderSegments( std::vector<Segment2D_ASP> borderSegments ){ 
	copy_Segment2D_vec_from_A_to_B( borderSegments , CCWborderSegments );
}

std::vector<Polygon_ASM> copy_Polygon_ASM_vector( std::vector<Polygon_ASM>& original ){
	std::vector<Polygon_ASM> rtnVec;
	size_t len = original.size();
	// For every Polygon_ASM in the original , Make a copy and add to the return vector
	for( size_t i = 0 ; i < len ; i++ ){  rtnVec.push_back( Polygon_ASM( original[i] ) );  }
	return rtnVec;
}

std::vector<Polygon_ASM> copy_Polygon_ASM_vector( const std::vector<Polygon_ASM>& original ){
	std::vector<Polygon_ASM> rtnVec;
	size_t len = original.size();
	// For every Polygon_ASM in the original , Make a copy and add to the return vector
	for( size_t i = 0 ; i < len ; i++ ){  rtnVec.push_back( Polygon_ASM( original[i] ) );  }
	return rtnVec;
}

bool area_A_lessThan_B( Polygon_ASM& A , Polygon_ASM& B ){  return A.get_area() < B.get_area();  }
bool area_A_grtrThan_B( Polygon_ASM& A , Polygon_ASM& B ){  return A.get_area() > B.get_area();  }

std::vector<Polygon_ASM> copy_Poly_vec_decreasing_area( std::vector<Polygon_ASM>& original ){
	// Return a copy of the vector sorted by area
	std::vector<Polygon_ASM> rtnVec = copy_Polygon_ASM_vector( original );
	std::sort( rtnVec.begin() , rtnVec.end() , area_A_grtrThan_B );
	return rtnVec;
}

std::vector<Polygon_ASM> Polygon_ASM_vector_at_pose( std::vector<Polygon_ASM>& original , Pose_ASP labPose ){
	// NOTE: This is a bit hacky because I still want to use the same polygon vector comparison function
	std::vector<Polygon_ASM> rtnVec = copy_Polygon_ASM_vector( original ); // Copy the original polygons
	size_t len = rtnVec.size();
	for( size_t i = 0 ; i < len ; i++ ){ // Now transform each of the polygons
		rtnVec[i].calc_lab_vertices( labPose );
		rtnVec[i].center3D = rtnVec[i].labCentr;
		rtnVec[i].xBasis   = rtnVec[i].labBases.row(0);
		rtnVec[i].yBasis   = rtnVec[i].labBases.row(1);
		rtnVec[i].normal3D = rtnVec[i].labBases.row(2);
	}
	return rtnVec;
}

IndexSearchResult match_vec2_poly_w_vec1_index( std::vector<Polygon_ASM>& vec1 , std::vector<Polygon_ASM>& vec2 , 
												size_t index , double CRIT_ANG , double CRIT_DST ){
	// Find the index of the polygon in 'vec2' that is coplanar the polygon in 'vec1' at 'index'
	
	//~ polygon_vec_report( vec1 ); // DEBUG
	//~ polygon_vec_report( vec2 ); // DEBUG
	
	IndexSearchResult result;
	result.result = false;
	result.index  = 9999;
	size_t len = vec2.size();
	if( index < vec1.size() ){
		// 0. Fetch the center and the z_Basis of the query poly
		Eigen::Vector3d qCenter = vec1[ index ].get_center();
		Eigen::Vector3d qZBasis = vec1[ index ].get_norm();
		Eigen::Vector3d polyCen , polyNrm;
		// 1. For each of the polys in 'vec2'
		for( size_t i = 0 ; i < len ; i++ ){
			// 2. Fetch the center and the z_Basis
			polyCen = vec2[ i ].get_center();
			polyNrm = vec2[ i ].get_norm();
			// 3. Find the projected distance from the center to the query poly and check if that distance <= 'CRIT_DST'
			if( abs( dist_to_plane( qZBasis , qCenter , polyCen ) ) <= CRIT_DST ){
				// 4. Find the angle between the z_Bases and check if that angle <= 'CRIT_ANG'
				if( angle_between( polyNrm , qZBasis ) <= CRIT_ANG ){
					// 5. If both criteria are met , store the index and break
					result.result = true;
					result.index  = i;
					//~ cout << "DEBUG , " << "Match is:      " << i << endl;
					//~ cout << "DEBUG , " << "Normals:       " << qZBasis << " , " << polyNrm << endl;
					//~ cout << "DEBUG , " << "Angle Between: " << angle_between( polyNrm , qZBasis ) << endl;
					//~ cout << "DEBUG , " << "Centers:       " << qCenter << " , " << polyCen << endl;
					//~ cout << "DEBUG , " << "Dist. Between: " << dist_to_plane( qZBasis , qCenter , polyCen ) << endl;
					break;
				}
			}
		} // 6. If there is no match, return failure
	}
	return result;
}

IndexSuccesLookup best_match_vec2_poly_w_vec1_index( std::vector<Polygon_ASM>& vec1 , std::vector<Polygon_ASM>& vec2 , 
													 size_t index , double CRIT_ANG , double CRIT_DST ){
	// Find the index of the polygon in 'vec2' that is coplanar the polygon in 'vec1' at 'index'
	
	//~ polygon_vec_report( vec1 ); // DEBUG
	//~ polygon_vec_report( vec2 ); // DEBUG
	
	IndexSuccesLookup result;

	size_t len = vec2.size();
	
	double score     =   0.0 , 
		   bestScore = -50.0 ,
		   distance  =   0.0 ,
		   angle     =   0.0 ;
	
	if( index < vec1.size() ){
		// 0. Fetch the center and the z_Basis of the query poly
		Eigen::Vector3d qCenter = vec1[ index ].get_center();
		Eigen::Vector3d qZBasis = vec1[ index ].get_norm();
		Eigen::Vector3d polyCen , polyNrm;
		// 1. For each of the polys in 'vec2'
		for( size_t i = 0 ; i < len ; i++ ){
			result.indices.push_back( i );
			// 2. Fetch the center and the z_Basis
			polyCen = vec2[ i ].get_center();
			polyNrm = vec2[ i ].get_norm();
			// 3. Find the projected distance from the center to the query poly and check if that distance <= 'CRIT_DST'
			distance = abs( dist_to_plane( qZBasis , qCenter , polyCen ) );
			// 4. Find the angle between the z_Bases and check if that angle <= 'CRIT_ANG'
			angle    = angle_between( polyNrm , qZBasis );
			// 5. Calculate score
			score = CRIT_DST / max( CRIT_DST , distance ) + CRIT_ANG / max( CRIT_ANG , angle ); // If both criteria are met, score == 2
			result.scoreVec.push_back( score );
			
			if( eq( score , 2.0d ) ){  
				result.result = true;  
				result.flagVec.push_back( true );
			}else{  result.flagVec.push_back( false );  }
			
			if( score > bestScore ){
				bestScore = score;
				result.bestDex = i;
			}
		} 
	}
	return result;
}

IndexMatchResult best_match_vec1_poly_to_vec2_poly( std::vector<Polygon_ASM>& vec1 , std::vector<Polygon_ASM>& vec2 , 
													double CRIT_ANG , double CRIT_DST ){
	// Find the matching pair of indices that are closest to being the same support  // sideA -> vec1 , sideB -> vec2
	
	//~ polygon_vec_report( vec1 ); // DEBUG
	//~ polygon_vec_report( vec2 ); // DEBUG
	
	IndexMatchResult result;
	result.match = false;

	size_t len1 = vec1.size() ,
		   len2 = vec2.size() ;
	
	double score     =   0.0 , 
		   bestScore = -50.0 ,
		   distance  =   0.0 ,
		   angle     =   0.0 ;
		   
	std::pair<size_t,size_t> bestPair = std::make_pair( (size_t)0 , (size_t)0 );
	
	Eigen::Vector3d polyCen , polyNrm;
	Eigen::Vector3d qCenter , qZBasis;
	
	// 1. For each of the polys in 'vec1'
	for( size_t i = 0 ; i < len1 ; i++ ){
		// 2. Fetch the center and the z_Basis of the query poly
		qCenter = vec1[ i ].get_center();
		qZBasis = vec1[ i ].get_norm();
		// 3. For each of the polys in 'vec2'
		for( size_t j = 0 ; j < len2 ; j++ ){
			// 4. Fetch the center and the z_Basis
			polyCen = vec2[ j ].get_center();
			polyNrm = vec2[ j ].get_norm();
			// 5. Find the projected distance from the center to the query poly and check if that distance <= 'CRIT_DST'
			distance = abs( dist_to_plane( qZBasis , qCenter , polyCen ) );
			// 6. Find the angle between the z_Bases and check if that angle <= 'CRIT_ANG'
			angle    = angle_between( polyNrm , qZBasis );
			// 7. Calculate score
			score = CRIT_DST / max( CRIT_DST , distance ) + CRIT_ANG / max( CRIT_ANG , angle ); // If both criteria are met, score == 2
			if( eq( score , 2.0d ) ){  
				result.match = true;  
			}
			if( score > bestScore ){
				bestScore = score;
				bestPair  = std::make_pair( i , j );
			}
		}
	}
	result.sideA = std::get<0>( bestPair ); 
	result.sideB = std::get<1>( bestPair );
	return result;
}

std::vector<std::pair<size_t,size_t>> all_matches_vec1_poly_to_vec2_poly( std::vector<Polygon_ASM>& vec1 , std::vector<Polygon_ASM>& vec2 , 
													                      double CRIT_ANG , double CRIT_DST ){
	// Find the matching pair of indices that are closest to being the same support  // sideA -> vec1 , sideB -> vec2
	
	//~ polygon_vec_report( vec1 ); // DEBUG
	//~ polygon_vec_report( vec2 ); // DEBUG
	
	std::vector<std::pair<size_t,size_t>> rtnPairs;

	size_t len1 = vec1.size() ,
		   len2 = vec2.size() ;
	
	double score     =   0.0 , 
		   bestScore = -50.0 ,
		   distance  =   0.0 ,
		   angle     =   0.0 ;
	
	Eigen::Vector3d polyCen , polyNrm;
	Eigen::Vector3d qCenter , qZBasis;
	
	// 1. For each of the polys in 'vec1'
	for( size_t i = 0 ; i < len1 ; i++ ){
		// 2. Fetch the center and the z_Basis of the query poly
		qCenter = vec1[ i ].get_center();
		qZBasis = vec1[ i ].get_norm();
		// 3. For each of the polys in 'vec2'
		for( size_t j = 0 ; j < len2 ; j++ ){
			// 4. Fetch the center and the z_Basis
			polyCen = vec2[ j ].get_center();
			polyNrm = vec2[ j ].get_norm();
			// 5. Find the projected distance from the center to the query poly and check if that distance <= 'CRIT_DST'
			distance = abs( dist_to_plane( qZBasis , qCenter , polyCen ) );
			// 6. Find the angle between the z_Bases and check if that angle <= 'CRIT_ANG'
			angle    = angle_between( polyNrm , qZBasis );
			// 7. Calculate score
			score = CRIT_DST / max( CRIT_DST , distance ) + CRIT_ANG / max( CRIT_ANG , angle ); // If both criteria are met, score == 2
			if( eq( score , 2.0d ) ){  
				rtnPairs.push_back( std::make_pair( i , j ) );
			}
		}
	}
	return rtnPairs;
}

bool is_arg_on_side_0_pair( size_t arg , const std::vector<std::pair<size_t,size_t>>& indexPairs ){
	size_t len = indexPairs.size();
	for( size_t i = 0 ; i < len ; i++ ){
		if( arg == std::get<0>( indexPairs[i] ) ){  return true;  }
	}
	return false;
}

bool is_arg_on_side_1_pair( size_t arg , const std::vector<std::pair<size_t,size_t>>& indexPairs ){
	size_t len = indexPairs.size();
	for( size_t i = 0 ; i < len ; i++ ){
		if( arg == std::get<1>( indexPairs[i] ) ){  return true;  }
	}
	return false;
}

void polygon_vec_report( std::vector<Polygon_ASM>& polyVec ){
	// Print information about the polygons
	sep( "Polygon Vector Report" , 2 , '~' );
	size_t len = polyVec.size();
	cout << "There are " << len << " polygons" << endl;
	for( size_t i = 0 ; i < len ; i++ ){ // For every polygon
		cout << "Center: " << polyVec[i].get_center() << " , Normal: " << polyVec[i].get_norm() 
		     << " , Sides: "  << polyVec[i].get_number_of_segments() << endl;
	}
}

// __ End Polygon_ASM ______________________________________________________________________________________________________________________


// == class Part_ASM =======================================================================================================================

/**
For now, not storing the STL data inside of the object , only the path to it

The center of the part is the center of the STL
    
**/

Part_ASM::Part_ASM( llin partID ){ // Default constructor
	ID       = partID;
	meshPath = "";
}

Part_ASM::Part_ASM( llin partID , string STLpath , // Manual Constructor
				    Eigen::MatrixXd verticesDense   , Eigen::MatrixXi facetsDense   , 
				    Eigen::MatrixXd verticesReduced , Eigen::MatrixXi facetsReduced , 
				    std::vector< std::vector<int> > facesList ,
				    std::vector< Eigen::Vector3d , Eigen::aligned_allocator< Eigen::Vector3d > > faceNormals ){ // Sides constructor
			  
	ID          = partID;
	meshPath    = STLpath; // Path to the STL source used to generate

	position    = Eigen::Vector3d(); // ----- Center of the part in R3 , lab frame
	orientation = Eigen::Quaterniond( 1 , 0 , 0 , 0 ); // Orientation in the lab frame
	

	vertcs3D    = verticesDense; //- Dense points of the mesh
	facets      = facetsDense; // -- Facets corresponding to the dense points
	reducdVtx   = verticesReduced; // Reduced points of the mesh
	reducdFct   = facetsReduced; // Facets corresponding to the reduced points
	reducdNrm   = N_from_VF( reducdVtx , reducdFct );
	
	// 1. Calc COM & Volume , store
	VolumeCentroid volCen = get_VolumeCentroid_from_V_F( get_dense_vertices() , get_dense_facets() );
	COM    = volCen.centroid; // Should be in the part frame
	volume = volCen.volume;
	move_COM_to_origin();
	
	std::vector<int> faceRow;
	llin I        = 0 ,
		 J        = 0 ,
	     leastDex = 0 ;
	Polygon_ASM facePoly;
	
	// 2. For each of the identified face lists , construct a Polygon object and append it to the vector
	for( I = 0 ; I < (llin)facesList.size() ; I++ ){
		
		faceRow = facesList[I];
		
		leastDex = min_num_in_vec( faceRow ); // Get the least facet index from the facet list for this face
		
		//~ cout << "DEBUG: Least in row: " << leastDex << endl;
		
		// Create a poly from each face
		facePoly = Polygon_ASM( verticesReduced , 
								facetsReduced , 
								faceRow , // faceLists2[i] , 
								Vec3D_from_aligned_stdvec( faceNormals , I ) );
		facePoly.set_ID( ID , leastDex ); // Set the ID of the polygon to be the least index of its facets
		faces.push_back( facePoly );
	}
	
	// 4. Store the mesh , Fetch the mesh and pose data from the part
	partMrkr = RViz_MeshMarker( *this ); 
	populate_face_markers();
	// A. Assign a random color , This will remain throughout its life
	rand_part_mrkr_color();
	
	// 5. Calc surf area
	calc_surface_area();
	
	// 6. Populate collision geo
	reload_collision_model();
	
	cout << "Successfully created a Part with " << faces.size() << " faces." << endl;
}

void Part_ASM::populate_face_markers(){
	size_t i = 0                   ,
		   numFaces = faces.size() ; 
		   
	bool SHOWDEBUG = false; // if( SHOWDEBUG ){  cout << "" << endl;  }
    
    if( SHOWDEBUG ){  sep( "Part_ASM::populate_face_markers" );  }

	if( SHOWDEBUG ){  cout << "DEBUG , " << "About to clear the old face markers ..." << endl;  }
		   
	// 1. Clear the old face markers , if they exist
	faceMrkrs.clear();
	
	if( SHOWDEBUG ){  cout << "DEBUG , " << "About to create the new face markers ..." << endl;  }
	
	// 2. Creat new markers from part frame vertex data
	for( i = 0 ; i < numFaces ; i++ ){
		if( SHOWDEBUG ){  cout << "DEBUG , " << "About to instantiate marker ..." << endl;  }
		RViz_MeshMarker temp{ faces[i].vertsPrt , // Eigen::MatrixXd& verticesDense 
			                  faces[i].facets  }; // Eigen::MatrixXi& facetsDense
		if( SHOWDEBUG ){  cout << "DEBUG , " << "About to assign color ..." << endl;  }
		temp.rand_color(); // --------- Set the face to a random color
		if( SHOWDEBUG ){  cout << "DEBUG , " << "About to add to vector ..." << endl;  }
		faceMrkrs.push_back( temp ); // Add the marker to the vector
	}
	if( SHOWDEBUG ){  cout << "DEBUG , " << "Done adding!" << endl;  }
	if( SHOWDEBUG ){  cout << "DEBUG , " << "There are " << faceMrkrs.size() << " face markers." << endl;  }
}

Part_ASM::Part_ASM( llin partID , string STLpath ){ // Automatic constructor
	
	bool SHOWDEBUG = false;
	
	ID          = partID;
	meshPath    = STLpath; // Path to the STL source used to generate

	position    = Eigen::Vector3d(); // ----- Center of the part in R3 , lab frame
	orientation = Eigen::Quaterniond( 1 , 0 , 0 , 0 ); // Orientation in the lab frame

	// Setup for loading
	Eigen::MatrixXd OV , V , N_faces; 
	Eigen::MatrixXi OF , F , SVI , SVJ;
	// Load and Reduce mesh
	igl::read_triangle_mesh( STLpath , OV , OF );
	igl::remove_duplicate_vertices( OV , OF , 0.0 , V , SVI , SVJ , F );
	
	vertcs3D    = OV; // Dense points of the mesh
	facets      = OF; // Facets corresponding to the dense points
	reducdVtx   = V; //- Reduced points of the mesh
	reducdFct   = F; //- Facets corresponding to the reduced points
	reducdNrm   = N_from_VF( reducdVtx , reducdFct );
	
	if( SHOWDEBUG ){  cout << "DEBUG , " << "About to calc centroid ... " << endl;  }
	
	// 1. Calc COM & Volume , store
	VolumeCentroid volCen = get_VolumeCentroid_from_V_F( get_dense_vertices() , get_dense_facets() );
	COM    = volCen.centroid; // Should be in the part frame
	volume = volCen.volume;
	move_COM_to_origin();
	
	igl::per_face_normals( V , F , N_faces ); // Compute per-face normals
	
	double CRITRN_ANG = 0.17453d; // Angle criterion [rad] // 0.17453 rad == 10 deg
	
	if( SHOWDEBUG ){  cout << "DEBUG , " << "About to calc faces ... " << endl;  }
	
	FaceIndices rtnStruct = faces_from_facets( V , F , N_faces , CRITRN_ANG );
	
	std::vector<std::vector<int>> faceLists /* <----------------------------------------- */ = rtnStruct.faceLists;
	std::vector< Eigen::Vector3d , Eigen::aligned_allocator< Eigen::Vector3d > > faceNormals = rtnStruct.faceNormals;
	
	if( SHOWDEBUG ){  
		cout << "About to create faces with length " << vec_vec_len( faceLists ) << endl;
		print_vec_vec( faceLists );
	}
	
	std::vector<int> faceRow;
	int I        = 0                ,
		J        = 0                ,
		numFaces = faceLists.size() ,
		leastDex = 0                ;
	Polygon_ASM facePoly;
	
	if( SHOWDEBUG ){  cout << "DEBUG , " << "About to calc face borders ... " << endl;  }
	
	// 2. For each of the identified face lists , construct a Polygon object and append it to the vector
	for( I = 0 ; I < numFaces ; I++ ){
		faceRow = faceLists[I];
		leastDex = min_num_in_vec( faceRow ); // Get the least facet index from the facet list for this face
		
		if( SHOWDEBUG ){  cout << "DEBUG: Least in row: " << leastDex << endl;  }
		
		// Create a poly from each face
		facePoly = Polygon_ASM( reducdVtx , 
								reducdFct , 
								faceRow , 
								Vec3D_from_aligned_stdvec( faceNormals , I ) ); // Note that the constructor unreduces vertices!
		facePoly.set_ID( ID , leastDex ); // Set the ID of the polygon to be the least index of its facets
		faces.push_back( facePoly );
	}
	
	if( SHOWDEBUG ){  
		cout << "DEBUG , Detected faces versus loaded faces: " << numFaces << " , " << faces.size() << endl;
		cout << "DEBUG , " << "About to create marker ... " << endl;
	}
	
	// 3. Store the mesh , Fetch the mesh and pose data from the part
	partMrkr = RViz_MeshMarker( *this ); 
	populate_face_markers();
	// A. Assign a random color , This will remain throughout its life
	rand_part_mrkr_color();
	
	if( SHOWDEBUG ){  cout << "DEBUG , " << "About to calc surface area ... ";  }
	
	// 4. Calc surf area
	calc_surface_area();
	
	if( SHOWDEBUG ){  cout << "DEBUG , " << "About to load collision model ... ";  }
	
	// 5. Populate collision geo
	reload_collision_model();
	
	if( SHOWDEBUG ){  cout << "Loaded!" << endl;  }
}

Part_ASM::Part_ASM( const Part_ASM& original ){ 
	// Copy Constructor
	meshPath     = original.meshPath; // Source mesh is the same
	ID           = original.ID; // + ASP_COPY_OFFSET; // No offset
	position     = original.position; // Eigen assignment is copy
	orientation  = original.orientation; // Eigen assignment is copy
	faces        = copy_Polygon_ASM_vector( original.faces );
	vertcs3D     = original.vertcs3D;
	facets       = original.facets;
	reducdVtx    = original.reducdVtx;
	reducdFct    = original.reducdFct;
	reducdNrm    = original.reducdNrm;
	COM          = original.COM;
	hullVFN      = copy_TriMeshVFN_ASP( original.hullVFN );
	surfaceArea  = original.surfaceArea;
	supports     = copy_Polygon_ASM_vector( original.supports );
	stableFace   = vec_copy( original.stableFace );
	spptEdgeDist = vec_vec_copy( original.spptEdgeDist );
	leastDist    = original.leastDist;
	volume       = original.volume;
	partMrkr     = RViz_MeshMarker( *this );
	partMrkr.set_color( original.partMrkr.get_uniform_color() ); // This will also reload the color
	populate_face_markers();
	//~ collsnGeo.load_RAPID_model( vertcs3D , facets );
	reload_collision_model();
	
	// NOTE: For now not copying local part freedom , Assuming that this will be populated in the proper context
}

Part_ASM::~Part_ASM(){ // Default destructor
	faces.clear();
}

void Part_ASM::calc_all_faces(){  // Calculate the lab positions of all of face
	size_t faceNum = faces.size() , 
	       i       = 0            ;
	for( i = 0 ; i < faceNum ; i ++ ){ 
		faces[i].set_lab_bases_from_part( *this );
		faces[i].calc_lab_vertices( position , orientation ); 
	}
}

void Part_ASM::status_report(){ // Print some info to help troubleshoot whether the poly was properly formed
	cout << "ID                  : " << ID                              << endl
		 << "Path to mesh source : " << meshPath                        << endl
	     << "Position            : " << Eig_vec3d_to_str( position )    << endl
	     << "Orientation         : " << Eig_Quatd_to_str( orientation ) << endl
	     << "Number of vertices  : " << vertcs3D.rows()                 << endl
	     << "Number of facets    : " << facets.rows()                   << endl
	     << "Number of faces     : " << faces.size()                    << endl;
}

Eigen::MatrixXd& Part_ASM::get_dense_vertices(){ return vertcs3D; }

const Eigen::MatrixXd& Part_ASM::get_dense_vertices() const { return vertcs3D; }

Eigen::MatrixXi& Part_ASM::get_dense_facets(){ return facets; }

const Eigen::MatrixXi& Part_ASM::get_dense_facets() const { return facets; }

Eigen::MatrixXi& Part_ASM::get_reduc_facets(){ return reducdFct; }

size_t Part_ASM::get_number_of_faces(){ return faces.size(); }

TriMeshVFN_ASP Part_ASM::get_dense_VFN(){ 
	// Fetch the dense trimesh
	TriMeshVFN_ASP rtnStruct;
	rtnStruct.V = vertcs3D;
	rtnStruct.F = facets;
	rtnStruct.N = reducdNrm; // These should be the same as the dense mesh
	return rtnStruct;
}

double Part_ASM::calc_surface_area(){
	 // Return the surface area of the part mesh
	 TriMeshVFN_ASP partMesh = get_dense_VFN();
	 surfaceArea = surface_area( partMesh );
	 return surfaceArea;
}

double Part_ASM::get_surface_area(){  return surfaceArea;  }

Eigen::Vector3d& Part_ASM::get_position(){ return position; }

Eigen::Quaterniond& Part_ASM::get_orientation(){ return orientation; }

Pose_ASP Part_ASM::get_pose(){ return Pose_ASP{ position , orientation }; } // 2018-01-31: For now, not storing the pose in its own var

Pose_ASP Part_ASM::get_pose_w_matx(){ return pose_from_position_orient( position , orientation ); } // 2018-01-31: For now, not storing the pose in its own var

TriMeshVFN_ASP Part_ASM::get_lab_VFN(){
	// Get a copy of the part mesh in the Lab Frame , sparse
	TriMeshVFN_ASP labMesh;
	labMesh.V = transform_V( reducdVtx , position , orientation );
	labMesh.F = reducdFct; // This should remain the same
	labMesh.N = transform_Dir( reducdNrm , orientation );
	labMesh.center = position;
	return labMesh;
}

Eigen::Vector3d Part_ASM::get_shift(){  return shiftVec;  } // Part Frame

void Part_ASM::set_position( const Eigen::Vector3d& pos , bool updateAuxGeo ){ 
	position = pos;
	if( updateAuxGeo ){ // If there are no other changes to make , Update all of the internal lab-fram geometry
		update_part_mrkr_pose();
		update_all_face_mrkr_pose();
		collsnGeo.load_RAPID_transform( get_pose_w_matx() );
	}
}

void Part_ASM::set_orientation( const Eigen::Quaterniond& orient , bool updateAuxGeo ){ 
	orientation = orient;  orientation.normalize();
	if( updateAuxGeo ){ // If there are no other changes to make , Update all of the internal lab-fram geometry
		update_part_mrkr_pose();
		update_all_face_mrkr_pose();
		collsnGeo.load_RAPID_transform( get_pose_w_matx() );
	}
}

void Part_ASM::set_pose( const Eigen::Vector3d& pos , const Eigen::Quaterniond& orient , bool updateAuxGeo ){
	set_position( pos , false ); // Set the position , Delay geo update
	set_orientation( orient , false ); orientation.normalize(); // Set the orientation and normalize , Delay geo update
	if( updateAuxGeo ){ // If there are no other changes to make , Update all of the internal lab-fram geometry
		update_part_mrkr_pose();
		update_all_face_mrkr_pose();
		collsnGeo.load_RAPID_transform( get_pose_w_matx() );
	}
}

void Part_ASM::set_pose( double pX , double pY , double pZ , 
						 double oW , double oX , double oY , double oZ , 
						 bool updateAuxGeo ){
	position << pX , pY , pZ; // Set the position
	orientation.w() = oW;  orientation.x() = oX;  orientation.y() = oY;  orientation.z() = oZ; // Set the orientation and normalize
	orientation.normalize();
	if( updateAuxGeo ){ // If there are no other changes to make , Update all of the internal lab-fram geometry
		update_part_mrkr_pose();
		update_all_face_mrkr_pose();
		collsnGeo.load_RAPID_transform( get_pose_w_matx() );
	}
}

void Part_ASM::set_pose( const Pose_ASP& partPose , bool updateAuxGeo ){ 
	set_pose( partPose.position , partPose.orientation , false ); // Set pose , Delay geo update
	if( updateAuxGeo ){ // If there are no other changes to make , Update all of the internal lab-fram geometry
		update_part_mrkr_pose();
		update_all_face_mrkr_pose();
		collsnGeo.load_RAPID_transform( get_pose_w_matx() );
	}
}

void Part_ASM::set_ID( llin id ){ ID = id; }

std::vector< Polygon_ASM >& Part_ASM::get_all_faces(){ return faces; }
const std::vector< Polygon_ASM >& Part_ASM::get_all_faces() const { return faces; }

std::vector< Polygon_ASM >& /* */ Part_ASM::get_all_supports(){ return supports; }
const std::vector< Polygon_ASM >& Part_ASM::get_all_supports() const { return supports; }

RAPID_Geo& Part_ASM::get_collsn_geo_ref(){  return collsnGeo;  } // Get a reference to the collision geometry

RViz_MeshMarker* Part_ASM::get_mrkr_ptr(){ return &partMrkr; }

void Part_ASM::load_face_mrkrs_into_mngr( RViz_MarkerManager& mngr ){
	size_t i       = 0                ,
		   numMrkr = faceMrkrs.size() ; 
	for( i = 0 ; i < numMrkr ; i ++ ){ mngr.add_marker( faceMrkrs[i] ); }
}

void Part_ASM::load_part_mrkrs_into_mngr( RViz_MarkerManager& mngr ){ mngr.add_marker( partMrkr ); } 

void Part_ASM::get_part_mrkr_color( float color[4] ){ // Get the part marker color
	partMrkr.get_color( color ); // Get the color of the marker and load it into 'colorArr' {r,g,b,a}
}

void Part_ASM::set_marker_opacity( float alpha ){  
	markerOpacity = alpha;  
	std::vector<float> curClr = partMrkr.get_uniform_color();
	curClr[3] = alpha;
	partMrkr.set_color( curClr );
}

void Part_ASM::set_part_mrkr_color( float R , float G , float B , float alpha ){ 
	// Set the part marker color
	partMrkr.set_color( R , G , B , alpha ); // Set the marker color RGBA
}

void Part_ASM::set_part_mrkr_color( const std::vector<float>& color ){ 
	if( color.size() == 4 ){
		set_part_mrkr_color( color[0] , color[1] , color[2] , color[3] ) ;
	}
}

void Part_ASM::rand_part_mrkr_color(){  
	partMrkr.rand_color();  
	set_marker_opacity( markerOpacity );
}

void Part_ASM::update_all_face_mrkr_pose(){
	size_t i       = 0                ,
		   numMrkr = faceMrkrs.size() ;
	for( i = 0 ; i < numMrkr ; i ++ ){ faceMrkrs[i].set_pose_from_part( *this ); }
}

void Part_ASM::update_part_mrkr_pose(){  partMrkr.set_pose_from_part( *this );  }

llin Part_ASM::get_ID(){ return ID; }

Eigen::Vector3d Part_ASM::get_face_lab_norm_by_ID( llin faceID ){
	size_t i        = 0            ,
		   numFaces = faces.size() ;
	for( i = 0 ; i < numFaces ; i++ ){
		if( faces[i].get_ID().face == faceID ){ return faces[i].get_lab_norm(); } // ID found , return the lab normal
	}
	return Eigen::Vector3d( nan("") , nan("") , nan("") ); // No such part , return NaN
}

Polygon_ASM* Part_ASM::get_face_by_ID( llin faceID ){
	size_t i        = 0            ,
		   numFaces = faces.size() ;
	Polygon_ASM* rtnPtr = nullptr;
	for( i = 0 ; i < numFaces ; i++ ){
		if( faces[i].get_ID().face == faceID ){  rtnPtr = &( faces[i] );  } // ID found , return the lab normal
	}
	return rtnPtr;
}

double Part_ASM::get_volume(){ return volume; }

double Part_ASM::get_radius(){
	size_t numPts = vertcs3D.rows();
	double mostDist = 0.0 ,
		   currDist = 0.0 ;
	Eigen::Vector3d currPnt;
	for( size_t i = 0 ; i < numPts ; i++ ){  
		currPnt  = vertcs3D.row(i);
		currDist = currPnt.norm();
		if( currDist > mostDist ){
			mostDist = currDist;
		}
	}
	return mostDist;
}

string Part_ASM::get_STL_path(){  return meshPath;  }

// TODO: MAKE THIS PART OF 'Part_ASM' init
std::vector< Polygon_ASM > cluster_VFN_to_Polygon_ASM( Eigen::MatrixXd V , Eigen::MatrixXi F , Eigen::MatrixXd N , 
													   double CRITRN_ANG , llin containerID ){
	std::vector< Polygon_ASM > rtnFaces;
	std::vector<int> faceRow;
	int I        = 0 ,
		J        = 0 ,
		numFaces = 0 ,
		leastDex = 0 ;
	Polygon_ASM facePoly;
	
	// URL , Eigen forgets types: https://stackoverflow.com/questions/23946658/error-mixing-types-with-eigen-matrices
	TriMeshVFN_ASP reducedVF = remove_duplicate_vertices_from_VF( V.cast<double>() , F.cast<int>() );
	
	// URL , Eigen forgets types: https://stackoverflow.com/questions/23946658/error-mixing-types-with-eigen-matrices
	FaceIndices rtnStruct = faces_from_facets( reducedVF.V.cast<double>() , reducedVF.F.cast<int>() , N.cast<double>() , CRITRN_ANG );
	
	std::vector<std::vector<int>> faceLists /* <----------------------------------------- */ = rtnStruct.faceLists;
	std::vector< Eigen::Vector3d , Eigen::aligned_allocator< Eigen::Vector3d > > faceNormals = rtnStruct.faceNormals;
	numFaces = faceLists.size();
	
	// For each of the identified face lists , construct a Polygon object and append it to the vector
	for( I = 0 ; I < numFaces ; I++ ){
		faceRow = faceLists[I];
		leastDex = min_num_in_vec( faceRow ); // Get the least facet index from the facet list for this face
		
		// Create a poly from each face
		facePoly = Polygon_ASM( reducedVF.V , // reducdVtx , 
								reducedVF.F , // reducdFct , 
								faceRow , 
								Vec3D_from_aligned_stdvec( faceNormals , I ) ); 
		facePoly.set_ID( containerID , leastDex ); // Set the ID of the polygon to be the least index of its facets
		rtnFaces.push_back( facePoly );
	}
	return rtnFaces;
}

/// ~ Support ~
///~ Eigen::Vector3d /* ---- */ COM; // ------ Center of mass
///~ TriMeshVFN_ASP /* ----- */ hullVFN; // -- Convex hull , part frame
///~ std::vector< Polygon_ASM > supports; // ---- Vector of all of the face objects
///~ Eigen::MatrixXd /* ---- */ projCOM; // -- Center of mass , projected onto each support
///~ std::vector< bool > /*- */ stableFace; // Flag for each support face , true == stable
///~ double /* ------------- */ leastDist; //- Least distance to a support edge
///~ double /* ------------- */ volume; // --- Volume of the part , in the same units as the source STL

void Part_ASM::determine_support_stability( double angleCritRad ){ 
	// Calculate the part positions of all of supports
	
	bool SHOWDEBUG = false;
	
	size_t i        = 0 ,
		   numSpprt = 0 , // Number of supporting faces
		   j        = 0 ,
		   numTri   = 0 ; // Number of triangles in the face
	
	std::list< Segment2D_ASP > segmentsList;
	std::vector< Segment2D_ASP > segmentsVec;
	Eigen::Vector2d point; 
	Eigen::MatrixXd poly;
	
	// 2. Calc the convex hull , store
	//~ hullVFN = V_to_ConvexHull_VFN( get_dense_vertices().cast<double>() );
	hullVFN = V_to_ConvexHull_VFN( get_dense_vertices() );
	//~ sep( "STEP 2 CLEAR" , 1 , '!' );
	
	// 3. Cluster the convex hull into supports , store 
	supports = cluster_VFN_to_Polygon_ASM( 
		hullVFN.V , hullVFN.F , hullVFN.N , 
		//~ hullVFN.V.cast<double>() , hullVFN.F.cast<int>() , hullVFN.N.cast<double>() , 
		angleCritRad , ID 
	);
	std::sort( supports.begin() , supports.end() , area_A_grtrThan_B );
	if( SHOWDEBUG ){  
		cout << "Printing support areas, decreasing" << endl;
		for( size_t i = 0 ; i < supports.size() ; i++ ){
			cout << supports[i].get_area() << " ";
		}
		cout << endl;
	}
	
	//~ sep( "STEP 3 CLEAR" , 1 , '!' );
	
	// 4. For each support face
	numSpprt = supports.size();
	stableFace.clear();
	spptEdgeDist.clear();
	//~ sep( "STEP 4 INIT ..." , 1 , '!' );
	for( i = 0 ; i < numSpprt ; i++ ){
		// 5. Project the center of mass onto the support poly
		supports[i].set_part_COM_proj( 
			pnt_proj_to_plane( COM , supports[i].get_center() , supports[i].get_norm() )
		);
		// Transform the point into the polygon frame
		Eigen::Vector3d xformProjCOM = point_basis_change( 
			//~ supports[i].get_COM_proj_3D().cast<double>() , 
			supports[i].get_COM_proj_3D() , 
			//~ supports[i].get_center().cast<double>()  , 
			supports[i].get_center()  , 
			//~ supports[i].get_X_basis().cast<double>() , supports[i].get_Y_basis().cast<double>() , supports[i].get_norm().cast<double>() 
			supports[i].get_X_basis() , supports[i].get_Y_basis() , supports[i].get_norm() 
		);
		//~ sep( "STEP 4 CLEAR" , 1 , '!' );
		
		// 6. project the point onto the 2D frame of the polygon
		supports[i].set_2D_COM_proj( Eigen::Vector2d( xformProjCOM(0) , xformProjCOM(1) ) );
		//~ sep( "STEP 6 CLEAR" , 1 , '!' );
		
		// 7. Compute the bounding polygon of the support face in 2D
		segmentsList.clear(); // Clear the segments from the previous operation
		if( SHOWDEBUG ){  cout << "DEBUG: " << "Cleared previous segment list!" << endl;  }
		segmentsList = get_outside_CCW_poly_from_mesh_subset( 
			supports[i].get_2D_vertices() , supports[i].get_facets() , 
			supports[i].calc_ordered_facet_neighbors() , 
			vec_range( (int)0 , (int)supports[i].get_facets().rows() - 1 )
		);
		if( SHOWDEBUG ){  cout << "DEBUG: " << "Generated a list of " << segmentsList.size() << " segments" << endl;  }
		segmentsVec.clear();
		segmentsVec = segment_list_to_segment_vector( segmentsList );
		supports[i].set_2D_CCWborderSegments( segmentsVec );
		if( SHOWDEBUG ){  cout << "DEBUG: " << "Set the segment border" << endl;  }
		supports[i].set_2D_CCWpolyV( get_closed_poly_verts_from_CCW_segments( segmentsList ) );
		if( SHOWDEBUG ){  cout << "DEBUG: " << "Set the vertex border" << endl;  }
		//~ sep( "STEP 7 CLEAR" , 1 , '!' );
		
		// 8. Determine if the projected COM lies within the support face boundary
		point = supports[i].get_2D_COM_proj();
		if( SHOWDEBUG ){  cout << "DEBUG, " << "Projected COM";  print_vec2d_inline( point );  cout << endl;  }
		poly  = supports[i].get_2D_CCWpolyV();
		if( SHOWDEBUG ){  cout << poly << endl;  }
		stableFace.push_back( point_in_poly_w( point , poly ) );
		//~ sep( "STEP 8 CLEAR" , 1 , '!' );
		
		// 9. Compute the shortest distance to the nearest polygon boundary
		std::vector<double> tempDistVec;
		size_t len = segmentsVec.size();
		for( size_t i = 0 ; i < len ; i++ ){  tempDistVec.push_back( d_point_to_segment_2D( point , segmentsVec[i] ) );  }
		spptEdgeDist.push_back( tempDistVec );
		if( SHOWDEBUG ){  sep( "STEP 9 CLEAR" , 1 , '!' );  }
	}
	//~ sep( "STEP 4 CLEAR" , 1 , '!' );
}

void Part_ASM::report_support_stability(){ 
	// Print a verbose accounting of the supporting surfaces
	size_t len = supports.size();
	cout << "For the part loaded from " << meshPath << " ..." << endl;
	cout << "Found " << len << " supporting surfaces." << endl;
	for( size_t i = 0 ; i < len ; i++ ){
		cout << "\t"   << "Support " << i << ":" << endl;
		cout << "\t\t" << "Polygon: ______ " << supports[i].CCWpolyV.rows() - 1 << " sides" << endl;
		cout << "\t\t" << "Stable?: ______ " << stableFace[i]             << endl;
		cout << "\t\t" << "Border Dist.: _ " << spptEdgeDist[i]           << endl;
		cout << "\t\t" << "Triangles: ____ " << supports[i].facets.rows() << endl;
		cout << "\t\t" << "Vec to Support: " << vec_from_pnt_to_plane( Eigen::Vector3d( 0.0 ,  0.0 , 0.0 ) , 
																	   supports[ i ].center3D ,
																	   supports[ i ].normal3D ) << endl;
		cout << "\t\t" << "Support Pose: _ " << get_lab_pose_for_support_index( i , Eigen::Vector2d( 0.0 , 0.0 ) ) << endl;
	}
	cout << "End " << meshPath << endl;
}

Pose_ASP Part_ASM::get_lab_pose_for_support_index( size_t supportIndex , Eigen::Vector2d offsetXY ){
    /// @brief Get the lab pose of the part as if it were resting on the support at 'index' , with the origin of the support over 'offsetXY'
    /// @param supportIndex - Index of the supporting face
    /// # @param offsetXY ----- [ X , Y ] offset from [ 0 , 0 ] where the part is placed on the infinite supporing plane
    /// def get_lab_pose_for_support_index( self , supportIndex , offsetXY = [ 0.0 , 0.0 ] ):	
    
    if( supportIndex > supports.size()-1 ){
		// Eigen::Quaterniond( -1.0 , -1.0 , -1.0 , -1.0 );
		Pose_ASP rtnStruct;
		rtnStruct.position    = Eigen::Vector3d(    nan("") , nan("") , nan("") );
		rtnStruct.orientation = Eigen::Quaterniond( nan("") , nan("") , nan("") , nan("") );
		return rtnStruct;
	} else {
		//~ turnQuat = Quaternion.shortest_btn_vecs( self.graspMesh.supports[ supportIndex ]['zBasis'] , [  0.0 ,  0.0 , -1.0 ] )
		Eigen::Quaterniond turnQuat;             
		turnQuat.setFromTwoVectors( supports[ supportIndex ].normal3D    ,
									Eigen::Vector3d( 0.0 ,  0.0 , -1.0 ) );
		//~ turnQuat.setFromTwoVectors( Eigen::Vector3d( 0.0 ,  0.0 , -1.0 ) ,
									//~ supports[ supportIndex ].normal3D    );
									
		Eigen::Vector3d planeOffset = vec_from_pnt_to_plane( Eigen::Vector3d( 0.0 ,  0.0 , 0.0 ) , 
														     //~ self.graspMesh.supports[ supportIndex ]['origin'] , 
														     supports[ supportIndex ].center3D ,
														     //~ self.graspMesh.supports[ supportIndex ]['zBasis'] )  , 
														     supports[ supportIndex ].normal3D );
		double factor = -1.0;
		// if( planeOffset(2) >= 0 ){  factor = 1.0;  }else{  factor = -1.0;  }
									
        //~ displace = np.add( 
        Eigen::Vector3d displace = 
			turnQuat.toRotationMatrix() * ( planeOffset * factor ) + Eigen::Vector3d( offsetXY(0) , offsetXY(1) , 0.0 );
            
		// if( displace(2) < 0 ){ displace(2) = displace(2) * -1.0; } // Flip a negative offset
            
        return pose_from_position_orient( displace , turnQuat );
	}
}

bool Part_ASM::is_support_i_stable( size_t supportIndex ){
	// Return the stability determination for the given support index
	if( supportIndex < supports.size() ){  return stableFace[ supportIndex ];  }else{  return false;  }
}
	
std::vector<bool> Part_ASM::get_stability_determination(){ return stableFace; } // Return the stability determination for all supports

void Part_ASM::move_COM_to_origin(){ 
	// Shift all of the vertices so that the COM is at (0,0,0) , part frame
	// NOTE: Only the vertices need to be shifted, the normals and the facets should remain the same
	// 0. Negate the COM position for a shift vec, and set the COM to (0,0,0)
	
	bool SHOWDEBUG = true;
	
	shiftVec = COM * -1; // Store the amount that we shifted the part 
	
	if( SHOWDEBUG ){  cout << "Shift " << get_STL_path() << " by " << endl << shiftVec << endl;  }
	
	COM << 0.0 , 0.0 , 0.0;
	// 1. Shift the dense vertices, if they are populated
	if( vertcs3D.rows()  > 0 ){  shift_V_inplace( vertcs3D  , shiftVec );  }
	// 2. Shift the sparse vertices, if they are populated
	if( reducdVtx.rows() > 0 ){  shift_V_inplace( reducdVtx , shiftVec );  }
}

void Part_ASM::unshift_COM(){
	// Undo the above operation
	COM = -shiftVec;
	// 1. Shift the dense vertices, if they are populated
	if( vertcs3D.rows()  > 0 ){  shift_V_inplace( vertcs3D  , -shiftVec );  }
	// 2. Shift the sparse vertices, if they are populated
	if( reducdVtx.rows() > 0 ){  shift_V_inplace( reducdVtx , -shiftVec );  }
}


// ~ Collision ~

void Part_ASM::reload_collision_model( double shrinkDist ){ 
	// Load the collision mesh with a slightly shrunken trimesh
	TriMeshVFN_ASP bigMesh = get_dense_VFN();
	TriMeshVFN_ASP smlMesh = shrink_dense_VFN_by_Vtx_normals( bigMesh , shrinkDist );
	collsnGeo.load_RAPID_model( smlMesh.V , smlMesh.F );
}

bool Part_ASM::collides_with( Part_ASM& other ){  return collsnGeo.collides_with( other.collsnGeo );  }

bool Part_ASM::collides_with( Floor_ASM& floor ){  return collsnGeo.collides_with( floor.collsnGeo );  } 


// ~ Bookkeeping ~

void Part_ASM::align_polygon_ID_w_part(){ 
	// Make sure that all the faces recognize this Part_ASM as their container
	size_t len_face = faces.size()    , 
	       len_sppt = supports.size() ; 
	for( size_t i = 0 ; i < len_face ; i++ ){
		faces[i].ID.part = ID;
	}
	for( size_t i = 0 ; i < len_sppt ; i++ ){
		supports[i].ID.part = ID;
	}
}

// __ End Part_ASM _________________________________________________________________________________________________________________________


// == class PartsCollection ================================================================================================================
	
// ~ Con/Destructors ~

PartsCollection::PartsCollection(){ /* NO ACTION */ } // Empty constructor

PartsCollection::PartsCollection( const std::vector<Part_ASM*>& collection ){
	partPtrVec = vec_copy( collection );
}

PartsCollection::~PartsCollection(){  for( size_t i = 0 ; i < partPtrVec.size() ; i++ ){  delif( partPtrVec[i] );  }  }

// ~ Bookkeeping ~
	
void PartsCollection::add_part_ptr( Part_ASM* prtPtr ){  
	partPtrVec.push_back( prtPtr );  
	offsets.push_back( origin_pose() ); // The length of the offset vec must match the length of the parts vec
}

void PartsCollection::add_part_ptr( Part_ASM* prtPtr , Pose_ASP& offset ){
	partPtrVec.push_back( prtPtr );  
	offsets.push_back( offset );
}

bool PartsCollection::index_OK( size_t index ){  return index < partPtrVec.size();  }

Part_ASM*& PartsCollection::operator[]( size_t index ){
	if( index_OK( index ) ){
		return partPtrVec[ index ];
	}else{
		 throw std::out_of_range ( "PartsCollection::operator[] , Given index " + to_string( index ) 
								 + " is out of range 0 to " + to_string( partPtrVec.size()-1 ) );
	}
}


// ~ Getters ~

size_t PartsCollection::size(){  return partPtrVec.size();  } // Return the number of parts stored

Pose_ASP PartsCollection::get_pose( size_t index ){
	if( index_OK( index ) ){
		return partPtrVec[ index ]->get_pose();
	}else{
		 throw std::out_of_range ( "PartsCollection::get_pose , Given index " + to_string( index ) 
								 + " is out of range 0 to " + to_string( partPtrVec.size()-1 ) );
	}
}

// ~ Setters ~

void PartsCollection::set_pose( const std::vector<Pose_ASP>& nuPoses ){
	 // Set the pose of every part
	 size_t len = nuPoses.size();
	 if( len == partPtrVec.size() ){
		 for( size_t i = 0 ; i < len ; i++ ){  partPtrVec[i]->set_pose( nuPoses[i] * offsets[i] );  } 
		 //~ for( size_t i = 0 ; i < len ; i++ ){  partPtrVec[i]->set_pose( offsets[i] * nuPoses[i] );  } 
	 }else{
		 throw std::out_of_range ( "PartsCollection::set_pose , Number of poses " + to_string( len ) 
								 + " did not match the number of parts " + to_string( partPtrVec.size() ) );
	 }
}

void PartsCollection::set_pose( const std::vector<Pose_ASP>& nuPoses , 
								size_t bgn , size_t end ){
	// Set the pose of selected parts
	if(  index_OK( bgn )  &&  index_OK( end )  ){
		if( bgn > end ){  size_t swap = bgn;  bgn = end;  end = swap;  }
		size_t len   = end - bgn + 1 , 
			   count = 0             ;
		if( nuPoses.size() < len ){
			throw std::out_of_range( "PartsCollection::set_pose , There are insufficient poses (" + to_string( nuPoses.size() ) + ") for range " 
								   + to_string( bgn ) + " to " + to_string( end ) );
		}else{
			for( size_t i = bgn ; i <= end ; i++ ){
				partPtrVec[i]->set_pose( nuPoses[ count ] * offsets[i] );
				count++;
			}
		}
	}else{
		throw std::out_of_range( "PartsCollection::set_pose , One of " + to_string( bgn ) + " or " + to_string( end )
							   + " is out of range 0 to " + to_string( partPtrVec.size()-1 ) );
	}
}

void PartsCollection::set_pose( const Pose_ASP& nuPose , size_t index ){
	// Set the pose of the selected part
	if( index_OK( index ) ){
		partPtrVec[ index ]->set_pose( nuPose * offsets[ index ] );
	}else{
		throw std::out_of_range( "PartsCollection::set_pose , Specified index " + to_string( index ) 
							   + " is out of range 0 to " + to_string( partPtrVec.size()-1 ) );
	}
}

void PartsCollection::set_pose( const Pose_ASP& nuPose ){
	// Set the pose of all the parts
	std::vector<size_t> indices = vec_range( (size_t) 0 , last_index( partPtrVec ) );
	size_t len = partPtrVec.size();
	for( size_t i = 0 ; i < len ; i++ ){  set_pose( nuPose , i );  }
}

void PartsCollection::set_offset( size_t index , Pose_ASP& offset ){
	if( index_OK( index ) ){
		offsets[ index ] = offset;
	}else{
		 throw std::out_of_range( "PartsCollection::operator[] , Given index " + to_string( index ) 
								+ " is out of range 0 to " + to_string( partPtrVec.size()-1 ) );
	}
}

void PartsCollection::unshift_all(){
	size_t len = partPtrVec.size();
	for( size_t i = 0 ; i < len ; i++ ){  partPtrVec[i]->unshift_COM();  }
}

// ~ Visualization ~

void PartsCollection::load_part_mrkrs_into_mngr( RViz_MarkerManager& mrkrMngr ){
	size_t len = partPtrVec.size();
	for( size_t i = 0 ; i < len ; i++ ){
		//~ partPtrVec[i]->rand_part_mrkr_color();
		partPtrVec[i]->load_part_mrkrs_into_mngr( mrkrMngr ); 
	}
}

void PartsCollection::load_part_mrkrs_into_mngr( RViz_MarkerManager& mrkrMngr , size_t bgn , size_t end ){
	// Load selected part markers into marker manager
	if(  index_OK( bgn )  &&  index_OK( end )  ){
		if( bgn > end ){  size_t swap = bgn;  bgn = end;  end = swap;  }
		for( size_t i = bgn ; i <= end ; i++ ){
			//~ partPtrVec[i]->rand_part_mrkr_color();
			partPtrVec[i]->load_part_mrkrs_into_mngr( mrkrMngr ); 
		}
	}else{
		throw std::out_of_range( "PartsCollection::load_part_mrkrs_into_mngr , One of " + to_string( bgn ) + " or " + to_string( end )
							   + " is out of range 0 to " + to_string( partPtrVec.size()-1 ) );
	}
}
	
// __ End PartsCollection __________________________________________________________________________________________________________________


// == class Floor_ASM ======================================================================================================================

// Con/Destructors
	
Floor_ASM::Floor_ASM( Eigen::Vector3d centerLoc , double xWidth , double yLngth ){
	center = centerLoc;
	Eigen::MatrixXd Vmesh = Eigen::MatrixXd::Zero( 6 , 3 ); // N x 3 matrix in which each row is a unique point in the mesh
	Eigen::MatrixXi Fmesh = Eigen::MatrixXi::Zero( 2 , 3 ); // M x 3 matrix in which each row is a list of indices of 'V' that comprise the facet
	Eigen::MatrixXd Nmesh = Eigen::MatrixXd::Zero( 2 , 3 ); // List of normal vectors corresponding to F
	// Triangle 1 , Lower Right
	Vmesh.row(0) = Eigen::Vector3d( -xWidth/2.0 , -yLngth/2.0 ,  0.0 );
	Vmesh.row(1) = Eigen::Vector3d(  xWidth/2.0 , -yLngth/2.0 ,  0.0 );
	Vmesh.row(2) = Eigen::Vector3d(  xWidth/2.0 ,  yLngth/2.0 ,  0.0 );
	Fmesh.row(0) << 0 , 1 , 2;
	Nmesh.row(0) << 0.0 , 0.0 , 1.0;
	// Triangle 2 , Upper Left
	Vmesh.row(3) = Eigen::Vector3d( -xWidth/2.0 , -yLngth/2.0 ,  0.0 );
	Vmesh.row(4) = Eigen::Vector3d(  xWidth/2.0 ,  yLngth/2.0 ,  0.0 );
	Vmesh.row(5) = Eigen::Vector3d( -xWidth/2.0 ,  yLngth/2.0 ,  0.0 );
	Fmesh.row(1) << 3 , 4 , 5;
	Nmesh.row(1) << 0.0 , 0.0 , 1.0;
	// Set meshes
	floorMesh = TriMeshVFN_ASP{ Vmesh , Fmesh , Nmesh };
	collsnGeo.load_RAPID_model( Vmesh , Fmesh );
	collsnGeo.load_RAPID_transform( pose_from_position_orient( centerLoc , no_turn_quat() ) );
}

// ~ Planning ~
bool Floor_ASM::collides_with( PartsCollection& effector ){
	// Return true if any parts in this assembly collide with any parts in 'cllctn'
	// NOTE: This function assumes that both this Floor_ASM and 'cllctn' have been set to appropriate poses and geometry updates run
	size_t cllctLen = effector.size();
	for ( size_t j = 0 ; j < cllctLen ; j++ ){ // Collection
		if(  (*effector[j]).collides_with( *this )  ){  return true;  }
	}
	return false;
}


// __ End Floor_ASM ________________________________________________________________________________________________________________________


// == class Assembly_ASM ===================================================================================================================

// ~ Constructors and Destructors ~

Assembly_ASM::Assembly_ASM( llin asmID ){ 
	ID = asmID; /* Assign ID */ 
	currentSupportIndex = 0;
	set_pose( origin_pose() );
} // Empty constructor

Assembly_ASM::Assembly_ASM( llin asmID , std::list< PartEntry_ASM >& pPartsList ){ // NOT USED
	// Parts list constructor
	ID = asmID; // Assign ID 
	partsList = pPartsList;
	currentSupportIndex = 0;
	set_pose( origin_pose() );
}

Assembly_ASM::~Assembly_ASM(){ partsList.clear(); } // Default destructor

// ~ Bookkeeping ~

void Assembly_ASM::add_part_w_pose( Part_ASM& part , Pose_ASP pose ){
	// Add a part that has a relative pose in the asm frame
	// NOTE: This function *copies* 'part'
	partsList.push_back( PartEntry_ASM{ Part_ASM( part ) , pose } );
}

bool Assembly_ASM::remove_part_w_ID( llin ID ){
	// Remove the part that has the 'partID' matching 'ID' , True on success and false otherwise
	std::list< PartEntry_ASM >::iterator it; // URL , Iterate through list: https://stackoverflow.com/a/22269465
	bool removed = false;
	for ( it = partsList.begin() ; it != partsList.end() ; ++it ){
		if( it->part.get_ID() == ID ){
			removed = true;
			break;
		}
	}
	//~ if( removed ){ partsList.remove( *it ); }
	if( removed ){ partsList.erase( it ); }
	return removed;
}

std::vector<llin> Assembly_ASM::get_part_ID_vec(){ 
	// Return a vector of all the part IDs in the order that they appear in the list
	std::vector<llin> rtnVec;
	std::list< PartEntry_ASM >::iterator it; // URL , Iterate through list: https://stackoverflow.com/a/22269465
	for ( it = partsList.begin() ; it != partsList.end() ; ++it ){
		rtnVec.push_back( it->part.get_ID() );
	}
	return rtnVec;
}

std::vector<llin> Assembly_ASM::get_balance_IDs_from_sub( const std::vector<llin>& subIDVec ){
	// Get the balance remaining after removing 'subIDVec'
	return vec_copy_without_elem( get_part_ID_vec() , subIDVec );
}

IndexSearchResult Assembly_ASM::index_w_ID( llin ID ){ 
	// Return the index that holds the partID , Otherwise return 9001
	std::list< PartEntry_ASM >::iterator it; // URL , Iterate through list: https://stackoverflow.com/a/22269465
	IndexSearchResult result = default_false_result();
	result.index = 0;
	for ( it = partsList.begin() ; it != partsList.end() ; ++it ){
		if( it->part.get_ID() == ID ){
			result.result = true;
			return result;
		}else{ result.index++; }
	}
	return result;
}

IDSearchResult Assembly_ASM::index_to_partID( size_t ndx ){ 
	// Return the part ID corresponding to the structure index { liaison , NDBG }
	IDSearchResult result = default_ID_search_rs();
	std::vector<llin> prtVec = get_part_ID_vec();
	if( ndx < prtVec.size() ){  
		result.result = true;
		result.ID     = prtVec[ ndx ];  
	}
	return result;
}

std::vector<size_t> Assembly_ASM::partID_vec_to_index_vec( const std::vector<llin>& partVec ){ 
	// Build a vector of corresponding to the indices that hold part IDs
	std::vector<size_t> rtnVec;
	size_t len = partVec.size();
	IndexSearchResult result = default_false_result();
	for( size_t i = 0 ; i < len ; i++ ){  
		result = index_w_ID( partVec[i] );
		if( result.result ){  rtnVec.push_back( result.index );  }
		else{  cout << "WARN , partID_vec_to_index_vec : Part ID " << partVec[i] << " does not exist in current parts list!" << endl;  }
	}
	return rtnVec;
}

std::vector<llin> Assembly_ASM::index_vec_to_partID_vec( const std::vector<size_t>& indxVec ){ 
	// Build a vector of corresponding to the indices that hold part IDs
	std::vector<llin> rtnVec;
	size_t len    = indxVec.size()   , 
		   lstLen = partsList.size() ;
	PartEntry_ASM* entry = nullptr;
	for( size_t i = 0 ; i < len ; i++ ){
		if( indxVec[i] < lstLen ){
			entry = elem_i_from_list( partsList , indxVec[i] );
			rtnVec.push_back( entry->part.get_ID() );
		}
	}
	return rtnVec;
}

std::vector<std::vector<size_t>> Assembly_ASM::partID_vec_vec_to_index_vec_vec( const std::vector<std::vector<llin>>& partVecVec ){ 
	// Build a vector of corresponding to the indices that hold part IDs
	std::vector<std::vector<size_t>> rtnVecVec;
	size_t len = partVecVec.size();
	for( size_t i = 0 ; i < len ; i++ ){  rtnVecVec.push_back( partID_vec_to_index_vec( partVecVec[i] ) );  }
	return rtnVecVec;
}

std::vector<std::vector<llin>> Assembly_ASM::index_vec_vec_to_partID_vec_vec( const std::vector<std::vector<size_t>>& indxVecVec ){ 
	// Build a vector of corresponding to the indices that hold part IDs
	std::vector<std::vector<llin>> rtnVecVec;
	size_t len = indxVecVec.size();
	for( size_t i = 0 ; i < len ; i++ ){  rtnVecVec.push_back( index_vec_to_partID_vec( indxVecVec[i] ) );  }
	return rtnVecVec;
}


// ~ Geometry ~

VolumeCentroid Assembly_ASM::calc_COM( std::vector<llin> partsVec ){ 
	// Calculate the center of mass , Handy to do after part addition / removal
	// NOTE: This function assumes that the origin of each part (part frame) has been shifted to its centroid (part frame)
	std::vector< VolumeCentroid > discretePointVolumes;
	// 1. Fetch the volume centroid from each of the parts , populating the list
	std::list< PartEntry_ASM >::iterator it; // URL , Iterate through list: https://stackoverflow.com/a/22269465
	for ( it = partsList.begin() ; it != partsList.end() ; ++it ){
		if( is_arg_in_vector( it->part.get_ID() , partsVec ) ){
			
			//~ cout << "DEBUG , " << "Volume " << it->part.get_volume() << " at " << it->pose.position << endl;
			
			discretePointVolumes.push_back( VolumeCentroid{ it->part.get_volume() , it->pose.position } );
		}
	}
	
	//~ cout << "DEBUG , " << "Found " << discretePointVolumes.size() << " volumes " << endl;
	
	// 2. Compute the subset centroid
	return get_VolumeCentroid_from_discrete_volume_centroids( discretePointVolumes );
}

void Assembly_ASM::calc_COM(){ 
	// Calculate the center of mass , Handy to do after part addition / removal
	// NOTE: This function assumes that the origin of each part (part frame) has been shifted to its centroid (part frame)
	VolumeCentroid vCentrd = calc_COM( get_part_ID_vec() );
	// 3. Set the COM and the total volume
	COM    = vCentrd.centroid;
	volume = vCentrd.volume;
}

Eigen::MatrixXd Assembly_ASM::calc_AABB(){ // Calc and set the AABB
	AABBasm = AABB( get_dense_vertices() ); 
	return AABBasm;
} 

Eigen::MatrixXd Assembly_ASM::lab_AABB(){
	// Calc and get the AABB
	return AABB( get_dense_lab_vertices() ); 
}

void Assembly_ASM::recalc_geo( double angleCritRad ){ // Recalculate the NDBG and support hull
	bool showDEBUG = false;
	
	if( showDEBUG ){  cout << "DEBUG , " << "Assembly_ASM::recalc_geo - " << "About to calculate supports ..." << endl;  }
	determine_support_stability( angleCritRad );
	if( showDEBUG ){  cout << "Found " << supports.size() << " supports" << endl;  }
	
	if( showDEBUG ){  cout << "DEBUG , " << "Assembly_ASM::recalc_geo - " << "About to calculate NDBG ..." << endl;  }
	calc_NDBG();
	
	if( showDEBUG ){  cout << "DEBUG , " << "Assembly_ASM::recalc_geo - " << "About to enhance NDBG ..." << endl;  }
	enhance_NDBG( DEF_NDBG_JIGGLE );
	
	if( showDEBUG ){  cout << "DEBUG , " << "Assembly_ASM::recalc_geo - " << "About to calculate liaison graph ..." << endl;  }
	populate_liaison_graph_from_NDBG();
	
	if( showDEBUG ){  cout << "DEBUG , " << "Assembly_ASM::recalc_geo - " << "About to calculate shared area graph ..." << endl;  }
	populate_shared_area_graph_from_adjacencies( DEFAULT_TRI_LVL );
}

std::vector<double> Assembly_ASM::get_part_volumes(){ 
	// Return a vector of all the part volumes in the order that they appear in the list
	std::vector<double> rtnVec;
	std::list< PartEntry_ASM >::iterator it; // URL , Iterate through list: https://stackoverflow.com/a/22269465
	for ( it = partsList.begin() ; it != partsList.end() ; ++it ){
		rtnVec.push_back( it->part.get_volume() );
	}
	return rtnVec;
}

IDDbblResult Assembly_ASM::largest_part_ID_w_vol(){ 
	// Return the ID of the most voluminous part along with the volume
	std::vector<llin>   partIDS  = get_part_ID_vec();
	std::vector<double> partVols = get_part_volumes();
	size_t /* ------ */ maxDex   = max_index_in_vec( partVols );
	return IDDbblResult{ partIDS[ maxDex ] , partVols[ maxDex ] };
}

Eigen::MatrixXd Assembly_ASM::get_part_centroids(){ 
	// Return a row-list matrix of all the part locations
	size_t len = partsList.size() , 
		   i   = 0                ;
	Eigen::MatrixXd allCenters = Eigen::MatrixXd::Zero( len , 3 );
	std::list< PartEntry_ASM >::iterator it; // URL , Iterate through list: https://stackoverflow.com/a/22269465
	for ( it = partsList.begin() ; it != partsList.end() ; ++it ){
		allCenters.row(i) = it->pose.position;
		i++;
	}
	return allCenters;
}

void Assembly_ASM::shift_part( llin ID , const Eigen::Vector3d& d ){
	// Shift the specified part within the assembly by 'd'
	PartEntry_ASM& partRef = get_entry_ref_w_ID( ID );
	partRef.pose.position += d;
	recalc_geo();
}


TriMeshVFN_ASP Assembly_ASM::get_lab_VFN( const std::vector<llin>& partsVec ){
	// Get VFN for a sub
	
	bool SHOWDEBUG = false; // if( SHOWDEBUG ){  cout << "" << endl;  }
	
	TriMeshVFN_ASP rtnMesh;
	size_t numParts   = partsVec.size();
	Part_ASM* partPtr = nullptr;
	
	if( SHOWDEBUG ){ sep( "get_lab_VFN" , 3 ) ;
					 cout << "About to process " << numParts << " meshes" << endl;  }
	
	// 1. For every part specified
	for( size_t i = 0 ; i < numParts ; i++ ){
		if( SHOWDEBUG ){  cout << "Loop " << i << " , Part ID " << partsVec[i] << endl;  }
		// 2. Fetch the part pointer
		partPtr = get_part_ptr_w_ID( partsVec[i] );
		if( SHOWDEBUG ){  cout << "Was there a pointer for this part?: " << yesno( partPtr ) << endl;  }
		// 3. If the part exists
		if( partPtr ){
			if( SHOWDEBUG ){  cout << "Merging mesh ..." << endl 
								   << "Fetched mesh has " << partPtr->get_lab_VFN().F.rows() << " triangles" << endl;  }
			// 4. Fetch lab mesh  &&  5. merge mesh with the return struct
			rtnMesh = merge_meshes( rtnMesh , partPtr->get_lab_VFN() );
		}else if( SHOWDEBUG ){  cout << "There was no part with ID " << partsVec[i] << endl;  }
		// else part DNE , skip
	}
	if( SHOWDEBUG ){  cout << "Return a mesh with " << rtnMesh.F.rows() << " facets" << endl
						   << "Vertices:" << endl << rtnMesh.V << endl;  }
	// 6. Return combined mesh
	return rtnMesh;
}

TriMeshVFN_ASP Assembly_ASM::get_lab_VFN(){
	// Get VFN for the entire ASM
	return get_lab_VFN( get_part_ID_vec() );
}

void Assembly_ASM::report_liaison_and_self_collision_state(){
	// Print out a report of what is adjacent and what is colliding
	// NOTE: This function assumes that geometry has been computed for assembly and that the assembly has been posed
	
	sep( "report_liaison_and_self_collision_state" , 10 );
	
	std::vector<llin>   allID    = get_part_ID_vec();
	size_t /* ------ */ numParts = allID.size();
	std::vector<size_t> indices  = vec_range( (size_t)0 , numParts-1 );
	
	cout << "There are " << numParts << " parts" << endl;
	
	if( numParts > 1 ){
	
		
		sep( "Liaison Graph" );
		std::vector<std::vector<bool>> asmLiaison = get_liaison_graph();
		print_vec_vec( asmLiaison );
		cout << endl;
		
		Part_ASM* i_part;
		Part_ASM* j_part;
		bool collision = false ,
			 connection = false ;
		std::vector<size_t> i_balance;
		std::vector<size_t> j_balance;
		std::vector<size_t> i_vec;
		std::vector<size_t> j_vec;
		std::vector<std::pair<size_t,size_t>> collsnPairs;
		
		sep( "Collision / Connection Report" );
		cout << endl;
		for( size_t i = 0 ; i < numParts - 1 ; i++ ){
			i_part = get_part_ptr_w_ID( allID[i] );
			i_vec = {i};
			i_balance = vec_copy_without_elem( indices , i );
			for( size_t j = i + 1 ; j < numParts ; j++ ){
				j_part = get_part_ptr_w_ID( allID[j] );
				j_vec = {j};
				j_balance = vec_copy_without_elem( indices , j );
				
				collision  = i_part->collides_with( *j_part );
				connection = asmLiaison[i][j];
				if( connection ){ // We only really care about connected pieces
					cout << "i: " << i << " , " << i_part->get_STL_path() << endl
						 << "Part i Freedom:" << freedom_string( freedom_class_from_constraints( asm_get_constraints_on_movd( i_vec , i_balance ) ) ) << endl 
						 << "j: " << j << " , " << j_part->get_STL_path() << endl 
						 << "Part j Freedom:" << freedom_string( freedom_class_from_constraints( asm_get_constraints_on_movd( j_vec , j_balance ) ) ) << endl 
						 << "Collision?:_ " << yesno( collision ) << endl
						 << "Connection?: " << yesno( connection ) << endl 
						 << "i --on-> j:_ " << ( connection ? 
												 freedom_string( freedom_class_from_constraints( asm_get_constraints_on_movd( i_vec , j_vec ) ) )  :
												 "N/A" ) << endl
						 << endl;
				 }
				 if( collision ){  collsnPairs.push_back( std::make_pair( i , j ) );  }
			}
		}
		sep( "Collision Summary" );
		cout << collsnPairs << endl;
		sep( "Connection Summary" );
		cout << "Liaison graph connected?: " << yesno( is_subgraph_connected( asmLiaison , indices ) ) << endl;
	}
	cout << endl << "REPORT COMPLETE!" << endl;;
}

// ~ Getters ~

size_t Assembly_ASM::how_many_parts(){ return partsList.size(); } // Return the number of parts that are contained in the assembly

Part_ASM& Assembly_ASM::get_part_ref_w_ID( llin ID ){ 
	// Return a reference to the part that has the 'partID' matching 'ID' 
	std::list< PartEntry_ASM >::iterator it; // URL , Iterate through list: https://stackoverflow.com/a/22269465
	for ( it = partsList.begin() ; it != partsList.end() ; ++it ){
		if( it->part.get_ID() == ID ){
			break;
		}
	}
	return it->part;
}

PartEntry_ASM& Assembly_ASM::get_entry_ref_w_ID( llin ID ){
	// Return a reference to the part entry that has the 'partID' matching 'ID' 
	std::list< PartEntry_ASM >::iterator it; // URL , Iterate through list: https://stackoverflow.com/a/22269465
	for ( it = partsList.begin() ; it != partsList.end() ; ++it ){
		if( it->part.get_ID() == ID ){
			break;
		}
	}
	return *it;
}
	
Part_ASM* Assembly_ASM::get_part_ptr_w_ID( llin ID ){ 
	// Return a pointer to the part that has the 'partID' matching 'ID' , Otherwise return 'nullptr'
	Part_ASM* rtnPtr = nullptr;
	std::list< PartEntry_ASM >::iterator it; // URL , Iterate through list: https://stackoverflow.com/a/22269465
	for ( it = partsList.begin() ; it != partsList.end() ; ++it ){
		if( it->part.get_ID() == ID ){  rtnPtr = &( it->part );  }
	}
	return rtnPtr;
}

Part_ASM& Assembly_ASM::get_part_ref_w_index( size_t index ){ 
	// Return a reference to the part at the specified index
	PartEntry_ASM* entry = elem_i_from_list( partsList , index );
	return entry->part;
}

Pose_ASP /* ---- */ Assembly_ASM::get_pose(){  return pose;  }
Eigen::Quaterniond  Assembly_ASM::get_orientation(){  return orientation;  }

Pose_ASP Assembly_ASM::get_pose_w_ID( llin ID ){ // Return the asm pose of the part that has the 'partID' matching 'ID' 
	IndexSearchResult result = index_w_ID( ID );
	if( result.result ){  return elem_i_from_list( partsList , result.index )->pose;  }
	else{  return err_pose();  }
}

Pose_ASP Assembly_ASM::get_lab_pose_w_ID( llin ID ){ // Return the lab pose of the part that has the 'partID' matching 'ID' 
	IndexSearchResult result = index_w_ID( ID );
	if( result.result ){  return elem_i_from_list( partsList , result.index )->part.get_pose();  }
	else{  return err_pose();  }
}

Eigen::MatrixXd Assembly_ASM::get_dense_vertices( std::vector<llin> partsVec ){ 
	size_t len = 0;
	
	// 1. First pass , Get the size of the matrix to allocate
	std::list< PartEntry_ASM >::iterator it; // URL , Iterate through list: https://stackoverflow.com/a/22269465
	for( it = partsList.begin() ; it != partsList.end() ; ++it ){
		if( is_arg_in_vector( it->part.get_ID() , partsVec ) ){
			len += it->part.get_dense_vertices().rows();
		}
	}
	
	// 2. Store the current pose and send the asm to the origin
	Pose_ASP origPose = pose;
	set_pose( origin_pose() );
	
	// 2. Second pass , Populate the matrix per part , per vertex
	Eigen::MatrixXd rtnVertcs3D = Eigen::MatrixXd::Zero( len , 3 );
	Eigen::MatrixXd temp;
	size_t /* --- */ numRowsPart = 0 ,
	                 index       = 0 ;
	for( it = partsList.begin() ; it != partsList.end() ; ++it ){
		if( is_arg_in_vector( it->part.get_ID() , partsVec ) ){
			temp = transform_V( it->part.get_dense_vertices() , 
								it->part.get_position()       , 
								it->part.get_orientation()    );
			numRowsPart = temp.rows();
			for( size_t j = 0 ; j < numRowsPart ; j++ ){
				rtnVertcs3D.row( index ) = temp.row( j );
				index++;
			}
		}
	}
	
	// 3. Restore the assembly to the original pose
	set_pose( origPose );
	
	// 4. Return the populated matrix of dense verices
	return rtnVertcs3D; 
}

Eigen::MatrixXd Assembly_ASM::get_dense_vertices( std::vector<llin> partsVec , Pose_ASP asmPose ){ 
	// Get the vertices of only the parts with IDs in 'partsVec'
	size_t len = 0;
	
	// 1. First pass , Get the size of the matrix to allocate
	std::list< PartEntry_ASM >::iterator it; // URL , Iterate through list: https://stackoverflow.com/a/22269465
	for( it = partsList.begin() ; it != partsList.end() ; ++it ){
		if( is_arg_in_vector( it->part.get_ID() , partsVec ) ){
			len += it->part.get_dense_vertices().rows();
		}
	}
	
	// 2. Store the current pose and send the asm to the designated pose
	Pose_ASP origPose = pose;
	set_pose( asmPose );
	
	// 2. Second pass , Populate the matrix per part , per vertex
	Eigen::MatrixXd rtnVertcs3D = Eigen::MatrixXd::Zero( len , 3 );
	Eigen::MatrixXd temp;
	size_t /* --- */ numRowsPart = 0 ,
	                 index       = 0 ;
	for( it = partsList.begin() ; it != partsList.end() ; ++it ){
		if( is_arg_in_vector( it->part.get_ID() , partsVec ) ){
			temp = transform_V( it->part.get_dense_vertices() , // Calc the lab position of all the part vertices
								it->part.get_position()       , 
								it->part.get_orientation()    );
			numRowsPart = temp.rows();
			for( size_t j = 0 ; j < numRowsPart ; j++ ){
				rtnVertcs3D.row( index ) = temp.row( j );
				index++;
			}
		}
	}
	
	// 3. Restore the assembly to the original pose
	set_pose( origPose );
	
	// 4. Return the populated matrix of dense verices
	return rtnVertcs3D; 
}

Eigen::MatrixXd Assembly_ASM::get_dense_vertices(){ 
	return get_dense_vertices( get_part_ID_vec() );
}

Eigen::MatrixXd Assembly_ASM::get_dense_vertices( Pose_ASP asmPose ){ // Get all of the vertices of all of the parts
	return get_dense_vertices( get_part_ID_vec() , asmPose );
}

Eigen::MatrixXd Assembly_ASM::get_dense_lab_vertices(){
	// Get all of the vertices of all of the parts , lab frame
	return get_dense_vertices( get_part_ID_vec() , get_pose() );
}

Eigen::MatrixXd Assembly_ASM::get_every_lab_center_except( llin excludeID ){
	// Return lab centers for all parts except that indicated
	// NOTE: If an invalid part ID is passed to the function, then the lab centers of all member parts will be returned
		 
	size_t len     = partsList.size() , 
		   counter = 0                ;
	std::vector<llin> allIDs = get_part_ID_vec();
	Eigen::MatrixXd rtnV;
	
	if( is_arg_in_vector( excludeID , allIDs ) ){
		rtnV = Eigen::MatrixXd::Zero( len - 1 , 3 );
	}else{
		rtnV = Eigen::MatrixXd::Zero( len     , 3 );
	}
	
	std::list< PartEntry_ASM >::iterator it; // URL , Iterate through list: https://stackoverflow.com/a/22269465
	for( it = partsList.begin() ; it != partsList.end() ; ++it ){
		if( it->part.get_ID() != excludeID ){
			rtnV.row( counter ) = it->part.get_position(); // Lab frame
			counter++;
		}
	}
	return rtnV;
}

Eigen::MatrixXd Assembly_ASM::get_part_lab_centers( std::vector<size_t>& selectDices ){
	// Return a row-list matrix of selected part locations
	
	bool SHOWDEBUG = false;
	
	size_t numParts = partsList.size()                                ,
		   len      = count_lessThan_in_vec( numParts , selectDices ) ,
		   pLen     = selectDices.size()                              ,
		   counter  = 0                                               ;
		   
	if( SHOWDEBUG ){  cout << "There are " << len << " applicable indices." << endl;  }
		   
	Eigen::MatrixXd rtnV = Eigen::MatrixXd::Zero( len , 3 );
	for( size_t i = 0 ; i < pLen ; i++ ){
		if( selectDices[i] < numParts ){  
			
			if( SHOWDEBUG ){  cout << "Found part index " << selectDices[i] << " at " 
				                   << get_part_ref_w_index( selectDices[i] ).get_position() << endl;  }
			
			rtnV.row( counter ) = get_part_ref_w_index( selectDices[i] ).get_position();
			counter++;
		}
	}
	return rtnV;
}
 
Eigen::MatrixXd Assembly_ASM::get_part_lab_centers( std::vector<llin>& selectIDs ){
	// Return a row-list matrix of selected part locations
	
	std::vector<llin> allIDs = get_part_ID_vec();
	
	size_t numParts = partsList.size()                          ,
		   len      = count_elems_also_in( selectIDs , allIDs ) ,
		   pLen     = selectIDs.size()                          ,
		   counter  = 0                                         ;
		   
	Eigen::MatrixXd rtnV = Eigen::MatrixXd::Zero( len , 3 );
	
	for( size_t i = 0 ; i < pLen ; i++ ){
		if( is_arg_in_vector( selectIDs[i] , allIDs ) ){  
			rtnV.row( counter ) = get_part_ref_w_ID( selectIDs[i] ).get_position();
			counter++;
		}
	}
	return rtnV;
}

Eigen::MatrixXd Assembly_ASM::get_part_asm_centers( std::vector<size_t>& selectDices ){ 
	// Return row-list of selected part locations , asm frame
	std::list< PartEntry_ASM >::iterator it; // URL , Iterate through list: https://stackoverflow.com/a/22269465

	Eigen::MatrixXd rtnPts;
	size_t i = 0;
	
	for( it = partsList.begin() ; it != partsList.end() ; ++it ){
		if( is_arg_in_vector( i , selectDices ) ){
			rtnPts = copy_V_plus_row( rtnPts , it->pose.position ); 
		}
		i++;
	}
	return rtnPts;
}



Eigen::MatrixXd Assembly_ASM::get_part_asm_centers( std::vector<llin>& selectIDs ){
	// Return row-list of selected part locations , asm frame
	std::vector<size_t> selectDices = partID_vec_to_index_vec( selectIDs );
	return get_part_asm_centers( selectDices );
}

std::vector<llin>& Assembly_ASM::get_NDBG_PartID_Lookup(){ return NDBG_PartID_Lookup; } // Return 'NDBG_PartID_Lookup' reference

size_t Assembly_ASM::get_support(){  return currentSupportIndex;  } // Return the current support for this assembly 

size_t Assembly_ASM::how_many_supports(){  return supports.size();  } // Return the number of supports for this assembly 

std::vector<std::vector<bool>> Assembly_ASM::get_liaison_graph(){  return vec_vec_copy( liaisonGraph );  } // Return copy of liaison graph 

std::vector<std::vector<double>> Assembly_ASM::get_shared_area_graph(){  return vec_vec_copy( sharedAreaGraph );  } // Return copy of shared area graph 

std::vector<size_t> liaison_neighbor_indices( std::vector<std::vector<bool>>& liaisonGraph , size_t query ){
	// Return the indices of all the neighbors of 'query' (row = 'query')
	// NOTE: This function assumes that 'liaisonGraph' is square and symmetic
	std::vector<size_t> adjacent;
	size_t rowLen = liaisonGraph[ 0 ].size();
	if( query < rowLen ){
		// Build an index of liaisons
		for( size_t i = 0 ; i < rowLen ; i++ ){  
			if( ( liaisonGraph[ query ][i] ) && ( query != i ) ){  adjacent.push_back( i );  }  
		}
	}
	return adjacent;
}

void add_undir_edge( std::vector< std::vector< bool > >& liaison , size_t index1 , size_t index2 ){ 
	// DEBUG , Add undirected edge to graph
	size_t len = liaison.size(); // NOTE: This function assumes that the 'liaison' matrix is square
	if( ( index1 < len ) && ( index2 < len ) ){
		liaison[ index1 ][ index2 ] = true;
		liaison[ index2 ][ index1 ] = true;
	}else{
		cout << "WARN , 'add_undir_edge': An invalid index was requested , !({" << index1 << " , " << index2 << "} < " << len <<")" << endl;
	}
}

std::vector<size_t> first_connected_subgraph_in_indices( std::vector<std::vector<bool>>& liaison , std::vector<size_t>& subGdices ){
	// Find the first connected subgraph that can be found among the 'subGdices' and return the corresponding indices
	
	bool SHOWDEBUG = false; // if( SHOWDEBUG ){  cout << "" << endl;  }
	
	size_t /* ------ */ dexLen = subGdices.size(); // ---------- The number of indices to inspect
	std::vector<bool>   marked = bool_false_vector( dexLen ); // Flag for whether each index is part of a connected subgraph
	size_t /* ------ */ firstDex = subGdices[0] , // ----------- First index to inspect, If there is a node not connected to this then not connected
						currNode; // --------------------------- Popped node under inspection
	std::queue<size_t>  frontier; // --------------------------- Nodes to inspect
	std::set<size_t>    visited; // ---------------------------- Nodes already inspected , Prevent cycles
	std::set<size_t>    subGnodes; // -------------------------- Set of all nodes in the first connected subgraph
	std::vector<size_t> neighbors; // -------------------------- Indices of all the liaison neghbors of one node
	std::vector<size_t> rtnVec; // ----------------------------- Return: Vector of all nodes in the first connected subgraph
	
	// 1. Mark the first node YES 
	marked[0] = true; // any node is connected to itself
	visited.insert( subGdices[0] ); // We have certainly visited the first node
	subGnodes.insert( subGdices[0] ); // The first node is certainly part of the first subgraph
	// 2. Fetch all the neighbors of this node
	neighbors = liaison_neighbor_indices( liaison , subGdices[0] );
	// 3. If there are neighbors to process
	if( neighbors.size() > 0 ){
		// 4. Push all of the neighbors that are also in the specified subgraph onto the frontier
		std::vector<size_t> temp = vec_minus_set( vec_intersection( neighbors , subGdices ) , visited );
		set_insert_vec( subGnodes , temp );
		enqueue_vec( frontier , temp );
		// 5. While there are nodes in the frontier
		while( frontier.size() > 0 ){
			// 6. Pop a node  
			currNode = queue_get_pop( frontier );
			// 7. If we have not yet visited this node
			if( !is_arg_in_set( currNode , visited ) ){
				// 7. Now we have visited this node
				visited.insert( currNode );
				// 8. Get the neighbors of the node
				neighbors = liaison_neighbor_indices( liaison , currNode );
				// 9. If there are any neighbors in the subgraph that we have not visited, then they are part of the subgraph
				temp = vec_minus_set( vec_intersection( neighbors , subGdices ) , visited );
				// 10. Add them to the subgraph
				set_insert_vec( subGnodes , temp );
				// 11. Add them to the frontier
				enqueue_vec( frontier , temp );
			}
		}
	} // 12. Else there are no neighbors of this node, We are done, Return the node only
	// 13. Return the contents of the identified set
	rtnVec = set_to_vec( subGnodes );
	return rtnVec;
}

bool is_subgraph_connected( std::vector< std::vector< bool > >& liaison , std::vector< size_t >& subGdices ){
	// Return true if all of the nodes at 'subGdices' are connected (within the subgraph), otherwise return false
	// NOTE: This function assumes that the edges are undirected, that is, the 'liaison' matrix is symmetric
	
	bool SHOWDEBUG = false; // if( SHOWDEBUG ){  cout << "" << endl;  }
	
	// If the subgraph is connected, then the first connected subgraph within the specified subgraph will be the same as the specified subgraph
	std::vector<size_t> firstSubG = first_connected_subgraph_in_indices( liaison , subGdices );
	
	bool areTheyTheSame = vec_same_contents( subGdices , firstSubG );
	
	// 6. If all the subgraph nodes have been marked YES , subgraph is connected ,  Return true  // 7. Otherwise return false
	if( SHOWDEBUG ){  cout << "DEBUG , " << "Connectivity Result: " << areTheyTheSame << endl;  }
	return areTheyTheSame;
}

bool is_subgraph_connected( Assembly_ASM* assembly , std::vector<llin>& prtIDs ){
	std::vector<std::vector<bool>> liaison   = assembly->get_liaison_graph();
	std::vector<size_t> /* ---- */ subGdices = assembly->partID_vec_to_index_vec( prtIDs );
	return is_subgraph_connected( liaison , subGdices );
}

std::vector<std::vector<size_t>> connected_subgraphs_in_subgraph( std::vector<std::vector<bool>>& liaison   , 
																  std::vector<size_t>&            subGdices ){
	// Find all of the connected subgraphs that can be found among the 'subGdices' and return the corresponding indices
	std::vector<size_t> remainder = vec_copy( subGdices ); // Nodes that have not been added to a subgraph
	std::vector<size_t> currSub; // Current sub-sub graph identified
	std::vector<std::vector<size_t>> rtnVecVec;
	// 0. While there are unused nodes
	while( remainder.size() > 0 ){
		// 1. Find a connected subgraph within the nodes
		currSub = first_connected_subgraph_in_indices( liaison , remainder );
		// 2. Add that subgraph to the collection
		rtnVecVec.push_back( currSub );
		// 3. Remove the identified subgraph from the remainder
		remainder = vec_copy_without_elem( remainder , currSub );
	}
	// 4. Return the collection of subgraphs
	return rtnVecVec;
}

std::vector<std::vector<llin>> connected_subgraphs_in_subgraph( Assembly_ASM* assembly , std::vector<llin>& prtIDs ){
	std::vector<std::vector<bool>>   liaison   = assembly->get_liaison_graph();
	std::vector<size_t> /* ------ */ subGdices = assembly->partID_vec_to_index_vec( prtIDs );
	std::vector<std::vector<size_t>> connDices = connected_subgraphs_in_subgraph( liaison , subGdices );
	return assembly->index_vec_vec_to_partID_vec_vec( connDices ); 
}

std::vector<size_t> Assembly_ASM::get_liaison_neighbors( size_t query ){
	// Return all the neighbors of 'query' , according to the 'liaison' graph
	return liaison_neighbor_indices( liaisonGraph , query );
}

std::vector<llin> Assembly_ASM::get_liaison_neighbors( llin query ){
	// Return all the neighbors of 'query' , according to the 'liaison' graph
	std::vector<llin> neighbors;
	// 1. Get the index of the query
	IndexSearchResult result = index_w_ID( query );
	if( result.result ){
		std::vector<size_t> adjacent = liaison_neighbor_indices( liaisonGraph , result.index );
		// 3. Transform the index vector into a part ID vector
		neighbors = index_vec_to_partID_vec( adjacent );
	}
	return neighbors;
}

// ~ Setters ~

void Assembly_ASM::set_pose( Pose_ASP asmPose ){
	// 1. Set the position , orientation , and pose
	pose        = asmPose;
	position    = asmPose.position;
	orientation = asmPose.orientation;
	// 2. Update the lab poses of all the contained parts
	std::list< PartEntry_ASM >::iterator it; // URL , Iterate through list: https://stackoverflow.com/a/22269465
	for ( it = partsList.begin() ; it != partsList.end() ; ++it ){
		it->part.set_pose( pose * it->pose ); 
	}
}

bool Assembly_ASM::set_support( size_t supportIndex ){
	// Assign the current support for this assembly , No transform is performed , Return true if successful
	if( supportIndex > supports.size() - 1 ){  return false;  }
	else{  currentSupportIndex = supportIndex;
		   return true;	 }
}

IndexSearchResult Assembly_ASM::get_support_from_current_pose( double angleCritRad ){
	// Assign the current support for this assembly based on its pose , no failure crit
	IndexSearchResult result;
	result.result = false;
	result.index  = BILLION;
	
	bool SHOWDEBUG = false; // if( SHOWDEBUG ){  cout << "" << endl;  }
	
	if( SHOWDEBUG ){ sep( "Assembly_ASM::get_support_from_current_pose" , 4 );  }
	
	double /* -- */ leastAngle = BILLION_D ,
					angle      = 0.0 ;
	size_t /* -- */ numSpprts  = how_many_supports();
	Eigen::Vector3d down /*-*/ = Eigen::Vector3d::UnitZ() * -1.0;
	Eigen::Vector3d spprtNorm;
	
	if( SHOWDEBUG ){  cout << "There are " << numSpprts << " supports" << endl 
						   << "There are " << how_many_parts() << " parts" << endl
						   << "Criterion: " << angleCritRad << endl;  }
	
	// 1. For each support
	for( size_t i = 0 ; i < numSpprts ; i++ ){
		// 2. Get the angle between the support normal and straight down
		spprtNorm = get_lab_support_normal( i );
		angle     = angle_between( spprtNorm , down );
		
		if( SHOWDEBUG ){  cout << "Support " << i << " angle between " << spprtNorm << " and " << down << " : " << angle << endl;  }
		
		// 3. If the angle is better than the current best
		if( angle < leastAngle ){
			// 4. Store the best index
			leastAngle   = angle;
			result.index = i;
			if( SHOWDEBUG ){  cout << "Log least " << i << " less than " << leastAngle << endl;  }
			// 5. If the angle meets the criterion
			if( angle <= angleCritRad ){
				if( SHOWDEBUG ){  cout << "Matching support " << i << endl;  }
				// 6. Flag success
				result.result = true;
			}
		}
	}
	
	if( SHOWDEBUG ){  cout << "Complete , Match Found?: " << yesno( result.result ) << endl;  }
	
	// N. Return the best candidate
	return result;
}

IndexSearchResult Assembly_ASM::set_support_from_current_pose( double angleCritRad ){
	// Assign the current support for this assembly based on its pose , no failure crit
	// 1. Suggest support index
	IndexSearchResult result = get_support_from_current_pose( angleCritRad );
	// 2. If there was support that matches , then set
	if( result.result ){  set_support( result.index );  }
	// 3. Return the best candidate
	return result;
}


// ~ Markers ~

void Assembly_ASM::set_marker_opacity( float alpha ){  
	markerOpacity = alpha;  
	std::list< PartEntry_ASM >::iterator it; // URL , Iterate through list: https://stackoverflow.com/a/22269465
	for ( it = partsList.begin() ; it != partsList.end() ; ++it ){
		it->part.set_marker_opacity( alpha );
	}
}

void Assembly_ASM::load_face_mrkrs_into_mngr( RViz_MarkerManager& mngr ){
	std::list< PartEntry_ASM >::iterator it; // URL , Iterate through list: https://stackoverflow.com/a/22269465
	for ( it = partsList.begin() ; it != partsList.end() ; ++it ){
		it->part.load_face_mrkrs_into_mngr( mngr ); 
	}
}

void Assembly_ASM::load_part_mrkrs_into_mngr( RViz_MarkerManager& mngr ){
	std::list< PartEntry_ASM >::iterator it; // URL , Iterate through list: https://stackoverflow.com/a/22269465
	for ( it = partsList.begin() ; it != partsList.end() ; ++it ){
		//~ it->part.rand_part_mrkr_color();
		it->part.load_part_mrkrs_into_mngr( mngr ); 
	}
}

// ~ Stability ~

SupportHull Assembly_ASM::determine_subasm_stability( std::vector<llin> subAsm , double angleCritRad ){
	// Calculate the asm positions of all of supports for some subset of current parts , part ID
	SupportHull rtnStruct;
	
	size_t i        = 0 ,
		   numSpprt = 0 , // Number of supporting faces
		   j        = 0 ,
		   numTri   = 0 ; // Number of triangles in the face
	
	std::list< Segment2D_ASP > segmentsList;
	std::vector< Segment2D_ASP > segmentsVec;
	Eigen::Vector2d point; 
	Eigen::MatrixXd poly;
	
	// 2. Calc the convex hull , store
	rtnStruct.hullVFN = V_to_ConvexHull_VFN( get_dense_vertices( subAsm ) );
	
	// 3. Cluster the convex hull into supports , store 
	rtnStruct.supports = cluster_VFN_to_Polygon_ASM( 
		rtnStruct.hullVFN.V , rtnStruct.hullVFN.F , rtnStruct.hullVFN.N , 
		angleCritRad , ID 
	);
	
	// 3.1. Calculate the COM
	VolumeCentroid subCOM = calc_COM( subAsm );
	rtnStruct.COM    = subCOM.centroid;
	rtnStruct.volume = subCOM.volume;
	
	// 3.5. Clear old support data
	numSpprt = rtnStruct.supports.size();
	rtnStruct.stableFace.clear();
	//~ spptEdgeDist.clear();
	// 4. For each support face
	for( i = 0 ; i < numSpprt ; i++ ){
		
		// 5. Project the center of mass onto the support poly
		rtnStruct.supports[i].set_part_COM_proj( 
			pnt_proj_to_plane( rtnStruct.COM , rtnStruct.supports[i].get_center() , rtnStruct.supports[i].get_norm() )
		);
		
		// Transform the point into the polygon frame
		Eigen::Vector3d xformProjCOM = point_basis_change( 
			rtnStruct.supports[i].get_COM_proj_3D() , 
			rtnStruct.supports[i].get_center()  , 
			rtnStruct.supports[i].get_X_basis() , rtnStruct.supports[i].get_Y_basis() , rtnStruct.supports[i].get_norm()
		);
		
		// 6. project the point onto the 2D frame of the polygon
		rtnStruct.supports[i].set_2D_COM_proj( Eigen::Vector2d( xformProjCOM(0) , xformProjCOM(1) ) );
		
		// 7. Compute the bounding polygon of the support face in 2D
		segmentsList.clear(); // Clear the segments from the previous operation
		segmentsList = get_outside_CCW_poly_from_mesh_subset( 
			rtnStruct.supports[i].get_2D_vertices() , rtnStruct.supports[i].get_facets() , 
			rtnStruct.supports[i].calc_ordered_facet_neighbors() , 
			vec_range( (int)0 , (int)rtnStruct.supports[i].get_facets().rows() - 1 )
		);
		
		//~ cout << "Found " << segmentsList.size() << " segments for this support." << endl;
		
		segmentsVec.clear();
		segmentsVec = segment_list_to_segment_vector( segmentsList );
		
		rtnStruct.supports[i].set_2D_CCWborderSegments( segmentsVec );
		rtnStruct.supports[i].set_2D_CCWpolyV( get_closed_poly_verts_from_CCW_segments( segmentsList ) );
		
		// 8. Determine if the projected COM lies within the support face boundary
		point = rtnStruct.supports[i].get_2D_COM_proj();
		poly  = rtnStruct.supports[i].get_2D_CCWpolyV();
		rtnStruct.stableFace.push_back( point_in_poly_w( point , poly ) );
		
		// 9. Compute the shortest distance to the nearest polygon boundary
		std::vector<double> tempDistVec;
		size_t len = segmentsVec.size();
		for( size_t i = 0 ; i < len ; i++ ){  tempDistVec.push_back( d_point_to_segment_2D( point , segmentsVec[i] ) );  }
		rtnStruct.spptEdgeDist.push_back( tempDistVec );
	}
	return rtnStruct;
}

SupportHull Assembly_ASM::determine_subasm_stability( std::vector<llin> subAsm , // Calculate the asm positions of all of supports 
													  Pose_ASP labPose , //           for some subset of current parts , part ID
													  double angleCritRad ){ // at the designated pose
	// Calculate the asm positions of all of supports for some subset of current parts , part ID
	SupportHull rtnStruct;
	
	size_t i        = 0 ,
		   numSpprt = 0 , // Number of supporting faces
		   j        = 0 ,
		   numTri   = 0 ; // Number of triangles in the face
	
	std::list< Segment2D_ASP > segmentsList;
	std::vector< Segment2D_ASP > segmentsVec;
	Eigen::Vector2d point; 
	Eigen::MatrixXd poly;
	
	// 2. Calc the convex hull , store
	rtnStruct.hullVFN = V_to_ConvexHull_VFN( get_dense_vertices( subAsm , labPose ) );
	
	// 3. Cluster the convex hull into supports , store 
	rtnStruct.supports = cluster_VFN_to_Polygon_ASM( 
		rtnStruct.hullVFN.V , rtnStruct.hullVFN.F , rtnStruct.hullVFN.N , 
		angleCritRad , ID 
	);
	
	// 3.1. Calculate the COM
	VolumeCentroid subCOM = calc_COM( subAsm ); 
	rtnStruct.COM    = transform_point( subCOM.centroid , labPose );
	rtnStruct.volume = subCOM.volume;
	
	// 3.5. Clear old support data
	numSpprt = rtnStruct.supports.size();
	rtnStruct.stableFace.clear();
	//~ spptEdgeDist.clear();
	// 4. For each support face
	for( i = 0 ; i < numSpprt ; i++ ){
		
		// 5. Project the center of mass onto the support poly
		rtnStruct.supports[i].set_part_COM_proj( 
			pnt_proj_to_plane( rtnStruct.COM , rtnStruct.supports[i].get_center() , rtnStruct.supports[i].get_norm() )
		);
		
		// Transform the point into the polygon frame
		Eigen::Vector3d xformProjCOM = point_basis_change( 
			rtnStruct.supports[i].get_COM_proj_3D() , 
			rtnStruct.supports[i].get_center()  , 
			rtnStruct.supports[i].get_X_basis() , rtnStruct.supports[i].get_Y_basis() , rtnStruct.supports[i].get_norm()
		);
		
		// 6. project the point onto the 2D frame of the polygon
		rtnStruct.supports[i].set_2D_COM_proj( Eigen::Vector2d( xformProjCOM(0) , xformProjCOM(1) ) );
		
		// 7. Compute the bounding polygon of the support face in 2D
		segmentsList.clear(); // Clear the segments from the previous operation
		segmentsList = get_outside_CCW_poly_from_mesh_subset( 
			rtnStruct.supports[i].get_2D_vertices() , rtnStruct.supports[i].get_facets() , 
			rtnStruct.supports[i].calc_ordered_facet_neighbors() , 
			vec_range( (int)0 , (int)rtnStruct.supports[i].get_facets().rows() - 1 )
		);
		
		//~ cout << "Found " << segmentsList.size() << " segments for this support." << endl;
		
		segmentsVec.clear();
		segmentsVec = segment_list_to_segment_vector( segmentsList );
		
		
		
		rtnStruct.supports[i].set_2D_CCWborderSegments( segmentsVec );
		rtnStruct.supports[i].set_2D_CCWpolyV( get_closed_poly_verts_from_CCW_segments( segmentsList ) );
		
		// 8. Determine if the projected COM lies within the support face boundary
		point = rtnStruct.supports[i].get_2D_COM_proj();
		poly  = rtnStruct.supports[i].get_2D_CCWpolyV();
		rtnStruct.stableFace.push_back( point_in_poly_w( point , poly ) );
		
		// 9. Compute the shortest distance to the nearest polygon boundary
		std::vector<double> tempDistVec;
		size_t len = segmentsVec.size();
		for( size_t i = 0 ; i < len ; i++ ){  tempDistVec.push_back( d_point_to_segment_2D( point , segmentsVec[i] ) );  }
		rtnStruct.spptEdgeDist.push_back( tempDistVec );
	}
	return rtnStruct;
}

void Assembly_ASM::determine_support_stability( double angleCritRad ){ 
	// Calculate the asm positions of all of supports
	
	bool SHOWDEBUG = false; // if( SHOWDEBUG ){  cout << "" << endl;  }
	
	if( SHOWDEBUG ){  sep( "Assembly_ASM::determine_support_stability" , 1 );
					  cout << "There are " << how_many_parts() << " parts" << endl  
					       << "Part IDs: " << get_part_ID_vec() << endl; }
	
	SupportHull allSupports = determine_subasm_stability( get_part_ID_vec() , angleCritRad );
	
	if( SHOWDEBUG ){  cout << "Calculated " << allSupports.supports.size() << " supports" << endl;  }
	
	//~ supports     = copy_Polygon_ASM_vector( allSupports.supports );
	supports     = copy_Poly_vec_decreasing_area( allSupports.supports );
	
	if( SHOWDEBUG ){  cout << "Copied " << supports.size() << " supports" << endl;  }
	
	COM          = allSupports.COM;
	hullVFN      = copy_TriMeshVFN_ASP( allSupports.hullVFN );
	spptEdgeDist = vec_vec_copy( allSupports.spptEdgeDist );
	stableFace   = vec_copy( allSupports.stableFace ); 
	volume       = allSupports.volume;
	
	if( SHOWDEBUG ){  cout << "Function counts " << how_many_supports() << " supports" << endl;  }
}

bool Assembly_ASM::validate_subasm_stability_for_support( std::vector<llin> subAsm , size_t supportIndex , 
														  double CRIT_ANG , double CRIT_DST ){
	// Will sub support like 'supportIndex' ?
	Pose_ASP supportPose = get_lab_pose_for_support_index( supportIndex , Eigen::Vector2d( 0.0 , 0.0 ) );
	SupportHull subSupports = determine_subasm_stability( subAsm , supportPose , CRIT_ANG );
	std::vector<Polygon_ASM> supportsXformd = Polygon_ASM_vector_at_pose( supports , supportPose );
	IndexSearchResult match = match_vec2_poly_w_vec1_index( supportsXformd , subSupports.supports , supportIndex , CRIT_ANG , CRIT_DST );
	if( match.result ){
		if( subSupports.stableFace[ match.index ] ){  return true;  }else{  return false;  }
	}else{  return false;  }
}

bool Assembly_ASM::validate_partID_subset_does_not_float( std::vector<llin> subAsm , 
														  double CRIT_ANG , double CRIT_DST ){
	// Return true if the 'subAsm' and the original assembly share at least one support
	Pose_ASP /* -- */ arbitraryPose = origin_pose();
	// 1. Fetch the original supports at an arbitrary pose
	std::vector<Polygon_ASM> origSuppXformd = Polygon_ASM_vector_at_pose( supports , arbitraryPose );
	// 2. Fetch the remainder supports at an arbitrary pose
	SupportHull subSupports = determine_subasm_stability( subAsm , arbitraryPose , CRIT_ANG ); 
	//~ std::vector<Polygon_ASM> blncSuppXformd = Polygon_ASM_vector_at_pose( balSupports.supports , arbitraryPose );
	// 3. Find out if there are *any* matching supports
	IndexMatchResult result = best_match_vec1_poly_to_vec2_poly( origSuppXformd , subSupports.supports , 
																 CRIT_ANG , CRIT_DST );
	return result.match;
}

bool Assembly_ASM::validate_subasm_remainder_does_not_float( std::vector<llin> subAsm , double CRIT_ANG , double CRIT_DST ){
	// Return true if the 'subAsm' remainder and the original assembly share at least one support
	// 0. Get the remainder indices
	std::vector<llin> balance /*-*/ = get_balance_IDs_from_sub( subAsm );
	return validate_partID_subset_does_not_float( balance , CRIT_ANG , CRIT_DST );
}

bool Assembly_ASM::validate_subasm_remainder_does_not_float( size_t originalSupport , std::vector<llin> subAsm , double CRIT_ANG , double CRIT_DST ){
	// Return true if the 'subAsm' remainder and the original assembly share at least one support
	// Return true if the best matching support between the original and the r
	Pose_ASP /* -- */ arbitraryPose = origin_pose();
	std::vector<llin> balance /*-*/ = get_balance_IDs_from_sub( subAsm );
	// 1. Fetch the original supports at an arbitrary pose
	std::vector<Polygon_ASM> origSuppXformd = Polygon_ASM_vector_at_pose( supports , arbitraryPose );
	// 2. Fetch the remainder supports at an arbitrary pose
	SupportHull refcSupports = determine_subasm_stability( balance , arbitraryPose , CRIT_ANG ); 
	//~ std::vector<Polygon_ASM> blncSuppXformd = Polygon_ASM_vector_at_pose( balSupports.supports , arbitraryPose );
	// 3. Find out if there are *any* matching supports
	std::vector<std::pair<size_t,size_t>> allSpprtMatches = all_matches_vec1_poly_to_vec2_poly( origSuppXformd , refcSupports.supports );
	
	return is_arg_on_side_0_pair( originalSupport , allSpprtMatches );
}

Pose_ASP Assembly_ASM::get_lab_pose_for_support_index( size_t supportIndex , Eigen::Vector2d offsetXY ){
	/// @brief Get the lab pose of the part as if it were resting on the support at 'index' , with the origin of the support over 'offsetXY'
    /// @param supportIndex - Index of the supporting face
    /// # @param offsetXY ----- [ X , Y ] offset from [ 0 , 0 ] where the part is placed on the infinite supporing plane
    /// def get_lab_pose_for_support_index( self , supportIndex , offsetXY = [ 0.0 , 0.0 ] ):	
    
    bool SHOWDEBUG = false;
    
    if( supportIndex > supports.size()-1 ){
		// Eigen::Quaterniond( -1.0 , -1.0 , -1.0 , -1.0 );
		Pose_ASP rtnStruct;
		rtnStruct.position    = err_vec3(); // NaN Vec
		rtnStruct.orientation = err_quat(); // NaN Quat
		return rtnStruct;
	} else {
		//~ turnQuat = Quaternion.shortest_btn_vecs( self.graspMesh.supports[ supportIndex ]['zBasis'] , [  0.0 ,  0.0 , -1.0 ] )
		Eigen::Quaterniond turnQuat;             
		turnQuat.setFromTwoVectors( supports[ supportIndex ].normal3D    ,
									Eigen::Vector3d( 0.0 ,  0.0 , -1.0 ) );
		
		if( SHOWDEBUG ){  cout << "Rotation will be: " << turnQuat << endl;  }
		
		Eigen::Vector3d pointCenter = get_average_V( get_dense_vertices() ); // Not the centroid, exactly
		
		Eigen::Vector3d rotatedCenter = turnQuat.toRotationMatrix() * pointCenter;
									
		Eigen::Vector3d planeOffset = vec_from_pnt_to_plane( Eigen::Vector3d( 0.0 ,  0.0 , 0.0 ) , 
														     
														     supports[ supportIndex ].center3D ,
														     
														     supports[ supportIndex ].normal3D );
														     
		if( SHOWDEBUG ){  cout << "Plane offset is: " << planeOffset << endl;  }
														     
		double factor = -1.0;
		// if( planeOffset(2) >= 0 ){  factor = 1.0;  }else{  factor = -1.0;  }
									
        //~ displace = np.add( 
        Eigen::Vector3d displace = 
			turnQuat.toRotationMatrix() * ( planeOffset * factor )
		  + Eigen::Vector3d( offsetXY(0) , offsetXY(1) , 0.0 )
		  + Eigen::Vector3d( -rotatedCenter(0) , -rotatedCenter(1) , 0.0 );
            
		Pose_ASP rtnStruct = pose_from_position_orient( displace , turnQuat );
            
		// DANGER: DIRTY HACK , NUDGE UP
		Pose_ASP savedPose = get_pose();
		set_pose( rtnStruct );
		Eigen::MatrixXd meshBox = AABB( get_lab_VFN() );
		double nudge = meshBox( 0 , 2 );
		displace(2) += -nudge;
		rtnStruct = pose_from_position_orient( displace , turnQuat );
		set_pose( savedPose );
            
        return rtnStruct;
	}
}

Pose_ASP Assembly_ASM::set_lab_pose_for_support_index( size_t supportIndex , Eigen::Vector2d offsetXY , bool setSupportAlso ){
	Pose_ASP rtnPose = get_lab_pose_for_support_index( supportIndex , offsetXY );
	set_pose( rtnPose );
	if( setSupportAlso ){  set_support( supportIndex );  }
	return rtnPose;
}

Pose_ASP Assembly_ASM::get_lab_pose_for_index_3D( size_t supportIndex , Eigen::Vector3d offsetXYZ ){
	// NOTE: This function assumes that the table is perfectly flat with +Z == UP
	// 1. Strip Z from the offset arg
	Eigen::Vector2d offsetXY{ offsetXYZ(0) , offsetXYZ(1) }; 
	// 2. Fetch the setdown pose
	Pose_ASP rtnPose = get_lab_pose_for_support_index( supportIndex , offsetXY );
	// 3. Put the Z component back in the setdown pose
	rtnPose.position(2) += offsetXYZ(2);
	// 4. Return
	return rtnPose;
}

Pose_ASP Assembly_ASM::set_lab_pose_for_index_3D( size_t supportIndex , Eigen::Vector3d offsetXYZ , bool setSupportAlso ){
	// NOTE: This function assumes that the table is perfectly flat with +Z == UP
	Pose_ASP rtnPose = get_lab_pose_for_index_3D( supportIndex , offsetXYZ );
	set_pose( rtnPose );
	if( setSupportAlso ){  set_support( supportIndex );  }
	return rtnPose;
}

void Assembly_ASM::report_support_stability(){ 
	//  Print a verbose accounting of the supporting surfaces
	size_t len = supports.size();
	cout << "Stability info for Assembly_ASM @ " << &(*this) << endl;
	cout << "Found " << len << " supporting surfaces." << endl;
	for( size_t i = 0 ; i < len ; i++ ){
		cout << "\t"   << "Support " << i << ":" << endl;
		cout << "\t\t" << "Polygon: ______ " << supports[i].CCWpolyV.rows() - 1 << " sides" << endl;
		cout << "\t\t" << "Stable?: ______ " << stableFace[i]             << endl;
		cout << "\t\t" << "Border Dist.: _ " << spptEdgeDist[i]           << endl;
		cout << "\t\t" << "Triangles: ____ " << supports[i].facets.rows() << endl;
		cout << "\t\t" << "Vec to Support: " << vec_from_pnt_to_plane( Eigen::Vector3d( 0.0 ,  0.0 , 0.0 ) , 
																	   supports[ i ].center3D ,
																	   supports[ i ].normal3D ) << endl;
		cout << "\t\t" << "Support Pose: _ " << get_lab_pose_for_support_index( i , Eigen::Vector2d( 0.0 , 0.0 ) ) << endl;
	}
	cout << "End " << &(*this) << endl;
}



bool Assembly_ASM::is_support_i_stable( size_t supportIndex ){
	// Return the stability determination for the given support index
	if( supportIndex < supports.size() ){  return stableFace[ supportIndex ];  }else{  return false;  }
}

std::vector<bool> Assembly_ASM::get_stability_determination(){ return stableFace; } // Return the stability determination for all supports

//~ void Assembly_ASM::move_COM_to_origin(); // Shift all of the vertices so that the COM is at (0,0,0) , asm frame // NOT USED

Eigen::Vector3d Assembly_ASM::get_support_normal( size_t supDex ){
	// Get the normal for specified support in the assembly frame
	if( supDex < supports.size() ){  return supports[ supDex ].get_norm();  }else{  return err_vec3();  }
}

Eigen::Vector3d Assembly_ASM::get_lab_support_normal( size_t supDex ){
	// Get the normal for specified support in the lab frame
	return orientation * get_support_normal( supDex );
}

// ~ NDBG ~

void Assembly_ASM::calc_NDBG( double CRIT_ANG , double CRIT_DST ){ 
	// Create the Non-Directional Blocking graph for each of the the parts presently in the assembly
	std::vector< Part_ASM* > partPtrList;
	size_t len = partsList.size();
	bool showDEBUG = false;
	
	if( showDEBUG ){  cout << "DEBUG , Assembly_ASM::calc_NDBG - " << "About to clear old NDBG ..." << endl;  }
	// 1. Clear any previous NDBG that may exist
	NDBG.clear();
	
	if( showDEBUG ){  cout << "DEBUG , Assembly_ASM::calc_NDBG - " << "About to send to the origin ..." << endl;  }
	// 2. Store the present pose and set the asm to the Origin Pose
	Pose_ASP savedPose = pose;
	set_pose( origin_pose() );
	
	if( showDEBUG ){  cout << "DEBUG , Assembly_ASM::calc_NDBG - " << "About to send to build list of part pointers ..." << endl;  }
	// 3. Build the list of part pointers
	//~ for( size_t i = 0 ; i < len ; i++ ){    }
	std::list< PartEntry_ASM >::iterator it; // URL , Iterate through list: https://stackoverflow.com/a/22269465
	for ( it = partsList.begin() ; it != partsList.end() ; ++it ){
		it->part.calc_all_faces(); // Recalc the lab-frame poses for all faces
		partPtrList.push_back( &(it->part) );
	}
	
	if( showDEBUG ){  cout << "DEBUG , Assembly_ASM::calc_NDBG - " << "About to actually calc NDBG ..." << endl;  }
	
	// 4. Calc NDBG and store
	NDBGrelation relation = NDBG_from_posed_parts( partPtrList , len , CRIT_ANG , CRIT_DST );
	NDBG          = relation.NDBG;
	adjacentFaces = relation.adjacencies;
	
	if( showDEBUG ){  cout << "DEBUG , Assembly_ASM::calc_NDBG - " << "About to reset pose ..." << endl;  }
	
	// 5. Set the pose back to what it was
	set_pose( savedPose );
	
	if( showDEBUG ){  cout << "DEBUG , Assembly_ASM::calc_NDBG - " << "About to save parts list ..." << endl;  }
	
	// 6. Save the a lookup to standardize on Part ID
	NDBG_PartID_Lookup = get_part_ID_vec(); // We just used the parts list to build the NDBG , So just use the list to create lookup
}

void Assembly_ASM::enhance_NDBG( double dDist , double CRIT_ANG , double CRIT_DST ){
	// Use spherical jiggle from literature to improve the NDBG , especially when it comes to cylindrical and loose cylindrical contact
	
	bool SHOWDEBUG = false , // if( SHOWDEBUG ){  cout << "" << endl;  }
		 BREAKPNTS = false ; // if( BREAKPNTS ){  waitkey( CONDITION , "MSG" );  }
		 
	// ~ Phase 0 : Init ~
		 
	PartEntry_ASM*      i_partEntry  = nullptr; // ---------------- Pointer to Part Entry i
	PartEntry_ASM*      j_partEntry  = nullptr; // ---------------- Pointer to Part Entry j
	bool /* -------- */ collsnChk    = false , // ----------------- Result for a single collision check
						dirPassed    = false ; // ----------------- Is the constraint consistent with the free directions?
	size_t /* ------ */ numParts     = how_many_parts() , // ------ Number of parts in the assembly
						numConstr    = 0 , // --------------------- Number of constraints
						numFree      = 0 ; // --------------------- Number of free directions identified
	Eigen::MatrixXd     cardinalDirs = cardinal_directions_R3(); // 6 x axial directions
	std::vector<double> pentAngles   = // ------------------------- 5 x angles spaced on a circle
		linspace( (double) 0.0 , (double) ( 4.0 / 5.0 ) * 2.0 * M_PI , 5 );
	Eigen::MatrixXd     pentDirs     = // ------------------------- 5 x directions spaced on a circle
		Eigen::MatrixXd::Zero( 5 , 3 );
	std::vector<size_t> movdDex; // ------------------------------- Part being moved
	std::vector<size_t> refcDex; // ------------------------------- Blocking part
	Eigen::Vector3d     savedPos , // ----------------------------- Position of part i before check
	                    posIncr , // ------------------------------ Differential movement
	                    curDir , // ------------------------------- Current direction to be tested
	                    oppDir , // ------------------------------- Opposite of current direction to be tested
	                    tmpDir , // ------------------------------- Vector for geometric construction / testing
	                    freDir ; // ------------------------------- Current free direction being inspected
	Sphere_d /* ---- */ sampleSphere{ 1.0 , origin_vec() , 2 };
	Eigen::MatrixXd     allSphereDirs;
	
	// 0. Save pose , even though this should be running during the asm creation phase at the origin
	Pose_ASP savedPose = get_pose();
	// 0.5. Send to the origin
	set_pose( origin_pose() );
	
	
	// ~~~ LAMBDA FUNCS ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
	
	// LAMBDA: [&] Capture all named variables by reference
	// Type defaults to void if there is no return statement
	auto check_small_trans = [&]( const Eigen::Vector3d& direction , double dist ){
		// Move the part in 'i_partEntry' in 'direction' by 'dist' , Return collision state with 'j_partEntry'
		bool   rtnCollsn = false;
		size_t numSteps  = 5;
		double div /*-*/ = dist / (double) numSteps;
		// 1. Save the current position
		savedPos = i_partEntry->part.get_position();
		for( size_t p = 0 ; p < numSteps ; p++ ){
			// 2. Calculate delta movement
			posIncr  = direction.normalized() * ( div * (double)p );
			// 3. Set the new position
			i_partEntry->part.set_position( savedPos + posIncr );
			// 4. Check collision
			rtnCollsn = i_partEntry->part.collides_with( j_partEntry->part );
			if( rtnCollsn ){  break;  }
		}
		// 5. Restore part to previous position
		i_partEntry->part.set_position( savedPos );
		// 6. Return
		return rtnCollsn;
	}; // NOTE: Lambda expression must end with a semicolon
	
	// ,,, END LAMBDA ,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,
	
	
	// 1. For each part
	for( size_t i = 0 ; i < numParts ; i ++ ){
		// 2. Get i part reference
		i_partEntry = elem_i_from_list( partsList , i );
		// 3. For each pairing
		for( size_t j = 0 ; j < numParts ; j++ ){
			if( i != j ){
				
				Eigen::MatrixXd     existingConstraints , // ------------------ Constraint normals from the original NDBG calc
									//~ confirmedCnstr , // ----------------------- Confirmed constraint normals from the original NDBG calc
									blkdDirs , // ----------------------------- Movements that resulted in collisions 
									freeDirs , // ----------------------------- Movements that did not result in collisions
									smplDirs ; // ----------------------------- Directions to try , computed from conditions
				
				//  4. Get j part reference
				j_partEntry = elem_i_from_list( partsList , j );
				
				if( SHOWDEBUG ){  cout << "For  " << i_partEntry->part.get_STL_path() << endl
									   << "-vs- " << j_partEntry->part.get_STL_path() << endl
									   << "Index " << i << " -vs- Index " << j << endl;  }
				
				//  5. Fetch the constraints already generated
				movdDex = { i };	refcDex = { j };
				existingConstraints = asm_get_constraints_on_movd( movdDex , refcDex ); // NDBG Order
				numConstr /* --- */ = existingConstraints.rows();
				
				if( SHOWDEBUG ){  cout << "There are " << numConstr << " original constraints" << endl;  }
				
				// ~ Phase 1 : Jiggle ~
				
				//  6. For each existing constraint
				for( size_t k = 0 ; k < numConstr ; k++ ){
					curDir = existingConstraints.row(k);
					curDir = Eig_vec3d_round_zero( curDir );
					//  7. Confirm constraints (move against normals)
					oppDir = curDir * -1.0;
					collsnChk = check_small_trans( oppDir , dDist );
					
					if( collsnChk ){  
						// Don't start off thinking the constraints from the original are good
						//~ confirmedCnstr = copy_V_plus_row( curDir , curDir );  
						blkdDirs /*-*/ = copy_V_plus_row( blkdDirs , oppDir );
					}
					//~ else{  freeDirs = copy_V_plus_row( freeDirs , oppDir );  }
					
					//  8. Try opposites (move with normals)
					collsnChk = check_small_trans( curDir , dDist );
					
					if( collsnChk ){  blkdDirs = copy_V_plus_row( blkdDirs , curDir );  }
					else{  freeDirs = copy_V_plus_row( freeDirs , curDir );  }
					
					//  9. Jiggle in 5 evenly-spaced angles perpendicular to each original constraints (Catch missed prismatic)
					//  A. Get the first perpendicular direction , theta = 0
					//~ tmpDir = get_any_perpendicular( curDir , Eigen::Vector3d( 1 , 2 , 3 ).normalized() );
					tmpDir = get_any_perpendicular( curDir );
					//  B. Rotate from first direction , about the current constraint
					for( size_t m = 0 ; m < pentAngles.size() ; m++ ){
						oppDir = Eigen::AngleAxisd( pentAngles[m] , curDir ) * tmpDir;
						oppDir = Eig_vec3d_round_zero( oppDir );
						collsnChk = check_small_trans( oppDir , dDist );
						if( collsnChk ){  blkdDirs = copy_V_plus_row( blkdDirs , oppDir );  }
						//~ else{  freeDirs = copy_V_plus_row( freeDirs , oppDir );  }
					}
				}
				
				if( SHOWDEBUG ){  cout << "After constraint tests there are " << blkdDirs.rows() << " blocked directions and " 
									   << freeDirs.rows() << " free directions" << endl;  }
				
				// 10. Jiggle in Cardinal Directions
				numConstr = cardinalDirs.rows();
				for( size_t k = 0 ; k < numConstr ; k++ ){
					curDir = cardinalDirs.row(k);
					curDir = Eig_vec3d_round_zero( curDir );
					collsnChk = check_small_trans( curDir , dDist );
					if( collsnChk ){  blkdDirs = copy_V_plus_row( blkdDirs , curDir );  }
					else{  freeDirs = copy_V_plus_row( freeDirs , curDir );  }
				}
				
				if( SHOWDEBUG ){  cout << "After cardinal tests there are " << blkdDirs.rows() << " blocked directions and " 
									   << freeDirs.rows() << " free directions" << endl;  }
				
				if( 0 ){ // If you can live without this, then simplify!
				
					// 11. Jiggle on sphere
					allSphereDirs = sampleSphere.sample_directions_from_direction_constraints( existingConstraints );
					numConstr = allSphereDirs.rows();
					for( size_t k = 0 ; k < numConstr ; k++ ){
						curDir = allSphereDirs.row(k);
						curDir = Eig_vec3d_round_zero( curDir );
						collsnChk = check_small_trans( curDir , dDist );
						if( collsnChk ){  blkdDirs = copy_V_plus_row( blkdDirs , curDir );  }
						//~ else{  freeDirs = copy_V_plus_row( freeDirs , curDir );  }
					}
					
					if( SHOWDEBUG ){  cout << "After sphere tests there are " << blkdDirs.rows() << " blocked directions and " 
										   << freeDirs.rows() << " free directions" << endl;  }
				}
				
				// ~ Phase 2 : Remove Conflicts ~
				Eigen::MatrixXd confirmedCnstr; // This must be fresh every loop
				numConstr = blkdDirs.rows();
				numFree   = freeDirs.rows();
				// 12. For each of the blocked directions
				for( size_t i = 0 ; i < numConstr ; i ++ ){
					dirPassed = true;
					// 13. Fetch the blocked direction
					curDir    = blkdDirs.row(i);
					// 14. Generate the appropriate constraint normal
					oppDir    = curDir * -1.0;
					// 15. Check against free direction  &&  16. If the constraint did not prevent a free direction from passing, then accept it
					if(  test_against_constraint_norms( freeDirs , oppDir )  ){ // Return true if ray does not violate constraints
						confirmedCnstr = copy_V_plus_row( confirmedCnstr , oppDir );  
					}
				}
				
				if( SHOWDEBUG ){  cout << "Conflicts cleaned. There are " << confirmedCnstr.rows() << " remaining constraints" << endl;  }
				
				// ~ Phase 3 : Reduce ~
				// 2018-07-26: At this time, there have not been problems with redundant constraints.  There's no reason to introduce additional complexity until it
				//             becomes a problem that impedes progress
				
				// ~ Phase 4 : Overwrite and Save ~
				//~ NDBG[i][j].overwrite_with_new( confirmedCnstr );
				NDBG[i][j].overwrite_with_new( uniqify_directions( confirmedCnstr ) );
				
			}
		}
	}

	///|/////////////////////
	/// FIXME : WORK AREA ///
	///|/////////////////////
	
	// N. Restore pose to original
	set_pose( savedPose );
}

// Return a reference to the NDBG
std::vector<std::vector<LocalFreedom_ASM>>& Assembly_ASM::get_NDBG(){  return NDBG; }

std::vector<size_t> Assembly_ASM::NDBG_order_vec_from_Part_ID_vec( std::vector<llin> subAsm ){
	// Get the NDBG indices for the associated Part IDs
	// NOTE: If the Part ID DNE in the original NDBG construction , then that part will be skipped and a warning is written to terminal
	size_t len = subAsm.size();
	IndexSearchResult result;
	std::vector<size_t> rtnVec;
	for( size_t i = 0 ; i < len ; i++ ){
		result = search_vec_for_arg( NDBG_PartID_Lookup , subAsm[i] );
		if( result.result ){  rtnVec.push_back( result.index );  }
		else{  cout << "WARN , NDBG_order_vec_from_Part_ID_vec: Could not find ID " << subAsm[i] << " in original NDBG lookup!" << endl;  }
	}
	return rtnVec;
}

interference_result Assembly_ASM::query_asm_NDBG( std::vector<size_t> movedParts , std::vector<size_t> referenceParts , 
												  const Eigen::Vector3d&  moveDir ){
	// Return the interference of 'movedParts' against 'referenceParts' in the 'moveDir'
	// NOTE: In order to use this function, you must know what order the parts appear in 'partsList' , Consider add/remove history
	
	// 1. Unrotate the lab-frame 'moveDir' ( NDBG was calc'd in the Origin Pose )
	Eigen::Quaterniond oppositeRot = orientation.inverse();
	Eigen::Vector3d asmDir = oppositeRot * moveDir;
	cout << "DEBUG , " << "Assembly-Frame Direction: " << asmDir << endl;
	// 2. Run query and return result
	return query_NDBG( NDBG , movedParts , referenceParts , asmDir );
}

interference_result Assembly_ASM::query_asm_NDBG( std::vector<llin> movedParts , // Return the interference of 'movedParts' against 'referenceParts'
												  std::vector<llin> referenceParts ,  // in the 'moveDir'
												  const Eigen::Vector3d&  moveDir ){ // Standardized on Part ID
	return query_asm_NDBG( NDBG_order_vec_from_Part_ID_vec( movedParts ) , 
						   NDBG_order_vec_from_Part_ID_vec( referenceParts ) , 
						   moveDir );
}

void Assembly_ASM::populate_liaison_graph_from_NDBG(){ 
	// Use the NDBG as the basis for the Liaison Graph, which essentially already contains it
	// NOTE: This function assumes that a populated interference between parts constitutes a liaison , The alternative is hand-coded
	//       liaison
	
	bool SHOWDEBUG = false;
	
	liaisonGraph.clear(); // Erase the old liaison graph
	size_t len = partsList.size();
	
	if( SHOWDEBUG ){  cout << "There are " << len << " parts to consider." << endl;  }
	
	for( size_t i = 0 ; i < len ; i++ ){
		
		std::vector< bool > temp;
		
		for( size_t j = 0 ; j < len ; j++ ){
			
			if( SHOWDEBUG ){  cout << "accessing NDBG[ " << i << " ][ " << j << " ] ..." << endl;  }
			
			if( NDBG[i][j].get_number_of_constraints() > 0 ){
				temp.push_back( true  );
			}else{
				temp.push_back( false );
			}
		}
		if( SHOWDEBUG ){  cout << "Adding sublist ..." << endl;  }
		liaisonGraph.push_back( temp );
	}
}

void Assembly_ASM::populate_shared_area_graph_from_adjacencies( size_t lvl ){
	// Calculate the adjacent areas between all of the parts
	// NOTE: This function assumes that the NDBG has already been calculated
	size_t numParts = how_many_parts()     , 
		   len      = adjacentFaces.size() ,
		   indexA   = 0                    ,
		   indexB   = 0                    ;
	Polygon_ASM* polyA = nullptr;
	Polygon_ASM* polyB = nullptr;
	double shared = 0.0;
	//~ Part_ASM*    partA = nullptr;
	//~ Part_ASM*    partB = nullptr;
	llin sideAPart , sideAFace , sideBPart , sideBFace;
	IndexSearchResult resultA = default_false_result();
	IndexSearchResult resultB = default_false_result();
	
	// 0. Set up a square vector with all-zero entries
	sharedAreaGraph = vec_vec_dbbl_zeros( numParts );
	// 1. For each adjacency
	for( size_t i = 0 ; i < len ; i++ ){
		shared = 0.0;
		sideAPart = adjacentFaces[i].a.part; // Load each side of the relationship
		sideAFace = adjacentFaces[i].a.face; 
		sideBPart = adjacentFaces[i].b.part;
		sideBFace = adjacentFaces[i].b.face; 
		// 2. Get side A polygon reference
		polyA = get_part_ptr_w_ID( sideAPart )->get_face_by_ID( sideAFace );
		// 3. Get side B polygon reference
		polyB = get_part_ptr_w_ID( sideBPart )->get_face_by_ID( sideBFace );
		// 4. Calculate the shared area
		shared = polyA->adjacent_area( *polyB , lvl );
		// 5. Accumulate at the appropriate entries
		resultA = index_w_ID( sideAPart );
		resultB = index_w_ID( sideBPart );
		if( resultA.result && resultB.result ){
			indexA = resultA.index;  indexB = resultB.index;  
			sharedAreaGraph[ indexA ][ indexB ] += shared; // Matrix is symmetric
			sharedAreaGraph[ indexB ][ indexA ] += shared;
		}
	}
}

// LAB FRAME
Eigen::MatrixXd Assembly_ASM::asm_get_constraints_on_movd( const std::vector<size_t>& movedParts , const std::vector<size_t>& referenceParts ){
    // 1. Get constraints in the assembly frame
    Eigen::MatrixXd allDirs = get_constraints_on_movd( NDBG , 
									                   movedParts , 
									                   referenceParts );
    
    // 2. Rotate the constraints into the lab frame
	rotate_directions_inplace( allDirs , orientation );
	// 3. Return
	return allDirs;
}

// LAB FRAME
Eigen::MatrixXd Assembly_ASM::asm_get_constraints_on_movd( const std::vector<llin>& movedParts , const std::vector<llin>& referenceParts ){
	return asm_get_constraints_on_movd( partID_vec_to_index_vec( movedParts ) , 
									    partID_vec_to_index_vec( referenceParts ) );
}

// LAB FRAME
Eigen::MatrixXd Assembly_ASM::asm_spherical_pyramid_corners_of_local_freedom( const std::vector<llin>& movedParts , const std::vector<llin>& referenceParts ){
    // 1. Get the corners
    Eigen::MatrixXd allCrnrs = spherical_pyramid_corners_of_local_freedom( NDBG , 
													                       partID_vec_to_index_vec( movedParts ) , 
													                       partID_vec_to_index_vec( referenceParts ) );
	// 2. Rotate the corners into the lab frame
	rotate_directions_inplace( allCrnrs , orientation );
	// 3. Return
	return allCrnrs;
}

// LAB FRAME
Eigen::Vector3d Assembly_ASM::asm_center_of_freedom_direction( std::vector<size_t> movedParts , std::vector<size_t> referenceParts ){
	
	bool SHOWDEBUG = false;
	
	// 1. Calculate nudge
	Eigen::Vector3d ASMnudge;
	Eigen::Vector3d movCen;
	Eigen::Vector3d refCen;
	
	// If either group of parts is empty, then the idea of separation does not make sense, nudge a default direction
	if(  ( movedParts.size() == 0 )  ||  ( referenceParts.size() == 0 )  ){
		ASMnudge = ASP_VECTORDEFAULT;
	// else both groups populated
	}else{
		// NOTE: The nudge must be calculated in the assembly frame , NOT the lab frame
		
		// A. Get center for moved parts , assembly frame
		//~ movCen = get_average_V( get_part_lab_centers( movedParts ) );
		movCen = get_average_V( get_part_asm_centers( movedParts ) );
		
		if( SHOWDEBUG ){  cout << "Center of Moved Parts: " << movCen << endl;  }
		
		// B. Get cetner for reference parts , assembly frame
		//~ refCen = get_average_V( get_part_lab_centers( referenceParts ) );
		refCen = get_average_V( get_part_asm_centers( referenceParts ) );
		if( SHOWDEBUG ){  cout << "Center of Ref. Parts:  " << refCen << endl;  }
		
		// C. Get the direction from ref center to mov center
		ASMnudge = ( movCen - refCen ).normalized();
	}
	
	// 2. Return freedom direction
	//    NOTE: The job of interpreting the correct freedom center from the constraint state belongs to 'center_of_freedom_direction'
	Eigen::Vector3d rtnDir = center_of_freedom_direction( NDBG , movedParts , referenceParts , ASMnudge ); // Suggest removal direction
	if( is_err( rtnDir ) ){  
		if( SHOWDEBUG ){  cout << "Returning: " << rtnDir << endl;  }
		return rtnDir;  
	}else{  
		
		if( SHOWDEBUG ){  
			cout << "Orientation: __________ " << orientation << endl;  
			cout << "Orientation Normalized: " << orientation.normalized() << endl;
			cout << "Returning Transformed:  " << ( orientation * rtnDir ) << endl;  
			cout << "Transformed Normalized: " << ( orientation.normalized() * rtnDir ) << endl;  
		}
		return orientation * rtnDir;  
	}
	
}

Eigen::Vector3d Assembly_ASM::asm_center_of_freedom_direction( std::vector<llin> movedParts , std::vector<llin> referenceParts ){ // Part ID
	std::vector<size_t> movdDices = NDBG_order_vec_from_Part_ID_vec( movedParts );
	std::vector<size_t> refcDices = NDBG_order_vec_from_Part_ID_vec( referenceParts );
	if(  ( movdDices.size() != movedParts.size() )  ||  ( refcDices.size() != referenceParts.size() )  ) 
		 return err_vec3();
	else
		return asm_center_of_freedom_direction( movdDices , refcDices );
}

Eigen::Vector3d Assembly_ASM::removal_dir_moved_vs_balance( std::vector<size_t> movedParts ){
	// Suggest center of freedom of moved parts -vs- balance , NDBG Order
	std::vector<size_t> balance = vec_copy_without_elem( vec_range( (size_t)0 , how_many_parts()-1 ) , movedParts );
	return asm_center_of_freedom_direction( movedParts , balance );
}

Eigen::Vector3d Assembly_ASM::removal_dir_moved_vs_balance( size_t movedPart ){
	// Suggest center of freedom of moved parts -vs- balance , NDBG Order
	std::vector<size_t> movedParts = { movedPart };
	std::vector<size_t> balance = vec_copy_without_elem( vec_range( (size_t)0 , how_many_parts()-1 ) , movedParts );
	return asm_center_of_freedom_direction( movedParts , balance );
}

Eigen::Vector3d Assembly_ASM::removal_dir_moved_vs_balance( std::vector<llin> movedParts ){
	// Suggest center of freedom of moved parts -vs- balance , Part ID
	std::vector<llin> balance = vec_copy_without_elem( get_part_ID_vec() , movedParts );
	return asm_center_of_freedom_direction( movedParts , balance ); // Part ID
}

Eigen::Vector3d Assembly_ASM::removal_dir_moved_vs_balance( llin movedPart ){
	// Suggest center of freedom of moved parts -vs- balance , Part ID
	std::vector<llin> movedParts = { movedPart };
	std::vector<llin> balance = vec_copy_without_elem( get_part_ID_vec() , movedParts );
	return asm_center_of_freedom_direction( movedParts , balance ); // Part ID
}

size_t Assembly_ASM::how_many_constraints( llin movedID , llin referenceID ){ 
	// Return the number of constraints between 'movedID' and 'referenceID'
	// NOTE: This function assumes that the NDBG has been populated
	std::vector<llin> allParts = get_part_ID_vec();
	IndexSearchResult movRes = search_vec_for_arg( allParts , movedID );
	IndexSearchResult refRes = search_vec_for_arg( allParts , referenceID );
	return NDBG[ movRes.index ][ refRes.index ].get_number_of_constraints();
}

size_t Assembly_ASM::how_many_liaisons( llin partID ){ 
	// Return the number of connections with other parts
	// NOTE: This function assumes that the liaison graph has already been built
	size_t outgoingEdges = 0 , 
		   rowLen        = 0 ,
		   nDex          = 0 ;
	IndexSearchResult partDex = index_w_ID( partID );
	if( partDex.result ){
		nDex   = partDex.index;
		rowLen = liaisonGraph[ nDex ].size();
		for( size_t i = 0 ; i < rowLen ; i++ ){  outgoingEdges += (size_t) liaisonGraph[ nDex ][ i ];  }
	}
	return outgoingEdges;
}

double Assembly_ASM::how_much_shared_area( llin partID ){
	// Return the total shared area with neighbors
	// NOTE: This function assumes that the shared area graph has already been built
	double neighborArea  = 0.0;
	size_t rowLen        = 0 ,
		   nDex          = 0 ;
	IndexSearchResult partDex = index_w_ID( partID );
	if( partDex.result ){
		nDex   = partDex.index;
		rowLen = liaisonGraph[ nDex ].size();
		for( size_t i = 0 ; i < rowLen ; i++ ){  neighborArea += sharedAreaGraph[ nDex ][ i ];  }
	}
	return neighborArea;
}

double Assembly_ASM::removal_interface_area( std::vector<llin> movedParts , std::vector<llin> referenceParts ){ 
	// how much area to slide past?	
	double rtnArea = 0.0;
	size_t intrLen = adjacentFaces.size() , 
		   lvl     = 30                   ; // Default triangle subdivision
	Polygon_ASM* polyA = nullptr;
	Polygon_ASM* polyB = nullptr;
	llin sideAPart , sideAFace , sideBPart , sideBFace;
	
	// 0. Store the current pose
	Pose_ASP storePose = get_pose();
	// 1. Set the assembly to a known pose
	set_pose( origin_pose() );
	// 2. Get the removal direction for the moved parts
	Eigen::Vector3d remDir = asm_center_of_freedom_direction( movedParts , referenceParts );
	// 3. If a valid direction was returned
	if( !is_err( remDir ) ){
		// 4. For each of the listed interfaces
		for( size_t i = 0 ; i < intrLen ; i++ ){
			sideAPart = adjacentFaces[i].a.part; // Load each side of the relationship
			sideAFace = adjacentFaces[i].a.face; 
			sideBPart = adjacentFaces[i].b.part;
			sideBFace = adjacentFaces[i].b.face; 
			// 5. If the interface spans moved / reference
			if(  ( is_arg_in_vector( sideAPart , movedParts ) && is_arg_in_vector( sideBPart , referenceParts ) ) || 
				 ( is_arg_in_vector( sideBPart , movedParts ) && is_arg_in_vector( sideAPart , referenceParts ) )  ){
				// A. Get side A polygon reference
				polyA = get_part_ptr_w_ID( sideAPart )->get_face_by_ID( sideAFace );
				// B. Get side B polygon reference
				polyB = get_part_ptr_w_ID( sideBPart )->get_face_by_ID( sideBFace );
				// 6. If the removal direction is in plane with the removal direction
				Eigen::Vector3d normA = polyA->get_lab_norm();
				if( perp_w_margin( normA , remDir ) ){
					// 7. Calculate and accumulate the shared area
					rtnArea += polyA->adjacent_area( *polyB , lvl );
				}
			}
		}
	}else if( remDir.norm() > 5 ){  return 0.0;  } // If returned the no-interaction code ( ||vec|| == 10 ) , there is no interface area
	else{  return -1.0;  } // Else there is no local freedom , Interface area N/A
	// 8. Restore pose
	set_pose( storePose );
	// 9. Return the total interface area
	return rtnArea;
}

// LAB FRAME
bool Assembly_ASM::test_translation_sub_removal( std::vector<size_t> movedParts , std::vector<size_t> referenceParts , 
												 const Eigen::Vector3d& direction , double distance , usll samples ){
	// Test removing 'movedParts' along 'direction' in incremental steps , Each step checking for collision with 'referenceParts'
	// NOTE: This function assumes that the assembly as a whole has already updated the poses of all constituent parts
	
	bool SHOWDEBUG = false; // if( SHOWDEBUG ){  
	
	size_t len_move = movedParts.size()     , 
		   len_refc = referenceParts.size() ;
	Eigen::Vector3d  posIncr = direction.normalized() * ( distance / (double)samples );
	Eigen::Vector3d  savedPos;
	PartEntry_ASM*   movePartEntry = nullptr ; 
	PartEntry_ASM*   refcPartEntry = nullptr ;
	bool collisionFAIL = false;
	usll counter = 0;
	
	if( SHOWDEBUG ){  cout << "DEBUG , " << "There are " << partsList.size() << " parts in this assembly" << endl;  }
	
	// 1. For each of the parts to be translated
	for( size_t m = 0 ; m < len_move ; m++ ){
		// 2. Save the current position of the moved part
		
		if( SHOWDEBUG ){  cout << "DEBUG , " << "Attempting to fetch part " << movedParts[m] << " from the list of parts." << endl;  }
		
		movePartEntry = elem_i_from_list( partsList , movedParts[m] );
		if( movePartEntry ){
			savedPos = movePartEntry->part.get_position();
			counter = 0;
			// 3. For each incremental translation
			for( usll i = 1 ; i <= samples ; i++ ){
				movePartEntry->part.set_position( savedPos + posIncr * i );
				counter++;
				// 4. For each of the parts to be checked against
				for( size_t r = 0 ; r < len_refc ; r++ ){
					
					if( SHOWDEBUG ){  cout << "DEBUG , " << "Attempting to fetch part " << referenceParts[r] << " from the list of parts." << endl;  }
					
					refcPartEntry = elem_i_from_list( partsList , referenceParts[r] );
					if( refcPartEntry ){
						// 5. Check collision
						collisionFAIL = movePartEntry->part.collides_with( refcPartEntry->part );
						
						if( collisionFAIL ) 
							if( SHOWDEBUG ){  cout << "DEBUG , " 
												   << "Collision! Mover " << movedParts[m] << " with Reference " << referenceParts[r] 
												   << " after " << counter << " steps!" << endl;
						    }
						
						// 6. If there is a collision shortcut to FAILURE
						if( collisionFAIL ) break;
					}else{  cout << "WARN , Assembly_ASM::test_translation_sub_removal: FAILED TO RETRIEVE REFERENCE PART!" << endl;  }
				}
				if( collisionFAIL ) break;
			}
			// 8. Restore the part to its original position
			movePartEntry->part.set_position( savedPos );
			// 7. If there is a failure , Shortcut from outer loop
			if( collisionFAIL ) break;
		}else{  cout << "WARN , Assembly_ASM::test_translation_sub_removal: FAILED TO RETRIEVE MOVED PART!" << endl;  }
		if( collisionFAIL ) break;
	}
	
	// 9. Return result
	return !collisionFAIL;
}

SuccessPoints Assembly_ASM::record_test_trans_sub_removal( std::vector<size_t> movedParts , std::vector<size_t> referenceParts ,
														   const Eigen::Vector3d& direction , double distance , usll samples ){
	// Test removing 'movedParts' along 'direction' in incremental steps , Each step checking for collision with 'referenceParts'
	// NOTE: This function assumes that the assembly as a whole has already updated the poses of all constituent parts
	
	bool SHOWDEBUG = false; // if( SHOWDEBUG ){  cout << "" << endl;  }
	
	if( SHOWDEBUG ){  sep( "Assembly_ASM::record_test_trans_sub_removal , std::vector<size_t>" );  }
	
	SuccessPoints rtnStruct = empty_SuccessPoints();
	rtnStruct.bestDir = direction; // Log this whether we were successful or not
	
	size_t len_move = movedParts.size()     , 
		   len_refc = referenceParts.size() ;
	Eigen::Vector3d  posIncr = direction.normalized() * ( distance / (double)samples );
	Eigen::Vector3d  savedPos;
	Eigen::Vector3d  curPoint;
	PartEntry_ASM*   movePartEntry = nullptr ; 
	PartEntry_ASM*   refcPartEntry = nullptr ;
	bool collisionFAIL = false;
	usll counter = 0;
	
	if( SHOWDEBUG ){  cout << "DEBUG , " << "There are " << partsList.size() << " parts in this assembly" << endl
						   << "Moved Parts: ___ " << movedParts     << endl
						   << "Reference Parts: " << referenceParts << endl  
						   << "Began with matrix of size: " << rtnStruct.goodSeqPos.size() << endl; }
	
	
	counter = 0;
	// 3. For each incremental translation
	for( usll i = 1 ; i <= samples ; i++ ){
		
		curPoint = posIncr * i;
		
		if( SHOWDEBUG ){  cout << "Iteration: " << i << " , Offset: " << curPoint << endl;  }
	
		// 1. For each of the parts to be translated
		for( size_t m = 0 ; m < len_move ; m++ ){
			// 2. Save the current position of the moved part
			
			if( SHOWDEBUG ){  cout << "DEBUG , " << "Attempting to fetch part " << movedParts[m] << " from the list of parts." << endl;  }
			
			movePartEntry = elem_i_from_list( partsList , movedParts[m] );
			
			if( movePartEntry ){
				
				if( SHOWDEBUG ){  cout << "Retrieved Mover: " << movedParts[m] << endl << "Save position and Move ..." << endl;  }
				
				savedPos = movePartEntry->part.get_position();
				movePartEntry->part.set_position( savedPos + curPoint );
				
				// 4. For each of the parts to be checked against
				for( size_t r = 0 ; r < len_refc ; r++ ){
					
					if( SHOWDEBUG ){  cout << "DEBUG , " << "Attempting to fetch part " << referenceParts[r] << " from the list of parts." << endl;  }
					
					refcPartEntry = elem_i_from_list( partsList , referenceParts[r] );
					if( refcPartEntry ){
						
						if( SHOWDEBUG ){  cout << "Retrieved Reference: " << referenceParts[r] << endl;  }
						
						// 5. Check collision
						collisionFAIL = movePartEntry->part.collides_with( refcPartEntry->part );
						
						if( collisionFAIL ) 
							if( SHOWDEBUG ){  cout << "DEBUG , " 
												   << "Collision! Mover " << movedParts[m] << " with Reference " << referenceParts[r] 
												   << " after " << counter << " steps!" << endl;
						    }
						
						// 6. If there is a collision shortcut to FAILURE
						if( collisionFAIL ) break;
					}else{  cout << "WARN , Assembly_ASM::test_translation_sub_removal: FAILED TO RETRIEVE REFERENCE PART!" << endl;  }
				}
				// 8. Restore the part to its original position
				movePartEntry->part.set_position( savedPos );
				if( collisionFAIL ) break;
			}else{  cout << "WARN , Assembly_ASM::test_translation_sub_removal: FAILED TO RETRIEVE MOVED PART!" << endl;  }
			// 7. If there is a failure , Shortcut from outer loop
			if( collisionFAIL ) break;
		}
		
		counter++;
		
		if( collisionFAIL ){ 
			break;
		}else{
			if( SHOWDEBUG ){  cout << "Storing offset " << curPoint << " ..." << endl;  }
			rtnStruct.goodSeqPos = copy_V_plus_row( rtnStruct.goodSeqPos , curPoint );
			if( SHOWDEBUG ){  cout << "Distance traveled " << curPoint.norm() << " ..." << endl;  }
			rtnStruct.travelDist = curPoint.norm();
		}
	}
	
	if( !collisionFAIL )
		rtnStruct.success = true;
	else
		rtnStruct.success = false;
	
	// 9. Return result
	return rtnStruct;
}

bool Assembly_ASM::test_translation_sub_removal( std::vector<llin> movedParts , std::vector<llin> referenceParts , // Test removing 'movedParts' 
												 const Eigen::Vector3d& direction , double distance , usll samples ){   // along 'direction' in incremental 
	// Part ID                                                              steps , Each step checking for collision with 'referenceParts'
	return test_translation_sub_removal( partID_vec_to_index_vec( movedParts ) , partID_vec_to_index_vec( referenceParts ) , 
										 direction , distance , samples );
}

SuccessPoints Assembly_ASM::record_test_trans_sub_removal( std::vector<llin> movedParts , std::vector<llin> referenceParts , // Test removing 'movedParts' 
														   const Eigen::Vector3d& direction , double distance , usll samples ){   // along 'direction' in incremental 
	// Part ID                                                              steps , Each step checking for collision with 'referenceParts'
	return record_test_trans_sub_removal( partID_vec_to_index_vec( movedParts ) , partID_vec_to_index_vec( referenceParts ) , 
										  direction , distance , samples );
}

bool Assembly_ASM::test_sub_removal_against_floor( std::vector<size_t> movedParts , Floor_ASM& floor , 
												   Eigen::Vector3d direction , double distance , usll samples ){ 
	// Test removing 'movedParts' along 'direction' in incremental steps , Each step checking for collision with 'floor'
	// NOTE: All part numbers NDBG order
	// NOTE: This function assumes that the assembly as a whole has already updated the poses of all constituent parts

	bool SHOWDEBUG = false;

	size_t len_move = movedParts.size();
	Eigen::Vector3d  posIncr = direction.normalized() * ( distance / (double)samples );
	Eigen::Vector3d  savedPos;
	PartEntry_ASM*   movePartEntry = nullptr ; 
	bool collisionFAIL = false;
	usll counter = 0;
	
	// 1. For each of the parts to be translated
	for( size_t m = 0 ; m < len_move ; m++ ){
		// 2. Save the current position of the moved part
		
		if( SHOWDEBUG ){  cout << "DEBUG , " << "Attempting to fetch part " << movedParts[m] << " from the list of parts." << endl;  }
		
		movePartEntry = elem_i_from_list( partsList , movedParts[m] );
		if( movePartEntry ){
			savedPos = movePartEntry->part.get_position();
			counter = 0;
			// 3. For each incremental translation
			for( usll i = 0 ; i < samples ; i++ ){
				movePartEntry->part.set_position( savedPos + posIncr * i );
				counter++;
				collisionFAIL = movePartEntry->part.collides_with( floor );
						
				if( collisionFAIL ) 
					if( SHOWDEBUG ){  cout << "DEBUG , " 
										   << "Collision! Mover " << movedParts[m] << " with floor after " << counter << " steps!" << endl; }
				
				// 6. If there is a collision shortcut to FAILURE
				if( collisionFAIL ) break;
			}
			// 8. Restore the part to its original position
			movePartEntry->part.set_position( savedPos );
			// 7. If there is a failure , Shortcut from outer loop
			if( collisionFAIL ) break;
		}else{  cout << "WARN , Assembly_ASM::test_translation_sub_removal: FAILED TO RETRIEVE MOVED PART!" << endl;  }
		if( collisionFAIL ) break;
	}
	
	// 9. Return result
	return !collisionFAIL;
	
}

bool Assembly_ASM::test_sub_removal_against_floor( std::vector<llin> movedParts , Floor_ASM& floor , // Test removing 'movedParts' along 'direction' in incremental 
												   Eigen::Vector3d direction , double distance , usll samples ){ // steps , Each step checking for 
	// Part ID                                                                                            collision with 'floor'
	return test_sub_removal_against_floor( partID_vec_to_index_vec( movedParts ) , floor , direction , distance , samples );
}

SuccessCode Assembly_ASM::validate_removal_staightline_stability( std::vector<llin> movedParts , std::vector<llin> referenceParts , 
																  Eigen::Vector3d direction , size_t supportDex , double distance , 
																  usll samples ){
																	  
	bool SHOWDEBUG = false;
	
	if( SHOWDEBUG ){  cout << "This is the ID version of 'validate_removal_staightline_stability'" << endl;  }
																	  
	// Validate for removal and stability 
	bool valid = true;
	SuccessCode result;
	double scale = 1.5;
	//  0. Save the pose that the asm is in
	Pose_ASP savedPose = get_pose();
	
	// DANGER: TRANSFORMATION
	Eigen::Vector3d relDir = orientation.inverse() * direction; // Store the relative direction so that we can rotate it for any support
	
	//  1. Save the support that the asm is on
	size_t savedSupport = currentSupportIndex;
	//  2. Set the asm support to that requested
	set_support( supportDex ); // 2018-02-23: Currently only the support index changes
	//  3. Send the asm to a support above ( 0 , 0 )
	set_lab_pose_for_support_index( supportDex , Eigen::Vector2d( 0.0 , 0.0 ) ); // Orientation changed
	//  4. Validate removal for direction and number of steps
	valid = test_translation_sub_removal( movedParts , referenceParts , direction , distance , samples );
	if( valid ){
		//  5. Instantiate a temp floor below the asm
		Eigen::MatrixXd span = span_from_AABB( calc_AABB() );
		Floor_ASM tempFloor{ Eigen::Vector3d( 0,0,0 ) , span(0,0) * scale , span(0,1) * scale };
		
		//~ cout << "DEBUG , " << "Created a floor at " << Eigen::Vector3d( 0 , 0 , 0 ) << " with X span " << span( 0 , 0 ) * scale
			 //~ << " and Y span " << span( 0 , 1 ) * scale << endl;
		
		//  6. Validate removal against the floor
		
		// DANGER: TRANSFORMATION
		//~ valid = test_sub_removal_against_floor( movedParts , tempFloor , direction , distance , samples );
		valid = test_sub_removal_against_floor( movedParts , tempFloor , ( orientation * relDir ) , distance , samples );
		
		if( valid ){
			//  7. Validate that the reference parts are stable as a collection after removal
			valid = validate_subasm_stability_for_support( referenceParts , supportDex );
			if( valid ){
				result.success = true;
				result.code    = 3;
				result.desc    = "SUCCESS";
			}else{
				result.success = false;
				result.code    = 2;
				result.desc    = "FAILED: Remaining sub ( reference ) will not hold current support pose";
			}
		}else{
			result.success = false;
			result.code    = 1;
			result.desc    = "FAILED: Translation removal -vs- floor";
		}
	}else{
		result.success = false;
		result.code    = 0;
		result.desc    = "FAILED: Translation removal ( without floor )";
	}
	//  8. Restore support
	set_support( savedSupport );
	//  9. Restore pose
	set_pose( savedPose );
	// 10. Return
	return result;
}

IndexSuccesLookup Assembly_ASM::suggest_support_for_staightline_stability( std::vector<llin> movedParts , std::vector<llin> referenceParts , 
																		   double distance , usll samples ){
	// Return the support most likely to support the specified straightline removal operation
	// NOTE: This function assumes that the assembly orientation has been set
	IndexSuccesLookup rtnStruct;  rtnStruct.result = false;
	size_t len = stableFace.size();
	Eigen::Vector3d remDir;
	double bestScore = -4e6;
	
	Pose_ASP origPose = get_pose();
	
	// 1. For each support
	for( size_t i = 0 ; i < len ; i++ ){
		set_lab_pose_for_support_index( i , Eigen::Vector2d(0,0) );
		// 2. Suggest a removal direction
		remDir = asm_center_of_freedom_direction( movedParts , referenceParts );
		// 3. Validate removal against floor for this support and direction  &&  Store the result
		SuccessCode result = Assembly_ASM::validate_removal_staightline_stability( movedParts , referenceParts , 
																				   remDir , i , distance , 
																				   samples );
		rtnStruct.succesVec.push_back( result );
		// 4. Score is the removal direction dotted with the opposite of the support normal (lab frame)
		rtnStruct.scoreVec.push_back( remDir.dot( get_lab_support_normal( i ) * -1.0 ) );
		// 5. Store the index
		rtnStruct.indices.push_back( i ); // Here for completeness
		// 6. If we passed stability and removal for the same support, mark
		if(  is_support_i_stable( i )  &&  result.success  ){
			rtnStruct.result = true;
			rtnStruct.flagVec.push_back( true );
		}else{  rtnStruct.flagVec.push_back( false );  }
		// 7. Keep tabs on the max score
		if( rtnStruct.scoreVec[i] > bestScore ){
			bestScore = rtnStruct.scoreVec[i];
			rtnStruct.bestDex = i;
		}
		
	}
	
	// 8. Restore pose
	set_pose( origPose );
	
	// N. Return
	return rtnStruct;
}

// ~ Planning ~

Assembly_ASM* Assembly_ASM::sub_from_spec( const std::vector<llin>& partsVec , size_t supportNum ){
	// Create a new assembly that has a subset of parts of this assembly , and return a pointer to it
	// NOTE: There is no paired 'delete' for this 'new', please be responsible!
	
	bool SHOWDEBUG = false; // if( SHOWDEBUG ){  cout << "" << endl;  }
	
	if( SHOWDEBUG ){  sep( "Assembly_ASM::sub_from_spec" , 2 );  }
	
	size_t len = partsVec.size();
	//~ Part_ASM tempPart;
	// 0. Create an assembly with the same ID as the current assembly // 2018-02-28: No need to track this ID across problem at this time
	Assembly_ASM* rtnAsm   = new Assembly_ASM( ID );
	Part_ASM*     tempPart = nullptr;
	// 1. For each part in the sub
	for( size_t i = 0 ; i < len ; i++ ){
		// 2. Retrieve the part  &&  3. Copy the part 
		tempPart = new Part_ASM( get_part_ref_w_ID( partsVec[i] ) ); // This should copy the color as well
		// 4. Add the part to the assembly with the same pose
		rtnAsm->add_part_w_pose( *tempPart , get_pose_w_ID( partsVec[i] ) );
		if( SHOWDEBUG ){ 
			std::vector<llin> tempID = { partsVec[i] };
			TriMeshVFN_ASP tempMesh = rtnAsm->get_lab_VFN( tempID );
			cout << "Added part has " << tempMesh.V.rows() << " vertices" << endl;
		}
	}
	if( SHOWDEBUG ){  cout << "Number of supports before recalc: " << rtnAsm->how_many_supports() << endl
						   << "Mesh Error before recalc: _______ " << yesno( is_err( get_lab_VFN().V ) ) << endl;  }
	rtnAsm->recalc_geo();
	if( SHOWDEBUG ){  cout << "Number of supports after recalc:_ " << rtnAsm->how_many_supports() << endl
						   << "Mesh Error after recalc: ________ " << yesno( is_err( get_lab_VFN().V ) ) << endl;  }
	rtnAsm->set_pose( get_pose() );
	rtnAsm->set_support( supportNum );
	
	// 5. Return a pointer to the completed assembly
	return rtnAsm;
}

Assembly_ASM* Assembly_ASM::replicate(){ 
	// Return a pointer to an identical assembly , With an identical pose and support
	// NOTE: There is no paired 'delete' for this 'new', please be responsible!
	Assembly_ASM* rtnAsm = sub_from_spec( get_part_ID_vec() , get_support() );
	rtnAsm->set_pose( get_pose() );
	rtnAsm->set_support( get_support() );
	return rtnAsm;
}

Assembly_ASM* Assembly_ASM::sub_from_spec( const std::vector<llin>& partsVec , double CRIT_ANG , double CRIT_DST ){ 
	// Generate a sub from a subset of parts , Attempt support match
	// Create a new assembly that has a subset of parts of this assembly , and return a pointer to it
	// NOTE: There is no paired 'delete' for this 'new', please be responsible!
	// NOTE: This function assumes that the support of this assembly has been set
	
	bool SHOWDEBUG = false; // if( SHOWDEBUG ){  cout << "" << endl;  }
	
	if( SHOWDEBUG ){  sep( "Assembly_ASM::sub_from_spec" , 2 );  }
	
	size_t len = partsVec.size();
	//~ Part_ASM tempPart;
	// 0. Create an assembly with the same ID as the current assembly // 2018-02-28: No need to track this ID across problem at this time
	Assembly_ASM* rtnAsm = new Assembly_ASM( ID );
	// 1. For each part in the sub
	for( size_t i = 0 ; i < len ; i++ ){
		// 2. Retrieve the part  &&  3. Copy the part 
		Part_ASM tempPart = Part_ASM( get_part_ref_w_ID( partsVec[i] ) );
		// 4. Add the part to the assembly with the same pose
		rtnAsm->add_part_w_pose( tempPart , get_pose_w_ID( partsVec[i] ) );
	}
	
	if( SHOWDEBUG ){  cout << "Number of supports before recalc: " << rtnAsm->how_many_supports() << endl
						   << "Mesh Error before recalc: _______ " << yesno( is_err( get_lab_VFN().V ) ) << endl;  }
	rtnAsm->recalc_geo();
	if( SHOWDEBUG ){  cout << "Number of supports after  recalc " << rtnAsm->how_many_supports() << endl
						   << "Mesh Error after recalc: _______ " << yesno( is_err( get_lab_VFN().V ) ) << endl;  }
	
	IndexSuccesLookup result = best_match_vec2_poly_w_vec1_index( supports , rtnAsm->supports , currentSupportIndex );
																  
	rtnAsm->set_support( result.bestDex );
	
	if( SHOWDEBUG ){  cout << "Mesh Error after set support: __ " << yesno( is_err( get_lab_VFN().V ) ) << endl;  }
	
	// 5. Return a pointer to the completed assembly
	return rtnAsm;
}

std::vector<TargetVFN_ASP> Assembly_ASM::lab_collision_targets_from_parts(){ 
	// Return a mesh target for each of the posed parts in the assembly
	// NOTE: This function assumes that the assembly and constituent part poses have been properly set
	// NOTE: This function returns mesh targets in NDBG index order
	std::vector<TargetVFN_ASP> allTargets;
	std::list< PartEntry_ASM >::iterator it; // URL , Iterate through list: https://stackoverflow.com/a/22269465
	for ( it = partsList.begin() ; it != partsList.end() ; ++it ){
		TriMeshVFN_ASP  tempMesh = it->part.get_lab_VFN();
		Eigen::MatrixXd tempAABB = AABB( tempMesh );
		allTargets.push_back(  TargetVFN_ASP{ tempMesh , tempAABB }  );
	}
	return allTargets;
}

bool Assembly_ASM::collides_with( Assembly_ASM& other ){
	// Return true if any of the parts in this assembly collide with any of the parts in 'other'
	// NOTE: This function assumes that both this assembly and 'other' have been set to appropriate poses and geometry updates run
	/* this  */ std::list< PartEntry_ASM >& partsList_i = partsList;		std::list< PartEntry_ASM >::iterator iter_i;
	/* other */ std::list< PartEntry_ASM >& partsList_j = other.partsList;	std::list< PartEntry_ASM >::iterator iter_j;
	for ( iter_i = partsList_i.begin() ; iter_i != partsList_i.end() ; ++iter_i ){ // this asm
		for ( iter_j = partsList_j.begin() ; iter_j != partsList_j.end() ; ++iter_j ){ // other asm
			if(  iter_i->part.collides_with( iter_j->part )  ){  return true;  }
		}	
	}
	return false;
}

bool Assembly_ASM::collides_with( Floor_ASM& floor ){
	// Return true if any of the parts in this assembly collide with 'floor'
	/* this  */ std::list< PartEntry_ASM >& partsList_i = partsList;		std::list< PartEntry_ASM >::iterator iter_i;
	for ( iter_i = partsList_i.begin() ; iter_i != partsList_i.end() ; ++iter_i ){ // this asm
		if(  iter_i->part.collides_with( floor )  ){  return true;  }
	}
	return false;
}

bool Assembly_ASM::collides_with( PartsCollection& cllctn ){
	// Return true if any parts in this assembly collide with any parts in 'cllctn'
	// NOTE: This function assumes that both this assembly and 'cllctn' have been set to appropriate poses and geometry updates run
	/* this  */ std::list< PartEntry_ASM >& partsList_i = partsList;		std::list< PartEntry_ASM >::iterator iter_i;
	size_t cllctLen = cllctn.size();
	for ( iter_i = partsList_i.begin() ; iter_i != partsList_i.end() ; ++iter_i ){ // this asm
		for ( size_t j = 0 ; j < cllctLen ; j++ ){ // Collection
			if(  iter_i->part.collides_with( *cllctn[j] )  ){  return true;  }
		}	
	}
	return false;
}

bool Assembly_ASM::collides_with( PartsCollection& cllctn , 
								  const std::vector<llin>& pPartsVec ){
	// Return true if any of the specified parts collide with any parts in 'cllctn'
	size_t cllctLen = cllctn.size();
	// NOTE: This function assumes that both this assembly and 'cllctn' have been set to appropriate poses and geometry updates run
	/* this  */ std::list< PartEntry_ASM >& partsList_i = partsList;		std::list< PartEntry_ASM >::iterator iter_i;
	for ( iter_i = partsList_i.begin() ; iter_i != partsList_i.end() ; ++iter_i ){ // this asm
		if( is_arg_in_vector( iter_i->part.get_ID() , pPartsVec ) ){
			for ( size_t j = 0 ; j < cllctLen ; j++ ){ // Collection
				if(  iter_i->part.collides_with( *cllctn[j] )  ){  return true;  }
			}	
		}
	}
	return false;
}

void Assembly_ASM::assembly_report(){ // Print some info about the assembly
	sep( "Assembly Report" , 2 , '~' );
	cout << "\t" << "Parts: _______ " << partsList.size() << endl;
	cout << "\t" << "Supprts: _____ " << supports.size() << endl;
	cout << "\t" << "Curr. Support: " << currentSupportIndex << endl;
}

void Assembly_ASM::part_lab_pose_report(){
	// Print the lab pose for every part
	std::list< PartEntry_ASM >& partsList_i = partsList;		std::list< PartEntry_ASM >::iterator iter_i;
	size_t len = partsList.size() , 
		   i   = 0                ;
	cout << "There are " << len << " parts to report on" << endl;
	for ( iter_i = partsList_i.begin() ; iter_i != partsList_i.end() ; ++iter_i ){ // this asm
		i++;
		cout << i << " of " << len << " : " << iter_i->part.get_pose() << endl;
	}
	cout << "Complete!" << endl;
}

BoolScore Assembly_ASM::check_Z_floor_OK( double zLevel , double margin ){
	// Return true if all parts of the assembly are above 'zLevel'
	// NOTE: This function assumes that the assembly has been set to an appropriate pose
	BoolScore rtnOK;  rtnOK.flag = true;  rtnOK.score = 0.0;
	//~ Eigen::MatrixXd allLabPts = get_dense_vertices(); // Fetch all the vertices in the lab frame
	Eigen::MatrixXd allLabPts = lab_AABB(); // Fetch all the vertices in the lab frame
	size_t len = allLabPts.rows();
	double currZ = 0.0;
	for( size_t i = 0 ; i < len ; i++ ){
		currZ = allLabPts( i , 2 );
		if(  ( zLevel > currZ )  &&  ( abs( zLevel - currZ ) > margin )  ){  rtnOK.flag = false;  }
		if( rtnOK.flag ){
			rtnOK.score = min( rtnOK.score , abs( zLevel - currZ ) );
		}else{
			rtnOK.score = max( rtnOK.score , abs( zLevel - currZ ) );
		}
		
	}
	return rtnOK;
}

IndexSearchResult match_sub_support_to_original( Assembly_ASM* original , size_t origSupport , Assembly_ASM* subAsm , double angleCritRad ){ 
	// Given a pointer to the 'original' Assembly_ASM and a subset 'subAsm', determine the support of 'subAsm' that matches
	// NOTE: This function does not perform any correctness or stability checks (floating sub parts , unstable result)
	// NOTE: This function assumes that the parts list of 'subAsm' is a subset of the parts list of 'original'
	// NOTE: This function assumes that stability determination has been run for both 'original' and 'subAsm'
	//  1. Get the first part in common between the two assemblies
	std::vector<llin> partVecOrg = original->get_part_ID_vec();
	std::vector<llin> partVecSub = subAsm->get_part_ID_vec();
	IndexMultiResult  partMatch = first_in_common_btn_vec_index( partVecOrg , partVecSub );
	IndexSearchResult spptMatch;  spptMatch.result = false;
	Pose_ASP orgPoseSaved , 
			 subPoseSaved , 
			 orgPartCrrnt , 
			 subPartCrrnt ;
	size_t lenSubSupports = subAsm->get_stability_determination().size(); // Get the number of supports that the sub has
	llin matchID = -200; // ID of the common part
	double angleDiff = 0.0;
	if( partMatch.result ){
		//  2. Store the part id of this part
		matchID = partVecOrg[ partMatch.indices[0] ];
		//  3. Store pose of the 'original'
		orgPoseSaved = original->get_pose();
		//  4. Set the pose of the  'original'
		original->set_lab_pose_for_support_index( origSupport , Eigen::Vector2d( 0.0 , 0.0 ) );
		orgPartCrrnt = original->get_lab_pose_w_ID( matchID );
		//  5. Store the support of 'subAsm'
		subPoseSaved = subAsm->get_pose();
		//  6. For each support of 'subAsm'
		for( size_t i = 0 ; i < lenSubSupports ; i++ ){
			//  7. Set the support of 'subAsm'
			subAsm->set_lab_pose_for_support_index( i , Eigen::Vector2d( 0.0 , 0.0 ) );
			//  8. Check if the matching part has the same lab orientation
			subPartCrrnt = subAsm->get_lab_pose_w_ID( matchID );
			angleDiff = angle_between( subPartCrrnt , orgPartCrrnt );
			if( abs( angleDiff ) <= angleCritRad ){
				spptMatch.result = true;
				spptMatch.index  = i;
			}
			if( spptMatch.result ){ break; }
		}
		//  9. Reset 'original' orientation
		original->set_pose( orgPoseSaved );
		// 10. Reset 'subAsm' orientation
		//~ subAsm->set_pose( subPoseSaved );
	}
	return spptMatch;
}

// __ End Assembly_ASM _____________________________________________________________________________________________________________________


// == class LocalFreedom_ASM ===============================================================================================================

LocalFreedom_ASM::LocalFreedom_ASM(){  /* Nothing to do here! */  } // Default constrctor
	
LocalFreedom_ASM::~LocalFreedom_ASM(){  constraintNormals.clear();  } // Destructor


// ~ Bookkeeping ~

void LocalFreedom_ASM::clear(){  constraintNormals.clear();  } // Remove all elements

void LocalFreedom_ASM::overwrite_with_new( const Eigen::MatrixXd& nuConstraints ){
	// Remove all old constraints and add new
	clear(); // Remove all old constraints
	size_t len = nuConstraints.rows();
	Eigen::Vector3d curDir;
	// Add new
	for( size_t i = 0 ; i < len ; i++ ){
		curDir = nuConstraints.row(i);
		add_constraint( curDir );
	}
}

bool LocalFreedom_ASM::test_direction( const Eigen::Vector3d& direction ){ 
	// Return true if the movement direction is unobstructed , otherwise return false
	
	bool SHOWDEBUG = false ,
		 DBNEWLINE = true  ;
	
	size_t i       = 0                        ,
		   numDirs = constraintNormals.size() ;
	double projVal = 0.0d;
	// double projection = 0.0d;
	// bool rtnVal = true;
	for( i = 0 ; i < numDirs ; i++ ){ // If the list has no constraints , then the function will return true for any input vector
		// If the projection of the test direction onto the plane normal is negative , it is on teh wrong side of the plane , return fail
		projVal = round_zero( vec3_project_mag( direction , Vec3D_from_aligned_stdvec( constraintNormals , i ) ) );
		
		if( SHOWDEBUG ){
			if( DBNEWLINE ){  cout << endl;  }
			cout << "DEBUG , projection of " << direction; 
			cout << " onto "                 << Vec3D_from_aligned_stdvec( constraintNormals , i ); 
			cout << " was "                  << projVal;
			if( DBNEWLINE ){  cout << endl;  }
		}
		
		if( projVal < 0.0d ){ 
			if( SHOWDEBUG ){  cout << "Direction Test FAIL!" << endl;  }
			return false; 
		}
	}
	if( SHOWDEBUG ){  cout << "Direction Test PASS!" << endl;  }
	return true; // Made it through all of the plane normal tests without failure , return succeed
}

Eigen::Vector3d LocalFreedom_ASM::suggest_safe_direction( const Eigen::Vector3d& direction ){ 
	// Return true if the movement direction is unobstructed , otherwise return false
	
	bool SHOWDEBUG = false;
	
	size_t i       = 0                        ,
		   numDirs = constraintNormals.size() ;
	double projVal = 0.0d;
	Eigen::Vector3d safDir = direction;
	Eigen::Vector3d currNorm;
	
	if( SHOWDEBUG ){  cout << "Input direction: __ " << safDir << endl; }
	
	// Forwards Run
	for( i = 0 ; i < numDirs ; i++ ){ // If the list has no constraints , then the function will return true for any input vector
		// If the projection of the test direction onto the plane normal is negative , it is on teh wrong side of the plane 
		currNorm = Vec3D_from_aligned_stdvec( constraintNormals , i );
		//~ if( round_zero( vec3_project_mag( safDir , currNorm ) ) < 0.0d ){
		if( vec3_project_mag( safDir , currNorm ) < 0.0d ){
			safDir = vec_proj_to_plane( safDir , currNorm ).normalized(); // If the constraint failed, make the direction 
		}
	}
	
	if( true ){ 
		// Backwards Run // Make sure to catch overflow
		for( i = numDirs-1 ; i >= 0 && i < numDirs ; i-- ){ // If the list has no constraints , then the function will return true for any input vector
			// If the projection of the test direction onto the plane normal is negative , it is on teh wrong side of the plane 
			currNorm = Vec3D_from_aligned_stdvec( constraintNormals , i );
			//~ if( round_zero( vec3_project_mag( safDir , currNorm ) ) < 0.0d ){
			if( vec3_project_mag( safDir , currNorm ) < 0.0d ){
				safDir = vec_proj_to_plane( safDir , currNorm ).normalized(); // If the constraint failed, make the direction 
			}
		}
	}
	
	if( SHOWDEBUG ){  cout << "Repaired direction: " << safDir << endl; }
	
	if( test_direction( safDir ) ){  return safDir;  }
	else{  return err_vec3();  }
}

void LocalFreedom_ASM::add_constraint( const Eigen::Vector3d& direction ){
	//~ constraintNormals.push_back( direction );
	constraintNormals.push_back( Eig_vec3d_round_zero( direction ) );
}

bool test_composite_freedom( std::vector< LocalFreedom_ASM* > freedomList , Eigen::Vector3d direction ){
	size_t i       = 0                  ,
		   numFree = freedomList.size() ;
	for( i = 0 ; i < numFree ; i++ ){ // If the freedom test fails for any in the list , the entire query fails
		if( ! freedomList[i]->test_direction( direction ) ){ return false; }
	}
	return true; // Made it through all freedom checks , Return success
}

void LocalFreedom_ASM::print_all_constraints(){
	size_t i      = 0                        ,
		   numCon = constraintNormals.size() ; 
	cout << "[ ";
	for( i = 0 ; i < numCon ; i++ ){
		print_vec3d_inline( Vec3D_from_aligned_stdvec( constraintNormals , i ) );
		if( i < numCon - 1 ){ cout << " , "; }
	}
	cout << " ]";
}

Eigen::MatrixXd LocalFreedom_ASM::get_all_constraint_normals(){
	size_t i      = 0                        ,
		   numCon = constraintNormals.size() ; 
	Eigen::MatrixXd rtnMatx = Eigen::MatrixXd::Zero( numCon , 3 );
	for( i = 0 ; i < numCon ; i++ ){
		rtnMatx.row(i) = Vec3D_from_aligned_stdvec( constraintNormals , i );
	}
	return rtnMatx;
}

size_t LocalFreedom_ASM::get_number_of_constraints(){ 
	// Return the number of constraint normals contained by the structure
	return constraintNormals.size();
}

// __ End LocalFreedom_ASM _________________________________________________________________________________________________________________

/// ___ END GEO ____________________________________________________________________________________________________________________________

// ___ End Classes _________________________________________________________________________________________________________________________


// === Functions ===========================================================================================================================

// === Mesh Processing ===

FaceIndices faces_from_facets( Eigen::MatrixXd V , Eigen::MatrixXi F , Eigen::MatrixXd N , double CRITRN_ANG ){
	
	// NOTE: This function assumes that the number of vertices has already been reduced
	
	int i         = 0        ,
		j         = 0        ,
		k         = 0        ,
		numRows   = F.rows() ,
		currFacet = 0        ;
	
	std::list<int> facetQueue;
	std::list<int> unusedFacets = lst_range( (int) 0 , numRows - 1 ); // Get a list of facet indices that have not been clustered
	std::list<int>::iterator find_it; // Iterator for list searches
	
	std::vector<std::vector<int>>  faceLists; //-- vectors of facet indices that comprise faces
	std::vector< Eigen::Vector3d , Eigen::aligned_allocator< Eigen::Vector3d > > faceNormals; // normals of the faces
	Eigen::Vector3d scratchRow;
	
	std::vector<std::vector<int>> adj_lst = get_adjacency_list( F );
	
	while( unusedFacets.size() > 0 ){ // While there are unused facets
	
		// Pop facet
		currFacet = unusedFacets.front();
		unusedFacets.remove( currFacet );
		
		// create new face with facet
		std::vector<int> temp;
		temp.push_back( currFacet ); 
		faceLists.push_back( temp ); // Add the new group to the face vector
		faceNormals.push_back( N.row( currFacet ) ); // Add the face normal
	
		// printf( "DEBUG: Neighbors " );
		
		for( i = 0 ; i < adj_lst[ currFacet ].size() ; i++ ){ // for each neighbor
			// if the neighbor is unused , enqueue
			find_it = find( unusedFacets.begin( ) , unusedFacets.end() , adj_lst[ currFacet ][ i ] );
			if( find_it != unusedFacets.end() ){
				// facetQueue.push_back( size_t( adj_lst[ currFacet ][ i ] ) );
				facetQueue.push_back( adj_lst[ currFacet ][ i ] );
			}
		} printf( "\n" );
		// facetQueue.unique(); // Uniqueify queue // Should not have to uniqueify if remove is used
	
		while( facetQueue.size() > 0 ){ // while the queue is non-empty
			// pop facet
			// currFacet = size_t( facetQueue.front() );
			currFacet = facetQueue.front();
			facetQueue.remove( currFacet );
			scratchRow = N.row( currFacet );
			// test angle
			if( angle_between( scratchRow , faceNormals[j] ) <= CRITRN_ANG ){ // angle pass
				// add the facet to the face
				faceLists[j].push_back( currFacet );
				// Calculate a new average normal
				
				faceNormals[j] = ( faceNormals[j] * (double) faceNormals.size() + scratchRow ) / ( faceNormals.size() + 1 );
				faceNormals[j].normalize();
				// mark the facet as used
				unusedFacets.remove( currFacet );
				// for each neighbor
				for( i = 0 ; i < adj_lst[ currFacet ].size() ; i++ ){
					// if the neighbor is unused , enqueue
					find_it = find( unusedFacets.begin( ) , unusedFacets.end() , adj_lst[ currFacet ][ i ] );
					if( find_it != unusedFacets.end() ){
						// facetQueue.push_back( size_t( adj_lst[ currFacet ][ i ] ) );
						facetQueue.push_back( adj_lst[ currFacet ][ i ] );
					}
				}
			} // angle fail : NO OP
		}
		j++; // Advance to the next cluster
	}
	
	FaceIndices rtnStruct;
	
	rtnStruct.faceLists   = faceLists;
	rtnStruct.faceNormals = faceNormals;
	
	return rtnStruct;
}

// ___ End Mesh ___

void ASP_init_colors(){
	_RED << 1.0 , 0.0 , 0.0; // Colors
	_YLW << 1.0 , 1.0 , 0.0;
	_GRN << 0.0 , 1.0 , 0.0;
	_AQU << 0.0 , 1.0 , 1.0;
	_BLU << 0.0 , 0.0 , 1.0;
	_PNK << 1.0 , 0.0 , 1.0;
	// static const vector<Eigen::Vector3d> COLORS_UNITY = { _RED , _YLW , _GRN , _AQU , _BLU , _PNK }; // doesn't work

	_WHT << 1.0 , 1.0 , 1.0; // Values
	_GRY << 0.5 , 0.5 , 0.5;
	_BLK << 0.0 , 0.0 , 0.0;
}

// === Planning ===

// NOTE: THIS FUNCTION IS SOMEWHAT INEFFICIENT , USE UNTIL YOU CAN WRITE A BETTER ONE
std::vector< part_face_pair > adjacent_sides_from_parts( std::vector< Part_ASM* > partPtrList , double CRIT_ANG , double CRIT_DST ){
	size_t numParts  = partPtrList.size() , // Fetch the number of parts in the list
		   numFaces  = 0                  , // Number of faces
		   nFacOthr  = 0                  , // Number of possible opposing faces
		   nFacOthr2 = 0                  ,
	       i         = 0                  , // Counting vars
	       j         = 0                  , //    /
	       k         = 0                  , //   /
	       fCount    = 0                  ; // _/
	double angle      = 0.0d , 
		   separation = 0.0d ; 
	bool   currFacAdj = false ; 
	std::vector< part_face_pair > rtnList;
	//~ std::vector< Polygon_ASM* > oppFacePtrList;
	std::vector< Polygon_ASM >  oppFaceList;
	std::vector< Polygon_ASM >  tempList;
	
	bool   showDEBUG = false , // DEBUG FLAGS
		   showMATCH = false , 
		   DEBUGVIZ  = false ,
		   FLATCHECK = false ;
	
	// 1. Calc the lab poses of all of the part sides
	for( i = 0 ; i < numParts ; i++ ){ 
		//~ partPtrList[i]->update_all_face_mrkr_pose(); 
		partPtrList[i]->calc_all_faces(); 
	}
	
	for( i = 0 ; i < numParts - 1 ; i++ ){ // 2. For each part
		
		std::vector< Polygon_ASM > partFacesList = partPtrList[i]->get_all_faces(); // Get a list of all the faces of this part
		
		if( showDEBUG ){  cout << "DEBUG: " << "Part i Pose" << partPtrList[i]->get_pose() << endl;  }
		
		// 3. Create a list of all the faces that do not belong to this part
		//~ oppFacePtrList.clear(); // Erase the list ( This will not delete the pointers )
		oppFaceList.clear();
		for( j = i + 1 ; j < numParts ; j++ ){ // For each of the parts that follow this one on the list
			tempList.clear();
			tempList = partPtrList[j]->get_all_faces(); // Fetch faces
			numFaces = tempList.size();
			if( showDEBUG ){  cout << "DEBUG: " << "Part j Pose" << partPtrList[j]->get_pose() << endl;  }
			//~ for( k = 0 ; k < numFaces ; k++ ){ oppFacePtrList.push_back( &(tempList[k]) ); }
			for( k = 0 ; k < numFaces ; k++ ){ oppFaceList.push_back( tempList[k] ); } // COPY ALL THE TIME
		} // Now there is a list of canidate opposing faces
		
		numFaces  = partFacesList.size();
		//~ nFacOthr  = oppFacePtrList.size();
		nFacOthr2 = oppFaceList.size();
		
		if( showDEBUG ){  
			cout << "DEBUG: partFacesList has  " << partFacesList.size() << " faces" << endl;
			cout << "DEBUG: oppFacePtrList has " << oppFaceList.size() << " faces" << endl;
		}
		
		// 4. For each pairing of face for this part and face from another part
		for( j = 0 ; j < numFaces ; j++ ){ // --- partFacesList   j
			//~ for( k = 0 ; k < nFacOthr ; k++ ){ // oppFacePtrList  k
			for( k = 0 ; k < nFacOthr2 ; k++ ){ // oppFacePtrList  k
				
				//~ if( oppFacePtrList[k]->is_adjacent_to( partFacesList[j] , CRIT_ANG , CRIT_DST ) ){
				if( oppFaceList[k].is_adjacent_to( partFacesList[j] , CRIT_ANG , CRIT_DST ) ){
					// 9. Append the pairing to the list
					//~ part_face_pair nuPair{ oppFacePtrList[k]->get_ID() , partFacesList[j].get_ID() };
					part_face_pair nuPair{ oppFaceList[k].get_ID() , partFacesList[j].get_ID() };
					rtnList.push_back( nuPair );
					
					// 10. Visualize debug info, if flag active
					if( DEBUGVIZ ){
						fCount++; // There is a new file for every face pair, this is likely very slow!
						stringstream rtnStr;
						std::fstream facetFile;
						Eigen::MatrixXd labFacet_j = partFacesList[j].get_lab_vertices();
						Eigen::MatrixXd labFacet_k = oppFaceList[k].get_lab_vertices();
						
						// 8. Determine if the flattened faces overlap
						Eigen::Vector3d origin = partFacesList[j].get_lab_center();
						Eigen::Vector3d zBasis = partFacesList[j].get_lab_norm();
						Eigen::Vector3d xBasis = Eigen::Vector3d( 0 , 1 , 0 ).cross( zBasis ); // Direction doesn't actually matter
						Eigen::MatrixXd flat_j = partFacesList[j].project_lab_pnts_onto_3D_plane_2D( origin , zBasis , xBasis );
						Eigen::MatrixXd flat_k = oppFaceList[k].project_lab_pnts_onto_3D_plane_2D( origin , zBasis , xBasis );
						//~ Eigen::MatrixXd flat_j = partFacesList[j].project_lab_brdr_onto_3D_plane_2D( origin , zBasis , xBasis , true );
						//~ Eigen::MatrixXd flat_k = oppFacePtrList[k]->project_lab_brdr_onto_3D_plane_2D( origin , zBasis , xBasis , true );
						
						// i. Open file for writing
						facetFile.open( "/home/jwatson/output/adjacent-facets_" + prepad( to_string( fCount ) , 5 , '0' ) + "_" + timestamp() + ".txt" , 
										std::fstream::out | std::fstream::trunc );
						if ( facetFile.is_open() ){ 
							
							// A. Write the vertices of one face to the file in a block of contiguous lines
							facetFile << vertices_to_string( labFacet_j );
							// B. Write an empty line
							facetFile << endl;
							// C. Write the vertices of one face to the file in a block of contiguous lines
							facetFile << vertices_to_string( labFacet_k );
							// D. Write the projection plane to the file
							facetFile << "origin: " << origin(0) << " , " << origin(1) << " , " << origin(2) << endl;
							facetFile << "zBasis: " << zBasis(0) << " , " << zBasis(1) << " , " << zBasis(2) << endl;
							facetFile << "xBasis: " << xBasis(0) << " , " << xBasis(1) << " , " << xBasis(2) << endl;
							
							facetFile << endl;
							facetFile << "~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~" << endl << endl;
							
							// E. Write the flat shapes to the file
							facetFile << verts2D_to_string( flat_j ) << endl << endl;
							facetFile << verts2D_to_string( flat_k ) << endl;
							
							// E. Close file
							facetFile.close();
						}else{  cout << "FAILED TO OPEN DEBUG FILE!" << endl;  }
					}
					
				}
			}
		}
	}
	return rtnList;
}

void print_part_face_pair_vec( std::vector< part_face_pair > pair_vec ){
	size_t i        = 0               ,
		   numPairs = pair_vec.size() ;
	for( i = 0 ; i < numPairs ; i++ ){
		cout << "[ ( " << pair_vec[i].a.part << " , " << pair_vec[i].a.face << " ) , ( " 
		               << pair_vec[i].b.part << " , " << pair_vec[i].b.face << " ) ]"    << endl;
	}
}

// return an NxN matrix of part freedoms related to the present pose // This should be a version of the completed product pose
// This represents the non-directional blocking graph
// NOTE: This function assumes that parts have the IDs 0 to totalProblemParts - 1 , inclusive
NDBGrelation NDBG_from_posed_parts( std::vector< Part_ASM* > partPtrList , size_t totalProblemParts ,
									double CRIT_ANG , double CRIT_DST ){
										
	NDBGrelation rtnStruct;
										
	size_t i        =  0                 , // Outer loop counter
		   j        =  0                 , // Inner loop counter
		   numParts = partPtrList.size() , // Total number of parts under consideration
		   k        =  0                 , // Adjacency loop counter
		   numPairs =  0                 , // Total number of adjacencies under consideration
		   oneDex   =  0                 ,
		   twoDex   =  0                 ;
	
	llin sideOnePart = -1 , // These hold each side of the relationship
		 sideOneFace = -1 ,
		 sideTwoPart = -1 ,
		 sideTwoFace = -1 ;
	bool showDEBUG = false;
	
	std::vector< part_face_pair > adjacencies; // Part adjacency relationships
	
	std::vector< std::vector< LocalFreedom_ASM > > NDBG; // Non-Directional Blocking Graph to Return
	
	IndexSearchResult partResult_i , 
					  partResult_j ;
	
	if(showDEBUG){  cout << "DEBUG , NDBG_from_posed_parts - " << "About to create graph structure ..." << endl;  }
	// 1. For every possible pairing of parts , create an empty local freedom object that represents their interference
	for( i = 0 ; i < totalProblemParts ; i++ ){
		std::vector< LocalFreedom_ASM > temp;
		NDBG.push_back( temp );
		for( j = 0 ; j < totalProblemParts ; j++ ){
			LocalFreedom_ASM nuFree{};
			NDBG[i].push_back( nuFree );
		}
	}
	
	if(showDEBUG){  
		cout << "DEBUG , NDBG_from_posed_parts - " 
			 << "Number of parts in vec: " << numParts << " , Number of parts in problem: " << totalProblemParts << endl;
		
		cout << "DEBUG , NDBG_from_posed_parts - " << "About to calc adjacency for " << partPtrList.size() << " parts ..." << endl;
	}
	// 2. Obtain adjacency
	adjacencies = adjacent_sides_from_parts( partPtrList , CRIT_ANG , CRIT_DST );
	
	if(showDEBUG){  
		cout << "DEBUG , NDBG_from_posed_parts - " << "Discovered " << adjacencies.size() << " adjacencies." << endl;
		cout << "DEBUG , NDBG_from_posed_parts - " << "About to store constraints ..." << endl;
	}
	
	// 3. For each relationship in the adjacency , push the corresponding constraints to both sides of the adjacency
	numPairs = adjacencies.size();
	for( k = 0 ; k < numPairs ; k++ ){
		sideOnePart = adjacencies[k].a.part; // Load each side of the relationship
		sideOneFace = adjacencies[k].a.face; 
		sideTwoPart = adjacencies[k].b.part;
		sideTwoFace = adjacencies[k].b.face; 
		
		// ~ DEBUG ~
		bool found1 = false , 
			 found2 = false ;
		
		for( i = 0 ; i < numParts ; i++ ){ 
			// 3.1. Get the index of part 1
			if( partPtrList[i]->get_ID() == sideOnePart ){ 
				oneDex = i;  
				found1 = true;  
			// 3.2. Get the index of part 2
			} else if( partPtrList[i]->get_ID() == sideTwoPart ){ 
				twoDex = i;  
				found2 = true;  
			} 
		}
		
		if( !found1 || !found2 ){
			cout << "DEBUG , NDBG_from_posed_parts - " << "One of the parts was not found!" << endl;
		}
		
		if(showDEBUG){  
			cout << "DEBUG , NDBG_from_posed_parts - " << "About to fetch norms ..." << endl;
			cout << partPtrList[ twoDex ]->get_face_lab_norm_by_ID( sideTwoFace ) << endl;
			cout << partPtrList[ oneDex ]->get_face_lab_norm_by_ID( sideOneFace ) << endl;
		}
		
		// For part one , add the normal of face in two
		//~ NDBG[ sideOnePart ][ sideTwoPart ].add_constraint( partPtrList[ twoDex ]->get_face_lab_norm_by_ID( sideTwoFace ) );
		NDBG[ oneDex ][ twoDex ].add_constraint( partPtrList[ twoDex ]->get_face_lab_norm_by_ID( sideTwoFace ) );
		
		// For part two , add the normal of face in one
		//~ NDBG[ sideTwoPart ][ sideOnePart ].add_constraint( partPtrList[ oneDex ]->get_face_lab_norm_by_ID( sideOneFace ) );
		NDBG[ twoDex ][ oneDex ].add_constraint( partPtrList[ oneDex ]->get_face_lab_norm_by_ID( sideOneFace ) );
	}
	
	// 4. Return the populated graph
	//~ return NDBG;
	rtnStruct.NDBG        = NDBG;
	rtnStruct.adjacencies = adjacencies;
	return rtnStruct;
}

interference_result query_NDBG( std::vector< std::vector< LocalFreedom_ASM > >& NDBG , 
								const std::vector<size_t>& movedParts , const std::vector<size_t>& referenceParts , const Eigen::Vector3d& moveDir ){
	size_t i      = 0                     ,
	       j      = 0                     ,
	       numMov = movedParts.size()     , 
	       numRef = referenceParts.size() ;
	       
	interference_result result;
	result.result = true;
	       
	// 1. For each moving part
	for( i = 0 ; i < numMov ; i++ ){
		// 2. For each reference part
		for( j = 0 ; j < numRef ; j++ ){
			// 3. Query freedom
			if( ! NDBG[ movedParts[i] ][ referenceParts[j] ].test_direction( moveDir ) ){
				// 4. If there is an interference , flag the response as false and add the interference to the vector
				//~ cout << "DEBUG , Failure testing " << movedParts[i] << " against " << referenceParts[j] << endl;
				result.result = false;
				result.interferingPartIDs.push_back( referenceParts[j] );
			}
		}
	}
	return result;
}

Eigen::MatrixXd get_constraints_on_movd( std::vector< std::vector< LocalFreedom_ASM > >& NDBG , 
										 const std::vector<size_t>& movedParts , const std::vector<size_t>& referenceParts ){
											 
	bool SHOWDEBUG = false ,
		 SHOWACCUM = false ;
											 
	Eigen::MatrixXd allDirs;
	size_t i      = 0                     ,
	       j      = 0                     ,
	       numMov = movedParts.size()     , 
	       numRef = referenceParts.size() ,
	       numNrm = 0                     ;
	//  1.1. For each moving part
	for( i = 0 ; i < numMov ; i++ ){
		//  1.2. For each reference part
		for( j = 0 ; j < numRef ; j++ ){
			
			if( SHOWDEBUG ){  cout << "[" << i << "][" << j << "]" << endl;  }
			
			// 1.3. Accumulate constraints associated with this pair
			if( allDirs.rows() < 1 ){
				allDirs = NDBG[ movedParts[i] ][ referenceParts[j] ].get_all_constraint_normals();
				
				if( SHOWDEBUG && SHOWACCUM ){
					cout << "Accumulating Normals ..." << endl;
					cout << allDirs << endl;
				}
				
			}else{
				allDirs = vstack( allDirs , NDBG[ movedParts[i] ][ referenceParts[j] ].get_all_constraint_normals() );
				
				if( SHOWDEBUG && SHOWACCUM ){
					cout << "Accumulating Normals ..." << endl;
					cout << allDirs << endl;
				}
				
			}
		}
	}
	return allDirs;
}

bool test_direction_against_constraints( const Eigen::Vector3d& direction , const Eigen::MatrixXd& constraints ){
	// Return true if 'direction' does not violate any 'constraints' , Otherwise return false
	bool SHOWDEBUG = false ,
		 DBNEWLINE = true  ;
	size_t i       = 0                  ,
		   numDirs = constraints.rows() ;
	double projVal = 0.0d;
	Eigen::Vector3d currNorm;
	// double projection = 0.0d;
	// bool rtnVal = true;
	for( i = 0 ; i < numDirs ; i++ ){ // If the list has no constraints , then the function will return true for any input vector
		// If the projection of the test direction onto the plane normal is negative , it is on teh wrong side of the plane , return fail
		currNorm = constraints.row(i);  currNorm.normalize();
		projVal = round_zero( vec3_project_mag( direction , currNorm ) );

		if( projVal < 0.0d ){ 
			if( SHOWDEBUG ){  cout << "Direction Test FAIL!" << endl;  }
			return false; 
		}
	}
	if( SHOWDEBUG ){  cout << "Direction Test PASS!" << endl;  }
	return true; // Made it through all of the plane normal tests without failure , return succeed
}

Eigen::MatrixXd corners_from_constraints( const Eigen::MatrixXd& constraints ){
	// Retrerive the directions of the edges (corners) of the local part freedom
	
	bool SHOWDEBUG = false ,
		 SHOWACCUM = false ;
		 
	size_t numNrm = 0 ,
		   i      = 0 ,
	       j      = 0 ;
	       
	Eigen::MatrixXd allDirs;
	Eigen::MatrixXd corners;
	Eigen::Vector3d AxB;
	Eigen::Vector3d BxA;
	Eigen::Vector3d A;
	Eigen::Vector3d B;
	Eigen::Vector3d avgV;
	
	double angBtn = 0.0;
	
	// LAMBDA: [&] Capture all named variables by reference
	// Type defaults to void if there is no return statement
	auto check = [&]( const Eigen::Vector3d& direction ){
		return test_direction_against_constraints( direction , allDirs );
	};
	
	//  1. Collect all of the constraint normals  &&  Determine the number of constraints
	allDirs = constraints;
	numNrm  = allDirs.rows();
	if( SHOWDEBUG ){  cout << "There are " << numNrm << " constraints" << endl;  }
	
	if( numNrm > 0 ){
		//  5. Calculate spherical pyramid corners 
		//    For each unique pair of normals
		for( i = 0 ; i < numNrm - 1 ; i++ ){
			for( j = i + 1 ; j < numNrm ; j++ ){
				if( SHOWDEBUG ){  cout << "[ i: " << i << " ][ j: " << j << " ]" << endl;  }
							
				//  6. Compute A x B and B x A
				A = allDirs.row( i );  B = allDirs.row( j );  
				AxB = A.cross( B ).normalized();
				BxA = B.cross( A ).normalized();
				
				angBtn = angle_between( A , B );
				
				//  7. Test resultants and add the ones that pass query to the list of pyramid corners , Only one will pass for prismatic interface
				
				// If the normals are pointing in the same direction or in opposite direction , No new information about corners , Skip
				if( !eq( angBtn , 0.0 ) &&   // Same Direction:     Constaints are identical
					!eq( angBtn , M_PI )  ){ // Opposite Direction: No new corners , But possible Planar Joint , Handled below
				
					if( SHOWDEBUG ){  cout << "Try " << A << " X " << B << " = " << AxB << " ... ";  }
					if( check( AxB ) ){  
						if( SHOWDEBUG ){  cout << "PASS" << endl;  }
						corners = copy_V_plus_row( corners , AxB );  
						
						if( SHOWDEBUG ){
							cout << "Accumulating Corners ..." << endl;
							cout << corners << endl;
						}
						
					}else{  if( SHOWDEBUG ){  cout << "FAIL" << endl;  }  }
					
					if( SHOWDEBUG ){  cout << "Try " << B << " X " << A << " = " << BxA << " ... ";  }
					if( check( BxA ) ){  
						if( SHOWDEBUG ){  cout << "PASS" << endl;  }
						corners = copy_V_plus_row( corners , BxA );  
						
						if( SHOWDEBUG ){
							cout << "Accumulating Corners ..." << endl;
							cout << corners << endl;
						}
						
					}else{  if( SHOWDEBUG ){  cout << "FAIL" << endl;  }  }
				}
			}
		}	
	}
	if( SHOWDEBUG ){
		cout << endl;
		cout << "Gathered " << corners.rows() << " winning corners." << endl;
		sep( "Corner Collection" );
		cout << corners << endl << endl;
	}
	return corners;
}

Eigen::MatrixXd self_conforming_constraints( const Eigen::MatrixXd& constraints ){
	// Return a subset of uniqie constraints that conform to all other constaints
	
	bool SHOWDEBUG = false; // if( SHOWDEBUG ){  cout << "" << endl;  }
	
	if( SHOWDEBUG ){  sep( "self_conforming_constraints" );  }
	
	size_t /* -- */ len = constraints.rows();
	Eigen::MatrixXd uniqConform;
	Eigen::MatrixXd otherConst;
	Eigen::Vector3d curDir;
	for( size_t i = 0 ; i < len ; i++ ){
		curDir     = constraints.row(i);
		otherConst = copy_V_minus_row( constraints , i );
		
		if( SHOWDEBUG ){  cout << "Current Dir: " << curDir << endl
							   << "Remaining Constraints" << endl << otherConst << endl;  }
		
		if( test_against_constraint_norms( otherConst , curDir ) ){
			uniqConform = copy_V_plus_row( uniqConform , curDir );
			if( SHOWDEBUG ){  cout << "PASS" << endl;  }
		}else if( SHOWDEBUG ){  cout << "FAIL" << endl;  }
		
		if( SHOWDEBUG ){  cout << endl;  }
	}
	return uniqify_directions( uniqConform );
}

size_t count_self_conforming_constraints( const Eigen::MatrixXd& constraints ){
	return self_conforming_constraints( constraints ).rows();
}

Eigen::MatrixXd spherical_pyramid_corners_of_local_freedom( std::vector< std::vector< LocalFreedom_ASM > >& NDBG , 
														    const std::vector<size_t>& movedParts , const std::vector<size_t>& referenceParts ){
	// Retrerive the directions of the edges (corners) of the local part freedom
	bool SHOWDEBUG = false ,
		 SHOWACCUM = false ;
			  
	size_t numMov = movedParts.size()     , 
	       numRef = referenceParts.size() ;
	       
	Eigen::MatrixXd allDirs = get_constraints_on_movd( NDBG , movedParts , referenceParts );
	       
	return corners_from_constraints( allDirs );
}

// ~~~ FREEDOM CLASS ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

FREEDOM_CLASS freedom_class_from_constraints( const Eigen::MatrixXd& constraints ){
	// Return freedom class designation from constraints
	
	bool SHOWDEBUG = false , 
		 SHODETAIL = false ;
	
	// 1. Compute corners
	Eigen::MatrixXd corners = corners_from_constraints( constraints );
	
	//  3. Determine Case
	
	//  A. Tally the counts that determine case
	size_t numCnstr      = constraints.rows()                               , 
		   numCornr      = corners.rows()                                   ,
		   numCrnDir     = count_unique_dirs( corners )                     ,
		   numCnstrOppDr = count_opposing_dirs( constraints )               ,
		   numCrnOppDir  = count_opposing_dirs( corners )                   ,
		   numConsistCon = count_self_conforming_constraints( constraints ) ;
		   
	if( SHOWDEBUG ){  sep( "Freedom Case Diagnostics" );
					  if( SHODETAIL ){
					  cout << "Constraints: " << endl /* -------------- */ << constraints   << endl
						   << "Corners: "     << endl /* -------------- */ << corners       << endl;
					  }
					  cout << "Number of Constraints: __________________ " << numCnstr      << endl
						   << "Number of Corners: ______________________ " << numCornr      << endl 
						   << "Number of Corner Directions: ____________ " << numCrnDir     << endl 
						   << "Number of Opposing Constraint Directions: " << numCnstrOppDr << endl
						   << "Number of Opposing Corner Directions: ___ " << numCrnOppDir  << endl
						   << "Number of Consistent Constraints: _______ " << numConsistCon << endl;  }
						   
	//    I. Free _______________ == 0 constraints , == 0 corners , == 0 opposing cnstrnt dir
	// 		 Expanding sphere , We don't really care about this case
	if( numCnstr == 0 ){
		return FREE_SPHERE;
	}
	
	//   IX. Locked _____________ No removal direction available
	Eigen::Vector3d remDir = center_of_freedom_direction( constraints );
	if( is_err( remDir ) ){  return LOCKED;  }
	
	//  VII. Prismatic Bidir ____ >= 3 constraints , >= 6 corners , >= 0 opposing cnstrnt dir , 1 opposing corner dir
	// VIII. Prismatic Unidir ___ >= 4 constraints , >= 4 corners , >= 0 opposing cnstrnt dir , corners point same direction
	//       Translating shadow , subclass determines if meshes are projected in 1 or 2 directions
	if(  ( numCornr >= 3 )  ){
		
		// If all the corners are facing the same direction , Prismatic Unidir , project in one direction
		if( numCrnDir == 1 ){
			return PRISM_UNI;
		// else If all the corners point in only 2 directions , Prismatic Bidir , project in both directions
		
		// DANGER: CRITERION CHANGED
		//~ }else if( numCrnOppDir == 1 ){
		}else if( numConsistCon == 0 ){
			return PRISM_BIDIR;
		}
		
	}
	
	//   II. Half-Space _________ == 1 constraint  , == 0 corners , == 0 opposing cnstrnt dir
	//    V. Dihedral ___________ == 2 constraints , == 2 corners , == 0 opposing cnstrnt dir
	//   VI. Corner _____________ >= 3 constraints , >= 3 corners , == 0 opposing cnstrnt dir
	//       All of these can be represented as a spherical pyramid
	if( numCnstrOppDr == 0 ){
		if( SHOWDEBUG ){  cout << "Computing shells for * Spherical Pyramid * Case ..." << endl;  }
		if( numCnstr == 1 ){  return HALF_SPACE;  }
		else if( numCrnDir == 2 ){  return DIHEDRAL;  }
		else{  return SPHERE_PYRAMID;  }
	}
	
	//  III. Planar _____________ >= 2 constraints , == 0 corners , == 1 opposing cnstrnt dir
	//   IV. Constrained Planar _ >= 2 constraints , == 4 corners , == 1 opposing cnstrnt dir
	if( numCnstrOppDr == 1 ){
		if( numCornr < 1 ){ // No Corners , Planar
			return PLANAR_FREE;
		}else{ // Some corners , Constrained Planar
			return PLANAR_CONSTR;
		}
	}
	//    X. ERROR ______________ This isn't suppoed to happen
	return ERR_FREEDOM;
}

string freedom_string( FREEDOM_CLASS freeState ){
	string rtnStr;
	switch( freeState ){
		case ERR_FREEDOM    :  rtnStr = "ERR_FREEDOM";  break;
		case LOCKED         :  rtnStr = "LOCKED";  break;
		case PRISM_UNI      :  rtnStr = "PRISM_UNI";  break;
		case PRISM_BIDIR    :  rtnStr = "PRISM_BIDIR";  break;
		case PLANAR_CONSTR  :  rtnStr = "PLANAR_CONSTR";  break;
		case PLANAR_FREE    :  rtnStr = "PLANAR_FREE";  break;
		case SPHERE_PYRAMID :  rtnStr = "SPHERE_PYRAMID";  break;
		case DIHEDRAL       :  rtnStr = "DIHEDRAL";  break;
		case HALF_SPACE     :  rtnStr = "HALF_SPACE";  break;
		case FREE_SPHERE    :  rtnStr = "FREE_SPHERE";  break;
		default             :  rtnStr = "UNHANDLED CASE";  break;
	}
	return rtnStr;
}

Eigen::MatrixXd sample_within_constraints( const Eigen::MatrixXd& constraints ,
										   double dTheta , size_t sphereDiv ){
											   
	bool SHOWDEBUG = false;
		   
	if( SHOWDEBUG ){  sep( "sample_within_constraints" );
					  cout << "Constraints: " << endl
						   << constraints << endl; }
											   
	Eigen::MatrixXd rtnMatx;
	
	FREEDOM_CLASS   freeState   = freedom_class_from_constraints( constraints );
	Eigen::MatrixXd corners     = corners_from_constraints( constraints );
	Eigen::MatrixXd opposingCon = get_opposing_dirs( constraints ); 
	size_t /* -- */ len /* - */ = 0;
	Eigen::Vector3d curDir;
	Eigen::Vector3d axis;
	Eigen::Vector3d bgnDir;
	Sphere_d /*- */ sampleSphere{ 1.0 , origin_vec() , sphereDiv };
	
	switch( freeState ){
		case ERR_FREEDOM:  
		case LOCKED:  
			rtnMatx = err_matx();
			break;
		case PRISM_UNI:  
			curDir = corners.row(0);
			rtnMatx = copy_V_plus_row( rtnMatx , curDir.normalized() );
			break;
		case PRISM_BIDIR:
			curDir  = corners.row(0);
			rtnMatx = copy_V_plus_row( rtnMatx , curDir.normalized() );
			curDir  = curDir * -1.0;
			rtnMatx = copy_V_plus_row( rtnMatx , curDir );
			break;
		case PLANAR_FREE:  
			axis   = opposingCon.row(0);
			bgnDir = get_any_perpendicular( axis );
			len    = (size_t) ceil( 2 * M_PI / dTheta );
			for( size_t i = 0 ; i < len ; i++ ){
				curDir = Eigen::AngleAxisd( (double) i * dTheta , axis ) * bgnDir;
				rtnMatx = copy_V_plus_row( rtnMatx , curDir.normalized() );
			}
			break;
		case PLANAR_CONSTR:  
			axis   = opposingCon.row(0);
			bgnDir = get_any_perpendicular( axis );
			len    = (size_t) ceil( 2 * M_PI / dTheta );
			
			if( SHOWDEBUG ){  cout << "PLANAR_CONSTR" << endl
								   << "Opposing Corners:_ " << endl << opposingCon << endl
								   << "Axis: ____________ " << axis << endl
								   << "Zero Angle Vec: __ " << bgnDir << endl
								   << "Number of Samples: " << len << endl; }
			
			for( size_t i = 0 ; i < len ; i++ ){
				curDir = Eigen::AngleAxisd( (double) i * dTheta , axis ) * bgnDir;
				if( SHOWDEBUG ){  cout << "Testing " << curDir << " ... ";  }
				if( test_direction_against_constraints( curDir , constraints ) ){
					if( SHOWDEBUG ){  cout << "PASS" << endl;  }
					rtnMatx = copy_V_plus_row( rtnMatx , curDir.normalized() );
				}else if( SHOWDEBUG ){  cout << "FAIL" << endl;  }
			}
			if( rtnMatx.rows() < 0 ){
				rtnMatx = err_matx();
			}
			break;
		case SPHERE_PYRAMID:  
		case DIHEDRAL:  
		case HALF_SPACE:  
			rtnMatx = sampleSphere.sample_directions_from_direction_constraints( constraints );
			if( rtnMatx.rows() < 0 ){
				rtnMatx = err_matx();
			}
			break;
		case FREE_SPHERE:  
			rtnMatx = sampleSphere.get_all_spherical_directions();
			break;
		default:  
			rtnMatx = err_matx();
	}
	return rtnMatx;
}

// ... END FREEDOM ......................................................................................................................... 

Eigen::Vector3d center_of_freedom_direction( std::vector< std::vector< LocalFreedom_ASM > >& NDBG , 
								             const std::vector<size_t>& movedParts , const std::vector<size_t>& referenceParts ,
								             const Eigen::Vector3d& nudge ){
	// Return the vector of relative motion that will remove a part in a direction furthest from constraints
			  
	bool SHOWDEBUG = false ,
		 SHOWACCUM = false ;
			  
	size_t i      = 0                     ,
	       j      = 0                     ,
	       numMov = movedParts.size()     , 
	       numRef = referenceParts.size() ,
	       numNrm = 0                     ;
	       
	Eigen::MatrixXd allDirs;
	Eigen::MatrixXd corners;
	Eigen::Vector3d AxB;
	Eigen::Vector3d BxA;
	Eigen::Vector3d A;
	Eigen::Vector3d B;
	Eigen::Vector3d avgV;
	
	Eigen::Vector3d rtnDir = err_vec3();
	double /* -- */ rtnMag = 0.0;
	
	double angBtn = 0.0;
	
	// LAMBDA: [&] Capture all named variables by reference
	// Type defaults to void if there is no return statement
	auto check = [&]( Eigen::Vector3d direction ){
		bool SHOWLAMBDA = false;
		//~ if( SHOWDEBUG && SHOWLAMBDA ){  cout << "Checking ..." << endl;  }
		interference_result result = query_NDBG( NDBG , movedParts , referenceParts , direction );
		//~ if( SHOWDEBUG && SHOWLAMBDA ){  cout << "Checked!" << endl;  }
		if( SHOWDEBUG ){  if( result.result ){  cout << "NDBG PASS!" << endl;  }else{  cout << "NDBG FAIL!" << endl;  }  }
		return result.result;
	};
			
	//  1. Collect all of the constraint normals  &&  Determine the number of constraints
					
	if( SHOWDEBUG ){  cout << "About to iterate through constraints ..." << endl;  }
	if( SHOWDEBUG ){  cout << "Moved Parts: " << movedParts << " , Reference Parts: " << referenceParts << endl;  }
	if( SHOWDEBUG ){  cout << "# Moved: " << numMov << " , # Reference: " << numRef << endl;  }
					
	allDirs = get_constraints_on_movd( NDBG , movedParts , referenceParts );
	numNrm = allDirs.rows();
	
	if( SHOWDEBUG ){
		cout << endl;
		cout << "Gathered " << allDirs.rows() << " constraints." << endl;
		sep( "Direction Collection" );
		cout << allDirs << endl;
	}
	
	//  2. FREE: If there are no constraints , Direction is the same as 'nudge' and magnitude is 10
	if( allDirs.rows() < 1 ){  
		//~ return Eigen::Vector3d( 0.0 , 0.0 , 0.0 );  
		if( SHOWDEBUG ){  cout << "Case: FREE" << endl;  }
		rtnDir = nudge;
		rtnMag = 10.0;
	//  3. HALF-SPACE: elif there is only one constraint , Direction is the same as constraint normal
	}else if( allDirs.rows() == 1 ){  
		if( SHOWDEBUG ){  cout << "Case: HALF-SPACE" << endl;  }
		rtnDir = allDirs.row(0);
		rtnMag = 1.0;
	//  4. else there are 2 or more constraints , Constraint is at least dihedral
	}else{
	
		//  5. Calculate spherical pyramid corners 
		corners = spherical_pyramid_corners_of_local_freedom( NDBG , movedParts , referenceParts );
		
		if( SHOWDEBUG ){
			cout << endl;
			cout << "Gathered " << corners.rows() << " winning corners." << endl;
			sep( "Corner Collection" );
			cout << corners << endl << endl;
		}
			
		//  8. PLANAR: If there are no corners , but two or more constraints , Direction is 'nudge' projected onto the freedom plane
		if( corners.rows() < 1 ){
			if( SHOWDEBUG ){  cout << "Case: PLANAR" << endl;  }
			A = allDirs.row(0);
			rtnDir = vec_proj_to_plane( nudge , A ).normalized();
			rtnMag = 5.0;
		}else{
			
			//  9. Compute average of the corners
			avgV = get_average_V( corners );
			if( SHOWDEBUG ){  cout << "Average Corner " << avgV << endl;  }
						
			//  10. If the average is NaN or 0 , then the suggested direction may be ambiguous
			if(  is_err( avgV )  ||  eq( avgV.norm() , 0.0d )  ){
				
				// 11. DIHEDRAL: If there are only two opposing corners , Then there are only two cross products that have not been skipped
				if( corners.rows() == 2 ){
					if( SHOWDEBUG ){  cout << "Case: DIHEDRAL" << endl;  }
					rtnDir = get_average_V( allDirs ).normalized();
					rtnMag = 1.0;
					
				// 12. PRISMATIC BIDIRECTIONAL: There are only two unique opposing corners representing a sliding joint
				}else{
					if( SHOWDEBUG ){  cout << "Case: PRISMATIC BIDIRECTIONAL" << endl;  }
					// A. Find the unique directions of the sliding joint
					bool foundB = false;
					size_t numCorners = corners.rows();
					A = corners.row(0);
					for( size_t k = 1 ; k < numCorners ; k++ ){
						B = corners.row(k);
						if( A.dot( B ) < 0 ){  break;  }
					}
					if( SHOWDEBUG ){  cout << "Prismatic Corner A: " << A << endl 
										   << "Prismatic Corner B: " << B << endl;  }
					// B. Dot the 'nudge' with each, and choose the direction that is closest to 'nudge'
					if( nudge.dot( A ) >= nudge.dot( B ) ){  rtnDir = A;  }
					else{  rtnDir = B;  }
					rtnMag = 2.0;
				}
			// 13. CORNER: else there are is a collection of corners with that do not cancel out
			}else{
				if( SHOWDEBUG ){  cout << "Case: CORNER" << endl;  }
				rtnDir = avgV.normalized();
				rtnMag = 1.0;
			}
		}
	}
	// N. Return the safest direction ( or error ) and magnitude code
	if( check( rtnDir ) ){  
		if( SHOWDEBUG ){  cout << "Returning: " << rtnDir * rtnMag << endl;  }
		//~ return rtnDir * rtnMag;  
		return rtnDir.normalized();  
	}
	else{  
		if( SHOWDEBUG ){  cout << "Returning: " << err_vec3() << endl;  }
		return err_vec3();  
	}
}

Eigen::Vector3d center_of_freedom_direction( const Eigen::MatrixXd& constraints , const Eigen::Vector3d& nudge ){
	// Return the vector of relative motion that will remove a part in a direction furthest from constraints
			  
	bool SHOWDEBUG = false ,
		 SHOWACCUM = false ;
	       
	Eigen::MatrixXd allDirs = constraints;
	Eigen::MatrixXd corners = corners_from_constraints( constraints );
	Eigen::Vector3d AxB;
	Eigen::Vector3d BxA;
	Eigen::Vector3d A;
	Eigen::Vector3d B;
	Eigen::Vector3d avgV;
	
	size_t i      = 0              ,
	       j      = 0              ,
	       numNrm = allDirs.rows() ;
	
	Eigen::Vector3d rtnDir = err_vec3();
	double /* -- */ rtnMag = 0.0;
	
	double angBtn = 0.0;
	
	// LAMBDA: [&] Capture all named variables by reference
	// Type defaults to void if there is no return statement
	auto check = [&]( const Eigen::Vector3d& direction ){
		return test_direction_against_constraints( direction , allDirs );
	};
			
	//  1. Collect all of the constraint normals  &&  Determine the number of constraints
					
	
	if( SHOWDEBUG ){
		cout << endl;
		cout << "Gathered " << allDirs.rows() << " constraints." << endl;
		sep( "Direction Collection" );
		cout << allDirs << endl;
	}
	
	//  2. FREE: If there are no constraints , Direction is the same as 'nudge' and magnitude is 10
	if( allDirs.rows() < 1 ){  
		//~ return Eigen::Vector3d( 0.0 , 0.0 , 0.0 );  
		if( SHOWDEBUG ){  cout << "Case: FREE" << endl;  }
		rtnDir = nudge;
		rtnMag = 10.0;
	//  3. HALF-SPACE: elif there is only one constraint , Direction is the same as constraint normal
	}else if( allDirs.rows() == 1 ){  
		if( SHOWDEBUG ){  cout << "Case: HALF-SPACE" << endl;  }
		rtnDir = allDirs.row(0);
		rtnMag = 1.0;
	//  4. else there are 2 or more constraints , Constraint is at least dihedral
	}else{
		
		if( SHOWDEBUG ){
			cout << endl;
			cout << "Gathered " << corners.rows() << " winning corners." << endl;
			sep( "Corner Collection" );
			cout << corners << endl << endl;
		}
			
		//  8. PLANAR: If there are no corners , but two or more constraints , Direction is 'nudge' projected onto the freedom plane
		if( corners.rows() < 1 ){
			if( SHOWDEBUG ){  cout << "Case: PLANAR" << endl;  }
			A = allDirs.row(0);
			rtnDir = vec_proj_to_plane( nudge , A ).normalized();
			rtnMag = 5.0;
		}else{
			
			//  9. Compute average of the corners
			avgV = get_average_V( corners );
			if( SHOWDEBUG ){  cout << "Average Corner " << avgV << endl;  }
						
			//  10. If the average is NaN or 0 , then the suggested direction may be ambiguous
			if(  is_err( avgV )  ||  eq( avgV.norm() , 0.0d )  ){
				
				// 11. DIHEDRAL: If there are only two opposing corners , Then there are only two cross products that have not been skipped
				if( corners.rows() == 2 ){
					if( SHOWDEBUG ){  cout << "Case: DIHEDRAL" << endl;  }
					rtnDir = get_average_V( allDirs ).normalized();
					rtnMag = 1.0;
					
				// 12. PRISMATIC BIDIRECTIONAL: There are only two unique opposing corners representing a sliding joint
				}else{
					if( SHOWDEBUG ){  cout << "Case: PRISMATIC BIDIRECTIONAL" << endl;  }
					// A. Find the unique directions of the sliding joint
					bool foundB = false;
					size_t numCorners = corners.rows();
					A = corners.row(0);
					for( size_t k = 1 ; k < numCorners ; k++ ){
						B = corners.row(k);
						if( A.dot( B ) < 0 ){  break;  }
					}
					if( SHOWDEBUG ){  cout << "Prismatic Corner A: " << A << endl 
										   << "Prismatic Corner B: " << B << endl;  }
					// B. Dot the 'nudge' with each, and choose the direction that is closest to 'nudge'
					if( nudge.dot( A ) >= nudge.dot( B ) ){  rtnDir = A;  }
					else{  rtnDir = B;  }
					rtnMag = 2.0;
				}
			// 13. CORNER: else there are is a collection of corners with that do not cancel out
			}else{
				if( SHOWDEBUG ){  cout << "Case: CORNER" << endl;  }
				rtnDir = avgV.normalized();
				rtnMag = 1.0;
			}
		}
	}
	// N. Return the safest direction ( or error ) and magnitude code
	if( check( rtnDir ) ){  
		if( SHOWDEBUG ){  cout << "Returning: " << rtnDir * rtnMag << endl;  }
		return rtnDir.normalized();  
	}
	else{  
		if( SHOWDEBUG ){  cout << "Returning: " << err_vec3() << endl;  }
		return err_vec3();  
	}
}

void print_constraint_state_for_part( std::vector< std::vector< LocalFreedom_ASM > >& NDBG , size_t partID ){
	size_t N = NDBG.size() ,
		   i = 0           ;
	for( i = 0 ; i < N ; i++ ){
		if( i != partID ){
			cout << i << ": ";  NDBG[ partID ][ i ].print_all_constraints();  cout << endl;
		}
	}
	//~ cout << endl;
}



// ___ End Planning ___

// ___ End Func ____________________________________________________________________________________________________________________________




/* === Spare Parts =========================================================================================================================



// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~


   ___ End Parts ___________________________________________________________________________________________________________________________

*/
