/***********  
AsmSeqPlan.cpp
James Watson , 2018 February
Structures and functions to geometric processing and assembly planning

Template Version: 2017-09-23
***********/

#include <AsmSeqPlan.h>


/// === GRAPH PLANNING =====================================================================================================================

string interpret_ACTIONS( const ACTIONS& action ){
	std::vector<string> codes = {
		"ACT_ROTATE" ,
		"ACT_REMOVE" ,
		"ACT_NO_OPR"
	};
	return codes[ action ];
}

// == struct AsmStateRep ===================================================================================================================

bool operator==( const AsmStateRep& state1 , const AsmStateRep& state2 ){
	// Return true if the two states are equal , Otherwise return false
	// 1. Two states must contain the same parts in order to be equal
	if( state1.partsVec.size() == state2.partsVec.size() ){
		if( vec_same_contents( state1.partsVec , state2.partsVec ) ){
			// 2. Two states must be in the same orientation in order to be equal
			if( state1.supportNum == state2.supportNum ){  return true;  }else{  return false;  }
		}else{  return false;  }
	}else{  return false;  }
}

AsmStateRep copy_AsmStateRep( AsmStateRep& original ){
	return AsmStateRep{ vec_copy( original.partsVec ) , original.supportNum , original.numSupports };
}

std::vector<AsmStateRep> copy_state_vec( std::vector<AsmStateRep>& original ){
	std::vector<AsmStateRep> rtnVec;
	size_t len = original.size();
	for( size_t i = 0 ; i < len ; i++ ){  rtnVec.push_back( copy_AsmStateRep( original[i] ) );  }
	return rtnVec;
}

void state_report( AsmStateRep& state ){
	// Print some info about the state
	sep( "State Repr. Report" , 2 , '~' );
	cout << "\t" << "Parts: _ " << state.partsVec.size() << " , " << state.partsVec    << endl;
	cout << "\t" << "Support: " << state.supportNum      << " / " << state.numSupports << endl;
}

// __ End AsmStateRep ______________________________________________________________________________________________________________________


// == struct ActionRep =====================================================================================================================

ActionRep rotate_action_spec( size_t bgn , size_t end ){ 
	// Return specification for a rotate action
	ActionRep rtnAct;
	rtnAct.action = ACT_ROTATE;
	rtnAct.bgnSup = bgn;
	rtnAct.endSup = end;
	return rtnAct;
}

ActionRep remove_action_spec( std::vector<llin> removeIDs ){ 
	// Return specification for a remove action
	ActionRep rtnAct;
	rtnAct.action  = ACT_REMOVE;
	rtnAct.removed = vec_copy( removeIDs );
	return rtnAct;
}

ActionRep remove_one_spec( llin removeID ){ 
	// Return specification for a single-part remove action
	ActionRep rtnAct;
	rtnAct.action  = ACT_REMOVE;
	std::vector<llin> temp;  temp.push_back( removeID );
	rtnAct.removed = temp;
	return rtnAct;
}

ActionRep no_op_asm(){ 
	// Return a no-op action
	ActionRep rtnAct;
	rtnAct.action  = ACT_NO_OPR;
	std::vector<llin> temp;
	rtnAct.removed = temp;
	rtnAct.bgnSup = 0;
	rtnAct.endSup = 0;
	return rtnAct;
}

// __ End ActionRep ________________________________________________________________________________________________________________________


// == struct AsmStateSuccessCode ===========================================================================================================

void clear_ASM_node( AsmStateSuccessCode& node ){  delif( node.stateNode );  }

// __ End AsmStateSuccessCode ______________________________________________________________________________________________________________


// == class AsmStateNode_ASP ===============================================================================================================

string interpret_ValidationCode( size_t index ){
	std::vector<string> codeNames = 
	{ 
		// ~ FAIL ~
		"ERROR_INCOMPLETE" , 
		"NEW_SUB_UNSTABLE" ,
		"NEW_SUB_DISJOINT" ,
		"FLOATING_HAM_ERR" , 
		"REMDIR_NOT_FOUND" ,
		"GOALPOSE_NOT_FND" , 
		"RRTSTAR_NO_SOLTN" ,
		"ALL_GRASP_COLLID" ,
		"GRASP_IK_FAILURE" ,
		"MOVER_SUB_LOCKED" ,
		"VALIDATION_FAULT" ,
		"BAD_RESTING_POSE" ,
		"REFERNC_DISJOINT" ,
		"IK_PATH_INCOMPLT" ,
		// ~ PASS ~
		"PASS_STRAIGHTLIN" ,
		"PASS_RRTSTAR_PTH" ,
		"ROTATED_OK_MAGIC" 
	};
	return codeNames[ index ];
}

// ~ Con/Destructors ~
AsmStateNode_ASP::AsmStateNode_ASP(){ // Create a blank state node
	cost     = DEFAULT_NODE_COST;
	forest   = nullptr;
	assembly = nullptr;
	removed  = nullptr;
}

AsmStateNode_ASP::AsmStateNode_ASP( Assembly_ASM* pAsm ){ // Populated with an assembly
	assembly = pAsm;
	removed  = nullptr;
	cost     = DEFAULT_NODE_COST;
	forest   = nullptr;
	memParts = pAsm->get_part_ID_vec();
	set_state_rep_from_Assembly_ASM( pAsm );
}

AsmStateNode_ASP::AsmStateNode_ASP( const AsmStateNode_ASP& original ){
	// Copy constructor
	
	bool SHOWDEBUG = true; // if( SHOWDEBUG ){  cout << "" << endl;  }
	if( SHOWDEBUG ){  cout << "About to create ..."  << endl;  }
	if( SHOWDEBUG ){  cout << "Set assembly ..." << original.assembly << endl;  }
	assembly = original.assembly; // NOTE: The new node will point to the *SAME* assembly as the original
	removed  = nullptr; // --------- NOTE: because of its usage, will not assume that the same sub will be removed twice in the same problem
	if( SHOWDEBUG ){  cout << "Set cost ..." << endl;  }
	cost     = DEFAULT_NODE_COST;
	forest   = nullptr;
	if( SHOWDEBUG ){  cout << "Set parts list ..." << endl;  }
	memParts = original.assembly->get_part_ID_vec();
	if( SHOWDEBUG ){  cout << "Set state from assembly ..." << endl;  }
	set_state_rep_from_Assembly_ASM( original.assembly );
}

AsmStateNode_ASP::~AsmStateNode_ASP(){ 
	// Erase all of the connected nodes and attached assemblies
	// 1. Clear all vectors
	outgoingEdges.clear(); // Should trigger recursive delete on the target nodes
	history.clear(); // Should trigger recursive delete on the target nodes
	path.clear();
	edgeActions.clear();
	memParts.clear();
	seqPoses.clear();
	// 2. Delete all attached assemblies
	delif( assembly );
	delif( removed  );
	// 3. Delete associated structures
	//~ delif( forest   ); // Probably a bad idea to delete upstream!
}


// ~ Graph Search ~

void AsmStateNode_ASP::add_edge( AsmStateRep nextState , ActionRep action , double edgeCost , bool recalcGeo ){ 
	// Create a new node according to the 'nextState' and connect via outgoing edge
	// NOTE: This function assumes that 'assembly' contains all the parts that are required by 'nextState', in other words , a sub-assembly
	AsmStateNode_ASP* temp = new AsmStateNode_ASP();
	temp->stateRep = nextState; // Assume that this was achieved on the next line
	temp->assembly = assembly->sub_from_spec( nextState.partsVec , nextState.supportNum ); // Construct asm from constituent parts
	if( recalcGeo ){  temp->assembly->recalc_geo();  }
	temp->forest   = forest; // Point to the same containing forest
	//~ temp->assembly->determine_support_stability();
	temp->stateRep.numSupports = temp->assembly->get_stability_determination().size();
	temp->parent = this;
	if( action.action == ACT_REMOVE ){ // Remove Action
		IndexSearchResult suppMatchResult = match_sub_support_to_original( assembly , 
																		   stateRep.supportNum , 
																		   temp->assembly  ); // Match sub support
		if( suppMatchResult.result ){
			cout << "DEBUG , " << "Found a matching support!" << endl;
			temp->assembly->set_support( suppMatchResult.index );
			temp->stateRep.supportNum = suppMatchResult.index;
		}else{
			cout << "DEBUG , " << "Did not find a matching support!" << endl;
			temp->assembly->set_support( nextState.supportNum );
			temp->stateRep.supportNum = nextState.supportNum;
		}
		
	}else{ // else Rotate Action
		temp->assembly->set_support( nextState.supportNum );
		temp->stateRep.supportNum = nextState.supportNum;
	}
	
	memParts = temp->assembly->get_part_ID_vec();
	
	outgoingEdges.push_back( temp ); 
	edgeActions.push_back( action );
	edgeCosts.push_back( edgeCost );
}

AsmStateNode_ASP*  AsmStateNode_ASP::add_edge( std::vector< llin >& partsVec , bool recalcGeo ){ 
	// Create a new node with specified parts and connect via outgoing edge
	size_t defualtSupport = 0;
	double defualtCost    = 0;
	AsmStateNode_ASP* temp = new AsmStateNode_ASP();
	temp->assembly = assembly->sub_from_spec( partsVec , defualtSupport ); // Construct asm from constituent parts
	temp->parent = this;
	if( recalcGeo ){  temp->assembly->recalc_geo();  }
	memParts = vec_copy( partsVec );
	
	outgoingEdges.push_back( temp ); 
	edgeActions.push_back( no_op_asm() );
	edgeCosts.push_back( defualtCost );
	
	return temp; // Return pointer to new node
}

AsmStateNode_ASP* AsmStateNode_ASP::add_edge_remove( Assembly_ASM* contents , Assembly_ASM* subRemove ){
	// Create a new node with specified assembly and connect via outgoing edge
	
	bool SHOWDEBUG = false;
	
	double defualtCost    = 0;
	
	if( SHOWDEBUG ){  cout << "Create a new node ..." << endl;  }
	AsmStateNode_ASP* temp = new AsmStateNode_ASP( contents );
	if( SHOWDEBUG ){  cout << "Assign the removed sub ..." << endl;  }
	temp->removed  = subRemove;
	if( SHOWDEBUG ){  cout << "Assign the parts list ..." << endl;  }
	if( contents ){  temp->memParts = contents->get_part_ID_vec();  }
	
	temp->parent = this;
	
	if( SHOWDEBUG ){  cout << "Add the edge ..." << endl;  }
	outgoingEdges.push_back( temp ); 
	
	if( SHOWDEBUG ){  cout << "Add the action description ..." << endl;  }
	if( subRemove ){  edgeActions.push_back( remove_action_spec( subRemove->get_part_ID_vec() ) );  }
	else{             edgeActions.push_back( no_op_asm() );                                         }
	
	if( SHOWDEBUG ){  cout << "Add the cost ..." << endl;  }
	edgeCosts.push_back( defualtCost );
	
	return temp; // Return pointer to new node
}

AsmStateNode_ASP* AsmStateNode_ASP::add_edge_remove( AsmStateNode_ASP* nuState ){
	// Create a new edge pointing to the specified state
	bool SHOWDEBUG = false; // if( SHOWDEBUG ){  cout << "" << endl;  }
	
	double defualtCost    = 0;
	
	nuState->parent = this;
	
	outgoingEdges.push_back( nuState ); 
	
	if( nuState->removed ){  edgeActions.push_back( remove_action_spec( nuState->removed->get_part_ID_vec() ) );  }
	else{                    edgeActions.push_back( no_op_asm() );                                                }
	
	edgeCosts.push_back( defualtCost );
	
	return nuState;
}

AsmStateNode_ASP* AsmStateNode_ASP::add_edge_rotate( size_t nuSupport ){
	// Create a new node with same assembly and new support and connect via outgoing edge
	
	bool SHOWDEBUG = false; // if( SHOWDEBUG ){  cout << "" << endl;  }
	
	double defualtCost = 0;
	
	if( SHOWDEBUG ){  cout << "Create a new node ..." << endl;  }
	AsmStateNode_ASP* temp = new AsmStateNode_ASP( *this );
	if( SHOWDEBUG ){  cout << "Set the support ..." << endl;  }
	temp->set_support( nuSupport );
	
	temp->parent = this;
	
	if( SHOWDEBUG ){  cout << "Add the edge ..." << endl;  }
	outgoingEdges.push_back( temp ); 
	if( SHOWDEBUG ){  cout << "Add the action description ..." << endl;  }
	edgeActions.push_back( rotate_action_spec( stateRep.supportNum , nuSupport ) );
	if( SHOWDEBUG ){  cout << "Add the cost ..." << endl;  }
	edgeCosts.push_back( defualtCost );
	
	if( SHOWDEBUG ){  cout << "Node creation complete, Return!" << endl;  }
	return temp; // Return pointer to new node
}

AsmStateNode_ASP* AsmStateNode_ASP::add_edge_rotate( size_t nuSupport , Assembly_ASM* posedAsm ){
	// Create a new node with same assembly and new support and connect via outgoing edge
	// NOTE: This function assumes that the assembly has already been posed
	
	bool SHOWDEBUG = false; // if( SHOWDEBUG ){  cout << "" << endl;  }
	
	double defualtCost = 0;
	
	if( SHOWDEBUG ){  cout << "Create a new node ..." << endl;  }
	posedAsm->set_support( nuSupport );
	AsmStateNode_ASP* temp = new AsmStateNode_ASP( posedAsm );
	if( SHOWDEBUG ){  cout << "Set the support ..." << endl;  }
	temp->set_support( nuSupport );
	
	temp->parent = this;
	
	if( SHOWDEBUG ){  cout << "Add the edge ..." << endl;  }
	outgoingEdges.push_back( temp ); 
	if( SHOWDEBUG ){  cout << "Add the action description ..." << endl;  }
	edgeActions.push_back( rotate_action_spec( stateRep.supportNum , nuSupport ) );
	if( SHOWDEBUG ){  cout << "Add the cost ..." << endl;  }
	edgeCosts.push_back( defualtCost );
	
	if( SHOWDEBUG ){  cout << "Node creation complete, Return!" << endl;  }
	return temp; // Return pointer to new node
}

AsmStateNode_ASP* AsmStateNode_ASP::add_edge_rotate( AsmStateNode_ASP* nuState ){
	// Create a new node with same assembly and new support and connect via outgoing edge
	
	bool SHOWDEBUG = false; // if( SHOWDEBUG ){  cout << "" << endl;  }
	
	double defualtCost = 0;
	
	nuState->parent = this;
	
	outgoingEdges.push_back( nuState ); 
	
	edgeActions.push_back( rotate_action_spec( stateRep.supportNum , nuState->stateRep.supportNum ) );
	
	edgeCosts.push_back( defualtCost );
	
	return nuState; // Return pointer to new node
}

void AsmStateNode_ASP::expand_single(){
	// Add edges for all rotations and single-part removals
	// NOTE: This function assumes that 'stateRep.numSupports' represents the correct number of supports, See 'add_edge'
	// NOTE: This function assumes that 'stateRep.supportNum' has been correctly set
	
	cout << "DEBUG , " << "ABOUT TO ENUMERATE ROTATIONS , TOTAL: " << stateRep.numSupports << endl;
	// 1. Create an edge for each possible rotation
	for( size_t i = 0 ; i < stateRep.numSupports ; i++ ){
		if( i != stateRep.supportNum ){ // If the support
			AsmStateRep nextState{ vec_copy( stateRep.partsVec ) , i , stateRep.numSupports };
			//~ std
			//~ ActionRep{  }
			add_edge( nextState , rotate_action_spec( stateRep.supportNum , i ) , rotation_cost( assembly , stateRep.supportNum , i ) , 
					  true );
		}
		//~ cout << i << " ";
		//~ if( i > 10 ) break;
	}
	//~ cout << endl;
	
	cout << "DEBUG , " << "ABOUT TO ENUMERATE REMOVALS" << endl;
	// 2. Create an edge for each possible single-part removal
	// a. Fetch all the part IDs for this assembly
	std::vector<llin> fullParts = assembly->get_part_ID_vec();
	std::vector<llin> rmdrParts; // Remainder of parts for each sub
	std::vector<llin> movdParts; // Part that will be removed , There will only be one at a time for this function
	size_t len = fullParts.size();
	size_t lastSupport = stateRep.supportNum; // Fetch the previous support
	// b. For each of the existing part IDs
	for( size_t j = 0 ; j < len ; j++){
		// c. Create a list parts, minus part j
		rmdrParts = vec_copy_without_elem( fullParts , fullParts[j] );
		// d. Create the next state spec && e. Add edge with cost
		AsmStateRep nextState{ rmdrParts , stateRep.supportNum , stateRep.numSupports };
		movdParts.clear();  movdParts.push_back( fullParts[j] ); // We are only removing one part
		add_edge( nextState , remove_one_spec( fullParts[j] ) , removal_cost( assembly , movdParts ) , true );
		
		//~ cout << j << " ";
		//~ if( j > 10 ) break;
	}
	//~ cout << endl;
}

// Return the next node in sequence , or return 'nullptr' if there is no next
AsmStateNode_ASP* AsmStateNode_ASP::get_next(){  return count_edges() ? (*this)[0] : nullptr;  }


// ~ Getters ~

bool AsmStateNode_ASP::has_edge( AsmStateNode_ASP* queryPtr ){ 
	// Return true if 'queryPtr' is one of the outgoing edges, otherwise return false
	return is_arg_in_vector( queryPtr , outgoingEdges );
}

bool AsmStateNode_ASP::has_part_ID( llin prtID ){ 
	// Return true if the part is present in the sub, otherwise return false
	return is_arg_in_vector( prtID , assembly->get_part_ID_vec() );
}

bool AsmStateNode_ASP::has_part_ID( std::vector<llin> prtID ){
	// Return true if all parts are present in the sub, otherwise return false
	std::vector<llin> allParts = assembly->get_part_ID_vec();
	size_t len = prtID.size();
	for( size_t i = 0 ; i < len ; i++ ){  if( !is_arg_in_vector( prtID[i] , allParts ) ){  return false;  }  }
	return true;
}

// Return the number edges outgoing from this node
size_t AsmStateNode_ASP::count_edges(){  return outgoingEdges.size();  }

AsmStateNode_ASP*& AsmStateNode_ASP::operator[]( size_t index ){
	// Return a reference to the pointer at the specified 'index'
	if( index < outgoingEdges.size() ){  return outgoingEdges[ index ];  }
	else{  throw std::out_of_range ( "AsmStateNode_ASP , There is no edge at this index: " + index );   }
}

// ~ Setters ~

void AsmStateNode_ASP::set_state_rep_from_Assembly_ASM( Assembly_ASM* pAsm ){
	// Read the assembly state from assembly object
	stateRep.partsVec    = assembly->get_part_ID_vec(); //-- All the parts that are present in this assembly // 2018-02-28: Vector to account for repeat fasteners
	stateRep.supportNum  = assembly->get_support(); // Support that the assembly is resting on
	stateRep.numSupports = assembly->get_stability_determination().size();
}

void AsmStateNode_ASP::set_support( size_t supNum ){
	// Set the support in both the assembly and the state representation
	assembly->set_support( supNum );
	stateRep.supportNum = supNum;
}

// __ End AsmStateNode_ASP _________________________________________________________________________________________________________________


// == Graph Search =========================================================================================================================

// == Cost Functions ==

double rotation_cost( Assembly_ASM* asm1 , size_t bgnSupport , size_t endSupport ){ return DEFAULT_EDGE_COST; } // 2018-02-28: PLACEHOLDER
double removal_cost(  Assembly_ASM* asm1 , std::vector<llin>& movedParts ){ return DEFAULT_EDGE_COST; } // ------- 2018-02-28: PLACEHOLDER

double edge_cost( AsmStateNode_ASP* state , ActionRep& action ){
	if( action.action == ACT_ROTATE ){ // Calc rotation cost
		return rotation_cost( state->assembly , action.bgnSup , action.endSup );
	}else{ // Calc removal cost
		return removal_cost( state->assembly , action.removed );
	}
}

double heuristic( AsmStateNode_ASP* state ){
	return (double) state->assembly->how_many_parts();
}

// __ End Cost __


// == Graph Search ==

bool simple_goal( AsmStateRep& evalState ){ 
	// Evaluate whether a goal state has been reached, only one part remaining
	return evalState.partsVec.size() <= 1;
}

bool validate_action_straight_simple( AsmStateNode_ASP* state , ActionRep& action ){
	// Validate a straight-line action , single-part only
	Eigen::Vector3d remDir; // Removal direction
	std::vector<llin> refParts;
	size_t currSupport;
	Pose_ASP savedPose;
	SuccessCode removalSuccess;
	if( action.action == ACT_ROTATE ){ // 1. Verify that the target support is stable
		// a. Fetch the target side && b. Query the target side and return whether it is stable or not
		return state->assembly->is_support_i_stable( action.endSup );
	}else{ // 2. Perform straight-line validation for a removal action	
		// a. Fetch the moved parts list
		// b. Build the stationary parts list
		refParts = vec_copy_without_elem( state->assembly->get_part_ID_vec() , action.removed[0] );
		// c. Fetch support
		currSupport = state->stateRep.supportNum;
		// d. Save the current pose
		savedPose = state->assembly->get_pose();
		// e. Set the pose for this support && f. Obtain a preferred direction for removal
		state->assembly->set_lab_pose_for_support_index( currSupport , Eigen::Vector2d( 0.0 , 0.0 ) );
		remDir = state->assembly->asm_center_of_freedom_direction( action.removed , refParts );
		// g. Validate for removal
		removalSuccess = state->assembly->validate_removal_staightline_stability( action.removed , refParts , remDir , currSupport );
		// h. Validate for stability - LATER
		// i. Restore pose
		state->assembly->set_pose( savedPose );
		// j. Return result
		return removalSuccess.success;
	}
}

struct priorityQ_less_than_ASPNode{ // Compare the costs of two 'AsmStateNode_ASP's
	bool operator()( const std::pair<double,AsmStateNode_ASP*>& op1 , const std::pair<double,AsmStateNode_ASP*>& op2 ) const{
		return std::get<0>( op1 ) < std::get<0>( op2 );
	}
};

AsmSearchResult a_star_single( AsmStateNode_ASP* init_state , 
							   bool (*is_goal)(AsmStateRep&) , bool (*validator)(AsmStateNode_ASP*,ActionRep&) ){
    /* init_state _ value of the initial state
	   is_goal ____ takes state as input returns true if it is a goal state
	   validator __ Check that a transition is valid                          */
		   
    std::priority_queue< std::pair< double , AsmStateNode_ASP* >                , 
                         std::vector< std::pair< double , AsmStateNode_ASP* > > ,
                         priorityQ_less_than_ASPNode                            > frontier;
	double totalCost = 0.0;
	double parntCost = 0.0;
	AsmSearchResult result;
	AsmStateNode_ASP* n_i;
	AsmStateNode_ASP* n_prime;
	std::vector<AsmStateRep> currentPath;
	std::vector<Assembly_ASM*> currentGeo;
	size_t numSuccessors = 0;
	
	// Package the first state and push it onto the frontier
    AsmStateNode_ASP* n0 = init_state; // Create a node from the stating state and push to the frontier
    totalCost = 0.0; // "A ship is safe in harbor, but that's not what ships are for." - William G.T. Shedd
    frontier.push( std::make_pair( totalCost , n0 ) );
    //~ // Include the beginning state for completeness' sake
    //~ result.path.push_back( copy_AsmStateRep( n0->stateRep ) ); 
    //~ result.geoSequence.push_back( n0->assembly );
    
    while( !frontier.empty() ){ // While there are still un-expanded states in the frontier
		n_i       = std::get<1>( frontier.top() ); // Pop the state with the least (accumulated cost + state heuristic)
		parntCost = std::get<0>( frontier.top() );  
		frontier.pop(); // pop does not return a value!
        if( !is_arg_in_vector( n_i->stateRep , result.visited ) ){ // If this state has not been visited before
            result.visited.push_back( copy_AsmStateRep( n_i->stateRep ) ); // Log visit
            currentPath = copy_state_vec( n_i->path ); // Path to the current state
            currentGeo  = vec_copy( n_i->history ); // Assemblies for each state in path
            if( is_goal( n_i->stateRep ) ){ // If this state is the goal, then return path and visit information
				// Concat path
                result.path.insert( result.path.end() , currentPath.begin() , currentPath.end() );
                result.path.push_back( n_i->stateRep );
                // Concat geo history
                result.geoSequence.insert( result.geoSequence.end() , currentGeo.begin() , currentGeo.end() );
                result.geoSequence.push_back( n_i->assembly );
                result.goalMet = true; // Mark success
                return result;
			}else{ // else is not the goal, calculate cost and heuristic for successors and push to frontier
				n_i->expand_single(); // Fetch all the successors for this node (Transition function)
				numSuccessors = n_i->outgoingEdges.size();
                for( size_t j = 0 ; j < numSuccessors ; j++ ){ // For each of the available actions
                    if( validate_action_straight_simple( n_i , n_i->edgeActions[j] ) ){ // If the action is valid, push onto frontier
						
						cout << "DEBUG , " << "Action ACCEPTED!" << endl;
						
						n_prime = n_i->outgoingEdges[j]; // Fetch the edge target
						// Assemble a plan that is the path so far plus the current action
						n_prime->path    = copy_state_vec( currentPath );  n_prime->path.push_back(    n_i->stateRep );
						n_prime->history = vec_copy( currentGeo  );        n_prime->history.push_back( n_i->assembly );
						totalCost = ( parntCost + n_i->edgeCosts[j] + heuristic( n_prime ) ) * -1;
						frontier.push( std::make_pair( totalCost , n_prime ) );
						
					}else{  cout << "DEBUG , " << "Action REJECTED!" << endl;  }
                    
				}
			}
		}
	}
    result.goalMet = false; // Mark failure
    return result; // It's about the jouney, not the destination 
}

// __ End Search __

/// ___ END GRAPH __________________________________________________________________________________________________________________________





/// === VISUALIZATION ======================================================================================================================

void visualize_search_plan( AsmSearchResult& searchResult , RViz_MarkerManager& mrkrMngr , 
							Eigen::Vector2d displayOrigin , Eigen::Vector2d offsetPerAction ){
	size_t len         = searchResult.path.size() , 
		   currSupport = 0                        ;
	Pose_ASP savedPose;
	Eigen::Vector2d currPosXY;
	// 1. For each state
	for( size_t i = 0 ; i < len ; i++ ){
		// 2. Fetch the support
		//~ currSupport = searchResult.path[i].supportNum;
		currSupport = searchResult.geoSequence[i]->get_support();
		// 3. Fetch and save the state geo pose
		savedPose = searchResult.geoSequence[i]->get_pose();
		// 4. Set the geo pose to the appropriate support
		currPosXY = displayOrigin + offsetPerAction * i;
		searchResult.geoSequence[i]->set_lab_pose_for_support_index( currSupport , currPosXY );
		// 5. Load the part markers into the marker manager
		searchResult.geoSequence[i]->load_part_mrkrs_into_mngr( mrkrMngr );
		// 6. Restore the state geo pose
		searchResult.geoSequence[i]->set_pose( savedPose );
	}
}

void visualize_Morato2013_Level_1( RViz_MarkerManager& mrkrMngr , 
								   Eigen::Vector2d displayOrigin , Eigen::Vector2d offsetPerLayer , Eigen::Vector2d offsetPerSub , 
								   Assembly_ASM* original , std::vector< std::vector< llin > > clusterVector ){
	// Visualize a single level of Morato 2013 in order to troubleshoot results
	size_t dfltSupport = 0                    , //- Default support, 0
		   numSubs     = clusterVector.size() ; //- Number of clusters
	Eigen::Vector2d layerOrigin = displayOrigin; // Origin for the current layer
	Eigen::Vector2d currPosXY   = displayOrigin; // Current position to put a sub
	
	Assembly_ASM* currSub = nullptr;
	
	cout << "DEBUG , " << "About to visualize the base assembly ..." << endl;
	
	// 1. Visualize the complete assembly
	Pose_ASP savedPose = original->get_pose();
	original->set_lab_pose_for_support_index( dfltSupport , currPosXY );
	
	//~ original->load_part_mrkrs_into_mngr( mrkrMngr ); // Load the part markers into the marker manager
	original->load_face_mrkrs_into_mngr( mrkrMngr ); // Load the face markers into the marker manager
	
	original->set_pose( savedPose );
	
	// 2. Visualize the layer subs
	layerOrigin += offsetPerLayer; // Increment layer origin by the offset
	// A. For each sub parts list
	for( size_t i = 0 ; i < numSubs ; i++ ){
		
		cout << "DEBUG , " << "About to visualize sub " << i+1 << " ..." << endl;
		
		// B. Form an assembly from the spec
		cout << "DEBUG , " << "About to delete the old sub ..." << endl;
		if( currSub ){  delete currSub;  } // Erase the old sub, if it exists
		cout << "DEBUG , " << "About to create new sub ..." << endl;
		currSub = original->sub_from_spec( clusterVector[i] , dfltSupport );
		
		// C. Increment the separation vector
		currPosXY = layerOrigin + i * offsetPerSub;
		
		cout << "DEBUG , " << "About to set support for sub " << i+1 << " ..." << endl;
		// D. Set the support and location
		currSub->recalc_geo();
		currSub->set_lab_pose_for_support_index( dfltSupport , currPosXY );
		
		cout << "DEBUG , " << "About to fetch markers for sub " << i+1 << " ..." << endl;
		// E. Load the part markers into the marker manager
		currSub->load_part_mrkrs_into_mngr( mrkrMngr ); // Load the part markers into the marker manager
	}
	
}


void visualize_tree_structure_plan( RViz_MarkerManager& mrkrMngr , 
									Eigen::Vector2d displayOrigin , Eigen::Vector2d offsetPerLayer , Eigen::Vector2d offsetPerSub , 
									AsmStateNode_ASP* rootNode ){ // Modified
	// Visualize full depth of a disassembly plan expressed as a tree structure in order to troubleshoot results
	size_t dfltSupport = 0; //- Default support, 0
	Eigen::Vector2d layerOrigin = displayOrigin; // Origin for the current layer
	Eigen::Vector2d currPosXY   = displayOrigin; // Current position to put a sub
	AsmStateNode_ASP* currNode  = nullptr; // ----- Sub to be drawn
	size_t numChilds            = 0 , // ----------- Number of successors of the current sub
		   layerSubCount        = 0 ; // NOT USED?
	std::queue<AsmStateNode_ASP*> currLvl; // All the nodes to be drawn at the current level
	std::queue<AsmStateNode_ASP*> nextLvl; // All the nodes that will be drawn at the next level
	
	
	//  1. Enqueue the root node in the next level
	nextLvl.push( rootNode );
	//  2. While the next level contains nodes
	while( nextLvl.size() > 0 ){
		//  3. Copy the next level to the current level
		currLvl = nextLvl;
		erase_queue( nextLvl );
		//  4. While the current level contains nodes
		while( currLvl.size() > 0 ){
			//  5. Retrieve and Pop a node
			currNode = queue_get_pop( currLvl );
			//  6. Display the node
			currNode->assembly->set_lab_pose_for_support_index( dfltSupport , currPosXY );
			currNode->assembly->load_part_mrkrs_into_mngr( mrkrMngr ); // Load the part markers into the marker manager
			//  7. Advance the layer location
			currPosXY += offsetPerSub;
			//  8. For each successor
			numChilds = currNode->outgoingEdges.size();
			for( size_t j = 0 ; j < numChilds ; j++ ){
				//  9. Enqueue the successor in the next level
				nextLvl.push( currNode->outgoingEdges[j] );
			}
			//~ layerSubCount++;
		}
		// 10. Advance the level location  &&  Set the current drawing location
		layerOrigin += offsetPerLayer;
		currPosXY = layerOrigin;
	}
}

/// ___ END VIZ ____________________________________________________________________________________________________________________________


/// === PLAN PLAYBACK ======================================================================================================================


// == class PlayerFrame ==

std::vector<double> PlayerFrame::get_q(){  return jnt_state_.position;  }
	
// __ End PlayerFrame __


// == class PlanRecording ==================================================================================================================

// ~ Con/Destructors ~
PlanRecording::~PlanRecording(){  clearif( frames );  }

// ~ Playback ~

PlayerFrame* PlanRecording::get_current_frame(){
	// Get the current frame
	return ( frames[ currDex ] );
}

PlayerFrame* PlanRecording::get_next_frame( bool loop ){
	// Get the next frame in sequence, Optionally loop to the front
	// 1. Increment frame
	currDex++;
	size_t len = frames.size();
	if( currDex >= len ){  
		if( loop )
			currDex = 0;  
		else
			currDex = len - 1;
	}
	// 2. Return pointer
	//~ return &( frames[ currDex ] );
	return ( frames[ currDex ] );
}

PlayerFrame* PlanRecording::get_frame( size_t framNum ){
	// Get the specified frame at index framNum
	if( framNum >= frames.size() ){
		return nullptr;
	}else{
		//~ return &( frames[ framNum ] );
		return ( frames[ framNum ] );
	}
}

PlayerFrame* PlanRecording::operator[]( size_t framNum ){ 
	// Get the specified frame at index framNum
	return get_frame( framNum );
}

void PlanRecording::erase_last_frame(){
	// Create a frame that erases the markers of the last element of frames
	PlayerFrame* temp = new PlayerFrame();
	RViz_MarkerManager& tmpMngr = last_elem( frames )->mrkrMngr;
	temp->mrkrMngr = RViz_MarkerManager::anti_manager( tmpMngr );
	push_back( temp );
}

// ~ Control ~

// Set frame to 0
void PlanRecording::rewind(){ currDex = 0; }

bool PlanRecording::set_frame( size_t framNum ){
	// Set frame to specified index
	if( framNum >= frames.size() ){
		return false;
	}else{
		currDex = framNum;
		return true;
	}
}

// ~ Editing ~

void PlanRecording::push_back( PlayerFrame* nuFrame ){
	// Add an animation frame to the vector
	frames.push_back( nuFrame );
}

// Erase all frames
void PlanRecording::clear(){  frames.clear();  }
	
	

// __ End PlanRecording ____________________________________________________________________________________________________________________


// == class PlanPlayer =====================================================================================================================

// ~ Constructor ~

PlanPlayer::PlanPlayer( double frameRateP , PlanRecording* recordingP ){
	frameTimer = StopWatch( 1.0 / frameRateP ); // Start a stopwatch with a 1/Hz [s] period
	recording  = recordingP; // ------------------ Playback data
}

PlanPlayer::~PlanPlayer(){  delif( recording );  }

PlayerFrame* PlanPlayer::current_frame( bool loop ){
	// Return a pointer to the frame that is relevant to this time
	
	// 1. Check if the frame has expired
	if( frameTimer.has_interval_elapsed() ){
		// 2. If the frame has expired, get next
		return recording->get_next_frame( loop );
	}else{
		// 3. If the frame has not expired, get current
		return recording->get_current_frame();
	}
}

// __ End PlanPlayer _______________________________________________________________________________________________________________________

/// ___ END PLAYBACK _______________________________________________________________________________________________________________________


/// === PLAN VALIDATION & EVALUATION =======================================================================================================

std::vector<Pose_ASP> simulate_disasm_step_straight( Assembly_ASM* before_ASM , Assembly_ASM* after_ASM , 
													 Pose_ASP befPose , size_t befSupport , 
													 double removeDist , size_t numSteps ,
													 Eigen::Vector3d floorCenter , double floorXWidth , double floorYLngth ){
														 
	// Return the steps that simulate removing 'after' from 'before' along a straight line
	
	bool SHOWDEBUG = true;
	
	std::vector<Pose_ASP> rtnSeq; // Sequence of poses to return
	std::vector<Pose_ASP> errSeq; // Empty sequence to return if there is an error
	bool rtnErr = false;
														 
	//  1. Get parts lists for moving and reference
	std::vector<llin> befIDs = before_ASM->get_part_ID_vec();
	std::vector<llin> movIDs = after_ASM ->get_part_ID_vec();
	std::vector<llin> refIDs = vec_copy_without_elem( befIDs , movIDs );
	//  2. Create a reference assembly , and set in the 'befPose'
	Assembly_ASM* refAsm = before_ASM->sub_from_spec( refIDs );
	refAsm->set_pose( befPose );
	//  3. Situate the moving assembly within the reference assembly
	Pose_ASP aftOrigPose = after_ASM->get_pose();
	Eigen::Quaterniond befOrient = befPose.orientation;
	after_ASM->set_pose( befPose );
	//  4. Request the removal direction
	Eigen::Vector3d remDir = before_ASM->asm_center_of_freedom_direction( movIDs , refIDs ); // Part ID
	if( !is_err( remDir ) ){
		Pose_ASP interPose{ Eigen::Vector3d( 0.0 , 0.0 , 0.0 ) , befOrient };
		//  5. Create a vector of intermediate poses for the moving sub
		Eigen::Vector3d bgn = befPose.position;
		Eigen::Vector3d end = bgn + remDir * removeDist;
		Eigen::MatrixXd posVec = linspace_pts_3D( bgn , end , numSteps );
		//  5.5. Create a floor to check against
		Floor_ASM tempFloor{ floorCenter , floorXWidth , floorYLngth };
		//  6. For each of the intermediate poses
		for( size_t i = 0 ; i < numSteps ; i++ ){
			//  6.5. Set the moving sub to the intermediate pose
			interPose.position = posVec.row(i);
			after_ASM->set_pose( interPose );
			//  7. Check that the moving sub is not in collision 
			//  A. Check reference collision
			if( after_ASM->collides_with( *refAsm ) ){
				//  8. If there is a collision, return error
				if( SHOWDEBUG ){  cout << "Plan failure, there was a collision with the reference assembly!" << endl;  }
				rtnErr = true;
				break;
			}
			//  B. Check floor collision
			if( after_ASM->collides_with( tempFloor ) ){
				//  8. If there is a collision, return error
				if( SHOWDEBUG ){  cout << "Plan failure, there was a collision with the floor!" << endl;  }
				rtnErr = true;
				break;
			}
			// If we made it this far, the step is valid, add it to the vector
			rtnSeq.push_back( interPose );
		} //  9. If all poses are free, then return the pose list that was checked
	}else{  
		if( SHOWDEBUG ){  cout << "Plan failure, no valid removal direction was returned!" << endl;  }
		rtnErr = true;  
	} // else part is not free to be removed
	// 10. Restore the after pose
	after_ASM->set_pose( aftOrigPose );
	// 11. Return
	if( rtnErr ){  return errSeq;  }else{  return rtnSeq;  }
}


std::vector<Pose_ASP> simulate_disasm_step_straight( AsmStateNode_ASP* before , AsmStateNode_ASP* after , 
													 Pose_ASP befPose , size_t befSupport , 
													 double removeDist , size_t numSteps ,
													 Eigen::Vector3d floorCenter , double floorXWidth , double floorYLngth ){
	// Return the steps that simulate removing 'after' from 'before' along a straight line
	
	bool SHOWDEBUG = true;
	
	std::vector<Pose_ASP> rtnSeq; // Sequence of poses to return
	std::vector<Pose_ASP> errSeq; // Empty sequence to return if there is an error
	bool rtnErr = false;
	
	// 0. Validate that 'after' is a successor to 'before'
	if( before->has_edge( after ) ){
		
		Assembly_ASM* before_ASM = before->assembly;
		Assembly_ASM* after_ASM  = after ->assembly;
		
		rtnSeq = simulate_disasm_step_straight( before_ASM , after_ASM , 
												befPose , befSupport , 
												removeDist , numSteps ,
												floorCenter , floorXWidth , floorYLngth );
		
	}else{  
		if( SHOWDEBUG ){  cout << "Plan failure, after did not succeed before!" << endl;  }
		rtnErr = true;  
	} // else client code did not pass a parent-child pair
	if( rtnErr ){  return errSeq;  }else{  return rtnSeq;  }
}

ParallelGraspState gripper_pose_from_grasp_pair( Eigen::MatrixXd pair , Eigen::Vector3d upZdir , double gripSepLim ){
	// Return the gripper config to grasp
	ParallelGraspState rtnStruct;
	// 1. Get the distance between the points
	Eigen::Vector3d ent = pair.row(0);
	Eigen::Vector3d ext = pair.row(1);
	double separation   = ( ext - ent ).norm();
	// 2. Get the X direction (point to increasing X in world)
	Eigen::Vector3d xBasis;
	if( ent(0) > ext(0) ){  xBasis = ( ent - ext ).normalized();  }
	else{                   xBasis = ( ext - ent ).normalized();  }
	// 3. Get the Y direction
	Eigen::Vector3d yBasis = upZdir.cross( xBasis ).normalized();
	// 4. Get the Z direction (enforce mutual orthogonality)
	Eigen::Vector3d zBasis = xBasis.cross( yBasis ).normalized();
	// 5. Get the center of the grasp
	Eigen::Vector3d origin = get_average_V( pair );
	// 6. Check if the fingers are not too far apart
	rtnStruct.feasible = separation <= gripSepLim;
	// 7. Calc the finger state, whether feasible or no
	std::vector<double> q_fing = { separation / 2.0 , separation / 2.0 };
	rtnStruct.q_fingers = q_fing;
	// 8. Calc the pose
	rtnStruct.graspTarget = pose_from_origin_bases( origin , xBasis , yBasis , zBasis );
	// N. Return
	return rtnStruct;
}

std::vector<llin> balance_ID_A_minus_B( Assembly_ASM* A , Assembly_ASM* B ){
	bool SHOWDEBUG = false;
	std::vector<llin> IDsA = A->get_part_ID_vec();
	if( SHOWDEBUG ){  cout << "A has: " << IDsA << endl;  }
	std::vector<llin> IDsB = B->get_part_ID_vec();
	if( SHOWDEBUG ){  cout << "B has: " << IDsB << endl;  }
	std::vector<llin> rtnVec = vec_copy_without_elem( IDsA , IDsB );
	if( SHOWDEBUG ){  cout << "Diff:_ " << rtnVec << endl;  }
	return rtnVec;
}

Assembly_ASM* Assembly_A_minus_B( Assembly_ASM* A , Assembly_ASM* B ){
	// Return a new assembly that has all the parts of 'A' without any of the parts of 'B'
	bool SHOWDEBUG = false;
	std::vector<llin> IDsAminusB = balance_ID_A_minus_B( A , B );
	if( SHOWDEBUG ){  cout << "Balance ID: " << IDsAminusB << endl;  }
	Assembly_ASM* rtnPtr = A->sub_from_spec( IDsAminusB );
	if( SHOWDEBUG ){  cout << "Returning object pointer: " << pointer_info_str( rtnPtr ) << endl;  }
	return rtnPtr;
}

std::vector<std::vector<llin>> successor_membership( AsmStateNode_ASP* rootNode ){
	// Return a vector vector of the part ID membership of each sub
	std::vector<std::vector<llin>> rtnVecVec;
	size_t len = rootNode->count_edges();
	for( size_t i = 0 ; i < len ; i++ ){  rtnVecVec.push_back( (*rootNode)[i]->assembly->get_part_ID_vec() );  }
	return rtnVecVec;
}

size_t suggest_support_for_straightline_access( AsmStateNode_ASP* rootNode , std::vector<size_t> selectedSuccesors ){
	// Return the support number that will block the fewest number of straightline removals, Assuming removals are unobstructed along freedom center
	Assembly_ASM*     rootAsm = rootNode->assembly;
	Eigen::MatrixXd   remDirs;
	Eigen::Vector3d   remDir;
	Assembly_ASM*     currAsm;
	std::vector<llin> currIDs;
	std::vector<llin> balnIDs;
	// 0. Store Assembly pose  &&  Set the assembly to the origin pose
	Pose_ASP origPose = rootAsm->get_pose();
	// 2. For each successor
	size_t len = rootNode->count_edges();
	for( size_t i = 0 ; i < len ; i++ ){
		if( is_arg_in_vector( i , selectedSuccesors ) ){
			currAsm = (*rootNode)[i]->assembly;
			// 3. Get a parts list
			currIDs = currAsm->get_part_ID_vec();
			// 4. Get a balance parts list
			balnIDs = balance_ID_A_minus_B( rootAsm , currAsm );
			// 4. Get a removal direction
			remDir = rootAsm->asm_center_of_freedom_direction( currIDs , balnIDs );
			if( !is_err( remDir ) ){  remDirs = copy_V_plus_row( remDirs , remDir );  }
		}
	}
	size_t /* ------ */ numSpprt = rootAsm->how_many_supports();
	size_t /* ------ */ numDirct = remDirs.rows();
	std::vector<double> access;	
	// 5. For each support
	for( size_t supDex = 0 ; supDex < numSpprt ; supDex++ ){
		double /* -- */ supSum  = 0.0;
		Eigen::Vector3d supNorm = rootAsm->get_support_normal( supDex );
		// 6. For each removal direction
		for( size_t dirDex = 0 ; dirDex < numDirct ; dirDex++ ){
			// 7. Cross the removal direction with the opposite of the support normal and sum
			remDir = remDirs.row( dirDex );
			supSum += remDir.dot( supNorm );		
		}
		access.push_back( supSum );
	}
	// 8. Get the index of the greatest sum and return , must be stable
	size_t rtnDex = max_index_in_vec( access );
	
	// while( !is_support_i_stable( size_t supportIndex ) )
	return rtnDex;
}

size_t suggest_support_for_straightline_access( AsmStateNode_ASP* rootNode ){
	std::vector<size_t> allEdgeDices = vec_range( (size_t) 0 , rootNode->count_edges() - 1 );
	return suggest_support_for_straightline_access( rootNode , allEdgeDices );
}

// 2018-06-14: For now, processing assemblies in stored order. The order of the sequence matters, and I need to implement a reasoned way to 
//             deal with this
// 2018-06-14: Ignoring Z-rotation for now! This will be important soon!

AsmStateNode_ASP* precedence_layer_root_to_action_sequence( AsmStateNode_ASP* rootNode , Eigen::Vector3d setdownCenter , 
														    double removeDist , size_t numSteps ,
														    double floorXWidth , double floorYLngth ){
	// Decompose a precedence layer into a sequence of executable actions that results in all of the subs in the layer
	
	bool SHOWDEBUG = true; // if( SHOWDEBUG ){  cout <<  << endl;  }
	
	AsmStateNode_ASP* currState = new AsmStateNode_ASP( *rootNode ); // Pointer to the current state, used to build a chain
	AsmStateNode_ASP* initState = currState; // ----------------------- This is the root / init state to return
	
	// A. Suggest an initial support
	size_t initSupport = suggest_support_for_straightline_access( rootNode );
	currState->set_support( initSupport );
	
	if( SHOWDEBUG ){  cout << "Suggested initial support: " << initSupport << endl;  }
	
	size_t len = rootNode->count_edges();
	
	//  0. Get the parts list for every root successor  &&  Generate coverage checklist
	std::vector<std::vector<llin>> subsIDvec = successor_membership( rootNode ); // ----------- Part IDs for every successor edge
	std::vector<bool> /* ------ */ coverage  = bool_false_vector( rootNode->count_edges() ); // Vector of bool flags, one for each edge
	
	//  1. Fetch the point for the root asm, This will be used to query the liaison graph
	Assembly_ASM* rootAsm = rootNode->assembly; // ---------------------------------------------- Assembly stored in the initial level state
	Pose_ASP origPose     = rootAsm->get_pose(); // --------------------------------------------- Get the pose stored in the initial assembly
	Pose_ASP befPose      = rootAsm->set_lab_pose_for_index_3D( initSupport , setdownCenter ); // Pose before any action takes place
	
	//  2. For each edge outgoing from the root
	Assembly_ASM*     currAsm     = nullptr; // --- Pointer to the current assembly
	Assembly_ASM*     balance     = nullptr; // --- ( Root - Current ) assembly , Considering the operation independently
	Assembly_ASM*     runningAsm  = rootAsm; // --- Running assembly, the leftovers as we progress through actions
	size_t /* ---- */ actionCount = 0 , // -------- Number of actions in the sequence
					  removeCount = 0 , // -------- Number of removal actions in the sequence
					  runnSupport = initSupport; // Support of the running balance
	std::vector<llin> currSubId; // --------------- Constituent parts of the current sub
	std::vector<llin> currBalId; // --------------- ( Root - Current ) parts , Considering the operation independently
	std::vector<llin> runningId; // --------------- Running Parts, the leftovers as we progress through actions
	Eigen::Vector3d   remDir; // ------------------ Removal direction
	
	if( SHOWDEBUG ){  cout << "About to instantiate floor at " << setdownCenter 
						   << " with width " << floorXWidth << " and length " << floorYLngth;  }
	
	// A. Instantiate a floot at the appropriate location
	Floor_ASM asmTable{ setdownCenter , floorXWidth , floorYLngth };
	
	if( SHOWDEBUG ){  cout << " ... Floor created!" << endl;  }
	
	for( size_t i = 0 ; i < len ; i++ ){
		
		if( SHOWDEBUG ){  cout << "Sequencing edge " << 1+i << " of " << len << endl;  }
		
		if( SHOWDEBUG ){  cout << "Set assembly ... "  << endl;  }
		currAsm   = (*rootNode)[i]->assembly;
		if( SHOWDEBUG ){  cout << "Retreive parts: ";  }
		currSubId = currAsm->get_part_ID_vec();
		if( SHOWDEBUG ){  cout << currSubId << endl;  }
		
		//  3. Calc the balance ( root minus successor )
		if( SHOWDEBUG ){  cout << "Compute balance: ";  }
		
		currBalId = balance_ID_A_minus_B( runningAsm , currAsm );
		if( currBalId.size() == 0 ){  continue;  } // Skip this assembly if there is no balance left
		
		balance   = Assembly_A_minus_B( runningAsm , currAsm );
		
		if( SHOWDEBUG ){  cout << currBalId << endl;  }
		
		if( SHOWDEBUG ){  cout << "Moved Parts: " << currSubId << " , Reference Parts: " << currBalId << endl;  }
		
		//  4. If the balance is connected 
		if(  is_subgraph_connected( runningAsm , currBalId )  ){
			
			if( SHOWDEBUG ){  cout << "Remainder subgraph is connected, Process edge ..." << endl;  }
			
			//  5. Verify straight removal against floor
			remDir = runningAsm->asm_center_of_freedom_direction( currSubId , currBalId ); // Part ID
			
			if( SHOWDEBUG ){  cout << "Suggested removal diection: " << remDir << endl;  }
			
			SuccessCode result = runningAsm->validate_removal_staightline_stability( currSubId , currBalId , // Validate for removal and stability 
																					 remDir , runnSupport , removeDist , numSteps );
			if( !result.success ){
				//  6. If removal blocked by floor, insert a rotation action // 2018-06-14: Ignoring Z-rotation for now! This will be important soon!
				
				if( SHOWDEBUG ){  cout << "Removal FAILED, adding a rotation action ... " << endl;  }
				
				IndexSuccesLookup dexFound = runningAsm->suggest_support_for_staightline_stability( currSubId , currBalId , removeDist , numSteps );
				
				if( SHOWDEBUG ){  
					cout << "New support: " << dexFound.bestDex << endl;  
					cout << "Does this node have an assembly?: " << ( currState->assembly == nullptr ? "NO" : "YES" ) << endl;  
				}
				
				currState = currState->add_edge_rotate( dexFound.bestDex );
				
				//~ Pose_ASP nuPose = currState->assembly->set_lab_pose_for_index_3D( dexFound.bestDex , setdownCenter );
				Pose_ASP nuPose = runningAsm->set_lab_pose_for_index_3D( dexFound.bestDex , setdownCenter , true );
				
				currState->staticPose = nuPose;
				
				if( SHOWDEBUG ){  cout << "Rotation added!" << endl;  }
				
			}
			
			if( SHOWDEBUG ){  cout << "Adding a removal action ..." << endl;  }
			
			//  7. When removal not blocked by floor, Generate a removal action
			currState = currState->add_edge_remove( balance , currAsm );
			
			currState->seqPoses = simulate_disasm_step_straight( runningAsm , currAsm , 
																 runningAsm->get_pose() , runningAsm->get_support() , 
																 removeDist , numSteps ,
																 setdownCenter , floorXWidth , floorYLngth );
			
			
			
			//  8. Mark sub coverage
			coverage[i] = true;
			//  9. Set the current balance to the remainder left after removal
			runningAsm = balance; 
		}else if( SHOWDEBUG ){  cout << "Remainder subgraph NOT connected, Skipping edge ..." << endl;  }
	}
	
	if( SHOWDEBUG ){  cout << "Determining subassembly coverage ..." << endl;  }
	
	// 10. The balance should be one of the remaining subs
	for( size_t j = 0 ; j < subsIDvec.size() ; j++ ){
		if( vec_same_contents( currBalId , subsIDvec[j] ) ){  coverage[j] = true;  }
	}
	
	if( SHOWDEBUG ){  cout << "Sub Coverage: " << coverage << endl;  }
	
	// 11. Restore the original pose
	rootAsm->set_pose( origPose );
	
	// 12. Return
	return initState;
}

AsmStateNode_ASP* precedence_layer_root_to_action_sequence_ALTERNATE( AsmStateNode_ASP* rootNode , Eigen::Vector3d setdownCenter , 
																	  double removeDist , size_t numSteps ,
																	  double floorXWidth , double floorYLngth ){
	// NOTE: This is to un-hack 'precedence_layer_root_to_action_sequence'
	// Input: A precedence layer representing the decomposition of the root node into subs, including the balance of removal operations
	// Objective: Return a sequence of actions that results in all of the subs in the sequence layer.  In other words, successor assmeblies
	//            are removed from the root node assembly until only the final successor remains
	// Contraints: Each serial action must be feasible, and its representation should be able to be rendered easily
	// Assumptions: There are no orientations / supports determined by the previous planner
	// Output: Renderable representation of the sequence
		// Remove: 
			// * Sub (running asm) in a pose that will support removal , Recoreded in the node and the assembly , NEW OBJECT
			// * Removed (moving asm) animated through a translation in the suggested direction, without collisions at any point
		// Rotate:
			// * Note the old orientation and why it is bad
			// * Compute a new orientation and why it is good
		
	// REQ'S
		// A. A function that finds a support that allows a removal operation
	
	bool SHOWDEBUG = true; // if( SHOWDEBUG ){  cout << "" << endl;  }
	
	//  0. Create a running assembly
	Assembly_ASM*     rootAsmbly    = rootNode->assembly;
	Assembly_ASM*     runningAsm    = rootAsmbly->replicate();
	std::vector<llin> runningIDs    = runningAsm->get_part_ID_vec();
	size_t            numSuccessors = rootNode->count_edges();
	
	if( SHOWDEBUG ){  cout << "Proccessing a root node with parts: " << runningIDs << endl;  }
	
	AsmStateNode_ASP* currState = new AsmStateNode_ASP( *rootNode ); // Pointer to the current state, used to build a chain
	AsmStateNode_ASP* initState = currState; // ----------------------- This is the root / init state to return
	
	Eigen::Vector3d remDir;
	
	std::vector<bool>              coverage;
	std::vector<std::vector<llin>> subsIDvec;
	
	//  A. Determine if there are successors to process
	if( numSuccessors > 0 ){ 
		// B. Determine an appropriate orientation for the complete asm
		
		if( SHOWDEBUG ){  cout << "Computing best orientation for the initial state ..." << endl;  }
		
		size_t initSupport = suggest_support_for_straightline_access( rootNode );
		currState->set_support( initSupport );
		runningAsm->set_support( initSupport );
		runningAsm->set_lab_pose_for_index_3D( initSupport , setdownCenter );
		if( SHOWDEBUG ){
			BoolScore isAbove = runningAsm->check_Z_floor_OK( setdownCenter(2) );
			cout << "Initial pose is above the table?: " << isAbove.flag << endl;
		}
		
		coverage  = bool_false_vector( rootNode->count_edges() ); // Vector of bool flags, one for each edge
		subsIDvec = successor_membership( rootNode ); // ----------- Part IDs for every successor edge
		
		std::vector<llin> balanceIDs;
		
		if( SHOWDEBUG ){  cout << "Found successors, begin ASP ..." << endl;  }
		//  1. For each successor
		for( size_t i = 0 ; i < numSuccessors ; i++ ){
			if( SHOWDEBUG ){  cout << "Successor " << i+1 << " of " << numSuccessors << endl;  }
			//  2. Compute the balance if the successor assembly is removed from the running assembly
			//     NOTE: The running assembly should only be changed after the removal is approved and the action node is generated
			Assembly_ASM*     succssrAsm = (*rootNode)[i]->assembly;
			std::vector<llin> removedIDs = succssrAsm->get_part_ID_vec();
			/* ----------- */ balanceIDs = balance_ID_A_minus_B( runningAsm , succssrAsm );
			
			if( balanceIDs.size() == 0 ){  continue;  } // Skip this assembly if there is no balance left
			
			if( SHOWDEBUG ){  
				cout << "Test removal of parts: " << removedIDs << endl
					 << "Leaving parts: _______ " << balanceIDs << endl
					 << "Validating ..." << endl;
			}
			
			//  3. If the balance is connected
			if(  is_subgraph_connected( runningAsm , balanceIDs )  ){
				if( SHOWDEBUG ){  cout << "Subgraph is connected. Planning action(s) for this edge ..." << endl;  }
				
				//  4. Check the validity of a removal operation
				//  A. Get the suggested removal direction
				remDir = runningAsm->asm_center_of_freedom_direction( removedIDs , balanceIDs ); // Part ID
				if( SHOWDEBUG ){  cout << "Suggested removal diection: " << remDir << endl;  }
				//  B. Validate removal
				size_t runnSupport = runningAsm->get_support();
				SuccessCode result = runningAsm->validate_removal_staightline_stability( removedIDs , balanceIDs , // Validate for removal and stability 
																					     remDir , runnSupport , removeDist , numSteps );
				//  5. If the removal cannot be preformed, trigger a rotation plan
				if( !result.success ){
					if( SHOWDEBUG ){  cout << "Removal FAILED, planning a rotation action ... " << endl;  }
					//    i. Make note of the current support
					if( SHOWDEBUG ){  cout << "Current support is: ___ " << runnSupport << endl;  }
					//   ii. Make note of the current orientation
					if( SHOWDEBUG ){  cout << "Current orientation is: " << runningAsm->get_pose() << endl;  }
					//  iii. Search for the orientation (support) that allows the removal
					IndexSuccesLookup dexFound = runningAsm->suggest_support_for_staightline_stability( removedIDs , balanceIDs , 
																										removeDist , numSteps );
					if( dexFound.result ){
						if( SHOWDEBUG ){  cout << "Support found that allows removal: " << dexFound.bestDex << endl;  }
						//   iv. New Node
						currState = currState->add_edge_rotate( dexFound.bestDex );
						//    v. Clone asm
						Assembly_ASM* temp = runningAsm->replicate();
						//   vi. Set support located (assembly and node)
						runningAsm->set_support( dexFound.bestDex );
						currState ->set_support( dexFound.bestDex );
						//  vii. Set the orientation computed (assembly and node)
						Pose_ASP statePose     = runningAsm->set_lab_pose_for_index_3D( dexFound.bestDex , setdownCenter );
						currState ->staticPose = statePose;
						// viii. Set the assembly for the node
						currState->assembly = temp;
						runningAsm          = temp; // The assembly is now the rotated assembly!
						if( SHOWDEBUG ){  cout << "Rotation added!" << endl;  }
					}else{
						if( SHOWDEBUG ){  cout << "Could not locate an appropriate removal support.  No action, skipping edge ..." << endl;  }
						continue; // Skip to the top of the next loop, Do not add any action node!
					}
				}
				//  6. If don't need to rotate  ||  No longer need to rotate
				
				if( SHOWDEBUG ){  cout << "Adding a removal action ..." << endl;  }
				//  7. New node
				Pose_ASP befPose  = runningAsm->get_pose();
				size_t befSupport = runningAsm->get_support();
				//  A. Running assembly ( assembly ) in the same pose as the initial asm, but minus the removed parts
				Assembly_ASM* balanceAsm = Assembly_A_minus_B( runningAsm , succssrAsm );
				balanceAsm->set_pose( befPose ); // Assumes that the same parts support the beginning and end states
				//  B. Removed assembly in undefined pose
				Assembly_ASM* removedAsm = succssrAsm->replicate();
				removedAsm->set_pose( befPose ); // Don't care
				//  C. Create a new node with the copied asm
				currState = currState->add_edge_remove( balanceAsm , removedAsm );
				//  8. Emit a sequence of poses to animate
				std::vector<Pose_ASP> remSeqPoses = simulate_disasm_step_straight( runningAsm , balanceAsm , 
																				   befPose , befSupport , 
																				   removeDist , numSteps ,
																				   setdownCenter , floorXWidth , floorYLngth );
				currState->seqPoses = remSeqPoses;
				if( SHOWDEBUG ){  cout << "Generated a removal trajectory with " << remSeqPoses.size() << " poses" << endl;  }
				//  9. Set the running assembly to ( previous - removed )
				runningAsm = balanceAsm;
				// 10. Mark sub coverage
				coverage[i] = true;
			}else{
				if( SHOWDEBUG ){  cout << "Subgraph NOT connected, skip edge.  Balance: " << balanceIDs << endl;  }
			}
			// 11. Next
		}
		
		if( SHOWDEBUG ){  cout << "Determing subassembly coverage ... ";  }
		// 10. The balance should be one of the remaining subs
		for( size_t j = 0 ; j < subsIDvec.size() ; j++ ){
			if( vec_same_contents( balanceIDs , subsIDvec[j] ) ){  coverage[j] = true;  }
		}
		if( SHOWDEBUG ){  cout << "Complete!"  << endl << "Sub Coverage: " << coverage << endl;  }
		
		// 12. Return
		return initState;
		
	// 13. Else there are no successors and thus no relevant actions to process , Return null
	}else{ 
		if( SHOWDEBUG ){  cout << "No successors, return NULL." << endl;  }
		return nullptr;
	}
}

AsmStateSuccessCode plan_straightline_REMOVE_action( Assembly_ASM* totalBeforeAction , Assembly_ASM* removedSub , 
												     Eigen::Vector3d setdownCenter , size_t initSupport ,
												     double removeDist , size_t numSteps ,
												     double floorXWidth , double floorYLngth ){
	// Given a "before" assembly (including sub to remove), and the removed sub, Attept to construct an state that is the result of the specified
	// remove action.
	
	bool SHOWDEBUG = false , // if( SHOWDEBUG ){  cout << "" << endl;  }
	     POSEDEBUG = false ; 
	
	// Goal: Make this as modular as possible so that I can use this again
	
	AsmStateSuccessCode rtnStruct;
	SuccessCode /* - */ rtnCode;
	
	AsmStateNode_ASP* rtnNode = new AsmStateNode_ASP();
	
	Assembly_ASM* 	runningAsm = totalBeforeAction->replicate();
	Assembly_ASM*   nodeAsmbly = removedSub; // ---- Fetch the removed asm so that we can inspect its contents
	Assembly_ASM*   balanceAsm = Assembly_A_minus_B( runningAsm , nodeAsmbly );
	Eigen::Vector3d remDir;
	
	// ~~~ TROUBLESHOOT POSE TROUBLESHOOTING ~~~
	
	if( SHOWDEBUG ){  
		cout << "Are they the same assembly?: __________ " << ( totalBeforeAction == runningAsm ) << endl 
			 << "Do they have the same number of parts?: " 
				<< ( ( totalBeforeAction->get_part_ID_vec() ).size() == ( runningAsm->get_part_ID_vec() ).size() ) << endl 
			 << "Do they have the same parts?: _________ " 
				<< vec_same_contents( totalBeforeAction->get_part_ID_vec() , runningAsm->get_part_ID_vec() ) << endl 
			 << "Table-top center is at: _______________ " << setdownCenter << endl 
			 << "Asm pose before place: ________________ " << runningAsm->get_pose() << endl
			 << "Bounding Box before place: " << endl << runningAsm->lab_AABB() << endl; 
		if( POSEDEBUG ){  
			cout << "Part Poses" << endl;
			runningAsm->part_lab_pose_report();
		}
	}
	Pose_ASP tablePose = runningAsm->set_lab_pose_for_index_3D( initSupport , setdownCenter );
	
	if( SHOWDEBUG ){  
		cout << "Asm pose after  place: ________________ " << runningAsm->get_pose() << endl 
			 << "Bounding Box after place:" << endl << runningAsm->lab_AABB() << endl;
		BoolScore isAbove = runningAsm->check_Z_floor_OK( setdownCenter(2) );
		cout << "Initial pose: _________________________ " << runningAsm->get_pose() << endl;
		cout << "Initial pose is above the table?: _____ " << isAbove.flag << " , by how much?: " << isAbove.score << endl;
		if( POSEDEBUG ){  
			cout << "Part Poses" << endl;
			runningAsm->part_lab_pose_report();
		}
	}
	
	// ~~~ PLAN A REMOVE ACTION ~~~
	
	// 0. Fetch part lists
	std::vector<llin> totalParts = runningAsm->get_part_ID_vec();
	std::vector<llin> removParts = nodeAsmbly->get_part_ID_vec();
	std::vector<llin> refrnParts = balance_ID_A_minus_B( runningAsm , nodeAsmbly );
	// 1. Check that the removed sub is a subset of the original asm
	if( vec2_contains_vec1( removParts , totalParts ) ){
		// 2. Check that there is local freedom
		remDir = runningAsm->asm_center_of_freedom_direction( removParts , refrnParts );
		if( SHOWDEBUG ){  
			cout << "Suggested removal direction: " << remDir << " for ..." << endl
				 << "\tRemoved: _ " << removParts << endl
				 << "\tReference: " << refrnParts << endl;
		}
		
		if( !is_err( remDir ) ){
			// 3. If there is local freedom, Check removal validity against floor
			SuccessCode removValid = 
				runningAsm->validate_removal_staightline_stability( removParts , refrnParts , // Validate for removal and stability 
																	remDir , initSupport , removeDist , numSteps );
			// 4. Print result and info
			if( SHOWDEBUG ){  
				cout << "Removal validation result: " << removValid.success << endl 
					 << "\tCode: " << removValid.code << " meaning " << removValid.desc << endl;
			}
			
			// 5. If removal is valid, simulate a removal
			if( removValid.success ){
			
				Pose_ASP befPose = runningAsm->get_pose(); 
				//                                                                 before remove , removed asm       // TODO: NAME THESE BETTER         
				std::vector<Pose_ASP> remSeqPoses = simulate_disasm_step_straight( runningAsm    , nodeAsmbly , 
																				   befPose , initSupport , 
																				   removeDist , numSteps ,
																				   setdownCenter , floorXWidth , floorYLngth );
				if( SHOWDEBUG ){  cout << "Generated a removal trajectory with " << remSeqPoses.size() << " poses" << endl;  }
				
				// 6. Populate assembly
				rtnNode->assembly   = balanceAsm;
				rtnNode->assembly->set_pose( befPose );
				if( SHOWDEBUG ){  cout << "Setting the balance pose to: " << befPose << endl;  }
				// 7. Populate removed
				rtnNode->removed    = nodeAsmbly->replicate();
				rtnNode->removed->set_pose( remSeqPoses[ last_index( remSeqPoses ) ] );
				if( SHOWDEBUG ){  cout << "Setting the removed pose to: " << remSeqPoses[ last_index( remSeqPoses ) ] << endl;  }
				// 8. Populate planned
				rtnNode->seqPoses   = remSeqPoses;
				// X. Populate balance pose
				rtnNode->staticPose = befPose;
				
				rtnNode->set_state_rep_from_Assembly_ASM( balanceAsm ); // Read the assembly state from assembly object
				rtnNode->set_support( initSupport ); // ------------------ Set the support in both the assembly and the state representation
				
				// SUCCESS
				rtnCode.success = true;
				rtnCode.code    = 0;
				rtnCode.desc    = "SUCCESS: All checks passed";
				
			}else{
				if( SHOWDEBUG ){  cout << "Error: Simulated sub removal failed!" << endl
									   << "\tCode: " << removValid.code << " meaning " << removValid.desc << endl;  }
				// 9. Return
				rtnNode = nullptr;
				
				// FAILURE
				rtnCode.success = false;
				rtnCode.code    = 10 + removValid.code;
				rtnCode.desc    = "FAILURE: Simulated removal failed with message - " + removValid.desc;
			}
			
		}else{
			if( SHOWDEBUG ){  cout << "ERROR: There was no local freedom found for moved parts " << removParts 
								   << " past reference parts " << refrnParts << endl;  }
			// 9. Return
			rtnNode = nullptr;
			
			// FAILURE
			rtnCode.success = false;
			rtnCode.code    = 1;
			rtnCode.desc    = "FAILURE: No local freefom";
		}
	}else{
		if( SHOWDEBUG ){  cout << "ERROR: Removed assembly " << removParts << " has parts not in total assembly " << totalParts << endl;  }
		// 9. Return
		rtnNode = nullptr;
		// FAILURE
		rtnCode.success = false;
		rtnCode.code    = 2;
		rtnCode.desc    = "FAILURE: Removed is not a subset of total";
	}
	
	rtnStruct.stateNode      = rtnNode;
	rtnStruct.creationDetail = rtnCode;
	return rtnStruct;
}

AsmStateSuccessCode plan_magic_ROTATE_action( Assembly_ASM* asmToRotate , 
											  Eigen::Vector3d setdownCenter , 
											  size_t initSupport , size_t finlSupport , 
											  double floorXWidth , double floorYLngth ){
	// Rotate the assembly to a different support , This planner is MAGIC and there are no intermediate steps shown or path planning done
	
	bool SHOWDEBUG = false , // if( SHOWDEBUG ){  cout << "" << endl;  }
	     POSEDEBUG = false ; 
	
	// Goal: Make this as modular as possible so that I can use this again
	
	AsmStateSuccessCode rtnStruct;
	SuccessCode /* - */ rtnCode;
	AsmStateNode_ASP*   rtnNode = new AsmStateNode_ASP();
	Assembly_ASM*       rtnAsm  = asmToRotate->replicate();
	
	size_t numSup = asmToRotate->how_many_supports();
	
	if( initSupport < numSup ){
		if( finlSupport < numSup ){
			
			// 1. Set the pose to the support
			rtnAsm->set_lab_pose_for_index_3D( finlSupport , setdownCenter , true );
			
			// 2. Populate assembly
			rtnNode->assembly = rtnAsm;
			if( SHOWDEBUG ){  cout << "Setting the assembly pose to: " << rtnAsm->get_pose() << endl;  }
			
			// 3. Populate asm pose
			rtnNode->staticPose = rtnAsm->get_pose();
			
			// 4. Set the support
			rtnNode->set_state_rep_from_Assembly_ASM( rtnAsm ); // Read the assembly state from assembly object
			rtnNode->set_support( finlSupport ); // -------------- Set the support in both the assembly and the state representation
			
			// SUCCESS
			rtnCode.success = true;
			rtnCode.code    = 0;
			rtnCode.desc    = "SUCCESS: All checks passed";
			
		}else{
			// FAILURE
			rtnNode         = nullptr;
			rtnCode.success = false;
			rtnCode.code    = 2;
			rtnCode.desc    = "FAILURE: Final support is an invalid index";
		}
	}else{
		// FAILURE
		rtnNode         = nullptr;
		rtnCode.success = false;
		rtnCode.code    = 1;
		rtnCode.desc    = "FAILURE: Initial support is an invalid index";
	}
	
	rtnStruct.stateNode      = rtnNode;
	rtnStruct.creationDetail = rtnCode;
	return rtnStruct;
}

AsmStateSuccessCode precedence_layer_ASP_simple_straightline( AsmStateNode_ASP* rootNode , Eigen::Vector3d setdownCenter , 
															  double removeDist , size_t numSteps ,
															  double floorXWidth , double floorYLngth ){
	// Attempt to generate an assembly sequence for the given precedence layer
	// NOTE: For this version, not planning for an optimal first pose.  Opting instead for the most simple approach possible
	
	bool   SHOWDEBUG = true; // if( SHOWDEBUG ){  cout << "" << endl;  }
	size_t FLOORCODE = 11;
	
	AsmStateSuccessCode rtnStruct;
	SuccessCode /* - */ rtnCode;
	
	//  0. Create a running assembly
	Assembly_ASM*     rootAsmbly    = rootNode->assembly;
	Assembly_ASM*     runningAsm    = rootAsmbly->replicate();
	std::vector<llin> runningIDs    = runningAsm->get_part_ID_vec();
	size_t            numSuccessors = rootNode->count_edges();
	
	if( SHOWDEBUG ){  cout << "Proccessing a root node with parts: " << runningIDs << endl;  }
	
	AsmStateNode_ASP* currState = new AsmStateNode_ASP( *rootNode ); // Pointer to the current state, used to build a chain
	AsmStateNode_ASP* initState = currState; // ----------------------- This is the root / init state to return
	
	Eigen::Vector3d remDir;
	
	std::vector<bool> /* ------ */ coverage; //- Succes flags for every successor edge
	std::vector<std::vector<llin>> subsIDvec; // Vector of bool flags, one for each edge
	
	//  A. Determine if there are successors to process
	if( numSuccessors > 0 ){ 
		
		subsIDvec = successor_membership( rootNode ); // - Fetch the constituents of all successors
		coverage  = bool_false_vector( numSuccessors ); // Set up flags
		
		std::vector<llin> balanceIDs;
		
		if( SHOWDEBUG ){  cout << endl << endl;  }
		
		//  1. For each successor
		for( size_t i = 0 ; i < numSuccessors ; i++ ){
			if( SHOWDEBUG ){  cout << "Successor " << i+1 << " of " << numSuccessors << endl;  }
			
			//  2. Compute the balance if the successor assembly is removed from the running assembly
			//     NOTE: The running assembly should only be changed after the removal is approved and the action node is generated
			Assembly_ASM*     succssrAsm = (*rootNode)[i]->assembly;
			std::vector<llin> removedIDs = succssrAsm->get_part_ID_vec();
			/* ----------- */ balanceIDs = balance_ID_A_minus_B( runningAsm , succssrAsm );
			
			if( SHOWDEBUG ){  
				cout << "Test removal of parts: " << removedIDs << endl
					 << "Leaving parts: _______ " << balanceIDs << endl
					 << "Validating ..." << endl;
			}
			
			// Skip this assembly if there is no balance left
			if( balanceIDs.size() == 0 ){  
				if( SHOWDEBUG ){  cout << "There is no part balance, last action has been planned, Skip to end ..." << endl;  }
			}else{
				if( SHOWDEBUG ){  cout << "There is a remaining balance, attempt to plan removal action" << endl;  }
				
				//  3. If the balance is connected
				if(  is_subgraph_connected( runningAsm , balanceIDs )  ){
					
					if( SHOWDEBUG ){  cout << "Subgraph is connected. Planning action(s) for this edge ..." << endl;  }
					
					//  4. Attempt a removal plan
					
					size_t initSupport = runningAsm->get_support();
					if( SHOWDEBUG ){  cout << "Running assembly is on support: " << initSupport << endl;  }
					
					AsmStateSuccessCode removePlan = plan_straightline_REMOVE_action( runningAsm , succssrAsm , 
																					  setdownCenter , initSupport ,
																					  removeDist , numSteps ,
																					  floorXWidth , floorYLngth );
					if( SHOWDEBUG ){  
						cout << "Attempted removal plan , Success?: " << removePlan.creationDetail.success << endl
							 << "\tCode: " << removePlan.creationDetail.code << " meaning " << removePlan.creationDetail.desc << endl;
					}
					//  5. If the plan succeeds, Add to sequence and update running
					if( removePlan.creationDetail.success ){
						if( SHOWDEBUG ){  cout << "Removal SUCCESS! Completed successor " << i << endl;  }
						currState = currState->add_edge_remove( removePlan.stateNode );
						runningAsm = currState->assembly;
						coverage[i] = true;
					//  6. Else the removal plan fails
					}else{ 
						if( SHOWDEBUG ){  cout << "Removal FAULURE! Clearing removal plan ..." << endl;  }
						//  A. Clear the response object of heap data
						clear_ASM_node( removePlan );
						//  6. If the plan fails for reason of floor
						// Code: 11 meaning FAILURE: Simulated removal failed with message - FAILED: Translation removal -vs- floor
						if( removePlan.creationDetail.code = FLOORCODE ){	
							if( SHOWDEBUG ){  cout << "\tFailure caused by floor collision. Attempting to reorient ..." << endl;  }
							//  B. attempt to find an appropriate orientation that will support removal
							// This function will simulate collisions with floor
							IndexSuccesLookup dexFound = runningAsm->suggest_support_for_staightline_stability( removedIDs , balanceIDs , 
							/*  C. If reorientation succeeds, then */											removeDist , numSteps );
							if( dexFound.result ){
								if( SHOWDEBUG ){  cout << "Valid orientation is available! Planning rotation ..." << endl;  }
								//  D. Plan a reorientation action // 2018-06-20: Currently this action plan always succeeds
								AsmStateSuccessCode rotatePlan = plan_magic_ROTATE_action( runningAsm , 
																						   setdownCenter , 
																						   initSupport , dexFound.bestDex , 
																						   floorXWidth , floorYLngth );
								//  E. If the rotation succeeds
								if( rotatePlan.creationDetail.success ){
									if( SHOWDEBUG ){  cout << "Rotation SUCCESS! Re-attempt removal ..." << endl;  }
									//  F. Add a rotate edge
									currState  = currState->add_edge_remove( rotatePlan.stateNode );
									runningAsm = currState->assembly;
									//  G. Now plan the remove action
									removePlan = plan_straightline_REMOVE_action( runningAsm , succssrAsm , 
																				  setdownCenter , dexFound.bestDex ,
																				  removeDist , numSteps ,
																				  floorXWidth , floorYLngth );
									if( removePlan.creationDetail.success ){
										if( SHOWDEBUG ){  cout << "Removal SUCCESS! Completed successor " << i << endl;  }
										currState  = currState->add_edge_remove( removePlan.stateNode );
										runningAsm = currState->assembly;
										coverage[i] = true;
									}else{
										if( SHOWDEBUG ){  cout << "Removal FAILURE! Skip successor" << i << ". Backing up from rotation ..." << endl;  }
										//  H. If the removal action fails again, back up from the rotation we did for the sake of this edge
										AsmStateNode_ASP* badNode = currState;
										currState = currState->parent;
										delif( badNode );
									}
								// ELSE ROTATION FAILS , Try the next successor
								}else if( SHOWDEBUG ){  cout << "Rotation FAILURE! Skip successor" << i << " ..." << endl;  }
							// else there is no support that allows this operation , Try the next successor
							}else if( SHOWDEBUG ){  cout << "No support allows removal operation! Skip successor" << i << " ..." << endl;  }
						}else if( SHOWDEBUG ){  cout << "\tFailure caused by other reasons. See above. Skip successor" << i << " ..." << endl;  }
					} // Removal recovery end
				}else{  if( SHOWDEBUG ){  cout << "Subgraph NOT connected, skip edge.  Balance: " << balanceIDs << endl;  }  }
			} // Process balance end
			if( SHOWDEBUG ){  cout << endl;  }
		} // Level ASP (for loop) end 
		
		// Investigate coverage and assign success
		if( SHOWDEBUG ){  
			cout << "Determining subassembly coverage ..." << endl
				 << "There are " << count_elem_true( coverage ) << " out of " << numSuccessors << " covered." << endl;  
		}
		
		// 10. The balance should be one of the remaining subs
		for( size_t j = 0 ; j < subsIDvec.size() ; j++ ){
			if( vec_same_contents( balanceIDs , subsIDvec[j] ) ){  coverage[j] = true;  }
		}
		
		if( SHOWDEBUG ){  
			cout << "Sub Coverage: " << coverage << endl
				 << "There are " << count_elem_true( coverage ) << " out of " << numSuccessors << " covered." << endl;
		}
		
		// Assign success
		if( all_elem_true( coverage ) ){
			if( SHOWDEBUG ){  cout << "Plan satisfies all subs!" << endl;  }
			// SUCCESS
			rtnCode.success = true;
			rtnCode.code    = 0;
			rtnCode.desc    = "SUCCESS: All subs accounted for and All checks passed";
		}else{
			if( SHOWDEBUG ){  cout << "Some subs unplanned! Coverage: " << coverage << endl;  }
			// FAILURE
			rtnCode.success = false;
			rtnCode.code    = 2;
			rtnCode.desc    = "FAILURE: Some subs unplanned!";
		}
		
	// 13. Else there are no successors and thus no relevant actions to process , Return null
	}else{ 
		if( SHOWDEBUG ){  cout << "No successors, No sequence to generate." << endl;  }
		// FAILURE
		delif( initState );
		rtnCode.success = false;
		rtnCode.code    = 1;
		rtnCode.desc    = "FAILURE: Root node contained no successors";
	}
	
	rtnStruct.stateNode      = initState;
	rtnStruct.creationDetail = rtnCode;
	return rtnStruct;
}

void inspect_level_ASP( AsmStateNode_ASP* rootNode ){
	// Print information about the level ASP
	AsmStateNode_ASP* nextNode = rootNode;
	std::vector<llin> partsVec;
	size_t numEdges = 0 ,
		   explored = 0 ;
	// 1. While there is a next node
	while( nextNode ){
		
		// 2. Print how many parts are in the assembly, if it exists  &&  A. print parts
		if( nextNode->assembly != nullptr ){
			partsVec = nextNode->assembly->get_part_ID_vec();
			cout << "Found an assembly with " << partsVec.size() << " parts: " << partsVec << endl;
		}else{  cout << "There was no assembly!" << endl;  }
		
		// 3. Print how many parts are in the removed , if it exists  &&  A. print parts
		if( nextNode->removed != nullptr ){
			partsVec = nextNode->removed->get_part_ID_vec();
			cout << "Found a removed sub with " << partsVec.size() << " parts: " << partsVec << endl;
		}else{  cout << "There was none removed!" << endl;  }
		
		// 5. Print the static pose 
		cout << "The static pose is " << nextNode->staticPose << endl;
		
		// 6. Print the number of poses recorded
		cout << "Found a motion sequence of " << nextNode->seqPoses.size() << " poses" << endl;
		
		// 4. Print the number of outgoing edges
		numEdges = nextNode->count_edges();
		cout << "There are " << numEdges << " outgoing edges." << endl;
		
		// N. If there are outgoing edges, assign the next node
		if( numEdges > 0 ){
			nextNode = (*nextNode)[0];
		}else{
			nextNode = nullptr;
		}
		
		explored++;
		
		cout << endl;
	}
	cout << "Explored " << explored << " nodes , COMPLETE." << endl << endl;
}

double manip_score_Yoshikawa85( Eigen::MatrixXd J ){
	// Return the manipulability score (Yoshikawa 1985)
	Eigen::MatrixXd JT = J.transpose();
	return sqrt(  ( J * JT ).determinant()  );
}

std::vector<double> extract_arm_state_from_msg( const sensor_msgs::JointState& stateMsg ){
    // Just get the LBR4 joint angles
    std::vector<double>::const_iterator bgn = stateMsg.position.begin();
    std::vector<double>::const_iterator end = stateMsg.position.begin() + 7;
    std::vector<double> /* --------- */ rtnVec( bgn , end );
    return rtnVec;
}

double arm_traj_duration( const std::vector<sensor_msgs::JointState>& armTraj , const std::vector<double>& jointSpeedMax , double speedFactor ){
	// Estimate the amount of time (in seconds) that it will take for the arm to move through 'armTraj' given some 'speedFactor' of the 'jointSpeedMax'
	
	bool SHOWDEBUG = false; // if( SHOWDEBUG ){  cout << "" << endl;  }
	
	std::vector<double> jointSpeeds = jointSpeedMax * speedFactor; // Maximum joint speeds 
	if( SHOWDEBUG ){  cout << "Estimating with max joint speeds: " << jointSpeeds << endl;  }
	std::vector<double> diff;
	std::vector<double> duration;
	std::vector<double> currState;
	std::vector<double> lastState;
	size_t len = armTraj.size();
	double totalTime = 0.0 ,
		   maxDuratn = 0.0 ;
	for( size_t i = 1 ; i < len ; i++ ){
		if( SHOWDEBUG ){  cout << "Joint state " << i+1 << " of " << len << endl;  }
		// diff = abs( armTraj[i].position - armTraj[i-1].position );
		currState = extract_arm_state_from_msg( armTraj[i] );
		lastState = extract_arm_state_from_msg( armTraj[i-1] );
		if( SHOWDEBUG ){  cout << "\tThis state: " << currState << endl 
							   << "\tLast state: " << lastState << endl ;  }
		if(  ( !is_err( currState ) )  &&  ( !is_err( lastState ) )  ){
			diff = abs( extract_arm_state_from_msg( armTraj[i] ) - extract_arm_state_from_msg( armTraj[i-1] ) );
			if( SHOWDEBUG ){  cout << "\tAngle Difference: " << diff << endl;  }
			duration = diff / jointSpeeds;
			if( SHOWDEBUG ){  cout << "\tJoint Durations:_ " << duration << endl;  }
			maxDuratn = max( duration );
		}
		totalTime += maxDuratn; // NOTE: If there was an error in joint retrieval, the max value of the previous frame will be accumulated
		if( SHOWDEBUG ){  cout << "\tAccumulated Time: " << totalTime << endl;  }
	}
	if( SHOWDEBUG ){  cout << "Final Accumulated Time: " << totalTime << endl;  }
	return totalTime;
}

/// ___ END VALIDATE _______________________________________________________________________________________________________________________


/// === HELPER FUNCTIONS ===================================================================================================================

Eigen::MatrixXd unblocked_constraints( const Eigen::MatrixXd& allConstraints ){
	// Return the subset of constraints that do not violate other contraints
	
	bool SHOWDEBUG = false; // if( SHOWDEBUG ){  cout << "" << endl;  }
	
	Eigen::MatrixXd rtnDirs;
	size_t /* -- */ numConstraints = allConstraints.rows();
	Eigen::MatrixXd constraint_minus_i;
	
	if( SHOWDEBUG ){ sep( "unblocked_constraints" , 2 );
					 cout << "Got " << numConstraints << " constraints." << endl;  }
	Eigen::Vector3d direction;
	for( size_t i = 0 ; i < numConstraints ; i++ ){
		direction = allConstraints.row(i);
		if( SHOWDEBUG ){  cout << "Evaluating constraint " << direction << " ..." << endl;  }
		constraint_minus_i = copy_V_minus_row( allConstraints , i );
		if( SHOWDEBUG ){  cout << "Redacted contraints: " << endl << constraint_minus_i << endl;  }
		if( test_against_constraint_norms( constraint_minus_i , direction ) ){  
			if( SHOWDEBUG ){  cout << "Direction passed! Add!" << endl;  }
			rtnDirs = copy_V_plus_row( rtnDirs , direction );  
		}else if( SHOWDEBUG ){  cout << "Direction failed! Do not add." << endl;  }
	}
	return rtnDirs;
}

/// ___ END HELPER _________________________________________________________________________________________________________________________

/* === Spare Parts =========================================================================================================================



   ___ End Parts ___________________________________________________________________________________________________________________________

*/

