/*
Cpp_test.cpp
James Watson , 2018 January
Test C++ syntax and concepts instead of dumping a lot of time into something that won't work

Dependencies: Cpp_Helpers , 
Template Version: 2017-09-23
*/

// === Init ================================================================================================================================

// ~~~ Includes ~~~
// ~~ Standard ( Common includes not already in Cpp_Helpers.h ) ~~ 
//#include <iomanip> // - Output precision and printing
//#include <exception> // error handling
//#include <stdexcept> // library provided error types like 'std::range_error'
//#include <tuple> // --- standard tuple datatype
//#include <cstddef> // - alias types names:  size_t, ptrdiff_t
//#include <cstring> // - C Strings: strcomp 
//#include <algorithm> // sort, search, min, max, sequence operations

// ~~ Local ~~ 
#include <Cpp_Helpers.h> // Shortcuts and Aliases , and Functions of general use for C++ programming
#include <ROS_Helpers.h> // - ROS Utilities and Shortcuts
#include <MathGeo_ASP.h>

// == class Foo ==

class Foo{
public:
	Foo( int& pInt );
	int myVeryOwnInt;
};

Foo::Foo( int& pInt ){ myVeryOwnInt = pInt; }

// __ End Foo __

Foo foo_with_four(){
	int pInt = 4; // Create an int and set it to 4
	return Foo( pInt ); // Note that the param is passed by reference
}

int main( int argc , char** argv ){
	
	cout << "Creating a Foo with a member 4" << endl;
	Foo FourFoo = foo_with_four();
	cout << "Foo has member " << FourFoo.myVeryOwnInt << ", depspite a reference being passed to constructor." << endl;
	
	cout << "Count in binary to 7" << endl;
	print_vec_vec( enumerate_in_base( 3 , 2 ) );
	
	cout << endl << "AABB Overlap Test" << endl;
	Eigen::MatrixXd bbox1 = Eigen::MatrixXd::Zero( 2 , 3 );
	Eigen::MatrixXd bbox2 = Eigen::MatrixXd::Zero( 2 , 3 );
	
	bbox1.row(0) = Eigen::Vector3d(  0.0 ,  0.0 ,  0.0 );
	bbox1.row(1) = Eigen::Vector3d(  1.0 ,  1.0 ,  1.0 );
	
	bbox2.row(0) = Eigen::Vector3d( -0.5 ,  0.5 ,  0.5 );
	bbox2.row(1) = Eigen::Vector3d(  0.5 ,  1.5 ,  1.5 );
	
	cout << "bbox1:" << endl << bbox1 << endl 
	     << "bbox2:" << endl << bbox2 << endl
	     << "Intersection: " << yesno( do_AABBs_intersect( bbox1 , bbox2 ) ) << endl;
	     
	bbox2.row(0) = Eigen::Vector3d( -0.5 , -0.5 , -0.5 );
	bbox2.row(1) = Eigen::Vector3d(  1.5 ,  1.5 ,  1.5 );
	
	cout << "bbox1:" << endl << bbox1 << endl 
	     << "bbox2:" << endl << bbox2 << endl
	     << "Intersection: " << yesno( do_AABBs_intersect( bbox1 , bbox2 ) ) << endl;
	     
	bbox2.row(0) = Eigen::Vector3d(  2.5 ,  2.5 ,  2.5 );
	bbox2.row(1) = Eigen::Vector3d(  3.5 ,  3.5 ,  3.5 );
	
	cout << "bbox1:" << endl << bbox1 << endl 
	     << "bbox2:" << endl << bbox2 << endl
	     << "Intersection: " << yesno( do_AABBs_intersect( bbox1 , bbox2 ) ) << endl;
	
	return 0;
}
