/*
DIG_shell.cpp
James Watson , 2018 June

% ROS Node %
Display meshes for expanding shells
Publises To ----> :
Subscribes To <-- :

Dependencies: ROS , Cpp_Helpers , ROS_Helpers
Template Version: 2018-06-06
*/

// === Init ================================================================================================================================

// ~~~ Includes ~~~
// ~~ ROS ~~
#include <ros/ros.h> // -------------- ROS , Publishers , Subscribers
#include <ros/package.h> // ------------------ Where are we? // http://wiki.ros.org/Packages#C.2B-.2B-
// ~ ROS Messages ~
#include "motion_cost/qFromPose.h"
#include "motion_cost/IKrequest.h"
#include "motion_cost/IKresponse.h"
#include "geometry_msgs/Pose.h"
#include "sensor_msgs/JointState.h"
#include <ll4ma_teleop/Joints2Jac.h>
#include <ll4ma_teleop/LinkPoses.h>
#include <ll4ma_teleop/FngrPoses.h>
// ~~ Local ~~
#include <Cpp_Helpers.h> // - C++ Utilities and Shortcuts
#include <ROS_Helpers.h> // - ROS Utilities and Shortcuts
#include <Model_Factory.h> // Assemblies to plan on
#include <AsmSeqPlan.h> // -- Identify grasp pairs , ASP
#include <comm_ASM.h> // ---- Messages and visualization
#include <Motion_Planning.h>

// ___ End Init ____________________________________________________________________________________________________________________________


/// ######### Node Vars #########

string /* -- */ NODE_NAME  = "DIG_Shell_Test";
int /* ----- */ RATE_HZ    = 30;
int /* ----- */ QUEUE_LEN  = 30;
Eigen::Vector3d ROBOT_BASE = Eigen::Vector3d( 0.55 , 0 , 1.20 ); // FUTURE: NEED A WAY TO LOAD THIS FROM A PARAM SERVER

/// ********* End Vars *********


// === Program Functions & Classes ===



// ___ End Functions & Classes ___


// === Program Vars ===

bool CONNECT_TO_ARM = true  ;

// ___ End Vars ___


// === main ================================================================================================================================

int main( int argc , char** argv ){ // Main takes the terminal command and flags that called it as arguments
	srand( time( 0 ) ); // Random seed based on the current clock time
	
	// 0. Init ROS  &&  Register node
	ros::init( argc , argv , NODE_NAME );
	
	// 1. Fetch handle to this node
	ros::NodeHandle nodeHandle;
	
	// 2. Init node rate
	ros::Rate heartbeat( RATE_HZ );
	
	// 3. Set up subscribers and publishers
	
	// ~ Publishers ~
	//~ ros::Publisher joint_cmd_pub = nodeHandle.advertise<sensor_msgs::JointState>( "/lbr4_teleop/joint_cmd" , QUEUE_LEN );
	//~ send_joint_command jntCmd_Call{ joint_cmd_pub };
	
	ros::Publisher vis_arr       = nodeHandle.advertise<visualization_msgs::MarkerArray>( "visualization_marker_array" , QUEUE_LEN );
	
	// ~ Subscribers ~
	// ros::Subscriber SUBSCRIBER_OBJ = nodeHandle.subscribe( "TOPIC_NAME" , QUEUE_LEN , CALLBACK_FUNCTION );
	
	// ~ Service Servers ~
	// ros::ServiceServer SERVER_OBJ = nodeHandle.advertiseService( "SERVICE_NAME" , SERV_CALLBACK );
	
	// ~ Service Clients ~
	
	//~ ros::ServiceClient  IK_Client = nodeHandle.serviceClient<motion_cost::qFromPose>( "q_from_pose" );
	//~ request_IK_solution IK_clientCall{ IK_Client };
	
	//~ ros::ServiceClient  J_Client = nodeHandle.serviceClient<ll4ma_teleop::Joints2Jac>( "/robot_commander/get_Jacobian" );
	//~ request_Jacobian    J_ClientCall{ J_Client };
	
	//~ ros::ServiceClient  FK_Client = nodeHandle.serviceClient<ll4ma_teleop::LinkPoses>( "/robot_commander/get_links_FK" );
	//~ request_FK_poses    FK_ClientCall{ FK_Client };  // { 1 , 2 , 3 , 4 , 5 , 6 , 7 , Grasp Target }

	//~ ros::ServiceClient  FNG_Client = nodeHandle.serviceClient<ll4ma_teleop::FngrPoses>( "/robot_commander/get_fingr_FK" );
	//~ request_FNG_poses   FNG_ClientCall{ FNG_Client };
	
	
	// N-1. Animation Init 
	srand( ( time( 0 ) % 1 ) * 1000.0 + getpid() ); // Random seed based on the current clock time
	RViz_MarkerManager mrkrMngr{};
	visualization_msgs::MarkerArray& markerArr = mrkrMngr.get_arr();
	
	
	/// === Preliminary { Setup , Instantiation , Planning  } ==============================================================================

	/// === SHELL TESTS ====================================================================================================================

	if( 0 ){

		cout << "About to build sphere ..." << endl;

		// ~~ Sphere ~~
		
		// 1. Instantiate a sphere
		double sphrRad = 1.0;
		Eigen::Vector3d sphrCntr{ 0,0,0 };
		Sphere_d sphere{ sphrRad , sphrCntr };
		// 2. Compute a 1/8 shell
		Eigen::MatrixXd constraints_0p125 = Eigen::MatrixXd::Zero(3,3);
		constraints_0p125 << 1,0,0,
							 0,1,0,
							 0,0,1;
		cout << "About to build shell ..." << endl;
		TriMeshVFN_ASP sphrSml = sphere.get_surface_w_direction_constraints( constraints_0p125 );
		
		cout << "About to expand shell ..." << endl;
		// 3. Get a shell expanded from the center
		TriMeshVFN_ASP sphrBig = expand_VFN_from_center( sphrSml , 2.0 );
		
		cout << "About to load markers ..." << endl;
		// 4. Load shells into marker manager
		RViz_MeshMarker mrkr{ sphrSml };
		mrkr.rand_color();
		mrkrMngr.add_marker( mrkr );
		mrkr = RViz_MeshMarker( sphrBig );
		mrkr.rand_color();
		mrkrMngr.add_marker( mrkr );
		
		
		// ~~ Ring ~~
		
		Eigen::Vector3d ringCntr{ -2 , -2 ,  2 };
		Eigen::Vector3d ringAxis{  0 ,  0 ,  1 };
		double ringRad = 1.0 , 
			   ringHgt = 1.0 ;
		Eigen::MatrixXd constraints_0p25 = Eigen::MatrixXd::Zero(3,3);
		constraints_0p25 << 1,0,0,
							0,1,0;
		TriMeshVFN_ASP ringSml = get_mesh_w_direction_constraints( revolved_segment( ringCntr , ringAxis , ringRad , ringHgt ) , constraints_0p25 );
		cout << "Small ring has " << ringSml.V.rows() << " vertices." << endl;
		cout << "Small ring has " << ringSml.F.rows() << " faces." << endl;
		TriMeshVFN_ASP ringBig = get_mesh_w_direction_constraints( expand_VFN_from_axis( ringSml , 2 ) , constraints_0p25 );
		cout << "Large ring has " << ringBig.V.rows() << " vertices." << endl;
		cout << "Large ring has " << ringBig.F.rows() << " faces." << endl;
		
		mrkr = RViz_MeshMarker( ringSml );
		mrkr.rand_color();
		mrkrMngr.add_marker( mrkr );
		mrkr = RViz_MeshMarker( ringBig );
		mrkr.rand_color();
		mrkrMngr.add_marker( mrkr );
		
		// ~~ Shadow ~~
		
		// 1. Instantiate part
		Eigen::Vector3d partPos{ 2 , 2 , 0 };
		Incrementer assign_part_ID{ 0 }; // Instantiate a functor that will assign a unique number to each part
		string pkgPath      = ros::package::getPath( "motion_cost" );
		string resourcePath = pkgPath + "/Resources/Assembly_CAD/SimpleCube/";
		string padPath      = resourcePath + "PocketPlateObst_04.stl";
		Part_ASM padPrt{ assign_part_ID() , padPath };
		padPrt.set_pose( Pose_ASP{ partPos , no_turn_quat() } );
		// A. Paint part
		padPrt.rand_part_mrkr_color();
		padPrt.update_part_mrkr_pose();
		padPrt.load_part_mrkrs_into_mngr( mrkrMngr );
		// 2. Get mesh  &&  3. Get shadow
		TriMeshVFN_ASP shadow = mesh_shadow_along_Z( padPrt.get_lab_VFN() , 0.004 ,
													 partPos - Eigen::Vector3d(  0.000 , -0.125 ,  0.000 ) ,
													 Eigen::Vector3d( 1,0,0 ) , Eigen::Vector3d( 0,0,-1 ) , Eigen::Vector3d( 0,1,0 ) );
		// A. Paint shadow
		mrkr = RViz_MeshMarker( shadow );
		mrkr.rand_color();
		mrkrMngr.add_marker( mrkr );
		
		mrkrMngr.diagnose_markers_OK();
	}
	
	if( 0 ){ // ~ Shadow & Planar ~
		
		// 1. Set up shells
		
		std::vector<llin> AandAB = { 9 , 2 }; // { Module A , Bridge AB } // Planar & Constrained Planar
		std::vector<llin> AandA1 = { 9 , 4 }; // { Module A , Chip   A1 } // Prismatic Unidir and Prismatic Bidir
		Eigen::Vector3d setDown{ 0,0,0 };
		
		//~ Assembly_ASM* smlSub = simple_phone( AandA1 );  // Prismatic Unidir and Prismatic Bidir
		Assembly_ASM* smlSub = simple_phone( AandAB ); // Planar & Constrained Planar
		smlSub->set_lab_pose_for_index_3D( 0 , setDown );
		smlSub->shift_part( 2 , Eigen::Vector3d( 0.000 , -0.001 , 0.001 ) ); // Shift out for unconstrained planar
		//~ smlSub->shift_part( 4 , Eigen::Vector3d( 0.000 , 0.000 , 0.001 ) ); // Shift up for bidir
		smlSub->load_part_mrkrs_into_mngr( mrkrMngr );
		
		//~ std::vector<TriMeshVFN_ASP> constrainedArcs = get_intersection_surfaces( smlSub , 4 , // Prismatic
		std::vector<TriMeshVFN_ASP> constrainedArcs = get_intersection_surfaces( smlSub , 2 , // Planar
																				 0.005 , 10 );
		size_t numShells = constrainedArcs.size();
		cout << "Got " << numShells << " shells" << endl;
		for( size_t i = 0 ; i < numShells ; i++ ){
			RViz_MeshMarker mrkr{ constrainedArcs[i] };
			mrkr.rand_color();
			mrkrMngr.add_marker( mrkr );
		}
		
		// 2. Set up obstruction
		Incrementer assign_part_ID{ 0 }; // Instantiate a functor that will assign a unique number to each part
		string pkgPath = ros::package::getPath( "motion_cost" );
		string resourcePath = pkgPath + "/Resources/Assembly_CAD/SimpleCube/";
		// ~ Create full paths to STLs ~
		std::vector<string> fNames;
		fNames.push_back( resourcePath + "CornerPlate_04.stl" );  Eigen::Vector3d pocket_shift{ -0.0542857 , -0.0542857 , -0.00857143 };
		size_t fLen = fNames.size();
		Part_ASM* floater = new Part_ASM( assign_part_ID() , fNames[0] );
		
		//~ floater->set_pose( Pose_ASP{ Eigen::Vector3d( 0.06 , 0.06 , 0.03 ) , no_turn_quat() } ); // Prismatic
		floater->set_pose( Pose_ASP{ Eigen::Vector3d( 0.00 , 0.00 , 0.06 ) , no_turn_quat() } ); // Planar
		
		
		cout << "Set part pose to " << floater->get_pose() << endl;
		floater->rand_part_mrkr_color();
		floater->load_part_mrkrs_into_mngr( mrkrMngr );
		
		// 3. Transform shells into collision targets
		std::vector<CollisionVFN_ASP*> collsnShells = prep_meshes_for_collision( constrainedArcs );
		cout << "Preparing to evaluate " << collsnShells.size() << " shells" << endl;
		
		// 4. Check collisions
		CollisionVFN_ASP* currShell;
		for( size_t i = 0 ; i < collsnShells.size() ; i++ ){
			currShell = collsnShells[i];
			cout << "Fraction of shell " << i+1 << " in collision: \t" << collision_fraction( currShell , floater ) << endl;
		}
	}
	
	if( 0 ){ // ~ Spherical Pyramid ~
	
		Incrementer assign_part_ID{ 0 }; // Instantiate a functor that will assign a unique number to each part
		Incrementer assign_asm_ID{ 0 }; //- Instantiate a functor that will assign a unique number to each assembly
		
		Assembly_ASM* simpleCube = new Assembly_ASM( assign_asm_ID() );
		
		string pkgPath = ros::package::getPath( "motion_cost" );
		string resourcePath = pkgPath + "/Resources/Assembly_CAD/SimpleCube/";
		// ~ Create full paths to STLs ~
		std::vector<string> fNames;
		
		fNames.push_back( resourcePath + "CornerPlate_04.stl" );  Eigen::Vector3d pocket_shift{ -0.0542857 , -0.0542857 , -0.00857143 };
		fNames.push_back( resourcePath + "MeterCube_04.stl"   );  Eigen::Vector3d cube_shift{   -0.02 , -0.02  , -0.02  };
		
		// ~~ 1. Create parts ~~
		size_t fLen = fNames.size();
		std::vector<Part_ASM> cubeParts;
		for( size_t i = 0 ; i < fLen ; i++ ){
			cout << fNames[i] << endl;
			cubeParts.push_back( Part_ASM{   assign_part_ID() , fNames[i]   } );
		}
		
		double sideLen = 0.04; // Side length is 4cm (1.6in)
		
		simpleCube->add_part_w_pose( cubeParts[ 0] , Pose_ASP{ -pocket_shift   , // pocket
															   no_turn_quat()  } );
		simpleCube->add_part_w_pose( cubeParts[ 1] , Pose_ASP{ -cube_shift + Eigen::Vector3d( sideLen , sideLen , sideLen * 0.25 ) , // Cube
															   no_turn_quat()                                                      } );
															   
		// 3. Determine the local freedom of each part (NDBG)
		simpleCube->recalc_geo();
		
		Eigen::Vector3d setDown{ 0,0,0 };
		simpleCube->set_lab_pose_for_index_3D( 0 , setDown );
		
		cout << "There are 2 parts with poses" << endl
			 << simpleCube->get_lab_pose_w_ID( 0 ) << endl
			 << simpleCube->get_lab_pose_w_ID( 1 ) << endl;
		
		
		
		//~ smlSub->shift_part( 2 , Eigen::Vector3d( 0.000 , -0.001 , 0.001 ) );
		//~ smlSub->shift_part( 4 , Eigen::Vector3d( 0.000 , 0.000 , 0.001 ) );
		simpleCube->load_part_mrkrs_into_mngr( mrkrMngr );
			
		std::vector<TriMeshVFN_ASP> constrainedArcs = get_intersection_surfaces( simpleCube , 1 , 
																				 0.005 , 10 );
		size_t numShells = constrainedArcs.size();
		cout << "Got " << numShells << " shells" << endl;
		for( size_t i = 0 ; i < numShells ; i++ ){
			RViz_MeshMarker mrkr{ constrainedArcs[i] };
			mrkr.rand_color();
			mrkrMngr.add_marker( mrkr );
		}
		
		Part_ASM* floater = new Part_ASM( assign_part_ID() , fNames[0] );
		Pose_ASP partPose{ simpleCube->get_pose().position + Eigen::Vector3d( 0.10 , 0.10 , 0.08 ) , no_turn_quat() };
		floater->set_pose( partPose );
		cout << "Set part pose to " << floater->get_pose() << endl;
		floater->rand_part_mrkr_color();
		floater->load_part_mrkrs_into_mngr( mrkrMngr );
		
		std::vector<CollisionVFN_ASP*> collsnShells = prep_meshes_for_collision( constrainedArcs );
		cout << "Preparing to evaluate " << collsnShells.size() << " shells" << endl;
		
		CollisionVFN_ASP* currShell;
		for( size_t i = 0 ; i < collsnShells.size() ; i++ ){
			currShell = collsnShells[i];
			cout << "Fraction of shell " << i+1 << " in collision: \t" << collision_fraction( currShell , floater ) << endl;
		}
	}
	
	/// ___ END SHELL ______________________________________________________________________________________________________________________
	
	
	
	/// === DIG TEST =======================================================================================================================
	
	cout << "Instantiate Simple Phone ... " << endl;
	
	Assembly_ASM*   simplePhone = simple_phone();
	simplePhone->set_pose( origin_pose() );
	Eigen::MatrixXd bb /* ----------- */ = simplePhone->calc_AABB();
	Eigen::Vector3d crnr1 /* -------- */ = bb.row(0);
	Eigen::Vector3d crnr2 /* -------- */ = bb.row(1);
	double /* -- */ width /* -------- */ = ( crnr2 - crnr1 ).norm() ,
					interval /* ----- */ = 0.005 ,
					stuckInfluenceFactor = 1.5 ;
	size_t /* -- */ numSteps /* ----- */ = (size_t) ceil( width/interval );
	
	
	
	// === DIG V1 , TROUBLESHOOTING ========================================================================================================
	
	if( 0 ){
		cout << "Running DIG V1 with interval " << interval << " and " << numSteps << " steps for a search radius of " << interval * numSteps << endl;
		std::vector<std::vector<double>> dig1 = DIG_from_asm_V1( simplePhone , interval , numSteps );
		cout << "Disassembly Interference Graph" << endl;
		print_vec_vec( dig1 );
	}else if( 0 ){
		// ** RECOVER **
		
		// ISSUE 1: ANTENNA IS BLOCKED , IT SHOULD BE ABLE TO BE REMOVED FROM THE TOP
		// RESOLUTION: Part indices were other than expected, and the antenna was placed such that no constraints were registered for it
		
		
		
		// 1. Just try to query the removal direction
		bool SHOWDEBUG = true;
		size_t mover = 7;
		std::vector<size_t> /* ------ */ movd; // ------------------ Indices of the moved parts
		std::vector<size_t> /* ------ */ refc; // ------------------ Indices of the reference parts
		Eigen::Vector3d /* ---------- */ remDir; // ---------------- Suggested removal direction
		std::vector<size_t> indices  = vec_range( (size_t)0 , simplePhone->how_many_parts()-1 );
		movd.clear();  movd.push_back( mover );           if( SHOWDEBUG ){  cout << "Moved: ___ " << movd << endl;  }
		refc = vec_copy_without_elem( indices , mover );  if( SHOWDEBUG ){  cout << "Reference: " << refc << endl;  }
		remDir = simplePhone->asm_center_of_freedom_direction( movd , refc ); // NDBG order
		if( SHOWDEBUG ){  cout << "Suggested Direction: " << remDir << endl;  }
		
	}
	
	
	
	// ___ END DIG V1 ______________________________________________________________________________________________________________________
	
	
	// === DIG V2 , TROUBLESHOOTING ========================================================================================================
	
	if( 0 ){
		cout << "Running DIG V2 with interval " << interval << " and " << numSteps << " steps for a search radius of " << interval * numSteps << endl;
		std::vector<std::vector<double>> dig2 = DIG_from_asm_V2( simplePhone , interval , numSteps , 2 );
		cout << "Disassembly Interference Graph" << endl;
		print_vec_vec( dig2 );
	}
	
	// ___ END DIG V2 ______________________________________________________________________________________________________________________
	
															 
	// === DIG V3 , TROUBLESHOOTING ========================================================================================================
	
	if( 1 ){
		std::vector<llin> baseOnly = { 0 };
		cout << "Running DIG V3 with interval " << interval << " and " << numSteps << " steps for a search radius of " << interval * numSteps << endl
			 << "with contraints from only the following IDs" << baseOnly << endl;
		
		
		std::vector<std::vector<double>> DIG3 = DIG_from_asm_V3( simplePhone , interval , numSteps ,
																 baseOnly ,
																 stuckInfluenceFactor );
		cout << "Disassembly Interference Graph V3" << endl;
		print_vec_vec( DIG3 );
	}
	
	// ___ END DIG V3 ______________________________________________________________________________________________________________________
	
	// === METHOD COMPONENTS TEST ==========================================================================================================
	
	if( 0 ){
		
		cout << "Get parts lists ... " << endl;
		
		//~ std::vector<llin> projectPartID = { 5  }; // Chip A1
		//~ std::vector<llin> projectPartID = { 2  }; // Bridge AB
		//~ std::vector<llin> projectPartID = { 9  }; // Module A
		//~ std::vector<llin> projectPartID = { 4  }; // Chip A1 
		std::vector<llin> projectPartID = { 5 , 2 , 9 , 4 };
		std::vector<llin> refcID        = vec_copy_without_elem( simplePhone->get_part_ID_vec() , projectPartID );
		
		cout << "Calculate shells ... " << endl;
		
		std::vector<TriMeshVFN_ASP> shells = get_intersection_surfaces( simplePhone , 
																	    projectPartID , refcID , 
																		0.005 , 10 );
					
		TriMeshVFN_ASP testMesh = simplePhone->get_lab_VFN( projectPartID );
		
		testMesh.axis = Eigen::Vector3d::UnitZ();
		
		testMesh = shift_VFN_along_axis( testMesh , 0.25 );
		RViz_MeshMarker mrkr{ testMesh };
		mrkr.rand_color();
		mrkrMngr.add_marker( mrkr );
																		
		simplePhone->load_part_mrkrs_into_mngr( mrkrMngr );
												
		cout << "Render shells ... " << endl;
																		
		size_t numShells = shells.size();
		cout << "Got " << numShells << " shells" << endl;
		for( size_t i = 0 ; i < numShells ; i++ ){
			
			cout << "Shell Center: " << shells[i].center          << endl
				 << "Shell Axis: _ " << shells[i].axis            << endl
				 << "Shell AABB:"    << endl << AABB( shells[i] ) << endl;
			
			mrkr = RViz_MeshMarker{ shells[i] };
			mrkr.rand_color();
			mrkrMngr.add_marker( mrkr );
		}
	}
	
	// ___ END COMPONENTS __________________________________________________________________________________________________________________
	
	
	// === SUB-ID TEST =====================================================================================================================
	
	if( 0 ){
		
		StopWatch algoTimer;
		
		// 1. Calc DIG
		cout << "Running DIG V2 with interval " << interval << " and " << numSteps << " steps for a search radius of " << interval * numSteps << endl;
		std::vector<std::vector<double>> dig2 = DIG_from_asm_V2( simplePhone , interval , numSteps , 2 );
		
		cout << "Computed DIG in " << algoTimer.seconds_elapsed() << " seconds." << endl;
		algoTimer.mark();
		
		//~ std::vector<std::vector<llin>> level1subs = asm_NRG_reduce_one_level( simplePhone , 
																			  //~ dig2 ,
																			  //~ width , interval , 2.0 );
																			  
		cout << "Level 1 subs in " << algoTimer.seconds_elapsed() << " seconds." << endl << endl 
			 << "Subassemblies: " << endl;
																			  
		//~ print_vec_vec( level1subs );
		
	}
	
	// ___ END SUB-ID ______________________________________________________________________________________________________________________
	
	/// ___ END DIG ________________________________________________________________________________________________________________________
	
		
	/// ___ End Preliminary ________________________________________________________________________________________________________________
	
	// N-2. Notify
	ros_log( "[" + NODE_NAME + "] Init OK and about to run ..." , INFO );
	
	// N. Main loop
	while( ros::ok() ){ // While neither node nor ROS has been shut down
		
		/// == NODE WORK ===================================================================================================================
		
		
		
		// 5. Paint frame
		vis_arr.publish( markerArr );
		
		
		//~ break; // ONLY ONCE
		
		/// __ END WORK ____________________________________________________________________________________________________________________
		
		ros::spinOnce(); // - Process messages
		heartbeat.sleep(); // Sleep for remainder of period
	}
	
	// N+1. Notify  &&  Exit
	
	ros_log( "[" + NODE_NAME + "] Exit OK, Goodbye!" , INFO );
	
	return 0; // I guess everything turned out alright at the end!
}

// ___ End main ____________________________________________________________________________________________________________________________


/* === Spare Parts =========================================================================================================================



   ___ End Spare ___________________________________________________________________________________________________________________________
*/
