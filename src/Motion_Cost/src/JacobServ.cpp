/*

#######################################################################################################
#                                                                                                     #
#  2018-06-21: At this time, trying to see if I can just add the needed service to 'joint_commander'  #
#                                                                                                     #
#######################################################################################################

*/



/*
JacobServ.cpp
James Watson , 2018 June

% ROS Node %
Service: Return the manipulator jacobian for the given joint angles
Publises To ----> :
Subscribes To <-- :

Dependencies: ROS , Cpp_Helpers , ROS_Helpers
Template Version: 2018-06-06
*/

// === Init ================================================================================================================================

// ~~~ Includes ~~~
// ~~ ROS ~~
#include <ros/ros.h> // --- ROS , Publishers , Subscribers
#include <ros/package.h> // Where are we? // http://wiki.ros.org/Packages#C.2B-.2B-
// ~ ROS Messages ~
// #include "PACKAGE_NAME/MSG_TYPE.h"
// #include "PACKAGE_NAME/SERVICE_TYPE.h"

// ~~ KDL ~~
#include <kdl_parser/kdl_parser.hpp>
//~ #include <kdl/jntarrayvel.hpp>
//~ #include <kdl/chainfksolver.hpp>
#include <kdl/chainjnttojacsolver.hpp>

// ~~ Boost ~~
#include <boost/scoped_ptr.hpp>

// ~~ Local ~~
#include <Cpp_Helpers.h> // C++ Utilities and Shortcuts
#include <ROS_Helpers.h> // ROS Utilities and Shortcuts

// ___ End Init ____________________________________________________________________________________________________________________________


/// ######### Node Vars #########

string NODE_NAME = "NODE_NAME";
int    RATE_HZ   = 100;
int    QUEUE_LEN = 100;

/// ********* End Vars *********


// === Program Functions & Classes ===

class ManipJacobService{
	
	
private:
	
	// ~~ std ~~
	int num_jnts_;
	std::string robot_name_ , root_link_ , tip_link_ , robot_description_;
	std::string jnt_cmd_topic_ , pose_cmd_topic_ , robot_state_topic_ , remote_robot_state_topic_;
	std::vector<std::string> jnt_names_ ;
	std::vector<double> jnt_upper_lims_ , jnt_lower_lims_ , nominal_jnts_;
	std::mutex mutex_;
	
	// ~~ KDL ~~
	KDL::Tree robot_tree_;
	KDL::Chain kdl_chain_;
	KDL::Jacobian J_kdl_;
	KDL::JntArrayVel q_qdot_;
	KDL::JntArray prev_q_;
	KDL::FrameVel robot_x_xdot_;
	KDL::Frame x_, prev_x_, x_des_, x_des_raw_;
	KDL::Twist x_diff_kdl_;
	//~ boost::scoped_ptr<KDL::ChainFkSolverPos> pos_fk_solver_;
	//~ boost::scoped_ptr<KDL::ChainFkSolverVel> vel_fk_solver_;
	boost::scoped_ptr<KDL::ChainJntToJacSolver> jac_solver_;
	
};

// ___ End Functions & Classes ___


// === Program Vars ===


// ___ End Vars ___


/// === Preliminary { Setup , Instantiation , Planning  } ==================================================================================



/// ___ End Preliminary ____________________________________________________________________________________________________________________


// === main ================================================================================================================================

int main( int argc , char** argv ){ // Main takes the terminal command and flags that called it as arguments
	srand( time( 0 ) ); // Random seed based on the current clock time
	
	/// === Preliminary { Setup , Instantiation , Planning  } ==============================================================================



	/// ___ End Preliminary ________________________________________________________________________________________________________________
	
	// 0. Init ROS  &&  Register node
	ros::init( argc , argv , NODE_NAME );
	
	// 1. Fetch handle to this node
	ros::NodeHandle nodeHandle;
	
	// 2. Init node rate
	ros::Rate heartbeat( RATE_HZ );
	
	// 3. Set up subscribers and publishers
	
	// ~ Publishers ~
	// ros::Publisher PUBLISHER_OBJ = nodeHandle.advertise<CATEGORY_MSGS::MSG_TYPE>( "TOPIC_NAME" , QUEUE_LEN );
	
	// ~ Subscribers ~
	// ros::Subscriber SUBSCRIBER_OBJ = nodeHandle.subscribe( "TOPIC_NAME" , QUEUE_LEN , CALLBACK_FUNCTION );
	
	// ~ Service Servers ~
	// ros::ServiceServer SERVER_OBJ = nodeHandle.advertiseService( "SERVICE_NAME" , SERV_CALLBACK );
	
	// ~ Service Clients ~
	// ros::ServiceClient CLIENT_OBJ = nodeHandle.serviceClient<CATEGORY::SERVICE_TYPE>( "SERVICE_NAME" );
	
	// N-1. Notify
	ros_log( "[" + NODE_NAME + "] Init OK and about to run ..." , INFO );
	
	// N. Main loop
	while( ros::ok() ){ // While neither node nor ROS has been shut down
		
		/// == NODE WORK ===================================================================================================================
		
		
		
		/// __ END WORK ____________________________________________________________________________________________________________________
		
		ros::spinOnce(); // - Process messages
		heartbeat.sleep(); // Sleep for remainder of period
	}
	
	// N+1. Notify  &&  Exit
	
	ros_log( "[" + NODE_NAME + "] Exit OK, Goodbye!" , INFO );
	
	return 0; // I guess everything turned out alright at the end!
}

// ___ End main ____________________________________________________________________________________________________________________________


/* === Spare Parts =========================================================================================================================



   ___ End Spare ___________________________________________________________________________________________________________________________
*/
