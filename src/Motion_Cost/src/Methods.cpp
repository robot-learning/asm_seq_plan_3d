/***********  
Methods.cpp
James Watson , 2018 July
Sequencing and Motion Planning, Together Forever

Template Version: 2018-06-07
***********/

#include "Methods.h"

// === Classes and Structs =================================================================================================================

SuccessPath empty_SuccessPath(){
	SuccessPath result;
	result.code /*-*/ = ERROR_INCOMPLETE;
	result.success    = false;
	result.desc /*-*/ = "";
	result.travelDist = 0.0;
	result.bestDir    = err_vec3();
	result.code_cached = ERROR_INCOMPLETE;
	result.desc_cached = "";
	return result;
}

SuccessPath magic_success_rotate(){
	SuccessPath result;
	result.code /*-*/ = ROTATED_OK_MAGIC;
	result.success    = true;
	result.desc /*-*/ = "ROTATED_OK_MAGIC: Rotate action was assigned an automatic PASS";
	result.travelDist = 0.0;
	result.code_cached = ROTATED_OK_MAGIC;
	result.desc_cached = "ROTATED_OK_MAGIC: Rotate action was assigned an automatic PASS";
	return result;
}

// ___ End Classes _________________________________________________________________________________________________________________________



// === Functions ===========================================================================================================================

/// === VALIDATION =========================================================================================================================


ValidationParams validation_defaults(){
	ValidationParams rtnStruct;
	// ~ Validation & Execution ~
	rtnStruct.support /* - */ =     0; // ------- Index of support
	
	rtnStruct.planPose /*- */ = origin_pose(); // Default placement for assembly   
	
	//~ rtnStruct.distance /*- */ =     0.25; // ---- Distance to lift
	rtnStruct.distance /*- */ =     0.05; // ---- Distance to lift
	
	//~ rtnStruct.numSamples      =   200; // ------- Number of samples for straightline validation
	rtnStruct.numSamples      =   100; // ------- Number of samples for straightline validation
	
	rtnStruct.sphereFraction  =     0.25; // ---- Fraction of free spherical directions to sample
	rtnStruct.RRT_K /* -- */  =  8000; // ------- Max number of samples for RRT*
	rtnStruct.RRT_Epsilon     =     0.005; // --- Max distance between configurations for RRT*
	rtnStruct.RRT_BiasProb    =     0.25; // ---- Probability with which RRT chooses the goal as the target sample
	rtnStruct.RRT_WeightPosn  =     1.0; // ----- Cost weight on position    for calculating configuration distance
	rtnStruct.RRT_WeightOrnt  =     1.0; // ----- Cost weight on orientation for calculating configuration distance
	rtnStruct.RRT_nghbrRadius = // -------------- Max radius around a new sample for rewiring
								rtnStruct.RRT_Epsilon * 2.5; 
	rtnStruct.RRT_liftBoxFctr =     1.02; // ---- Multiple of 'distance' for the bounding box
	rtnStruct.RRT_shortCutN   =     0; // ------- No limit on neighbors to search
	// ~ Execution ~
	rtnStruct.setdownLoc        = Eigen::Vector3d( 0.9  , 0.0 , 0.59 );
	rtnStruct.ROBOT_BASE        = Eigen::Vector3d( 0.55 , 0.0 , 1.20 );
	rtnStruct.graspTileSize     = 0.004; // [m]
	rtnStruct.graspDTheta       = M_PI / 4.0;
	rtnStruct.preferredWristDir = Eigen::Vector3d::UnitZ();
	rtnStruct.preferredCenter   = rtnStruct.setdownLoc + rtnStruct.preferredWristDir * rtnStruct.distance;
	rtnStruct.IKsearchLimit     =  1; // Number of attempts for each grasp
	rtnStruct.IKattemptLimit    =  5; // Number of attempts for a pose we care about
	rtnStruct.maxReplanFailures =  3;
	rtnStruct.bel_minNumSeeds   =  2; // Min number of bases to look for (Belhadj)
	
	return rtnStruct;    
}

ValidationParams copy_params( const ValidationParams& original ){
	ValidationParams rtnStruct;
	// ~ Validation & Execution ~
	rtnStruct.support /* - */ = original.support;
	rtnStruct.planPose /*- */ = original.planPose;
	rtnStruct.distance /*- */ = original.distance;
	rtnStruct.numSamples      = original.numSamples;
	rtnStruct.sphereFraction  = original.sphereFraction;
	rtnStruct.RRT_K /* -- */  = original.RRT_K;
	rtnStruct.RRT_Epsilon     = original.RRT_Epsilon;
	rtnStruct.RRT_BiasProb    = original.RRT_BiasProb;
	rtnStruct.RRT_WeightPosn  = original.RRT_WeightPosn;
	rtnStruct.RRT_WeightOrnt  = original.RRT_WeightOrnt;
	rtnStruct.RRT_nghbrRadius = original.RRT_nghbrRadius;
	rtnStruct.RRT_liftBoxFctr = original.RRT_liftBoxFctr;
	rtnStruct.RRT_shortCutN   = original.RRT_shortCutN;
	// ~ Execution ~
	rtnStruct.setdownLoc /*- */ = original.setdownLoc;
	rtnStruct.ROBOT_BASE /*- */ = original.ROBOT_BASE;
	rtnStruct.graspTileSize     = original.graspTileSize;
	rtnStruct.graspDTheta /*-*/ = original.graspDTheta;
	rtnStruct.preferredWristDir = original.preferredWristDir;
	rtnStruct.preferredCenter   = original.preferredCenter;
	rtnStruct.IKsearchLimit     = original.IKsearchLimit;
	rtnStruct.IKattemptLimit    = original.IKattemptLimit;
	rtnStruct.maxReplanFailures = original.maxReplanFailures;
	rtnStruct.bel_minNumSeeds   = original.bel_minNumSeeds; 
	
	return rtnStruct;
}


SuccessPath validate_removal_multimode_w_stability( Assembly_ASM* original , // Validate for removal and stability , Using Straightline and RRT*
												    std::vector<llin>& movedParts , std::vector<llin>& referenceParts , 
												    ValidationParams params  , 
												    const Eigen::Vector3d& direction ){
	// Validate for removal and stability , Using Straightline and (not yet) RRT*
	// NOTE: The main idea is to run minimal failure checks and maximal success checks
	// NOTE: Weak Version
	//       This function will not rule on whether the assembly should be rotated to accomodate the operation
	// NOTE: This function makes a copy of the assembly and works with it in the origin frame
	
	bool SHOWDEBUG = false; // if( SHOWDEBUG ){  cout << "" << endl;  }
	
	if( SHOWDEBUG ){ sep( "validate_removal_multimode_w_stability" );
					 cout << "Prepare to be validated! Think Happy Thoughts!" << endl << "Assembly Info:" << endl
						  << "Number of parts: ____ " << original->how_many_parts() << endl
						  << "Moved Parts: ________ " << movedParts << endl
						  << "Reference Parts: ____ " << referenceParts << endl
						  << "Assembly Pose: ______ " << original->get_pose() << endl
						  << "Client Code Suggests: " << direction << endl ;  }
			
	// ~~ Init ~~		
	
	SuccessPath result = empty_SuccessPath();
	result.travelDist  = 0.0;
	result.success     = false;
	result.movdIDs     = vec_copy( movedParts );
	result.refcIDs     = vec_copy( referenceParts );
	
	SuccessPoints vanillaChk = empty_SuccessPoints();
	
	// 0. Init
	stringstream diagnosticStr;
	
	// A. Instantiate a test asm to manipulate
	Assembly_ASM* testAsm = original->replicate();
	testAsm->set_pose( origin_pose() );
	
	// ~~~ LAMBDA FUNCS ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
	
	auto log_farthest = [&]( const SuccessPoints& pointsResult ){
		// Cache the farthest we traveled during checks
		if( pointsResult.travelDist > result.travelDist ){ 
			result.travelDist = pointsResult.travelDist;
			result.bestDir    = pointsResult.bestDir;
			result.path       = translate_pose_by_sequence( origin_pose() , pointsResult.goodSeqPos );
		}
	};
	
	auto log_winner = [&]( const SuccessPoints& pointsResult ){
		// overwrite the farthest we traveled during checks
		result.travelDist = pointsResult.travelDist;
		result.bestDir    = pointsResult.bestDir;
		result.path       = translate_pose_by_sequence( origin_pose() , pointsResult.goodSeqPos );
	};
	
	auto add_winning_direction = [&]( const Eigen::Vector3d& winDir ){
		// Add to our collection of good directions
		result.goodDirs = copy_V_plus_row( result.goodDirs , winDir );
	};
	
	auto perform_vanilla_check = [&]( const Eigen::Vector3d& tstDir ){
		// Run the check from the previous version
		vanillaChk = empty_SuccessPoints();
		vanillaChk = testAsm->record_test_trans_sub_removal( movedParts , referenceParts , // Test removing 'movedParts' 
															 tstDir  , params.distance , params.numSamples );
		log_farthest( vanillaChk );
		if( vanillaChk.success ){  add_winning_direction( tstDir );  }
		
		if( !(result.success)  &&  vanillaChk.success ){
			if( SHOWDEBUG ){  cout << "PASS_STRAIGHTLIN: Successful for relative direction " << tstDir << " and distance " << result.travelDist << endl;  }
			result.success = true;
			result.code = PASS_STRAIGHTLIN;
			diagnosticStr << "PASS_STRAIGHTLIN: Successful for relative direction " << tstDir << " and distance " << result.travelDist << endl;
			result.desc = diagnosticStr.str();
			log_winner( vanillaChk );
		}
	};
	
	// ,,, END LAMBDA ,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,
			
	/// ~~ Check 0 : Bad Request ~~
	
	bool chkPass = false;
	std::vector<size_t> movdDices;
	std::vector<size_t> refcDices;		
	movdDices = testAsm->NDBG_order_vec_from_Part_ID_vec( movedParts );
	refcDices = testAsm->NDBG_order_vec_from_Part_ID_vec( referenceParts );
	
	if( SHOWDEBUG ){  cout << "movdDices: " << movdDices << endl;  }
	if( SHOWDEBUG ){  cout << "refcDices: " << refcDices << endl;  }
	
	chkPass = (  ( movdDices.size() == movedParts.size() )  &&  ( refcDices.size() == referenceParts.size() )  );
	
	if( !chkPass ){
		if( SHOWDEBUG ){  cout << "ERROR_INCOMPLETE: Bad request , Some of Moved " << movedParts << " or Reference " << referenceParts 
							   << " not found in the complete assembly " << testAsm->get_part_ID_vec() << endl;  }
		result.success = false;
		result.code = ERROR_INCOMPLETE;
		diagnosticStr << "ERROR_INCOMPLETE: Bad request , Some of Moved " << movedParts << " or Reference " << referenceParts 
					  << " not found in the complete assembly " << testAsm->get_part_ID_vec() << endl;
		result.desc = diagnosticStr.str();
		return result;
	}else if( SHOWDEBUG ){  cout << "Request well-formed." << endl;  }
			
	/// ~~ Check 1 : Stability Concerns ~~
	
	chkPass = false;
	std::vector<std::vector<llin>> connSubGraphs;
	
	chkPass = testAsm->validate_partID_subset_does_not_float( referenceParts );
	if( !chkPass ){
		if( SHOWDEBUG ){  cout << "FLOATING_HAM_ERR: Reference " << referenceParts << " does not share any supports with the original assembly" << endl;  }
		result.success = false;
		result.code = FLOATING_HAM_ERR;
		diagnosticStr << "FLOATING_HAM_ERR: Reference " << referenceParts << " does not share any supports with the original assembly" << endl;
		result.desc = diagnosticStr.str();
		return result;
	}else if( SHOWDEBUG ){  cout << "Was not floating." << endl;  }
		

	// DANGER: Will this change results?
	//  Check that the mover is not disconnected from many at once
	if( SHOWDEBUG ){  cerr << "About the evaluate connectivity ..." << endl;  }
	//  A. Check that the sub is connected
	connSubGraphs = connected_subgraphs_in_subgraph( testAsm , referenceParts );
	if( connSubGraphs.size() > 1 ){ 
		//  B. If not, FAIL
		if( SHOWDEBUG ){  cout << "REFERNC_DISJOINT: Remainder " << referenceParts 
							   << " contains disjoint connected subgraphs: " << connSubGraphs << " , Simultaneous disconnect not allowed " << endl;  }
		result.success = false;
		result.code = NEW_SUB_DISJOINT;
		diagnosticStr << "REFERNC_DISJOINT: Remainder " << referenceParts 
					  << " contains disjoint connected subgraphs: " << connSubGraphs << " , Simultaneous disconnect not allowed " << endl;
		result.desc = diagnosticStr.str();
		return result;
	}
						  
	/// ~~ Check 2 : Mover Freedom ~~
	
	// 1. Get freedom class
	Eigen::MatrixXd relConstraints;
	FREEDOM_CLASS   moverFreedom;
	
	if( SHOWDEBUG ){  cout << "Calculate freedom class ..." << endl;  }
	relConstraints = testAsm->asm_get_constraints_on_movd( movedParts , referenceParts ); // PartID
	moverFreedom   = freedom_class_from_constraints( relConstraints );
	if( SHOWDEBUG ){  cout << "Freedom: " << freedom_string( moverFreedom ) << endl;  }
	
	if( moverFreedom == LOCKED ){
		if( SHOWDEBUG ){  cout << "MOVER_SUB_LOCKED: Mover " << movedParts << " is blocked by reference " << referenceParts << endl;  }
		result.success = false;
		result.code = MOVER_SUB_LOCKED;
		diagnosticStr << "MOVER_SUB_LOCKED: Mover " << movedParts << " is blocked by reference " << referenceParts << endl;
		result.desc = diagnosticStr.str();
		return result;
	}else if( SHOWDEBUG ){  cout << "Mover " << movedParts << " was not locked." << endl;  }
	
	/// ~~ Check 3 : Suggested Straightline ~~
	
	Eigen::Vector3d relDir = err_vec3() ,
					remDir = err_vec3() ;
	
	if( SHOWDEBUG ){  cout << "Suggest a removal direction ..." << endl;  }
	relDir = Eig_vec3d_round_zero( testAsm->asm_center_of_freedom_direction( movedParts , referenceParts ) ); // Part ID
	remDir = Eig_vec3d_round_zero( original->get_orientation() * relDir );
	if( SHOWDEBUG ){  cout << "Relative Removal: ___ " << relDir << endl
						   << "Suggested Lab RemDir: " << remDir << endl;  }
						   
	
	     
	if( SHOWDEBUG ){  cout << "Perform the vanilla check ..." << endl;  }
	
	perform_vanilla_check( relDir );
	
	if( SHOWDEBUG ){  cout << "Check Passed?: ___ " << yesno( vanillaChk.success ) << endl
						   << "Points Recorded: _ " << vanillaChk.goodSeqPos.rows() << " out of " << params.numSamples << " required" << endl
						   << "Distance Traveled: " << vanillaChk.travelDist << " [m] out of " << params.distance << " required" << endl;  }
	
	
	/// ~~ Check 4 : Constraint-Sampled Straightline ~~
	
	Eigen::MatrixXd sampleDirs;
	size_t /* -- */ numSamples = 0;
	Eigen::Vector3d smpDir;
	
	if( SHOWDEBUG ){  cout << "Sample on constraints ..." << endl << "Constraints:" << endl << relConstraints << endl;  }
	
	sampleDirs = sample_within_constraints( relConstraints );
	numSamples = sampleDirs.rows();
	if( numSamples > 0 ){
		for( size_t i = 0 ; i < numSamples ; i++ ){
			smpDir = sampleDirs.row(i);
			perform_vanilla_check( smpDir );
		}
	}
	
	// Clean up repeat directions
	result.goodDirs = uniqify_directions( result.goodDirs );
	
	if( SHOWDEBUG ){  cout << "Returning the following winning directions:" << endl << result.goodDirs << endl 
						   << "Best Direction: " << result.bestDir << endl ;  }
	
	// N-1. Clean up
	delif( testAsm );
	
	//  N. Return the story of our success
	return result;
}
/// ___ END VALIDATE _______________________________________________________________________________________________________________________

/// === Competing Methods ==================================================================================================================

	
/// === PLAN EXECUTION =====================================================================================================================

// ~~~ LIFT PLANNER ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

bool any_poses_below_Z( const std::vector<Pose_ASP>& allPoses , double Zlvl ){
	size_t len = allPoses.size();
	for( size_t i  = 0 ; i < len ; i++ ){
		if( allPoses[i].position(2) < Zlvl ){ 
			cout << "any_poses_below_Z , Pose failed criterion , Z >= " << Zlvl << " : " << allPoses[i] << endl;
			return true;
		}
	}
	return false;
}

// ~~~ LEVEL PLANNER ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

// === class ActionSpec ===

ActionSpec::ActionSpec( Assembly_ASM* pAssembly , SuccessPath pLiftAction , ACTIONS pAction ){
	assembly   = pAssembly->replicate();
	liftAction = pLiftAction;
	action     = pAction;
}

ActionSpec::~ActionSpec(){  delif( assembly );  }

// ___ End ActionSpec ___

IndexMatches best_starting_support_for_level( AsmStateNode_ASP* planRoot , ValidationParams params ){
	// Find the support that will allow the most actions from the beginning
	
	bool SHOWDEBUG = true; // if( SHOWDEBUG ){  cout << "" << endl;  }
	
	if( SHOWDEBUG ){  cout << "Init ..." << endl;  }
	
	IndexMatches rtnStruct;
	rtnStruct.bestDex = BILLION;
	
	// ~~ Phase 0: Init ~~
	//  A. Get the number of edges
	size_t /* ------ */ numEdges  = planRoot->outgoingEdges.size(); // Number of edges from the root
	std::vector<size_t> edgeDices     , // --------------------------- Indices of all the edges
						suggestSupprt ; // --------------------------- Recommended support to execute each edge
	std::vector<llin>   subPartIDs , // ------------------------------ Parts contained in the edge sub
						currBalanc ; // ------------------------------ Remainder parts for the assembly being looked at
	IndexSuccesLookup   supportVote; // ------------------------------ Info about which support is best for a certain action
	 
	if( numEdges > 0 ){
		if( SHOWDEBUG ){  cout << "There are " << numEdges << " edges" << endl 
							   << "Finding the best starting support ..." << endl;  }
		//  B. Get a list of edge indices
		edgeDices = vec_range( (size_t)0 , numEdges-1 );
	}else{
		if( SHOWDEBUG ){  cout << "There were no edges!" << endl;  }
		return rtnStruct;
	}
						   
	if( numEdges > 0 ){
		//  B. Get a list of edge indices
		edgeDices = vec_range( (size_t)0 , numEdges-1 );
		
		// ~~ Phase 1: Beginning Pose ~~
	
		//  1. For each node
		for( size_t i = 0 ; i < numEdges ; i++ ){
			subPartIDs = planRoot->outgoingEdges[i]->assembly->get_part_ID_vec();
			currBalanc = planRoot->assembly->get_balance_IDs_from_sub( subPartIDs );
			//  A. Suggest a support for edge sub removal and store
			supportVote = planRoot->assembly->suggest_support_for_staightline_stability( subPartIDs , currBalanc , 
																						 params.distance , params.numSamples ); 
			suggestSupprt.push_back( supportVote.bestDex ); // Log the best support whether it was successful or not
		}
		//  B. Set the root node to the support with the most votes
		rtnStruct.bestDex = most_numerous_value_in( suggestSupprt );
		rtnStruct.indices = all_indices_equal_to_val( suggestSupprt , rtnStruct.bestDex );
		if( SHOWDEBUG ){  cout << "Found the best support:" << rtnStruct.bestDex << endl;  }
		
	}else if( SHOWDEBUG ){  cout << "There were no edges!" << endl;  }
	
	return rtnStruct;
}


// ~~~ TREE PLANNER ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~


// === class LevelPlanNode ===

// ~ Con/Destructors ~

LevelPlanNode::LevelPlanNode( AsmStateNode_ASP* linkedState , size_t pDepth ){
	// Create a node linked to an assembly state
	stateLink = linkedState;
	depth     = pDepth;
	parent    = nullptr;
	timeInSec = 0.0;
}

void LevelPlanNode::_init( bool pAllPassed , std::vector<ActionSpec*>& pLevelPlan ){
	// Load precedence level execution data
	allPassed = pAllPassed;
	levelPlan = vec_copy( pLevelPlan );
}

LevelPlanNode::~LevelPlanNode(){
	// erase pointers
	clearif( levelPlan  ); 
	clearif( successors );
}

// ~ Methods ~

void LevelPlanNode::add_child( LevelPlanNode* successor ){  
	successor->parent = this;
	successors.push_back( successor );  
}

LevelPlanNode* LevelPlanNode::get_root(){
	if( !parent ){  return this;  } // ---- Base Case ____ : At root, return 'this'
	else{  return parent->get_root();  } // Recursive Case : There is parent , so return its root
}

// ___ End LevelPlanNode ___


bool failed_edges_in_level( const std::vector<ActionSpec*>& levelPlan ){
	// Scan the level plan for failed actions
	size_t len = levelPlan.size();
	for( size_t i = 0 ; i < len ; i++ ){
		if( levelPlan[i]->action == ACT_NO_OPR ){  return true;  }
	}
	return false;
}

/// ___ END EXEC ___________________________________________________________________________________________________________________________
	
// ~~ Helper Functions ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

	
// ~~ Part Interaction Clusters , 2013 Morato ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

std::vector< std::vector< llin > > kMeans_part_clusters_ID( Assembly_ASM* original , size_t k ){
	// Separate the assembly into 'k' clusters based on their centroid locations
	size_t numParts /* ------- */ = original->how_many_parts();
	std::vector< llin > partIDvec = original->get_part_ID_vec();
	Eigen::MatrixXd allCenters    = original->get_part_centroids();
	std::vector< std::vector< llin > > clusterIDs;  
	vec_vec_top_populate( clusterIDs , k ); // Create 'k' sublists
	PointsClassfd partClusters = kMeans_clusters_3D( k , allCenters );
	for( size_t i = 0 ; i < numParts ; i++ ){  clusterIDs[ partClusters.assignments(i) ].push_back( partIDvec[i] );  }
	return clusterIDs;
}

std::vector< std::vector< llin > > cluster_one_level_disasm( Assembly_ASM* original , ValidationParams params ){ 
	// Take the current assembly and break it into subs using the Morato method
	
	bool SHOWDEBUG = true; // if( SHOWDEBUG ){  cout << "" << endl;  }
	
	/// VALIDATION METHOD ////
	bool multiValidate = true;
	SuccessPath valResult;
	SuccessPath remResult;
	///|//////////////////////
	
	size_t /* -----> */ k           =  2                          , // -- Number of subs to attempt
		                numParts    =  original->how_many_parts() , // -- Number of parts in the original asm
		                clusNum     =  0                          , // -- Number of clusters
		                subLen      =  0                          , // -- Number of parts in each sub
		                maxKattmpts =  5                          ; // -- Maximum number of times to try any k
	bool /* -------> */ remvSuccess = false , // ------------------------ Flag for whether a removal operation is valid or not
		                isConnected = false ; // ------------------------ Flag for whether the sub is connected or not
	usll /* -------> */ numSampls   = params.numSamples; // ------------- Number of position samples to use for validation
	double /* -----> */ movDist     = params.distance; // --------------- Distance from starting position at which sub is considered removed
	std::vector<size_t> kTrials     = vec_index_zeros( numParts - 1 ); // Number of times that a k-means subdivision is tried
	
	Assembly_ASM* /* ------------ */ currSub; // ----- 'original' minus the largest part
	std::vector<std::vector<llin>>   subIDVecs; // --- Part IDs     for each of the clusters , Part ID
	std::vector<std::vector<llin>>   cnnctdSubs; // -- Part IDs     for connected subgraphs  , Part ID
	std::vector<std::vector<size_t>> indices; // ----- Part indices for each of the clusters , NDBG Indices
	std::vector<std::vector<bool>>   currLiason; // -- Current Liaison Graph
	std::vector<std::vector<size_t>> splitDices; // -- Vectors of indices of parts that form connected subs
	std::vector<std::vector<llin>>   splitSubs; // --- Vectors of IDs     of parts that form connected subs
	std::vector<llin> /* -------- */ movdParts; // --- Moved parts
	std::vector<llin> /* -------- */ rfrcParts; // --- Reference parts ( including base! ) discounting only the parts in the current sub
	std::vector<llin> /* -------- */ runningBalnc; //- Parts remaining after successful removals
	Eigen::Vector3d /* ---------- */ removDir; // ---- Removal direction suggested by the local freedom constraints
	std::vector<std::vector<llin>>   rtnLayerSubs; //- Vector of sub ID listings to return
	llin /* --------------------- */ baseID; // ------ ID of the part identified as base ( withheld , see below )
	std::vector<llin> /* -------- */ failedSubs; // -- Holds all of the failed assemblies
	std::vector<size_t> /* ------ */ dumpDices; // --- Holds indices of all the failed assemblies
	
	original->recalc_geo();
	currLiason = original->get_liaison_graph();
	// 0. Find the largest part and remove it from consideration
	// a. Find the largest part
	IDDbblResult largest = original->largest_part_ID_w_vol();
	// b. Create a sub without the largest part
	std::vector<llin> allID   = original->get_part_ID_vec();
	std::vector<llin> smllrID = vec_copy_without_elem( allID , largest.ID );
	baseID = largest.ID;
	currSub = original->sub_from_spec( smllrID , (size_t)0 );
	currSub->recalc_geo();
	
	if( SHOWDEBUG ){  cout << "Base Part (Largest): " << largest.ID << endl
						   << "Non-Base Parts: ____ " << smllrID << endl;  }
	
	//  1. While k < number of total parts 
	//~ while( ( k <= numParts ) ){ 
	//  1. While k < number of total parts || We have not tried any one subdvision too many times
	while( ( k <= numParts ) || ( max_num_in_vec( kTrials ) <= maxKattmpts ) ){ 
		
		if( SHOWDEBUG ){  cout  << endl << "Attempting k = " << k << endl;  }
		
		//  2. Separate parts into k = 2 k-means clusters
		subIDVecs = kMeans_part_clusters_ID( currSub , k ); // Asm clustered without the largest part
		
		if( SHOWDEBUG ){  cout << "DEBUG , " << "Clustering yielded " << subIDVecs.size() << " subs." << endl;  }
		
		//  3. Check for empty clusters ( Example: More than one mean converges on the same location )
		if( vec_vec_any_empty( subIDVecs ) ){
			// Eliminate empty clusters
			subIDVecs = vec_vec_copy_nonempty( subIDVecs );
			if( SHOWDEBUG ){  cout << "DEBUG , " << "Eliminating empties yielded " << subIDVecs.size() << " subs." << endl;  }
		}
		
		// Fetch size and index info for further processing , Clear the previous list of clusters
		clusNum = subIDVecs.size(); // Number of clusters that remain after eliminating empties
		indices = original->partID_vec_vec_to_index_vec_vec( subIDVecs ); // Fetch sub indices from IDs
		cnnctdSubs.clear(); // Erase subs from last iteration
		
		//  4. For each of the remaining clusters
		for( size_t i = 0 ; i < clusNum ; i++ ){
			//  5. Check that each of the clusters are connected
			if( !is_subgraph_connected( currLiason , indices[i] ) ){
				//  6. If there are disconnected clusters , split them into connected subgraphs ( based on liaison )
				splitDices = connected_subgraphs_in_subgraph( currLiason , indices[i] );
				//  A. Convert indices back into part IDs
				splitSubs = original->index_vec_vec_to_partID_vec_vec( splitDices );
				//  7. Push the split clusters onto the list of all subs
				extend_vec_vec_with( cnnctdSubs , splitSubs );
			}else{ // else the sub is connected, just copy verbatim
				cnnctdSubs.push_back( vec_copy( subIDVecs[i] ) );
			}
		}
		
		if( SHOWDEBUG ){  cout << "DEBUG , " << "Separating unconnected subs yielded " << cnnctdSubs.size() << " subs." << endl;
					      print_vec_vec( cnnctdSubs ); 
					      cout << endl;  }
		
		clusNum = cnnctdSubs.size(); // Number of clusters that remain after splitting unconnected subs
		
		failedSubs.clear();
		
		//  8. For each of the connected subs
		for( size_t i = 0 ; i < clusNum ; i++ ){
			
			if( SHOWDEBUG ){  cout << "Cluster " << i+1 << " of " << clusNum << endl;  }
			
			remvSuccess = false;
			//  9. Test the clusters one at a time 
			//  b. Construct the list of moved and reference parts
			movdParts = cnnctdSubs[i]; // The cluster is the moved parts
			rfrcParts = vec_copy_without_elem( allID , movdParts ); // All the other parts are the reference parts
			
			if( SHOWDEBUG ){  cout << "Moved Parts: ___ " << movdParts << endl
								   << "Reference Parts: " << rfrcParts << endl;  }
			
			//  c. Obtain the preferred removal direction
			if( SHOWDEBUG ){  cout << "About to obtain Removal Direction ..." << endl;  }
			removDir = original->asm_center_of_freedom_direction( movdParts , rfrcParts ); // Part ID
			if( SHOWDEBUG ){  cout << "Removal Direction: " << removDir << endl;  }
			
			if( !is_err( removDir ) ){
				
				//  d. attempt removal
				/// ### VALIDATE SUB ###
				if( multiValidate ){
					//  I. Validate using multiple directions and methods
					//     i.e. Validate for removal and stability , Using Straightline and RRT*
					if( SHOWDEBUG ){  cout << "About to run validator ..." << endl;  }
					
					// A. Run validator for feasibility of single assembly , It will only pass if it is free
					valResult = validate_removal_multimode_w_stability( original , 
																		movdParts , rfrcParts , 
																		params , 
																		removDir );
													
					if( SHOWDEBUG ){  cout << "Validation completed! , Result: " << yesno( valResult.success ) << endl;  }
					// II. Set flag
					if( SHOWDEBUG ){  cout << "Set flag" << endl;  }
					remvSuccess = valResult.success;
				}else{
					remvSuccess = original->test_translation_sub_removal( movdParts , rfrcParts , // Test removing 'movedParts' 
																		  removDir  , movDist   , numSampls );
				}
				/// *** END VALIDATE ***
				
				
			}else{  remvSuccess = false;  }
																  
			if( remvSuccess ){ // Add the ones that succeed to the layer
				if( SHOWDEBUG ){  cout << "DEBUG , " << "Removal success! Adding to layer ... ";  }
				rtnLayerSubs.push_back( vec_copy( movdParts ) );
				if( SHOWDEBUG ){  cout << "Added!" << endl;  }
			}else{  // else the cluster failed, leave in the asm to be taken into the other layer
				if( SHOWDEBUG ){  cout << "DEBUG , " << "Removal failure!" << endl;  }
				extend_vec_with( failedSubs , movdParts );
				if( SHOWDEBUG ){  cout << "Failure Heap: " << failedSubs << endl;  }
			}
		}
		
		// 10. If we found subs to remove , Then return them
		if( rtnLayerSubs.size() > 0 ){  
			// Make sure that the base part is accounted for
			std::vector<llin> baseVec = { baseID };
			
			// If there were failed subs, they belong with the base for reevaluation
			if( failedSubs.size() > 0 ){  
				extend_vec_with( baseVec , failedSubs );  
				dumpDices = original->partID_vec_to_index_vec( baseVec ); // Fetch sub indices from IDs
				
				if( !is_subgraph_connected( currLiason , dumpDices ) ){
					//  6. If there are disconnected clusters , split them into connected subgraphs ( based on liaison )
					splitDices = connected_subgraphs_in_subgraph( currLiason , dumpDices );
					//  A. Convert indices back into part IDs
					splitSubs = original->index_vec_vec_to_partID_vec_vec( splitDices );
					//  7. Push the split clusters onto the list of all subs
					extend_vec_vec_with( rtnLayerSubs , splitSubs );
				}else{ // else the sub is connected, just copy verbatim
					rtnLayerSubs.push_back( baseVec );
				}
			}else{ rtnLayerSubs.push_back( baseVec ); }
			
			return rtnLayerSubs;  
		}
		// 11. Else did not find subs, increment k 
		else{  
			kTrials[ cnnctdSubs.size() ]++; // We tried this many subs 'kTrials[ i ]' times
			k++; // -------------------------- Next time start with k+1 subs
		}
		
	}
	delif( currSub );
	return rtnLayerSubs; // If we made it here, an empty subs vector is returned 
}

AsmStateNode_ASP* Morato2013Modified_full_depth( Assembly_ASM* original , ValidationParams params ){
	
	AsmStateNode_ASP* rootNode = new AsmStateNode_ASP( original );
	std::queue<AsmStateNode_ASP*> subNodes;
	AsmStateNode_ASP* currNode = nullptr;
	std::vector<std::vector<llin>> asmClusterLvl1;
	size_t len = 0;
	bool SHOWDEBUG = true;
	
	// 1. Enqueue original (node)
	subNodes.push( rootNode );
	// 2. While there are still assemblies in queue
	while( subNodes.size() > 0 ){
		// 3. Retrieve and Pop node
		currNode = subNodes.front();  subNodes.pop();
		// 4. If the asm has more than one part
		//~ if( currNode->memParts.size() > 1 ){
		if( currNode->assembly->get_part_ID_vec().size() > 1 ){
			//~ asmClusterLvl1.clear();
			// 5. Execute 1st-level clustering
			asmClusterLvl1 = cluster_one_level_disasm( currNode->assembly , params );
			len = asmClusterLvl1.size();
			if( len > 0 ){ // This should always be true if the asm clustered had more than one part
				// 6. For every parts list produced
				for( size_t i = 0 ; i <  len ; i++ ){
					// 7. Produce a new assembly , load into Node  &&  8. Enqueue the new assembly
					subNodes.push( currNode->add_edge( asmClusterLvl1[i] ) );
				}
			}
		}
	}
	return rootNode;
}

PointsClassfd kMeans_clusters_3D( size_t k , Eigen::MatrixXd points ){
	size_t          numPts     = points.rows() , 
					iterLim    = 1000          , 
					counter    = 0             ;
	Eigen::ArrayXXi assignment = Eigen::ArrayXXi::Zero( 1 , numPts );
	Eigen::ArrayXXd distance   = Eigen::ArrayXXd::Zero( 1 , numPts );
	bool            changed    = true;
	IndexDbblResult closest;
	Eigen::Vector3d currPnt;
	std::vector< std::vector< size_t > > indices;
	PointsClassfd rtnStruct;
	// 1. Get the bounding box of the points
	Eigen::MatrixXd corners = AABB( points );
	// 2. Initialize the means at random points within the bounding box
	Eigen::MatrixXd means   = sample_from_AABB( k , corners );
	do{
		changed = false; // ------------------- Reset changed flag
		vec_vec_top_populate( indices , k ); // Reset means lookup
		
		// 3. Assign each point to a mean
		for( size_t i = 0 ; i < numPts ; i++ ){
			currPnt = points.row(i);
			closest = closest_point_to_sq( means , currPnt );
			if( assignment(i) != closest.index ){  changed = true;  }
			assignment(i) = closest.index;
			distance(i)   = closest.measure;
			indices[ closest.index ].push_back( i ); // Store the assignment in a lookup for the next step
		}
		
		// 4. Update means
		for( size_t j = 0 ; j < k ; j++ ){  means.row(j) = selected_average_V( points , indices[j] );  }
		
		// 5. Update counter
		counter++;
	}while( changed && ( counter <= iterLim ) ); // While there are still assignment changes and the iteration limit has not been reached
	// Return the means that define the classes and the class assignments
	rtnStruct.points = means;
	rtnStruct.assignments = assignment;
	return rtnStruct;
}

// ~~ Base Part Fitness & Force Fits , 2017 Belhadj ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

NumsClassfd kMeans_clusters_1D( size_t k , std::vector<double> points ){ // NOT USED
	// NOTE: This function assumes that k >= 2
	size_t numPts  = points.size() , 
		   iterLim = 1000          , 
		   counter = 0             ;
	bool   changed = true;
	double minPnt  = min_num_in_vec( points ) , 
		   maxPnt  = max_num_in_vec( points ) ,
		   currPnt = 0.0                      ;
	std::vector<double> means;
	std::vector<double> distance   = vec_dbbl_zeros( k );
	std::vector<size_t> assignment = vec_index_zeros( k );
	std::vector< std::vector< size_t > > indices;
	IndexDbblResult closest;
	NumsClassfd rtnStruct;
	// 1. Init means
	if( k == 2 ){ // A. If k = 2 , Init means on opposite ends of the number line
		means.push_back( minPnt );
		means.push_back( maxPnt );
	}else{ // B. If k > 2 , Init means evenly
		means = linspace( minPnt , maxPnt , k );
	}
	// 2. Iterate
	do{
		changed = false; // ------------------- Reset changed flag
		vec_vec_top_populate( indices , k ); // Reset means lookup
		
		// 3. Assign each point to a mean
		for( size_t i = 0 ; i < numPts ; i++ ){
			currPnt = points[i];
			closest = closest_point_to_sq( means , currPnt );
			if( assignment[i] != closest.index ){  changed = true;  }
			assignment[i] = closest.index;
			distance[i]   = closest.measure;
			indices[ closest.index ].push_back( i ); // Store the assignment in a lookup for the next step
		}
		
		// 4. Update means
		for( size_t j = 0 ; j < k ; j++ ){  means[j] = selected_average_vec( points , indices[j] );  }
		
		// 5. Update counter
		counter++;
	}while( changed && ( counter <= iterLim ) ); // While there are still assignment changes and the iteration limit has not been reached
	// 3. Load the return structure and return
	rtnStruct.points = means;
	rtnStruct.assignments = assignment;
	return rtnStruct;
}

std::vector<std::vector<double>> numbers_1D_breakdown( NumsClassfd& classifications ){
	// Create a vector vector of the points as they appear in their classes
	// NOTE: This function does not sort points
	std::vector<std::vector<double>> rtnVecVec;
	// 1. Recover k from the cluster result
	size_t k   = max_num_in_vec( classifications.assignments ) + 1 , 
	       len = classifications.points.size()                     ;
	// 2. For each class
	for( size_t classNum = 0 ; classNum < k ; classNum++ ){
		// 3. Push a new group vector onto the result
		std::vector<double> temp;  rtnVecVec.push_back( temp );
		// 4. Search the assignments for the current class and add them to the current group
		for( size_t i = 0 ; i < len ; i++ ){
			if( classifications.assignments[i] == classNum ){  rtnVecVec[ classNum ].push_back( classifications.points[i] );  }
		}
	}
	// 5. Return groups
	return rtnVecVec;
}

// Return true if the weights sum to 1
bool weights_sum_to_one( BasePartFitness& components ){  return eq( (double)1.0 , components.w_alpha + components.w_beta + components.w_gamma );  }

void normalize_weights( BasePartFitness& components ){
	// if the weights do not sum to 1, normalize them
	if( !weights_sum_to_one( components ) ){ // Don't bother unless the weights violate the constraint
		double sum = components.w_alpha + components.w_beta + components.w_gamma;
		components.w_alpha /= sum;
		components.w_beta  /= sum;
		components.w_gamma /= sum;
	}
}

double base_fitness_from_components( BasePartFitness& components ){
	// Set and Return the base part fitness for this part, assuming all of the components have already been calculated
	normalize_weights( components ); // Enforce normalized weights
	double fitness = components.w_alpha * ( components.Si_bndrySurf / components.St_totalSurf )
	               + components.w_beta  * components.Nr_Adjacency
	               + components.w_gamma * ( components.Vi_partVolum / components.Vt_totVolume ); // Equation 5
	components.baseFitness = fitness;
	return fitness;
}

std::map< llin , BasePartFitness > calc_base_fitness_for_parts( Assembly_ASM& assembly , double alpha , double beta , double gamma ){
	// Calculate the fitness for each part in the assembly to serve as a base part
	
	bool SHOWDEBUG = false;
	
	std::vector<BasePartFitness> fitnessData;
	std::map< llin , BasePartFitness > rtnMap; 
	
	if( SHOWDEBUG ){  cout << "About to fetch asm size ..." << endl;  }
	
	//  1. Get a vector of all of the part IDs
	std::vector<llin> partIDs = assembly.get_part_ID_vec();
	size_t len /* -------- */ = partIDs.size();
	double totalVolume /*--*/ = 0.0 , 
		   totalArea /* -- */ = 0.0 ,
		   fitness  /* --- */ = 0.0 ;
		   
	if( SHOWDEBUG ){  cout << "About to calculate part data ..." << endl;  }
		   
	//  2. For each part ID
	for( size_t i = 0 ; i < len ; i++ ){
		Part_ASM& currPart = assembly.get_part_ref_w_ID( partIDs[i] );
		BasePartFitness temp;
		
		if( SHOWDEBUG ){  cout << "About to fetch volume ..." << endl;  }
		//  3. Get the part volume and assign
		temp.Vi_partVolum = currPart.get_volume();
		//  4. Accumulate part volume
		totalVolume += temp.Vi_partVolum;
		
		if( SHOWDEBUG ){  cout << "About to fetch shared area ..." << endl;  }
		//  5. Get the neighboring surface area and assign
		temp.Si_bndrySurf = assembly.how_much_shared_area( partIDs[i] );
		
		if( SHOWDEBUG ){  cout << "About to fetch liaisons ..." << endl;  }
		//  6. Get the number of neighbors
		temp.Nr_Adjacency = assembly.how_many_liaisons( partIDs[i] );
		
		if( SHOWDEBUG ){  cout << "About to surface area ..." << endl;  }
		//  7. Accumulate surface area
		totalArea += currPart.get_surface_area();
		//  8. Store the struct
		fitnessData.push_back( temp );
	}
	
	if( SHOWDEBUG ){  
		cout << "Part data calculated!" << endl;  
		cout << "About to store accumulated data ..." << endl;  
	}
	
	//  9. For each created structure
	for( size_t i = 0 ; i < len ; i++ ){
		// 10. Assign the total volume for every part
		fitnessData[i].St_totalSurf = totalArea;
		// 11. Assign the total surface aread for every part
		fitnessData[i].Vt_totVolume = totalVolume;
		// 12. Calc fitness
		fitnessData[i].w_alpha = alpha;
		fitnessData[i].w_beta  = beta;
		fitnessData[i].w_gamma = gamma;
		base_fitness_from_components( fitnessData[i] );
		// 13. Iterate vectors and assign fitness mapping for every part
		rtnMap[ partIDs[i] ] = fitnessData[i];
	}
	return rtnMap;
}

llin ID_w_max_base_fitness( std::map< llin , BasePartFitness >& baseScores ){
	std::map< llin , BasePartFitness >::iterator it;
	std::vector<llin> partIDs = map_keys_vec( baseScores );
	llin   maxPart = partIDs[0];
	double maxScor = baseScores[ maxPart ].baseFitness;
	for ( it = baseScores.begin(); it != baseScores.end(); it++ ){
		if( it->second.baseFitness > maxScor ){
			maxPart = it->first;
			maxScor = it->second.baseFitness;
		}
	}
	return maxPart;
}

IDScoreLookup assoc_IDs_scores( std::map< llin , BasePartFitness >& baseScores ){
	// Associate part IDs with their base part fitness scores
	IDScoreLookup rtnStruct;
	rtnStruct.IDvec = map_keys_vec( baseScores );
	size_t len = rtnStruct.IDvec.size();
	for( size_t i = 0 ; i < len ; i++ ){  rtnStruct.scoreVec.push_back( baseScores[ rtnStruct.IDvec[i] ].baseFitness );  }
	return rtnStruct;
}

std::vector<std::vector<bool>> dissociate_indices( std::vector<std::vector<bool>>& liaisonG , std::vector<size_t> indices ){ 
	// Remove edges in 'liaisonG' between the given 'indices'
	std::vector<std::vector<bool>> rtnVecVec = vec_vec_copy( liaisonG );
	size_t len = indices.size() ,
		   a   = 0              ,
		   b   = 0              ;
	for( size_t i = 0 ; i < len - 1 ; i++ ){
		a = indices[i];
		for( size_t j = i + 1 ; j < len ; j++ ){
			b = indices[j];
			rtnVecVec[a][b] = false;
			rtnVecVec[b][a] = false;
		}
	}
	return rtnVecVec;
}

double lo_bound_for_fraction_geq( std::vector<double>& unorderedVec , double fraction ){
	// Return the number above which there are at least the specified 'fraction' of elements
	std::vector<double> sortedVec = vec_copy( unorderedVec ); 
	sort( sortedVec.begin() , sortedVec.end() );
	double len    = (double) unorderedVec.size() ,
		   margin = 0.5                          ;
	size_t sLen = unorderedVec.size();
	
	if( fraction >= 1 ){  return sortedVec[0] - margin;  } // If asked for everything , Return threshold below the lowest
	else if( fraction <= 0 ){  return sortedVec[ sLen - 1 ] + margin;  } // if asked for nothing , Return threshold above the highest
	else{
		size_t N = ceil( len * fraction );
		if( (llin) sLen - N >= 0 ){
			if( N == 0 ){  return sortedVec[ sLen - 1 ] + margin;  }
			return ( sortedVec[ sLen - N - 1 ] + sortedVec[ sLen - N ] ) / 2.0;
		}else{  return sortedVec[0] - margin;  }
	}
}

double divider_for_largest_gap( std::vector<double>& unorderedVec ){
	// Return the number that splits the largest gap in the sorted version of 'unorderedVec'
	std::vector<double> sortedVec = vec_copy( unorderedVec ); 
	sort( sortedVec.begin() , sortedVec.end() );
	size_t len    = unorderedVec.size() ,
		   maxDex = 0                   ;
	std::vector<double> gaps;
	for( size_t i = 0 ; i < len - 1 ; i++ ){  gaps.push_back( sortedVec[ i+1 ] - sortedVec[ i ] );  }
	maxDex = max_index_in_vec( gaps ); // Get the index of the largest gap
	return ( sortedVec[ maxDex ] + sortedVec[ maxDex + 1 ] ) / 2.0;
}

std::vector< std::vector< llin > > Belhadj2017_one_level( Assembly_ASM* original , 
														  double alpha , double beta , double gamma , 
														  double initFrac , size_t numIter , 
														  ValidationParams params ){
	// Return a list of subassemblies that can be identified at a single level
	bool SHOWDEBUG = true;
	
	/// VALIDATION METHOD ////
	bool multiValidate = true;
	SuccessPath valResult;
	///|//////////////////////
	
	if( SHOWDEBUG ){  cout << "About to allocate vars..." << endl;  }
	
	std::vector<std::vector<llin>> rtnSubConstituentPartIDs; // Vector of vectors containing the part IDs of each sub // return
	
	// ~ Global Bookkeeping ~
	std::vector<llin> /* -------- */ allParts  = original->get_part_ID_vec(); // Part IDs of all constituent parts of 'original'
	size_t /* ------------------- */ numParts  = allParts.size() , // ---------- Number of parts in the original
									 i_base    = 0 , // ------------------------ Index of the base currently being looked at 
									 numBases  = 0 ; // ------------------------ Number of bases identified in the init phase
	double /* ------------------- */ threshold = 0.0 , // ---------------------- Cutoff for separating bases from other parts
									 divider   = 0.0 ; // ---------------------- Cutoff for separating bases from other parts
	std::vector<llin> /* -------- */ baseParts; // ----------------------------- IDs of identified bases
	std::vector<size_t> /* ------ */ baseDices; // ----------------------------- Indices of identified bases
	std::vector<std::vector<bool>>   originlLiaison; // ------------------------ Liaison graph of 'original' assembly
	std::vector<std::vector<bool>>   reducedLiaison; // ------------------------ Liaison graph with connections between base parts removed
	std::set<llin> /* ----------- */ assignedIDs; // --------------------------- Parts that have already been assigned to subs
	std::vector<llin> /* -------- */ endingBalance; // -- All the parts that were added to no sub
	std::vector<size_t> /* ------ */ endBalanceDices; //- Indices of all of the leftover parts
	std::vector<std::vector<size_t>> connBalSubDices; //- Indices of all the connected subgraphs in the ending balance
	std::vector<llin> /* -------- */ oneBalSub; // ------ A single balance sub, split from the final balance by subgraph
	
	// ~ Per-Iteration Bookkeeping ~
	std::vector<llin>   currSub; // ----------- IDs belonging to the sub under construction
	std::queue<llin>    frontier; // ---------- Parts to evaluate for addition to the current sub
	std::set<llin>      visited; // ----------- Parts that have already been scrutinizied and should not be added to the queue
	std::vector<size_t> currNghbrDices; // ---- Indices of the neighbors of a single query on the reduced liaison
	std::vector<llin>   currNghbrIDs; // ------ IDs corresponding to the neighbors of a single query on the reduced liaison
	llin /* -------- */ currPartID = -1; // --- Current part under scrutiny
	bool /* -------- */ addCurrent = false , // Flag for whether to add the part under scrutiny to the assembly
						remSucceed = false ; // Flag for whether the sub under scrutiny can be removed succesfully
	std::vector<llin>   currBalance; // ------- IDs: ( all - sub - current ) = balance
	std::vector<llin>   tempMoveVec; // ------- Moving parts for shared sliding area calcs
	double /* ------ */ remIntSub = 0.0 , // -- Removal interface area between current part and sub     , < 0 if locked
						remIntBal = 0.0 ; // -- Removal interface area between current part and balance , < 0 if locked
	
	if( SHOWDEBUG ){  cout << "About to calculate scores...";  }
	
	// 1. Get base part fitness && Store in map
	std::map< llin , BasePartFitness > baseScores = calc_base_fitness_for_parts( *original , alpha , beta , gamma );  
	
	if( SHOWDEBUG ){  
		cout << "Calculated!" << endl;  
		cout << "About to associate scores ..." << endl;  
	}
	
	IDScoreLookup scoreTable = assoc_IDs_scores( baseScores );
	if( SHOWDEBUG ){  
		cout << "Parts: " << allParts << " (NDBG order)" << endl
			 << "~~~~~~~~~~" << endl
			 << "ID: __ " << scoreTable.IDvec << " (associated order)" << endl
			 << "scores:" << scoreTable.scoreVec << endl;  
	}
	
	
	if( SHOWDEBUG ){  cout << "About to classify parts..." << endl;  }
	
	// 2. Classify parts into base / non-base according to fitness numbers
	
	divider = divider_for_largest_gap( scoreTable.scoreVec ); // Look for a clear difference between base and not
	
	// NOTE: INFINITE recursion results if a sub is not at least partially decomposed at every level until it has one part
	//       Therefore we must identify at least two base parts.
	double dividerReductionFactor = 0.9;
	while(  ( count_grtrThan_in_vec( divider , scoreTable.scoreVec ) < params.bel_minNumSeeds )  &&  ( divider > EPSILON )  ){  
		divider *= dividerReductionFactor;
	}
	// If the divider reduced to nothing, return empty vec
	if( divider <= EPSILON ){  
		if( SHOWDEBUG ){  cout << "Failed to find a satisfactory base cutoff!" << endl;  }
		return rtnSubConstituentPartIDs;  
	} 
	threshold = divider;
	
	if( SHOWDEBUG ){  cout << "About to select bases with threshold " << threshold << " ..." << endl;  }
	
	baseParts.clear();  baseDices.clear();
	for( size_t partDex = 0 ; partDex < numParts ; partDex++ ){
		if( baseScores[ allParts[ partDex ] ].baseFitness >= threshold ){
			baseParts.push_back( allParts[ partDex ] );
			baseDices.push_back( partDex );
		}
	}
	
	if( SHOWDEBUG ){  
		cout << "Found " << baseDices.size() << endl 
			 << "\tbase parts: " << baseParts << endl 
			 << "\tbase index: " << baseDices << endl;
	}
	
	set_insert_vec( assignedIDs , baseParts );
	
	numBases = baseParts.size(); // Get the number of base parts identified
	//  2. Remove connections between base parts in the liaison graph
	originlLiaison = original->get_liaison_graph();
	
	if( SHOWDEBUG ){  
		cout << endl << "Original Liaison Graph" << endl;  
		print_vec_vec( originlLiaison );
		cout << endl;
	}
	
	reducedLiaison = dissociate_indices( originlLiaison , baseDices );
	
	if( SHOWDEBUG ){  
		cout << endl << "Reduced Liaison Graph" << endl;  
		print_vec_vec( reducedLiaison );
		cout << endl;
	}
	
	//  3. For each of the base parts
	for( size_t i_base = 0 ; i_base < numBases ; i_base++ ){ // Formerly j
		
		if( SHOWDEBUG ){  sep( "Base " + to_string( baseParts[i_base] ) );
						  cout << "Accruing to base " << i_base + 1 << " , Part: " << baseParts[i_base] << " ..." << endl;  }
		
		//   i. Reset sub bookkeeping
		visited.clear();
		erase_queue( frontier );
		//  ii. Start a new sub
		currSub = {  baseParts[ i_base ]  };
		// iii. We have visited the base
		visited.insert( baseParts[ i_base ] ); 
		//  4. Enqueue all base neighbors  &&  Add to the inspected set
		//  A. Get IDs of base neighbors
		currNghbrDices = liaison_neighbor_indices( reducedLiaison , baseDices[ i_base ] );
		if( SHOWDEBUG ){  cout << "Neighbor Indices: " << currNghbrDices << endl;  }
		currNghbrIDs   = original->index_vec_to_partID_vec( currNghbrDices );
		if( SHOWDEBUG ){  cout << "Neighbor Parts: _ " << currNghbrIDs << endl;  }
		
		//  B. Enqueue
		//~ enqueue_vec_not_in_set( frontier , currNghbrIDs , visited );
		enqueue_vec_not_in_either_set( frontier , currNghbrIDs , visited , assignedIDs );
		
		if( SHOWDEBUG ){  cout << "Investigate " << frontier.size() << " neighbor parts." << endl;  }
		
		//  5. While there are parts in queue
		while( frontier.size() > 0 ){
			
			//  6. Pop the current part under scrutiny
			currPartID = queue_get_pop( frontier );
			
			//  6.1. If we have not visited the part before
			if(  ( !is_arg_in_set( currPartID , visited ) )  &&  ( !is_arg_in_set( currPartID , assignedIDs ) )  ){
				
				//  6.2. Mark the part as visited
				visited.insert( currPartID );
			
				//  6.3. Reset addition decision
				addCurrent = false;
				
				//  7. Get part ID in sub ( tempSub )
				//  8. Get part ID in ( balance - current part )
				currBalance = vec_copy_without_elem( allParts , currPartID , currSub );
				tempMoveVec = { currPartID }; // Current part is only moving part for sliding evals
				
				if( SHOWDEBUG ){  
					cout << "About to calc interface area ..." << endl
						 << "Part " << tempMoveVec << " from current sub " << currSub     << endl
						 << "Part " << tempMoveVec << " from balance ___ " << currBalance << endl;
				}
				
				// ~ Eval Shared area ~
				
				//  9. Get interface: Current -vs- sub
				remIntSub = original->removal_interface_area( tempMoveVec , currSub );
				// 10. Get interface: Current -vs- balance
				remIntBal = original->removal_interface_area( tempMoveVec , currBalance );
				
				// ~ Make an addition decision ~
				if( SHOWDEBUG ){  cout << "Interface Areas , Sub: " << remIntSub << " , Balance: " << remIntBal << endl;  }
				
				// 11. If neither are locked, assign the part to the side with the greatest interface area
				if(  ( remIntSub >= 0.0 )  &&  ( remIntBal >= 0.0 )  ){
					if( SHOWDEBUG ){  cout << "Niether part is locked!" << endl;  }
					// A. If the sub has the greater interface
					if( remIntSub >= remIntBal ){
						addCurrent = true;
					} // else do not add to sub and leave it alone
				// 12. Else one or more are locked
				// 13. If both are locked , Assume that we added something out of order, Failure 
				}else if(  ( remIntSub < 0.0 )  &&  ( remIntBal < 0.0 )  ){
					if( SHOWDEBUG ){  cout << "Both parts locked! Sub failed , Flush queue ..." << endl;  }
					//  A. Flush all parts from sub except the base
					currSub = vec_copy_one_index( currSub , 0 );
					//  B. Flush the queue , This will end the inner loop
					erase_queue( frontier );
				// 14. Else one is locked , assign to the locked side if on the sub side
				}else if( remIntSub < 0.0 ){
					if( SHOWDEBUG ){  cout << "Locked on sub side only, adding..." << endl;  }
					addCurrent = true;
				}else{ 
					// else locked on the balance side, leave it alone
					if( SHOWDEBUG ){  cout << "Locked on balance side only, do NOT add." << endl;  }
				}
				
				// ~ Add part if approved ~
				
				// 15. If adding the current
				//~ if( addCurrent ){
				if( addCurrent ){
					if( SHOWDEBUG ){  cout << "About to ADD the part " << currPartID << " to " << currSub << endl;  }
					// A. Append it to the sub vector
					currSub.push_back( currPartID );
					// B. Fetch neighbors
					IndexSearchResult result = search_vec_for_arg( allParts , currPartID );
					if( result.result ){
						currNghbrDices = liaison_neighbor_indices( reducedLiaison , result.index );
						currNghbrIDs   = original->index_vec_to_partID_vec( currNghbrDices );
						//  B. Enqueue IDs that have not been visited
						//~ enqueue_vec_not_in_set( frontier , currNghbrIDs , visited );
						enqueue_vec_not_in_either_set( frontier , currNghbrIDs , visited , assignedIDs );
					}
				}else{
					if( SHOWDEBUG ){  cout << "Part " << currPartID << " NOT added to " << currSub << endl;  }
				}
			}
		}
		
		// ~ With the frontier now emptied, we should either have a reasonable sub or a single base part, Evaluate in either case ~
		if( SHOWDEBUG ){  cout << endl;  }
		
		// 16. Sub validation: If sub cannot be removed on its own (entire balance intact), it does not belong in this precedence layer
		//                     Flag the sub as a failure
		currBalance = vec_copy_without_elem( allParts , currSub );
		// A. Test for a removal direction
		Eigen::Vector3d remDir = original->asm_center_of_freedom_direction( currSub , currBalance );
		if( SHOWDEBUG ){  
			cout << "About to validate sub with " << currSub.size() << " parts ..." << endl
				 << "Balance comprises " << currBalance.size() << " parts ..." << endl
				 << "Sub: _____________ " << currSub << endl 
				 << "Bal: _____________ " << currBalance << endl 
				 << "Removal Direction: " << remDir << endl;
		}
		if( !is_err( remDir ) ){
			// B. If exists , Validate removal
			/// ### VALIDATE SUB ###
			if( multiValidate ){
				//  I. Validate using multiple directions and methods
				//     i.e. Validate for removal and stability , Using Straightline and RRT*
				if( SHOWDEBUG ){  cout << "About to run validator ..." << endl;  }
				valResult = validate_removal_multimode_w_stability( original , 
																	currSub , currBalance , 
																	params , 
																	remDir );
				if( SHOWDEBUG ){  cout << "Validation completed! , Result: " << yesno( valResult.success ) << endl;  }
				// II. Set flag
				if( SHOWDEBUG ){  cout << "Set flag" << endl;  }
				remSucceed = valResult.success;
			}else{
				remSucceed = original->test_translation_sub_removal( currSub , currBalance , // Test removing 'movedParts' 
																     remDir , params.distance , params.numSamples );
			}
			/// *** END VALIDATE ***
			
			// C. If Success, Append sub to the return structure
			if( remSucceed ){
				rtnSubConstituentPartIDs.push_back( currSub );
				// C.1. Mark these parts as assigned so that they do not get put on two subs!
				set_insert_vec( assignedIDs , currSub );
				if( SHOWDEBUG ){  cout << "Removal success!!" << endl;  }
			}else if( SHOWDEBUG ){  cout << "Failed to remove!" << endl;  }
		}else if( SHOWDEBUG ){  cout << "Failed to find a removal direction!" << endl << endl << endl;  }
	}
	
	/* ~ Now ~ 
	   1. all the bases have been evaluated as nuclei for subs 
	   2. subs formed if appropriate 
	   3. subs added to layer if can be singly removed  */
	
	if( SHOWDEBUG ){  cout << "Found " << rtnSubConstituentPartIDs.size() << " successful subs ..." << endl;  }
	
	// ~ Collect Balance and form into "Leftover Sub" ~
	
	endingBalance = vec_copy_without_elem( allParts , rtnSubConstituentPartIDs ); // This should pick up the base as well
	
	if( SHOWDEBUG ){  cout << "Subs before cleanup: " << endl;  
					  print_vec_vec( rtnSubConstituentPartIDs );  
					  cout << endl << "Remainder IDs: " << endingBalance << endl; }
	
	if( endingBalance.size() > 0 ){ 
	
		// Before we go ahead and add the balance, make sure that it is connected, It could be full of separate rejected subs!
		endBalanceDices = original->NDBG_order_vec_from_Part_ID_vec( endingBalance );
		connBalSubDices = connected_subgraphs_in_subgraph( originlLiaison , endBalanceDices );
		if( SHOWDEBUG ){  cout << "Connected remainder indices:" << endl;  
						  print_vec_vec( connBalSubDices );
						  cout << endl;  }
		// NOTE: We are making the assumption that all of the actual ID'd subs were moved successfully, so it is okay to class the remainder 
		//       chunks as their own subs , and that they furthermore do not interfere with each other
		for( size_t balDex = 0 ; balDex < connBalSubDices.size() ; balDex++ ){
			oneBalSub = original->index_vec_to_partID_vec( connBalSubDices[ balDex ] );
			if( SHOWDEBUG ){  cout << "Adding balance sub: " << oneBalSub << endl;  }  
			rtnSubConstituentPartIDs.push_back( oneBalSub );
		}
	}else if( SHOWDEBUG ){  cout << "There was no remainder to clean!" << endl;  }
	
	if( SHOWDEBUG ){  
		sep( "Final Contents of Precedence Level" );
		print_vec_vec( rtnSubConstituentPartIDs );
		cout << endl;
	}
	
	return rtnSubConstituentPartIDs;
}

AsmStateNode_ASP* Belhadj2017_full_depth( Assembly_ASM* original  , 
										  double alpha , double beta , double gamma , 
										  double initFrac , size_t numIter ,
										  ValidationParams params ){
	AsmStateNode_ASP* rootNode = new AsmStateNode_ASP( original );
	std::queue<AsmStateNode_ASP*> subNodes;
	AsmStateNode_ASP* currNode = nullptr;
	AsmStateNode_ASP* nuNode   = nullptr;
	std::vector<std::vector<llin>> asmClusterLvl1;
	size_t len = 0;
	bool SHOWDEBUG = false;
	
	// 1. Enqueue original (node)
	subNodes.push( rootNode );
	// 2. While there are still assemblies in queue
	while( subNodes.size() > 0 ){
		if( SHOWDEBUG ){  cout << "There are " << subNodes.size() << " nodes to process ..." << endl;  }
		
		// 3. Retrieve and Pop node
		currNode = subNodes.front();  subNodes.pop();
		// 4. If the asm has more than one part
		//~ if( currNode->memParts.size() > 1 ){
		if( currNode->assembly->get_part_ID_vec().size() > 1 ){
			if( SHOWDEBUG ){  cout << "Processing a sub with " << currNode->assembly->get_part_ID_vec().size() << " parts ..." << endl;  }
			// 5. Execute 1st-level clustering
			asmClusterLvl1 = Belhadj2017_one_level( currNode->assembly , 
													alpha , beta , gamma , 
													initFrac , numIter ,
													params );
			if( SHOWDEBUG ){  
				sep( "Sub ID Result" );  
				print_vec_vec( asmClusterLvl1 );
			}
			len = asmClusterLvl1.size();
			if( SHOWDEBUG ){  cout << "There are " << len << " edges to add ..." << endl;  }
			
			if( len > 0 ){ // This should always be true if the asm clustered had more than one part
				// 6. For every parts list produced
				for( size_t i = 0 ; i <  len ; i++ ){
					if( SHOWDEBUG ){  
						cout << "Creating sub " << i + 1 << " of " << len << endl  
							 << "Sub contains " << asmClusterLvl1[i] << endl;  
					}
					// 7. Produce a new assembly , load into Node  &&  8. Enqueue the new assembly
					nuNode = currNode->add_edge( asmClusterLvl1[i] );
					if( SHOWDEBUG ){  
						if( nuNode ){  cout << "Good Pointer!" << endl;  }
						else{  cout << "Bad Pointer!" << endl;  }
					}
					subNodes.push( nuNode );
				}
			}
			if( SHOWDEBUG ){  cout << "After processing level, there are " << subNodes.size() << " in queue" << endl;  }
		}
	}
	return rootNode;
}

/// ___ End Competing ______________________________________________________________________________________________________________________


/// === DISASSEMBLY INTERFERENCE GRAPH (DIG) ===============================================================================================

std::vector<TriMeshVFN_ASP> get_intersection_surfaces( Assembly_ASM* assembly , llin projectPartID , const std::vector<llin>& refcID , 
													   double dRadiusInterval , size_t numSteps ,
													   bool interior ){
	// Get the surfaces that will be intersected with the surrounding parts
	// NOTE: This function returns meshes with coordinates defined in the lab frame
	// NOTE: This function assumes that 'original' and all its parts have been posed and the NDBG calculated and stored in the asm

	bool SHOWDEBUG = false; // if( SHOWDEBUG ){  cout << "" << endl;  }

	std::vector<TriMeshVFN_ASP> meshVec;
	
	//  1. Get the COM of the part
	Pose_ASP /*- */ partPose = assembly->get_lab_pose_w_ID( projectPartID );
	Eigen::Vector3d partCOM  = partPose.position;
	if( SHOWDEBUG ){  cout << "Part Lab Position: " << partCOM << endl;  }
	//  2. Get the constraints & corners the situated part
	std::vector<llin> movdID /* - */ = { projectPartID };
	//~ Eigen::Vector3d   remDir /* - */ = asm_center_of_freedom_direction( movdID , refcID );
	if( SHOWDEBUG ){  cout << "About to calc constraints ..." << endl;  }
	Eigen::MatrixXd   constraints    = assembly->asm_get_constraints_on_movd( movdID , refcID );
	if( SHOWDEBUG ){  cout << "About to calc corners ..." << endl;  }
	Eigen::MatrixXd   corners /*- */ = assembly->asm_spherical_pyramid_corners_of_local_freedom( movdID , refcID );
	if( SHOWDEBUG ){  cout << "About to calc part radius ..." << endl;  }
	double /* ---- */ partRad /*- */ = assembly->get_part_ptr_w_ID( projectPartID )->get_radius() ,
		              height /* - */ = 0.0 ,
					  gridSize /*-*/ = partRad / GRIDDIVISOR_DEFAULT ,
					  shellOffset    = OFFSETNONZERO  ;
	
	double totalRad = partRad ,
		   currFctr = 1.0     ;
	std::vector<double> factorSeq;
	if( SHOWDEBUG ){  cout << "About to calc shell radii ..." << endl;  }
	for( size_t i = 0 ; i < numSteps ; i++ ){
		totalRad += dRadiusInterval;
		currFctr = totalRad / partRad;
		factorSeq.push_back( currFctr );
	}
	
	TriMeshVFN_ASP firstMesh;
	TriMeshVFN_ASP currMesh;
	Eigen::Vector3d axis;
	
	//  3. Determine Case
	
	//  A. Tally the counts that determine case
	size_t numCnstr      = constraints.rows()                 , 
		   numCornr      = corners.rows()                     ,
		   numCrnDir     = count_unique_dirs( corners )       ,
		   numCnstrOppDr = count_opposing_dirs( constraints ) ,
		   numCrnOppDir  = count_opposing_dirs( corners )     ;
		   
	if( SHOWDEBUG ){  sep( "Freedom Case Diagnostics" );
					  cout << "Constraints: " << endl /* -------------- */ << constraints  << endl
						   << "Corners: "     << endl /* -------------- */ << corners      << endl
						   << "Number of Constraints: __________________ " << numCnstr     << endl
						   << "Number of Corners: ______________________ " << numCornr     << endl 
						   << "Number of Corner Directions: ____________ " << numCrnDir    << endl 
						   << "Number of Opposing Constraint Directions: " << numCnstrOppDr << endl
						   << "Number of Opposing Corner Directions: ___ " << numCrnOppDir << endl;  }
		   
	//    I. Free _______________ == 0 constraints , == 0 corners , == 0 opposing cnstrnt dir
	// 		 Expanding sphere , We don't really care about this case
	if( numCnstr == 0 ){
		if( SHOWDEBUG ){  cout << "Computing shells for * Free * Case ..." << endl;  }
		//~ Sphere_d smlSphere{ partRad , partCOM , ICOS_SPHERE_DIVISN };
		Sphere_d smlSphere{ shellOffset , partCOM , ICOS_SPHERE_DIVISN }; // a bit meaningless
		firstMesh = smlSphere.get_mesh();
		meshVec.push_back( firstMesh );
		//~ for( size_t i = 0 ; i < numSteps ; i++ ){
			//~ currMesh = expand_VFN_from_center( firstMesh , factorSeq[i] );
			//~ meshVec.push_back( currMesh );
		//~ }
		return meshVec;
	}
	
	//  VII. Prismatic Bidir ____ >= 3 constraints , >= 6 corners , >= 0 opposing cnstrnt dir , 1 opposing corner dir
	// VIII. Prismatic Unidir ___ >= 4 constraints , >= 4 corners , >= 0 opposing cnstrnt dir , corners point same direction
	//       Translating shadow , subclass determines if meshes are projected in 1 or 2 directions
	if(  ( numCornr >= 3 )  ){
		Eigen::Vector3d zBasis = corners.row(0); // It does not matter which corner we retrieve if they are colinear
		zBasis.normalize();
		Eigen::Vector3d yBasis = get_any_perpendicular( zBasis );
		Eigen::Vector3d xBasis = yBasis.cross( zBasis ).normalized();
		// If all the corners are facing the same direction , Prismatic Unidir , project in one direction
		if( numCrnDir == 1 ){
			if( SHOWDEBUG ){  cout << "Computing shells for * Prismatic Unidir * Case ..." << endl;  }
			// 1. Get shadow
			TriMeshVFN_ASP partMesh = assembly->get_part_ptr_w_ID( projectPartID )->get_lab_VFN();
			TriMeshVFN_ASP shadow = mesh_shadow_along_Z( partMesh , 
														 gridSize ,
														 partCOM ,
														 xBasis , yBasis , zBasis );
			// 2. First shell at the edge of the part
			double frntshift = furthest_extent_from_radius_in_dir( partMesh , zBasis );
			double shiftDist = frntshift;
			firstMesh = shift_VFN_along_axis( shadow , shiftDist );
			meshVec.push_back( firstMesh );
			// 3. Project shells at intervals
			for( size_t i = 1 ; i < numSteps ; i++ ){
				shiftDist += dRadiusInterval;
				currMesh = shift_VFN_along_axis( shadow , shiftDist );
				meshVec.push_back( currMesh );
			}
			
			if( interior ){ // If client code requested interior surfaces , generate
				
				if( SHOWDEBUG ){  cout << "Calc interior shells ..." << endl;  }
				
				// A. Get the distance from the shadow to the "bottom" of the part
				double backshift = furthest_extent_from_radius_in_dir( partMesh , -zBasis );
				double thickness = backshift + frntshift;
				// B. Move the center down the axis to the bottom of the part
				shiftDist = dRadiusInterval;
				Eigen::Vector3d newCen = shadow.center - shadow.axis.normalized() * backshift;
				if( SHOWDEBUG ){  cout << "Old Center: " << shadow.center << endl
					                   << "New Center: " << newCen << endl
					                   << "Thickness:_ " << thickness << endl;  }
				firstMesh = shift_VFN_along_axis( shadow , -backshift + shellOffset );
				firstMesh.center = newCen;
				meshVec.push_back( firstMesh );
				while( shiftDist + shellOffset <= thickness ){
					if( SHOWDEBUG ){  cout << "Shift Distance: " << shiftDist << endl;  }
					currMesh = shift_VFN_along_axis( firstMesh , shiftDist );
					meshVec.push_back( currMesh );
					shiftDist += dRadiusInterval;
				}
			}
			
			return meshVec;
		// else If all the corners point in only 2 directions , Prismatic Bidir , project in both directions
		}else if( numCrnOppDir == 1 ){
			if( SHOWDEBUG ){  cout << "Computing shells for * Prismatic Bidir * Case ..." << endl;  }
			// 1. Get shadow
			TriMeshVFN_ASP partMesh = assembly->get_part_ptr_w_ID( projectPartID )->get_lab_VFN();
			TriMeshVFN_ASP shadow = mesh_shadow_along_Z( partMesh , 
														 gridSize ,
														 partCOM ,
														 xBasis , yBasis , zBasis );
			// 2. First shell at the edge of the part
			double shiftFrwd = furthest_extent_from_radius_in_dir( partMesh ,  zBasis );
			double frntshift = shiftFrwd; // cache
			double shiftBack = furthest_extent_from_radius_in_dir( partMesh , -zBasis );
			double backshift = shiftBack; // cache
			// A. Forward
			firstMesh = shift_VFN_along_axis( shadow , shiftFrwd );
			meshVec.push_back( firstMesh );
			// B. Backward
			firstMesh = shift_VFN_along_axis( shadow , shiftBack );
			meshVec.push_back( firstMesh );
			// 3. Project shells at intervals
			for( size_t i = 1 ; i < numSteps ; i++ ){
				shiftFrwd += dRadiusInterval;
				shiftBack -= dRadiusInterval;
				// A. Forward
				currMesh = shift_VFN_along_axis( shadow , shiftFrwd );
				meshVec.push_back( currMesh );
				// B. Backward
				currMesh = shift_VFN_along_axis( shadow , shiftBack );
				meshVec.push_back( currMesh );
			}
			
			if( interior ){ // If client code requested interior surfaces , generate
				// A. Get the distance from the shadow to the "bottom" of the part
				double thickness = backshift + frntshift;
				// B. Move the center down the axis to the bottom of the part
				double shiftDist = dRadiusInterval;
				Eigen::Vector3d newCen = shadow.center - shadow.axis.normalized() * backshift;
				firstMesh = shift_VFN_along_axis( shadow , -backshift + shellOffset );
				firstMesh.center = newCen;
				meshVec.push_back( firstMesh );
				while( shiftDist + shellOffset <= thickness ){
					currMesh = shift_VFN_along_axis( firstMesh , shiftDist );
					meshVec.push_back( currMesh );
					shiftDist += dRadiusInterval;
				}
			}
			
			return meshVec;
		}
	}
	
	//   II. Half-Space _________ == 1 constraint  , == 0 corners , == 0 opposing cnstrnt dir
	//    V. Dihedral ___________ == 2 constraints , == 2 corners , == 0 opposing cnstrnt dir
	//   VI. Corner _____________ >= 3 constraints , >= 3 corners , == 0 opposing cnstrnt dir
	//       All of these can be represented as a spherical pyramid
	if( numCnstrOppDr == 0 ){
		if( SHOWDEBUG ){  cout << "Computing shells for * Spherical Pyramid * Case ..." << endl;  }
		TriMeshVFN_ASP partMesh = assembly->get_part_ptr_w_ID( projectPartID )->get_lab_VFN();
		size_t cnstrLen = constraints.rows();
		double mostBack = -BILLION_D ,
			   currBack = 0.0        ,
			   mostFrnt = 0.0        ;
		Eigen::Vector3d currStraint;
		Eigen::Vector3d sphereCen = partCOM;
		for( size_t i = 0 ; i < cnstrLen ; i++ ){
			currStraint = constraints.row(i);
			currBack = furthest_extent_from_radius_in_dir( partMesh , -currStraint );
			sphereCen -= currStraint * currBack;
			//~ if( currBack > mostBack ){
				//~ mostBack  = currBack;
				//~ mostFrnt  = furthest_extent_from_radius_in_dir( partMesh , currStraint );
				//~ sphereCen = partCOM - currStraint * mostBack;
			//~ }
		}
		double firstRad = partRad * 1.75;
		Sphere_d smlSphere{ firstRad , sphereCen , ICOS_SPHERE_DIVISN };
		firstMesh = smlSphere.get_surface_w_direction_constraints( constraints );
		meshVec.push_back( firstMesh );
		for( size_t i = 0 ; i < numSteps ; i++ ){
			currMesh = expand_VFN_from_center( firstMesh , factorSeq[i] );
			meshVec.push_back( currMesh );
		}
		
		if( interior ){ // If client code requested interior surfaces , generate
			double radius = shellOffset;
			while( radius < firstRad ){
				smlSphere = Sphere_d( radius , sphereCen , ICOS_SPHERE_DIVISN );
				currMesh = smlSphere.get_surface_w_direction_constraints( constraints );
				meshVec.push_back( currMesh );
				radius += dRadiusInterval;
			}
		}
		
		return meshVec;
	}
	
	//  III. Planar _____________ >= 2 constraints , == 0 corners , == 1 opposing cnstrnt dir
	//   IV. Constrained Planar _ >= 2 constraints , == 4 corners , == 1 opposing cnstrnt dir
	if( numCnstrOppDr == 1 ){
		TriMeshVFN_ASP  partMesh = assembly->get_part_ptr_w_ID( projectPartID )->get_lab_VFN();
		Eigen::MatrixXd opDr     = get_opposing_dirs( constraints ); 
		Eigen::Vector3d axis     = opDr.row(0);
		// 1. Get the extent in the constraint direction
		double /* -- */ extUp    = furthest_extent_from_radius_in_dir( partMesh ,  axis );
		Eigen::Vector3d top      = partCOM + axis.normalized() * extUp;
		double /* -- */ extDn    = furthest_extent_from_radius_in_dir( partMesh , -axis );
		Eigen::Vector3d btm      = partCOM - axis.normalized() * extDn;
		
		double /* -- */ height   = abs( extUp ) + abs( extDn );
		// 2. Construct a ring at the part radius
		
		if( numCornr < 1 ){ // No Corners , Planar
			if( SHOWDEBUG ){  cout << "Computing shells for * Planar * Case ..." << endl;  }
			// A. Pick a new center for the ring
			Eigen::Vector3d ringCen  = ( top + btm ) / 2.0;
			TriMeshVFN_ASP  ringMesh = revolved_segment( ringCen , axis , partRad , height );
			meshVec.push_back( ringMesh );
			// 3. Construct concentric rings
			for( size_t i = 0 ; i < numSteps ; i++ ){
				currMesh = expand_VFN_from_axis( ringMesh , factorSeq[i] );
				meshVec.push_back( currMesh );
			}
			
			if( interior ){ // If client code requested interior surfaces , generate
				double radius = shellOffset;
				while( radius < partRad ){
					currMesh = revolved_segment( ringCen , axis , radius , height );
					meshVec.push_back( currMesh );
					radius += dRadiusInterval;
				}
			}
			
			// 4. Return
			return meshVec;
		}else{ // Some corners , Constrained Planar
			if( SHOWDEBUG ){  cout << "Computing shells for * Constrained Planar * Case ..." << endl;  }
			// A. Move to the extent furthest from the removal direction
			
			Eigen::MatrixXd unopConstr = get_unopposed_dirs( constraints ); 
			size_t cnstrLen = unopConstr.rows();
			double mostBack = -BILLION_D ,
				   currBack = 0.0        ,
				   mostFrnt = 0.0        ;
			Eigen::Vector3d currStraint;
			Eigen::Vector3d ringCen;
			for( size_t i = 0 ; i < cnstrLen ; i++ ){
				currStraint = unopConstr.row(i);
				currBack = furthest_extent_from_radius_in_dir( partMesh , -currStraint );
				if( currBack > mostBack ){
					mostBack = currBack;
					mostFrnt = furthest_extent_from_radius_in_dir( partMesh , currStraint );
					ringCen  = partCOM - currStraint * mostBack;
				}
			}
			TriMeshVFN_ASP  ringMesh = revolved_segment( ringCen , axis , mostBack + mostFrnt , height );
			// 4. Construct the constrained arc
			firstMesh = get_mesh_w_direction_constraints( ringMesh , unopConstr );
			meshVec.push_back( firstMesh );
			// 5. Construct concentric arcs
			for( size_t i = 0 ; i < numSteps ; i++ ){
				currMesh = expand_VFN_from_axis( firstMesh , factorSeq[i] );
				meshVec.push_back( currMesh );
			}
			
			if( interior ){ // If client code requested interior surfaces , generate
				double radius = shellOffset;
				while( radius < mostBack + mostFrnt ){
					ringMesh = revolved_segment( ringCen , axis , radius , height );
					currMesh = get_mesh_w_direction_constraints( ringMesh , unopConstr );
					meshVec.push_back( currMesh );
					radius += dRadiusInterval;
				}
			}
			
			// 6. Return
			return meshVec;
		}
	}
	
	cerr << "get_intersection_surfaces: UNHANDLED FREEDOM CASE!" << endl;
	return meshVec;
}


std::vector<TriMeshVFN_ASP> get_intersection_surfaces( Assembly_ASM* assembly , 
													   const std::vector<llin>& projectPartID , const std::vector<llin>& refcID , 
													   double dRadiusInterval , size_t numSteps ,
													   bool interior ){
	bool SHOWDEBUG = false; // if( SHOWDEBUG ){  cout << "" << endl;  }
	
	if( SHOWDEBUG ){  cout << "Init vars ..." << endl;  }
	
	std::vector<TriMeshVFN_ASP> meshVec;
	TriMeshVFN_ASP firstMesh;
	TriMeshVFN_ASP currMesh;
	
	if( SHOWDEBUG ){  cout << "Get sub mesh ..." << endl;  }
														   
	// 0. Get the sub mesh
	TriMeshVFN_ASP partMesh = assembly->get_lab_VFN( projectPartID );
	partMesh.center = get_average_V( partMesh.V );
	Eigen::Vector3d partCOM = get_average_V( partMesh.V );
	double /* ---- */ partRad /*- */ = V_radius_from_center( partMesh.V , partCOM ) ,
		              height /* - */ = 0.0 ,
					  gridSize /*-*/ = partRad / 10.0 ,
					  shellOffset    = OFFSETNONZERO  ; // 
					  
	double totalRad = partRad ,
		   currFctr = 1.0     ;
	std::vector<double> factorSeq;
	if( SHOWDEBUG ){  cout << "About to calc shell radii ..." << endl;  }
	for( size_t i = 0 ; i < numSteps ; i++ ){
		totalRad += dRadiusInterval;
		currFctr = totalRad / partRad;
		factorSeq.push_back( currFctr );
	}
	if( SHOWDEBUG ){  cout << "Radii Sequence: " << factorSeq << endl;  }
					  
	if( SHOWDEBUG ){  cout << "Get constraint and freedom info ..." << endl;  }
					  
	// 1. Get constraints & corners
	Eigen::MatrixXd constraints = assembly->asm_get_constraints_on_movd( projectPartID , refcID );
	Eigen::MatrixXd corners     = corners_from_constraints( constraints );
	// 2. Get freedom class
	FREEDOM_CLASS freeType = freedom_class_from_constraints( constraints );
	
	if( SHOWDEBUG ){  cout << "Handle freedom cases ..." << endl;  }
	
	// 3. Calc shells according to freedom class
	switch( freeType ){
		
		case ERR_FREEDOM: // If there
			cerr << "get_intersection_surfaces: UNHANDLED FREEDOM CASE!" << endl;
			return meshVec;
			
		case LOCKED:
		    return meshVec;
		    
		case PRISM_UNI:{
			if( SHOWDEBUG ){  cout << "Computing shells for * Prismatic Unidir * Case ..." << endl;  }
			Eigen::Vector3d zBasis = corners.row(0); // It does not matter which corner we retrieve if they are colinear
			zBasis.normalize();
			Eigen::Vector3d yBasis = get_any_perpendicular( zBasis );
			Eigen::Vector3d xBasis = yBasis.cross( zBasis ).normalized();
			// 1. Get shadow
			//~ TriMeshVFN_ASP partMesh = assembly->get_part_ptr_w_ID( projectPartID )->get_lab_VFN();
			TriMeshVFN_ASP shadow = mesh_shadow_along_Z( partMesh , 
														 gridSize ,
														 partCOM ,
														 xBasis , yBasis , zBasis );
			// 2. First shell at the edge of the part
			double frntshift = furthest_extent_from_radius_in_dir( partMesh , zBasis );
			double shiftDist = frntshift;
			if( SHOWDEBUG ){  cout << "Shell AABB: ___ " << endl << AABB( partMesh ) << endl 
								   << "Shift Distance: " << shiftDist << endl;  }
			firstMesh = shift_VFN_along_axis( shadow , shiftDist );
			meshVec.push_back( firstMesh );
			// 3. Project shells at intervals
			for( size_t i = 1 ; i < numSteps ; i++ ){
				shiftDist += dRadiusInterval;
				if( SHOWDEBUG ){  cout << "Shift Distance: " << shiftDist << endl;  }
				currMesh = shift_VFN_along_axis( shadow , shiftDist );
				meshVec.push_back( currMesh );
			}
			
			if( interior ){ // If client code requested interior surfaces , generate
				
				if( SHOWDEBUG ){  cout << "Calc interior shells ..." << endl;  }
				
				// A. Get the distance from the shadow to the "bottom" of the part
				double backshift = furthest_extent_from_radius_in_dir( partMesh , -zBasis );
				double thickness = backshift + frntshift;
				// B. Move the center down the axis to the bottom of the part
				shiftDist = dRadiusInterval;
				Eigen::Vector3d newCen = shadow.center - shadow.axis.normalized() * backshift;
				if( SHOWDEBUG ){  cout << "Old Center: " << shadow.center << endl
					                   << "New Center: " << newCen << endl
					                   << "Thickness:_ " << thickness << endl;  }
				firstMesh = shift_VFN_along_axis( shadow , -backshift + shellOffset );
				firstMesh.center = newCen;
				meshVec.push_back( firstMesh );
				while( shiftDist + shellOffset <= thickness ){
					if( SHOWDEBUG ){  cout << "Shift Distance: " << shiftDist << endl;  }
					currMesh = shift_VFN_along_axis( firstMesh , shiftDist );
					meshVec.push_back( currMesh );
					shiftDist += dRadiusInterval;
				}
			}
			
			return meshVec;
		}
		
		case PRISM_BIDIR:{
			if( SHOWDEBUG ){  cout << "Computing shells for * Prismatic Bidir * Case ..." << endl;  }
			Eigen::Vector3d zBasis = corners.row(0); // It does not matter which corner we retrieve if they are colinear
			zBasis.normalize();
			Eigen::Vector3d yBasis = get_any_perpendicular( zBasis );
			Eigen::Vector3d xBasis = yBasis.cross( zBasis ).normalized();
			// 1. Get shadow
			//~ TriMeshVFN_ASP partMesh = assembly->get_part_ptr_w_ID( projectPartID )->get_lab_VFN();
			TriMeshVFN_ASP shadow = mesh_shadow_along_Z( partMesh , 
														 gridSize ,
														 partCOM ,
														 xBasis , yBasis , zBasis );
			// 2. First shell at the edge of the part
			double shiftFrwd = furthest_extent_from_radius_in_dir( partMesh ,  zBasis );
			double frntshift = shiftFrwd; // cache
			double shiftBack = furthest_extent_from_radius_in_dir( partMesh , -zBasis );
			double backshift = shiftBack; // cache
			// A. Forward
			firstMesh = shift_VFN_along_axis( shadow , shiftFrwd );
			meshVec.push_back( firstMesh );
			// B. Backward
			firstMesh = shift_VFN_along_axis( shadow , shiftBack );
			meshVec.push_back( firstMesh );
			// 3. Project shells at intervals
			for( size_t i = 1 ; i < numSteps ; i++ ){
				shiftFrwd += dRadiusInterval;
				shiftBack -= dRadiusInterval;
				// A. Forward
				currMesh = shift_VFN_along_axis( shadow , shiftFrwd );
				meshVec.push_back( currMesh );
				// B. Backward
				currMesh = shift_VFN_along_axis( shadow , shiftBack );
				meshVec.push_back( currMesh );
			}
			
			if( interior ){ // If client code requested interior surfaces , generate
				// A. Get the distance from the shadow to the "bottom" of the part
				double thickness = backshift + frntshift;
				// B. Move the center down the axis to the bottom of the part
				double shiftDist = dRadiusInterval;
				Eigen::Vector3d newCen = shadow.center - shadow.axis.normalized() * backshift;
				firstMesh = shift_VFN_along_axis( shadow , -backshift + shellOffset );
				firstMesh.center = newCen;
				meshVec.push_back( firstMesh );
				while( shiftDist + shellOffset <= thickness ){
					currMesh = shift_VFN_along_axis( firstMesh , shiftDist );
					meshVec.push_back( currMesh );
					shiftDist += dRadiusInterval;
				}
			}
			
			return meshVec;
		}
			
		case PLANAR_CONSTR:{
			//~ TriMeshVFN_ASP  partMesh = assembly->get_part_ptr_w_ID( projectPartID )->get_lab_VFN();
			Eigen::MatrixXd opDr     = get_opposing_dirs( constraints ); 
			Eigen::Vector3d axis     = opDr.row(0);
			// 1. Get the extent in the constraint direction
			double /* -- */ extUp    = furthest_extent_from_radius_in_dir( partMesh ,  axis );
			Eigen::Vector3d top      = partCOM + axis.normalized() * extUp;
			double /* -- */ extDn    = furthest_extent_from_radius_in_dir( partMesh , -axis );
			Eigen::Vector3d btm      = partCOM - axis.normalized() * extDn;
			double /* -- */ height   = abs( extUp ) + abs( extDn );
			// A. Move to the extent furthest from the removal direction
			
			Eigen::MatrixXd unopConstr = get_unopposed_dirs( constraints ); 
			size_t cnstrLen = unopConstr.rows();
			double mostBack = -BILLION_D ,
				   currBack = 0.0        ,
				   mostFrnt = 0.0        ;
			Eigen::Vector3d currStraint;
			Eigen::Vector3d ringCen;
			for( size_t i = 0 ; i < cnstrLen ; i++ ){
				currStraint = unopConstr.row(i);
				currBack = furthest_extent_from_radius_in_dir( partMesh , -currStraint );
				if( currBack > mostBack ){
					mostBack = currBack;
					mostFrnt = furthest_extent_from_radius_in_dir( partMesh , currStraint );
					ringCen  = partCOM - currStraint * mostBack;
				}
			}
			TriMeshVFN_ASP  ringMesh = revolved_segment( ringCen , axis , mostBack + mostFrnt , height );
			// 4. Construct the constrained arc
			firstMesh = get_mesh_w_direction_constraints( ringMesh , unopConstr );
			meshVec.push_back( firstMesh );
			// 5. Construct concentric arcs
			for( size_t i = 0 ; i < numSteps ; i++ ){
				currMesh = expand_VFN_from_axis( firstMesh , factorSeq[i] );
				meshVec.push_back( currMesh );
			}
			
			if( interior ){ // If client code requested interior surfaces , generate
				double radius = shellOffset;
				while( radius < mostBack + mostFrnt ){
					ringMesh = revolved_segment( ringCen , axis , radius , height );
					currMesh = get_mesh_w_direction_constraints( ringMesh , unopConstr );
					meshVec.push_back( currMesh );
					radius += dRadiusInterval;
				}
			}
			
			// 6. Return
			return meshVec;
		}
			
		case PLANAR_FREE:{
			Eigen::MatrixXd opDr     = get_opposing_dirs( constraints ); 
			Eigen::Vector3d axis     = opDr.row(0);
			// 1. Get the extent in the constraint direction
			double /* -- */ extUp    = furthest_extent_from_radius_in_dir( partMesh ,  axis );
			Eigen::Vector3d top      = partCOM + axis.normalized() * extUp;
			double /* -- */ extDn    = furthest_extent_from_radius_in_dir( partMesh , -axis );
			Eigen::Vector3d btm      = partCOM - axis.normalized() * extDn;
			double /* -- */ height   = abs( extUp ) + abs( extDn );
			if( SHOWDEBUG ){  cout << "Computing shells for * Planar * Case ..." << endl;  }
			// A. Pick a new center for the ring
			Eigen::Vector3d ringCen  = ( top + btm ) / 2.0;
			TriMeshVFN_ASP  ringMesh = revolved_segment( ringCen , axis , partRad , height );
			meshVec.push_back( ringMesh );
			// 3. Construct concentric rings
			for( size_t i = 0 ; i < numSteps ; i++ ){
				currMesh = expand_VFN_from_axis( ringMesh , factorSeq[i] );
				meshVec.push_back( currMesh );
			}
			
			if( interior ){ // If client code requested interior surfaces , generate
				double radius = shellOffset;
				while( radius < partRad ){
					currMesh = revolved_segment( ringCen , axis , radius , height );
					meshVec.push_back( currMesh );
					radius += dRadiusInterval;
				}
			}
			
			// 4. Return
			return meshVec;
		}
		
		case SPHERE_PYRAMID:
		case DIHEDRAL:
		case HALF_SPACE:{
			if( SHOWDEBUG ){  cout << "Computing shells for * Spherical Pyramid * Case ..." << endl;  }
			//~ TriMeshVFN_ASP partMesh = assembly->get_part_ptr_w_ID( projectPartID )->get_lab_VFN();
			size_t cnstrLen = constraints.rows();
			double mostBack = -BILLION_D ,
				   currBack = 0.0        ,
				   mostFrnt = 0.0        ;
			Eigen::Vector3d currStraint;
			Eigen::Vector3d sphereCen = partCOM;
			for( size_t i = 0 ; i < cnstrLen ; i++ ){
				currStraint = constraints.row(i);
				currBack = furthest_extent_from_radius_in_dir( partMesh , -currStraint );
				sphereCen -= currStraint * currBack;
				//~ if( currBack > mostBack ){
					//~ mostBack  = currBack;
					//~ mostFrnt  = furthest_extent_from_radius_in_dir( partMesh , currStraint );
					//~ sphereCen = partCOM - currStraint * mostBack;
				//~ }
			}
			
			double firstRad = partRad * 1.75;
			
			Sphere_d smlSphere{ firstRad , sphereCen , ICOS_SPHERE_DIVISN };
			firstMesh = smlSphere.get_surface_w_direction_constraints( constraints );
			meshVec.push_back( firstMesh );
			for( size_t i = 0 ; i < numSteps ; i++ ){
				currMesh = expand_VFN_from_center( firstMesh , factorSeq[i] );
				meshVec.push_back( currMesh );
			}
			return meshVec;
			
			if( interior ){ // If client code requested interior surfaces , generate
				double radius = shellOffset;
				while( radius < firstRad ){
					smlSphere = Sphere_d( radius , sphereCen , ICOS_SPHERE_DIVISN );
					currMesh = smlSphere.get_surface_w_direction_constraints( constraints );
					meshVec.push_back( currMesh );
					radius += dRadiusInterval;
				}
			}
			
		}
			
		case FREE_SPHERE:{
			if( SHOWDEBUG ){  cout << "Computing shells for * Free * Case ..." << endl;  }
			//~ Sphere_d smlSphere{ partRad , partCOM , ICOS_SPHERE_DIVISN };
			Sphere_d smlSphere{ shellOffset , partCOM , ICOS_SPHERE_DIVISN }; // Somewhat meaningless but would like to avoid transform errors
			firstMesh = smlSphere.get_mesh();
			meshVec.push_back( firstMesh );
			//~ for( size_t i = 0 ; i < numSteps ; i++ ){
				//~ currMesh = expand_VFN_from_center( firstMesh , factorSeq[i] );
				//~ meshVec.push_back( currMesh );
			//~ }
			return meshVec;
		}
		
		default:
			cerr << "get_intersection_surfaces: UNHANDLED FREEDOM CASE!" << endl;
	}
}

std::vector<TriMeshVFN_ASP> get_intersection_surfaces( Assembly_ASM* assembly , llin projectPartID , 
													   double dRadiusInterval , size_t numSteps ,
													   bool interior ){
	std::vector<llin> refcID /* - */ = vec_copy_without_elem( assembly->get_part_ID_vec() , projectPartID );
	return get_intersection_surfaces( assembly , projectPartID , refcID , dRadiusInterval , numSteps , interior );
}

std::vector<CollisionVFN_ASP*> prep_meshes_for_collision( const std::vector<TriMeshVFN_ASP>& meshSeq ){
	// Return pointers to collision meshes corresponding to 'meshSeq'
	std::vector<CollisionVFN_ASP*> rntVec;
	CollisionVFN_ASP* currMesh;
	size_t len = meshSeq.size();
	for( size_t i = 0 ; i < len ; i++ ){  
		currMesh = new CollisionVFN_ASP( meshSeq[i] );
		rntVec.push_back( currMesh );
	}
	return rntVec;
}

// Return the fraction of 'CollisionVFN_ASP' that intersects 'mesh'
double collision_fraction( CollisionVFN_ASP* shell , const TriMeshVFN_ASP& mesh ){
	// Return the fraction of 'CollisionVFN_ASP' that intersects 'part'
	/* There are 3 types of shells: { SPHERICAL , REVOLUTE , SHADOW }
	   Collision checking proceeds in 2 passes: 1) Mesh collision, and 2) Ray Casting */
	// NOTE: This function assumes that each triangle of 'shell' is roughly the same size
	
	bool SHOWDEBUG = false , 
		 TRIDETAIL = false ; // if( SHOWDEBUG ){  cout << "" << endl;  }
	
	// 1. Count triangles
	int numTri = (int) shell->mesh.F.rows();
	if( SHOWDEBUG ){  cout << "Shell has " << numTri << " triangles" << endl;  }
	CollisionVFN_ASP partMesh{ mesh };

	//~ RAPID_Geo& partCollsn = part->get_collsn_geo_ref();
	RAPID_Geo& partCollsn = partMesh.collsn_geo;

	if( SHOWDEBUG ){  cout << "Part has " << partMesh.mesh.F.rows() << " triangles" << endl;  }
	// 1) Mesh collision
	std::set<int> collTri = shell->collsn_geo.collision_F( partCollsn );
	int collsnNumTri = (int) collTri.size();
	
	if( SHOWDEBUG ){  
		if( TRIDETAIL ){
			cout << "Shell collision model has: " << shell->collsn_geo.numTris << " triangles" << endl
				 << "Shell Transform , T: " << endl << matx_1x3_to_string( shell->collsn_geo.T ) << " R: " << endl 
				 << matx_3x3_to_string( shell->collsn_geo.R ) << endl 
				 << "Part collision model has:_ " << partCollsn.numTris << " triangles" << endl
				 << "Part Transform , T: " << endl << matx_1x3_to_string( partCollsn.T ) << " R: " << endl 
				 << matx_3x3_to_string( partCollsn.R ) << endl;
		}
		cout << "Mesh collision: " << collsnNumTri << " triangles already in collision" << endl
			 << "Collision with other method?: " << shell->collsn_geo.collides_with( partCollsn ) << endl;  
	}
	
	// 2) Ray Casting
	bool /* ---- */ skipTri = false;
	Eigen::Vector3d rayOrg;
	Eigen::Vector3d rayDir;
	Eigen::Vector3d triCen;
	Eigen::Vector3d center = shell->mesh.center;
	Eigen::Vector3d axis   = shell->mesh.axis;
	Eigen::MatrixXd V      = shell->mesh.V;
	Eigen::MatrixXi F      = shell->mesh.F;
	double /* -- */ radius = 0.0;
	RayHits result;
	double  resDst = 0.0;
	for( int i = 0 ; i < numTri ; i++ ){
		skipTri = is_arg_in_set( i , collTri );
		if( !skipTri ){
			triCen = get_average_V( get_triangle_i( V , F , i ) );
			if( SHOWDEBUG ){  cout << "\tTriangle " << i+1 << " with center " << triCen << endl;  }
			switch( shell->mesh.type ){
				case SPHERICAL:
					rayOrg = center;
					break;
				case REVOLUTE:
					rayOrg = pnt_proj_onto_ray( triCen , center , axis );
					break;
				case SHADOW:
					rayOrg = pnt_proj_to_plane( triCen , center , axis );
					break;
				default:
					cerr << "collision_fraction: UNHANDLED SHELL TYPE!" << endl;
			}
			rayDir = ( triCen - rayOrg );
			radius = rayDir.norm();
			if( SHOWDEBUG ){  cout << "\t\tradius: " << radius << endl;  }  
			rayDir = rayDir.normalized();
			result = ray_intersect_CollsnVFN( rayOrg , rayDir , partMesh );
			if( SHOWDEBUG ){  cout << "\t\trayOrg: " << rayOrg << endl
								   << "\t\trayDir: " << rayDir << endl
								   << "\t\tradius: " << radius << endl;  }  
			if( result.anyHits ){
				if( SHOWDEBUG ){  cout << "\t\tHits?:_ YES!" << endl; }
				resDst = least_dist_from_pnt_to_hit( rayOrg , result );
				if( SHOWDEBUG ){  cout << "\t\tDist?:_ " << resDst << endl; }
				if( resDst <= radius ){  collsnNumTri++;  }
			}else if( SHOWDEBUG ){  cout << "\t\tHits?:_ NO!" << endl; }
		}
	}
	if( SHOWDEBUG ){ cout << "After raycasting: " << collsnNumTri << " triangles in collision" << endl;
					 cout << endl << endl;  }
	return (double) collsnNumTri / numTri;
}

double collision_fraction( CollisionVFN_ASP* shell , Part_ASM* part ){
	// Return the fraction of 'CollisionVFN_ASP' that intersects 'part'
	/* There are 3 types of shells: { SPHERICAL , REVOLUTE , SHADOW }
	   Collision checking proceeds in 2 passes: 1) Mesh collision, and 2) Ray Casting */
	// NOTE: This function assumes that each triangle of 'shell' is roughly the same size
	return collision_fraction( shell , part->get_lab_VFN() );
}

std::vector<std::vector<double>> DIG_from_asm_V1( Assembly_ASM* assembly , double dRadiusInterval , size_t numSteps ){
	/* Disassembly Interference Graph V1: Every mover vs every obstruction with all parts in place */
	
	bool SHOWDEBUG = false; // if( SHOWDEBUG ){  cout << "" << endl;  }
	
	if( SHOWDEBUG ){  cout << "Init vars ..." << endl;  }
	
	std::vector<std::vector<double>> DIG; // ------------------- Disassembly Intereference Graph
	std::vector<size_t> /* ------ */ movd; // ------------------ Indices of the moved parts
	std::vector<size_t> /* ------ */ refc; // ------------------ Indices of the reference parts
	Eigen::Vector3d /* ---------- */ remDir; // ---------------- Suggested removal direction
	size_t /* ------------------- */ numShells = 0; // --------- Number of interference shells for this local freedom state
	double /* ------------------- */ mostFrac  = -BILLION_D , // Greatest fraction of interference for a single part->part directed pair
									 currFrac  = 0.0        ; // Fraction of interference for a part->shell pair
	Part_ASM* /* ---------------- */ currPart  = nullptr; // --- Pointer to interfering part
	std::vector<TriMeshVFN_ASP>      projShells; // ------------ Interference shells for a single freedom state
	std::vector<CollisionVFN_ASP*>   collsnShells; // ---------- Collision shells for a single freedom state
	//  1. Get the number of parts
	size_t /* ------ */ numParts = assembly->how_many_parts();
	std::vector<size_t> indices  = vec_range( (size_t)0 , numParts-1 );
	std::vector<llin>   partIDs  = assembly->get_part_ID_vec();
	//  2. Get the liaison graph
	std::vector<std::vector<bool>> liaisonG = assembly->get_liaison_graph();
	
	
	//  3. For each mover
	for( size_t i = 0 ; i < numParts ; i++ ){
		if( SHOWDEBUG ){  cout << "Mover " << i+1 << " of " << numParts << endl;  }
		//  4. Get the mover freedom
		movd.clear();  movd.push_back( i );           if( SHOWDEBUG ){  cout << "Moved: ___ " << movd << endl;  }
		refc = vec_copy_without_elem( indices , i );  if( SHOWDEBUG ){  cout << "Reference: " << refc << endl;  }
		remDir = assembly->asm_center_of_freedom_direction( movd , refc ); // NDBG order
		if( SHOWDEBUG ){  cout << "Suggested Direction: " << remDir << endl;  }
		//  5. If the mover freedom is empty, give 1 to all other parts
		if( is_err( remDir ) ){
			std::vector<double> temp;
			for( size_t j = 0 ; j < numParts ; j++ ){  temp.push_back(  ( i!=j ? 1.0 : 0.0 )  );  }
			DIG.push_back( temp );
		}else{
		//  6. If the mover freedom is non-empty, then get shells
			projShells   = get_intersection_surfaces( assembly , partIDs[i] , dRadiusInterval , numSteps );
			collsnShells = prep_meshes_for_collision( projShells );
			numShells = collsnShells.size();
			std::vector<double> temp;
			//  7. For every unilateral pairing of parts: mover --to-> obstacle , i != j
			for( size_t j = 0 ; j < numParts ; j++ ){
				if( SHOWDEBUG ){  cout << "\tObstruction " << j+1 << " of " << numParts << endl;  }
				if( i != j ){
					currPart = assembly->get_part_ptr_w_ID( partIDs[j] );
					//  8. For each shell
					mostFrac = -BILLION_D;
					for( size_t k = 0 ; k < numShells ; k++ ){
						//  A. Evaluate shell for collision fraction
						currFrac = collision_fraction( collsnShells[k] , currPart );
						//  9. Assign the greatest fraction to the obstacle
						mostFrac = max( mostFrac , currFrac );
					}
					temp.push_back( mostFrac );
				}else{  temp.push_back( 0.0 );  }
			}
			DIG.push_back( temp );
		}
	}
	if( SHOWDEBUG ){  cout << "Returning ..." << endl;  }
	// 10. Return DIG
	return DIG;
}

std::vector<std::vector<double>> DIG_from_asm_V2( Assembly_ASM* assembly , double dRadiusInterval , size_t numSteps , 
												  double stuckInfluenceFactor ){
	/* Disassembly Interference Graph V2: Every mover vs every obstruction with absent obstruction freedom , Limited range for stuck parts */
	
	bool SHOWDEBUG = false; // if( SHOWDEBUG ){  cout << "" << endl;  }
	
	if( SHOWDEBUG ){  cout << "Init vars ..." << endl;  }
	
	std::vector<std::vector<double>> DIG; // ------------------- Disassembly Intereference Graph
	std::vector<size_t> /* ------ */ movd; // ------------------ Indices of moved parts
	std::vector<size_t> /* ------ */ xcld; // ------------------ Indices of { moved , interfering } parts
	std::vector<size_t> /* ------ */ refc; // ------------------ Indices of reference parts
	Eigen::Vector3d /* ---------- */ remDir; // ---------------- Suggested removal direction
	size_t /* ------------------- */ numShells =  0; // -------- Number of interference shells for this local freedom state
	double /* ------------------- */ mostFrac  = -BILLION_D , // Greatest fraction of interference for a single part->part directed pair
									 currFrac  =  0.0       , // Fraction of interference for a part->shell pair
									 mxPrtRad  =  0.0       ; // Radius the region of influence of a trapped part , measured from that part's COM
	Part_ASM* /* ---------------- */ currPart  = nullptr; // --- Pointer to interfering part
	Part_ASM* /* ---------------- */ movePart  = nullptr; // --- Pointer to moved       part
	Pose_ASP /* ----------------- */ crPtPose; // -------------- Pose of the interfering part
	Pose_ASP /* ----------------- */ mvPtPose; // -------------- Pose of the moved       part
	std::vector<TriMeshVFN_ASP>      projShells; // ------------ Interference shells for a single freedom state
	std::vector<CollisionVFN_ASP*>   collsnShells; // ---------- Collision shells for a single freedom state
	Sphere_d* /* ---------------- */ trappedSphere = nullptr; // Sphere modeling the region of influence of a trapped part
	CollisionVFN_ASP* /* -------- */ sphereMesh = nullptr; // -- Collision geometry for 'trappedSphere'
	//  1. Get the number of parts
	size_t /* ------ */ numParts = assembly->how_many_parts();
	std::vector<size_t> indices  = vec_range( (size_t)0 , numParts-1 );
	std::vector<llin>   partIDs  = assembly->get_part_ID_vec();
	//  2. Get the liaison graph
	std::vector<std::vector<bool>> liaisonG = assembly->get_liaison_graph();
	//  3. For every unilateral pairing of parts: mover --to-> obstacle , i != j
	for( size_t i = 0 ; i < numParts ; i++ ){
		if( SHOWDEBUG ){  cout << "Mover " << i+1 << " of " << numParts << endl;  }
		
		std::vector<double> temp;
		if( SHOWDEBUG ){  cout << "About to fetch " << partIDs[i] << " ..." << endl;  }
		movePart = assembly->get_part_ptr_w_ID( partIDs[i] );          if( SHOWDEBUG ){  cout << "Fetch Part: _ " << movePart->get_ID() << endl;  }
		mxPrtRad = ( movePart->get_radius() ) * stuckInfluenceFactor;  if( SHOWDEBUG ){  cout << "Stuck Radius: " << mxPrtRad << endl;  }
		mvPtPose = movePart->get_pose();							   if( SHOWDEBUG ){  cout << "Mover Pose: _ " << mvPtPose << endl;  }
		if( SHOWDEBUG ){  cout << "About to build sphere ..." << endl;  }
		delif( trappedSphere );
		trappedSphere = new Sphere_d( mxPrtRad , mvPtPose.position );
		delif( sphereMesh );
		if( SHOWDEBUG ){  cout << "About to build sphere collision mesh ..." << endl;  }
		sphereMesh = new CollisionVFN_ASP( trappedSphere->get_mesh() );  
		
		for( size_t j = 0 ; j < numParts ; j++ ){
			if( SHOWDEBUG ){  cout << "\tObstruction " << j+1 << " of " << numParts << endl;  }
			if( i != j ){
				currPart = assembly->get_part_ptr_w_ID( partIDs[j] );
				crPtPose = currPart->get_pose();
				//  4. Get the mover freedom with the obstruction absent
				movd.clear();  movd.push_back( i );
				xcld.clear();  xcld.push_back( i );  xcld.push_back( j );
				refc = vec_copy_without_elem( indices , xcld );
				remDir = assembly->asm_center_of_freedom_direction( movd , refc ); // NDBG order
				if( SHOWDEBUG ){  cout << "Suggested Direction: " << remDir << endl;  }
				//  5. If the mover freedom is empty, give 1 to all other parts within radius
				if( is_err( remDir ) ){
					if( SHOWDEBUG ){  cout << "Mover immobile .." << endl;  }
					if( collision_fraction( sphereMesh , currPart ) > 0.01 ){  
						temp.push_back( 1.0 );  if( SHOWDEBUG ){  cout << "Part is close, place blame" << endl;  }
					}else{  
						temp.push_back( 0.0 );  if( SHOWDEBUG ){  cout << "Part is far, not an issue" << endl;  }
					}
				}else{
				//  6. If the mover freedom is non-empty, then get shells
					if( SHOWDEBUG ){  cout << "Calculating shells ..." << endl;  }
					projShells   = get_intersection_surfaces( assembly , partIDs[i] , 
															  assembly->index_vec_to_partID_vec( refc ) , 
															  dRadiusInterval , numSteps );
					clearif( collsnShells );
					collsnShells = prep_meshes_for_collision( projShells );
					numShells = collsnShells.size();
					//  7. Evaluate each shell for collision fraction
					mostFrac = -BILLION_D;
					for( size_t k = 0 ; k < numShells ; k++ ){
						if( SHOWDEBUG ){  cout << "Shell " << k+1 << " of " << numShells << endl;  }
						//  A. Evaluate shell for collision fraction
						currFrac = collision_fraction( collsnShells[k] , currPart );
						//  9. Assign the greatest fraction to the obstacle
						mostFrac = max( mostFrac , currFrac );
					}
					temp.push_back( mostFrac );
				}
				//  8. Assign the greatest fraction to the obstacle
			}else{  temp.push_back( 0.0 );  }
		}
		DIG.push_back( temp );
	}
	delif( sphereMesh );
	if( SHOWDEBUG ){  cout << "Returning ..." << endl;  }
	//  9. Return DIG
	return DIG;
}

std::vector<std::vector<double>> DIG_from_asm_V3( Assembly_ASM* assembly , double dRadiusInterval , size_t numSteps ,
												  const std::vector<llin>& limitedRefc ,
												  double stuckInfluenceFactor ){
	/* Disassembly Interference Graph V3: Every mover vs every obstruction considering limited constraints for shell computation */
	
	bool SHOWDEBUG = false; // if( SHOWDEBUG ){  cout << "" << endl;  }
	
	if( SHOWDEBUG ){  cout << "Init vars ..." << endl;  }
	
	std::vector<std::vector<double>> DIG; // ------------------- Disassembly Intereference Graph
	std::vector<size_t> /* ------ */ movd; // ------------------ Indices of the moved parts
	std::vector<llin> /* -------- */ mover; // ----------------- IDs of the moved parts
	std::vector<size_t> /* ------ */ refc; // ------------------ Indices of the reference parts
	Eigen::Vector3d /* ---------- */ remDir; // ---------------- Suggested removal direction
	size_t /* ------------------- */ numShells =  0; // -------- Number of interference shells for this local freedom state
	double /* ------------------- */ mostFrac  = -BILLION_D , // Greatest fraction of interference for a single part->part directed pair
									 currFrac  =  0.0       , // Fraction of interference for a part->shell pair
									 mxPrtRad  =  0.0       ; // Radius the region of influence of a trapped part , measured from that part's COM
	Part_ASM* /* ---------------- */ currPart  = nullptr; // --- Pointer to interfering part
	Part_ASM* /* ---------------- */ movePart  = nullptr; // --- Pointer to moved       part
	Pose_ASP /* ----------------- */ mvPtPose; // -------------- Pose of the moved       part
	std::vector<TriMeshVFN_ASP>      projShells; // ------------ Interference shells for a single freedom state
	std::vector<CollisionVFN_ASP*>   collsnShells; // ---------- Collision shells for a single freedom state
	Sphere_d* /* ---------------- */ trappedSphere = nullptr; // Sphere modeling the region of influence of a trapped part
	CollisionVFN_ASP* /* -------- */ sphereMesh = nullptr; // -- Collision geometry for 'trappedSphere'
	//  1. Get the number of parts
	size_t /* ------ */ numParts    = assembly->how_many_parts();
	std::vector<size_t> indices     = vec_range( (size_t)0 , numParts-1 );
	std::vector<llin>   partIDs     = assembly->get_part_ID_vec();
	std::vector<size_t> constrDices = assembly->partID_vec_to_index_vec( limitedRefc );
	//  2. Get the liaison graph
	std::vector<std::vector<bool>> liaisonG = assembly->get_liaison_graph();
	
	
	//  3. For each mover
	for( size_t i = 0 ; i < numParts ; i++ ){
		if( SHOWDEBUG ){  cout << "Mover " << i+1 << " of " << numParts << endl;  }
		//  4. Get the mover freedom
		movd.clear();  movd.push_back( i );           if( SHOWDEBUG ){  cout << "Moved: ___ " << movd << endl;  }
		
		//~ refc = vec_copy_without_elem( indices , i );  if( SHOWDEBUG ){  cout << "Reference: " << refc << endl;  }
		refc = constrDices; // Constraints are provided only by the specified parts
		
		remDir = assembly->asm_center_of_freedom_direction( movd , refc ); // NDBG order
		if( SHOWDEBUG ){  cout << "Suggested Direction: " << remDir << endl;  }
		
		//  5. If the mover freedom is empty, calc collision fractions for a spherical region
		if( is_err( remDir ) ){
			
			if( SHOWDEBUG ){  cout << "About to fetch " << partIDs[i] << " ..." << endl;  }
			movePart = assembly->get_part_ptr_w_ID( partIDs[i] );          if( SHOWDEBUG ){  cout << "Fetch Part: _ " << movePart->get_ID() << endl;  }
			mxPrtRad = ( movePart->get_radius() ) * stuckInfluenceFactor;  if( SHOWDEBUG ){  cout << "Stuck Radius: " << mxPrtRad << endl;  }
			mvPtPose = movePart->get_pose();							   if( SHOWDEBUG ){  cout << "Mover Pose: _ " << mvPtPose << endl;  }
			
			if( SHOWDEBUG ){  cout << "About to build sphere ..." << endl;  }
			delif( trappedSphere );
			trappedSphere = new Sphere_d( mxPrtRad , mvPtPose.position );
			delif( sphereMesh );
			if( SHOWDEBUG ){  cout << "About to build sphere collision mesh ..." << endl;  }
			sphereMesh = new CollisionVFN_ASP( trappedSphere->get_mesh() );
			
			std::vector<double> temp;
			for( size_t j = 0 ; j < numParts ; j++ ){  
				if( i != j ){
					currPart = assembly->get_part_ptr_w_ID( partIDs[j] );
					currFrac = collision_fraction( sphereMesh , currPart );
					temp.push_back( currFrac );
				}else{  temp.push_back( 0.0 );  }
			}
			DIG.push_back( temp );
			
		}else{
		//  6. If the mover freedom is non-empty, then get shells
		
			//~ projShells   = get_intersection_surfaces( assembly , partIDs[i] , dRadiusInterval , numSteps );
			mover      = { partIDs[i] };
			projShells = get_intersection_surfaces( assembly , 
													mover , limitedRefc , 
													dRadiusInterval , numSteps );
			
			collsnShells = prep_meshes_for_collision( projShells );
			numShells    = collsnShells.size();
			std::vector<double> temp;
			//  7. For every unilateral pairing of parts: mover --to-> obstacle , i != j
			for( size_t j = 0 ; j < numParts ; j++ ){
				if( SHOWDEBUG ){  cout << "\tObstruction " << j+1 << " of " << numParts << endl;  }
				if( i != j ){
					currPart = assembly->get_part_ptr_w_ID( partIDs[j] );
					//  8. For each shell
					mostFrac = -BILLION_D;
					for( size_t k = 0 ; k < numShells ; k++ ){
						//  A. Evaluate shell for collision fraction
						currFrac = collision_fraction( collsnShells[k] , currPart );
						//  9. Assign the greatest fraction to the obstacle
						mostFrac = max( mostFrac , currFrac );
					}
					temp.push_back( mostFrac );
				}else{  temp.push_back( 0.0 );  }
			}
			DIG.push_back( temp );
		}
	}
	if( SHOWDEBUG ){  cout << "Returning ..." << endl;  }
	// 10. Return DIG
	return DIG;
}

double assembled_NRG_at_index_from_DIG( size_t j , const std::vector<std::vector<double>>& DIG ){
	// Sum up the assembled energy for the part at index 'j' from all donor parts 'i'
	/* NOTE: In the DIG procedures above, outer loop 'i' is the mover (donor), and assigns an energy contribution to 'j'.  
			 In order calculate 'j's total energy, sum along 'j' */
	size_t len = DIG.size();
	double NRG = 0;
	if( j < len ){
		for( size_t i = 0 ; i < len ; i++ ){  NRG += DIG[i][j];  }
		return NRG;
	}else{  throw std::out_of_range ( "assembled_NRG_at_index_from_DIG , Index " + to_string( j )
									+ " out of range [ 0 , " + to_string( len-1 ) + " ] !" );   }
}

double assembled_NRG_at_indices_from_DIG( const std::vector<size_t>& jDices , const std::vector<std::vector<double>>& DIG ){
	// NRG --> Part
	size_t lenDIG = DIG.size()    ,
		   lenDex = jDices.size() ,
		   j      = 0             ; 
	double NRG    = 0;
	for( size_t jDex = 0 ; jDex < lenDex ; jDex++ ){
		j = jDices[ jDex ];
		if( j < lenDIG ){
			for( size_t i = 0 ; i < lenDIG ; i++ ){  NRG += DIG[i][j];  }
		}else{  throw std::out_of_range ( "assembled_NRG_at_indices_from_DIG , Index " + to_string( j )
										+ " out of range [ 0 , " + to_string( lenDIG-1 ) + " ] !" );   }
	}
	return NRG;
}

double donated_NRG_from_index_from_DIG( size_t i , const std::vector<std::vector<double>>& DIG ){ 
	// NRG <-- Part 
	size_t len = DIG.size();
	double NRG = 0;
	if( i < len ){
		for( size_t j = 0 ; j < len ; j++ ){  NRG += DIG[i][j];  }
		return NRG;
	}else{  throw std::out_of_range ( "donated_NRG_from_index_from_DIG , Index " + to_string( i )
									+ " out of range [ 0 , " + to_string( len-1 ) + " ] !" );   }
}

double donated_NRG_from_indices_from_DIG( const std::vector<size_t>& iDices , const std::vector<std::vector<double>>& DIG ){
	// NRG <-- Part 
	size_t lenDIG = DIG.size()    ,
		   lenDex = iDices.size() ,
		   i      = 0             ; 
	double NRG    = 0;
	for( size_t iDex = 0 ; iDex < lenDex ; iDex++ ){
		i = iDices[ iDex ];
		if( i < lenDIG ){
			for( size_t j = 0 ; j < lenDIG ; j++ ){  NRG += DIG[i][j];  }
		}else{  throw std::out_of_range ( "donated_NRG_from_index_from_DIG , Index " + to_string( i )
										+ " out of range [ 0 , " + to_string( lenDIG-1 ) + " ] !" );   }
	}
	return NRG;
}

double NRG_donated_by_i_to_j( size_t i , size_t j , const std::vector<std::vector<double>>& DIG ){
	size_t len = DIG.size();
	if(  ( i < len )  &&  ( j < len )  ){
		return DIG[i][j];
	}else{  throw std::out_of_range ( "NRG_donated_by_i_to_j , One of i=" + to_string( i ) + " or j=" + to_string( j  )
									+ " out of range [ 0 , " + to_string( len-1 ) + " ] !" );   }
}

double NRG_receivd_by_j_from_i( size_t j , size_t i , const std::vector<std::vector<double>>& DIG ){
	return NRG_donated_by_i_to_j( i , j , DIG );
}

struct priorityQ_less_than_size_t{ 
	// Compare the costs of two indices , Get the least first
	bool operator()( const std::pair<double,size_t>& op1 , const std::pair<double,size_t>& op2 ) const{
		return std::get<0>( op1 ) < std::get<0>( op2 );
	}
};

struct priorityQ_greater_than_size_t{ 
	// Compare the costs of two indices , Get the greatest first
	bool operator()( const std::pair<double,size_t>& op1 , const std::pair<double,size_t>& op2 ) const{
		return std::get<0>( op1 ) > std::get<0>( op2 );
	}
};

double max_blocking_fraction_sub1_vs_sub2( Assembly_ASM* assembly , // Create projection of one sub and collide it with another
										   const std::vector<llin>& sub1 , const std::vector<llin>& sub2 , // sub1 moves "against" sub2
										   const std::vector<llin>& refcID , // Reference parts for determining freedom
										   double dRadiusInterval , size_t numSteps ){
	// Blocking fraction sub -vs- sub , with the subset of parts that determine constraints specified by the user
	
	bool SHOWDEBUG = false; // if( SHOWDEBUG ){  cout << "" << endl;  }
	
	// 1. Form a mesh of sub 2
	TriMeshVFN_ASP sub2mesh = assembly->get_lab_VFN( sub2 ); // Get VFN for a sub
	
	if( SHOWDEBUG ){  cout << "Sub2 mesh has " << sub2mesh.F.rows() << " facets" << endl;  }
	
	// 2. Get the shells from sub 1
	std::vector<TriMeshVFN_ASP> sub1shells = get_intersection_surfaces( assembly , 
																	    sub1 , refcID , 
																		dRadiusInterval , numSteps );
	if( SHOWDEBUG ){  cout << "Got " << sub1shells.size() << " shells" << endl;  }
	// 3. Get the greatest fraction 1 -vs- 2
	double currFrac =  0.0       ,
		   mostFrac = -BILLION_D ;
	// A. Prep shells
	std::vector<CollisionVFN_ASP*> collsnShells = prep_meshes_for_collision( sub1shells );
	// B. For each shell , collide
	size_t numShells = collsnShells.size();
	for( size_t i = 0 ; i < numShells ; i++ ){
		currFrac = collision_fraction( collsnShells[i] , sub2mesh ); // Return the fraction of 'CollisionVFN_ASP' that intersects 'mesh'
		if( SHOWDEBUG ){  cout << "Collide shell " << i+1 << " , Fraction: " << currFrac << endl;  }
		mostFrac = max( mostFrac , currFrac );
	}
	if( SHOWDEBUG ){  cout << "Greatest fraction was: " << mostFrac << endl;  }
	// 4. Return greatest fraction
	return mostFrac;
}


std::vector<std::pair<llin,double>> sub_vs_parts_blockage_map( Assembly_ASM* assembly , // Create projection of one sub and collide it with another
															   const std::vector<llin>& sub , const std::vector<llin>& opponents , // sub moves "against" opponents
															   const std::vector<llin>& refcID , // Reference parts for determining freedom
															   double dRadiusInterval , size_t numSteps ){
	// Blocking calculator: Return a mapping (pair vector) from all parts not in the sub to the fraction that each blocks the sub
	std::vector<std::pair<llin,double>> rtnVec;
	// 0. Init
	size_t    numOppose = opponents.size();
	Part_ASM* currPart  = nullptr; // --- Pointer to interfering part
	double    currFrac  = 0.0 ,
			  mostFrac  = 0.0 ;
	// 1. Get the shells from the sub
	std::vector<TriMeshVFN_ASP> subShells = get_intersection_surfaces( assembly , 
																	   sub , refcID , 
																	   dRadiusInterval , numSteps );
	// A. Prep shells
	std::vector<CollisionVFN_ASP*> collsnShells = prep_meshes_for_collision( subShells );
	size_t numShells = subShells.size();
	// 2. For each opponent
	for( size_t i = 0 ; i < numOppose ; i++ ){
		// A. Get the part reference
		currPart = assembly->get_part_ptr_w_ID( opponents[i] );
		mostFrac = -BILLION_D;
		// 3. For each shell
		for( size_t j = 0 ; j < numShells ; j++ ){
			// 4. Get the blocking fraction , Store max
			currFrac = collision_fraction( collsnShells[j] , currPart );
			mostFrac = max( mostFrac , currFrac );
		}
		// 5. Push pair
		rtnVec.push_back(  std::make_pair( opponents[i] , mostFrac )  );
	}
	// N. Return
	return rtnVec;
}

double sum_blockage_across_parts( const std::vector<std::pair<llin,double>>& IDblockagePairs ){
	// Return the total of all per-part blockages computed by 'sub_vs_parts_blockage_map'
	size_t numPairs = IDblockagePairs.size();
	double total = 0.0;
	// 1. For each pair
	for( size_t i = 0 ; i < numPairs ; i++ ){
		// 2. Fetch the blocking score  &&  3. Add to the total
		total += std::get<1>( IDblockagePairs[i] );
	}
	return total;
}

double sum_blockage_across_parts_excluding( const std::vector<std::pair<llin,double>>& IDblockagePairs , const std::vector<llin>& excluded ){
	// Return the total of all per-part blockages computed by 'sub_vs_parts_blockage_map'
	size_t numPairs = IDblockagePairs.size();
	double total    = 0.0 ,
		   score    = 0.0 ;
	llin   currID   = 0;
	
	// 1. For each pair
	for( size_t i = 0 ; i < numPairs ; i++ ){
		currID = std::get<0>( IDblockagePairs[i] );
		score  = std::get<1>( IDblockagePairs[i] );
		// 1.5. If the ID is not in the excluded list
		if( !is_arg_in_vector( currID , excluded ) ){
			// 2. Fetch the blocking score  &&  3. Add to the total
			total += score;
		}
	}
	return total;
}

/// ~~~ SIR PLANNER ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

std::vector<std::vector<llin>> sub_ID_blockage_reduction_one_level( Assembly_ASM* original , 
																	double alpha , double beta , double gamma ,
																	double dRadiusInterval , size_t numSteps ,
																	double stuckInfluenceFactor , 
																	double blockThresh , 
																	ValidationParams params  ){ 
	// Identify subassemblies by accruing parts that reduce the overall blockage 
	// NOTE: THis function assumes that there is only one base part per precedence level
	
	bool SHOWDEBUG = true , // if( SHOWDEBUG ){  cout << "" << endl;  }
		 MOCKUPTST = true ; // if( MOCKUPTST ){  cout << "" << endl;  } // NOT USED?
	
	if( SHOWDEBUG ){ sep( "sub_ID_blockage_reduction_one_level" );  }

	std::vector<std::vector<llin>> rtnSubVec; // Verified subs that belong to this precedence level
	std::vector<std::vector<llin>> untestSub; // Untested subs formed by the method, need to check blockage and validation
	
	// ~~ IDEA ~~
	/*  I. Pull parts away from the base, along with the subs that go with these parts
	   II. Each decision to accrue parts should free parts, or at least reduce their blockage
	  III. Use shells to determine precedence. */
	 
	// ~~ CONCEPTS ~~
	/*  I. Compute shells with the least constraints
	   II. Use most free shells to compute blocking with all parts , I + II are to estimate what could be 
	  III. Compute freedom class with all parts. This is to account for the reality of what must be executed */
	
	/// VALIDATION METHOD ////
	bool multiValidate = true;
	SuccessPath valResult;
	///|//////////////////////
	
	// ~~ Requirements ~~
	
	// ~ Data Structures ~
	
	//  A. Master frontier , Persists for the duration of the algo
	//  B. Set of assigned parts
	//  C. Sub frontier, cleared at the beginning of every operation
	
	// ~~ Algo ~~
	
	
	if( SHOWDEBUG ){  cout << "~ Init Phase ~" << endl;  }
	// ~ Init Phase ~
	
	//  0. Init Vars
	std::vector<llin> /*- */ allPartID     = original->get_part_ID_vec();
	std::vector<size_t>      allDices      = vec_range( (size_t)0 , allPartID.size()-1 );
	
	double /* ----------- */ score /* - */ = 0.0 , //- Blocked or base fitness score, per part
							 qScore /*- */ = 0.0 , //- Score from a popped pair
							 maxBlokd      = 0.0 ; //- Blockage score for a tested assembly
	size_t /* ----------- */ currDex /*-*/ = 0 , // -- Index of the part being considered for addition to the sub
						     numNghbrs     = 0 , // -- Number of neighbors returned from a liaison lookup
						     nucleus /*-*/ = 0 , // -- Current sub nucleus being investigated
						     baseDex /*-*/ = 0 ; // -- Index of the base part
	bool /* ------------- */ addPartDecide = false; // Do we add the neighbor or not?
	std::pair<double,size_t> currQpair; // ----------- Popped index with score
	std::set<size_t> /* - */ assignedDices; // ------- Indices that have been assigned to an assembly , Once per assembly
	std::set<size_t> /* - */ visited; // ------------- Indices that have been tested for sub addition , Once per iteration
	std::vector<size_t>      neighbors; // ----------- Indices of the neighbors of the part under scrutiny
	std::vector<size_t>      currSub; // ------------- Contents of the current subassembly
	Eigen::MatrixXd /* -- */ curStraint; // ---------- Constraints on the current assembly
	Eigen::MatrixXd /* -- */ addStraint; // ---------- Constraints on the {current assembly + presumptive part}
	FREEDOM_CLASS /* ---- */ curFree; // ------------- Freedom class of the current assembly
	FREEDOM_CLASS /* ---- */ addFree; // ------------- Freedom class of the {current assembly + presumptive part}
	std::vector<size_t>      curMovDices; // --------- Moved indices for the current assembly
	std::vector<size_t>      curRfcDices; // --------- Reference indices for the current assembly
	std::vector<size_t>      addMovDices; // --------- Moved indices for the {current assembly + presumptive part}
	std::vector<size_t>      addRfcDices; // --------- Reference indices for {current assembly + presumptive part}
	double /* ----------- */ curBlockage = 0.0 , // -- Blockage calculation for the current assembly
							 addBlockage = 0.0 ; // -- Blockage calculation for {current assembly + presumptive part}
							 
	std::vector<std::pair<llin,double>> blockMap;
	
	if( SHOWDEBUG ){  cout << "Create queues ..." << endl;  }
	//  0.5. Init structs
	std::priority_queue< std::pair<double,size_t>              , 
                         std::vector<std::pair<double,size_t>> ,
                         priorityQ_less_than_size_t            > masterQ; // Greatest first
                         
	std::priority_queue< std::pair<double,size_t>              , 
                         std::vector<std::pair<double,size_t>> ,
                         priorityQ_less_than_size_t            > frontierQ; // Greatest first
	
	if( SHOWDEBUG ){  cout << "Locate base part ..." << endl;  }
	
	//  1. Choose base (Use the simplest method of the two competing, Not the "fitness" method)
	//     There must be only one base per precedence level
	IDDbblResult /*- */ largest      = original->largest_part_ID_w_vol();
	llin /* -------- */ baseID       = largest.ID;
	
	std::vector<llin>   idExceptBase = vec_copy_without_elem( allPartID , baseID );
	//~ std::vector<llin>   idExceptBase = allPartID;
	
	std::vector<size_t> nonBaseDices = original->partID_vec_to_index_vec( idExceptBase );
	std::vector<size_t> baseDexVec   = vec_copy_without_elem( allDices , nonBaseDices );
	if( baseDexVec.size() > 1 ){  cerr << "Too many bases!" << endl;  return rtnSubVec;   }
	size_t /* ------ */ numNonBase   = idExceptBase.size();
	if( nonBaseDices.size() != numNonBase ){  cerr << "Some non-base parts not located!" << endl;  return rtnSubVec;   }
	
	// 1.1. Make sure that we do not assign the base
	set_insert_vec( assignedDices , baseDexVec );
	
	IndexSearchResult result = original->index_w_ID( baseID );
	if( result.result ){  baseDex = result.index;  }else{  cerr << "Base part not located!" << endl;  return rtnSubVec;   }
	if( SHOWDEBUG ){  cout << "Base part has ID " << baseID << endl;  }
	if( SHOWDEBUG ){  cout << "Computing base part fitness ..."  << endl;  }
	
	//  2. Calculate base part fitness for all other parts
	std::map<llin,BasePartFitness> fitnessMap = calc_base_fitness_for_parts( *original , alpha , beta , gamma );
	if( SHOWDEBUG ){  cout << "Computed base part fitness for " << fitnessMap.size() << " parts" << endl
						   << "About to compute DIG ..." << endl;  }
	
	//  3. Compute shells of 1st parts against the base only // Use a specialized DIG, Will be used later in the algo
	std::vector<llin> baseOnly; 
	baseOnly.push_back( baseID );
	std::vector<std::vector<double>> DIG3 = DIG_from_asm_V3( original , dRadiusInterval , numSteps ,
															 baseOnly ,
															 stuckInfluenceFactor );
	if( SHOWDEBUG ){  cout << "DIG Computed:" << endl;  
				      print_vec_vec( DIG3 ); }
	if( SHOWDEBUG ){  cout << "Sorting parts by base part fitness ..." << endl;  }
	
	//  4. Enqueue all in master frontier: Score = ( base part fitness ) * ( Max blocked Fraction (max donation) )
	for( size_t i = 0 ; i < numNonBase ; i++ ){
		// A. Calc score
		score = fitnessMap[ idExceptBase[i] ].baseFitness;
		// B. Enque
		result = original->index_w_ID( idExceptBase[i] );
		if( result.result ){  
			if( SHOWDEBUG ){  cout << "Enque index " << result.index << " with score " << score << endl;  }
			masterQ.push(  std::make_pair( score , result.index )  );  
		}
	}
	if( SHOWDEBUG ){  cout << "Loaded " << masterQ.size() << " possible nuclei into queue" << endl;  }
	
	//  ~ Merge Phase ~
	
	//  5. While master
	while( masterQ.size() > 0 ){
	
		//  5. Pop the highest score
		//     Note that it is possible that a part that is popped might not be put into an assembly.  We leave it be so that it can be considered
		//     for other assemblies
		currQpair = queue_get_pop( masterQ );
		nucleus   = std::get<1>( currQpair );
		
		if( SHOWDEBUG ){  cout << "There are " << masterQ.size() << " parts in MASTER queue after pop" << endl
							   << "Working on possible nucleus: " << nucleus << endl;  }
		
		//  6. If the 1st has not been assigned
		if( !is_arg_in_set( nucleus , assignedDices ) ){
			// A. Start a new presumptive sub
			currSub.clear();
			currSub = { nucleus };
			// B. Clear the visited set for the new sub
			visited.clear();  
			
			// DANGER: THIS SHOULD HAVE WORKED
			// C. Count the nucleus as visited so that it is not added twice
			//~ visited.insert( nucleus );
			
			if( SHOWDEBUG ){  cout << "Index " << nucleus << " was not assigned , getting neighbors ..." << endl;  }
			
			//  6.5. Get neighbors
			neighbors = original->get_liaison_neighbors( nucleus );
			numNghbrs = neighbors.size();
			if( SHOWDEBUG ){  cout << "Got neighbors: " << neighbors << endl;  }
		
			//  7. Enque neighbors , Score them by how much they block the present 1st
			for( size_t i = 0 ; i < numNghbrs ; i++ ){
				if( ( !is_arg_in_set( neighbors[i] , visited ) )  &&  ( !is_arg_in_set( neighbors[i] , assignedDices ) )  ){
					//  A. Get the fraction that the neighbor blocks the 1st
					score = NRG_donated_by_i_to_j( nucleus , neighbors[i] , DIG3 );
					if( SHOWDEBUG ){  cout << "Index " << neighbors[i] << " has not been visited , Assign score: " << score << endl;  }
					//  B. Enque
					frontierQ.push(  std::make_pair( score , neighbors[i] )  );
				}else if( SHOWDEBUG ){  cout << "Index " << neighbors[i] << " was already visited" << endl;  }
			}
			
			if( SHOWDEBUG ){  cout << "Iterating through neighbors ... " << endl;  }
			//  8. While frontier
			while( frontierQ.size() > 0 ){
				//  9. Pop the most blocking
				currQpair = queue_get_pop( frontierQ );
				currDex   = std::get<1>( currQpair );
				qScore    = std::get<0>( currQpair );
				if( SHOWDEBUG ){  cout << "Popped neighbor " << currDex << " with score " << qScore << endl;  }
				//  9.1. If the part has not been visited
				if( !is_arg_in_set( currDex , visited ) ){
					//  9.2. Mark the part as visited
					visited.insert( currDex );
					//  9.3. Get part membership vectors
					curMovDices = currSub;
					curRfcDices = vec_copy_without_elem( allDices , curMovDices );
					addMovDices = vec_copy( curMovDices );  addMovDices.push_back( currDex ); // Add the presumptive part
					addRfcDices = vec_copy_without_elem( allDices , addMovDices );
					//  9.4. Reset part decision
					addPartDecide = false;
					//  9.5. If not assigned
					if( !is_arg_in_set( currDex , assignedDices ) ){
						// 10. Get constraints and class (full situ)
						//  A. Constraints for current sub
						curStraint = original->asm_get_constraints_on_movd( curMovDices , curRfcDices ); // NDBG Order
						//  B. Freedom state for current sub
						curFree    = freedom_class_from_constraints( curStraint );
						//  C. Constraints with addition
						addStraint = original->asm_get_constraints_on_movd( addMovDices , addRfcDices ); // NDBG Order
						//  D. Freedom state with addition
						addFree    = freedom_class_from_constraints( addStraint );
						
						// 11. If a blocked sub was freed , Accept addition
						if(  ( curFree == LOCKED )  &&  ( addFree > LOCKED )  ){  
							addPartDecide = true;  
							if( SHOWDEBUG ){  cout << "ACCEPT new part, UNLOCK" << endl;  }
						// 12. If a free sub was blocked , Reject addition
						}else if(  ( curFree > LOCKED )  &&  ( addFree == LOCKED )  ){  
							addPartDecide = false;  
							if( SHOWDEBUG ){  cout << "REJECT new part, LOCK" << endl;  }
						// 12.5. If there was not an immediate acceptance or rejection
						}else{
							if( SHOWDEBUG ){  cout << "Current Sub ID: ___ " << original->index_vec_to_partID_vec( curMovDices ) << endl  
												   << "Current Opponents:_ " << original->index_vec_to_partID_vec( curRfcDices ) << endl  
												   << "Addition Sub ID: __ " << original->index_vec_to_partID_vec( addMovDices ) << endl  
												   << "Addition Opponents: " << original->index_vec_to_partID_vec( addRfcDices ) << endl  
												   << "Base ID: __________ " << original->index_vec_to_partID_vec( baseDexVec ) << endl
												   << "Current Sub Index:_ " << curMovDices << endl  
												   << "Current Opponents:_ " << curRfcDices << endl  
												   << "Addition Sub Index: " << addMovDices << endl  
												   << "Addition Opponents: " << addRfcDices << endl  
												   << "Base Index: _______ " << baseDexVec  << endl;  }
							
							// 13. Get the blocking constrained by base only 
							//  A. Blocking for current sub (current sub and presumptive part move independently)
							blockMap = sub_vs_parts_blockage_map( original , // Create projection of one sub and collide it with another
															   original->index_vec_to_partID_vec( curMovDices ) , 
															   original->index_vec_to_partID_vec( curRfcDices ) , // sub moves "against" opponents
															   original->index_vec_to_partID_vec( baseDexVec ) , // Reference parts for determining freedom
															   dRadiusInterval , numSteps );
							if( SHOWDEBUG ){  cout << "\tCurrent Blockmap:_ " << blockMap << endl;  } 
							double donated = donated_NRG_from_index_from_DIG( currDex , DIG3 ); // NRG <-- Part 
							curBlockage = max( sum_blockage_across_parts( blockMap ) , donated );
							if( SHOWDEBUG ){  cout << "\tDonated: _________ " << donated << endl
												   << "\tCurrent Blockage:_ " << curBlockage << endl;  } 
							//  B. Blocking with addition (current sub and presumptive part move together)
							blockMap = sub_vs_parts_blockage_map( original , // Create projection of one sub and collide it with another
																  original->index_vec_to_partID_vec( addMovDices ) , 
																  original->index_vec_to_partID_vec( addRfcDices ) , // sub moves "against" opponents
																  original->index_vec_to_partID_vec( baseDexVec ) , // Reference parts for determining freedom
																  dRadiusInterval , numSteps );
							if( SHOWDEBUG ){  cout << "\tAddition Blockmap: " << blockMap << endl;  } 
							addBlockage = sum_blockage_across_parts( blockMap );
							if( SHOWDEBUG ){  cout << "\tAddition Blockage: " << addBlockage << endl;  } 
							// 14. If blocking was reduced , Add part , Otherwise reject // ALTERNATE: Accept any that do not increase blocking
							if(  ( curBlockage > addBlockage )  ||  eq( curBlockage , addBlockage , 0.05 )  ){  
								addPartDecide = true; 
								if( SHOWDEBUG ){  cout << "ACCEPT new part, MAX BLOCKAGE REDUCED" << endl;  } 
							}else if( SHOWDEBUG ){  cout << "REJECT new part, BLOCKAGE NOT REDUCED" << endl;  } 
						}
					}
					if( SHOWDEBUG ){  cout << "Accept new part?: " << yesno( addPartDecide ) << endl << endl;  }
									   
					// 14.1. If the part was added , 
					if( addPartDecide ){
						// 14.1. Add the part to the running assembly
						currSub.push_back( currDex );
						// 14.3 enqueue neighbors
						// 14.4. Get neighbors
						neighbors = original->get_liaison_neighbors( currDex );
						numNghbrs = neighbors.size();
						if( SHOWDEBUG ){  cout << "Got neighbors: " << neighbors << endl;  }
					
						// 14.5. Enque neighbors , Score them by how much they block the present 1st
						for( size_t i = 0 ; i < numNghbrs ; i++ ){
							if(  ( !is_arg_in_set( neighbors[i] , visited ) )  &&  ( !is_arg_in_set( neighbors[i] , assignedDices ) )  ){
								//  A. Get the fraction that the neighbor blocks the 1st
								score = NRG_donated_by_i_to_j( currDex , neighbors[i] , DIG3 );
								if( SHOWDEBUG ){  cout << "Index " << neighbors[i] << " has not been visited , Assign score: " << score << endl;  }
								//  B. Enque
								frontierQ.push(  std::make_pair( score , neighbors[i] )  );
							}else if( SHOWDEBUG ){  cout << "Index " << neighbors[i] << " was already visited" << endl;  }
						}
					}
				}
			}
				
			// 15. If an assembly of >1 parts was formed
			if( currSub.size() > 1 ){
				// 16. Add parts to the assigned set
				set_insert_vec( assignedDices , currSub );
				
				// DANGER: THIS IS A HACK
				//~ currSub = vec_unique_only( currSub ); // Uniqify 'currSub'
				
				// 16.A. Add the sub to vector of untested subs
				untestSub.push_back( original->index_vec_to_partID_vec( currSub ) );
			} // else do nothing , This part belongs to another assembly
		} 
		if( SHOWDEBUG ){  cout << endl << endl;  }
	}
	// ~ Precedence Phase ~
			
	// 17. Determine if there are any loose parts and make them their own subs
	if( SHOWDEBUG ){  cout << "Cleaning up one-part subs ..." << endl;  }
	//  A. For every non-base part
	for( size_t i = 0 ; i < numNonBase ; i++ ){
		//  B. Check to see if it is assigned  &&  C. If it has not been assigned, put in vector and add to untested subs
		if( !is_arg_in_set( nonBaseDices[i] , assignedDices ) ){
			std::vector<llin> temp = { idExceptBase[i] };
			untestSub.push_back( temp );
			if( SHOWDEBUG ){  cout << "Adding one-part sub with ID: " << temp << endl;  }
		} // Otherwise was used elsewhere , No action
	}
	
	size_t numUntested = untestSub.size();
	if( SHOWDEBUG ){  cout << "There are " << numUntested << " subs to test" << endl;  }
	// 18. For every identified assembly
	std::vector<llin> movd;
	std::vector<llin> refc;
	std::vector<llin> leftOvers = { baseID };
	Eigen::Vector3d   remDir;
	bool /* ------ */ validated = false;
	for( size_t i = 0 ; i < numUntested ; i++ ){
		validated = false;
		if( SHOWDEBUG ){  cout << "Testing sub " << i+1 << " with IDs " << untestSub[i] << endl;  }
		// 19. If free (considering all other parts) 
		// Note that in order to be considered part of the precedence level, the sub has to be able to removed by itself without respect to order
		movd = untestSub[i];
		refc = vec_copy_without_elem( allPartID , movd );
		//~ if( SHOWDEBUG ){  cout << "Testing sub " << i+1 << " with IDs " << movd << " and reference " << refc << endl;  }
		if( SHOWDEBUG ){  cout << "Testing sub " << i+1 << " with IDs " << movd << " against all other parts " << endl;  }
		remDir = original->removal_dir_moved_vs_balance( movd ); // Suggest center of freedom of moved parts -vs- balance , Part ID
		if( !is_err( remDir ) ){
			// 20. Compute blocking against entire asm
			maxBlokd = max_blocking_fraction_sub1_vs_sub2( original , // Create projection of one sub and collide it with another
														   movd , refc , // sub1 moves "against" sub2
														   refc , // Reference parts for determining freedom
														   dRadiusInterval , numSteps );
			if( SHOWDEBUG ){  cout << "Blocked fraction of tested sub: " << maxBlokd << endl;  }
			
			// 21. If blocking < rejection threshold
			if( maxBlokd < blockThresh ){
				// 22. VALIDATE
				/// ### VALIDATE SUB ###
				if( multiValidate ){
					//  I. Validate using multiple directions and methods
					//     i.e. Validate for removal and stability , Using Straightline and RRT*
					if( SHOWDEBUG ){  cout << "About to run validator ..." << endl;  }
					valResult = validate_removal_multimode_w_stability( original , 
																		movd , refc , 
																		params , 
																		remDir );
					if( SHOWDEBUG ){  cout << "Validation completed! , Result: " << yesno( valResult.success ) << endl;  }
					// II. Set flag
					if( SHOWDEBUG ){  cout << "Set flag" << endl;  }
					validated = valResult.success;
				}else{
					validated = original->test_translation_sub_removal( movd , refc , // Test removing 'movedParts' 
																		remDir , dRadiusInterval * (double) params.numSamples , 
																		params.numSamples * 2 );   // along 'direction' in incremental 
				}
				/// *** END VALIDATE ***
				
				if(  SHOWDEBUG  &&  ( !validated )  ){  cout << "Straightline validation FAILED!" << endl;  }
			}else{ // Reject parts that are blocked
				validated = false;  
				if( SHOWDEBUG ){  cout << "Blocked threshold FAILED!" << endl;  }
			}
		}else{ // Reject parts without any local freedom
			validated = false;  
			if( SHOWDEBUG ){  cout << "Freedom check FAILED!" << endl;  }
		}
		
		if( !validated ){
		// 19.5. Else not free , dump back onto default assembly
			extend_vec_with( leftOvers , movd );
			if( SHOWDEBUG ){  cout << "Tested sub was REJECTED!  Leftover sub:" << leftOvers << endl;  }
		}else{
			//~ rtnSubVec.push_back( movd ); 
			// DANGER: THIS MASKS NUCLEUS REPEAT ISSUE , FIXES FOR ISSUE RESULT IN LESS SUBS PER LEVEL , WEIRD
			rtnSubVec.push_back( vec_unique_only( movd ) ); // Uniqify sub
			if( SHOWDEBUG ){  cout << "Tested sub was ACCEPTED!" << endl;  }
		}
	}
	
	// 23. Check that the leftovers are connected
	bool leftoversConnected = is_subgraph_connected( original , leftOvers );
	if( SHOWDEBUG ){  cout << "Remainder connected?: " << yesno( leftoversConnected ) << endl;  }
	if( leftoversConnected ){
		rtnSubVec.push_back( vec_unique_only( leftOvers ) );
	}else{
		std::vector<std::vector<llin>> conTrashSubs = connected_subgraphs_in_subgraph( original , leftOvers );
		for( size_t i = 0 ; i < conTrashSubs.size() ; i++ ){
			rtnSubVec.push_back( vec_unique_only( conTrashSubs[i] ) );
		}
	}
	
	// N. Return the list of validated subs
	return rtnSubVec;
}


AsmStateNode_ASP* sub_ID_blockage_reduction_full_depth( Assembly_ASM* original , 
														double alpha , double beta , double gamma ,
														double dRadiusInterval , size_t numSteps ,
														double stuckInfluenceFactor , 
														double blockThresh , 
													    ValidationParams params  ){
	
	AsmStateNode_ASP* rootNode = new AsmStateNode_ASP( original );
	std::queue<AsmStateNode_ASP*> subNodes;
	AsmStateNode_ASP* currNode = nullptr;
	AsmStateNode_ASP* nuNode   = nullptr;
	std::vector<std::vector<llin>> asmClusterLvl1;
	size_t len = 0;
	bool SHOWDEBUG = false;
	
	// 1. Enqueue original (node)
	subNodes.push( rootNode );
	// 2. While there are still assemblies in queue
	while( subNodes.size() > 0 ){
		
		if( SHOWDEBUG ){  cout << "There are " << subNodes.size() << " nodes to process ..." << endl;  }
		
		// 3. Retrieve and Pop node
		currNode = subNodes.front();  subNodes.pop();
		// 4. If the asm has more than one part
		//~ if( currNode->memParts.size() > 1 ){
		if( currNode->assembly->get_part_ID_vec().size() > 1 ){
			
			if( SHOWDEBUG ){  cout << "Processing a sub with " << currNode->assembly->get_part_ID_vec().size() << " parts ..." << endl;  }
			
			// 5. Execute 1st-level clustering
			
			asmClusterLvl1 = sub_ID_blockage_reduction_one_level( currNode->assembly , 
																  alpha , beta , gamma ,
																  dRadiusInterval , numSteps ,
																  stuckInfluenceFactor , 
																  blockThresh ,
																  params );
							 
			
			if( SHOWDEBUG ){  
				sep( "Sub ID Result" );  
				print_vec_vec( asmClusterLvl1 );
			}
			
			
			len = asmClusterLvl1.size();
			
			if( SHOWDEBUG ){  cout << "There are " << len << " edges to add ..." << endl;  }
			
			if( len > 0 ){ // This should always be true if the asm clustered had more than one part
				// 6. For every parts list produced
				for( size_t i = 0 ; i <  len ; i++ ){
					
					if( SHOWDEBUG ){  
						cout << "Creating sub " << i + 1 << " of " << len << endl  
							 << "Sub contains " << asmClusterLvl1[i] << endl;  
					}
					
					// 7. Produce a new assembly , load into Node  &&  8. Enqueue the new assembly
					nuNode = currNode->add_edge( asmClusterLvl1[i] );
					
					if( SHOWDEBUG ){  
						if( nuNode ){  cout << "Good Pointer!" << endl;  }
						else{  cout << "Bad Pointer!" << endl;  }
					}
					
					subNodes.push( nuNode );
				}
			}
			
			if( SHOWDEBUG ){  cout << "After processing level, there are " << subNodes.size() << " in queue" << endl;  }
			
		}
	}
	return rootNode;
}

/// ,, END SIR ,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,

/// ___ END DIG ____________________________________________________________________________________________________________________________


/// === VISUALIZATION ======================================================================================================================

ShellVizMap sub_vs_parts_blockage_viz( Assembly_ASM* assembly , // Create projection of one sub and collide it with another
									   const std::vector<llin>& sub , const std::vector<llin>& opponents , // sub moves "against" opponents
									   const std::vector<llin>& refcID , // Reference parts for determining freedom
									   double dRadiusInterval , size_t numSteps ){
	// Blocking calculator: Return a mapping (pair vector) from all parts not in the sub to the fraction that each blocks the sub
	ShellVizMap rtnStruct;
	std::vector<std::pair<llin,double>> rtnVec;
	// 0. Init
	size_t    numOppose = opponents.size();
	Part_ASM* currPart  = nullptr; // --- Pointer to interfering part
	double    currFrac  = 0.0 ,
			  mostFrac  = 0.0 ;
	// 1. Get the shells from the sub
	std::vector<TriMeshVFN_ASP> subShells = get_intersection_surfaces( assembly , 
																	   sub , refcID , 
																	   dRadiusInterval , numSteps );
	// A. Prep shells
	std::vector<CollisionVFN_ASP*> collsnShells = prep_meshes_for_collision( subShells );
	size_t numShells = subShells.size();
	// 2. For each opponent
	for( size_t i = 0 ; i < numOppose ; i++ ){
		// A. Get the part reference
		currPart = assembly->get_part_ptr_w_ID( opponents[i] );
		mostFrac = -BILLION_D;
		// 3. For each shell
		for( size_t j = 0 ; j < numShells ; j++ ){
			// 4. Get the blocking fraction , Store max
			currFrac = collision_fraction( collsnShells[j] , currPart );
			mostFrac = max( mostFrac , currFrac );
		}
		// 5. Push pair
		rtnVec.push_back(  std::make_pair( opponents[i] , mostFrac )  );
	}
	// N. Return
	rtnStruct.mapping   = rtnVec;
	rtnStruct.subShells = subShells;
	return rtnStruct;
}

void load_shells_into_mrkrMngr( const std::vector<TriMeshVFN_ASP>& shells , RViz_MarkerManager& mrkrMngr ){
	size_t numShells = shells.size();
	for( size_t i = 0 ; i < numShells ; i++ ){
		RViz_MeshMarker mrkr{ shells[i] };
		mrkr.rand_color();
		mrkrMngr.add_marker( mrkr );
	}
}

/// ___ END VIZ ____________________________________________________________________________________________________________________________


// ___ End Func ____________________________________________________________________________________________________________________________




/* === Spare Parts =========================================================================================================================



   ___ End Parts ___________________________________________________________________________________________________________________________

*/

