/***********  
Model_Factory.cpp
James Watson , 2018 June
Load ASP models with an optional subset of parts

NOTE: This module assumes that only one assembly at a time, and therefore the part numbers generated will hold true

Template Version: 2017-09-23
***********/

#include "Model_Factory.h"

// === Assemblies ==========================================================================================================================

/// ~~~ Simple Phone ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Assembly_ASM* simple_phone(){
    // A radio-looking thing that implies sub-assemblies in its design , Based neither on a real product nor literature
    std::vector<llin> partNums = vec_range( (llin) 0 , (llin) 12 );
    return simple_phone( partNums );
}

Assembly_ASM* simple_phone( std::vector<llin>& partIDs ){
    // A radio-looking thing that implies sub-assemblies in its design , Ability to select a subset of parts
    // NOTE: This function does not enforce connectivity
    
    bool SHOWDEBUG = false; // if( SHOWDEBUG ){  cout << "" << endl;  }
    
    if( SHOWDEBUG ){  cout << "Init ..." << endl;  }
    
    Incrementer assign_part_ID{ 0 }; // Instantiate a functor that will assign a unique number to each part
    Incrementer assign_asm_ID{ 0 }; //- Instantiate a functor that will assign a unique number to each assembly
	
	double SML_Z_CRCTN = 999.7 * EPSILON; // Small Z correction
	
	if( SHOWDEBUG ){  cout << "Using small Z correction: " << SML_Z_CRCTN << endl;  }
	
	if( SHOWDEBUG ){  cout << "Allocate ..." << endl;  }
	
	Assembly_ASM* simplePhone = new Assembly_ASM( assign_asm_ID() );
	
	if( SHOWDEBUG ){  cout << "Build paths ..." << endl;  }
	
	// ~~ 0. Create file paths ~~
	// ~ Fetch the folder to the STL files ~
	string pkgPath = ros::package::getPath( "motion_cost" );
	string resourcePath = pkgPath + "/Resources/Assembly_CAD/SimplePhone/";
	// ~ Create full paths to STLs ~
	std::vector<string> fNames;
/* 0*/ fNames.push_back( resourcePath + "body_SCL.stl" );      Eigen::Vector3d body_shift{    -3.92054e-05 ,  9.47465e-05 , -0.00919433 };
/* 1*/ fNames.push_back( resourcePath + "antenna_SCL.stl" );   Eigen::Vector3d antenna_shift{  0.005       , -0.0175      , -0.00375    };
/* 2*/ fNames.push_back( resourcePath + "bridgeAB_SCL.stl" );  Eigen::Vector3d bridgeAB_shift{ 0.0         ,  0.0         , -0.0025     };
/* 3*/ fNames.push_back( resourcePath + "bridgeBC_SCL.stl" );  Eigen::Vector3d bridgeBC_shift{ 0.0         ,  0.0         , -0.0025     };
/* 4*/ fNames.push_back( resourcePath + "chipA1_SCL.stl" );    Eigen::Vector3d chipA1_shift{   0.0         ,  0.0         , -0.0025     };
/* 5*/ fNames.push_back( resourcePath + "chipA1_SCL.stl" );    
/* 6*/ fNames.push_back( resourcePath + "chipB1_SCL.stl" );    Eigen::Vector3d chipB1_shift{  -0.002       ,  0.00666667  , -0.0025     };
/* 7*/ fNames.push_back( resourcePath + "chipB2_SCL.stl" );    Eigen::Vector3d chipB2_shift{   0.00371429  ,  0.0114286   , -0.0025     };
/* 8*/ fNames.push_back( resourcePath + "chipC1_SCL.stl" );    Eigen::Vector3d chipC1_shift{   0.0         , -0.00697957  , -0.0025     };
/* 9*/ fNames.push_back( resourcePath + "moduleA_SCL.stl" );   Eigen::Vector3d moduleA_shift{  0.0         ,  0.000178571 , -0.00446429 };
/*10*/ fNames.push_back( resourcePath + "moduleB_SCL.stl" );   Eigen::Vector3d moduleB_shift{  7.01754e-05 ,  0.0         , -0.00453947 };
/*11*/ fNames.push_back( resourcePath + "moduleC_SCL.stl" );   Eigen::Vector3d moduleC_shift{  0.0         ,  1.39405e-05 , -0.00428903 };
/*12*/ fNames.push_back( resourcePath + "panel_SCL.stl" );     Eigen::Vector3d panel_shift{    0.0         , 0.0          , -0.00125    };

    if( SHOWDEBUG ){  cout << "Create parts ..." << endl;  }

	// ~ Create parts ~
	size_t fLen = fNames.size();
	std::vector<Part_ASM> phoneParts;
	for( size_t i = 0 ; i < fLen ; i++ ){
		if( SHOWDEBUG ){  cout << fNames[i];  }
		phoneParts.push_back( Part_ASM{   assign_part_ID() , fNames[i]   } );
		if( SHOWDEBUG ){  cout << "\tPart created!" << endl;  }
	}
	if( SHOWDEBUG ){  cout << "Added " << phoneParts.size() << " parts." << endl;  }
	
	// 2. Create an assembly , add parts
	
	bool ACTIVATE_ALL = false;
	
	// ~~ Body ~~
if( is_arg_in_vector( (llin)0 , partIDs ) ){
/* 0*/ simplePhone->add_part_w_pose( phoneParts[ 0] , Pose_ASP{ -body_shift , no_turn_quat() } ); // body
}

	// ~~ Module A ~~
if( is_arg_in_vector( (llin)9 , partIDs ) ){
	/* 9*/ simplePhone->add_part_w_pose( phoneParts[ 9] , Pose_ASP{ -moduleA_shift  + Eigen::Vector3d(  0.0000 ,  0.04000 ,  0.0025 + SML_Z_CRCTN ) , // moduleA
																	 no_turn_quat()                                       } );
}
if( is_arg_in_vector( (llin)4 , partIDs ) ){
	/* 4*/ simplePhone->add_part_w_pose( phoneParts[ 4] , Pose_ASP{ -chipA1_shift   + Eigen::Vector3d(  0.0000 ,  0.04875 ,  0.0100 + SML_Z_CRCTN * 2 ) , // chipA1
																	 no_turn_quat()                                       } );
}
if( is_arg_in_vector( (llin)5 , partIDs ) ){
	/* 5*/ simplePhone->add_part_w_pose( phoneParts[ 5] , Pose_ASP{ -chipA1_shift   + Eigen::Vector3d(  0.0000 ,  0.03875 ,  0.0100 + SML_Z_CRCTN * 2 ) , // chipA1
																	 no_turn_quat()                                       } ); 
}

	// ~~ Module B ~~
if( is_arg_in_vector( (llin)10 , partIDs ) ){
	/*10*/ simplePhone->add_part_w_pose( phoneParts[10] , Pose_ASP{ -moduleB_shift  + Eigen::Vector3d(  0.0000 ,  0.00000 ,  0.0025  ) , // moduleB
																 no_turn_quat()                                       } );
}
if( is_arg_in_vector( (llin)6 , partIDs ) ){
	/* 6*/ simplePhone->add_part_w_pose( phoneParts[ 6] , Pose_ASP{ -chipB1_shift   + Eigen::Vector3d( -0.0075 ,  0.01000 ,  0.0100   ) , // chipB1
																 no_turn_quat()                                       } );
}
if( is_arg_in_vector( (llin)7 , partIDs ) ){
	/* 7*/ simplePhone->add_part_w_pose( phoneParts[ 7] , Pose_ASP{ -chipB2_shift   + Eigen::Vector3d(  0.0075 ,  0.01000 ,  0.0100   ) , // chipB2
																 no_turn_quat()                                       } ); 
}

	// ~~ Module C ~~
if( is_arg_in_vector( (llin)11 , partIDs ) ){
	/*11*/ simplePhone->add_part_w_pose( phoneParts[11] , Pose_ASP{ -moduleC_shift  + Eigen::Vector3d(  0.0000 , -0.04000 ,  0.0025 ) , // moduleC
		                                                     no_turn_quat()                                       } );
}
if( is_arg_in_vector( (llin)8 , partIDs ) ){
	/* 8*/ simplePhone->add_part_w_pose( phoneParts[ 8] , Pose_ASP{ -chipC1_shift   + Eigen::Vector3d(  0.0000 , -0.04750 ,  0.0100 ) , // chipC1
																 no_turn_quat()                                       } ); 
}

	// ~~ Misc & Bridges ~~
if( is_arg_in_vector( (llin)2 , partIDs ) ){
	/* 2*/ simplePhone->add_part_w_pose( phoneParts[ 2] , Pose_ASP{ -bridgeAB_shift + Eigen::Vector3d(  0.0000 ,  0.02250 ,  0.0100 + SML_Z_CRCTN ) , // bridgeAB
																 no_turn_quat()                                       } );
}
if( is_arg_in_vector( (llin)3 , partIDs ) ){
	/* 3*/ simplePhone->add_part_w_pose( phoneParts[ 3] , Pose_ASP{ -bridgeBC_shift + Eigen::Vector3d(  0.0000 , -0.02500 ,  0.0100 ) , // bridgeBC
																 no_turn_quat()                                       } );
}
if( is_arg_in_vector( (llin)1 , partIDs ) ){
	/* 1*/ simplePhone->add_part_w_pose( phoneParts[ 1] , Pose_ASP{ -antenna_shift  + Eigen::Vector3d( -0.0200 ,  0.05250 ,  0.0050 + SML_Z_CRCTN ) , // antenna
																 no_turn_quat()                                       } ); 
}	
    if( SHOWDEBUG ){  cout << "Recalc geo ..." << endl;  }

	// 3. Determine the local freedom of each part (NDBG)
	simplePhone->recalc_geo();
		
	if( SHOWDEBUG ){  cout << "Return ..." << endl;  }
	
	return simplePhone;
}

/// ~~~ Simple Cube ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Assembly_ASM* simple_cube(){
	// NOTE: This function does not enforce connectivity
    
    Incrementer assign_part_ID{ 0 }; // Instantiate a functor that will assign a unique number to each part
    Incrementer assign_asm_ID{ 0 }; //- Instantiate a functor that will assign a unique number to each assembly
    
	Assembly_ASM* simpleCube = new Assembly_ASM( assign_asm_ID() );
	
	// ~~ 0. Create file paths ~~
	// ~ Fetch the folder to the STL files ~
	string pkgPath = ros::package::getPath( "motion_cost" );
	string resourcePath = pkgPath + "/Resources/Assembly_CAD/SimpleCube/";
	// ~ Create full paths to STLs ~
	std::vector<string> fNames;
	
	fNames.push_back( resourcePath + "PocketPlate_04.stl" );  Eigen::Vector3d pocket_shift{ -0.06 , -0.06 , -0.00970588 };
	fNames.push_back( resourcePath + "MeterCube_04.stl"   );  Eigen::Vector3d cube_shift{   -0.02 , -0.02 , -0.02       };
	
	// ~~ 1. Create parts ~~
	size_t fLen = fNames.size();
	std::vector<Part_ASM> cubeParts;
	for( size_t i = 0 ; i < fLen ; i++ ){
		cout << fNames[i] << endl;
		cubeParts.push_back( Part_ASM{   assign_part_ID() , fNames[i]   } );
	}
	
	double sideLen = 0.04; // Side length is 4cm (1.6in)
	
	simpleCube->add_part_w_pose( cubeParts[ 0] , Pose_ASP{ -pocket_shift   , // pocket
														   no_turn_quat()  } );
	simpleCube->add_part_w_pose( cubeParts[ 1] , Pose_ASP{ -cube_shift + Eigen::Vector3d( sideLen , sideLen , sideLen * 0.25 ) , // Cube
														   no_turn_quat()                                                      } );
														   
	// 3. Determine the local freedom of each part (NDBG)
	simpleCube->recalc_geo();
		
	return simpleCube;
}


/// ~~~ Simple Cube Obstructed ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Assembly_ASM* simple_cube_obstr(){
	// NOTE: This function does not enforce connectivity
    
    Incrementer assign_part_ID{ 0 }; // Instantiate a functor that will assign a unique number to each part
    Incrementer assign_asm_ID{ 0 }; //- Instantiate a functor that will assign a unique number to each assembly
    
	Assembly_ASM* simpleCube = new Assembly_ASM( assign_asm_ID() );
	
	// ~~ 0. Create file paths ~~
	// ~ Fetch the folder to the STL files ~
	string pkgPath = ros::package::getPath( "motion_cost" );
	string resourcePath = pkgPath + "/Resources/Assembly_CAD/SimpleCube/";
	// ~ Create full paths to STLs ~
	std::vector<string> fNames;
	
	fNames.push_back( resourcePath + "PocketPlateObst_04.stl" );  Eigen::Vector3d pocket_shift{ -0.06 , -0.054 , -0.025 };
	fNames.push_back( resourcePath + "MeterCube_04.stl"   );      Eigen::Vector3d cube_shift{   -0.02 , -0.02  , -0.02  };
	
	// ~~ 1. Create parts ~~
	size_t fLen = fNames.size();
	std::vector<Part_ASM> cubeParts;
	for( size_t i = 0 ; i < fLen ; i++ ){
		cout << fNames[i] << endl;
		cubeParts.push_back( Part_ASM{   assign_part_ID() , fNames[i]   } );
	}
	
	double sideLen = 0.04; // Side length is 4cm (1.6in)
	
	simpleCube->add_part_w_pose( cubeParts[ 0] , Pose_ASP{ -pocket_shift   , // pocket
														   no_turn_quat()  } );
	simpleCube->add_part_w_pose( cubeParts[ 1] , Pose_ASP{ -cube_shift + Eigen::Vector3d( sideLen , sideLen , sideLen * 0.25 ) , // Cube
														   no_turn_quat()                                                      } );
														   
	// 3. Determine the local freedom of each part (NDBG)
	simpleCube->recalc_geo();
		
	return simpleCube;
}

/// ~~~ Drive Motor , Wang ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Assembly_ASM* drive_motor( std::vector<llin>& partIDs ){
	// NOTE: This function does not enforce connectivity

	Incrementer assign_part_ID{ 0 }; // Instantiate a functor that will assign a unique number to each part
    Incrementer assign_asm_ID{ 0 }; //- Instantiate a functor that will assign a unique number to each assembly
    
	Assembly_ASM* rtnAsm = new Assembly_ASM( assign_asm_ID() );
	
	double SML_Z_CRCTN = 999.7 * EPSILON; // Small Z correction
	
	// ~~ 0. Create file paths ~~
	// ~ Fetch the folder to the STL files ~
	string pkgPath = ros::package::getPath( "motion_cost" );
	string resourcePath = pkgPath + "/Resources/Assembly_CAD/DriveMotorWang/";
	// ~ Create full paths to STLs ~
	std::vector<string> fNames;
	
	fNames.push_back( resourcePath + "Base_SCL.stl" );  	        Eigen::Vector3d base_shift{    0.0       , 0.0  , -0.011205 };
	fNames.push_back( resourcePath + "Motor_SCL.stl" );  	        Eigen::Vector3d motor_shift{   0.0       , 0.0  , -0.011053 };
	// fNames.push_back( resourcePath + "Bushing_SCL.stl" );	    Eigen::Vector3d bushing_shift{ 0.0       , 0.0  , -0.01     };
	fNames.push_back( resourcePath + "bushing_INVERT_SCL.stl" );	Eigen::Vector3d bushing_shift{ 0.0       , 0.0  , -0.01     };
	//fNames.push_back( resourcePath + "Bushing_SCL.stl" );
	fNames.push_back( resourcePath + "bushing_INVERT_SCL.stl" );
	//~ fNames.push_back( resourcePath + "Pillar_SCL.stl" );  	        Eigen::Vector3d pillar_shift{  0.0       , 0.0  , -0.019759 };
	fNames.push_back( resourcePath + "Pillar_GRASP_SCL.stl" );  	Eigen::Vector3d pillar_shift{  0.0       , 0.0  , -0.021425 };
	//~ fNames.push_back( resourcePath + "Pillar_SCL.stl" );  	
	fNames.push_back( resourcePath + "Pillar_GRASP_SCL.stl" );  	
	// fNames.push_back( resourcePath + "Cover_SCL.stl" );  	    Eigen::Vector3d cover_shift{   0.032611  , 0.05 ,  0.001857 };
	//~ fNames.push_back( resourcePath + "Cover_SMLHOL_SCL.stl" );      Eigen::Vector3d cover_shift{   0.032568  , 0.05 ,  0.001847 };
	fNames.push_back( resourcePath + "Cover_GRASP_SCL.stl" );       Eigen::Vector3d cover_shift{   0.03 , 0.05 , 0.002669 };
	//~ fNames.push_back( resourcePath + "Shell_SCL.stl" );  	        Eigen::Vector3d shell_shift{   0.0       , 0.0  , -0.0375   };
	fNames.push_back( resourcePath + "Shell_GRASP_SCL.stl" );  	    Eigen::Vector3d shell_shift{   0.0       , 0.0  , -0.03125   };
	
	
	
	// ~~ 1. Create parts ~~
	size_t fLen = fNames.size();
	std::vector<Part_ASM> partsVec;
	for( size_t i = 0 ; i < fLen ; i++ ){
		cout << fNames[i] << endl;
		partsVec.push_back( Part_ASM{   assign_part_ID() , fNames[i]   } );
	}
	
if( is_arg_in_vector( (llin) 0 , partIDs ) ){
	rtnAsm->add_part_w_pose( partsVec[ 0] , Pose_ASP{ -base_shift  , 
													   no_turn_quat()  } );
}
if( is_arg_in_vector( (llin) 1 , partIDs ) ){
	rtnAsm->add_part_w_pose( partsVec[ 1] , Pose_ASP{ -motor_shift + Eigen::Vector3d( 0.0 , 0.0 , 0.03 ) , 
													   no_turn_quat()  } );
}
if( is_arg_in_vector( (llin) 2 , partIDs ) ){
	rtnAsm->add_part_w_pose( partsVec[ 2] , Pose_ASP{ -bushing_shift + Eigen::Vector3d( -0.0225 , -0.0225 , 0.005 ) , 
													   Eigen::AngleAxisd( M_PI / 52.0 , Eigen::Vector3d::UnitZ() ) * no_turn_quat()  } );
}
if( is_arg_in_vector( (llin) 3 , partIDs ) ){
	rtnAsm->add_part_w_pose( partsVec[ 3] , Pose_ASP{ -bushing_shift + Eigen::Vector3d(  0.0225 ,  0.0225 , 0.005 ) , 
													   Eigen::AngleAxisd( M_PI / 52.0 , Eigen::Vector3d::UnitZ() ) * no_turn_quat()  } );
}
if( is_arg_in_vector( (llin) 4 , partIDs ) ){
	rtnAsm->add_part_w_pose( partsVec[ 4] , Pose_ASP{ -pillar_shift + Eigen::Vector3d( -0.04 ,  -0.015 , 0.025 ) , 
													   Eigen::AngleAxisd( M_PI / 52.0 , Eigen::Vector3d::UnitZ() ) * no_turn_quat()  } );
}
if( is_arg_in_vector( (llin) 5 , partIDs ) ){
	rtnAsm->add_part_w_pose( partsVec[ 5] , Pose_ASP{ -pillar_shift + Eigen::Vector3d(  0.04 ,   0.015 , 0.025 ) , 
													   Eigen::AngleAxisd( M_PI / 52.0 , Eigen::Vector3d::UnitZ() ) * no_turn_quat()  } );
}
if( is_arg_in_vector( (llin) 6 , partIDs ) ){
	rtnAsm->add_part_w_pose( partsVec[ 6] , Pose_ASP{ -cover_shift + Eigen::Vector3d(  0.03 ,   0.05 , 0.0725 ) , 
													   Eigen::Quaterniond( 0.707 , 0.000 , 0.000 , 0.707 )  } );
}
if( is_arg_in_vector( (llin) 7 , partIDs ) ){
	rtnAsm->add_part_w_pose( partsVec[ 7] , Pose_ASP{ -shell_shift + Eigen::Vector3d(  0.00 ,   0.00 , 0.005 ) , 
													   no_turn_quat()  } );
}	
	
	
	
	// 3. Determine the local freedom of each part (NDBG)
	rtnAsm->recalc_geo();
		
	return rtnAsm;
}

Assembly_ASM* drive_motor(){
	std::vector<llin> partIDs = vec_range( (llin) 0 , (llin) 7 );
	return drive_motor( partIDs );
}

/// ~~~ Toy Plane , Lee ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Assembly_ASM* toy_plane( std::vector<llin>& partIDs ){
	// NOTE: This function does not enforce connectivity

	Incrementer assign_part_ID{ 0 }; // Instantiate a functor that will assign a unique number to each part
    Incrementer assign_asm_ID{ 0 }; //- Instantiate a functor that will assign a unique number to each assembly
    
	Assembly_ASM* rtnAsm = new Assembly_ASM( assign_asm_ID() );
	
	// ~~ 0. Create file paths ~~
	// ~ Fetch the folder to the STL files ~
	string pkgPath = ros::package::getPath( "motion_cost" );
	string resourcePath = pkgPath + "/Resources/Assembly_CAD/Plane_Lee/";
	// ~ Create full paths to STLs ~
	std::vector<string> fNames;
	
	fNames.push_back( resourcePath + "Block_SMP_SCL.stl" );  		Eigen::Vector3d block_shift{    0.0 , 0.0 ,  0.0      };
	fNames.push_back( resourcePath + "Block_SMP_SCL.stl" );  		
	fNames.push_back( resourcePath + "Block_SMP_SCL.stl" );  		
	fNames.push_back( resourcePath + "Bar7_SMP_SCL.stl" );  		Eigen::Vector3d bar7_shift{     0.0 , 0.0 , -0.005    };
	fNames.push_back( resourcePath + "Bar7_SMP_SCL.stl" );  		
	fNames.push_back( resourcePath + "Bar7_SMP_SCL.stl" );  		
	fNames.push_back( resourcePath + "Bar7_SMP_SCL.stl" );  		
	fNames.push_back( resourcePath + "Bar3_SMP_SCL.stl" );  		Eigen::Vector3d bar3_shift{     0.0 , 0.0 , -0.005    };
	fNames.push_back( resourcePath + "Bar3_SMP_SCL.stl" );  		
	fNames.push_back( resourcePath + "BoltLong_SCL.stl" );  	Eigen::Vector3d bltLong_shift{  0.0 , 0.0 , -0.046145 };
	fNames.push_back( resourcePath + "BoltLong_SCL.stl" );  	
	fNames.push_back( resourcePath + "BoltShort_SCL.stl" );		Eigen::Vector3d bltShort_shift{ 0.0 , 0.0 , -0.019702 };
	fNames.push_back( resourcePath + "BoltShort_SCL.stl" );		
	fNames.push_back( resourcePath + "Wheel_SMP_SCL.stl" );  		Eigen::Vector3d wheel_shift{    0.0 , 0.0 , -0.005    };
	fNames.push_back( resourcePath + "Wheel_SMP_SCL.stl" );  		
	fNames.push_back( resourcePath + "BoltShort_SCL.stl" );
	fNames.push_back( resourcePath + "BoltShort_SCL.stl" );
	
	
	// ~~ 1. Create parts ~~
	size_t fLen = fNames.size();
	std::vector<Part_ASM> partsVec;
	for( size_t i = 0 ; i < fLen ; i++ ){
		cout << fNames[i] << endl;
		partsVec.push_back( Part_ASM{   assign_part_ID() , fNames[i]   } );
	}
	
	double sideLen = 0.040 ,
		   platThc = 0.010 ;
	
if( is_arg_in_vector( (llin) 0 , partIDs ) ){
	rtnAsm->add_part_w_pose( partsVec[ 0] , Pose_ASP{ -block_shift  , 
													   no_turn_quat()  } );
}
if( is_arg_in_vector( (llin) 1 , partIDs ) ){
    rtnAsm->add_part_w_pose( partsVec[ 1] , Pose_ASP{ -block_shift + Eigen::Vector3d( sideLen , 0 , 0 ) , 
													   no_turn_quat()  } );
}
if( is_arg_in_vector( (llin) 2 , partIDs ) ){
    rtnAsm->add_part_w_pose( partsVec[ 2] , Pose_ASP{ -block_shift + Eigen::Vector3d( 0 , 0 , -sideLen ) , 
													   no_turn_quat()  } );
}
if( is_arg_in_vector( (llin) 3 , partIDs ) ){
	rtnAsm->add_part_w_pose( partsVec[ 3] , Pose_ASP{ -bar7_shift + Eigen::Vector3d( -0.080 , 0 , sideLen/2.0 ) , 
													   no_turn_quat()  } );
}
if( is_arg_in_vector( (llin) 4 , partIDs ) ){
	rtnAsm->add_part_w_pose( partsVec[ 4] , Pose_ASP{ -bar7_shift + Eigen::Vector3d( 0 , 0 , sideLen/2.0 + platThc ) , 
													   Eigen::Quaterniond(  0.707 ,  0.000 ,  0.000 ,  0.707 )  } );
}
if( is_arg_in_vector( (llin) 5 , partIDs ) ){
	rtnAsm->add_part_w_pose( partsVec[ 5] , Pose_ASP{ -bar7_shift + Eigen::Vector3d( -sideLen , 0 , sideLen/2.0 + platThc ) , 
													   Eigen::Quaterniond(  0.707 ,  0.000 ,  0.000 ,  0.707 )  } );
}
if( is_arg_in_vector( (llin) 6 , partIDs ) ){
	rtnAsm->add_part_w_pose( partsVec[ 6] , Pose_ASP{ -bar7_shift + Eigen::Vector3d( -sideLen*5 , 0 , sideLen/2.0 + platThc ) , 
													   Eigen::Quaterniond(  0.707 ,  0.000 ,  0.000 ,  0.707 )  } );
}
if( is_arg_in_vector( (llin) 7 , partIDs ) ){
	rtnAsm->add_part_w_pose( partsVec[ 7] , Pose_ASP{ -bar3_shift + Eigen::Vector3d( 3.0*sideLen/2.0 + platThc/2.0 , 0 , -platThc/2.0 ) , 
													   Eigen::Quaterniond(  0.653 ,  0.271 ,  0.653 ,  0.271 )  } );
}
if( is_arg_in_vector( (llin) 8 , partIDs ) ){
	rtnAsm->add_part_w_pose( partsVec[ 8] , Pose_ASP{ -bar3_shift + Eigen::Vector3d( 3.0*sideLen/2.0 + 3.0*platThc/2.0 , 0 , -platThc/2.0 ) , 
													   Eigen::Quaterniond(  0.271 ,  0.653 ,  0.271 ,  0.653 )  } );
}
if( is_arg_in_vector( (llin) 9 , partIDs ) ){
	rtnAsm->add_part_w_pose( partsVec[ 9] , Pose_ASP{ -bltLong_shift + Eigen::Vector3d( 0 , 0 , -sideLen + platThc ) , 
													   Eigen::AngleAxisd( M_PI / 52.0 , Eigen::Vector3d::UnitZ() ) * no_turn_quat()  } );
}
if( is_arg_in_vector( (llin)10 , partIDs ) ){
	rtnAsm->add_part_w_pose( partsVec[10] , Pose_ASP{ -bltLong_shift + Eigen::Vector3d( sideLen + 3*platThc/2.0 + 0.001 , 0 , -sideLen -0.006 ) , 
													   Eigen::Quaterniond(  0.707 ,  0.000 ,  0.707 ,  0.000 )  } );
}
if( is_arg_in_vector( (llin)11 , partIDs ) ){
	rtnAsm->add_part_w_pose( partsVec[11] , Pose_ASP{ -bltShort_shift + Eigen::Vector3d( -sideLen , 0 , -sideLen + platThc + 0.050 ) , 
													   Eigen::AngleAxisd( M_PI / 52.0 , Eigen::Vector3d::UnitZ() ) * no_turn_quat()  } );
}
if( is_arg_in_vector( (llin)12 , partIDs ) ){
	rtnAsm->add_part_w_pose( partsVec[12] , Pose_ASP{ -bltShort_shift + Eigen::Vector3d( -sideLen*5 , 0 , -sideLen + platThc + 0.050 ) , 
													   Eigen::AngleAxisd( M_PI / 52.0 , Eigen::Vector3d::UnitZ() ) * no_turn_quat()  } );
}
if( is_arg_in_vector( (llin)13 , partIDs ) ){
	rtnAsm->add_part_w_pose( partsVec[13] , Pose_ASP{ -wheel_shift + Eigen::Vector3d( 0 , sideLen/2.0+0.005 , -sideLen-0.005 ) , 
													   Eigen::Quaterniond(  0.707 ,  0.707 ,  0.000 ,  0.000 )  } );
}
if( is_arg_in_vector( (llin)14 , partIDs ) ){
	rtnAsm->add_part_w_pose( partsVec[14] , Pose_ASP{ -wheel_shift + Eigen::Vector3d( 0 , -sideLen/2.0-0.005 , -sideLen-0.005  ) , 
													   Eigen::Quaterniond(  0.707 ,  0.707 ,  0.000 ,  0.000 )  } );
}
if( is_arg_in_vector( (llin)15 , partIDs ) ){
	rtnAsm->add_part_w_pose( partsVec[15] , Pose_ASP{ -bltShort_shift + Eigen::Vector3d( 0 , +sideLen/2.0+platThc - 0.0005 , -3.0*sideLen/2.0 ) , 
													   Eigen::Quaterniond(  0.707 , -0.707 ,  0.000 ,  0.000 )  } );
}
if( is_arg_in_vector( (llin)16 , partIDs ) ){
	rtnAsm->add_part_w_pose( partsVec[16] , Pose_ASP{ -bltShort_shift + Eigen::Vector3d( 0 , -sideLen/2.0-platThc + 0.0005 , -3.0*sideLen/2.0 ) , 
													   Eigen::Quaterniond(  0.707 ,  0.707 ,  0.000 ,  0.000 )  } );
}	
	// 3. Determine the local freedom of each part (NDBG)
	rtnAsm->recalc_geo();
		
	return rtnAsm;
}

Assembly_ASM* toy_plane(){
	std::vector<llin> partIDs = vec_range( (llin)  0 , (llin) 16 );
	return toy_plane( partIDs );
}

/// ~~~ Validator , Kheder ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Assembly_ASM* validator( std::vector<llin>& partIDs ){
	// NOTE: This function does not enforce connectivity

	Incrementer assign_part_ID{ 0 }; // Instantiate a functor that will assign a unique number to each part
    Incrementer assign_asm_ID{ 0 }; //- Instantiate a functor that will assign a unique number to each assembly
    
	Assembly_ASM* rtnAsm = new Assembly_ASM( assign_asm_ID() );
	
	// ~~ 0. Create file paths ~~
	// ~ Fetch the folder to the STL files ~
	string pkgPath = ros::package::getPath( "motion_cost" );
	string resourcePath = pkgPath + "/Resources/Assembly_CAD/ValidatorKheder/";
	// ~ Create full paths to STLs ~
	std::vector<string> fNames;
	
	fNames.push_back( resourcePath + "CeeBig_SCL.stl" );  		Eigen::Vector3d CeeBig_shift{   -0.03      , 0.0      , 0.016132  };
	fNames.push_back( resourcePath + "CeeLtl_SCL.stl" );  		Eigen::Vector3d CeeLtl_shift{    0.000193  , 0.000527 , -0.005    };
	fNames.push_back( resourcePath + "Pillar_SCL.stl" );  		Eigen::Vector3d Pillar_shift{    0.0       , 0.0      , -0.01     };
	fNames.push_back( resourcePath + "Pillar_SCL.stl" );  		
	fNames.push_back( resourcePath + "Bar_SCL.stl" );  			Eigen::Vector3d Bar_shift{       0.0       , 0.0      , -0.005    };
	fNames.push_back( resourcePath + "BoltShort_SCL.stl" ); 	Eigen::Vector3d BoltShort_shift{ 0.0       , 0.0      , -0.016437 };
	fNames.push_back( resourcePath + "BoltShort_SCL.stl" ); 	
	fNames.push_back( resourcePath + "BoltShort_SCL.stl" ); 	
	fNames.push_back( resourcePath + "BoltShort_SCL.stl" ); 	
	fNames.push_back( resourcePath + "BoltLong_SCL.stl" );  	Eigen::Vector3d BoltLong_shift{  0.0       , 0.0      , -0.036153 };
	fNames.push_back( resourcePath + "BoltLong_SCL.stl" );  	
	fNames.push_back( resourcePath + "NutSmall_SCL.stl" );  	Eigen::Vector3d NutSmall_shift{  0.0       , 0.0      , -0.0045   };
	fNames.push_back( resourcePath + "NutSmall_SCL.stl" );  	
	fNames.push_back( resourcePath + "BoltLrg_SCL.stl" );  		Eigen::Vector3d BoltLrg_shift{   0.0       , 0.060278 ,  0.0      };
	fNames.push_back( resourcePath + "NutLarge_SCL.stl" );		Eigen::Vector3d NutLarge_shift{  0.0       , 0.0045   ,  0.0      };
	
	// ~~ 1. Create parts ~~
	size_t fLen = fNames.size();
	std::vector<Part_ASM> partsVec;
	for( size_t i = 0 ; i < fLen ; i++ ){
		cout << fNames[i] << endl;
		partsVec.push_back( Part_ASM{   assign_part_ID() , fNames[i]   } );
	}
	
if( is_arg_in_vector( (llin) 0 , partIDs ) ){
	rtnAsm->add_part_w_pose( partsVec[ 0] , Pose_ASP{ -CeeBig_shift  , 
													   no_turn_quat()  } );
}
if( is_arg_in_vector( (llin) 1 , partIDs ) ){
	rtnAsm->add_part_w_pose( partsVec[ 1] , Pose_ASP{ -CeeLtl_shift + Eigen::Vector3d( 0.02075 , 0.000375 , -0.090 ) ,
													   Eigen::Quaterniond(  0.707 ,  0.000 ,  0.000 ,  0.707 )  } );
}
if( is_arg_in_vector( (llin) 2 , partIDs ) ){
	rtnAsm->add_part_w_pose( partsVec[ 2] , Pose_ASP{ -Pillar_shift + Eigen::Vector3d( 0.010 ,  0.020 , -0.110 ) ,
													   no_turn_quat()  } );
}
if( is_arg_in_vector( (llin) 3 , partIDs ) ){
	rtnAsm->add_part_w_pose( partsVec[ 3] , Pose_ASP{ -Pillar_shift + Eigen::Vector3d( 0.010 , -0.020 , -0.110 ) ,
													   no_turn_quat()  } );
}
if( is_arg_in_vector( (llin) 4 , partIDs ) ){
	rtnAsm->add_part_w_pose( partsVec[ 4] , Pose_ASP{ -Bar_shift + Eigen::Vector3d( 0.010 , 0.0 , -0.120 ) ,
													   Eigen::Quaterniond(  0.707 ,  0.000 ,  0.000 ,  0.707 )  } );
}
if( is_arg_in_vector( (llin) 5 , partIDs ) ){
	rtnAsm->add_part_w_pose( partsVec[ 5] , Pose_ASP{ -BoltShort_shift + Eigen::Vector3d( 0.010 ,  0.060 , -0.107875 ) ,
													   Eigen::Quaterniond(  0.000 ,  0.000 ,  1.000 ,  0.000 )  } );
}
if( is_arg_in_vector( (llin) 6 , partIDs ) ){
	rtnAsm->add_part_w_pose( partsVec[ 6] , Pose_ASP{ -BoltShort_shift + Eigen::Vector3d( 0.050 ,  0.060 , -0.107875 ) ,
													   Eigen::Quaterniond(  0.000 ,  0.000 ,  1.000 ,  0.000 )  } );
}
if( is_arg_in_vector( (llin) 7 , partIDs ) ){
	rtnAsm->add_part_w_pose( partsVec[ 7] , Pose_ASP{ -BoltShort_shift + Eigen::Vector3d( 0.010 , -0.060 , -0.107875 ) ,
													   Eigen::Quaterniond(  0.000 ,  0.000 ,  1.000 ,  0.000 )  } );
}
if( is_arg_in_vector( (llin) 8 , partIDs ) ){
	rtnAsm->add_part_w_pose( partsVec[ 8] , Pose_ASP{ -BoltShort_shift + Eigen::Vector3d( 0.050 , -0.060 , -0.107875 ) ,
													   Eigen::Quaterniond(  0.000 ,  0.000 ,  1.000 ,  0.000 )  } );
}
if( is_arg_in_vector( (llin) 9 , partIDs ) ){
	rtnAsm->add_part_w_pose( partsVec[ 9] , Pose_ASP{ -BoltLong_shift + Eigen::Vector3d( 0.010 , -0.020 , -0.107875 - 0.0220 ) ,
													   no_turn_quat()  } );
}
if( is_arg_in_vector( (llin)10 , partIDs ) ){
	rtnAsm->add_part_w_pose( partsVec[10] , Pose_ASP{ -BoltLong_shift + Eigen::Vector3d( 0.010 ,  0.020 , -0.107875 - 0.0220 ) ,
													   no_turn_quat()  } );
}
if( is_arg_in_vector( (llin)11 , partIDs ) ){
	rtnAsm->add_part_w_pose( partsVec[11] , Pose_ASP{ -NutSmall_shift + Eigen::Vector3d( 0.010 ,  0.020 , -0.107875 - 0.021125 ) ,
													   no_turn_quat()  } );
}
if( is_arg_in_vector( (llin)12 , partIDs ) ){
	rtnAsm->add_part_w_pose( partsVec[12] , Pose_ASP{ -NutSmall_shift + Eigen::Vector3d( 0.010 , -0.020 , -0.107875 - 0.021125 ) ,
													   no_turn_quat()  } );
}
if( is_arg_in_vector( (llin)13 , partIDs ) ){
	rtnAsm->add_part_w_pose( partsVec[13] , Pose_ASP{ -BoltLrg_shift + Eigen::Vector3d( 0.030 ,  0.070 , -0.030 ) ,
													   no_turn_quat()  } );												   
}
if( is_arg_in_vector( (llin)14 , partIDs ) ){
	rtnAsm->add_part_w_pose( partsVec[14] , Pose_ASP{ -NutLarge_shift + Eigen::Vector3d( 0.030 , -0.070 , -0.030 ) ,
													   no_turn_quat()  } );
}	
	// 3. Determine the local freedom of each part (NDBG)
	rtnAsm->recalc_geo();
		
	return rtnAsm;
}

Assembly_ASM* validator( ){
	std::vector<llin> partIDs = vec_range( (llin) 0 , (llin) 14 );
	return validator( partIDs );
}

// ___ End Assembly ________________________________________________________________________________________________________________________




/* === Spare Parts =========================================================================================================================



   ___ End Parts ___________________________________________________________________________________________________________________________

*/
