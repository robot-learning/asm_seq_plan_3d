/***********  
Motion_Planning.cpp
James Watson , 2018 June
Grasp Selection , Grasp Planning , and Arm Motion

Template Version: 2018-06-07
***********/

#include "Motion_Planning.h"

/// === ARM SERVICE WRAPPERS ===============================================================================================================

std::vector<double> request_IK_solution::operator()( Pose_ASP X_desired , const std::vector<double>& jointSeed , size_t attemptLimit ){
    // Attempt to obtain an IK solution from 'serviceCallFunc' for 'X_desired' for up to 'attemptLimit' , using 'jointSeed' as a starting point
                                             
    bool SHOWDEBUG = false; // if( SHOWDEBUG ){  cout << "" << endl;  }
                                             
    bool   isValid  = false;
    size_t attempts = 0;
    std::vector<double> rtnSoln;
    // 1. Create a service message
    motion_cost::qFromPose  srvMsg; //- Message for communication with the solver
    sensor_msgs::JointState seedMsg; // Seed joint state for the next request
    geometry_msgs::Pose     currPoseMSG = ROS_Pose_from_Pose_ASP( X_desired );
    // A. Load seed angles
    seedMsg.position = jointSeed;
    srvMsg.request.request.seed = seedMsg;
    
    // 2. While there is not a valid solution and the request limit has not been reached
    while(  !isValid  &&  ( attempts < attemptLimit )  ){
        // 3. Populate the message with the request data
        srvMsg.request.request.worldPose = currPoseMSG;
        // 4. Call the service
        if( IKclient.call( srvMsg ) ){
            if( SHOWDEBUG ){  cout << "Solution: " << srvMsg.response.response.qSoln.position << endl;   
                              ros_log( "Response returned!"    , INFO );                               }
            isValid = srvMsg.response.response.valid;
        }else if( SHOWDEBUG ){  ros_log( "NO response returned!" , WARN );  }
        // 5. If the response is valid
        if( isValid ){	
            // 6. Unpack the solution
            rtnSoln = srvMsg.response.response.qSoln.position;  
        }else if( SHOWDEBUG ){  cout << "Invalid! , Attempt " << attempts + 1 << " of " << attemptLimit << endl;  }
        // 7. Increment attempts
        attempts++;
    }
    if( SHOWDEBUG ){  cout << "Retuning a valid solution?: " << isValid << endl;  }
    // 8. Return the solution, Whether it is valid or not
    return ( isValid ? rtnSoln : vec_nan( jointSeed.size() ) );
}

Eigen::MatrixXd request_Jacobian::operator()( const std::vector<double>& q_joints7 ){
    
    bool SHOWDEBUG = false; // if( SHOWDEBUG ){  cout << "" << endl;  }
    
    if( SHOWDEBUG ){  cout << "Received Joint Angles: " << q_joints7 << endl;  }
    
    Eigen::MatrixXd rtnMatx = Eigen::MatrixXd::Constant( 6 , 7 , nan("") );
    ll4ma_teleop::Joints2Jac srvMsg;
    srvMsg.request.JointPos = q_joints7;
    if( Jclient.call( srvMsg ) ){
        if( SHOWDEBUG ){  ros_log( "Got a Jacobian response!" , INFO );  }
        rtnMatx = extract_Jacobian( srvMsg );
    }else if( SHOWDEBUG ){  ros_log( "There was NO Jacobian response!" , WARN );  }
    return rtnMatx;
}


std::vector<Pose_ASP> request_FK_poses::operator()( const std::vector<double>& q_joints7 ){
    
    bool SHOWDEBUG = false; // if( SHOWDEBUG ){  cout << "" << endl;  }
    
    std::vector<Pose_ASP> rtnVec;
    // 0. Init message
    ll4ma_teleop::LinkPoses srvMsg;
    // 1. Load request
    srvMsg.request.JointPos = q_joints7;
    // 2. Fetch solutions
    if( FKclient.call( srvMsg ) ){  
        rtnVec = load_ASP_Poses_from_Poses_ROS( srvMsg.response.allFrames );
        if( SHOWDEBUG ){  cout << "Got a response , Loaded " << rtnVec.size() << " poses." << endl;  }
    }else if( SHOWDEBUG ){  ros_log( "There was NO pose response!" , WARN );  }
    return rtnVec;
}


std::vector<Pose_ASP> request_FNG_poses::operator()( const std::vector<double>& q_joints9 ){
    bool SHOWDEBUG = true; // if( SHOWDEBUG ){  cout << "" << endl;  }
    
    std::vector<Pose_ASP> rtnVec;
    // 0. Init message
    ll4ma_teleop::FngrPoses srvMsg;
    // 1. Load request
    srvMsg.request.JointPos = q_joints9;
    // 2. Fetch solutions
    if( FNGclient.call( srvMsg ) ){  
        rtnVec = load_ASP_Poses_from_Poses_ROS( srvMsg.response.fngFrames );
        if( SHOWDEBUG ){  cout << "Got a response , Loaded " << rtnVec.size() << " poses." << endl;  }
    }else if( SHOWDEBUG ){  ros_log( "There was NO pose response!" , WARN );  }
    return rtnVec;
}



/// ___ END SERVICES _______________________________________________________________________________________________________________________


/// === GRASP PLANNING FUNCTIONS ===========================================================================================================

bool priorityQ_less_than_ParGrasp::operator()( const std::pair<double,ParallelGraspState>& op1 , const std::pair<double,ParallelGraspState>& op2 ) const{
    return std::get<0>( op1 ) < std::get<0>( op2 );
}

bool priorityQ_greater_than_ParGrasp::operator()( const std::pair<double,ParallelGraspState>& op1 , const std::pair<double,ParallelGraspState>& op2 ) const{
    return std::get<0>( op1 ) > std::get<0>( op2 );
}


std::vector<ParallelGraspState> rank_grasp_pairs( Assembly_ASM* targetAsm , 
												  Eigen::Vector3d approachFrom , Eigen::Vector3d robotBase , Eigen::Vector3d workCenter , 
												  Eigen::MatrixXd approaches , double tileSize ){
	// Rank grasps generated by shooting rays from different directions tiled at the spacing specified
	
	// NOTE: This function assumes that the pose of 'targetAsm' has been set
	// NOTE: This function returns a grasp target in the **lab** frame 
	
	// FUTURE: This function should take feasibility into account
	// FUTURE: This function should take manipulability into account
	// FUTURE: This function should reject grasps in collision
	
	Eigen::Vector3d curDir; // ------------------- Current direction to shoot rays
	Eigen::Vector3d perpAxis{ 1 , 1.5 , 1.75 }; // Anything different from 'curDir', used in axes construction , Don't care
	Eigen::Vector3d pntN; // --------------------- Current entry point
	Eigen::Vector3d pntX; // --------------------- Current exit point
	Eigen::Vector3d X_opt1; // ------------------- First 'xBasis' choice
	Eigen::Vector3d X_opt2; // ------------------- Second 'xBasis' choice
	Eigen::Vector3d Z_opt1; // ------------------- First 'zBasis' choice
	Eigen::Vector3d Z_opt2; // ------------------- Second 'zBasis' choice
	Eigen::Vector3d fromRobot; // ---------------- Direction pointing from robot base to grasp center
	
	Eigen::Vector3d origin; // grasp center
	Eigen::Vector3d xBasis; // grasp 'xBasis'
	Eigen::Vector3d yBasis; // grasp 'yBasis'
	Eigen::Vector3d zBasis; // grasp 'zBasis'
	
	double approachDist = 2.0000 , // [m] , Rays approach from this far away from asm
		   ANGLE_CRIT   = 0.2618 , // [rad] , 15deg , Reject z choice if angle between this and x axis is below this criterion
		   gScore       = 0.0000 , // Grasp score, lower is better
		   separation   = 0.0000 , // Grasp width
		   PROB_SCALE   = 0.5    , // [m] , Problem scale , Max distance that gripper should be from the problem center
		   MAX_ANGLE    = M_PI / 2.0;
	
	std::priority_queue< std::pair< double , ParallelGraspState >                , 
                         std::vector< std::pair< double , ParallelGraspState > > ,
                         priorityQ_greater_than_ParGrasp                         > ranking; // Sort grasps by increasing score
	
	size_t numDirs = approaches.rows();
	// 1. For each approach
	for( size_t i = 0 ; i < numDirs ; i++ ){
		// A. Calc pairs
		curDir = approaches.row(i);
		RayHits graspPairs = get_opposite_grasp_pairs_for_ASM_and_direction( targetAsm , 
																			 curDir , 
																		     perpAxis , approachDist ,
																		     tileSize );
		size_t numPairs = graspPairs.enter.rows();
		// 2. For each pair
		for( size_t j = 0 ; j < numPairs ; j++ ){
			// 3. Calc info to generate a grasp
			pntN       = graspPairs.enter.row(j);
			pntX       = graspPairs.exit.row(j);
			separation = ( pntN - pntX ).norm();
			origin     = ( pntN + pntX ) / 2.0;
			fromRobot  = ( origin - robotBase ).normalized();
			// A. Choose X along grasp points, 2 possible, Prefer an X that points away from the robot base
			X_opt1 = ( pntN - pntX ).normalized();
			X_opt2 = ( pntX - pntN ).normalized();
			if( X_opt1.dot( fromRobot ) > X_opt2.dot( fromRobot ) ){  xBasis = X_opt1;  }else{  xBasis = X_opt2;  }
			// B. Choose Z guide that points towards 'approachCenter'
			Z_opt1 = ( approachFrom - origin ).normalized();
			Z_opt2 = ( robotBase - origin ).normalized();
			// C. Compute Y
			if( angle_between( xBasis , Z_opt1 ) > ANGLE_CRIT ){
				yBasis = Z_opt1.cross( xBasis ).normalized();
			// i. If cannot choose a direction perpendicular to X, then Z should point towards robot base	
			}else if( angle_between( xBasis , Z_opt2 ) > ANGLE_CRIT ){
				yBasis = Z_opt2.cross( xBasis ).normalized();
			// ii. If neither of these can be chosen perpendicular to X, then grasp fails, continue to next	
			}else{  continue;  }
			// D. Compute Z
			zBasis = xBasis.cross( yBasis ).normalized();
			// 4. Compute score
			gScore = zBasis.dot( Z_opt1 ) + ( workCenter - origin ).norm() / PROB_SCALE; // The lower the better
			// 5. Create the grasp target object
			std::vector<double> q_fingers = { separation / 2.0 , separation / 2.0 };
			ParallelGraspState temp{  
				pose_from_origin_bases( origin ,
										xBasis , yBasis , zBasis ) , 
				q_fingers , 
				separation <= MAX_BAXTER_GRIP_SEP ,
				gScore , // The lower the better
				min( graspPairs.n_Metric(j,0) , graspPairs.x_Metric(j,0) )
			};
			// 6. Push grasp onto queue
			ranking.push( std::make_pair( gScore , temp ) );
		}
	}
	// 7. Unspool queue
	std::vector<ParallelGraspState> rtnVec;
	while( ranking.size() > 0 ){
		rtnVec.push_back(  std::get<1>( ranking.top() )  ); // Fetch the state with the least score
		ranking.pop(); // ------------------------------------ Erase the state with the least score
	}
	return rtnVec;
}

std::vector<ParallelGraspState> rank_grasp_pairs( const RayHits& allPairs , 
												  const Eigen::Vector3d& preferredWristDir , const Eigen::Vector3d& preferredCenter , 
												  double dTheta , double CRIT_ANG ){
	// Generate grasps given a set of grasp pairs. // Hopefully a bit simpler than the above?
	// NOTE: This is to rank grasps by preferred position and wrist direction and to presuppose as few criteria as possible
	//       Let the planner sort it out!
	
	bool SHOWDEBUG = false;  // if( SHOWDEBUG ){  cout << "" << endl;  }
	
	std::priority_queue< std::pair< double , ParallelGraspState >                , 
                         std::vector< std::pair< double , ParallelGraspState > > ,
                         priorityQ_less_than_ParGrasp                            > ranking; // Sort grasps by increasing score
	
	size_t numPairs = allPairs.enter.rows();
	size_t /* -- */ Adex = 0 , /* --------- */ Bdex = 0 ;
	Eigen::Vector3d Aside;     Eigen::Vector3d Bside;  
	Eigen::MatrixXd sides = Eigen::MatrixXd::Zero( 2 , 3 );
	Eigen::Vector3d origin;
	Eigen::Vector3d xBasis , yBasis , zBasis;
	Eigen::Vector3d /* -- */ yIncr  , zIncr;
	// Get the vector of angles
	std::vector<double> angleIncr = linspace( (double)0 , (double)M_PI , (size_t) ceil( M_PI / dTheta ) + 1 );
	std::vector<double> signSelect = {  1 , -1 };
	std::vector<double> q_fingers;
	size_t /* ------ */ angleNum = angleIncr.size();
	// 0. Get the furthest distance to any hit. This is the scale for the distance
	double farthestDist = greatest_dist_from_pnt_to_hit( preferredCenter , allPairs ) , // Scale for distance measure
		   linScore     = 0.0 ,
		   angScore     = 0.0 ,
		   sgnMlt       = 1.0 ,
		   separation   = 0.0 ,
		   MAX_ANGLE    = M_PI / 2.0; // Downward pointing grasps will be stupid
	// 1. For each grasp pair
	for( size_t i = 0 ; i < numPairs ; i++ ){
		// 2. for wrist flip and wrist flop
		sides.row(0) = allPairs.enter.row(i);
		sides.row(1) = allPairs.exit.row(i);
		if( SHOWDEBUG ){  cout << "Enter: " << allPairs.enter.row(i) << " , Exit: " << allPairs.exit.row(i) <<  endl;  }
		for( size_t j = 1 ; j < 3 ; j++ ){
			//              j ==    1 , 2
			Adex = is_even( j ) ? 1 : 0; /* 0 , 1 */  Aside = sides.row( Adex );
			Bdex = is_odd(  j ) ? 1 : 0; /* 1 , 0 */  Bside = sides.row( Bdex );
			
			if( SHOWDEBUG ){  cout << "j: " << j << " , j Even?: " << is_even( j ) << " , j Odd?: " << is_odd(  j ) <<  endl
							       << "Adex: " << Adex << " , Bdex: " << Bdex <<  endl;  }
			
			xBasis = ( Aside - Bside ).normalized();
			// 3. Get the origin and bases 
			origin   = ( Aside + Bside ) / 2.0;
			separation = ( Aside - Bside ).norm();
			
			if( SHOWDEBUG ){  cout << "Aside: " << Aside << " , Bside: " << Bside <<  endl;  }
			
			// 5. Get the distance score = Distance / Scale
			linScore = ( origin - preferredCenter ).norm();
			if(  ( angle_between( xBasis , preferredWristDir )               < CRIT_ANG )  ||  
				 ( abs( angle_between( xBasis , preferredWristDir ) - M_PI ) < CRIT_ANG )  ){
				Eigen::MatrixXd bases = get_any_orthogBasis_for_X( xBasis );
				yBasis = bases.row(1);
				zBasis = bases.row(2);
			}else{
				yBasis = preferredWristDir.cross( xBasis );
				zBasis = xBasis.cross( yBasis );
			}
			
			if( SHOWDEBUG ){  cout << "xBasis: " << xBasis << " , " << "yBasis: " << yBasis << " , " << "zBasis: " << zBasis << endl;  }
			
			// 4. For each increment of dTheta in [ 0 , pi ]
			for( size_t k = 0 ; k < angleNum ; k++ ){
				// 5. For positive and negative
				for( size_t m = 0 ; m < 2 ; m++ ){
					sgnMlt = signSelect[m];
					yIncr  = Eigen::AngleAxisd( sgnMlt * angleIncr[k] , xBasis ) * yBasis;
					zIncr  = Eigen::AngleAxisd( sgnMlt * angleIncr[k] , xBasis ) * zBasis;
					
					if( angle_between( preferredWristDir , zIncr ) <= MAX_ANGLE ){
					
						// 6. Get the angle score = angle_between( grasp Z , preferredWristDir ) / pi
						angScore = angle_between( zIncr , preferredWristDir ) / M_PI;
						// 7. Calc score , and push onto queue (least score is best)
						q_fingers = { separation / 2.0 , separation / 2.0 };
						
						if( SHOWDEBUG ){  cout << pose_from_origin_bases( origin , xBasis , yIncr , zIncr ) << endl;  }
						
						ParallelGraspState temp{  
							pose_from_origin_bases( origin ,
													xBasis , yIncr , zIncr ) , 
							q_fingers , 
							separation <= MAX_BAXTER_GRIP_SEP ,
							linScore + angScore , // The lower the better
							min( allPairs.n_Metric(i,0) , allPairs.x_Metric(i,0) )
						};
						ranking.push( std::make_pair( linScore + angScore , temp ) );
					}
				}
			}
		}
	}
	// 8. Unspool queue in increasing score order
	std::vector<ParallelGraspState> rtnVec;
	while( ranking.size() > 0 ){
		rtnVec.push_back(  std::get<1>( queue_get_pop( ranking ) )  ); // Fetch the state with the least score  &&  Erase the state with the least score
	}
	// 9. Return the grasps
	return rtnVec;
}

// ~~ Manipulation Planning ~~

RayHits get_opposite_grasp_pairs_for_ASM_and_direction( Assembly_ASM* assembly , 
														const Eigen::Vector3d& direction , const Eigen::Vector3d& xDir , double distance ,
														double tileSize ){
	// Shoot a square grid of rays into the 'assembly' along the 'direction', Return candidate grasp pairs
	// NOTE: This function does not evaluate grasp pairs at all, it only reports all of the FILO pairs
	
	bool SHOWDEBUG = false;
	
	RayHits rtnStruct;  rtnStruct.anyHits = false;
	
	// 0. Obtain point cloud
	if( SHOWDEBUG ){  cout << "About to fetch mesh data ..." << endl;  }
	std::vector<TargetVFN_ASP> partTargets = assembly->lab_collision_targets_from_parts();
	Eigen::MatrixXd labV = extract_V_from_TargetVFN( partTargets );
	
	// 1. Construct bases with the casting direction as the z basis
	if( SHOWDEBUG ){  cout << "About to construct bases ..." << endl;  }
	Eigen::Vector3d zBasis = direction.normalized();
	Eigen::Vector3d yBasis = zBasis.cross( xDir ).normalized();
	Eigen::Vector3d xBasis = yBasis.cross( zBasis ).normalized();
	Eigen::Vector3d origin = assembly->get_pose().position - xDir * distance; // Back off from the assembly against 'xDir' by 'distance'
	// 2. Obtain AABB in the shooting basis, transformed into the lab frame
	if( SHOWDEBUG ){  cout << "About to construct AABB ..." << endl;  }
	Eigen::MatrixXd shootAABB = arbitrary_BB_parent_frame( labV , origin , xBasis , yBasis , zBasis );
	// 3. Calculate the projection plane from the corners
	if( SHOWDEBUG ){  cout << "About to calculate tiling parameters ..." << endl;  }
	Eigen::Vector3d minCrnr = shootAABB.row(0);
	Eigen::Vector3d maxCrnr = shootAABB.row(1);
	double xMag = ( maxCrnr - minCrnr ).dot( xBasis );  Eigen::Vector3d xExtent = xBasis * xMag;  
	double yMag = ( maxCrnr - minCrnr ).dot( yBasis );  Eigen::Vector3d yExtent = yBasis * yMag;  
	
	if( SHOWDEBUG ){  cout << "About to tile ..." << endl;  }
	Eigen::MatrixXd rayOrgs;  Eigen::MatrixXd rayDirs;  Eigen::Vector3d point;
	// 4. Tile the plane with points  &&  5. For each of the tiled points
	for( double x_i = 0.0 ; x_i < xMag ; x_i += tileSize ){
		for( double y_j = 0.0 ; y_j < yMag ; y_j += tileSize ){
			// 6. Accumulate rays
			point = minCrnr + xBasis * x_i + yBasis * y_j;
			rayOrgs = copy_V_plus_row( rayOrgs , point  );
			rayDirs = copy_V_plus_row( rayDirs , zBasis );
		}
	}
	if( SHOWDEBUG ){  cout << "About to shoot rays ..." << endl;  }
	// 7. Shoot rays  &&  Return hits
	return perforate_meshes_and_obtain_FILO_pairs( partTargets , rayOrgs , rayDirs );
}

RayHits tile_ASM_grasps_orthogonal( Assembly_ASM* assembly , 
									const Eigen::Vector3d& xBasis , const Eigen::Vector3d& yBasis , const Eigen::Vector3d& zBasis , 
									double distance , double tileSize ){
	// Tile the assembly with opposing grasp pairs aligned with the specified bases
	
	bool SHOWDEBUG = false; // if( SHOWDEBUG ){  cout << "" << endl;  }
	
	if( SHOWDEBUG ){  cout << "About to generate Z grasps ..." << endl;  }
	RayHits zHits = get_opposite_grasp_pairs_for_ASM_and_direction( assembly , 
																	zBasis , xBasis , distance ,
																	tileSize );
	if( SHOWDEBUG ){  cout << "About to generate X grasps ..." << endl;  }
	RayHits xHits = get_opposite_grasp_pairs_for_ASM_and_direction( assembly , 
																	xBasis , yBasis , distance ,
																	tileSize );
	if( SHOWDEBUG ){  cout << "About to generate Y grasps ..." << endl;  }
	RayHits yHits = get_opposite_grasp_pairs_for_ASM_and_direction( assembly , 
																	yBasis , zBasis , distance ,
																	tileSize );
	if( SHOWDEBUG ){  cout << "Concat Z and X ..." << endl 
						   << "Z has " << zHits.enter.rows() << " pairs" << endl
						   << "X has " << xHits.enter.rows() << " pairs" << endl;  }
	RayHits zxHits = concat_hits( zHits , xHits );
	if( SHOWDEBUG ){  cout << "Concat ZX and Y ..." << endl 
						   << "ZX has " << zxHits.enter.rows() << " pairs" << endl
						   << "Y has  " << yHits.enter.rows() << " pairs" << endl;  }
	RayHits zxyHits = concat_hits( zxHits , yHits );
	if( SHOWDEBUG ){  cout << "Return ..." << endl;  }
	return zxyHits;
} 

// COMBINED
std::vector<ParallelGraspState> generate_and_rank_grasp_pairs( Assembly_ASM* assembly , 
															   const Eigen::Vector3d& xBasis , const Eigen::Vector3d& yBasis , const Eigen::Vector3d& zBasis , 
															   double distance , double tileSize ,
															   const Eigen::Vector3d& preferredWristDir , const Eigen::Vector3d& preferredCenter , 
															   double dTheta , double CRIT_ANG ){
	RayHits graspHits = tile_ASM_grasps_orthogonal( assembly , 
											        xBasis , yBasis , zBasis , 
												    distance , tileSize );
	return rank_grasp_pairs( graspHits , 
							 preferredWristDir , preferredCenter , 
							 dTheta , CRIT_ANG );
}

/// ___ END GRASP FUNC _____________________________________________________________________________________________________________________


/// === COLLISION WRAPPERS =================================================================================================================


bool DummyCollision::operator()( const Pose_ASP& pose ){  return false;  }

bool AsmCollision::operator()( const Pose_ASP& pose ){  	
	bool SHOWDEBUG = false , 
		 collidesw = false ;
	movd->set_pose( pose );
	collidesw = movd->collides_with( *refc );
	if( SHOWDEBUG && collidesw ){  cout << "COLLISION" << endl;  }
	return collidesw;
}

bool HandAsmCollision::operator()( const Pose_ASP& pose ){  
	// NOTE: This function assumes that the gripper state has already been set	
	bool SHOWDEBUG = false , 
		 collidesw = false ;
	
	// 1. Set mover to pose
	movd->set_pose( pose );
	// 2. Set Gripper to relative pose
	handModel->set_pose( pose * relHandPose );
	// 3. Check collision mover with ref
	collidesw = movd->collides_with( *refc );
	if( collidesw ){  
		if( SHOWDEBUG ){  cout << "MOVER COLLISION!" << endl;  }
		return true;  
	}
	// 4. Check collision hand with ref
	collidesw = refc->collides_with( handModel->LBR4hand );
	if( collidesw ){  
		if( SHOWDEBUG ){  cout << "HAND COLLISION!" << endl;  }
		return true;  
	}
	// 5. If neither collided, Return false
	return false;
}

bool HandAsmFlrCollision::operator()( const Pose_ASP& pose ){
	bool SHOWDEBUG = false , 
		 collidesw = false ;
	
	// 1. Set mover to pose
	movd->set_pose( pose );
	// 2. Set Gripper to relative pose
	handModel->set_pose( pose * relHandPose );
	// 3. Check collision mover with ref
	collidesw = movd->collides_with( *refc );
	if( collidesw ){  
		if( SHOWDEBUG ){  cout << "MOVER COLLISION!" << endl;  }
		return true;  
	}
	// 4. Check collision hand with ref
	collidesw = refc->collides_with( handModel->LBR4hand );
	if( collidesw ){  
		if( SHOWDEBUG ){  cout << "HAND COLLISION!" << endl;  }
		return true;  
	}
	// 4. Check collision floor with hand
	floor->collides_with( handModel->LBR4hand );
	// 4. Check collision floor with mover
	collidesw = movd->collides_with( *floor );
}

/// ___ END WRAPPERS _______________________________________________________________________________________________________________________


/// === ARM COLLISION GEOMETRY =============================================================================================================

// === struct hand_collision_model ===

void hand_collision_model::init( llin partIDoffset ){
    
    bool SHOWDEBUG = false;
    
    Incrementer assign_part_ID{ partIDoffset };
    
    //               
    double fingerZ = 0.0623 - 0.005; // Don't cheese it too hard!
    double fingerX = 0.0025; // 0.01025;
    
    Pose_ASP baxPalmPoseRel = 
        Pose_ASP{ Eigen::Vector3d( -0.0004  , -0.0029 , 0.132  ) , Eigen::Quaterniond( -0.5      ,  0.5       , 0.5      , -0.5      ) };
	Pose_ASP baxFngRPoseRel = 
	    Pose_ASP{ Eigen::Vector3d(  fingerX ,  0.0    , fingerZ ) , Eigen::Quaterniond(  0.0      ,  0.707107  , 0.0      , -0.707107 ) };
	Pose_ASP baxFnRTPoseRel = 
	    Pose_ASP{ Eigen::Vector3d(  0.00675 ,  0.0    , 0.0183 ) , Eigen::Quaterniond(  0.0      ,  0.707107  , 0.707107 ,  0.0      ) };
	Pose_ASP baxFngLPoseRel = 
	    Pose_ASP{ Eigen::Vector3d( -fingerX ,  0.0    , fingerZ ) , Eigen::Quaterniond(  0.707107 ,  0.0       , 0.707107 ,  0.0      ) };
	Pose_ASP baxFnLTPoseRel =  
	    Pose_ASP{ Eigen::Vector3d( -0.00675 ,  0.0    , 0.0183 ) , Eigen::Quaterniond(  0.0      , -0.707107  , 0.707107 ,  0.0      ) };
	    
    //~ std::vector<string> baxterNames = { "baxterPalm_collsn.stl" , // Palm
										//~ "baxterFing_collsn.stl" , // Right Finger
										//~ "baxterFPad_collsn.stl" ,
										//~ "baxterFing_collsn.stl" , // Left Finger
										//~ "baxterFPad_collsn.stl" };
    std::vector<string> baxterNames = { "baxterPalm_collsn.stl" ,  // Palm
										"baxterFing_collsn_THIN.stl" ,  // Right Finger
										"baxterFing_collsn_THIN.stl" }; // Left Finger
										
    string robotPartPkg = ros::package::getPath( "motion_cost" );
    for( size_t i = 0 ; i < baxterNames.size() ; i++ ){
        string fullPath = ( robotPartPkg ) + "/Resources/Assembly_CAD/BxtrHandCollsn/" + ( baxterNames[i] );
        if( SHOWDEBUG ){  cout << "Adding part " << fullPath << " ...";  }
        LBR4hand.add_part_ptr( new Part_ASM( assign_part_ID() , fullPath ) );
        if( SHOWDEBUG ){  cout << "Success!" << endl;  }
    }
    
    LBR4hand.set_offset( 0 , baxPalmPoseRel ); // Palm
    LBR4hand.set_offset( 1 , baxFngRPoseRel ); // Right Finger
    //~ LBR4hand.set_offset( 2 , baxFnRTPoseRel );
    LBR4hand.set_offset( 2 , baxFngLPoseRel ); // Left Finger
    //~ LBR4hand.set_offset( 4 , baxFnLTPoseRel );
}

void hand_collision_model::set_finger_state( const std::vector<double>& states ){
    // NOTE: The actual poses will not reset untill you set_pose
    R_FngrState  = states[0];
    L_FngrState  = states[1];
    R_FngrOffset = Pose_ASP{ Eigen::Vector3d(  R_FngrState , 0.0 , 0.0 ) , no_turn_quat() };
    L_FngrOffset = Pose_ASP{ Eigen::Vector3d( -L_FngrState , 0.0 , 0.0 ) , no_turn_quat() };
}

Pose_ASP hand_collision_model::get_pose(){  return absPose;  }

void hand_collision_model::set_pose( const Pose_ASP& nuPose ){
	absPose = nuPose;
    LBR4hand.set_pose( nuPose                , 0 ); // Palm
    LBR4hand.set_pose( nuPose * R_FngrOffset , 1 ); // Right Finger
    LBR4hand.set_pose( nuPose * L_FngrOffset , 2 ); // Left Finger
}

// Load part markers into marker manager
void hand_collision_model::load_part_mrkrs_into_mngr( RViz_MarkerManager& mrkrMngr ){  
	LBR4hand.load_part_mrkrs_into_mngr( mrkrMngr );  
}

// ___ End hand_collision_model ___

/// ___ END COLLISION GEO __________________________________________________________________________________________________________________


/// === RRT Removal Path Planning ==========================================================================================================

double dot( const Eigen::Quaterniond op1 , const Eigen::Quaterniond op2 ){
	// Compute the inner product of the quaternion elements
	return op1.w() * op2.w()
		 + op1.x() * op2.x()
		 + op1.y() * op2.y()
		 + op1.z() * op2.z();
}

double distance_between( const Eigen::Vector3d op1 , Eigen::Vector3d op2 , double w_posn ){  return w_posn * ( op1 - op2 ).norm();  }

double distance_between( const Eigen::Quaterniond op1 , Eigen::Quaterniond op2 , double w_ornt ){  return w_ornt * ( 1 - abs( dot( op1 , op2 ) ) );  }

double distance_between( const Pose_ASP& op1 , const Pose_ASP& op2 , double w_posn , double w_ornt ){
	// Compute the weighted distance between poses
	// NOTE: Both Q and -Q (all elements negated) represent the same orientation
	return distance_between( op1.position , op2.position , w_posn ) + distance_between( op1.orientation , op2.orientation , w_ornt );
}

Pose_ASP one_step_from_to( const Pose_ASP& opFrom , const Pose_ASP& opTo , double w_posn , double w_ornt , double stepMag ){
	
	bool SHOWDEBUG = false; // if( SHOWDEBUG ){  cout << "" << endl;  }
	
	if( SHOWDEBUG ){  sep( "one_step_from_to" , 4 );  }
	
	// Return the pose that is as close to 1*epsilon as possible away from 'opFrom' towards 'opTo'
	Pose_ASP rtnPose = origin_pose();
	if( SHOWDEBUG ){  cout << "Init Pose: " << rtnPose << endl;  }
	
	if( SHOWDEBUG ){  cout << "About to calculate distance ..." << endl;  }
	// 1. Get the linear distance
	double linDist = distance_between( opFrom.position    , opTo.position    , w_posn );
	// 2. Get the angular distance
	double angDist = distance_between( opFrom.orientation , opTo.orientation , w_ornt );
	if( SHOWDEBUG ){  cout << "Linear: " << linDist << " , Angular: " << angDist << endl;  }
	
	// 3. Return the ending pose if there is essentially no difference
	double totDist = linDist + angDist;
	
	// DANGER: COPY POSE
	//~ if( eq( totDist , 0.0 ) ){  return copy_pose( opTo );  }
	if( eq( totDist , 0.0 ) ){  return opTo;  }
	
	
	// 4. Compute proportian fractions of step size for the linear and angular components
	double linStep = ( linDist / totDist ) * stepMag , 
		   angStep = ( angDist / totDist ) * stepMag ;
	if( SHOWDEBUG ){  cout << "Linear Step: " << linStep << " , Angular Step: " << angStep << endl;  }
		   
	// 5. Compute the new position
	if( w_posn > 0 ){ // If the user supplied 0 , then no translation will be applied
		double eucDist = linStep / w_posn;
		rtnPose.position = opFrom.position + ( opTo.position - opFrom.position ).normalized() * eucDist;
	}else{  rtnPose.position = opFrom.position;  }
	if( SHOWDEBUG ){  cout << "Old Position: " << opFrom.position << " , New Position: " << rtnPose.position << endl;  }
	if( SHOWDEBUG ){  cout << "'rtnPose': " << rtnPose << endl;  }
	
	// 6. Compute the new orientation
	if( w_ornt > 0 ){ // If the user supplied 0 , then no rotation will be applied
		double angDist = angStep / w_ornt; // Should have recovered a quantity in [0,1]
		angDist = min( angDist , 1.0 );
		if( SHOWDEBUG ){  cout << "About to apply SLERP ..." << endl;  }
		rtnPose.orientation = opFrom.orientation.slerp( angDist , opTo.orientation );
		if( SHOWDEBUG ){  cout << "SLERP Output: " << opFrom.orientation.slerp( angDist , opTo.orientation ) << endl;  }
	}else{  rtnPose.orientation = opFrom.orientation;  }
	
	if( SHOWDEBUG ){  
		cout << "Old Orientation: " << opFrom.orientation << " , New Orientation: " << rtnPose.orientation << endl
			 << "Return: " << rtnPose << endl;
	}
	
	// 7. Return pose
	return rtnPose;
}

std::vector<Pose_ASP> all_compass_samples( const Pose_ASP& nearbyValid , double w_posn , double w_ornt , double stepMag ){
	// Sample on a compass around 'nearbyValid'
	std::vector<Pose_ASP> rtnSamples;
	Eigen::MatrixXd allAxes = Eigen::MatrixXd::Zero( 6 , 3 );
	allAxes <<  1 ,  0 ,  0 ,
			   -1 ,  0 ,  0 ,
			    0 ,  1 ,  0 ,
			    0 , -1 ,  0 ,
			    0 ,  0 ,  1 ,
			    0 ,  0 , -1 ;
	size_t numAxs = allAxes.rows();
	Eigen::Vector3d currAxis;
	Pose_ASP /*- */ currPose;
	//  1. Fetch the permissible linear  distance
	double eucDist = stepMag / w_posn;
	//  2. Populate translation poses
	//  A. For each axis
	for( size_t i = 0 ; i < numAxs ; i++ ){
		//  B. Get the translation and create new pose
		currAxis = allAxes.row(i);
		currPose = Pose_ASP{ 
			nearbyValid.position + currAxis * eucDist ,
			nearbyValid.orientation
		};
		//  C. Push back
		rtnSamples.push_back( currPose );
	}
	//  3. Fetch the permissible angular distance
	double angDist = stepMag / w_ornt;
	//  4. Populate rotation poses
	//  A. For each axis
	for( size_t i = 0 ; i < numAxs ; i++ ){
		//  B. Get the rotation and create new pose
		currAxis = allAxes.row(i);
		currPose = Pose_ASP{ 
			nearbyValid.position  , 
			nearbyValid.orientation.slerp( 
				angDist , 
				Eigen::AngleAxisd( M_PI , currAxis ) * nearbyValid.orientation
			)
		};
		//  C. Push back
		rtnSamples.push_back( currPose );
	}
	//  5. Return
	return rtnSamples;
}

// === class NodePose ===

NodePose::NodePose( const Pose_ASP& nodeState ){
	
	// DANGER: COPY POSE
	//~ state  = copy_pose( nodeState );
	state  = nodeState;
	
	parent = nullptr;
	cost_2_parent = 0.0;
	cost_2_root   = 0.0;
}

void NodePose::set_state( const Pose_ASP& nodeState ){  
	
	// DANGER: COPY POSE
	//~ state = copy_pose( nodeState );  
	state = nodeState;  
	
}

NodePose::~NodePose(){
	// Delete this node and all successors
	std::list<NodePose*>::iterator it;
	for( it = edges.begin() ; it != edges.end() ; ++it ){  delif( *it );  } // This should recursively call the destructor on all successors
	edges.clear();
}

void NodePose::add_child( NodePose* nuChild ){  edges.push_back( nuChild );  }

void NodePose::remove_child_by_ptr( NodePose* nodePtr ){  edges.remove( nodePtr );  } // NOTE: This function does not delete the removed node

void NodePose::reassign_parent( NodePose* nuParent , double w_posn , double w_ornt ){
	
	bool SHOWDEBUG = true;
	
	if( SHOWDEBUG ){
		if( this == nuParent ){  throw std::logic_error( "NodePose::reassign_parent , Attempted to assign a self-loop!" );  }
	}
	
	// 1. Remove this node from the parent's children
	if( parent ){  parent->remove_child_by_ptr( this );  }
	// 2. Add this node as child of new parent
	nuParent->add_child( this );
	// 3. Assign parent
	parent = nuParent;
	// 4. Recalc costs
	cost_2_parent = distance_between( state , nuParent->state , w_posn , w_ornt );
	cost_2_root   = cost_2_parent + nuParent->cost_2_root;
}

std::vector<Pose_ASP> NodePose::path_to_node(){
	// Return the sequence of states from the most proximal node to the leaf
	// NOTE: This function does not check if the parents lead to the root
	// NOTE: This function assumes that 'leaf' is a part of this tree, although this is not in any way enforced
	
	bool SHOWDEBUG = true; // if( SHOWDEBUG ){    }
	
	std::vector<Pose_ASP> rtnVec;
	NodePose* currNode = this;
	size_t counter = 0; 
	while( currNode ){
		rtnVec.push_back( currNode->state );
		if( currNode->isRoot ){  break;  }
		if( currNode == currNode->parent ){
			cout << "Self-loop detected!" << endl;
			cout << "Parent:" << currNode->parent->state << endl;
			cout << "Node:_ " << currNode->state << endl;
			break;
		}
		if( SHOWDEBUG ){  
			counter++;  
			if( counter > 2000 ){  
				cout << "Counter limit reached!" << endl;
				break;  
			}  
		}
		currNode = currNode->parent;
	}
	std::reverse( rtnVec.begin() , rtnVec.end() );
	return rtnVec;
}

std::vector<std::pair<Pose_ASP,Pose_ASP>> NodePose::get_distal_edges(){
	// Return a list of all edges distal to this node
	// NOTE: Edges will be listed in depth-first order
	
	bool SHOWDEBUG = false; // if( SHOWDEBUG ){  cout << "" << endl;  }
	
	std::vector<std::pair<Pose_ASP,Pose_ASP>> rtnVec;
	size_t len = edges.size();
	if( SHOWDEBUG ){  cout << "There are " << len << " edges at this node.  About to iterate..." << endl;  }
	// 0. For each edge , If they exist
	if( len > 0 ){
		//~ for( size_t i = 0 ; i < len ; i++ ){
		for( std::list<NodePose*>::const_iterator it = edges.begin() , end = edges.end() ; it != end ; ++it ){
			if( (*it) != this ){
				// 1. Get edge to immediate successor
				
				// DANGER: COPY POSE
				//~ rtnVec.push_back( std::make_pair( copy_pose( state ) , copy_pose( (*it)->state ) ) );
				rtnVec.push_back( std::make_pair( state , (*it)->state ) );
				
				
				// 2. Get all edges distal to that successor
				extend_vec_with( rtnVec , (*it)->get_distal_edges() );
			}
		}
	}
	if( SHOWDEBUG ){  cout << "Iterations complete!" << endl;  }
	return rtnVec;
}
// ___ End NodePose ___


// === class TreePose ===

TreePose::TreePose( const Pose_ASP& q_root ){
	rootNode = new NodePose( q_root );
	rootNode->isRoot = true;
	allNodes.push_back( rootNode );
}

TreePose::TreePose( const TreePose& other ){
	// Copy contructor
	// NOTE: This function does not copy nodes and will point to the same root node as 'other'
	rootNode = other.rootNode;
	allNodes = other.allNodes; // This is a list copy operation
}

TreePose::~TreePose(){
	delif( rootNode ); // This should recursively call node destructors from root to leaves
	allNodes.clear();
}

size_t TreePose::size(){  return allNodes.size();  }

void TreePose::add_node( NodePose* nuNode , NodePose* parent , double w_posn , double w_ornt ){
	// NOTE: This function assumes that 'nuNode' is not yet a part of the tree
	// NOTE: This function does not add 'parent' to tree bookkeeping
	if( nuNode != parent ){
		nuNode->cost_2_parent = distance_between( nuNode->state , parent->state , w_posn , w_ornt );
		nuNode->cost_2_root   = nuNode->cost_2_parent + parent->cost_2_root;
		nuNode->parent = parent;
		parent->add_child( nuNode );
		allNodes.push_back( nuNode ); 
	}
}

NodePose* TreePose::add_by_state( const Pose_ASP& q , NodePose* parent , double w_posn , double w_ornt ){
	// Create a new TreeNode at state 'q' and assign it 'parent'
	NodePose* nuNode = new NodePose( q );
	add_node( nuNode , parent , w_posn , w_ornt );
	return nuNode;
}

NodePose* TreePose::add_state_seq( const std::vector<Pose_ASP>& qVec , NodePose* parent , double w_posn , double w_ornt ){
	// Add a sequence of states with 'parent' as the root
	size_t len = qVec.size();
	NodePose* nextNode = parent;
	Pose_ASP q;
	for( size_t i = 0 ; i < len ; i++ ){
		q = qVec[i];
		nextNode = add_by_state( q , nextNode , w_posn , w_ornt );
	}
	return nextNode;
}

std::pair<NodePose*,double> TreePose::find_nearest( const Pose_ASP& q_query , double w_posn , double w_ornt ){
	double leastDist = distance_between( rootNode->state , q_query , w_posn , w_ornt );
	NodePose* n_close = rootNode;
	double dist_i = BILLION_D;
	for( std::list<NodePose*>::const_iterator it = allNodes.begin() , end = allNodes.end() ; it != end ; ++it ){
		dist_i = distance_between( (*it)->state , q_query , w_posn , w_ornt );
		if( dist_i < leastDist ){
			n_close = *it;
			leastDist = dist_i;
		}
	}
	return std::make_pair( n_close , leastDist );
}

std::pair<NodePose*,double> TreePose::find_nearest_unmarked( const Pose_ASP& q_query , double w_posn , double w_ornt ){
	double leastDist = distance_between( rootNode->state , q_query , w_posn , w_ornt );
	NodePose* n_close = rootNode;
	double dist_i = BILLION_D;
	for( std::list<NodePose*>::const_iterator it = allNodes.begin() , end = allNodes.end() ; it != end ; ++it ){
		if( !(*it)->compassFlag ){
			dist_i = distance_between( (*it)->state , q_query , w_posn , w_ornt );
			if( dist_i < leastDist ){
				n_close = *it;
				leastDist = dist_i;
			}
		}
	}
	return std::make_pair( n_close , leastDist );
}

std::vector<std::pair<NodePose*,double>> TreePose::get_neighborhood( const Pose_ASP& q_query , double radius , size_t NshortCut , 
																	 double w_posn , double w_ornt ){
	bool SHOWDEBUG = false; // if( SHOWDEBUG ){  cout << "" << endl;  }
	
	if( SHOWDEBUG ){ sep( "get_neighborhood" , 2 );
					 cout << "Searching through " << allNodes.size() << " nodes for up to " << NshortCut << " within radius " << radius << endl;  }
	
	size_t count = 0;
	double dist_i = BILLION_D;
	if( NshortCut == 0 ){  NshortCut = BILLION_D;  }
	std::vector<std::pair<NodePose*,double>> rtnVec;
	for( std::list<NodePose*>::const_iterator it = allNodes.begin() , end = allNodes.end() ; it != end ; ++it ){
		dist_i = distance_between( (*it)->state , q_query , w_posn , w_ornt );
		if( dist_i <= radius ){
			count++;
			rtnVec.push_back( std::make_pair( *it , (*it)->cost_2_root ) );
		}
		if( count >= NshortCut ){  break;  }
	}
	if( SHOWDEBUG ){  cout << "Found " << rtnVec.size() << " nodes" << endl;  }
	return rtnVec;
}

// ___ End TreePose ___

void load_RRT_soln_into_mrkrMngr( RViz_MarkerManager& mngr , 
								  const std::vector<std::pair<Pose_ASP,Pose_ASP>>& allEdges , 
								  const std::vector<Pose_ASP>& solnPath , 
								  double epsilonSize ){
									  
	bool SHOWDEBUG = true; // if( SHOWDEBUG ){  cout << "" << endl;  }
									  
	// 1. Paint all of the edges
	size_t lenEdge  = allEdges.size() , 
		   lenSoln  = solnPath.size() ;
	float  edgeThic = epsilonSize / 50.0 ,
		   edgeAlfa = 0.500 ;
	float  solnThic = edgeThic * 2 ,
		   solnAlfa = 1.000 ;
	std::vector<float> edgeColor = { 0.0 , 0.0 , 1.0 };
	std::vector<float> solnColor = { 0.0 , 1.0 , 0.0 };
	std::pair<Pose_ASP,Pose_ASP> currEdge;
	Pose_ASP pose1;  Pose_ASP pose2;
	
	if( SHOWDEBUG ){  cout << "About to render edges ..." << endl;  }
	
	// A. For each edge
	for( size_t i = 0 ; i < lenEdge ; i++ ){
		// B. Get a straight-path marker
		currEdge = allEdges[i];
		// C. Add it to the manager
		mngr.add_marker( 
			get_straightPath_marker( (std::get<0>(currEdge)).position , (std::get<1>(currEdge)).position , 
									 edgeThic , edgeColor[0] , edgeColor[1] , edgeColor[2] , edgeAlfa )
		);
	}
	
	if( SHOWDEBUG ){  cout << "About to render solution ..." << endl;  }
	
	// 2. Paint the solution path
	if( lenSoln > 0 ){
		// A. Fetch the first point
		pose1 = solnPath[0];
		// B. For each following point
		for( size_t j = 1 ; j < lenSoln ; j++ ){
			// C. Paint a straight-path from the last point to this point
			pose2 = solnPath[j];
			mngr.add_marker( 
				get_straightPath_marker( pose1.position , pose2.position , 
										 solnThic , solnColor[0] , solnColor[1] , solnColor[2] , solnAlfa )
			);
			pose1 = pose2;
		}
	}else if( SHOWDEBUG ){  cout << "There was no solution to render!" << endl;  }
	if( SHOWDEBUG ){  cout << "Rendering complete!" << endl;  }
}

/// ___ End RRT ____________________________________________________________________________________________________________________________

/* === Spare Parts =========================================================================================================================



   ___ End Parts ___________________________________________________________________________________________________________________________

*/
