/*
V_norm_test.cpp
James Watson , 2018 May
Test integration of the ASP_3D lib --> ROS , Show markers and move them around

Dependencies: Cpp_Helpers , ROS , Eigen , RAPID
Template Version: 2017-09-23
*/

// === Init ================================================================================================================================

// ~~~ Includes ~~~
// ~~ ROS ~~
// ~ Core ~
#include <ros/ros.h> // --- ROS presides over all
#include <ros/package.h> // Where are we? // http://wiki.ros.org/Packages#C.2B-.2B-
// ~~ Includes ~~
#include <Eigen/Core> // ---- The living heart of Eigen
#include <Eigen/Dense> // --- Cross Product , etc.
#include <Eigen/Geometry> //- Quaternion , etc
// ~ LIBIGL ~
#include <igl/read_triangle_mesh.h> // ------ URL , Load STL : https://github.com/libigl/libigl/issues/273
#include <igl/remove_duplicate_vertices.h> // More compact representation of the model
#include <igl/per_face_normals.h> // -------- Needed for agglomerating facets into faces
#include <igl/per_vertex_normals.h> // ------ Needed for shrinking meshes
// ~ RViz ~
#include <visualization_msgs/Marker.h> // ---- Display geometry
#include <visualization_msgs/MarkerArray.h> // RViz marker array
// ~~ Local ~~ 
#include <Cpp_Helpers.h> // Shortcuts and Aliases , and Functions of general use for C++ programming
#include <ASP_3D.h> // ---- Assembly geometry
#include <AsmSeqPlan.h> //- Graph planning


// using namespace quickhull;

// ___ End Init ____________________________________________________________________________________________________________________________

// === Program Vars ===

string projectOuputDir = "/home/jwatson/output/";

// ___ End Vars ___


// === Program Functions ===



// ___ End Funcs ___

// === main ================================================================================================================================

int main( int argc , char** argv ){
	
	srand( time( 0 ) ); // Random seed based on the current clock time
	
	
	// ~~~ Calc Normals ~~~
	
	sep( "Vertex Normals" );
	
	string pkgPath = ros::package::getPath( "motion_cost" );
	string resourcePath;
	string STLpath;
	
	if( false ){
		resourcePath = pkgPath + "/Resources/Assembly_CAD/SimpleCube/";
		STLpath = resourcePath + "MeterCube.stl"; // Path to the STL source used to generate
	}else{
		resourcePath = pkgPath + "/Resources/Assembly_CAD/SimplePhone/";
		STLpath = resourcePath + "moduleB_SCL.stl";
	}

	// Setup for loading
	Eigen::MatrixXd OV , V , N_faces; 
	Eigen::MatrixXi OF , F , SVI , SVJ;
	// Load and Reduce mesh
	igl::read_triangle_mesh( STLpath , OV , OF );
	igl::remove_duplicate_vertices( OV , OF , 0.0 , V , SVI , SVJ , F );
	
	Eigen::MatrixXd vertcs3D  = OV; // Dense points of the mesh
	Eigen::MatrixXi facets    = OF; // Facets corresponding to the dense points
	
	Eigen::MatrixXd reducdVtx = V; //- Reduced points of the mesh
	Eigen::MatrixXi reducdFct = F; //- Facets corresponding to the reduced points
	Eigen::MatrixXd reducdNrm = N_from_VF( reducdVtx , reducdFct );
	
	cout << STLpath << endl
		 << "Loaded an STL mesh with " << V.rows() << " vertices, " << F.rows() << " facets, and " << reducdNrm.rows() << " normals." << endl;
		 
	Eigen::MatrixXd N_vertices;
	igl::per_vertex_normals( V , F , 
								 //~ igl::PER_VERTEX_NORMALS_WEIGHTING_TYPE_AREA , // Pointing at uneven angles
								 //~ igl::PER_VERTEX_NORMALS_WEIGHTING_TYPE_UNIFORM , // Even more wonky
								 igl::PER_VERTEX_NORMALS_WEIGHTING_TYPE_ANGLE ,
								 N_vertices );
	
	cout << "There are " << N_vertices.rows() << " vertex normals" << endl;
	cout << N_vertices << endl;
	
	Eigen::Vector3d n_i;
	
	cout << "Normal lengths ... " << endl;
	for( size_t i = 0 ; i < N_vertices.rows() ; i++ ){
		n_i = N_vertices.row(i);
		cout << n_i.norm() << " ";
	}  
	cout << endl;
	
	
	// ~~~ Write File ~~~
	
	std::fstream facetFile;
	size_t fCount = 0;
	
	// i. Open file for writing
	facetFile.open( "/home/jwatson/output/adjacent-facets_" + prepad( to_string( fCount ) , 5 , '0' ) + "_" + timestamp() + ".txt" , 
					std::fstream::out | std::fstream::trunc );
	if ( facetFile.is_open() ){ 
		
		// A. Write vertices
		facetFile << "BGN:V" << endl;
		facetFile << vertices_to_string( V );
		facetFile << "END" << endl;
		
		// B. Write facets
		facetFile << "BGN:F" << endl;
		facetFile << facets_to_string( F );
		facetFile << "END" << endl;
		
		// C. Write vertex normals
		facetFile << "BGN:NV" << endl;
		facetFile << vertices_to_string( N_vertices );
		facetFile << "END" << endl;
		
		// N. Close file
		facetFile.close();
	}else{  cout << "FAILED TO OPEN DEBUG FILE!" << endl;  }
	
	return 0; // I guess everything turned out alright at the end!
}

// ___ End main ____________________________________________________________________________________________________________________________


/* === Spare Parts =========================================================================================================================


	
   ___ End Spare ___________________________________________________________________________________________________________________________
*/

