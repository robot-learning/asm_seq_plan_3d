/*
mesh_test.cpp
James Watson , 2017 October
Test integration of the ASP_3D lib --> ROS , Show markers and move them around

Dependencies:
Template Version: 2017-09-23
*/

// === Init ================================================================================================================================

// ~~~ Includes ~~~
// ~~ Standard ( Common includes not already in Cpp_Helpers.h ) ~~ 
//#include <iomanip> // - Output precision and printing
//#include <exception> // error handling
//#include <stdexcept> // library provided error types like 'std::range_error'
//#include <tuple> // --- standard tuple datatype
//#include <cstddef> // - alias types names:  size_t, ptrdiff_t
//#include <cstring> // - C Strings: strcomp 
//#include <algorithm> // sort, search, min, max, sequence operations

// ~~ ROS ~~
// ~ Core ~
#include <ros/ros.h> // ---------------------------- ROS presides over all
#include <ros/package.h> // Where are we? // http://wiki.ros.org/Packages#C.2B-.2B-
// ~ RViz ~
#include <visualization_msgs/Marker.h> // ---------- Display geometry
#include <visualization_msgs/MarkerArray.h> // ----- RViz marker array

// ~~ Local ~~ 
#include <Cpp_Helpers.h> // Shortcuts and Aliases , and Functions of general use for C++ programming
#include <ASP_3D.h>

// ___ End Init ____________________________________________________________________________________________________________________________


// === Program Functions & Classes ===



// ___ End Functions & Classes ___


// === Program Vars ===

// ~~ Display ~~
int updateHz = 30;
// ~~ Bookkeeping ~~
size_t i        = 0 , 
	   numFaces = 0 ;
Incrementer assign_part_ID{ 0 }; // Instantiate a functor that will assign a unique number to each part

// ___ End Vars ___


// === main ================================================================================================================================

int main( int argc , char** argv ){
	
	srand( time( 0 ) ); // Random seed based on the current clock time
	
	
	
	// == Problem Setup ==
	
	// 0. Create file paths
	string pkgPath = ros::package::getPath( "motion_cost" );
	string EL_fileName  = pkgPath + "/Resources/EL_SCLD.stl";
	string CEE_fileName = pkgPath + "/Resources/CEE_SCLD.stl";
	cout << "Path to EL : " << EL_fileName  << endl;
	cout << "Path to CEE: " << CEE_fileName << endl;
	
	// 1. Create part object(s)
	Part_ASM EL_part{  assign_part_ID() , EL_fileName };
	Part_ASM CEE_part{ assign_part_ID() , CEE_fileName };
	
	// 2. set the poses for the part(s)
	Eigen::Vector3d posEL;		
	if( true ){
		posEL   <<  0.2d   ,  0.2d   ,  0.2d   ;
	} else {
		posEL   <<  0.6d   ,  0.6d   ,  0.6d   ;
	}
	Eigen::Quaterniond quatEL{              0.0d   ,  0.0d   ,  0.707d , -0.707d };
	EL_part.set_pose( posEL , quatEL );
	Eigen::Vector3d posCEE;		posCEE  <<  0.0d   ,  0.0d   ,  0.0d   ;
	Eigen::Quaterniond quatCEE{             1.0d   ,  0.0d   ,  0.0d   ,  0.0d   };
	CEE_part.set_pose( posCEE , quatCEE );
	sep( "EL" );
	EL_part.status_report();
	
	sep( "CEE" );
	CEE_part.status_report();
	
	// 3. Calc positions of the face vertices
	EL_part.calc_all_faces();
	CEE_part.calc_all_faces();
	
	// 4. For each of the faces , create a marker and load them into the marker manager , cycling through colors
	RViz_MarkerManager mrkrMngr{};
	EL_part.load_face_mrkrs_into_mngr(  mrkrMngr );
	CEE_part.load_face_mrkrs_into_mngr( mrkrMngr );
	
	
	std::vector< Part_ASM* > partPtrList;
	partPtrList.push_back( &EL_part );
	partPtrList.push_back( &CEE_part );
	
	std::vector< part_face_pair > pairs = 
		adjacent_sides_from_parts( partPtrList , 0.0698d , 0.001d );
		
	sep( "Adjacent Sides" , 2 );
	print_part_face_pair_vec( pairs );
	
	if( false ){
		sep( "TROUBLESHOOTING" );
		// ~ Part ID ~
		cout << "EL ID: _____ " << EL_part.get_ID()  << endl;
		cout << "CEE ID: ____ " << CEE_part.get_ID() << endl; // IDs assigned OK!
		// ~ Number of Faces ~ 
		cout << "EL # Faces:  " << EL_part.get_number_of_faces()  << endl;
		cout << "CEE # Faces: " << CEE_part.get_number_of_faces() << endl; // Number of faces OK!
		// ~ Face ID ~
		std::vector< Polygon_ASM > EL_faces  = EL_part.get_all_faces();
		std::vector< Polygon_ASM > CEE_faces = CEE_part.get_all_faces();
		
		cout << "EL Faces: __ ";
		numFaces = EL_faces.size();
		for( i = 0 ; i < numFaces ; i++ ){
			cout << "( " << EL_faces[i].get_ID().part << " , " << EL_faces[i].get_ID().face << " ) , ";
		} cout << endl;
		
		cout << "CEE Faces: _ ";
		numFaces = CEE_faces.size();
		for( i = 0 ; i < numFaces ; i++ ){
			cout << "( " << CEE_faces[i].get_ID().part << " , " << CEE_faces[i].get_ID().face << " ) , ";
		} cout << endl;
		
	}
	
	cout << endl;
	
	sep( "Local Freedom Test" );
	
	LocalFreedom_ASM freedom{};
	
	cout << " Adding constraint ( 0 , 0 , 1 ) " << endl;
	freedom.add_constraint( Eigen::Vector3d( 0 , 0 , 1 ) );
	
	cout << " Test Direction (  1 ,  1 ,  1 ) : " << freedom.test_direction( Eigen::Vector3d(  1 ,  1 ,  1 ) ) << endl; // T , OK
	cout << " Test Direction (  1 ,  1 , -1 ) : " << freedom.test_direction( Eigen::Vector3d(  1 ,  1 , -1 ) ) << endl; // F , OK
	
	cout << " Adding constraint ( 1 , 0 , 0 ) " << endl;
	freedom.add_constraint( Eigen::Vector3d( 1 , 0 , 0 ) );
	
	cout << " Test Direction (  1 ,  0 ,  1 ) : " << freedom.test_direction( Eigen::Vector3d(  1 ,  0 ,  1 ) ) << endl; // T , OK
	cout << " Test Direction ( -1 ,  0 ,  1 ) : " << freedom.test_direction( Eigen::Vector3d( -1 ,  0 ,  1 ) ) << endl; // F , OK
	cout << " Test Direction ( -1 ,  0 , -1 ) : " << freedom.test_direction( Eigen::Vector3d( -1 ,  0 , -1 ) ) << endl; // F , OK
	cout << " Test Direction (  1 ,  0 , -1 ) : " << freedom.test_direction( Eigen::Vector3d(  1 ,  0 , -1 ) ) << endl; // F , OK
	
	cout << " Adding constraint ( 0 , 1 , 0 ) " << endl;
	freedom.add_constraint( Eigen::Vector3d( 0 , 1 , 0 ) );
	cout << " Test Direction (  1 ,  1 ,  1 ) : " << freedom.test_direction( Eigen::Vector3d(  1 ,  1 ,  1 ) ) << endl; // T , OK
	cout << " Test Direction ( -1 ,  1 ,  1 ) : " << freedom.test_direction( Eigen::Vector3d( -1 ,  1 ,  1 ) ) << endl; // F , OK
	cout << " Test Direction (  1 , -1 ,  1 ) : " << freedom.test_direction( Eigen::Vector3d(  1 , -1 ,  1 ) ) << endl; // F , OK
	cout << " Test Direction (  1 ,  1 , -1 ) : " << freedom.test_direction( Eigen::Vector3d(  1 ,  1 , -1 ) ) << endl; // F , OK
	
	sep( "Test Composite Freedom" , 3 );
	std::vector< LocalFreedom_ASM* > freedomList;
	LocalFreedom_ASM freedom1{};  freedom1.add_constraint( Eigen::Vector3d( 0 , 0 , 1 ) );
	LocalFreedom_ASM freedom2{};  freedom2.add_constraint( Eigen::Vector3d( 0 , 1 , 0 ) );
	LocalFreedom_ASM freedom3{};  freedom3.add_constraint( Eigen::Vector3d( 1 , 0 , 0 ) );
	freedomList.push_back( &freedom1 );  freedomList.push_back( &freedom2 );  freedomList.push_back( &freedom3 );
	
	cout << " Test Direction (  1 ,  1 ,  1 ) : " << test_composite_freedom( freedomList , 
																			 Eigen::Vector3d(  1 ,  1 ,  1 ) ) << endl; // T , OK
	cout << " Test Direction ( -1 ,  1 ,  1 ) : " << test_composite_freedom( freedomList , 
																			 Eigen::Vector3d( -1 ,  1 ,  1 ) ) << endl; // F , OK
	cout << " Test Direction (  1 , -1 ,  1 ) : " << test_composite_freedom( freedomList , 
																			 Eigen::Vector3d(  1 , -1 ,  1 ) ) << endl; // F , OK
	cout << " Test Direction (  1 ,  1 , -1 ) : " << test_composite_freedom( freedomList , 
																			 Eigen::Vector3d(  1 ,  1 , -1 ) ) << endl; // F , OK
	
	
	sep( "Non-Directional Blocking Graph Test" );
	
	// std::vector< Part_ASM* > partPtrList;  partPtrList.push_back( &EL_part );  partPtrList.push_back( &CEE_part );
	
	cout << "Creating NDBG ... ";
	std::vector< std::vector< LocalFreedom_ASM > > NDBG = NDBG_from_posed_parts( partPtrList , 
																				 2 ,
																				 0.0698d , 
																				 0.001d );
	cout << "Done!" << endl;

	std::vector<size_t> queryParts;      queryParts.push_back(     0 );
	std::vector<size_t> referenceParts;  referenceParts.push_back( 1 );
									 
	
	
	cout << "Interferences for Part 0" << endl;
	print_constraint_state_for_part( NDBG , 0 );
	cout << "Interferences for Part 1" << endl;
	print_constraint_state_for_part( NDBG , 1 );
	
	interference_result result = query_NDBG( NDBG , queryParts , referenceParts , Eigen::Vector3d( 1.0 , 0.0 , 0.0 ) );
	
	cout << "Attempting the remove the query part via ( 1.0 , 0.0 , 0.0 ) , result: " << result.result << " , "
		 << result.interferingPartIDs << endl;
		 
	result = query_NDBG( NDBG , queryParts , referenceParts , Eigen::Vector3d( 0.0 , 1.0 , 0.0 ) );
	
	cout << "Attempting the remove the query part via ( 0.0 , 1.0 , 0.0 ) , result: " << result.result << " , "
		 << result.interferingPartIDs << endl;
		 
	result = query_NDBG( NDBG , queryParts , referenceParts , Eigen::Vector3d( 0.0 , 0.0 , 1.0 ) );
	
	cout << "Attempting the remove the query part via ( 0.0 , 0.0 , 1.0 ) , result: " << result.result << " , "
		 << result.interferingPartIDs << endl;
		 
	result = query_NDBG( NDBG , queryParts , referenceParts , Eigen::Vector3d( -1.0 , 0.0 , 0.0 ) );
	
	cout << "Attempting the remove the query part via ( -1.0 , 0.0 , 0.0 ) , result: " << result.result << " , "
		 << result.interferingPartIDs << endl;
		 
	result = query_NDBG( NDBG , queryParts , referenceParts , Eigen::Vector3d( 0.0 , -1.0 , 0.0 ) );
	
	cout << "Attempting the remove the query part via ( 0.0 , -1.0 , 0.0 ) , result: " << result.result << " , "
		 << result.interferingPartIDs << endl;
		 
	result = query_NDBG( NDBG , queryParts , referenceParts , Eigen::Vector3d( 0.0 , 0.0 , -1.0 ) );
	
	cout << "Attempting the remove the query part via ( 0.0 , 0.0 , -1.0 ) , result: " << result.result << " , "
		 << result.interferingPartIDs << endl;
		 
	result = query_NDBG( NDBG , queryParts , referenceParts , Eigen::Vector3d( 1.0 , 0.0 , 1.0 ) );
	
	cout << "Attempting the remove the query part via ( 1.0 , 0.0 , 1.0 ) , result: " << result.result << " , "
		 << result.interferingPartIDs << endl;
		 
		 
	sep( "Centroid Tests" );
	
	VolumeCentroid EL_vol = get_VolumeCentroid_from_V_F( EL_part.get_dense_vertices() , EL_part.get_dense_facets() );
	cout << "EL Part  , Volume: "; printf( "%10.8f" , EL_vol.volume ); cout << " , Centroid: "; print_vec3d_inline( EL_vol.centroid ); cout << endl;
	
	VolumeCentroid CEE_vol = get_VolumeCentroid_from_V_F( CEE_part.get_dense_vertices() , CEE_part.get_dense_facets() );
	cout << "CEE Part , Volume: "; printf( "%10.8f" , CEE_vol.volume ); cout  <<" , Centroid: "; print_vec3d_inline( CEE_vol.centroid ); cout << endl;
	
	// __ End Setup __
	
	/// === VISUALIZATION ==================================================================================================================
	
	// == ROS Loop ==
	
	// = ROS Start =
	
	// 5. Start ROS
	srand( ( time( 0 ) % 1 ) * 1000.0 + getpid() ); // Random seed based on the current clock time
	
	ros::init( argc , argv , "adjacency_test_cpp" );
	
	
	// ros::Publisher vis_pub = node_handle.advertise<visualization_msgs::Marker>(       "visualization_marker"       , 0 );
	ros::NodeHandle node_handle;
	ros::Publisher vis_arr = node_handle.advertise<visualization_msgs::MarkerArray> ( "visualization_marker_array" , 100 );
	
	ros::Rate r( updateHz ); // 30Hz refresh rate
	
	// ~ Icosahedron Test ~
	Icosahedron_d icosEx{ 1 , Eigen::Vector3d(  2 ,  2 , 2 ) };
	RViz_MeshMarker icosMrkr{ icosEx.get_vertices() , icosEx.get_facets() };
	icosMrkr.set_color( 1 , 0 , 0 , 1 );
	mrkrMngr.add_marker( icosMrkr );
	
	// ~ Sphere Test ~ 
	Sphere_d sphrEx{ 1 , Eigen::Vector3d(  -2 ,  2 , 2 ) , 20 };
	RViz_MeshMarker sphrMrkr{ sphrEx.get_vertices() , sphrEx.get_facets() };
	sphrMrkr.set_color( 1 , 0 , 0 , 1 );
	mrkrMngr.add_marker( sphrMrkr );
	
	// ~ Coordinate Axes ~
	mrkrMngr.add_marker( get_axes_marker( 5 , 0.01f ) );
	
	if( false ){
		sep( "Sphere Troubleshooting" );
		cout << "V" << endl << sphrEx.get_vertices() << endl;
		cout << "F" << endl << sphrEx.get_facets() << endl;
	}
	
	// _ End Start _
	
	visualization_msgs::MarkerArray& markerArr = mrkrMngr.get_arr();
	
	float t    = 0.0f  ,
	      incr = 0.07f ;
	
	// 6. Set the animation loop and display
	while ( ros::ok() ){ // while roscore is running
        // cout << ".";

		// Wait for a subscriber to start looking for this topic
		while ( vis_arr.getNumSubscribers() < 1 ){
			if ( !ros::ok() ){ return 0; }
			printf( "." );
			ROS_WARN_ONCE( "Please create a subscriber to the marker" );
			sleep( 1 );
			cout << ".";
		} 
		
		//~ CEE_mrkr.set_pose( 0.0 , 0.0 , 0.0      , 1.0 , 0.0 , 0.0 , 0.0 );
        //~ EL_part.set_pose(  0.0 , 0.0 , cos( t ) , 1.0 , 0.0 , 0.0 , 0.0 );
        
        markerArr = mrkrMngr.rebuild_mrkr_array();
        
        vis_arr.publish( markerArr );
        
        t += incr;

		r.sleep();
	}
	
	// __ End ROS __
	
	
	return 0; // I guess everything turned out alright at the end!
}

/// ___ END VISUAL _________________________________________________________________________________________________________________________

// ___ End main ____________________________________________________________________________________________________________________________


/* === Spare Parts =========================================================================================================================



   ___ End Spare ___________________________________________________________________________________________________________________________
*/
