/*
level_exec.cpp
James Watson , 2018 June

% ROS Node %
Plan and execute one precedence level of a disassembly plan
Publises To ----> :
Subscribes To <-- :

Dependencies: ROS , Cpp_Helpers , ROS_Helpers
Template Version: 2018-06-06
*/

// === Init ================================================================================================================================

// ~~~ Includes ~~~
// ~~ ROS ~~
#include <ros/ros.h> // -------------- ROS , Publishers , Subscribers
#include <ros/package.h> // ------------------ Where are we? // http://wiki.ros.org/Packages#C.2B-.2B-
// ~ ROS Messages ~
#include "motion_cost/qFromPose.h"
#include "motion_cost/IKrequest.h"
#include "motion_cost/IKresponse.h"
#include "geometry_msgs/Pose.h"
#include "sensor_msgs/JointState.h"
#include <ll4ma_teleop/Joints2Jac.h>
#include <ll4ma_teleop/LinkPoses.h>
#include <ll4ma_teleop/FngrPoses.h>
// ~~ Local ~~
#include <Cpp_Helpers.h> // - C++ Utilities and Shortcuts
#include <ROS_Helpers.h> // - ROS Utilities and Shortcuts
#include <Model_Factory.h> // Assemblies to plan on
#include <AsmSeqPlan.h> // -- Identify grasp pairs , ASP
#include <comm_ASM.h> // ---- Messages and visualization
#include <Motion_Planning.h>

// ___ End Init ____________________________________________________________________________________________________________________________


/// ######### Node Vars #########

string /* -- */ NODE_NAME  = "Collision_Test";
int /* ----- */ RATE_HZ    = 30;
int /* ----- */ QUEUE_LEN  = 30;
Eigen::Vector3d ROBOT_BASE = Eigen::Vector3d( 0.55 , 0 , 1.20 ); // FUTURE: NEED A WAY TO LOAD THIS FROM A PARAM SERVER

/// ********* End Vars *********


// === Program Functions & Classes ===




// ___ End Functions & Classes ___


// === Program Vars ===

bool simViz    = true  , 
	 staticViz = false ;

// ___ End Vars ___


// === main ================================================================================================================================

int main( int argc , char** argv ){ // Main takes the terminal command and flags that called it as arguments
	srand( time( 0 ) ); // Random seed based on the current clock time
	
	// 0. Init ROS  &&  Register node
	ros::init( argc , argv , NODE_NAME );
	
	// 1. Fetch handle to this node
	ros::NodeHandle nodeHandle;
	
	// 2. Init node rate
	ros::Rate heartbeat( RATE_HZ );
	
	// 3. Set up subscribers and publishers
	
	// ~ Publishers ~
	ros::Publisher joint_cmd_pub = nodeHandle.advertise<sensor_msgs::JointState>( "/lbr4_teleop/joint_cmd" , QUEUE_LEN );
	send_joint_command jntCmd_Call{ joint_cmd_pub };
	
	ros::Publisher vis_arr       = nodeHandle.advertise<visualization_msgs::MarkerArray>( "visualization_marker_array" , QUEUE_LEN );
	
	// ~ Subscribers ~
	// ros::Subscriber SUBSCRIBER_OBJ = nodeHandle.subscribe( "TOPIC_NAME" , QUEUE_LEN , CALLBACK_FUNCTION );
	
	// ~ Service Servers ~
	// ros::ServiceServer SERVER_OBJ = nodeHandle.advertiseService( "SERVICE_NAME" , SERV_CALLBACK );
	
	// ~ Service Clients ~
	
	ros::ServiceClient  IK_Client = nodeHandle.serviceClient<motion_cost::qFromPose>( "q_from_pose" );
	request_IK_solution IK_clientCall{ IK_Client };
	
	ros::ServiceClient  J_Client = nodeHandle.serviceClient<ll4ma_teleop::Joints2Jac>( "/robot_commander/get_Jacobian" );
	request_Jacobian    J_ClientCall{ J_Client };
	
	ros::ServiceClient  FK_Client = nodeHandle.serviceClient<ll4ma_teleop::LinkPoses>( "/robot_commander/get_links_FK" );
	request_FK_poses    FK_ClientCall{ FK_Client };

	ros::ServiceClient  FNG_Client = nodeHandle.serviceClient<ll4ma_teleop::FngrPoses>( "/robot_commander/get_fingr_FK" );
	request_FNG_poses   FNG_ClientCall{ FNG_Client };

	
	
	// N-1. Animation Init 
	srand( ( time( 0 ) % 1 ) * 1000.0 + getpid() ); // Random seed based on the current clock time
	RViz_MarkerManager mrkrMngr{};
	visualization_msgs::MarkerArray& markerArr = mrkrMngr.get_arr();
	
	
	/// === Preliminary { Setup , Instantiation , Planning  } ==============================================================================

	// I . Request Poses
	sep( "Get Link Poses" );
	
	std::vector<double> jnts1 = { 0 , 0 , 0 , 0 , 0 , 0 , 0 };
	std::vector<Pose_ASP> poses1 = FK_ClientCall( jnts1 );
	for( size_t i = 0 ; i < poses1.size() ; i++ ){
		cout << "Pose " << i+1 << ":\t" << poses1[i] << endl;
	}
	
	std::vector<double> jnts2 = { M_PI/2.0 , M_PI/2.0 , M_PI/2.0 , M_PI/2.0 , M_PI/2.0 , M_PI/2.0 , M_PI/2.0 };
	std::vector<Pose_ASP> poses2 = FK_ClientCall( jnts2 );
	for( size_t i = 0 ; i < poses2.size() ; i++ ){
		cout << "Pose " << i+1 << ":\t" << poses2[i] << endl;
	}

	
	// II. Load Arm Models
	sep( "Arm Representation" );
	
	bool LOADHAND = true  , 
		 TRYONEBX = false ;
	
	string robotURDFpkg = ros::package::getPath( "ll4ma_robots_description" );
	Incrementer assign_part_ID{ 1000 };
	
	
	
	LBR4_collision_model<request_FK_poses,request_FNG_poses> LBR4collsn{ FK_ClientCall , FNG_ClientCall };
	LBR4collsn.init();
	
	LBR4collsn.LBR4arm.load_part_mrkrs_into_mngr( mrkrMngr );
	
	std::vector<double> jntPos = { 0.0 , 0.0 , 0.0 , 0.0 , 0.0 , 0.0 , 0.0 };
	std::vector<double> fngPos = { 0.03 , 0.03 };
	double /* ------ */ dTheta = 0.1047 / 4.0 /* ------ */ , // frame angle increment , 6deg
	/* ------------- */ theta  = 0.0000 /* ------ */ , // Angle for sinusoid
	/* ------------- */ maxOpn = MAX_BAXTER_GRIP_SEP , // Max spread of fingers
	/* ------------- */ spread = 0.0000 /* ------ */ ; // Spread of fingers
	
	
	std::vector<Pose_ASP> linkPoses;
	std::vector<Pose_ASP> fngrPoses;
	
	// III. Instantiate a model for us to collide with
	
	Assembly_ASM* simplePhone = simple_phone();
	Eigen::Vector3d floorCenter{ 0.9 , 0.0 , 0.59 }; // Table center
	simplePhone->set_lab_pose_for_index_3D( 0 , floorCenter , true );
	simplePhone->load_part_mrkrs_into_mngr( mrkrMngr );
	
	/// ___ End Preliminary ________________________________________________________________________________________________________________
	
	// N-2. Notify
	ros_log( "[" + NODE_NAME + "] Init OK and about to run ..." , INFO );
	
	// N. Main loop
	while( ros::ok() ){ // While neither node nor ROS has been shut down
		
		/// == NODE WORK ===================================================================================================================
		
		// 1. Set joint angles
		jntPos[0] = M_PI / 2.0;
		if( 0 ){
			//~ jntPos += dTheta; // Advance arm joints
			jntPos[1] += dTheta; // Advance arm joints
		}
		theta += dTheta; // Advance sinusoid
		spread = maxOpn/2.0 * sin( theta ) + maxOpn;
		if( 0 ){
			fngPos[0] = spread/2.0;  fngPos[1] = spread/2.0;  
		}
		
		//~ cout << "Setting joints to:_ " << jntPos << endl;
		//~ cout << "Setting fingers to: " << fngPos << endl;
		
		LBR4collsn.set_poses_from_joints9( jntPos , fngPos );
			
        // 4. Update markers
        mrkrMngr.rebuild_mrkr_array();
        
        // 5. Paint frame
        vis_arr.publish( markerArr );
		
		
		// 6. Send joint command
		jntCmd_Call( jntPos , fngPos , 1 );
		
		if( simplePhone->collides_with( LBR4collsn.LBR4arm ) ){
			ros_log( "COLLISION!" , ERROR );
		}
		
		//~ break; // ONLY ONCE
		
		/// __ END WORK ____________________________________________________________________________________________________________________
		
		ros::spinOnce(); // - Process messages
		heartbeat.sleep(); // Sleep for remainder of period
	}
	
	// N+1. Notify  &&  Exit
	
	ros_log( "[" + NODE_NAME + "] Exit OK, Goodbye!" , INFO );
	
	return 0; // I guess everything turned out alright at the end!
}

// ___ End main ____________________________________________________________________________________________________________________________


/* === Spare Parts =========================================================================================================================



   ___ End Spare ___________________________________________________________________________________________________________________________
*/
