/*
asm_test.cpp
James Watson , 2018 January
Test integration of the ASP_3D lib --> ROS , Show markers and move them around

Dependencies: Cpp_Helpers , ROS , Eigen
Template Version: 2017-09-23
*/

// === Init ================================================================================================================================

// ~~~ Includes ~~~
// ~~ Standard ( Common includes not already in Cpp_Helpers.h ) ~~ 
//#include <iomanip> // - Output precision and printing
//#include <exception> // error handling
//#include <stdexcept> // library provided error types like 'std::range_error'
//#include <tuple> // --- standard tuple datatype
//#include <cstddef> // - alias types names:  size_t, ptrdiff_t
//#include <cstring> // - C Strings: strcomp 
//#include <algorithm> // sort, search, min, max, sequence operations

// ~~ ROS ~~
// ~ Core ~
#include <ros/ros.h> // --- ROS presides over all
#include <ros/package.h> // Where are we? // http://wiki.ros.org/Packages#C.2B-.2B-
// ~~ Includes ~~
#include <Eigen/Core> // ---- The living heart of Eigen
#include <Eigen/Dense> // --- Cross Product , etc.
#include <Eigen/Geometry> //- Quaternion , etc
// ~ RViz ~
#include <visualization_msgs/Marker.h> // ---- Display geometry
#include <visualization_msgs/MarkerArray.h> // RViz marker array
// ~~ Local ~~ 
// #include <Cpp_Helpers.h> // Shortcuts and Aliases , and Functions of general use for C++ programming
#include <ASP_3D.h> // ---- Assembly planning
#include <QuickHull.hpp> // Convex hull

// using namespace quickhull;

// ___ End Init ____________________________________________________________________________________________________________________________


// === Program Functions & Classes ===



// ___ End Functions & Classes ___


// === Program Vars ===

quickhull::QuickHull<double> qh; // hull object to be returned
std::vector<quickhull::Vector3<double>> pointCloud; // Collection of points to form a hull around

// ___ End Vars ___


// === main ================================================================================================================================

int main( int argc , char** argv ){
	
	srand( time( 0 ) ); // Random seed based on the current clock time
	
	
	
	// === Parts Init ===
	
	sep( "Instantiating Parts ..." );
	
	Incrementer assign_part_ID{ 0 }; // Instantiate a functor that will assign a unique number to each part
	
	// 0. Create file paths
	string pkgPath = ros::package::getPath( "motion_cost" );
	string EL_fileName  = pkgPath + "/Resources/EL_SCLD.stl";
	string CEE_fileName = pkgPath + "/Resources/CEE_SCLD.stl";
	string CUBE_fileName = pkgPath + "/Resources/CUBE.stl";
	cout << "Path to EL : " << EL_fileName  << endl;
	cout << "Path to CEE: " << CEE_fileName << endl;
	
	// 1. Create part object(s)
	Part_ASM EL_part{   assign_part_ID() , EL_fileName   };
	Part_ASM CEE_part{  assign_part_ID() , CEE_fileName  };
	Part_ASM CUBE_part{ assign_part_ID() , CUBE_fileName };
	
	// 2. set the poses for the part(s)
	Eigen::Vector3d posEL;		
	Eigen::Vector3d posCUBE;  posCUBE <<  0.0   ,  0.0   ,  0.0;
	if( true ){  posEL   <<  0.2   ,  0.2   ,  0.2   ;  } 
	else{        posEL   <<  0.6   ,  0.6   ,  0.6   ;  }
	Eigen::Quaterniond quatEL{              0.0   ,  0.0   ,  0.707 , -0.707 };
	EL_part.set_pose( posEL , quatEL );
	Eigen::Vector3d posCEE;		posCEE  <<  0.0   ,  0.0   ,  0.0   ;
	Eigen::Quaterniond quatCEE{             1.0   ,  0.0   ,  0.0   ,  0.0   };
	Eigen::Quaterniond quatCUBE{             1.0   ,  0.0   ,  0.0   ,  0.0   };
	CEE_part.set_pose( posCEE , quatCEE );
	CUBE_part.set_pose( posCUBE , quatCUBE );
	
	// 3. Calc supports
	
	// ~~ EL ~~
	sep( "CUBE Supports" , 2 , '*' );
	CUBE_part.determine_support_stability();
	//~ CUBE_part.report_support_stability();
	cout << endl;
	
	sep( "CEE Supports" , 2 , '*' );
	CEE_part.determine_support_stability();
	//~ CEE_part.report_support_stability();
	cout << endl;
	
	sep( "EL Supports" , 2 , '*' );
	EL_part.determine_support_stability();
	//~ EL_part.report_support_stability();
	cout << endl;
	
	// ___ End Parts Init ___
	
	// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
	
	
	sep( "Assembly Membership Test" );
	
	Incrementer assign_asm_ID{ 0 };
	
	// 4. Create an assembly, add parts, then remove parts.  Report number of parts at each step
	
	Assembly_ASM testAsm{ assign_asm_ID() };
	
	// Now add all the parts that we created to the assembly
	testAsm.add_part_w_pose( EL_part   , Pose_ASP{ Eigen::Vector3d( 0 , 0 , 0 ) , Eigen::Quaterniond( 1 , 0 , 0 , 0 ) } );
	testAsm.add_part_w_pose( CEE_part  , Pose_ASP{ Eigen::Vector3d( 0 , 0 , 0 ) , Eigen::Quaterniond( 1 , 0 , 0 , 0 ) } );
	testAsm.add_part_w_pose( CUBE_part , Pose_ASP{ Eigen::Vector3d( 0 , 0 , 0 ) , Eigen::Quaterniond( 1 , 0 , 0 , 0 ) } );
	
	cout << "After adding, the test assembly has " << testAsm.how_many_parts() << " parts" << endl;
	
	cout << "Removing parts ..." << endl;
	
	bool success = false;
	for( llin i = 0 ; i < 3 ; i++ ){
		success = testAsm.remove_part_w_ID( i );
		if( success )
			cout << "After removing ID " << i << ", the test assembly has " << testAsm.how_many_parts() << " parts" << endl;
		else
			cout << "Could not remove ID " << i << "!" << endl;
	}
	
	cout << endl;
	
	// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
	
	sep( "Pose Composition Test" );
	
	Pose_ASP P1{ Eigen::Vector3d( 1 , 0 , 0 ) , Eigen::Quaterniond( 0.924 , 0.383 , 0.000 , 0.000 ) };
	Pose_ASP P2{ Eigen::Vector3d( 0 , 0 , 1 ) , Eigen::Quaterniond( 0.924 , 0.383 , 0.000 , 0.000 ) };
	Pose_ASP P3 = P1 * P2;
	
	cout << P3 << endl;
	
	cout << endl;
	
	// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
	
	sep( "Assembly Pose Test" );
	
	// Add parts back into the assembly
	//~ testAsm.add_part_w_pose( EL_part   , Pose_ASP{ Eigen::Vector3d(  0.2 ,  0.2 ,  0.2 ) , 
	testAsm.add_part_w_pose( EL_part   , Pose_ASP{ Eigen::Vector3d(  0.2 * ( 0.25 - 0.075 ) , 0.0 , 0.2 * 0.125 ) , 
												   Eigen::Quaterniond(  0.000 ,  0.000 ,  0.707 , -0.707 ) } );
	testAsm.add_part_w_pose( CEE_part  , Pose_ASP{ Eigen::Vector3d(  0.0 ,  0.0 ,  0.0 ) , 
												   Eigen::Quaterniond(  1.000 ,  0.000 ,  0.000 ,  0.000 ) } );
	
	testAsm.set_pose( Pose_ASP{ Eigen::Vector3d( 0 , 0 , 1 ) , Eigen::Quaterniond( 0.924 , 0.383 , 0.000 , 0.000 ) } );
	
	cout << "Computing assembly stability ... ";
	testAsm.determine_support_stability();
	cout << "COMPLETE!" << endl;
	
	testAsm.report_support_stability();
	
	cout << "EL Pose:  " << testAsm.get_part_ref_w_ID( 0 ).get_pose() << endl;
	cout << "CEE Pose: " << testAsm.get_part_ref_w_ID( 1 ).get_pose() << endl;
	
	// Make sure that the faces are in the correct pose
	EL_part.calc_all_faces();
	CEE_part.calc_all_faces();
	
	/// === VISUALIZATION ==================================================================================================================
	
	// == ROS Loop ==
	
	// = ROS Start =
	
	// 5. Start ROS
	// ~ Animation Vars ~
	srand( ( time( 0 ) % 1 ) * 1000.0 + getpid() ); // Random seed based on the current clock time
	int updateHz = 30;
	//~ Eigen::Quaterniond deltaQuat{ 0.990 , 0.059 , 0.064 , 0.108 }; // Turn by this much every frame
	Eigen::Quaterniond deltaEX{ 0.999 , 0.052 , 0.000 , 0.000 }; // Turn by this much every frame
	Eigen::Quaterniond deltaEY{ 0.999 , 0.000 , 0.052 , 0.000 }; // Turn by this much every frame
	Eigen::Quaterniond deltaEZ{ 0.999 , 0.000 , 0.000 , 0.052 }; // Turn by this much every frame
	Pose_ASP temp;
	
	// Connect to ROS
	ros::init( argc , argv , "adjacency_test_cpp" );
	// Set up a node with refresh rate
	ros::NodeHandle node_handle;
	ros::Publisher vis_arr = node_handle.advertise<visualization_msgs::MarkerArray> ( "visualization_marker_array" , 100 );
	ros::Rate r( updateHz ); // 30Hz refresh rate
	
	// Set up visualization management
	RViz_MarkerManager mrkrMngr{};
	visualization_msgs::MarkerArray& markerArr = mrkrMngr.get_arr();
	
	std::vector<bool> stabilityVec = testAsm.get_stability_determination();
	size_t len = stabilityVec.size();
	Eigen::Vector2d offsetStep{ 0.30 , 0.00 };
	Eigen::Vector2d putdown_XY{ 0.00 , 0.00 };
	
	// 1. For each of the possible supports
	for( size_t i = 0 ; i < len ; i++ ){

		// 2. Determine if the support is stable
		if( stabilityVec[i] ){
			
			// 3. Calc the offset
			putdown_XY += offsetStep;
			
			// 4. Set the putdown pose
			testAsm.set_pose( testAsm.get_lab_pose_for_support_index( i , putdown_XY ) );
			
			//~ // 5. Calc positions of the face vertices
			//~ EL_part.calc_all_faces();
			
			// 6. For each of the faces , create a marker and load them into the marker manager , cycling through colors
			testAsm.load_face_mrkrs_into_mngr( mrkrMngr );
		}
	}
	
	//~ testAsm.load_face_mrkrs_into_mngr( mrkrMngr );
	
	
	// 6. Set the animation loop and display
	while ( ros::ok() ){ // while roscore is running
        // cout << ".";

		// Wait for a subscriber to start looking for this topic
		while ( vis_arr.getNumSubscribers() < 1 ){
			if ( !ros::ok() ){ return 0; }
			printf( "." );
			ROS_WARN_ONCE( "Please create a subscriber to the marker" );
			sleep( 1 );
			cout << ".";
		} 
		
		//~ temp = testAsm.get_pose();
		//~ temp.orientation = temp.orientation * deltaEZ;
		//~ temp.orientation = temp.orientation * deltaEY;
		//~ temp.orientation = temp.orientation * deltaEX;
		//~ temp.orientation.normalize();
		//~ testAsm.set_pose( temp );
        
        //~ markerArr = mrkrMngr.rebuild_mrkr_array();
        
        vis_arr.publish( markerArr );

		r.sleep();
	}
	
	
	/// ___ END VISUAL _____________________________________________________________________________________________________________________
	
	return 0; // I guess everything turned out alright at the end!
}



// ___ End main ____________________________________________________________________________________________________________________________


/* === Spare Parts =========================================================================================================================

// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
	
	sep( "Visualizing Stability" );
	
	// == ROS Loop ==
	
	// = ROS Start =
	
	
	// 5. Start ROS
	srand( ( time( 0 ) % 1 ) * 1000.0 + getpid() ); // Random seed based on the current clock time
	
	ros::init( argc , argv , "stable_test_cpp" );
	
	
	// ros::Publisher vis_pub = node_handle.advertise<visualization_msgs::Marker>(       "visualization_marker"       , 0 );
	ros::NodeHandle node_handle;
	ros::Publisher vis_arr = node_handle.advertise<visualization_msgs::MarkerArray> ( "visualization_marker_array" , 100 );
	int updateHz = 30;
	ros::Rate r( updateHz ); // 30Hz refresh rate
	
	RViz_MarkerManager mrkrMngr{};
	
	std::vector<bool> stabilityVec = EL_part.get_stability_determination();
	size_t len = stabilityVec.size();
	Eigen::Vector2d offsetStep{ 0.30 , 0.00 };
	Eigen::Vector2d putdown_XY{ 0.00 , 0.00 };
	
	// 1. For each of the possible supports
	for( size_t i = 0 ; i < len ; i++ ){

		// 2. Determine if the support is stable
		if( stabilityVec[i] ){
			
			// 3. Calc the offset
			putdown_XY += offsetStep;
			
			// 4. Set the putdown pose
			EL_part.set_pose( EL_part.get_lab_pose_for_support_index( i , putdown_XY ) );
			
			// 5. Calc positions of the face vertices
			EL_part.calc_all_faces();
			
			// 6. For each of the faces , create a marker and load them into the marker manager , cycling through colors
			EL_part.load_face_mrkrs_into_mngr( mrkrMngr );
		}
	}
	
	// Fetch the markers for publishing
	visualization_msgs::MarkerArray& markerArr = mrkrMngr.get_arr();
	
	// 7. Set the animation loop and display
	while ( ros::ok() ){ // while roscore is running
        // cout << ".";

		// Wait for a subscriber to start looking for this topic
		while ( vis_arr.getNumSubscribers() < 1 ){
			if ( !ros::ok() ){ return 0; }
			printf( "." );
			ROS_WARN_ONCE( "Please create a subscriber to the marker" );
			sleep( 1 );
			cout << ".";
		} 
        
        // markerArr = mrkrMngr.rebuild_mrkr_array();
        
        vis_arr.publish( markerArr );

		r.sleep();
	}
	
	// __ End ROS __

   ___ End Spare ___________________________________________________________________________________________________________________________
*/
