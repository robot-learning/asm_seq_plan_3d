/*
base_part_test.cpp
James Watson , 2018 February
Test integration of the ASP_3D lib --> ROS , Show markers and move them around

Dependencies: Cpp_Helpers , ROS , Eigen , RAPID
Template Version: 2017-09-23
*/

// === Init ================================================================================================================================

// ~~~ Includes ~~~
// ~~ ROS ~~
// ~ Core ~
#include <ros/ros.h> // --- ROS presides over all
#include <ros/package.h> // Where are we? // http://wiki.ros.org/Packages#C.2B-.2B-
// ~~ Includes ~~
#include <Eigen/Core> // ---- The living heart of Eigen
#include <Eigen/Dense> // --- Cross Product , etc.
#include <Eigen/Geometry> //- Quaternion , etc
// ~ RViz ~
#include <visualization_msgs/Marker.h> // ---- Display geometry
#include <visualization_msgs/MarkerArray.h> // RViz marker array
// ~~ Local ~~ 
#include <Cpp_Helpers.h> // Shortcuts and Aliases , and Functions of general use for C++ programming
#include <ASP_3D.h> // ---- Assembly geometry
#include <AsmSeqPlan.h> //- Graph planning


// using namespace quickhull;

// ___ End Init ____________________________________________________________________________________________________________________________


/// === PREBUILT ASSEMBLIES ================================================================================================================

Incrementer assign_part_ID{ 0 }; // Instantiate a functor that will assign a unique number to each part
Incrementer assign_asm_ID{ 0 }; //- Instantiate a functor that will assign a unique number to each assembly

Assembly_ASM* simple_phone(){
	
	Assembly_ASM* simplePhone = new Assembly_ASM( assign_asm_ID() );
	
	// ~~ 0. Create file paths ~~
	// ~ Fetch the folder to the STL files ~
	string pkgPath = ros::package::getPath( "motion_cost" );
	string resourcePath = pkgPath + "/Resources/Assembly_CAD/SimplePhone/";
	// ~ Create full paths to STLs ~
	std::vector<string> fNames;
/* 0*/ fNames.push_back( resourcePath + "body_SCL.stl" );      Eigen::Vector3d body_shift{    -3.92054e-05 ,  9.47465e-05 , -0.00919433 };
/* 1*/ fNames.push_back( resourcePath + "antenna_SCL.stl" );   Eigen::Vector3d antenna_shift{  0.005       , -0.0175      , -0.00375    };
/* 2*/ fNames.push_back( resourcePath + "bridgeAB_SCL.stl" );  Eigen::Vector3d bridgeAB_shift{ 0.0         ,  0.0         , -0.0025     };
/* 3*/ fNames.push_back( resourcePath + "bridgeBC_SCL.stl" );  Eigen::Vector3d bridgeBC_shift{ 0.0         ,  0.0         , -0.0025     };
/* 4*/ fNames.push_back( resourcePath + "chipA1_SCL.stl" );    Eigen::Vector3d chipA1_shift{   0.0         ,  0.0         , -0.0025     };
/* 5*/ fNames.push_back( resourcePath + "chipA1_SCL.stl" );    
/* 6*/ fNames.push_back( resourcePath + "chipB1_SCL.stl" );    Eigen::Vector3d chipB1_shift{  -0.002       ,  0.00666667  , -0.0025     };
/* 7*/ fNames.push_back( resourcePath + "chipB2_SCL.stl" );    Eigen::Vector3d chipB2_shift{   0.00371429  ,  0.0114286   , -0.0025     };
/* 8*/ fNames.push_back( resourcePath + "chipC1_SCL.stl" );    Eigen::Vector3d chipC1_shift{   0.0         , -0.00697957  , -0.0025     };
/* 9*/ fNames.push_back( resourcePath + "moduleA_SCL.stl" );   Eigen::Vector3d moduleA_shift{  0.0         ,  0.000178571 , -0.00446429 };
/*10*/ fNames.push_back( resourcePath + "moduleB_SCL.stl" );   Eigen::Vector3d moduleB_shift{  7.01754e-05 ,  0.0         , -0.00453947 };
/*11*/ fNames.push_back( resourcePath + "moduleC_SCL.stl" );   Eigen::Vector3d moduleC_shift{  0.0         ,  1.39405e-05 , -0.00428903 };
/*12*/ fNames.push_back( resourcePath + "panel_SCL.stl" );     Eigen::Vector3d panel_shift{    0.0         , 0.0          , -0.00125    };
	// ~ Create parts ~
	size_t fLen = fNames.size();
	std::vector<Part_ASM> phoneParts;
	for( size_t i = 0 ; i < fLen ; i++ ){
		cout << fNames[i] << endl;
		phoneParts.push_back( Part_ASM{   assign_part_ID() , fNames[i]   } );
	}
	cout << "Added " << phoneParts.size() << " parts." << endl;
	
	// 2. Create an assembly , add parts
	
	// ~ body ~
/* 0*/ simplePhone->add_part_w_pose( phoneParts[ 0] , Pose_ASP{ -body_shift , no_turn_quat() } ); // body
/*10*/ simplePhone->add_part_w_pose( phoneParts[10] , Pose_ASP{ -moduleB_shift  + Eigen::Vector3d(  0.0000 ,  0.00000 ,  0.0025 ) , // moduleB
		                                                     no_turn_quat()                                       } ); 
/* 9*/ simplePhone->add_part_w_pose( phoneParts[ 9] , Pose_ASP{ -moduleA_shift  + Eigen::Vector3d(  0.0000 ,  0.04000 ,  0.0025 ) , // moduleA
		                                                     no_turn_quat()                                       } ); 
/*11*/ simplePhone->add_part_w_pose( phoneParts[11] , Pose_ASP{ -moduleC_shift  + Eigen::Vector3d(  0.0000 , -0.04000 ,  0.0025 ) , // moduleC
		                                                     no_turn_quat()                                       } ); 

if( true ){ // Disable all but a known connected subset
	/* 4*/ simplePhone->add_part_w_pose( phoneParts[ 4] , Pose_ASP{ -chipA1_shift   + Eigen::Vector3d(  0.0000 ,  0.04875 ,  0.0100 ) , // chipA1
																 no_turn_quat()                                       } ); 
	/* 5*/ simplePhone->add_part_w_pose( phoneParts[ 5] , Pose_ASP{ -chipA1_shift   + Eigen::Vector3d(  0.0000 ,  0.03875 ,  0.0100 ) , // chipA1
																 no_turn_quat()                                       } ); 
	/* 2*/ simplePhone->add_part_w_pose( phoneParts[ 2] , Pose_ASP{ -bridgeAB_shift + Eigen::Vector3d(  0.0000 ,  0.02250 ,  0.0100 ) , // bridgeAB
																 no_turn_quat()                                       } ); 
	/* 6*/ simplePhone->add_part_w_pose( phoneParts[ 6] , Pose_ASP{ -chipB1_shift   + Eigen::Vector3d( -0.0075 ,  0.01000 ,  0.0100 ) , // chipB1
																 no_turn_quat()                                       } ); 
	/* 7*/ simplePhone->add_part_w_pose( phoneParts[ 7] , Pose_ASP{ -chipB2_shift   + Eigen::Vector3d(  0.0075 ,  0.01000 ,  0.0100 ) , // chipB2
																 no_turn_quat()                                       } ); 
	/* 3*/ simplePhone->add_part_w_pose( phoneParts[ 3] , Pose_ASP{ -bridgeBC_shift + Eigen::Vector3d(  0.0000 , -0.02500 ,  0.0100 ) , // bridgeBC
																 no_turn_quat()                                       } ); 
	/* 8*/ simplePhone->add_part_w_pose( phoneParts[ 8] , Pose_ASP{ -chipC1_shift   + Eigen::Vector3d(  0.0000 , -0.04750 ,  0.0100 ) , // chipC1
																 no_turn_quat()                                       } ); 
	/* 1*/ simplePhone->add_part_w_pose( phoneParts[ 1] , Pose_ASP{ -antenna_shift  + Eigen::Vector3d( -0.0200 ,  0.05250 ,  0.0050 ) , // antenna
																 no_turn_quat()                                       } ); 
}
	
	// 3. Determine the local freedom of each part (NDBG)
	simplePhone->recalc_geo();
	
	// 4. Print a report
	simplePhone->assembly_report();
	
	
	sep( "Create the Liaison Graph" );
	simplePhone->populate_liaison_graph_from_NDBG();
	
	simplePhone->populate_shared_area_graph_from_adjacencies( 30 );
	
	return simplePhone;
}

Assembly_ASM* simple_cube(){
	
	Assembly_ASM* simpleCube = new Assembly_ASM( assign_asm_ID() );
	
	// ~~ 0. Create file paths ~~
	// ~ Fetch the folder to the STL files ~
	string pkgPath = ros::package::getPath( "motion_cost" );
	string resourcePath = pkgPath + "/Resources/Assembly_CAD/SimpleCube/";
	
	// ~ Create full paths to STLs ~
	std::vector<string> fNames;
	fNames.push_back( resourcePath + "PocketPlate.stl" );	Eigen::Vector3d pcktShift{ -1.5 , -1.5 , -0.242647 }; 
	fNames.push_back( resourcePath + "MeterCube.stl" ); 	Eigen::Vector3d cubeShift{ -0.5 , -0.5 , -0.5      }; 
	
	// ~ Create parts ~
	size_t fLen = fNames.size();
	std::vector<Part_ASM> cubeParts;
	for( size_t i = 0 ; i < fLen ; i++ ){
		cout << fNames[i] << endl;
		cubeParts.push_back( Part_ASM{   assign_part_ID() , fNames[i]   } );
	}
	cout << "Added " << cubeParts.size() << " parts." << endl;
	
	simpleCube->add_part_w_pose( cubeParts[ 0] , Pose_ASP{ -pcktShift + Eigen::Vector3d(  0.0000 ,  0.0000 ,  0.0000 ) , // Pocket Plate
																 no_turn_quat()                                        } ); 
	simpleCube->add_part_w_pose( cubeParts[ 1] , Pose_ASP{ -cubeShift + Eigen::Vector3d(  1.0000 ,  1.0000 ,  0.2500 ) , // Meter Cube
																 no_turn_quat()                                        } ); 
	
	return simpleCube;
}

/// ___ END PREBUILT _______________________________________________________________________________________________________________________


// === main ================================================================================================================================

int main( int argc , char** argv ){
	
	srand( time( 0 ) ); // Random seed based on the current clock time
	
	// Set up visualization management
	RViz_MarkerManager mrkrMngr{};
	visualization_msgs::MarkerArray& markerArr = mrkrMngr.get_arr();
	
	// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
	
	if( true ){
	
		sep( "Create the Simple Phone" );
		
		Assembly_ASM* simplePhone = simple_phone();
		
		cout << "About to calculate part fitness ..." << endl;
		
		std::map< llin , BasePartFitness > scores =  
			calc_base_fitness_for_parts( *simplePhone , 0.4 , 0.2 , 0.4 );  
		
		cout << "There are " << scores.size() << " scores in the map." << endl;
			
		sep( "Calculate Base Part Fitness" );
			
		std::vector<llin> partID = simplePhone->get_part_ID_vec();
		size_t numParts = partID.size();
		for( size_t i = 0 ; i < numParts ; i++ ){
			cout << "Part: " << partID[i] << " , Fitness: " << scores[ partID[i] ].baseFitness << endl;
		}
		
	}else if( true ){
		
		sep( "Create the Simple Cube" );
		
		Assembly_ASM* simpleCube = simple_cube();
		
		simpleCube->set_pose( origin_pose() );
		simpleCube->load_part_mrkrs_into_mngr( mrkrMngr );
		
		// 3. Determine the local freedom of each part (NDBG)
		simpleCube->recalc_geo();
		
		sep( "Calculate Shared Area for Simple Cube" );
		
		cout << "Number of constraints b/n Plate and Cube: " << simpleCube->how_many_constraints( 0 , 1 ) << endl;
		
		simpleCube->populate_shared_area_graph_from_adjacencies( 30 );
		print_vec_vec( simpleCube->get_shared_area_graph() );
		
		cout << "Fetch part-specific shared area: " << simpleCube->how_much_shared_area( 0 ) 
			 << " , " << simpleCube->how_much_shared_area( 1 ) << endl;
			 
		sep( "Removal Interface" );
		
		std::vector<llin> movd;  movd.push_back( 1 ); // Move cube
		std::vector<llin> rfrc;  rfrc.push_back( 0 ); // Stay plate
		
		cout << "Removal Interface Area , Cube from Plate: " << simpleCube->removal_interface_area( movd , rfrc ) << endl;
		
	}
	
	/// === VISUALIZATION ==================================================================================================================
	
	// == ROS Loop ==
	
	// = ROS Start =
	
	// 5. Start ROS
	
	// ~ Animation Vars ~
	srand( ( time( 0 ) % 1 ) * 1000.0 + getpid() ); // Random seed based on the current clock time
	int updateHz = 30;
	Eigen::Vector2d offsetStep{ 0.30 , 0.00 };
	Eigen::Vector2d asmPlnStep{ 0.00 , 0.40 };
	
	
	
	// ~ Visualize Results ~
	
		
	
	// std::vector<SuccessCode> vizChk = mrkrMngr.diagnose_markers_OK();
	
	// Connect to ROS
	ros::init( argc , argv , "graph_test_cpp" );
	// Set up a node with refresh rate
	ros::NodeHandle node_handle;
	ros::Publisher vis_arr = node_handle.advertise<visualization_msgs::MarkerArray> ( "visualization_marker_array" , 100 );
	ros::Rate r( updateHz ); // 30Hz refresh rate
	
	// 6. Set the animation loop and display
	while ( ros::ok() ){ // while roscore is running
        // cout << ".";

		// Wait for a subscriber to start looking for this topic
		while ( vis_arr.getNumSubscribers() < 1 ){
			if ( !ros::ok() ){ return 0; }
			printf( "." );
			ROS_WARN_ONCE( "Please create a subscriber to the marker" );
			sleep( 1 );
			cout << ".";
		} 
        
        vis_arr.publish( markerArr );

		r.sleep();
	}
	
	/// ___ END VISUALIZATION ______________________________________________________________________________________________________________
	
	return 0; // I guess everything turned out alright at the end!
}

// ___ End main ____________________________________________________________________________________________________________________________


/* === Spare Parts =========================================================================================================================


	
   ___ End Spare ___________________________________________________________________________________________________________________________
*/

