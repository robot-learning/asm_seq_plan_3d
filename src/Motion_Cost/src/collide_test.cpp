#include <RAPID.H>
#include <math.h>
#include <stdio.h>

#define LISTS 1

main()
{
  // First, get a couple of RAPID_models in which to put our models

  RAPID_model *b1 = new RAPID_model;
  RAPID_model *b2 = new RAPID_model;
  
  // Then, load the models with triangles.  The following loads each 
  // with a torus of 2*n1*n2 triangles.

  fprintf(stderr, "loading tris into RAPID_model objects...");  fflush(stderr);
  
  double a = 1.0;  // major radius of the tori
  double b = 0.2;  // minor radius of the tori

  int n1 = 50;     // tori will have n1*n2*2 triangles each
  int n2 = 50;

  int uc, vc;
  int count = 0;
  
  for(uc=0; uc<n1; uc++)
    for(vc=0; vc<n2; vc++)
      {
        double u1 = (2.0*M_PI*uc) / n1; 
        double u2 = (2.0*M_PI*(uc+1)) / n1; 
        double v1 = (2.0*M_PI*vc) / n2; 
        double v2 = (2.0*M_PI*(vc+1)) / n2; 

        double p1[3], p2[3], p3[3], p4[3];

        p1[0] = (a - b * cos(v1)) * cos(u1);
        p2[0] = (a - b * cos(v1)) * cos(u2);
        p3[0] = (a - b * cos(v2)) * cos(u1);
        p4[0] = (a - b * cos(v2)) * cos(u2);
        p1[1] = (a - b * cos(v1)) * sin(u1);
        p2[1] = (a - b * cos(v1)) * sin(u2);
        p3[1] = (a - b * cos(v2)) * sin(u1);
        p4[1] = (a - b * cos(v2)) * sin(u2);
        p1[2] = b * sin(v1);
        p2[2] = b * sin(v1);
        p3[2] = b * sin(v2);
        p4[2] = b * sin(v2);

        b1->AddTri(p1, p2, p3, count);
        b1->AddTri(p4, p2, p3, count+1);
        b2->AddTri(p1, p2, p3, count);
        b2->AddTri(p4, p2, p3, count+1);

        count += 2;
      }
  fprintf(stderr, "done\n");  fflush(stderr);
  fprintf(stderr, "Tori have %d triangles each.\n", count);
  fprintf(stderr, "building hierarchies...");  fflush(stderr);
  b1->EndModel();
  b2->EndModel();
  fprintf(stderr, "done.\n"); fflush(stderr); 
  
  // Now we are free to call the interference detection routine.
  // But first, construct the transformations which define the placement
  // of our two hierarchies in world space:

  // This placement causes them to overlap a large amount.

  double R1[3][3], R2[3][3], T1[3], T2[3];
  
  R1[0][0] = R1[1][1] = R1[2][2] = 1.0; // Object 1 orientation , 3 x 3 rotation matrix
  R1[0][1] = R1[1][0] = R1[2][0] = 0.0;
  R1[0][2] = R1[1][2] = R1[2][1] = 0.0;

  R2[0][0] = R2[1][1] = R2[2][2] = 1.0; // Object 2 orientation , 3 x 3 rotation matrix
  R2[0][1] = R2[1][0] = R2[2][0] = 0.0;
  R2[0][2] = R2[1][2] = R2[2][1] = 0.0;
  
  T1[0] = 1.0;  T1[1] = 0.0; T1[2] = 0.0; // Object 1 translation , 3 x 1 vector
  T2[0] = 0.0;  T2[1] = 0.0; T2[2] = 0.0; // Object 2 translation , 3 x 1 vector

  // Now we can perform a collision query:

  RAPID_Collide(R1, T1, b1, R2, T2, b2, RAPID_ALL_CONTACTS);

  // Looking at the report, we can see where all the contacts were, and
  // also how many tests were necessary:

  printf("All contacts between overlapping tori:\n");
  
  printf("Num box tests: %d\n", RAPID_num_box_tests);
  printf("Num contact pairs: %d\n", RAPID_num_contacts);
#if LISTS
  int i;
  for(i=0; i<RAPID_num_contacts; i++)
    {
      printf("\t contact %4d: tri %4d and tri %4d\n", 
             i, RAPID_contact[i].id1, RAPID_contact[i].id2);
    }
#endif

  // Notice the RAPID_ALL_CONTACTS flag we used in the call to collide().
  // The alternative is to use the RAPID_FIRST_CONTACT flag, instead,
  // so that the collide routine searches for contacts until it locates
  // the first one.  It takes many many fewer tests to locate a single
  // contact this way.

  RAPID_Collide(R1, T1, b1, R2, T2, b2, RAPID_FIRST_CONTACT); // Shortcut on first triangle collision

  printf("First contact between overlapping tori:\n");
  
  printf("Num box tests: %d\n", RAPID_num_box_tests);
  printf("Num contact pairs: %d\n", RAPID_num_contacts);
#if LISTS
  for(i=0; i<RAPID_num_contacts; i++)
    {
      printf("\t contact %4d: tri %4d and tri %4d\n", 
             i, RAPID_contact[i].id1, RAPID_contact[i].id2);
    }
#endif

  // By rotating one of them around the x-axis 90 degrees, they 
  // are now interlocked, but not quite touching.

  R1[0][0] = 1.0;  R1[0][1] = 0.0;  R1[0][2] = 0.0;
  R1[1][0] = 0.0;  R1[1][1] = 0.0;  R1[1][2] =-1.0;
  R1[2][0] = 0.0;  R1[2][1] = 1.0;  R1[2][2] = 0.0;
  
  RAPID_Collide(R1, T1, b1, R2, T2, b2, RAPID_FIRST_CONTACT);

  printf("No contact between interlocked but nontouching tori:\n");
  
  printf("Num box tests: %d\n", RAPID_num_box_tests);
  printf("Num contact pairs: %d\n", RAPID_num_contacts);
#if LISTS
  for(i=0; i<RAPID_num_contacts; i++)
    {
      printf("\t contact %4d: tri %4d and tri %4d\n", 
             i, RAPID_contact[i].id1, RAPID_contact[i].id2);
    }
#endif

  // By moving one of the tori closer to the other, they
  // almost touch.  This is the case that requires a lot
  // of work wiht methods which use bounding boxes of limited
  // aspect ratio.  Oriented bounding boxes are more efficient
  // at determining noncontact than spheres, octree, or axis-aligned
  // bounding boxes for scenarios like this.  In this case, the interlocked
  // tori are separated by 0.0001 at their closest point.


  T1[0] = 1.5999;
  
  RAPID_Collide(R1, T1, b1, R2, T2, b2, RAPID_FIRST_CONTACT);

  printf("Many tests required for interlocked but almost touching tori:\n");
  
  printf("Num box tests: %d\n", RAPID_num_box_tests);
  printf("Num contact pairs: %d\n", RAPID_num_contacts);
#if LISTS
  for(i=0; i<RAPID_num_contacts; i++)
    {
      printf("\t contact %4d: tri %4d and tri %4d\n", 
             i, RAPID_contact[i].id1, RAPID_contact[i].id2);
    }
#endif

	printf( "\nTwo-Triangle Contact Test\n" );  

	// Allocate models
	RAPID_model *tri1 = new RAPID_model;
	RAPID_model *tri2 = new RAPID_model;
	double p1[3] , p2[3] , p3[3];

	// Reset matrices to no rotation
	
	R1[0][0] = R1[1][1] = R1[2][2] = 1.0; // Diagonals = 1
	R1[0][1] = R1[1][0] = R1[2][0] = 0.0; // Off-diag  = 0
	R1[0][2] = R1[1][2] = R1[2][1] = 0.0;

	R2[0][0] = R2[1][1] = R2[2][2] = 1.0; // Diagonals = 1
	R2[0][1] = R2[1][0] = R2[2][0] = 0.0; // Off-diag  = 0
	R2[0][2] = R2[1][2] = R2[2][1] = 0.0;
	
	
	tri1->BeginModel();
	
	p1[0] = 6.0;  p1[1] = 3.0;  p1[2] = 0.0;  
	p2[0] = 2.0;  p2[1] = 5.0;  p2[2] = 0.0;  
	p3[0] = 2.0;  p3[1] = 1.0;  p3[2] = 0.0;  
	
	tri1->AddTri( p1 , p2 , p3 , 1 );
	
	tri1->EndModel();
	
	
	tri2->BeginModel();
	
	p1[0] = 4.0;  p1[1] = 5.0;  p1[2] = 0.0;  
	p2[0] = 0.0;  p2[1] = 3.0;  p2[2] = 0.0;  
	p3[0] = 4.0;  p3[1] = 1.0;  p3[2] = 0.0;  
	
	tri2->AddTri( p1 , p2 , p3 , 1 );
	
	tri2->EndModel();
	
	float distance = 1.0;
	
	T1[0] = 0.0;  T1[1] = 0.0;  T1[2] = 0.0;  
	T2[0] = 0.0;  T2[1] = 0.0;  T2[2] = distance;  
	
	for( int i = 0 ; i < 12 ; i++ ){
		
		distance /= 10; // Reduce distance by a power of 10
		T2[2] = distance;  
		RAPID_Collide( R1 , T1 , tri1 , R2 , T2 , tri2 , RAPID_FIRST_CONTACT );
		printf( "At a distance of: %.14f , " , distance );
		if( RAPID_num_contacts > 0 ){
			printf( "COLLISION! \n");
		}else{
			printf( "No collision \n");
		}
	}
	
	distance = 0.0; // Reduce distance to 0
	T2[2] = distance;  
	RAPID_Collide( R1 , T1 , tri1 , R2 , T2 , tri2 , RAPID_FIRST_CONTACT );
	printf( "At a distance of: %.14f , " , distance );
	if( RAPID_num_contacts > 0 ){
		printf( "COLLISION! \n");
	}else{
		printf( "No collision \n");
	}

	// Then, load the models with triangles.  The following loads each 
	// with a torus of 2*n1*n2 triangles.

	

  return 0;  
}
