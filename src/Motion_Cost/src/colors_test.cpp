#include <igl/readOFF.h>
#include <igl/viewer/Viewer.h>
#include <igl/jet.h>
#include "tutorial_shared_path.h"

#include "Cpp_Helpers.h"

#include <ros/ros.h> // --------------------- ROS presides over all
#include <ros/package.h> // ----------------- Where are we? // http://wiki.ros.org/Packages#C.2B-.2B-

Eigen::MatrixXd V;
Eigen::MatrixXi F;
Eigen::MatrixXd C;

size_t i = 0;

int main( int argc , char *argv[] ){
	
	// Load a mesh in OFF format
	string pkgPath = ros::package::getPath("motion_cost");
	cout << "Found the package path for 'motion_cost' : " << pkgPath << endl;
	string filename = pkgPath + "/shared/screwdriver.off";
	cout << "Constructed the full STL path : " << filename << endl;
	igl::readOFF( filename , V , F );

	// Plot the mesh
	igl::viewer::Viewer viewer;
	viewer.data.set_mesh(V, F);

	// Use the z coordinate as a scalar field over the surface
	Eigen::VectorXd Z = V.col( 2 );

	// Compute per-vertex colors
	igl::jet( Z , true , C );

	printf( "There are %zi facets \n" , F.rows() );
	printf( "There are %zi vertices \n" , V.rows() );
	
	printf( "There are %zi rows in the color matrix \n" , C.rows() );
	
	sep( "COLORS!" );
	
	if( false ){
		for( i = 0 ; i < C.rows() ; i++ ){ cout << C.row( i ) << endl; }
	}
	
	/*
	There are 6786 facets 
	There are 3395 vertices 
	There are 3395 rows in the color matrix <-- There is a color defined for each vertex
	*/

	// Add per-vertex colors
	viewer.data.set_colors( C );

	// Launch the viewer
	viewer.launch();
}
