/***********  
comm_ASM.cpp
James Watson , 2018 June
Messages, Communication, and Visualization for Assembly Planning

Template Version: 2018-06-07
***********/

#include "comm_ASM.h"

/// ==== MESSAGES ==========================================================================================================================

geometry_msgs::Pose ROS_Pose_from_Pose_ASP( const Pose_ASP& pose ){
	// Load an Pose_ASP into a ROS Pose message and return
	geometry_msgs::Pose rtnPose;
	rtnPose.position.x    = pose.position(0);
	rtnPose.position.y    = pose.position(1);
	rtnPose.position.z    = pose.position(2);
	rtnPose.orientation.w = pose.orientation.w();
	rtnPose.orientation.x = pose.orientation.x();
	rtnPose.orientation.y = pose.orientation.y();
	rtnPose.orientation.z = pose.orientation.z();
	return rtnPose;
}

Pose_ASP ASP_Pose_from_Pose_ROS( const geometry_msgs::Pose& pose ){
    // Return the Pose_ASP that corresponds to the ROS pose
    return pose_from_pXYZ_oWXYZ( pose.position.x , 
                                 pose.position.y , 
                                 pose.position.z , 
                                 pose.orientation.w , 
                                 pose.orientation.x , 
                                 pose.orientation.y , 
                                 pose.orientation.z );;
}

std::vector<Pose_ASP> load_ASP_Poses_from_Poses_ROS( const std::vector<geometry_msgs::Pose>& poses ){
    std::vector<Pose_ASP> rtnVec;
    size_t len = poses.size();
    for( size_t i = 0 ; i < len ; i++ ){  rtnVec.push_back( ASP_Pose_from_Pose_ROS( poses[i] ) );  }
    return rtnVec;
}

void pack_joint_and_gripper_states_into_msg( const std::vector<double>& armState , // ---- Joint positions of arm
											 const std::vector<double>& fingState , // --- Joint positions of gripper
											 sensor_msgs::JointState& stateMsg ){ // Load state data here
	// Load arm joint and gripper joint states into the message parameter
	std::vector<double> totalPos = vec_copy( armState );
	extend_vec_with( totalPos , fingState );
	stateMsg.name     = vec_copy( ALL_JOINT_NAMES );
	stateMsg.position = totalPos;
}

Eigen::MatrixXd extract_Jacobian( const ll4ma_teleop::Joints2Jac& msg ){
	// Construct a matrix from the jacobian response object
	int numCols = msg.response.numJoints , 
		numDims = 6                      ,
		count   = 0                      ;
	Eigen::MatrixXd rtnMatx = Eigen::MatrixXd::Zero( numDims , numCols );
	for( int i = 0 ; i < numDims ; i++ ){
        for( int j = 0 ; j < numCols ; j++ ){
			rtnMatx(i,j) = msg.response.flatJac[ count ];
			count++;
		}
	}
	return rtnMatx;
}

/// ____ END MESSAGE _______________________________________________________________________________________________________________________


/// === ARM PUBLISHER WRAPPERS =============================================================================================================

// ros::Publisher& FKclient;
void send_joint_command::operator()( const std::vector<double>& q_joints7 , const std::vector<double>& fing_pos2 , size_t repeat ){
    sensor_msgs::JointState temp;
    std::vector<double> fingVec = { 0 , 0 };
    if( fing_pos2.size() != 0 ){  fingVec = vec_copy( fing_pos2 );  } // Set a default gripper state if none given
    pack_joint_and_gripper_states_into_msg( q_joints7 , // Joint positions of arm
                                            fingVec , // Joint positions of gripper
                                            temp ); // --- Load state data here
    for( size_t i = 0 ; i < repeat ; i++ ){  jntCmdPublisher.publish( temp );  }
}

/// ___ END PUBLISHER ______________________________________________________________________________________________________________________


/// ==== VISUALIZATION =====================================================================================================================

void display_sequence_without_animation( AsmStateNode_ASP* rootNode , RViz_MarkerManager& nodeMrkrMngr ,
										 Eigen::Vector2d planStart , Eigen::Vector2d actionIncr , Eigen::Vector2d remvOffset ){
	// Diagnose a disassembly sequence for a single level
	
	bool SHOWDEBUG = true;
	
	Eigen::Vector2d   planPoint = planStart;
	AsmStateNode_ASP* currNode  = rootNode;
	Assembly_ASM*     stateAsm;
	Assembly_ASM*     removAsm;
	size_t stateSup = 0 ,
		   removSup = 0 ,
		   count    = 0 ;
	Pose_ASP stateSave;
	Pose_ASP removSave;
	
	// 1. While the state is not null
	while( currNode ){
		stateAsm = currNode->assembly;  stateSup = stateAsm->get_support();  stateSave = stateAsm->get_pose();
		removAsm = currNode->removed;   
		if( removAsm ){  removSup = removAsm->get_support();  removSave = removAsm->get_pose();  }
		// 2. Set the assembly down on its support
		stateAsm->set_lab_pose_for_support_index( stateSup , planPoint );
		// 3. Set the removed down on its support, offset
		if( removAsm )
			removAsm->set_lab_pose_for_support_index( removSup , planPoint + remvOffset );
		// 4. Load markers
		stateAsm->load_part_mrkrs_into_mngr( nodeMrkrMngr );
		if( removAsm )
			removAsm->load_part_mrkrs_into_mngr( nodeMrkrMngr );
		// 5. Advance setdown location
		planPoint += actionIncr;
		// 6. Advance node
		if( SHOWDEBUG ){  cout << "About to advance to the next node ..." << endl;  }
		currNode = currNode->get_next();
		count++;
	}
	cout << "Processed " << count << " nodes." << endl;
}

void display_sequence_without_animation_ALTERNATE( AsmStateNode_ASP* rootNode , RViz_MarkerManager& nodeMrkrMngr ,
												   Eigen::Vector2d planStart , Eigen::Vector2d actionIncr , Eigen::Vector2d remvOffset ){
	// Diagnose a disassembly sequence for a single level
	
	bool SHOWDEBUG = true;
	
	Eigen::Vector2d   planPoint = planStart;
	Eigen::Vector3d   ofstPoint;
	AsmStateNode_ASP* currNode  = rootNode;
	Assembly_ASM*     stateAsm;
	Assembly_ASM*     stateLast = nullptr;
	Assembly_ASM*     removAsm;
	Assembly_ASM*     removLast = nullptr;
	size_t stateSup = 0 ,
		   removSup = 0 ,
		   count    = 0 ;
	Pose_ASP stateSave;
	Pose_ASP removSave;
	Pose_ASP offstPose;
	
	// 1. While the state is not null
	while( currNode ){
		stateAsm = currNode->assembly;  stateSup = stateAsm->get_support();  stateSave = stateAsm->get_pose();
		removAsm = currNode->removed;   
		size_t trajLen = currNode->seqPoses.size();
		
		if( SHOWDEBUG ){  
			cout << "Found a state assembly: _ " << ( stateAsm ? "Yes" : "No" ) << endl;  
			cout << "Found a removed assembly: " << ( removAsm ? "Yes" : "No" ) << endl;  
			cout << "Trajectory Length: ______ " << trajLen << endl;
		}
		
		// 2. For each REMOVE action
		if( removAsm ){
			
			if( trajLen > 0 ){
			
				// A. Place ASM in the specified pose , Offset according to the step in sequence
				stateSave = stateAsm->get_pose();
				ofstPoint = Eigen::Vector3d( (double) planPoint(0) , (double) planPoint(1) , 0 );
				stateAsm->set_pose(
					pose_from_position_orient( 
						stateSave.position + ofstPoint, 
						stateSave.orientation 
					)
				);
				stateAsm->load_part_mrkrs_into_mngr( nodeMrkrMngr );
				stateAsm->set_pose( stateSave );
				// B. Place the removed sub at the end of its trajectory , Offset according to the step in sequence
				size_t last_i = trajLen - 1;
				
				if( SHOWDEBUG ){  cout << "The last pose in the sequence is " << currNode->seqPoses[ last_i ] << endl;  }
				
				removAsm->set_pose( currNode->seqPoses[ last_i ] ); // don't have to reset this
				removAsm->load_part_mrkrs_into_mngr( nodeMrkrMngr );
				
			}else if( SHOWDEBUG ){  
				cout << "ERROR: Remove operation without a trajectory!" << endl;
			}
		// 3. For each ROTATE action	
		}else{
			// A. Put initial pose in line
			if( stateLast ){
				stateSave = stateLast->get_pose();
				ofstPoint = Eigen::Vector3d( (double) planPoint(0) , (double) planPoint(1) , 0 );
				stateLast->set_pose(
					pose_from_position_orient( 
						stateSave.position + ofstPoint, 
						stateSave.orientation 
					)
				);
				stateLast->load_part_mrkrs_into_mngr( nodeMrkrMngr );
				stateLast->set_pose( stateSave );
			}
			// B. Put final pose in offset
			stateSave = stateAsm->get_pose();
			ofstPoint = Eigen::Vector3d( ( (double) planPoint(0) + (double) remvOffset(0) ) , ( (double) planPoint(1) + (double) remvOffset(1) ) , 0 );
			stateAsm->set_pose(
				pose_from_position_orient( 
					stateSave.position + ofstPoint, 
					stateSave.orientation 
				)
			);
			stateAsm->load_part_mrkrs_into_mngr( nodeMrkrMngr );
			stateAsm->set_pose( stateSave );
		}
		
		stateLast = stateAsm;
		removLast = removAsm;
		
		// X. Advance offset location
		planPoint += actionIncr;
		
		if( SHOWDEBUG ){  cout << "About to advance to the next node ..." << endl;  }
		currNode = currNode->get_next();
		count++;
		
		if( SHOWDEBUG ){  cout << endl;  }
	}
	if( SHOWDEBUG ){  cout << "Processed " << count << " nodes." << endl;  }
	
}

PlayerFrame* spam_frame(){
	// Return a frame that is just cubes
	PlayerFrame* rtnFrame = new PlayerFrame();
	rtnFrame->mrkrMngr.spam_cubes();
	return rtnFrame;
}

/// ____ END VIZ ___________________________________________________________________________________________________________________________


/// === ASM ANIMATION ======================================================================================================================

template < typename F , typename J > 
PlanRecording recording_from_level_seq_simple( AsmStateNode_ASP* rootNode , 
											   Eigen::Vector3d floorCenter , 
											   size_t numRotDwellFrames , 
											   F serviceCallFunc , J jacobCallFunc ){
	
	bool VERBOSE   = false ,
		 SHOWDEBUG = false ,
		 TS_JACOB  = true  ;
	
	PlanRecording record;
	size_t count = 0;
	AsmStateNode_ASP* currNode = rootNode;
	
	std::vector<double> zeroArm = { 0.0 , 0.0 , 0.0 , 0.0 , 0.0 , 0.0 , 0.0 };
	std::vector<double> zeroFng = { 0.0 , 0.0 };
	
	Eigen::Vector3d ROBOT_BASE = Eigen::Vector3d( 0.55 , 0 , 1.20 ); // FUTURE: NEED A WAY TO LOAD THIS FROM A PARAM SERVER

	//  1. For every action
	while( currNode ){
		//  A. Determine if there is an action from the last state
		if( currNode->count_edges() > 0 ){
			
			if( SHOWDEBUG ){  cout << "There is an action to process ..." << endl;  }
			
			//  2. Determine whether it is a rotate or a remove action  &&  3. If it is a remove action
			if( currNode->edgeActions[0].action == ACT_REMOVE ){
				
				if( SHOWDEBUG ){  cout << "Rendering a REMOVE action ..." << endl;  }
				
				currNode = (*currNode)[0]; // Advance to the next state
				
				if( SHOWDEBUG ){  
					cout << "\tCurrent assembly contains: " << currNode->assembly->get_part_ID_vec() << endl;
					cout << "\tRemoved sub contains ___ : " << currNode->removed->get_part_ID_vec()  << endl;
				}
				
				
				if( SHOWDEBUG ){  cout << "About to plan arm trajectory for this action ..." << endl;  }
				
				currNode->removed->set_pose( currNode->seqPoses[0] );
				Eigen::Vector3d approachFrom = floorCenter + Eigen::Vector3d( 0.0 , 0.0 , 1.0 );
				Eigen::Vector3d workCenter   = floorCenter + Eigen::Vector3d( 0.0 , 0.0 , 0.2 );
				Eigen::MatrixXd approaches = Eigen::MatrixXd::Zero( 3 , 3 );
				approaches << -1 ,  0 ,  0 ,
				               0 , -1 ,  0 ,
				               0 ,  0 , -1 ;
				double tileSize = 0.005;
				std::vector<sensor_msgs::JointState> armTraj = arm_motion_for_trajectory( currNode->removed , currNode->seqPoses ,
																						  approachFrom , ROBOT_BASE , workCenter , 
																						  approaches , tileSize , 
																						  serviceCallFunc );
				size_t armTrajLen = armTraj.size();
						
						
				if( SHOWDEBUG ){  cout << "Arm states: " << armTrajLen << " , Poses: " << currNode->seqPoses.size() 
									   << " , Equal in number?: " << ( armTrajLen == currNode->seqPoses.size() ) << endl;  }
				
				if(  ( armTrajLen == currNode->seqPoses.size() )  &&  ( armTraj.size() > 1 )  ){
					double hiScore = -9001.0 ,
						   loScore =  9001.0 ,
						   score   =     0.0 ;
					std::vector<double> jntPos;
					Eigen::MatrixXd Jm;
					// 4. For each arm state
					for( size_t j = 0 ; j < armTrajLen ; j++ ){
						jntPos = extract_arm_state_from_msg( armTraj[j] );
						if( TS_JACOB ){ sep( "Extracted Joint Positon" , 4 , '#' );
										cout << jntPos << endl;  }
						// 5. Fetch Jacobian
						Jm = jacobCallFunc( jntPos );
						if( TS_JACOB ){  cout << "Received Jacobian:" << endl
											  << Jm << endl;  }
						// 6. Compute manip score
						score = manip_score_Yoshikawa85( Jm );
						if( TS_JACOB ){  cout << "Manip Score: " << score << endl;  }
						// A. Test highest
						hiScore = max( hiScore , score );
						if( TS_JACOB ){  cout << "High Score:_ " << hiScore << endl;  }
						// B. Test lowest
						loScore = min( loScore , score );
						if( TS_JACOB ){  cout << "Low Score: _ " << loScore << endl;  }
					}
					// 6. Compute execution time
					double armTime = arm_traj_duration( armTraj , LBR4_MAX_ANG_VEL , 1.0 );
					
					cout << endl;
					sep( "Processed an Arm Trajectory" , 4 , '%' );
					cout << "Highest Manip Score: " << hiScore << endl
						 << "Lowest Manip Score:_ " << loScore << endl
						 << "Time to Execute: ___ " << armTime << " seconds" << endl << endl;
					
				}else{
					
				}
				
				cout << "Arm trajectory planning COMPLETE! Creating frames ..." << endl;
				
				
				//  4. For each pose in sequence
				for( size_t i = 0 ; i < currNode->seqPoses.size() ; i++ ){
					PlayerFrame* frame = new PlayerFrame(); // Instantiate frame 
					//  5. Place the removed sub in the pose
					currNode->removed->set_pose( currNode->seqPoses[i] );
					//  6. Load the removed sub markers
					currNode->removed->load_part_mrkrs_into_mngr( frame->mrkrMngr );
					//  7. Load the current sub markers (these are static)
					currNode->assembly->set_lab_pose_for_index_3D( currNode->assembly->get_support() , floorCenter );
					currNode->assembly->load_part_mrkrs_into_mngr( frame->mrkrMngr );
					//  8. Load zero joint angles
					frame->jnt_state_ = armTraj[i];
					//  8.5. Load frame into record
					record.push_back( frame );
				}
				
				record.push_back( spam_frame() ); // Overwrite extra markers with a spam frame
				
			//  9. If it is a rotate action
			}else if( currNode->edgeActions[0].action == ACT_ROTATE ){
				
				cout << "Rendering a ROTATE action ..." << endl;
				
				currNode = (*currNode)[0]; // Advance to the next state
				
				cout << "\tCurrent assembly contains: " << currNode->assembly->get_part_ID_vec() << endl;
				
				cout << "Generating frames ..." << endl;
				
				// 10. For the number of dwell frames
				for( size_t j = 0 ; j < numRotDwellFrames ; j++ ){
					
					if( VERBOSE ){  cout << "Frame " << j+1 << " of " << numRotDwellFrames << " ..." << endl;  }
					
					PlayerFrame* frame = new PlayerFrame(); // Instantiate frame 
					// 11. Load the markers from the rotated sub (these are static)
					currNode->assembly->set_lab_pose_for_index_3D( currNode->assembly->get_support() , floorCenter );
					currNode->assembly->load_part_mrkrs_into_mngr( frame->mrkrMngr );
					// 12. Load zero joint angles
					sensor_msgs::JointState temp;
					pack_joint_and_gripper_states_into_msg( zeroArm , // Joint positions of arm
															zeroFng , // ------------ Joint positions of gripper
															temp ); // -------------- Load state data here
					record.push_back( frame );
				}
				
				if( VERBOSE ){  cout << "Frames generated! Output a spam frame ..." << endl;  }
				
				record.push_back( spam_frame() ); // Overwrite extra markers with a spam frame
				
				if( VERBOSE ){  cout << "Cubes were spammed!" << endl;  }
				
			}else{  cout << "SEQUENCE ERROR: Undefined action!" << endl;  }
		}else{  
			cout << "SEQUENCE END" << endl;
			currNode = nullptr;  
		}
		count++;
	}
	return record;
}

/// ___ END ANIMATION ______________________________________________________________________________________________________________________

/* === Spare Parts =========================================================================================================================



   ___ End Parts ___________________________________________________________________________________________________________________________

*/
