/*
component_test_1.cpp
James Watson , 2018 May
Test integration of the ASP_3D lib --> ROS , Show markers and move them around

Dependencies: Cpp_Helpers , ROS , Eigen , RAPID
Template Version: 2017-09-23
*/

// === Init ================================================================================================================================

// ~~~ Includes ~~~
// ~~ ROS ~~
// ~ Core ~
#include <ros/ros.h> // --- ROS presides over all
#include <ros/package.h> // Where are we? // http://wiki.ros.org/Packages#C.2B-.2B-
// ~~ Includes ~~
#include <Eigen/Core> // ---- The living heart of Eigen
#include <Eigen/Dense> // --- Cross Product , etc.
#include <Eigen/Geometry> //- Quaternion , etc
// ~ RViz ~
#include <visualization_msgs/Marker.h> // ---- Display geometry
#include <visualization_msgs/MarkerArray.h> // RViz marker array
// ~~ Local ~~ 
#include <Cpp_Helpers.h> // Shortcuts and Aliases , and Functions of general use for C++ programming
#include <ASP_3D.h> // ---- Assembly geometry
#include <AsmSeqPlan.h> //- Graph planning


// using namespace quickhull;

// ___ End Init ____________________________________________________________________________________________________________________________

// === Program Vars ===

string projectOuputDir = "/home/jwatson/output/";

// ___ End Vars ___



// === main ================================================================================================================================

int main( int argc , char** argv ){
	
	srand( time( 0 ) ); // Random seed based on the current clock time
	
	sep( "linspace" );
	
	for( size_t i = 0 ; i < 12 ; i++ ){
		cout << linspace( 0.0d , 10.0d , i ) << endl;
	}
	
	for( size_t i = 0 ; i < 12 ; i++ ){
		cout << linspace( 10.0d , -10.0d , i ) << endl;
	}
	
	cout << endl;
	
	sep( "lo_bound_for_fraction_geq" );
	
	std::vector<double> numCollection;
	for( size_t i = 5 ; i >= 1 ; i-- ){  numCollection.push_back( i );  }
	cout << "Collection: " << numCollection << endl << endl;
	
	double fraction = 0.0;

	while( fraction <= 1.0 ){
		
		cout << "Lo bound for fraction " << fraction << ": " 
			 << lo_bound_for_fraction_geq( numCollection , fraction ) << endl;
			 
		fraction += 0.1;
		
	}
	
	cout << endl;
	
	sep( "divider_for_largest_gap" );
	
	std::vector<double> numCollection2;
	
	numCollection2.push_back(  0 );
	numCollection2.push_back( 10 );
	numCollection2.push_back(  3 );
	numCollection2.push_back( 12 );
	numCollection2.push_back(  6 ); //  6 to 10 is largest gap
	//~ numCollection2.push_back( -5 ); // -5 to  0 is largest gap
	
	cout << "Collection: " << numCollection2 << endl << endl;
	
	cout << "Divider for largest gap: " << divider_for_largest_gap( numCollection2 ) << endl;
	
	sep( "Overflow Test" );
	
	size_t testVar = 0;
	
	testVar--;
	
	cout << "size_t , Subtracted 1 from 0: " << testVar << endl; // 18446744073709551615 , big!
	
	sep( "Undefined Cross Products" );
	
	Eigen::Vector3d A{  1 , 0 , 0 };
	Eigen::Vector3d B{ -1 , 0 , 0 };
	
	cout << "A x A , " << A << " x " << A << " = " << A.cross( A ) << endl;
	cout << "A x B , " << A << " x " << B << " = " << A.cross( B ) << endl;
	cout << "B x A , " << B << " x " << A << " = " << B.cross( A ) << endl;
	cout << "B x B , " << B << " x " << B << " = " << B.cross( B ) << endl;
	
	
	sep( "Eigen Assignment Test" );
	
	Eigen::MatrixXd matx(1,3);
	matx.row(0) << 1,2,3;
	cout << matx << endl;
	
	return 0; // I guess everything turned out alright at the end!
}

// ___ End main ____________________________________________________________________________________________________________________________


/* === Spare Parts =========================================================================================================================


	
   ___ End Spare ___________________________________________________________________________________________________________________________
*/

