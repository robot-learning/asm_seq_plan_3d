/*
DIG_shell.cpp
James Watson , 2018 June

% ROS Node %
Display meshes for expanding shells
Publises To ----> :
Subscribes To <-- :

Dependencies: ROS , Cpp_Helpers , ROS_Helpers
Template Version: 2018-06-06
*/

// === Init ================================================================================================================================

// ~~~ Includes ~~~
// ~~ ROS ~~
#include <ros/ros.h> // -------------- ROS , Publishers , Subscribers
#include <ros/package.h> // ------------------ Where are we? // http://wiki.ros.org/Packages#C.2B-.2B-
// ~ ROS Messages ~
#include "motion_cost/qFromPose.h"
#include "motion_cost/IKrequest.h"
#include "motion_cost/IKresponse.h"
#include "geometry_msgs/Pose.h"
#include "sensor_msgs/JointState.h"
#include <ll4ma_teleop/Joints2Jac.h>
#include <ll4ma_teleop/LinkPoses.h>
#include <ll4ma_teleop/FngrPoses.h>
// ~~ Local ~~
#include <Cpp_Helpers.h> // - C++ Utilities and Shortcuts
#include <ROS_Helpers.h> // - ROS Utilities and Shortcuts
#include <Model_Factory.h> // Assemblies to plan on
#include <AsmSeqPlan.h> // -- Identify grasp pairs , ASP
#include <comm_ASM.h> // ---- Messages and visualization
#include <Motion_Planning.h>

// ___ End Init ____________________________________________________________________________________________________________________________


/// ######### Node Vars #########

string /* -- */ NODE_NAME  = "DIG_Shell_Test";
int /* ----- */ RATE_HZ    = 30;
int /* ----- */ QUEUE_LEN  = 30;
Eigen::Vector3d ROBOT_BASE = Eigen::Vector3d( 0.55 , 0 , 1.20 ); // FUTURE: NEED A WAY TO LOAD THIS FROM A PARAM SERVER

/// ********* End Vars *********


// === Program Functions & Classes ===

struct ShellVizMap{
	std::vector<std::pair<llin,double>> mapping;
	std::vector<TriMeshVFN_ASP> /* - */ subShells;
};

ShellVizMap sub_vs_parts_blockage_viz( Assembly_ASM* assembly , // Create projection of one sub and collide it with another
									   const std::vector<llin>& sub , const std::vector<llin>& opponents , // sub moves "against" opponents
									   const std::vector<llin>& refcID , // Reference parts for determining freedom
									   double dRadiusInterval , size_t numSteps ){
	// Blocking calculator: Return a mapping (pair vector) from all parts not in the sub to the fraction that each blocks the sub
	ShellVizMap rtnStruct;
	std::vector<std::pair<llin,double>> rtnVec;
	// 0. Init
	size_t    numOppose = opponents.size();
	Part_ASM* currPart  = nullptr; // --- Pointer to interfering part
	double    currFrac  = 0.0 ,
			  mostFrac  = 0.0 ;
	// 1. Get the shells from the sub
	std::vector<TriMeshVFN_ASP> subShells = get_intersection_surfaces( assembly , 
																	   sub , refcID , 
																	   dRadiusInterval , numSteps );
	// A. Prep shells
	std::vector<CollisionVFN_ASP*> collsnShells = prep_meshes_for_collision( subShells );
	size_t numShells = subShells.size();
	// 2. For each opponent
	for( size_t i = 0 ; i < numOppose ; i++ ){
		// A. Get the part reference
		currPart = assembly->get_part_ptr_w_ID( opponents[i] );
		mostFrac = -BILLION_D;
		// 3. For each shell
		for( size_t j = 0 ; j < numShells ; j++ ){
			// 4. Get the blocking fraction , Store max
			currFrac = collision_fraction( collsnShells[j] , currPart );
			mostFrac = max( mostFrac , currFrac );
		}
		// 5. Push pair
		rtnVec.push_back(  std::make_pair( opponents[i] , mostFrac )  );
	}
	// N. Return
	rtnStruct.mapping   = rtnVec;
	rtnStruct.subShells = subShells;
	return rtnStruct;
}

void load_shells_into_mrkrMngr( const std::vector<TriMeshVFN_ASP>& shells , RViz_MarkerManager& mrkrMngr ){
	size_t numShells = shells.size();
	for( size_t i = 0 ; i < numShells ; i++ ){
		RViz_MeshMarker mrkr{ shells[i] };
		mrkr.rand_color();
		mrkrMngr.add_marker( mrkr );
	}
}

// ___ End Functions & Classes ___


// === Program Vars ===

bool CONNECT_TO_ARM = true  ;

// ___ End Vars ___


// === main ================================================================================================================================

int main( int argc , char** argv ){ // Main takes the terminal command and flags that called it as arguments
	srand( time( 0 ) ); // Random seed based on the current clock time
	
	// 0. Init ROS  &&  Register node
	ros::init( argc , argv , NODE_NAME );
	
	// 1. Fetch handle to this node
	ros::NodeHandle nodeHandle;
	
	// 2. Init node rate
	ros::Rate heartbeat( RATE_HZ );
	
	// 3. Set up subscribers and publishers
	
	// ~ Publishers ~
	//~ ros::Publisher joint_cmd_pub = nodeHandle.advertise<sensor_msgs::JointState>( "/lbr4_teleop/joint_cmd" , QUEUE_LEN );
	//~ send_joint_command jntCmd_Call{ joint_cmd_pub };
	
	ros::Publisher vis_arr       = nodeHandle.advertise<visualization_msgs::MarkerArray>( "visualization_marker_array" , QUEUE_LEN );
	
	// ~ Subscribers ~
	// ros::Subscriber SUBSCRIBER_OBJ = nodeHandle.subscribe( "TOPIC_NAME" , QUEUE_LEN , CALLBACK_FUNCTION );
	
	// ~ Service Servers ~
	// ros::ServiceServer SERVER_OBJ = nodeHandle.advertiseService( "SERVICE_NAME" , SERV_CALLBACK );
	
	// ~ Service Clients ~
	
	//~ ros::ServiceClient  IK_Client = nodeHandle.serviceClient<motion_cost::qFromPose>( "q_from_pose" );
	//~ request_IK_solution IK_clientCall{ IK_Client };
	
	//~ ros::ServiceClient  J_Client = nodeHandle.serviceClient<ll4ma_teleop::Joints2Jac>( "/robot_commander/get_Jacobian" );
	//~ request_Jacobian    J_ClientCall{ J_Client };
	
	//~ ros::ServiceClient  FK_Client = nodeHandle.serviceClient<ll4ma_teleop::LinkPoses>( "/robot_commander/get_links_FK" );
	//~ request_FK_poses    FK_ClientCall{ FK_Client };  // { 1 , 2 , 3 , 4 , 5 , 6 , 7 , Grasp Target }

	//~ ros::ServiceClient  FNG_Client = nodeHandle.serviceClient<ll4ma_teleop::FngrPoses>( "/robot_commander/get_fingr_FK" );
	//~ request_FNG_poses   FNG_ClientCall{ FNG_Client };
	
	
	// N-1. Animation Init 
	srand( ( time( 0 ) % 1 ) * 1000.0 + getpid() ); // Random seed based on the current clock time
	RViz_MarkerManager mrkrMngr{};
	visualization_msgs::MarkerArray& markerArr = mrkrMngr.get_arr();
	
	
	/// === Preliminary { Setup , Instantiation , Planning  } ==============================================================================

	
	cout << "Instantiate Simple Phone ... " << endl;
	
	Assembly_ASM*   simplePhone = simple_phone();
	cout << "Set pose for Simple Phone ... " << endl;
	simplePhone->set_pose( origin_pose() );
	Eigen::MatrixXd bb /* ----------- */ = simplePhone->calc_AABB();
	Eigen::Vector3d crnr1 /* -------- */ = bb.row(0);
	Eigen::Vector3d crnr2 /* -------- */ = bb.row(1);
	double /* -- */ width /* -------- */ = ( crnr2 - crnr1 ).norm() ,
					interval /* ----- */ = 0.0025 ,
					stuckInfluenceFactor = 1.5 ;
	size_t /* -- */ numSteps /* ----- */ = (size_t) ceil( width/interval );
	
	
	cout << "Instantiate Simple Phone 2 ... " << endl;
	Assembly_ASM*   simplePhone2 = simple_phone();
	cout << "Set pose for Simple Phone 2 ... " << endl;
	simplePhone2->set_pose( Pose_ASP{ Eigen::Vector3d( 0.0 , 0.25 , 0.0 ) , no_turn_quat() } );
	
	float markerOpacity = 0.5;
	cout << "About to set opacity ... " << endl;
	simplePhone->set_marker_opacity( markerOpacity );
	simplePhone2->set_marker_opacity( markerOpacity );
	
	Assembly_ASM* testAsm = drive_motor();
	
	/// === COMPONENTS TEST ================================================================================================================
	
	if( 0 ){
		std::vector<llin> sub1   = { 5 , 9 , 4 };
		std::vector<llin> sub2   = { 2 };
		std::vector<llin> allSub = { 5 , 9 , 4 , 2 };
		std::vector<llin> refcID = vec_copy_without_elem( simplePhone->get_part_ID_vec() , allSub );
		
		double maxBlok = max_blocking_fraction_sub1_vs_sub2( simplePhone , // Create projection of one sub and collide it with another
															 sub1 , sub2 , // sub1 moves "against" sub2
															 refcID , // Reference parts for determining freedom
															 0.0025 , 20 );

		cout << "Mod A + Chips blocked by Bridge AB: " << maxBlok << endl;
	}
	/// ___ END COMP _______________________________________________________________________________________________________________________

	
	/// === TROUBLESHOOTING ================================================================================================================
	
	if( 0 ){
		cout << "About to load markers ... " << endl;
		simplePhone->load_part_mrkrs_into_mngr( mrkrMngr );
		simplePhone2->load_part_mrkrs_into_mngr( mrkrMngr );
		
		//~ Current Sub ID:_ [ 10 ]
		//~ Addition Sub ID: [ 10, 7 ]

		double dRadiusInterval =  0.0025;
		size_t numSteps        = 20;

		std::vector<llin> allID = simplePhone->get_part_ID_vec();

		std::vector<llin> sub1 = { 10 };		std::vector<llin> opponents1 = vec_copy_without_elem( allID , sub1 );
		std::vector<llin> sub2 = { 10, 7 };		std::vector<llin> opponents2 = vec_copy_without_elem( allID , sub2 );
		std::vector<llin> refcID = { 0 };
		
		stuckInfluenceFactor = 1.5 ;
		
		std::vector<std::vector<double>> DIG3 = DIG_from_asm_V3( simplePhone , dRadiusInterval , numSteps ,
																 refcID ,
																 stuckInfluenceFactor );
		
		ShellVizMap sub1shells = sub_vs_parts_blockage_viz( simplePhone , // Create projection of one sub and collide it with another
															sub1 , opponents1 , // sub moves "against" opponents
															refcID , // Reference parts for determining freedom
															dRadiusInterval , numSteps );

		IndexSearchResult result = simplePhone->index_w_ID( 7 );
		double loneBlokd = donated_NRG_from_index_from_DIG( result.index , DIG3 ); // NRG <-- Part 
														
		cout << "Sub 1 Mapping: __ " <<  sub1shells.mapping << endl
			 << "Total: __________ " << sum_blockage_across_parts( sub1shells.mapping ) << endl
			 << "Add part blocked: " << loneBlokd << endl;
														
		load_shells_into_mrkrMngr( sub1shells.subShells , mrkrMngr );
															
		ShellVizMap sub2shells = sub_vs_parts_blockage_viz( simplePhone2 , // Create projection of one sub and collide it with another
															sub2 , opponents2 , // sub moves "against" opponents
															refcID , // Reference parts for determining freedom
															dRadiusInterval , numSteps );
															
		cout << "Sub 2 Mapping: " <<  sub2shells.mapping << endl
			 << "Total: _______ " << sum_blockage_across_parts( sub2shells.mapping ) << endl;
															
		load_shells_into_mrkrMngr( sub2shells.subShells , mrkrMngr );
		
	}
	
	
	/// ___ END TS _________________________________________________________________________________________________________________________
	
	
	/// === SUB-ID TEST ====================================================================================================================
	
	if( 0 ){
		
		StopWatch algoTimer;
		
		std::vector<std::vector<llin>> level1subs = sub_ID_blockage_reduction_one_level( simplePhone2 , 
																						 0.33 , 0.33 , 0.33 ,
																						 interval , numSteps ,
																						 stuckInfluenceFactor ,
																						 0.85 );
																			  
		cout << "Level 1 subs in " << algoTimer.seconds_elapsed() << " seconds." << endl << endl 
			 << "Subassemblies: " << endl;
																			  
		print_vec_vec( level1subs );
		
	}
	
	if( 1 ){
		
		StopWatch algoTimer;
		
		AsmStateNode_ASP* fullTree = sub_ID_blockage_reduction_full_depth( simplePhone2 , 
																		   0.33 , 0.33 , 0.33 ,
																		   interval , numSteps ,
																		   stuckInfluenceFactor ,
																		   0.85 );
																		   
		cout << "Full sequence completed in " << algoTimer.seconds_elapsed() << " seconds." << endl;
		
		visualize_tree_structure_plan( mrkrMngr , 
									   Eigen::Vector2d( 0 , 0 ) , Eigen::Vector2d( 0.125 , 0.0 ) , Eigen::Vector2d( 0.0 , 0.125 ) , 
									   fullTree );
		
	}
	
	if( 0 ){
		
		StopWatch algoTimer;
		
		AsmStateNode_ASP* fullTree = sub_ID_blockage_reduction_full_depth( testAsm , 
																		   0.30 , 0.40 , 0.30 ,
																		   interval , numSteps ,
																		   stuckInfluenceFactor ,
																		   0.85 );
																		   
		cout << "Full sequence completed in " << algoTimer.seconds_elapsed() << " seconds." << endl;
		
		visualize_tree_structure_plan( mrkrMngr , 
									   Eigen::Vector2d( 0 , 0 ) , Eigen::Vector2d( 0.125 , 0.0 ) , Eigen::Vector2d( 0.0 , 0.125 ) , 
									   fullTree );
		
	}
	
	/// ___ END SUB-ID _____________________________________________________________________________________________________________________
	
	
	
		
	/// ___ End Preliminary ________________________________________________________________________________________________________________
	
	// N-2. Notify
	ros_log( "[" + NODE_NAME + "] Init OK and about to run ..." , INFO );
	
	// N. Main loop
	while( ros::ok() ){ // While neither node nor ROS has been shut down
		
		/// == NODE WORK ===================================================================================================================
		
		
		
		// 5. Paint frame
		vis_arr.publish( markerArr );
		
		
		//~ break; // ONLY ONCE
		
		/// __ END WORK ____________________________________________________________________________________________________________________
		
		ros::spinOnce(); // - Process messages
		heartbeat.sleep(); // Sleep for remainder of period
	}
	
	// N+1. Notify  &&  Exit
	
	ros_log( "[" + NODE_NAME + "] Exit OK, Goodbye!" , INFO );
	
	return 0; // I guess everything turned out alright at the end!
}

// ___ End main ____________________________________________________________________________________________________________________________


/* === Spare Parts =========================================================================================================================



   ___ End Spare ___________________________________________________________________________________________________________________________
*/
