/*
graph_test.cpp
James Watson , 2018 February
Test integration of the ASP_3D lib --> ROS , Show markers and move them around

Dependencies: Cpp_Helpers , ROS , Eigen , RAPID
Template Version: 2017-09-23
*/

// === Init ================================================================================================================================

// ~~~ Includes ~~~
// ~~ ROS ~~
// ~ Core ~
#include <ros/ros.h> // --- ROS presides over all
#include <ros/package.h> // Where are we? // http://wiki.ros.org/Packages#C.2B-.2B-
// ~~ Includes ~~
#include <Eigen/Core> // ---- The living heart of Eigen
#include <Eigen/Dense> // --- Cross Product , etc.
#include <Eigen/Geometry> //- Quaternion , etc
// ~ RViz ~
#include <visualization_msgs/Marker.h> // ---- Display geometry
#include <visualization_msgs/MarkerArray.h> // RViz marker array
// ~~ Local ~~ 
#include <Cpp_Helpers.h> // Shortcuts and Aliases , and Functions of general use for C++ programming
#include <ASP_3D.h> // ---- Assembly geometry
#include <AsmSeqPlan.h> //- Graph planning


// using namespace quickhull;

// ___ End Init ____________________________________________________________________________________________________________________________

// === Program Vars ===

Incrementer assign_part_ID{ 0 }; // Instantiate a functor that will assign a unique number to each part
Incrementer assign_asm_ID{ 0 }; //- Instantiate a functor that will assign a unique number to each assembly

// ___ End Vars ___


// === main ================================================================================================================================

int main( int argc , char** argv ){
	
	srand( time( 0 ) ); // Random seed based on the current clock time
	
	// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
	
	sep( "Node Expansion Test (Single Part Removal)" );
	
	// 0. Create file paths
	string pkgPath = ros::package::getPath( "motion_cost" );
	string EL_fileName  = pkgPath + "/Resources/EL_SCLD.stl";
	string CEE_fileName = pkgPath + "/Resources/CEE_SCLD.stl";
	string CUBE_fileName = pkgPath + "/Resources/CUBE.stl";
	cout << "Path to EL : " << EL_fileName  << endl;
	cout << "Path to CEE: " << CEE_fileName << endl;
	
	// 1. Create part object(s)
	Part_ASM EL_part{   assign_part_ID() , EL_fileName   }; // 0 // Moved part
	Part_ASM CEE_part{  assign_part_ID() , CEE_fileName  }; // 1 // Reference part
	Part_ASM CUBE_part{ assign_part_ID() , CUBE_fileName }; // 2 // Not used
	
	std::vector<llin> movParts;  movParts.push_back( EL_part.get_ID()  ); // EL ---> Moved Part
	std::vector<llin> refParts;  refParts.push_back( CEE_part.get_ID() ); // CEE --> Reference part
		
	std::vector< Polygon_ASM > EL_faces  = EL_part.get_all_faces();
	cout << "EL has  " << EL_faces.size()  << " faces." << endl;
	std::vector< Polygon_ASM > CEE_faces = CEE_part.get_all_faces();
	cout << "CEE has " << CEE_faces.size() << " faces." << endl;	
		
	// 2. Create an assembly , add parts
	Assembly_ASM testAsm{ assign_asm_ID() };
	testAsm.add_part_w_pose( EL_part   , Pose_ASP{ Eigen::Vector3d(  0.2 * ( 0.25 - 0.075 ) , 0.0 , 0.2 * 0.125 ) , 
												   Eigen::Quaterniond(  0.000 ,  0.000 ,  0.707 , -0.707 ) } );
	testAsm.add_part_w_pose( CEE_part  , Pose_ASP{ Eigen::Vector3d(  0.0 ,  0.0 ,  0.0 ) , 
												   Eigen::Quaterniond(  1.000 ,  0.000 ,  0.000 ,  0.000 ) } );
	testAsm.set_pose( origin_pose() ); // Don't forget to set the asm pose!
	//~ testAsm.set_pose( Pose_ASP{ Eigen::Vector3d( 0 , 0 , 1 ) , Eigen::Quaterniond( 0.924 , 0.383 , 0.000 , 0.000 ) } );
	// 3. Determine the local freedom of each part (NDBG)
	testAsm.recalc_geo();
	
	// 3. Create a node
	AsmStateNode_ASP testNode( &testAsm );
	
	// 4. Expand the node
	testNode.expand_single();
	
	// 5. Report
	cout << "The expanded node has " << testNode.outgoingEdges.size() << " edges" << endl;
	size_t len = testNode.outgoingEdges.size();
	for( size_t i = 0 ; i < len ; i++ ){
		testNode.outgoingEdges[i]->assembly->assembly_report();
	}
	
	// 6. Plan disassembly
	
	
							   
	//~ cout << "Returned a plan of " << result.plan.size()        << " actions"                 << endl
	     //~ << "                   " << result.path.size()        << " states"                  << endl
	     //~ << "                   " << result.visited.size()     << " visited nodes"           << endl
	     //~ << "                   " << result.geoSequence.size() << " geometry specifications" << endl
	     //~ << "     Goal reached? " << result.goalMet                                          << endl << endl;
	     
	//~ sep( "State Report" );
	//~ size_t planLen = result.path.size();
	//~ size_t numPrts = 0;
	//~ for( size_t k = 0 ; k < planLen ; k++ ){
		//~ // 'visualize_search_plan' looks at 'geoSequence' , so troubleshoot that
		//~ cout << "State " << k << endl;
		//~ cout << "\t" << "Pose: ___ " << result.geoSequence[k]->get_pose() << endl;
		//~ cout << "\t" << "Vertices: " << endl;
		//~ cout << "\t" << result.geoSequence[k]->get_dense_vertices() << endl;
		//~ numPrts = result.geoSequence[k]->how_many_parts(); 
		//~ for( size_t m = 0 ; m < numPrts ; m++ ){
			//~ cout << "\t" << "Part " << m << endl;
			//~ cout << "\t\t"  << "Part Pose: ___ " << result.geoSequence[k]->get_part_ref_w_index( m ).get_mrkr_ptr()->get_pose()     << endl;
			//~ cout << "\t\t"  << "Part Vertices: " << result.geoSequence[k]->get_part_ref_w_index( m ).get_mrkr_ptr()->get_vertices() << endl;
		//~ }
	//~ }
	
	
	/// === VISUALIZATION ==================================================================================================================
	
	// == ROS Loop ==
	
	// = ROS Start =
	
	// 5. Start ROS
	
	// ~ Animation Vars ~
	srand( ( time( 0 ) % 1 ) * 1000.0 + getpid() ); // Random seed based on the current clock time
	int updateHz = 30;
	Eigen::Vector2d offsetStep{ 0.30 , 0.00 };
	Eigen::Vector2d asmPlnStep{ 0.00 , 0.40 };
	
	// Set up visualization management
	RViz_MarkerManager mrkrMngr{};
	visualization_msgs::MarkerArray& markerArr = mrkrMngr.get_arr();
	
	// ~ Visualize Results ~
	
	
	
	size_t numSuppt = testNode.assembly->get_stability_determination().size();
	
	for( size_t i = 0 ; i < numSuppt ; i++ ){
		
		// **** SET INITIAL SUPPORT ******
	
		testNode.set_support( i );
		
		// ******************************* 
	
		AsmSearchResult result = a_star_single( &testNode , 
								   simple_goal , validate_action_straight_simple ); 
		
		visualize_search_plan( result , mrkrMngr , asmPlnStep * i , offsetStep );
		
	}
	
	// ~ Check Visualization ~
	//~ std::vector<SuccessCode> vizChk = mrkrMngr.diagnose_markers_OK();
	
	// Connect to ROS
	ros::init( argc , argv , "graph_test_cpp" );
	// Set up a node with refresh rate
	ros::NodeHandle node_handle;
	ros::Publisher vis_arr = node_handle.advertise<visualization_msgs::MarkerArray> ( "visualization_marker_array" , 100 );
	ros::Rate r( updateHz ); // 30Hz refresh rate
	
	// 6. Set the animation loop and display
	while ( ros::ok() ){ // while roscore is running
        // cout << ".";

		// Wait for a subscriber to start looking for this topic
		while ( vis_arr.getNumSubscribers() < 1 ){
			if ( !ros::ok() ){ return 0; }
			printf( "." );
			ROS_WARN_ONCE( "Please create a subscriber to the marker" );
			sleep( 1 );
			cout << ".";
		} 
        
        vis_arr.publish( markerArr );

		r.sleep();
	}
	
	return 0; // I guess everything turned out alright at the end!
}

// ___ End main ____________________________________________________________________________________________________________________________


/* === Spare Parts =========================================================================================================================


	
   ___ End Spare ___________________________________________________________________________________________________________________________
*/

