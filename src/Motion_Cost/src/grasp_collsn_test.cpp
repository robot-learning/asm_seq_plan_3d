/*
level_exec.cpp
James Watson , 2018 June

% ROS Node %
Plan and execute one precedence level of a disassembly plan
Publises To ----> :
Subscribes To <-- :

Dependencies: ROS , Cpp_Helpers , ROS_Helpers
Template Version: 2018-06-06
*/

// === Init ================================================================================================================================

// ~~~ Includes ~~~
// ~~ ROS ~~
#include <ros/ros.h> // -------------- ROS , Publishers , Subscribers
#include <ros/package.h> // ------------------ Where are we? // http://wiki.ros.org/Packages#C.2B-.2B-
// ~ ROS Messages ~
#include "motion_cost/qFromPose.h"
#include "motion_cost/IKrequest.h"
#include "motion_cost/IKresponse.h"
#include "geometry_msgs/Pose.h"
#include "sensor_msgs/JointState.h"
#include <ll4ma_teleop/Joints2Jac.h>
#include <ll4ma_teleop/LinkPoses.h>
#include <ll4ma_teleop/FngrPoses.h>
// ~~ Local ~~
#include <Cpp_Helpers.h> // - C++ Utilities and Shortcuts
#include <ROS_Helpers.h> // - ROS Utilities and Shortcuts
#include <Model_Factory.h> // Assemblies to plan on
#include <AsmSeqPlan.h> // -- Identify grasp pairs , ASP
#include <comm_ASM.h> // ---- Messages and visualization
#include <Motion_Planning.h>

// ___ End Init ____________________________________________________________________________________________________________________________


/// ######### Node Vars #########

string /* -- */ NODE_NAME  = "Grasp_Model_Test";
int /* ----- */ RATE_HZ    = 30;
int /* ----- */ QUEUE_LEN  = 30;
Eigen::Vector3d ROBOT_BASE = Eigen::Vector3d( 0.55 , 0 , 1.20 ); // FUTURE: NEED A WAY TO LOAD THIS FROM A PARAM SERVER

/// ********* End Vars *********


// === Program Functions & Classes ===




// ___ End Functions & Classes ___


// === Program Vars ===

bool CONNECT_TO_ARM = true  ;

// ___ End Vars ___


// === main ================================================================================================================================

int main( int argc , char** argv ){ // Main takes the terminal command and flags that called it as arguments
	srand( time( 0 ) ); // Random seed based on the current clock time
	
	// 0. Init ROS  &&  Register node
	ros::init( argc , argv , NODE_NAME );
	
	// 1. Fetch handle to this node
	ros::NodeHandle nodeHandle;
	
	// 2. Init node rate
	ros::Rate heartbeat( RATE_HZ );
	
	// 3. Set up subscribers and publishers
	
	// ~ Publishers ~
	ros::Publisher joint_cmd_pub = nodeHandle.advertise<sensor_msgs::JointState>( "/lbr4_teleop/joint_cmd" , QUEUE_LEN );
	send_joint_command jntCmd_Call{ joint_cmd_pub };
	
	ros::Publisher vis_arr       = nodeHandle.advertise<visualization_msgs::MarkerArray>( "visualization_marker_array" , QUEUE_LEN );
	
	// ~ Subscribers ~
	// ros::Subscriber SUBSCRIBER_OBJ = nodeHandle.subscribe( "TOPIC_NAME" , QUEUE_LEN , CALLBACK_FUNCTION );
	
	// ~ Service Servers ~
	// ros::ServiceServer SERVER_OBJ = nodeHandle.advertiseService( "SERVICE_NAME" , SERV_CALLBACK );
	
	// ~ Service Clients ~
	
	ros::ServiceClient  IK_Client = nodeHandle.serviceClient<motion_cost::qFromPose>( "q_from_pose" );
	request_IK_solution IK_clientCall{ IK_Client };
	
	ros::ServiceClient  J_Client = nodeHandle.serviceClient<ll4ma_teleop::Joints2Jac>( "/robot_commander/get_Jacobian" );
	request_Jacobian    J_ClientCall{ J_Client };
	
	ros::ServiceClient  FK_Client = nodeHandle.serviceClient<ll4ma_teleop::LinkPoses>( "/robot_commander/get_links_FK" );
	request_FK_poses    FK_ClientCall{ FK_Client };  // { 1 , 2 , 3 , 4 , 5 , 6 , 7 , Grasp Target }

	ros::ServiceClient  FNG_Client = nodeHandle.serviceClient<ll4ma_teleop::FngrPoses>( "/robot_commander/get_fingr_FK" );
	request_FNG_poses   FNG_ClientCall{ FNG_Client };
	
	
	// N-1. Animation Init 
	srand( ( time( 0 ) % 1 ) * 1000.0 + getpid() ); // Random seed based on the current clock time
	RViz_MarkerManager mrkrMngr{};
	visualization_msgs::MarkerArray& markerArr = mrkrMngr.get_arr();
	
	
	/// === Preliminary { Setup , Instantiation , Planning  } ==============================================================================

	// 1. Instantiate collision model in zero-pose, gripper closed
	
	LBR4_collision_model<request_FK_poses,request_FNG_poses> LBR4collsn{ FK_ClientCall , FNG_ClientCall };
	LBR4collsn.init();
	
	LBR4collsn.LBR4arm.load_part_mrkrs_into_mngr( mrkrMngr );
	
	std::vector<double> jntPos = { 0.0 , 0.0 , 0.0 , 0.0 , 0.0 , 0.0 , 0.0 };
	std::vector<double> fngPos = { 0.000 , 0.000 };
	double /* ------ */ dTheta = 0.1047 / 4.0 /* ------ */ , // frame angle increment , 6deg
	/* ------------- */ theta  = 0.0000 /* ------ */ , // Angle for sinusoid
	/* ------------- */ maxOpn = MAX_BAXTER_GRIP_SEP , // Max spread of fingers
	/* ------------- */ spread = 0.0000 /* ------ */ ; // Spread of fingers
	
	jntCmd_Call( jntPos , fngPos , 25 ); // ---------------- Set the robot arm so that we can get poses
	LBR4collsn.set_poses_from_joints9( jntPos , fngPos ); // Set the collision model so that we can get a grasp model
	
	if( false ){
		LBR4collsn.LBR4arm.load_part_mrkrs_into_mngr( mrkrMngr );
	}
	
	// 2. Get pose of the target
	std::vector<Pose_ASP> linkPoses = FK_ClientCall( jntPos );  // { 1 , 2 , 3 , 4 , 5 , 6 , 7 , Grasp Target }
	Pose_ASP target = last_elem( linkPoses );
	
	// 3. Get relative poses of all parts in hand + wrist
	Pose_ASP baxPalmPose = LBR4collsn.get_pose( 7);
	Pose_ASP baxFngRPose = LBR4collsn.get_pose( 8);
	Pose_ASP baxFnRTPose = LBR4collsn.get_pose( 9);
	Pose_ASP baxFngLPose = LBR4collsn.get_pose(10);
	Pose_ASP baxFnLTPose = LBR4collsn.get_pose(11);
	
	Pose_ASP baxPalmPoseRel = baxPalmPose / target;
	Pose_ASP baxFngRPoseRel = baxFngRPose / target;
	Pose_ASP baxFnRTPoseRel = baxFnRTPose / target;
	Pose_ASP baxFngLPoseRel = baxFngLPose / target;
	Pose_ASP baxFnLTPoseRel = baxFnLTPose / target;
	
	cout << "Palm relative to target: __________ " << baxPalmPoseRel << endl;
	cout << "Right Finger relative to target: __ " << baxFngRPoseRel << endl;
	cout << "Right Fingertip relative to target: " << baxFnRTPoseRel << endl;
	cout << "Left Finger to target: ____________ " << baxFngLPoseRel << endl;
	cout << "Left Fingertip relative to target:_ " << baxFnLTPoseRel << endl;
	
	// 4. Construct this collection in the same place as the whole-arm collision model (source file)
	// OK!
	
	       theta       = 0.0;
	       dTheta      = 2 * M_PI / (double)RATE_HZ / 2.0;
	double oneSideFngr = 0.0;
	
	hand_collision_model hand;
	hand.init();
	std::vector<double> fingState = { 0.0 , 0.0 };
	Pose_ASP wayOutPose{  Eigen::Vector3d( 2.0 , 2.0 , 0.0 )  ,  no_turn_quat()  };
	hand.set_pose( wayOutPose );
	
	// 1. Load part models
	if( true ){
		hand.LBR4hand.load_part_mrkrs_into_mngr( mrkrMngr );
	}
	
	
	/// ___ End Preliminary ________________________________________________________________________________________________________________
	
	// N-2. Notify
	ros_log( "[" + NODE_NAME + "] Init OK and about to run ..." , INFO );
	
	// N. Main loop
	while( ros::ok() ){ // While neither node nor ROS has been shut down
		
		/// == NODE WORK ===================================================================================================================
		
		if( 0 ){
			theta += dTheta;
			oneSideFngr = abs( MAX_BAXTER_GRIP_SEP / 2.0 * sin( theta ) );
		}else{
			oneSideFngr = 0;
		}
		vec_assign_all_same( fingState , oneSideFngr );
		
		// 2. set fing state
		hand.set_finger_state( fingState );
		// 3. Set pose
		hand.set_pose( wayOutPose );
		// 4. animate
		
		if( CONNECT_TO_ARM ){	
			// 4. Update markers
			mrkrMngr.rebuild_mrkr_array();
			
		}
		
		// 5. Paint frame
		vis_arr.publish( markerArr );
		
		
		//~ break; // ONLY ONCE
		
		/// __ END WORK ____________________________________________________________________________________________________________________
		
		ros::spinOnce(); // - Process messages
		heartbeat.sleep(); // Sleep for remainder of period
	}
	
	// N+1. Notify  &&  Exit
	
	ros_log( "[" + NODE_NAME + "] Exit OK, Goodbye!" , INFO );
	
	return 0; // I guess everything turned out alright at the end!
}

// ___ End main ____________________________________________________________________________________________________________________________


/* === Spare Parts =========================================================================================================================



   ___ End Spare ___________________________________________________________________________________________________________________________
*/
