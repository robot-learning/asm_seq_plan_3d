/*
grasp_plan_test.cpp
James Watson , 2018 July

% ROS Node %
Test assembly problems for correctness
Publises To ----> :
Subscribes To <-- :

Dependencies: ROS , Cpp_Helpers , ROS_Helpers
Template Version: 2018-06-06
*/

// === Init ================================================================================================================================

// ~~~ Includes ~~~
// ~~ ROS ~~
#include <ros/ros.h> // -------------- ROS , Publishers , Subscribers
#include <ros/package.h> // ------------------ Where are we? // http://wiki.ros.org/Packages#C.2B-.2B-
// ~ ROS Messages ~
#include "motion_cost/qFromPose.h"
#include "motion_cost/IKrequest.h"
#include "motion_cost/IKresponse.h"
#include "geometry_msgs/Pose.h"
#include "sensor_msgs/JointState.h"
#include <ll4ma_teleop/Joints2Jac.h>
#include <ll4ma_teleop/LinkPoses.h>
#include <ll4ma_teleop/FngrPoses.h>
// ~~ Local ~~
#include <Cpp_Helpers.h> // - C++ Utilities and Shortcuts
#include <ROS_Helpers.h> // - ROS Utilities and Shortcuts
#include <ASP_3D.h>
#include <Model_Factory.h> // Assemblies to plan on
#include <AsmSeqPlan.h> // -- Identify grasp pairs , ASP
#include <comm_ASM.h> // ---- Messages and visualization
#include <Motion_Planning.h>
#include <Methods.h>

// ___ End Init ____________________________________________________________________________________________________________________________


/// ######### Node Vars #########

string /* -- */ NODE_NAME  = "Grasp_Investigation";
int /* ----- */ RATE_HZ    = 30;
int /* ----- */ QUEUE_LEN  = 30;
Eigen::Vector3d ROBOT_BASE = Eigen::Vector3d( 0.55 , 0 , 1.20 ); // FUTURE: NEED A WAY TO LOAD THIS FROM A PARAM SERVER

/// ********* End Vars *********


// === Program Functions & Classes ===

void load_grasp_markers_into_mngr( RayHits& graspPnts , RViz_MarkerManager& mngr ){

	Eigen::Vector3d bgn      , 
					end      , 
					entr     ,  
					exit     , 
					approach ;
	double segLen   = 0.010;
	float  lineThic = 0.00125;
	
	for( size_t i = 0 ; i < graspPnts.enter.rows() ; i++ ){
		
		// Add the entry marker
		entr = graspPnts.enter.row(i);
		exit = graspPnts.exit.row(i);
		approach = ( exit - entr ).normalized();
		
		bgn  = entr - approach * segLen;
		end  = entr;
		mngr.add_marker(  
			get_straightPath_marker( bgn , end , 
									 lineThic , 0.0 , 1.0 , 0.0 , 1.0 )
		);
		
		// Add the exit marker
		
		bgn  = exit + approach * segLen;
		end  = exit;
		mngr.add_marker(  
			get_straightPath_marker( bgn , end , 
									 lineThic , 0.0 , 1.0 , 0.0 , 1.0 )
		);
	}
}

// ___ End Functions & Classes ___


// === Program Vars ===

bool CONNECT_TO_ARM = true;
//~ PlanRecording recording;

// ___ End Vars ___


// === main ================================================================================================================================

int main( int argc , char** argv ){ // Main takes the terminal command and flags that called it as arguments
	srand( time( 0 ) ); // Random seed based on the current clock time
	
	// 0. Init ROS  &&  Register node
	ros::init( argc , argv , NODE_NAME );
	
	// 1. Fetch handle to this node
	ros::NodeHandle nodeHandle;
	
	// 2. Init node rate
	ros::Rate heartbeat( RATE_HZ );
	
	// 3. Set up subscribers and publishers
	
	// ~ Publishers ~
	//~ ros::Publisher joint_cmd_pub = nodeHandle.advertise<sensor_msgs::JointState>( "/lbr4_teleop/joint_cmd" , QUEUE_LEN );
	//~ send_joint_command jntCmd_Call{ joint_cmd_pub };
	
	ros::Publisher vis_arr       = nodeHandle.advertise<visualization_msgs::MarkerArray>( "visualization_marker_array" , QUEUE_LEN );
	
	// ~ Subscribers ~
	// ros::Subscriber SUBSCRIBER_OBJ = nodeHandle.subscribe( "TOPIC_NAME" , QUEUE_LEN , CALLBACK_FUNCTION );
	
	// ~ Service Servers ~
	// ros::ServiceServer SERVER_OBJ = nodeHandle.advertiseService( "SERVICE_NAME" , SERV_CALLBACK );
	
	// ~ Service Clients ~
	
	//~ ros::ServiceClient  IK_Client = nodeHandle.serviceClient<motion_cost::qFromPose>( "q_from_pose" );
	//~ request_IK_solution IK_clientCall{ IK_Client };
	
	//~ ros::ServiceClient  J_Client = nodeHandle.serviceClient<ll4ma_teleop::Joints2Jac>( "/robot_commander/get_Jacobian" );
	//~ request_Jacobian    J_ClientCall{ J_Client };
	
	//~ ros::ServiceClient  FK_Client = nodeHandle.serviceClient<ll4ma_teleop::LinkPoses>( "/robot_commander/get_links_FK" );
	//~ request_FK_poses    FK_ClientCall{ FK_Client };  // { 1 , 2 , 3 , 4 , 5 , 6 , 7 , Grasp Target }

	//~ ros::ServiceClient  FNG_Client = nodeHandle.serviceClient<ll4ma_teleop::FngrPoses>( "/robot_commander/get_fingr_FK" );
	//~ request_FNG_poses   FNG_ClientCall{ FNG_Client };
	
	
	// N-1. Animation Init 
	srand( ( time( 0 ) % 1 ) * 1000.0 + getpid() ); // Random seed based on the current clock time
	RViz_MarkerManager mrkrMngr{};
	visualization_msgs::MarkerArray& markerArr = mrkrMngr.get_arr();

	// ~ Rviz Axes ~
	if(1){
		mrkrMngr.add_marker( get_axes_marker( 0.5 , 0.0025 ) );
	}
	
	/// === Preliminary { Setup , Instantiation , Planning  } ==============================================================================

	// ~ Set up scenario ~

	ValidationParams params = validation_defaults();

	Assembly_ASM*     testAsm = simple_phone();
	testAsm->set_lab_pose_for_index_3D( 0 , params.setdownLoc );
	
	Pose_ASP /* -- */ initPose = testAsm->get_pose();
	std::vector<llin> movd     = { 11, 8, 3 };
	Assembly_ASM*     movdAsm  = testAsm->sub_from_spec( movd );
	std::vector<llin> refc     = { 1, 0, 9, 4, 5, 10, 6, 7,  2 };
	Assembly_ASM*     refcAsm  = testAsm->sub_from_spec( refc );
		
	movdAsm->set_pose( initPose );
	refcAsm->set_pose( initPose );
	
	movdAsm->set_marker_opacity( 0.65 );
	movdAsm->load_part_mrkrs_into_mngr( mrkrMngr );
	
	refcAsm->set_marker_opacity( 0.45 );
	refcAsm->load_part_mrkrs_into_mngr( mrkrMngr );
		
		
	// ~ Get grasp pairs ~
		
	RayHits graspHits = tile_ASM_grasps_orthogonal( movdAsm , 
											        Eigen::Vector3d::UnitX() , Eigen::Vector3d::UnitY() , Eigen::Vector3d::UnitZ() , 
												    params.distance , params.graspTileSize );
												    
	load_grasp_markers_into_mngr( graspHits , mrkrMngr );
	
	
	// ~ Get grasps ~
	
	Eigen::Vector3d preferredCenter = params.setdownLoc + Eigen::Vector3d( 0 , 1 , 0 ) * params.distance;
	
	sep( "Validation Parameters" , 3 );
	cout << "Distance: ________________ " << params.distance << endl
		 << "Tile Size: _______________ " << params.graspTileSize << endl
		 << "Preferred Wrist Direction: " << params.preferredWristDir << endl
		 << "Preferred Center: ________ " << preferredCenter << endl
		 << "dTheta: __________________ " << params.graspDTheta << endl;
	
	std::vector<ParallelGraspState> rankedPairs = 
				//~ generate_and_rank_grasp_pairs( testAsm , 
				generate_and_rank_grasp_pairs( movdAsm , 
											   Eigen::Vector3d::UnitX() , Eigen::Vector3d::UnitY() , Eigen::Vector3d::UnitZ() , 
											   params.distance , params.graspTileSize ,
											   params.preferredWristDir , 
											   //~ params.preferredCenter , 
											   //~ end_straightline_part_pose( result.bestDir ).position , 
											   preferredCenter , 
											   params.graspDTheta );
											   
	cout << "Generated " << rankedPairs.size() << " grasps" << endl;
			
	/// ___ End Preliminary ________________________________________________________________________________________________________________
	
	// N-2. Notify
	ros_log( "[" + NODE_NAME + "] Init OK and about to run ..." , INFO );
	
	cerr << "Before loop!" << endl;
	size_t loopCounter =  0 ,
		   LOOPLIMIT   = 10 ;
	
	// N. Main loop
	while( ros::ok() ){ // While neither node nor ROS has been shut down
		
		/// == NODE WORK ===================================================================================================================

		//~ cerr << "Loop ";

		// Request frame from renderer

		// A. Fetch frame
		//~ currFrame = player.current_frame( LOOP );
		// B. Render object markers
		//~ visualization_msgs::MarkerArray& markerArr = currFrame->mrkrMngr.get_arr();
		
		
		// COMING SOON PHASE 3
		//~ // C. Render the joint state
		//~ jntCmd_Call( currFrame->get_q() );

		// 5. Paint frame
		vis_arr.publish( markerArr );
		
		if( loopCounter >= LOOPLIMIT ){  break;  }  loopCounter++;
		//~ break; // ONLY ONCE
		
		/// __ END WORK ____________________________________________________________________________________________________________________
		
		ros::spinOnce(); // - Process messages
		heartbeat.sleep(); // Sleep for remainder of period
	}
	
	cerr << "Outside loop!" << endl;
	
	// N+1. Notify  &&  Exit
	
	ros_log( "[" + NODE_NAME + "] Exit OK, Goodbye!" , INFO );
	
	return 0; // I guess everything turned out alright at the end!
}

// ___ End main ____________________________________________________________________________________________________________________________


/* === Spare Parts =========================================================================================================================



   ___ End Spare ___________________________________________________________________________________________________________________________
*/
