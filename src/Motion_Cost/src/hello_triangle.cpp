/*
This is a summary of the "Hello Triangle" tutorial that can be found at: http://antongerdelan.net/opengl/hellotriangle.html

GLFW is a helper library that will start the OpenGL "context" for us so that it talks to (almost) any operating system in the same way.
The context is a running copy of OpenGL, tied to a window on the operating system.

You will likely have to DL the GLFW source, build, and install.
*/

// There's a library called GLEW that makes sure that we include the latest version of GL, and not a default [ancient] version on the system path.
#include <GL/glew.h> // Include GLEW before GLFW to use the latest GL libraries
#include <GLFW/glfw3.h> // GLFW helper library
#include <stdio.h>
#include <unistd.h>

int main() {
	// start GL context and O/S window using the GLFW helper library , minimal shell that will start the GL context
	if ( !glfwInit() ) {
		fprintf( stderr , "ERROR: could not start GLFW3\n" );
		return 1;
	} 
	
	// uncomment these lines if on Apple OS X // get the newest available version of OpenGL on Apple
	/*glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 3);
	glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 2);
	glfwWindowHint(GLFW_OPENGL_FORWARD_COMPAT, GL_TRUE);
	glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);*/
	
	GLFWwindow* window = glfwCreateWindow( 640 , 480 , "Hello Triangle" , NULL , NULL );
	if ( !window ) {
		fprintf(stderr, "ERROR: could not open window with GLFW3\n"); // Print an error if the drivers / emvironment cannot support OpenGL
		glfwTerminate();
		return 1;
	}
	
	glfwMakeContextCurrent( window );
	
	// To improve support for newer OpenGL releases, we can put the flag glewExperimental = GL_TRUE; before starting GLEW. 
	// start GLEW extension handler
	glewExperimental = GL_TRUE;
	glewInit();
	
	// get version info
	const GLubyte* renderer = glGetString( GL_RENDERER ); // get renderer string
	const GLubyte* version = glGetString( GL_VERSION ); // version as a string
	printf("Renderer: %s\n", renderer);
	printf("OpenGL version supported %s\n", version);
	
	// tell GL to only draw onto a pixel if the shape is closer to the viewer
	glEnable( GL_DEPTH_TEST ); // enable depth-testing
	glDepthFunc( GL_LESS ); // depth-testing interprets a smaller value as "closer"
	
	
	// Okay, let's define a triangle from 3 points.
	// Here I want to define a triangle, with the points given in clock-wise order
	float points[] = {
	//   x    ,  y    ,  x
		 0.0f ,  0.5f ,  0.0f , // All the z-coords are 0, flat shape
		 0.5f , -0.5f ,  0.0f ,
		-0.5f , -0.5f ,  0.0f
	};
	
	GLuint vbo = 0;
	// We will copy this chunk of memory onto the graphics card in a unit called a vertex buffer object (VBO). 
	glGenBuffers(1, &vbo); // "generate" an empty buffer , get the unsigned int handle for it
	glBindBuffer(GL_ARRAY_BUFFER, vbo); // set it as the current buffer in OpenGL's state machine by "binding"
	glBufferData(GL_ARRAY_BUFFER, 9 * sizeof(float), points, GL_STATIC_DRAW); // then copy the points into the currently bound buffer: 
	
	// Most meshes will use a collection of one or more vertex buffer objects to hold vertex points, texture-coordinates, vertex normals, etc.
	
	GLuint vao = 0;
	/* we have new thing called the vertex array object (VAO), which remembers all of the vertex buffers that you want to use, 
	and the memory layout of each one. */
	glGenVertexArrays(1, &vao); // create the vertex array object and get the uint handle for it
	glBindVertexArray(vao); // We set up the vertex array object once per mesh.
	glEnableVertexAttribArray(0);
	glBindBuffer(GL_ARRAY_BUFFER, vbo); // When we want to draw, all we do then is bind the VAO and draw. 
	/*  We bind it, to bring it in to focus in the state machine. This lets us enable the first attribute; 0. 
	We are only using a single vertex buffer, so we know that it will be attribute location 0. 
	
	The glVertexAttribPointer function defines the layout of our first vertex buffer;*/
	glVertexAttribPointer( 0 , // "0" means define the layout for attribute number 0. 
		                   3 , // "3" means that the variables are vec3 made from every 3 numbers in the buffer. 
		                   GL_FLOAT , // floats (GL_FLOAT) type of number in the buffer 
		                   GL_FALSE , 
		                   0 , 
		                   NULL );
	
	/* We need to use a shader program, written in OpenGL Shader Language (GLSL), 
	to define **how** to draw our shape from the vertex array object. */
	
	/* This shader programme is made from the minimum 2 parts. Both are be written in plain text, and look a lot like C programs. 
	Loading these from plain-text files would be nicer */
	
	// ~~ Vertex Shader ~~ 
	
	// Define the vertex shader
	const char* vertex_shader = // vertex shader, which describes where the 3d points should end up on the display
	"#version 400\n" // The first line says which version of the shading language to use;
	"in vec3 vp;" /* vertex shader has 1 input variable; a vec3 (vector made from 3 floats), which matches up to our VAO's attribute pointer
	                 This means that each vertex shader gets 3 of the 9 floats from our buffer - 
	                 therefore 3 vertex shaders will run concurrently; each one positioning 1 of the vertices. */
	"void main() {"
	"  gl_Position = vec4(vp, 1.0);" // The output has a reserved name gl_Position and expects a 4d float
	"}"; // haven't modified this at all, just added a 1 to the 4th component. The 1 at the end just means "don't calculate any perspective"
	
	// Compile the vertex shader
	GLuint vs = glCreateShader(GL_VERTEX_SHADER);
	glShaderSource(vs, 1, &vertex_shader, NULL);
	glCompileShader(vs);
	
	// ~~ Fragment Shader ~~
	
	const char* fragment_shader = // fragment shader which colours the surfaces, pixel by pixel. You can guess that for a triangle,  
	"#version 400\n" // version   // we will have lots more fragment shaders running than vertex shaders for this shape.
	"out vec4 frag_colour;"
	"void main() {"
	"  frag_colour = vec4( 0.5 , 0.0 , 0.5 , 1.0 );" // has 1 output - a 4d vector representing a colour made from red, blue, green, and alpha  
	"}";                // R   , G   , B   , \alpha  // - each component has a value between 0 and 1. We aren't using the alpha component.
	
	// Compile the fragment shader
	GLuint fs = glCreateShader(GL_FRAGMENT_SHADER);
	glShaderSource(fs, 1, &fragment_shader, NULL);
	glCompileShader(fs);
	
	// ~~ Complete Shader Program ~~
	// Now, these compiled shaders must be combined into a single, executable GPU shader program
	
	GLuint shader_programme = glCreateProgram(); // create an empty "program"
	glAttachShader(shader_programme, fs); // attach the shaders
	glAttachShader(shader_programme, vs);
	glLinkProgram(shader_programme); // then link them together. 
	
	/* We draw in a loop. Each iteration draws the screen once; a "frame" of rendering. The loop exits if the window is closed. */
	while(!glfwWindowShouldClose(window)) { // While there is no signal for the window to close
	  glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT); // First we clear the drawing surface
	  glUseProgram(shader_programme); // then set the shader program that should be "in use" for all further drawing
	  glBindVertexArray(vao); // We set our VAO (not the VBO) as the input variables that should be used for all further drawing 
	  // draw points 0-3 from the currently bound VAO with current in-use shader
	  glDrawArrays(GL_TRIANGLES, // Then we can draw, and we want to draw in triangles mode (1 triangle for every 3 points) 
	  	           0, 3);  // and draw from point number 0, for 3 points.
	  // update other events like input handling 
	  glfwPollEvents(); // GLFW3 requires that we manually call glfwPollEvents() to update non-graphical events like key-presses.
	  // put the stuff we've been drawing onto the display
	  glfwSwapBuffers(window); // Finally, we flip the swap surface onto the screen, 
	} // Done!                 // and the screen onto the next drawing surface (we have 2 drawing buffers).
	
	// close GL context and any other GLFW resources
	glfwTerminate();
	return 0;
}

