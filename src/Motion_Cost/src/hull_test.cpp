/*
hull_test.cpp
James Watson , 2017 October
Test integration of the ASP_3D lib --> ROS , Show markers and move them around

Dependencies:
Template Version: 2017-09-23
*/

// === Init ================================================================================================================================

// ~~~ Includes ~~~
// ~~ Standard ( Common includes not already in Cpp_Helpers.h ) ~~ 
//#include <iomanip> // - Output precision and printing
//#include <exception> // error handling
//#include <stdexcept> // library provided error types like 'std::range_error'
//#include <tuple> // --- standard tuple datatype
//#include <cstddef> // - alias types names:  size_t, ptrdiff_t
//#include <cstring> // - C Strings: strcomp 
//#include <algorithm> // sort, search, min, max, sequence operations

// ~~ ROS ~~
// ~ Core ~
#include <ros/ros.h> // ---------------------------- ROS presides over all
#include <ros/package.h> // Where are we? // http://wiki.ros.org/Packages#C.2B-.2B-
// ~ RViz ~
#include <visualization_msgs/Marker.h> // ---------- Display geometry
#include <visualization_msgs/MarkerArray.h> // ----- RViz marker array
// ~~ Local ~~ 
#include <Cpp_Helpers.h> // Shortcuts and Aliases , and Functions of general use for C++ programming
#include <ASP_3D.h> // ---- Assembly planning
#include <QuickHull.hpp> // Convex hull

// using namespace quickhull;

// ___ End Init ____________________________________________________________________________________________________________________________


// === Program Functions & Classes ===



// ___ End Functions & Classes ___


// === Program Vars ===

quickhull::QuickHull<double> qh; // hull object to be returned
std::vector<quickhull::Vector3<double>> pointCloud; // Collection of points to form a hull around

// ___ End Vars ___


// === main ================================================================================================================================

int main( int argc , char** argv ){
	
	srand( time( 0 ) ); // Random seed based on the current clock time
	
	sep( "Vanilla quickhull Test" );
	
	for( int I = 0 ; I < 8 ; I++ ){ // Add these a bunch more times and see how quickhull responds
		pointCloud.push_back( quickhull::Vector3<double>( 0.0 , 0.0 , 0.0 ) ); // RESULT: Quickull reduces vertices to only those needed
		pointCloud.push_back( quickhull::Vector3<double>( 1.0 , 0.0 , 0.0 ) );
		pointCloud.push_back( quickhull::Vector3<double>( 0.0 , 1.0 , 0.0 ) );
		pointCloud.push_back( quickhull::Vector3<double>( 1.0 , 1.0 , 0.0 ) );
		pointCloud.push_back( quickhull::Vector3<double>( 0.0 , 0.0 , 1.0 ) );
		pointCloud.push_back( quickhull::Vector3<double>( 1.0 , 0.0 , 1.0 ) );
		pointCloud.push_back( quickhull::Vector3<double>( 0.0 , 1.0 , 1.0 ) );
		pointCloud.push_back( quickhull::Vector3<double>( 1.0 , 1.0 , 1.0 ) );
		pointCloud.push_back( quickhull::Vector3<double>( 0.5 , 0.5 , 0.5 ) ); // This should be absorbed by the hull // IT IS
	}
	
	// Comute the convex hull
	quickhull::ConvexHull<double> hull               = qh.getConvexHull( pointCloud , true , false );
	// Get the tri-mesh for the convex hull
	std::vector<size_t> indexBuffer                  = hull.getIndexBuffer(); // What is the structure of this buffer?
	cout << "Index buffer:" << endl << indexBuffer << endl << "size: " << indexBuffer.size() << endl;
	quickhull::VertexDataSource<double> vertexBuffer = hull.getVertexBuffer();
	cout << "VertexDataSource size: " << vertexBuffer.size() << endl;
	
	// Result is a mesh with 36 triangular facets , 2 for each of the 6 rectangular sides of the cube 
	
	cout << "All the vertices:" << endl;
	
	size_t i        = 0                  , 
		   numVerts = indexBuffer.size() ;
	
	for( i = 0 ; i < numVerts ; i++ ){
		print_vec3d_inline( Eig_vec3d_round_zero( hullV3d_to_eigV3d( vertexBuffer[ indexBuffer[i] ] ) ) ); cout << endl;
		if( ( i + 1 ) % 3 == 0 ) cout << endl;
	}
	
	// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
	
	sep( "quickhull Wrapper Test" );
	
	Eigen::MatrixXd inputV = Eigen::MatrixXd::Zero( 9 , 3 );
	
	inputV.row(0) = Eigen::Vector3d( 0.0 , 0.0 , 0.0 ); 
	inputV.row(1) = Eigen::Vector3d( 1.0 , 0.0 , 0.0 );
	inputV.row(2) = Eigen::Vector3d( 0.0 , 1.0 , 0.0 );
	inputV.row(3) = Eigen::Vector3d( 1.0 , 1.0 , 0.0 );
	inputV.row(4) = Eigen::Vector3d( 0.0 , 0.0 , 1.0 );
	inputV.row(5) = Eigen::Vector3d( 1.0 , 0.0 , 1.0 );
	inputV.row(6) = Eigen::Vector3d( 0.0 , 1.0 , 1.0 );
	inputV.row(7) = Eigen::Vector3d( 1.0 , 1.0 , 1.0 );
	inputV.row(8) = Eigen::Vector3d( 0.5 , 0.5 , 0.5 );
	
	TriMeshVFN_ASP hullMesh = V_to_ConvexHull_VFN( inputV );
	
	cout << "V"        << endl
		 << hullMesh.V << endl 
		 << "F"        << endl
		 << hullMesh.F << endl 
		 << "N"        << endl
		 << hullMesh.N << endl;
	
	// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
	
	sep( "Part Stability Test" );
	
	Incrementer assign_part_ID{ 0 }; // Instantiate a functor that will assign a unique number to each part
	
	// 0. Create file paths
	string pkgPath = ros::package::getPath( "motion_cost" );
	string EL_fileName  = pkgPath + "/Resources/EL_SCLD.stl";
	string CEE_fileName = pkgPath + "/Resources/CEE_SCLD.stl";
	cout << "Path to EL : " << EL_fileName  << endl;
	cout << "Path to CEE: " << CEE_fileName << endl;
	
	// 1. Create part object(s)
	Part_ASM EL_part{  assign_part_ID() , EL_fileName  };
	Part_ASM CEE_part{ assign_part_ID() , CEE_fileName };
	
	// 2. set the poses for the part(s)
	Eigen::Vector3d posEL;		
	if( true ){  posEL   <<  0.2d   ,  0.2d   ,  0.2d   ;  } 
	else{        posEL   <<  0.6d   ,  0.6d   ,  0.6d   ;  }
	Eigen::Quaterniond quatEL{              0.0d   ,  0.0d   ,  0.707d , -0.707d };
	EL_part.set_pose( posEL , quatEL );
	Eigen::Vector3d posCEE;		posCEE  <<  0.0d   ,  0.0d   ,  0.0d   ;
	Eigen::Quaterniond quatCEE{             1.0d   ,  0.0d   ,  0.0d   ,  0.0d   };
	CEE_part.set_pose( posCEE , quatCEE );
	
	// 3. Calc supports
	
	// ~~ EL ~~
	sep( "EL Supports" , 2 , '*' );
	EL_part.determine_support_stability();
	std::vector< Polygon_ASM > EL_support = EL_part.get_all_supports();
	cout << "Found " << EL_support.size() << " supporting surfaces." << endl;
	
	
	// ~~ CEE ~~
	sep( "CEE Supports" , 2 , '*' );
	CEE_part.determine_support_stability();
	std::vector< Polygon_ASM > CEE_support = CEE_part.get_all_supports();
	cout << "Found " << CEE_support.size() << " supporting surfaces." << endl;
	
	std::vector<std::vector<int>> CEE_adj_ordered = facet_adjacency_list_ordered( CEE_part.get_reduc_facets() );
	
	cout << "CEE Adjacency" << endl
		 << CEE_adj_ordered << endl;
	
	int foo[3][2] = { { 1 , 2 } , { 3 , 4 } , { 5 , 6 } }; // Nested array assignment test
	
	return 0; // I guess everything turned out alright at the end!
}

/// ___ END VISUAL _________________________________________________________________________________________________________________________

// ___ End main ____________________________________________________________________________________________________________________________


/* === Spare Parts =========================================================================================================================



   ___ End Spare ___________________________________________________________________________________________________________________________
*/
