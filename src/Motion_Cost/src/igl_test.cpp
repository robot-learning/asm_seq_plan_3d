#include <Cpp_Helpers.h> // -------- Functions of general use
#include <igl/viewer/Viewer.h> // -- GUI for looking at meshes
#include <igl/readOFF.h> // -------- load OFF ( BUNNY )
#include "tutorial_shared_path.h" // Path to all of the example models

// URL , Load STL : https://github.com/libigl/libigl/issues/273
#include <igl/read_triangle_mesh.h>
#include <igl/remove_duplicate_vertices.h>
#include <igl/writeOBJ.h>
#include <Eigen/Core>

bool BUNNY = false; // Do you want to see the bunny?

bool STLTEST = true; // Do you want to to load the STL?

Eigen::MatrixXd V;
Eigen::MatrixXi F;


int main( int argc , char *argv[] ){
	
	if( false ){ // cube
		// Inline mesh of a cube
		const Eigen::MatrixXd V= (Eigen::MatrixXd(8,3)<<
		0.0 , 0.0 , 0.0 ,
		0.0 , 0.0 , 1.0 ,
		0.0 , 1.0 , 0.0 ,
		0.0 , 1.0 , 1.0 ,
		1.0 , 0.0 , 0.0 ,
		1.0 , 0.0 , 1.0 ,
		1.0 , 1.0 , 0.0 ,
		1.0 , 1.0 , 1.0 ).finished();
		const Eigen::MatrixXi F = (Eigen::MatrixXi(12,3)<<
		1 , 7 , 5 ,
		1 , 3 , 7 ,
		1 , 4 , 3 ,
		1 , 2 , 4 ,
		3 , 8 , 7 ,
		3 , 4 , 8 ,
		5 , 7 , 8 ,
		5 , 8 , 6 ,
		1 , 5 , 6 ,
		1 , 6 , 2 ,
		2 , 6 , 8 ,
		2 , 8 , 4 ).finished().array()-1;

		sep( "This is a silly message!" );

		// Plot the mesh
		igl::viewer::Viewer viewer;
		viewer.data.set_mesh(V, F);
		viewer.data.set_face_based(true);
		viewer.launch();
	}
	
	if( BUNNY ){ // BUNNY!
		
		// Load a mesh in OFF format
		igl::readOFF( TUTORIAL_SHARED_PATH "/bunny.off" , V , F );

		// Plot the mesh
		igl::viewer::Viewer viewer;
		viewer.data.set_mesh( V , F );
		viewer.launch();
		
	}
	
	if( STLTEST ){
		
		using namespace std;
		// using namespace Eigen;
		using namespace igl;
		
		// Announce the file that is being worked on
		string filename = "../Resources/EL.stl"; // Path to the mesh to load
		cout << "  'working on'  = " << filename << endl;
		
		// Load the STL file // Read stl matrix
		Eigen::MatrixXd OV;
		Eigen::MatrixXi OF;
		igl::read_triangle_mesh( filename , OV , OF );
		cout << " original  mesh containts " << OV.rows() << " vertices and " << OF.rows() << " faces" << endl;
		
		// Remove duplicated vertices
		Eigen::MatrixXd V;
		Eigen::MatrixXi F , SVI , SVJ;
		igl::remove_duplicate_vertices( OV , OF , 0.0 , V , SVI , SVJ , F );
		cout << " after removing duplicates, mesh containts " << V.rows() << " vertices and " << F.rows() << " faces" << endl;
		
		// Plot the mesh
		viewer::Viewer viewer;
		viewer.data.set_mesh( OV , OF );
		viewer.launch();
	}
	
	return 0; // Everything turned out OK in the end
}
