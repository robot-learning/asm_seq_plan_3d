/*
integration01.cpp
James Watson , 2018 June

% ROS Node %
Test assembly problems for correctness
Publises To ----> :
Subscribes To <-- :

Dependencies: ROS , Cpp_Helpers , ROS_Helpers
Template Version: 2018-06-06
*/

// === Init ================================================================================================================================

// ~~~ Includes ~~~
// ~~ ROS ~~
#include <ros/ros.h> // -------------- ROS , Publishers , Subscribers
#include <ros/package.h> // ------------------ Where are we? // http://wiki.ros.org/Packages#C.2B-.2B-
// ~ ROS Messages ~
#include "motion_cost/qFromPose.h"
#include "motion_cost/IKrequest.h"
#include "motion_cost/IKresponse.h"
#include "geometry_msgs/Pose.h"
#include "sensor_msgs/JointState.h"
#include <ll4ma_teleop/Joints2Jac.h>
#include <ll4ma_teleop/LinkPoses.h>
#include <ll4ma_teleop/FngrPoses.h>
// ~~ Local ~~
#include <Cpp_Helpers.h> // - C++ Utilities and Shortcuts
#include <ROS_Helpers.h> // - ROS Utilities and Shortcuts
#include <Model_Factory.h> // Assemblies to plan on
#include <AsmSeqPlan.h> // -- Identify grasp pairs , ASP
#include <comm_ASM.h> // ---- Messages and visualization
#include <Motion_Planning.h>
#include <Methods.h>

// ___ End Init ____________________________________________________________________________________________________________________________


/// ######### Node Vars #########

string /* -- */ NODE_NAME  = "integration01";
int /* ----- */ RATE_HZ    = 30;
int /* ----- */ QUEUE_LEN  = 30;
Eigen::Vector3d ROBOT_BASE = Eigen::Vector3d( 0.55 , 0 , 1.20 ); // FUTURE: NEED A WAY TO LOAD THIS FROM A PARAM SERVER

/// ********* End Vars *********


// === Program Functions & Classes ===



// ___ End Functions & Classes ___


// === Program Vars ===

bool CONNECT_TO_ARM = true;

// ___ End Vars ___


// === main ================================================================================================================================

int main( int argc , char** argv ){ // Main takes the terminal command and flags that called it as arguments
	srand( time( 0 ) ); // Random seed based on the current clock time
	
	// 0. Init ROS  &&  Register node
	ros::init( argc , argv , NODE_NAME );
	
	// 1. Fetch handle to this node
	ros::NodeHandle nodeHandle;
	
	// 2. Init node rate
	ros::Rate heartbeat( RATE_HZ );
	
	// 3. Set up subscribers and publishers
	
	// ~ Publishers ~
	//~ ros::Publisher joint_cmd_pub = nodeHandle.advertise<sensor_msgs::JointState>( "/lbr4_teleop/joint_cmd" , QUEUE_LEN );
	//~ send_joint_command jntCmd_Call{ joint_cmd_pub };
	
	ros::Publisher vis_arr       = nodeHandle.advertise<visualization_msgs::MarkerArray>( "visualization_marker_array" , QUEUE_LEN );
	
	// ~ Subscribers ~
	// ros::Subscriber SUBSCRIBER_OBJ = nodeHandle.subscribe( "TOPIC_NAME" , QUEUE_LEN , CALLBACK_FUNCTION );
	
	// ~ Service Servers ~
	// ros::ServiceServer SERVER_OBJ = nodeHandle.advertiseService( "SERVICE_NAME" , SERV_CALLBACK );
	
	// ~ Service Clients ~
	
	//~ ros::ServiceClient  IK_Client = nodeHandle.serviceClient<motion_cost::qFromPose>( "q_from_pose" );
	//~ request_IK_solution IK_clientCall{ IK_Client };
	
	//~ ros::ServiceClient  J_Client = nodeHandle.serviceClient<ll4ma_teleop::Joints2Jac>( "/robot_commander/get_Jacobian" );
	//~ request_Jacobian    J_ClientCall{ J_Client };
	
	//~ ros::ServiceClient  FK_Client = nodeHandle.serviceClient<ll4ma_teleop::LinkPoses>( "/robot_commander/get_links_FK" );
	//~ request_FK_poses    FK_ClientCall{ FK_Client };  // { 1 , 2 , 3 , 4 , 5 , 6 , 7 , Grasp Target }

	//~ ros::ServiceClient  FNG_Client = nodeHandle.serviceClient<ll4ma_teleop::FngrPoses>( "/robot_commander/get_fingr_FK" );
	//~ request_FNG_poses   FNG_ClientCall{ FNG_Client };
	
	
	// N-1. Animation Init 
	srand( ( time( 0 ) % 1 ) * 1000.0 + getpid() ); // Random seed based on the current clock time
	RViz_MarkerManager mrkrMngr{};
	visualization_msgs::MarkerArray& markerArr = mrkrMngr.get_arr();
	
	
	/// === Preliminary { Setup , Instantiation , Planning  } ==============================================================================

	// ~ Rviz Axes ~
	if(0){
		mrkrMngr.add_marker( get_axes_marker( 0.5 , 0.005 ) );
	}
	
	//~ Pose_ASP probPose{ Eigen::Vector3d( 0 , 0 , 0.25 ) , no_turn_quat() };
	
	Assembly_ASM*   testAsm = simple_phone();
	cout << "Set pose for Simple Phone ... " << endl;
	testAsm->set_pose( origin_pose() );
	Eigen::MatrixXd bb /* ----------- */ = testAsm->calc_AABB();
	Eigen::Vector3d crnr1 /* -------- */ = bb.row(0);
	Eigen::Vector3d crnr2 /* -------- */ = bb.row(1);
	double /* -- */ width /* -------- */ = ( crnr2 - crnr1 ).norm() / 2.0 ,
					interval /* ----- */ = 0.0025 ,
					stuckInfluenceFactor = 1.5 ;
	size_t /* -- */ numSteps /* ----- */ = (size_t) ceil( width/interval );
	
	
	// Task 01: Morato Test , COMPLETE , Issue 4 closed
	// Task 04: Validator draft test , Complete
	if( 0 ){
		
		sep( "Morato Test" );
		
		ValidationParams params = validation_defaults();
		
		StopWatch algoTimer;
		
		AsmStateNode_ASP* fullTree = Morato2013Modified_full_depth( testAsm , params );
		
		cout << "Full sequence completed in " << algoTimer.seconds_elapsed() << " seconds." << endl;
		
		visualize_tree_structure_plan( mrkrMngr , 
									   Eigen::Vector2d( 0 , 0 ) , Eigen::Vector2d( 0.125 , 0.0 ) , Eigen::Vector2d( 0.0 , 0.125 ) , 
									   fullTree );
	}
	
	// Task 02: Belhadj Test , Complete
	// Task 05: Validator draft test , Complete , Issue 1 closed
	if( 0 ){
		
		sep( "Belhadj Test" );
		
		ValidationParams params = validation_defaults();
		
		if( 0 ){
		
			// ~ Troubleshooting Assembly ~
			std::vector<llin> partIDs = { 10 , 6 , 7 , 2 , 3 };
			Assembly_ASM* TSAsm = simple_phone( partIDs );
		
			std::vector<std::vector<llin>> levelOne = Belhadj2017_one_level( TSAsm , 
																			 0.4 , 0.2 , 0.4 , 
																			 0.8 , 10 ,
																			 params );
			cout << "Level 1 Results: " << endl;
			
			print_vec_vec( levelOne );
		
		}else{
			
			StopWatch algoTimer;
			
			AsmStateNode_ASP* fullTree = Belhadj2017_full_depth( testAsm , 
																 0.4 , 0.2 , 0.4 , 
																 0.8 , 10 ,
																 params );
			
			cout << "Full sequence completed in " << algoTimer.seconds_elapsed() << " seconds." << endl;
			
			visualize_tree_structure_plan( mrkrMngr , 
										   Eigen::Vector2d( 0 , 0 ) , Eigen::Vector2d( 0.125 , 0.0 ) , Eigen::Vector2d( 0.0 , 0.125 ) , 
										   fullTree );
		}
	}
	
	// Task 03: SIR Test , COMPLETE
	// Task 06: Validator draft test , COMPLETE
	if( 1 ){
		
		sep( "SIR Test" );
		
		ValidationParams params = validation_defaults();
		
		StopWatch algoTimer;
		
		AsmStateNode_ASP* fullTree = sub_ID_blockage_reduction_full_depth( testAsm , 
																		   0.33 , 0.33 , 0.33 ,
																		   interval , numSteps ,
																		   stuckInfluenceFactor ,
																		   0.85 ,
																		   params );
																		   
		cout << "Full sequence completed in " << algoTimer.seconds_elapsed() << " seconds." << endl;
		
		visualize_tree_structure_plan( mrkrMngr , 
									   Eigen::Vector2d( 0 , 0 ) , Eigen::Vector2d( 0.125 , 0.0 ) , Eigen::Vector2d( 0.0 , 0.125 ) , 
									   fullTree );
		
	}
	
	
		
	/// ___ End Preliminary ________________________________________________________________________________________________________________
	
	// N-2. Notify
	ros_log( "[" + NODE_NAME + "] Init OK and about to run ..." , INFO );
	
	// N. Main loop
	while( ros::ok() ){ // While neither node nor ROS has been shut down
		
		/// == NODE WORK ===================================================================================================================
		
		
		
		// 5. Paint frame
		vis_arr.publish( markerArr );
		
		
		//~ break; // ONLY ONCE
		
		/// __ END WORK ____________________________________________________________________________________________________________________
		
		ros::spinOnce(); // - Process messages
		heartbeat.sleep(); // Sleep for remainder of period
	}
	
	// N+1. Notify  &&  Exit
	
	ros_log( "[" + NODE_NAME + "] Exit OK, Goodbye!" , INFO );
	
	return 0; // I guess everything turned out alright at the end!
}

// ___ End main ____________________________________________________________________________________________________________________________


/* === Spare Parts =========================================================================================================================



   ___ End Spare ___________________________________________________________________________________________________________________________
*/
