/*
integration01.cpp
James Watson , 2018 June

% ROS Node %
Test assembly problems for correctness
Publises To ----> :
Subscribes To <-- :

Dependencies: ROS , Cpp_Helpers , ROS_Helpers
Template Version: 2018-06-06
*/

// === Init ================================================================================================================================

// ~~~ Includes ~~~
// ~~ ROS ~~
#include <ros/ros.h> // -------------- ROS , Publishers , Subscribers
#include <ros/package.h> // ------------------ Where are we? // http://wiki.ros.org/Packages#C.2B-.2B-
// ~ ROS Messages ~
#include "motion_cost/qFromPose.h"
#include "motion_cost/IKrequest.h"
#include "motion_cost/IKresponse.h"
#include "geometry_msgs/Pose.h"
#include "sensor_msgs/JointState.h"
#include <ll4ma_teleop/Joints2Jac.h>
#include <ll4ma_teleop/LinkPoses.h>
#include <ll4ma_teleop/FngrPoses.h>
// ~~ Local ~~
#include <Cpp_Helpers.h> // - C++ Utilities and Shortcuts
#include <ROS_Helpers.h> // - ROS Utilities and Shortcuts
#include <Model_Factory.h> // Assemblies to plan on
#include <AsmSeqPlan.h> // -- Identify grasp pairs , ASP
#include <comm_ASM.h> // ---- Messages and visualization
#include <Motion_Planning.h>
#include <Methods.h>

// ___ End Init ____________________________________________________________________________________________________________________________


/// ######### Node Vars #########

string /* -- */ NODE_NAME  = "integration02";
int /* ----- */ RATE_HZ    = 30;
int /* ----- */ QUEUE_LEN  = 30;
Eigen::Vector3d ROBOT_BASE = Eigen::Vector3d( 0.55 , 0 , 1.20 ); // FUTURE: NEED A WAY TO LOAD THIS FROM A PARAM SERVER

/// ********* End Vars *********


// === Program Functions & Classes ===

struct ShellVizMap{
	std::vector<std::pair<llin,double>> mapping;
	std::vector<TriMeshVFN_ASP> /* - */ subShells;
};

ShellVizMap sub_vs_parts_blockage_viz( Assembly_ASM* assembly , // Create projection of one sub and collide it with another
									   const std::vector<llin>& sub , const std::vector<llin>& opponents , // sub moves "against" opponents
									   const std::vector<llin>& refcID , // Reference parts for determining freedom
									   double dRadiusInterval , size_t numSteps ){
	// Blocking calculator: Return a mapping (pair vector) from all parts not in the sub to the fraction that each blocks the sub
	ShellVizMap rtnStruct;
	std::vector<std::pair<llin,double>> rtnVec;
	// 0. Init
	size_t    numOppose = opponents.size();
	Part_ASM* currPart  = nullptr; // --- Pointer to interfering part
	double    currFrac  = 0.0 ,
			  mostFrac  = 0.0 ;
	// 1. Get the shells from the sub
	std::vector<TriMeshVFN_ASP> subShells = get_intersection_surfaces( assembly , 
																	   sub , refcID , 
																	   dRadiusInterval , numSteps );
	// A. Prep shells
	std::vector<CollisionVFN_ASP*> collsnShells = prep_meshes_for_collision( subShells );
	size_t numShells = subShells.size();
	// 2. For each opponent
	for( size_t i = 0 ; i < numOppose ; i++ ){
		// A. Get the part reference
		currPart = assembly->get_part_ptr_w_ID( opponents[i] );
		mostFrac = -BILLION_D;
		// 3. For each shell
		for( size_t j = 0 ; j < numShells ; j++ ){
			// 4. Get the blocking fraction , Store max
			currFrac = collision_fraction( collsnShells[j] , currPart );
			mostFrac = max( mostFrac , currFrac );
		}
		// 5. Push pair
		rtnVec.push_back(  std::make_pair( opponents[i] , mostFrac )  );
	}
	// N. Return
	rtnStruct.mapping   = rtnVec;
	rtnStruct.subShells = subShells;
	return rtnStruct;
}

void load_shells_into_mrkrMngr( const std::vector<TriMeshVFN_ASP>& shells , RViz_MarkerManager& mrkrMngr ){
	size_t numShells = shells.size();
	for( size_t i = 0 ; i < numShells ; i++ ){
		RViz_MeshMarker mrkr{ shells[i] };
		mrkr.rand_color();
		mrkrMngr.add_marker( mrkr );
	}
}

void report_SuccessPath( const SuccessPath& resultsInfo ){
	// Print out the results of a lift action plan
	size_t len = resultsInfo.path.size();
	sep( "Lift Planner Report" );
	cout << "Successful Plan?:_ " << yesno( resultsInfo.success ) << endl
		 << "Returned Code: ___ " << interpret_ValidationCode( resultsInfo.code ) << endl
		 << "Returned String: _ " << resultsInfo.desc << endl
		 << "Elements in Path:_ " << len << endl
		 << "Travel Distance: _ " << resultsInfo.travelDist << endl
		 << "Removal Direction: " << resultsInfo.bestDir << endl
		 << "Good Directions: _ " << resultsInfo.goodDirs.rows() << endl
		 << "Free Poses: ______ " << resultsInfo.posesFree.size() << endl
		 << "Inner Code: ______ " << resultsInfo.code_cached << endl
		 << "Inner String: ____ " << resultsInfo.desc_cached << endl;
	if( resultsInfo.path.size() > 0 ){
		cout << "Initial Pose: ____ " << resultsInfo.initPose << endl
			 << "Final Pose: ______ " << resultsInfo.finlPose << endl;
		if(0){
			for( size_t i = 0 ; i < resultsInfo.path.size() ; i++ ){
				cout << resultsInfo.path[i] << endl;
			}
		}
	}
	cout << "Report COMPLETE!" << endl;
}

void report_level_plan( const std::vector<ActionSpec*>& levelPlan ){
	size_t len = levelPlan.size();
	sep( "Level Plan" );
	if( len > 0 ){
		for( size_t i  = 0 ; i < len ; i++ ){
			if( levelPlan[i] ){
				cout << "Action " << i+1 << " of " << len << endl
					 //~ << "Moved Asm: ___ " << levelPlan[i]->movdAsm->get_part_ID_vec()  << endl
					 //~ << "Reference Asm: " << levelPlan[i]->refcAsm->get_part_ID_vec()  << endl
					 << "Action: ______ " << interpret_ACTIONS( levelPlan[i]->action ) << endl;
				report_SuccessPath( levelPlan[i]->liftAction );
				cout << endl;
			}else{  cout << "Encountered an empty action plan!" << endl;  }
		}
		cout << "Level plan report COMPLETE!" << endl;
	}
}

void report_tree_plan( LevelPlanNode* treeRoot ){
	sep( "Tree Plan Report" );
	std::queue<LevelPlanNode*> subNodes;
	LevelPlanNode* /* ----- */ currNode = nullptr;
	size_t /* ------------- */ len      = 0;
	subNodes.push( treeRoot );
	while( subNodes.size() > 0 ){
		currNode = queue_get_pop( subNodes );
		cout << "Node at depth " << currNode->depth << endl 
			 << "Success?: _ " << yesno( currNode->allPassed ) << endl
			 << "Successors: " << currNode->successors.size() << endl << endl;
		len = currNode->successors.size();
		for( size_t i = 0 ; i < len ; i++ ){  subNodes.push( currNode->successors[i] );  }
	}
}


void record_lift_plan( Assembly_ASM* original , PlanRecording& recording , const SuccessPath& remPath ){
	// Visualize the removal operation represented by 'remPath'
	
	bool SHOWDEBUG = true  , // if( SHOWDEBUG ){  cout << "" << endl;  }
		 BREAKPNTS = true  , // if( BREAKPNTS ){  waitkey();  }
		 POSDETAIL = false ;
	
	if( SHOWDEBUG ){  sep( "record_lift_plan" , 3 );  
					  cout << "movd: " << remPath.movdIDs << endl  
					       << "refc: " << remPath.refcIDs << endl; }
	
	Assembly_ASM* movdAsm   = original->sub_from_spec( remPath.movdIDs );	movdAsm->set_pose( remPath.initPose ); // Lifted asm
	Assembly_ASM* refcAsm   = original->sub_from_spec( remPath.refcIDs ); 	refcAsm->set_pose( remPath.initPose ); // Remaining asm
	
	if( BREAKPNTS ){  
		if( remPath.initPose.position(2) < 0.5 ){
			cout << "Got a bad 'remPath.initPose': " << remPath.initPose << endl;
			waitkey();  
		}
		if( remPath.finlPose.position(2) < 0.5 ){
			cout << "Got a bad 'remPath.finlPose': " << remPath.finlPose << endl;
			waitkey();  
		}
	}
	
	size_t /*- */ numFrames = remPath.path.size();
	PlayerFrame*  currframe = nullptr;
	for( size_t j = 0 ; j < numFrames ; j++ ){
		// a. Instantiate frame 
		currframe = new PlayerFrame();
		// a. Set pose
		movdAsm->set_pose( remPath.path[j] );
		
		if( 0 ){  
			if( remPath.path[j].position(2) < 0.5 ){
				cout << "Got a bad 'remPath.path[j]': " << remPath.path[j] << endl;
				waitkey();  
			}
		}
		
		// b. Set markers
		movdAsm->load_part_mrkrs_into_mngr( currframe->mrkrMngr );
		refcAsm->load_part_mrkrs_into_mngr( currframe->mrkrMngr );
		
		// COMING SOON PHASE 3
		// c. Set joint states
		//~ pack_joint_and_gripper_states_into_msg( stateSeq.qSeq[j] , //- Joint positions of arm
												//~ stateSeq.gripState , // Joint positions of gripper
												//~ currframe->jnt_state_ );
												
		recording.push_back( currframe );
	}
}

//~ void record_level_plan( std::vector<ActionSpec*>& levelSeq , PlanRecording& recording , size_t numRotDwellFrames ){
void record_level_plan( std::vector<ActionSpec*>& levelSeq , PlanRecording& recording ){
	
	bool SHOWDEBUG = true; // if( SHOWDEBUG ){  cout << "" << endl;  }
	
	if( SHOWDEBUG ){  sep( "record_level_plan" , 3 );  }
	
	size_t /*- */ len /*- */ = levelSeq.size() , 
				  numFrames  = 0;
	ActionSpec*   currAction = nullptr;
	Assembly_ASM* currAsm    = nullptr; 
	SuccessPath   stateSeq   = empty_SuccessPath();
	PlayerFrame*  currframe  = nullptr;
	
	//~ // PHASE 3 COMING SOON
	//~ std::vector<double> dummyJoints = {0,0,0,0,0,0,0};
	//~ std::vector<double> dummyGrippr = {0,0};
	
	for( size_t i = 0 ; i < len ; i++ ){
		currAction = levelSeq[i];
		switch( currAction->action ){
			case ACT_REMOVE:
				currAsm  = currAction->assembly;
				stateSeq = currAction->liftAction;
				record_lift_plan( currAsm , recording , stateSeq );
				break;
			case ACT_ROTATE:
			case ACT_NO_OPR:
			default:
				cout << "ACTION WAS BAD OR EMPTY" << endl;
		}
		//~ recording.erase_last_frame(); // Clear markers after each action
	}
	if( SHOWDEBUG ){  cout << "LEVEL RECORD COMPLETE" << endl;  }
}



void record_tree_plan( LevelPlanNode* treeRoot , PlanRecording& recording ){
	sep( "Tree Plan Animate" );
	std::queue<LevelPlanNode*> subNodes;
	LevelPlanNode* /* ----- */ currNode = nullptr;
	size_t /* ------------- */ len      = 0;
	subNodes.push( treeRoot );
	while( subNodes.size() > 0 ){
		currNode = queue_get_pop( subNodes );
		
		cout << "Node at depth " << currNode->depth << endl 
			 << "Success?: _ " << yesno( currNode->allPassed ) << endl
			 << "Successors: " << currNode->successors.size() << endl 
			 << "About to load level plan ..." << endl << endl;
		
		record_level_plan( currNode->levelPlan , recording );
		
		len = currNode->successors.size();
		for( size_t i = 0 ; i < len ; i++ ){  subNodes.push( currNode->successors[i] );  }
	}
}

// ___ End Functions & Classes ___


// === Program Vars ===

bool CONNECT_TO_ARM = true;
PlanRecording recording;

// ___ End Vars ___


// === main ================================================================================================================================

int main( int argc , char** argv ){ // Main takes the terminal command and flags that called it as arguments
	srand( time( 0 ) ); // Random seed based on the current clock time
	
	// 0. Init ROS  &&  Register node
	ros::init( argc , argv , NODE_NAME );
	
	// 1. Fetch handle to this node
	ros::NodeHandle nodeHandle;
	
	// 2. Init node rate
	ros::Rate heartbeat( RATE_HZ );
	
	// 3. Set up subscribers and publishers
	
	// ~ Publishers ~
	ros::Publisher joint_cmd_pub = nodeHandle.advertise<sensor_msgs::JointState>( "/lbr4_teleop/joint_cmd" , QUEUE_LEN );
	send_joint_command jntCmd_Call{ joint_cmd_pub };
	
	ros::Publisher vis_arr       = nodeHandle.advertise<visualization_msgs::MarkerArray>( "visualization_marker_array" , QUEUE_LEN );
	
	// ~ Subscribers ~
	// ros::Subscriber SUBSCRIBER_OBJ = nodeHandle.subscribe( "TOPIC_NAME" , QUEUE_LEN , CALLBACK_FUNCTION );
	
	// ~ Service Servers ~
	// ros::ServiceServer SERVER_OBJ = nodeHandle.advertiseService( "SERVICE_NAME" , SERV_CALLBACK );
	
	// ~ Service Clients ~
	
	ros::ServiceClient  IK_Client = nodeHandle.serviceClient<motion_cost::qFromPose>( "q_from_pose" );
	request_IK_solution IK_clientCall{ IK_Client };
	
	//~ ros::ServiceClient  J_Client = nodeHandle.serviceClient<ll4ma_teleop::Joints2Jac>( "/robot_commander/get_Jacobian" );
	//~ request_Jacobian    J_ClientCall{ J_Client };
	
	//~ ros::ServiceClient  FK_Client = nodeHandle.serviceClient<ll4ma_teleop::LinkPoses>( "/robot_commander/get_links_FK" );
	//~ request_FK_poses    FK_ClientCall{ FK_Client };  // { 1 , 2 , 3 , 4 , 5 , 6 , 7 , Grasp Target }

	//~ ros::ServiceClient  FNG_Client = nodeHandle.serviceClient<ll4ma_teleop::FngrPoses>( "/robot_commander/get_fingr_FK" );
	//~ request_FNG_poses   FNG_ClientCall{ FNG_Client };
	
	
	// N-1. Animation Init 
	srand( ( time( 0 ) % 1 ) * 1000.0 + getpid() ); // Random seed based on the current clock time
	RViz_MarkerManager mrkrMngr{};
	visualization_msgs::MarkerArray& markerArr = mrkrMngr.get_arr();
	
	
	/// === Preliminary { Setup , Instantiation , Planning  } ==============================================================================

	// ~ Rviz Axes ~
	mrkrMngr.add_marker( get_axes_marker( 2.0 , 0.005 ) );
	
	//~ Pose_ASP probPose{ Eigen::Vector3d( 0 , 0 , 0.25 ) , no_turn_quat() };
	
	std::vector<llin> partial = {  0 , // base
								  10 , 6 , 7 , // Mod B + Chips
								   9 , 4 , 5 , // Mod A + Chips
								  11 , 8 , // Mod C + Chip
								   1 };  // Antenna
	//~ std::vector<llin> modBlift = { 10 , 6 , 7 };
	
	//~ Assembly_ASM*   testAsm = simple_phone( partial );
	Assembly_ASM*   topAsm = simple_phone();
	
	cout << "Set pose for Simple Phone ... " << endl;
	topAsm->set_pose( origin_pose() );
	
	Eigen::MatrixXd bb /* ----------- */ = topAsm->calc_AABB();
	Eigen::Vector3d crnr1 /* -------- */ = bb.row(0);
	Eigen::Vector3d crnr2 /* -------- */ = bb.row(1);
	double /* -- */ width /* -------- */ = ( crnr2 - crnr1 ).norm() / 2.0 ,
					interval /* ----- */ = 0.0025 ,
					stuckInfluenceFactor = 1.5 ;
	size_t /* -- */ numSteps /* ----- */ = (size_t) ceil( width/interval );
	
	// <<< OUTPUT >>>
	//~ PlanRecording recording;
	//~ PlanPlayer player{ 30.0 , &recording };
	//~ PlayerFrame* currFrame = nullptr;
	//~ bool LOOP = true;
	
	// Task 7: Test lift planner
	if( 0 ){
		ValidationParams params = validation_defaults();
		params.IKsearchLimit  = 1; // 2018-07-19: These have been moved to default
		params.IKattemptLimit = 5;
		params.graspTileSize  = 0.004;
		params.graspDTheta    = M_PI / 4.0;
		params.RRT_K          = 8000;
		
		// 1. Determine mover and reference
		std::vector<llin> movd = { 7 };
		std::vector<llin> refc = { 0 , 10 , 6 };
		// 2. Situate the assembly
		topAsm->set_lab_pose_for_index_3D( 0 , params.setdownLoc , true );
		topAsm->set_marker_opacity( 0.5 );
		topAsm->load_part_mrkrs_into_mngr( mrkrMngr );
		// 3. Situate the floor
		Floor_ASM floor{ params.setdownLoc , 1 , 1 };
		
		SuccessPath planResult = plan_one_removal_action_multimode( topAsm , movd , refc ,
																	floor , params ,
																	IK_clientCall );
		
		report_SuccessPath( planResult );
	}
	
	
	
	/// !!!! TASK 12 : RECOVER LEVEL PLANNER !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
	
	// Task 8: Test level planner
	if( 0 ){
		ValidationParams  params   = validation_defaults();
		AsmStateNode_ASP* planRoot = Morato2013Modified_full_depth( topAsm , params );
		Floor_ASM floor{ params.setdownLoc , 1 , 1 };
		std::vector<ActionSpec*> levelPlan = tree_plan_exec_one_level( planRoot , floor , IK_clientCall , params );
		report_level_plan( levelPlan );
		record_level_plan( levelPlan , recording );
	}
	
	/// **** END LEVEL *********************************************************************************************************************
	
	// Task 9: Test tree planner
	if( 1 ){
		ValidationParams  params   = validation_defaults();
		params.distance      = 0.10;
		params.RRT_K         = 30000;
		params.RRT_shortCutN = 500;
		
		if( 0 ){
			topAsm->report_liaison_and_self_collision_state();
			topAsm->set_lab_pose_for_index_3D( 0 , params.setdownLoc );
			
			std::vector<llin> movd = { 2 };
			std::vector<llin> refc = vec_copy_without_elem( topAsm->get_part_ID_vec() , movd );
			
			Eigen::Vector3d remDir = topAsm->asm_center_of_freedom_direction( movd , refc ); // Part ID
			
			cout << "Suggested Removal Direction: " << remDir << endl;
			topAsm->set_marker_opacity( 0.5 );
			topAsm->load_part_mrkrs_into_mngr( mrkrMngr );
			
		}else{
			AsmStateNode_ASP* planRoot = nullptr;
			
			if( 1 ){
				planRoot = Morato2013Modified_full_depth( topAsm , params );
			}else if( 0 ){
				planRoot = Belhadj2017_full_depth( topAsm , 
												   //~ 0.4 , 0.2 , 0.4 , 
												   0.30 , 0.40 , 0.30 ,
												   0.8 , 10 ,
												   params );
			}else{
				planRoot = sub_ID_blockage_reduction_full_depth( topAsm , 
																 0.30 , 0.40 , 0.30 ,
																 //~ 0.33 , 0.33 , 0.33 ,
																 interval , numSteps ,
																 stuckInfluenceFactor ,
																 0.85 ,
																 params );
			}
			
			
			Floor_ASM floor{ params.setdownLoc , 1 , 1 };
			
			LevelPlanNode* fullTree = tree_plan_exec_full_depth( planRoot , floor , IK_clientCall , params );
			
			report_tree_plan( fullTree );
			
			record_tree_plan( fullTree , recording );
		}
	}
	
	/// !!!! TASK 10 : RECOVER VALIDATOR !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
	
	if(0){
		
		std::vector<llin> TS_partial = {  0 , // base
		//~ std::vector<llin> TS_partial = {  10 , // Module B
								  //~ 10 , }; // Mod B
								  //~ 10 , 6 , 7 , // Mod B + Chips
								  //~ 10 , 6 , 7 }; // Mod B + Chips
								   //~ 9 , 4 , 5 , // Mod A + Chips
								  //~ 11 , 8 , // Mod C + Chip
								   1 };  // Antenna
								   //~ 2 };  // Bridge AB
		
		ValidationParams params = validation_defaults();
		
		//~ Assembly_ASM* TS_testAsm = simple_phone( TS_partial );
		Assembly_ASM* TS_testAsm = simple_phone(  );
		TS_testAsm->set_pose( Pose_ASP{ params.setdownLoc , no_turn_quat() } );
		
		TS_testAsm->set_marker_opacity( 0.75 );
		TS_testAsm->load_part_mrkrs_into_mngr( mrkrMngr );
		
		//~ std::vector<llin> movd = { 10 };
		std::vector<llin> movd = {  1 };
		std::vector<llin> refc = {  0 };
		//~ std::vector<llin> movd = { 9,4,5,10,6,7,11,8,2,3,1 };
		//~ std::vector<llin> refc = {  0 };
		
		if(1) TS_testAsm->report_liaison_and_self_collision_state(); // ASM CONNECTIONS OK
		
		SuccessPath TS_Success;
		
		
		// ~ A. One Test ~
		if(1){
			TS_Success = validate_removal_multimode_w_stability( TS_testAsm , // Validate for removal and stability , Using Straightline and RRT*
																 movd , refc , 
																 params );
			report_SuccessPath( TS_Success );
		}
		
		// ~ B. Rotate and Test ~
		if(0){
			for( size_t i = 0 ; i < 4 ; i++ ){
				TS_testAsm->set_pose( Pose_ASP{ params.setdownLoc , 
												Eigen::AngleAxisd( (double)i * M_PI / 2.0 , Eigen::Vector3d::UnitZ() ) * no_turn_quat() } );
				TS_Success = validate_removal_multimode_w_stability( TS_testAsm , // Validate for removal and stability , Using Straightline and RRT*
																	 movd , refc , 
																	 params );
				cout << endl;
			}
		}
		
	}
	
	/// **** END VALIDATE ******************************************************************************************************************
		
	
		
	/// !!!! TASK 11 : RECOVER LIFT PLANNER !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
	
	if(0){
		
		std::vector<llin> TS_partial = {  0 , // base
		//~ std::vector<llin> TS_partial = {  10 , // Module B
								  //~ 10 , }; // Mod B
								  //~ 10 , 6 , 7 , // Mod B + Chips
								  //~ 10 , 6 , 7 }; // Mod B + Chips
								   //~ 9 , 4 , 5 , // Mod A + Chips
								  //~ 11 , 8 , // Mod C + Chip
								   1 };  // Antenna
								   //~ 2 };  // Bridge AB
		
		ValidationParams params = validation_defaults();
		
		Assembly_ASM* TS_testAsm = simple_phone( TS_partial );
		//~ Assembly_ASM* TS_testAsm = simple_phone(  );
		
		TS_testAsm->set_pose( Pose_ASP{ params.setdownLoc , 
										Eigen::AngleAxisd( M_PI / 2.0 , Eigen::Vector3d::UnitZ() ) * no_turn_quat() } );
		
		TS_testAsm->set_marker_opacity( 0.75 );
		TS_testAsm->load_part_mrkrs_into_mngr( mrkrMngr );
		
		// 1. Determine mover and reference
		std::vector<llin> movd = { 1 };
		std::vector<llin> refc = { 0 };
		// 3. Situate the floor
		Floor_ASM floor{ params.setdownLoc , 1 , 1 };
		
		SuccessPath planResult = plan_one_removal_action_multimode( TS_testAsm , movd , refc ,
																	floor , params ,
																	IK_clientCall );
																	
		record_lift_plan( TS_testAsm , recording , planResult );
		
		report_SuccessPath( planResult );
		
	}
	
	/// **** END LIFT **********************************************************************************************************************
	
	size_t LOOPLIMIT = 10 ,
		   counter   =  0 ;
	PlayerFrame*  currFrame = nullptr;	
	bool /* -- */ LOOP	    = true;
	PlanPlayer player{ 20.0 , &recording };
		
	/// ___ End Preliminary ________________________________________________________________________________________________________________
	
	// N-2. Notify
	ros_log( "[" + NODE_NAME + "] Init OK and about to run ..." , INFO );
	
	cerr << "Before loop!" << endl;
	
	// N. Main loop
	while( ros::ok() ){ // While neither node nor ROS has been shut down
		
		/// == NODE WORK ===================================================================================================================

		cerr << "Loop ";

		// Request frame from renderer

		// A. Fetch frame
		currFrame = player.current_frame( LOOP );
		// B. Render object markers
		visualization_msgs::MarkerArray& markerArr = currFrame->mrkrMngr.get_arr();
		
		
		// COMING SOON PHASE 3
		//~ // C. Render the joint state
		//~ jntCmd_Call( currFrame->get_q() );

		// 5. Paint frame
		vis_arr.publish( markerArr );
		
		//~ if( counter >= LOOPLIMIT ){  break;  }  counter++;
		//~ break; // ONLY ONCE
		
		/// __ END WORK ____________________________________________________________________________________________________________________
		
		ros::spinOnce(); // - Process messages
		heartbeat.sleep(); // Sleep for remainder of period
	}
	
	cerr << "Outside loop!" << endl;
	
	// N+1. Notify  &&  Exit
	
	ros_log( "[" + NODE_NAME + "] Exit OK, Goodbye!" , INFO );
	
	return 0; // I guess everything turned out alright at the end!
}

// ___ End main ____________________________________________________________________________________________________________________________


/* === Spare Parts =========================================================================================================================



   ___ End Spare ___________________________________________________________________________________________________________________________
*/
