/*
integration03.cpp
James Watson , 2018 June

% ROS Node %
Test assembly problems for correctness
Publises To ----> :
Subscribes To <-- :

Dependencies: ROS , Cpp_Helpers , ROS_Helpers
Template Version: 2018-06-06
*/

// === Init ================================================================================================================================

// ~~~ Includes ~~~
// ~~ ROS ~~
#include <ros/ros.h> // -------------- ROS , Publishers , Subscribers
#include <ros/package.h> // ------------------ Where are we? // http://wiki.ros.org/Packages#C.2B-.2B-
// ~ ROS Messages ~
#include "motion_cost/qFromPose.h"
#include "motion_cost/IKrequest.h"
#include "motion_cost/IKresponse.h"
#include "geometry_msgs/Pose.h"
#include "sensor_msgs/JointState.h"
#include <ll4ma_teleop/Joints2Jac.h>
#include <ll4ma_teleop/LinkPoses.h>
#include <ll4ma_teleop/FngrPoses.h>
// ~~ Local ~~
#include <Cpp_Helpers.h> // - C++ Utilities and Shortcuts
#include <ROS_Helpers.h> // - ROS Utilities and Shortcuts
#include <Model_Factory.h> // Assemblies to plan on
#include <AsmSeqPlan.h> // -- Identify grasp pairs , ASP
#include <comm_ASM.h> // ---- Messages and visualization
#include <Motion_Planning.h>
#include <Methods.h>

// ___ End Init ____________________________________________________________________________________________________________________________


/// ######### Node Vars #########

string /* -- */ NODE_NAME  = "integration03";
int /* ----- */ RATE_HZ    = 30;
int /* ----- */ QUEUE_LEN  = 30;
Eigen::Vector3d ROBOT_BASE = Eigen::Vector3d( 0.55 , 0 , 1.20 ); // FUTURE: NEED A WAY TO LOAD THIS FROM A PARAM SERVER

/// ********* End Vars *********


// === Program Functions & Classes ===

void report_SuccessPath( const SuccessPath& resultsInfo ){
	// Print out the results of a lift action plan
	size_t len = resultsInfo.path.size();
	sep( "Lift Planner Report" );
	cout << "Successful Plan?:_ " << yesno( resultsInfo.success ) << endl
		 << "Returned Code: ___ " << interpret_ValidationCode( resultsInfo.code ) << endl
		 << "Returned String: _ " << resultsInfo.desc << endl
		 << "Elements in Path:_ " << len << endl
		 << "Travel Distance: _ " << resultsInfo.travelDist << endl
		 << "Removal Direction: " << resultsInfo.bestDir << endl
		 << "Good Directions: _ " << resultsInfo.goodDirs.rows() << endl
		 << "Free Poses: ______ " << resultsInfo.posesFree.size() << endl
		 << "Inner Code: ______ " << resultsInfo.code_cached << endl
		 << "Inner String: ____ " << resultsInfo.desc_cached << endl;
	if( resultsInfo.path.size() > 0 ){
		cout << "Initial Pose: ____ " << resultsInfo.initPose << endl
			 << "Final Pose: ______ " << resultsInfo.finlPose << endl;
		if(0){
			for( size_t i = 0 ; i < resultsInfo.path.size() ; i++ ){
				cout << resultsInfo.path[i] << endl;
			}
		}
	}
	cout << "Report COMPLETE!" << endl;
}

void report_level_plan( const std::vector<ActionSpec*>& levelPlan ){
	size_t len = levelPlan.size();
	sep( "Level Plan" );
	if( len > 0 ){
		for( size_t i  = 0 ; i < len ; i++ ){
			if( levelPlan[i] ){
				cout << "Action " << i+1 << " of " << len << endl
					 //~ << "Moved Asm: ___ " << levelPlan[i]->movdAsm->get_part_ID_vec()  << endl
					 //~ << "Reference Asm: " << levelPlan[i]->refcAsm->get_part_ID_vec()  << endl
					 << "Action: ______ " << interpret_ACTIONS( levelPlan[i]->action ) << endl;
				report_SuccessPath( levelPlan[i]->liftAction );
				cout << endl;
			}else{  cout << "Encountered an empty action plan!" << endl;  }
		}
		cout << "Level plan report COMPLETE!" << endl;
	}
}

void report_tree_plan( LevelPlanNode* treeRoot ){
	sep( "Tree Plan Report" );
	std::queue<LevelPlanNode*> subNodes;
	LevelPlanNode* /* ----- */ currNode = nullptr;
	size_t /* ------------- */ len      = 0;
	subNodes.push( treeRoot );
	while( subNodes.size() > 0 ){
		currNode = queue_get_pop( subNodes );
		cout << "Node at depth " << currNode->depth << endl 
			 << "Success?: _ " << yesno( currNode->allPassed ) << endl
			 << "Successors: " << currNode->successors.size() << endl << endl;
		len = currNode->successors.size();
		for( size_t i = 0 ; i < len ; i++ ){  subNodes.push( currNode->successors[i] );  }
	}
}


void record_lift_plan( Assembly_ASM* original , PlanRecording& recording , const SuccessPath& remPath ){
	// Visualize the removal operation represented by 'remPath'
	
	bool SHOWDEBUG = true  , // if( SHOWDEBUG ){  cout << "" << endl;  }
		 BREAKPNTS = false , // if( BREAKPNTS ){  waitkey();  }
		 POSDETAIL = false ;
	
	if( SHOWDEBUG ){  sep( "record_lift_plan" , 3 );  
					  cout << "movd: " << remPath.movdIDs << endl  
					       << "refc: " << remPath.refcIDs << endl; }
	
	Assembly_ASM* movdAsm   = original->sub_from_spec( remPath.movdIDs );	movdAsm->set_pose( remPath.initPose ); // Lifted asm
	Assembly_ASM* refcAsm   = original->sub_from_spec( remPath.refcIDs ); 	refcAsm->set_pose( remPath.initPose ); // Remaining asm
	
	// PHASE 3 TEMPORARY
	hand_collision_model handModel;		handModel.init();
	handModel.set_finger_state( remPath.bestGrasp.q_fingers );
	
	if( BREAKPNTS ){  
		if( remPath.initPose.position(2) < 0.5 ){
			cout << "Got a bad 'remPath.initPose': " << remPath.initPose << endl;
			waitkey();  
		}
		if( remPath.finlPose.position(2) < 0.5 ){
			cout << "Got a bad 'remPath.finlPose': " << remPath.finlPose << endl;
			waitkey();  
		}
	}
	
	size_t /*- */ numFrames = min( remPath.path.size() , remPath.qSeq.size() );
	PlayerFrame*  currframe = nullptr;
	for( size_t j = 0 ; j < numFrames ; j++ ){
		// a. Instantiate frame 
		currframe = new PlayerFrame();
		// b. Set pose
		movdAsm->set_pose( remPath.path[j] );
		handModel.set_pose( remPath.handPath[j] );
		
		if( 0 ){  
			if( remPath.path[j].position(2) < 0.5 ){
				cout << "Got a bad 'remPath.path[j]': " << remPath.path[j] << endl;
				waitkey();  
			}
		}
		
		// c. Set markers , Asm
		movdAsm->load_part_mrkrs_into_mngr( currframe->mrkrMngr );
		refcAsm->load_part_mrkrs_into_mngr( currframe->mrkrMngr );
		// d. Set markers , Hand
		handModel.load_part_mrkrs_into_mngr( currframe->mrkrMngr );
		
		// e. Set joint states
		pack_joint_and_gripper_states_into_msg( remPath.qSeq[j] , //- Joint positions of arm
												remPath.gripState , // Joint positions of gripper
												currframe->jnt_state_ );
												
		recording.push_back( currframe );
	}
}

//~ void record_level_plan( std::vector<ActionSpec*>& levelSeq , PlanRecording& recording , size_t numRotDwellFrames ){
void record_level_plan( std::vector<ActionSpec*>& levelSeq , PlanRecording& recording ){
	
	bool SHOWDEBUG = true; // if( SHOWDEBUG ){  cout << "" << endl;  }
	
	if( SHOWDEBUG ){  sep( "record_level_plan" , 3 );  }
	
	size_t /*- */ len /*- */ = levelSeq.size() , 
				  numFrames  = 0;
	ActionSpec*   currAction = nullptr;
	Assembly_ASM* currAsm    = nullptr; 
	SuccessPath   stateSeq   = empty_SuccessPath();
	PlayerFrame*  currframe  = nullptr;
	
	//~ // PHASE 3 COMING SOON
	//~ std::vector<double> dummyJoints = {0,0,0,0,0,0,0};
	//~ std::vector<double> dummyGrippr = {0,0};
	
	for( size_t i = 0 ; i < len ; i++ ){
		currAction = levelSeq[i];
		switch( currAction->action ){
			case ACT_REMOVE:
				currAsm  = currAction->assembly;
				stateSeq = currAction->liftAction;
				record_lift_plan( currAsm , recording , stateSeq );
				break;
			case ACT_ROTATE:
			case ACT_NO_OPR:
			default:
				cout << "ACTION WAS BAD OR EMPTY" << endl;
		}
		//~ recording.erase_last_frame(); // Clear markers after each action
	}
	if( SHOWDEBUG ){  cout << "LEVEL RECORD COMPLETE" << endl;  }
}



void record_tree_plan( LevelPlanNode* treeRoot , PlanRecording& recording ){
	sep( "Tree Plan Animate" );
	std::queue<LevelPlanNode*> subNodes;
	LevelPlanNode* /* ----- */ currNode = nullptr;
	size_t /* ------------- */ len      = 0;
	subNodes.push( treeRoot );
	while( subNodes.size() > 0 ){
		currNode = queue_get_pop( subNodes );
		
		cout << "Node at depth " << currNode->depth << endl 
			 << "Success?: _ " << yesno( currNode->allPassed ) << endl
			 << "Successors: " << currNode->successors.size() << endl 
			 << "About to load level plan ..." << endl << endl;
		
		record_level_plan( currNode->levelPlan , recording );
		
		len = currNode->successors.size();
		for( size_t i = 0 ; i < len ; i++ ){  subNodes.push( currNode->successors[i] );  }
	}
}

// ___ End Functions & Classes ___


// === Program Vars ===

bool CONNECT_TO_ARM = true;
PlanRecording recording;

// ___ End Vars ___


// === main ================================================================================================================================

int main( int argc , char** argv ){ // Main takes the terminal command and flags that called it as arguments
	srand( time( 0 ) ); // Random seed based on the current clock time
	
	// 0. Init ROS  &&  Register node
	ros::init( argc , argv , NODE_NAME );
	
	// 1. Fetch handle to this node
	ros::NodeHandle nodeHandle;
	
	// 2. Init node rate
	ros::Rate heartbeat( RATE_HZ );
	
	// 3. Set up subscribers and publishers
	
	// ~ Publishers ~
	ros::Publisher joint_cmd_pub = nodeHandle.advertise<sensor_msgs::JointState>( "/lbr4_teleop/joint_cmd" , QUEUE_LEN );
	send_joint_command jntCmd_Call{ joint_cmd_pub };
	
	ros::Publisher vis_arr       = nodeHandle.advertise<visualization_msgs::MarkerArray>( "visualization_marker_array" , QUEUE_LEN );
	
	// ~ Subscribers ~
	// ros::Subscriber SUBSCRIBER_OBJ = nodeHandle.subscribe( "TOPIC_NAME" , QUEUE_LEN , CALLBACK_FUNCTION );
	
	// ~ Service Servers ~
	// ros::ServiceServer SERVER_OBJ = nodeHandle.advertiseService( "SERVICE_NAME" , SERV_CALLBACK );
	
	// ~ Service Clients ~
	
	ros::ServiceClient  IK_Client = nodeHandle.serviceClient<motion_cost::qFromPose>( "q_from_pose" );
	request_IK_solution IK_clientCall{ IK_Client };
	
	//~ ros::ServiceClient  J_Client = nodeHandle.serviceClient<ll4ma_teleop::Joints2Jac>( "/robot_commander/get_Jacobian" );
	//~ request_Jacobian    J_ClientCall{ J_Client };
	
	//~ ros::ServiceClient  FK_Client = nodeHandle.serviceClient<ll4ma_teleop::LinkPoses>( "/robot_commander/get_links_FK" );
	//~ request_FK_poses    FK_ClientCall{ FK_Client };  // { 1 , 2 , 3 , 4 , 5 , 6 , 7 , Grasp Target }

	//~ ros::ServiceClient  FNG_Client = nodeHandle.serviceClient<ll4ma_teleop::FngrPoses>( "/robot_commander/get_fingr_FK" );
	//~ request_FNG_poses   FNG_ClientCall{ FNG_Client };
	
	
	// N-1. Animation Init 
	srand( ( time( 0 ) % 1 ) * 1000.0 + getpid() ); // Random seed based on the current clock time
	RViz_MarkerManager mrkrMngr{};
	visualization_msgs::MarkerArray& markerArr = mrkrMngr.get_arr();
	
	
	/// === Preliminary { Setup , Instantiation , Planning  } ==============================================================================

	// ~ Rviz Axes ~
	if( 0 ){ // NOT PRETTY
		mrkrMngr.add_marker( get_axes_marker( 2.0 , 0.0025 ) );
	}
	
	
	/// ~~~~ Module Box , LL4MA ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
	/// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
	
	if( 0 ){
		std::vector<llin> partial = { 0 , // Base
									  2 , 3 , // Bushings
									  1 , // Motor
									  4 , 5 , // Pillars
									  6 , // Cover
									  7 }; // Shell
									  
		ValidationParams params = validation_defaults();
									  
		//~ Assembly_ASM* testAsm = drive_motor( partial );
		Assembly_ASM* testAsm = simple_phone();
		testAsm->set_lab_pose_for_index_3D( 0 , params.setdownLoc );
		//~ testAsm->set_marker_opacity( 0.75 );
		//~ testAsm->load_part_mrkrs_into_mngr( mrkrMngr );
		testAsm->report_liaison_and_self_collision_state();
		
		if( 1 ){
		
			/***/ sep( "Morato Planner" ); /***********************************************************************************************/
			
			params = validation_defaults();
			params.graspTileSize = 0.004;
			params.graspDTheta   = M_PI / 6.0;
			
			if( 0 ){ // One level , Static
			
				std::vector<std::vector<llin>> clusterSubs = cluster_one_level_disasm( testAsm , params );
				
				cout << "Generated the following clusters:" << endl;
				print_vec_vec( clusterSubs );
				cout << "Report complete!" << endl;
			
			}
			
			if( 1 ){ // Full Depth 
			
				StopWatch algoTimer;
				
				AsmStateNode_ASP* fullTree = Morato2013Modified_full_depth( testAsm , params );
				
				cout << "Full sequence completed in " << algoTimer.seconds_elapsed() << " seconds." << endl;
				
				if( 0 ){ // Static
				
					visualize_tree_structure_plan( mrkrMngr , 
												   Eigen::Vector2d( 0 , 0 ) , Eigen::Vector2d( 0.125 , 0.0 ) , Eigen::Vector2d( 0.0 , 0.125 ) , 
												   fullTree );
				}else{ // Full Depth , Animated
				 
					Floor_ASM floor{ params.setdownLoc , 1 , 1 };

					LevelPlanNode* fullPlan = tree_plan_exec_full_depth( fullTree , floor , IK_clientCall , params );

					report_tree_plan( fullPlan );

					record_tree_plan( fullPlan , recording );
					 
				}
			}
		}else if( 0 ){
			
			/***/ sep( "Belhadj Planner" ); /**********************************************************************************************/
			
			params = validation_defaults();
			
			// Full Depth 
			
			StopWatch algoTimer;
			
			AsmStateNode_ASP* fullTree = Belhadj2017_full_depth( testAsm , 
																 0.4 , 0.2 , 0.4 , 
																 0.8 , 10 ,
																 params );
			
			cout << "Full sequence completed in " << algoTimer.seconds_elapsed() << " seconds." << endl;
			
			if( 0 ){ // Static
			
				visualize_tree_structure_plan( mrkrMngr , 
											   Eigen::Vector2d( 0 , 0 ) , Eigen::Vector2d( 0.125 , 0.0 ) , Eigen::Vector2d( 0.0 , 0.125 ) , 
											   fullTree );
			}else{ // Full Depth , Animated

				Floor_ASM floor{ params.setdownLoc , 1 , 1 };

				LevelPlanNode* fullPlan = tree_plan_exec_full_depth( fullTree , floor , IK_clientCall , params );

				report_tree_plan( fullPlan );

				record_tree_plan( fullPlan , recording );
				 
			}
					 
					 
			
			
		}else{
			
			/***/ sep( "SIR Planner" );  /*************************************************************************************************/
		
			ValidationParams params = validation_defaults();
			
			testAsm->set_pose( origin_pose() );
			Eigen::MatrixXd bb /* ----------- */ = testAsm->calc_AABB();
			Eigen::Vector3d crnr1 /* -------- */ = bb.row(0);
			Eigen::Vector3d crnr2 /* -------- */ = bb.row(1);
			double /* -- */ width /* -------- */ = ( crnr2 - crnr1 ).norm() / 2.0 ,
							interval /* ----- */ = 0.0025 ,
							stuckInfluenceFactor = 1.5 ;
			size_t /* -- */ numSteps /* ----- */ = (size_t) ceil( width/interval );
			
			// Full Depth
			
			StopWatch algoTimer;
			
			AsmStateNode_ASP* fullTree = sub_ID_blockage_reduction_full_depth( testAsm , 
																			   0.33 , 0.33 , 0.33 ,
																			   interval , numSteps ,
																			   stuckInfluenceFactor ,
																			   0.85 ,
																			   params );
																			   
			cout << "Full sequence completed in " << algoTimer.seconds_elapsed() << " seconds." << endl;
			
			if( 0 ){ // Static
			
				visualize_tree_structure_plan( mrkrMngr , 
											   Eigen::Vector2d( 0 , 0 ) , Eigen::Vector2d( 0.125 , 0.0 ) , Eigen::Vector2d( 0.0 , 0.125 ) , 
											   fullTree );
										   
			}else{ // Full Depth , Animated
			 

			 
				Floor_ASM floor{ params.setdownLoc , 1 , 1 };

				LevelPlanNode* fullPlan = tree_plan_exec_full_depth( fullTree , floor , IK_clientCall , params );

				report_tree_plan( fullPlan );

				record_tree_plan( fullPlan , recording );
				 
			}
		}
	}
	
	
	/// ~~~~ Drive Motor , Wang ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
	/// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
	
	if( 1 ){
		std::vector<llin> partial = { 0 , // Base
									  2 , 3 , // Bushings
									  1 , // Motor
									  4 , 5 , // Pillars
									  6 , // Cover
									  7 }; // Shell
		//~ Assembly_ASM* testAsm = drive_motor( partial );
		
		ValidationParams params = validation_defaults();
		params.graspTileSize    =  0.003;
		params.graspDTheta      = M_PI / 25.0;
		params.distance         =  0.100; // ---- Distance to lift
		params.IKsearchLimit    =  12; // Number of attempts for each grasp
		params.IKattemptLimit   =  37; // Number of attempts for a pose we care about
		
		Assembly_ASM* testAsm = drive_motor();
		testAsm->set_lab_pose_for_index_3D( 0 , params.setdownLoc );
		//~ testAsm->set_marker_opacity( 0.75 );
		//~ testAsm->load_part_mrkrs_into_mngr( mrkrMngr );
		testAsm->report_liaison_and_self_collision_state();
		
		if( 0 ){
		
			/***/ sep( "Morato Planner" ); /***********************************************************************************************/
			
			params = validation_defaults();
			
			if( 0 ){ // Single Level , Static
			
				std::vector<std::vector<llin>> clusterSubs = cluster_one_level_disasm( testAsm , params );
				
				cout << "Generated the following clusters:" << endl;
				print_vec_vec( clusterSubs );
				cout << "Report complete!" << endl;
			
			}
			
			if( 1 ){ 
			
				StopWatch algoTimer;
				
				AsmStateNode_ASP* fullTree = Morato2013Modified_full_depth( testAsm , params );
				
				cout << "Full sequence completed in " << algoTimer.seconds_elapsed() << " seconds." << endl;
				
				
				if( 1 ){ // Full Depth , Static
				
					visualize_tree_structure_plan( mrkrMngr , 
												   Eigen::Vector2d( 0 , 0 ) , Eigen::Vector2d( 0.125 , 0.0 ) , Eigen::Vector2d( 0.0 , 0.125 ) , 
												   fullTree );
				}else{ // Full Depth , Animated

					Floor_ASM floor{ params.setdownLoc , 1 , 1 };

					LevelPlanNode* fullPlan = tree_plan_exec_full_depth( fullTree , floor , IK_clientCall , params );

					report_tree_plan( fullPlan );

					record_tree_plan( fullPlan , recording );
				}
			}
		}else if( 0 ){
			
			/***/ sep( "Belhadj Planner" ); /**********************************************************************************************/
			
			ValidationParams params = validation_defaults();
			
			
			
			StopWatch algoTimer;
			
			AsmStateNode_ASP* fullTree = Belhadj2017_full_depth( testAsm , 
																 0.4 , 0.2 , 0.4 , 
																 0.8 , 10 ,
																 params );
			
			cout << "Full sequence completed in " << algoTimer.seconds_elapsed() << " seconds." << endl;
			
			if( 1 ){ // Full Depth , Static
				
				visualize_tree_structure_plan( mrkrMngr , 
											   Eigen::Vector2d( 0 , 0 ) , Eigen::Vector2d( 0.125 , 0.0 ) , Eigen::Vector2d( 0.0 , 0.125 ) , 
											   fullTree );
											   
			}else{ // Full Depth , Animated

				Floor_ASM floor{ params.setdownLoc , 1 , 1 };

				LevelPlanNode* fullPlan = tree_plan_exec_full_depth( fullTree , floor , IK_clientCall , params );

				report_tree_plan( fullPlan );

				record_tree_plan( fullPlan , recording );
				 
			}
		}else{
			
			/***/ sep( "SIR Planner" );  /*************************************************************************************************/
		
			ValidationParams params = validation_defaults();
			
			testAsm->set_pose( origin_pose() );
			Eigen::MatrixXd bb /* ----------- */ = testAsm->calc_AABB();
			Eigen::Vector3d crnr1 /* -------- */ = bb.row(0);
			Eigen::Vector3d crnr2 /* -------- */ = bb.row(1);
			double /* -- */ width /* -------- */ = ( crnr2 - crnr1 ).norm() / 2.0 ,
							interval /* ----- */ = 0.0025 ,
							stuckInfluenceFactor = 1.5 ;
			size_t /* -- */ numSteps /* ----- */ = (size_t) ceil( width/interval );
			
			
			
			StopWatch algoTimer;
			
			AsmStateNode_ASP* fullTree = sub_ID_blockage_reduction_full_depth( testAsm , 
																			   0.33 , 0.33 , 0.33 ,
																			   interval , numSteps ,
																			   stuckInfluenceFactor ,
																			   0.85 ,
																			   params );
																			   
			cout << "Full sequence completed in " << algoTimer.seconds_elapsed() << " seconds." << endl;
			
			if( 1 ){ // Full Depth , Static
				
				visualize_tree_structure_plan( mrkrMngr , 
											   Eigen::Vector2d( 0 , 0 ) , Eigen::Vector2d( 0.125 , 0.0 ) , Eigen::Vector2d( 0.0 , 0.125 ) , 
											   fullTree );
			}else{ // Full Depth , Animated
				 
				Floor_ASM floor{ params.setdownLoc , 1 , 1 };

				LevelPlanNode* fullPlan = tree_plan_exec_full_depth( fullTree , floor , IK_clientCall , params );

				report_tree_plan( fullPlan );

				record_tree_plan( fullPlan , recording );
				 
			}
		}
	}
	
	
/// ^^^^^^^^^^^^^^^^^^^ FUNCTIONAL MODELS ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^ ///
/// ******************************************************************************************************************************************************* ///
/// VVVVVVVVVVVVVVVVVVV NEED SOME WORK , ADAPT YOUR METHOD TO AVOID INFINITE LOOPS VVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVV ///
	
	
	
	/// ~~~~ Validator , Kheder ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
	/// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
	
	if( 0 ){
		std::vector<llin> partial = {  0 , // Big Cee
									   1 , // Little Cee
									   2 ,  3 , // Pillars
									   5 ,  6 ,  7 ,  8 , // Short Bolts
									   9 , 10 , // Long Bolts
									   4 , // Bar
									  11 , 12 , // Small Nuts
									  13 , // Large Bolt
									  14 }; // Large Nut
		//~ Assembly_ASM* testAsm = validator( partial );
		Assembly_ASM* testAsm = validator( );
		//~ testAsm->set_marker_opacity( 0.75 );
		//~ testAsm->load_part_mrkrs_into_mngr( mrkrMngr );
		
		testAsm->report_liaison_and_self_collision_state();
		
		if( 0 ){
		
			/***/ sep( "Morato Planner" ); /***********************************************************************************************/
			
			ValidationParams params = validation_defaults();
			
			StopWatch algoTimer;
			
			AsmStateNode_ASP* fullTree = Morato2013Modified_full_depth( testAsm , params );
			
			cout << "Full sequence completed in " << algoTimer.seconds_elapsed() << " seconds." << endl;
			
			visualize_tree_structure_plan( mrkrMngr , 
										   Eigen::Vector2d( 0 , 0 ) , Eigen::Vector2d( 0.125 , 0.0 ) , Eigen::Vector2d( 0.0 , 0.125 ) , 
										   fullTree );
		}else if( 1 ){
			
			/***/ sep( "Belhadj Planner" ); /**********************************************************************************************/
			
			ValidationParams params = validation_defaults();
			params.bel_minNumSeeds = 10;
			
			StopWatch algoTimer;
			
			AsmStateNode_ASP* fullTree = Belhadj2017_full_depth( testAsm , 
																 0.3 , 0.4 , 0.3 , 
																 0.8 , 10 ,
																 params );
			
			cout << "Full sequence completed in " << algoTimer.seconds_elapsed() << " seconds." << endl;
			
			visualize_tree_structure_plan( mrkrMngr , 
										   Eigen::Vector2d( 0 , 0 ) , Eigen::Vector2d( 0.125 , 0.0 ) , Eigen::Vector2d( 0.0 , 0.125 ) , 
										   fullTree );
			
		}else{
			
			/***/ sep( "SIR Planner" );  /*************************************************************************************************/
		
			ValidationParams params = validation_defaults();
			
			testAsm->set_pose( origin_pose() );
			Eigen::MatrixXd bb /* ----------- */ = testAsm->calc_AABB();
			Eigen::Vector3d crnr1 /* -------- */ = bb.row(0);
			Eigen::Vector3d crnr2 /* -------- */ = bb.row(1);
			double /* -- */ width /* -------- */ = ( crnr2 - crnr1 ).norm() / 2.0 ,
							interval /* ----- */ = 0.0025 ,
							stuckInfluenceFactor = 1.5 ;
			size_t /* -- */ numSteps /* ----- */ = (size_t) ceil( width/interval );
			
			StopWatch algoTimer;
			
			AsmStateNode_ASP* fullTree = sub_ID_blockage_reduction_full_depth( testAsm , 
																			   0.33 , 0.33 , 0.33 ,
																			   interval , numSteps ,
																			   stuckInfluenceFactor ,
																			   0.85 ,
																			   params );
																			   
			cout << "Full sequence completed in " << algoTimer.seconds_elapsed() << " seconds." << endl;
			
			visualize_tree_structure_plan( mrkrMngr , 
										   Eigen::Vector2d( 0 , 0 ) , Eigen::Vector2d( 0.125 , 0.0 ) , Eigen::Vector2d( 0.0 , 0.125 ) , 
										   fullTree );
			
		}
		
	}
	
	
	/// ~~~~ Toy Plane , Lee ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
	/// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
	
	if( 0 ){
		std::vector<llin> partial = {  0 ,  1 ,  2 , // Blocks
									   3 ,  4 ,  5 ,  6 , // Bar 7
									   7 ,  8 , // Bar 3
									   9 , 10 , // Long Bolts
									  11 , 12 , // Short Bolts
									  13 , 14 , // Wheels
									  15 , 16 }; // Axles (Short Bolts)
		Assembly_ASM* testAsm = toy_plane( partial );
		testAsm->set_marker_opacity( 0.75 );
		testAsm->load_part_mrkrs_into_mngr( mrkrMngr );
		
		testAsm->report_liaison_and_self_collision_state();
		
		if( 0 ){
		
			/***/ sep( "Morato Planner" ); /***********************************************************************************************/
			
			ValidationParams params = validation_defaults();
			
			StopWatch algoTimer;
			
			AsmStateNode_ASP* fullTree = Morato2013Modified_full_depth( testAsm , params );
			
			cout << "Full sequence completed in " << algoTimer.seconds_elapsed() << " seconds." << endl;
			
			visualize_tree_structure_plan( mrkrMngr , 
										   Eigen::Vector2d( 0 , 0 ) , Eigen::Vector2d( 0.125 , 0.0 ) , Eigen::Vector2d( 0.0 , 0.125 ) , 
										   fullTree );
		}else if( 0 ){
			
			/***/ sep( "Belhadj Planner" ); /**********************************************************************************************/
			
			ValidationParams params = validation_defaults();
			
			StopWatch algoTimer;
			
			AsmStateNode_ASP* fullTree = Belhadj2017_full_depth( testAsm , 
																 0.4 , 0.2 , 0.4 , 
																 0.8 , 10 ,
																 params );
			
			cout << "Full sequence completed in " << algoTimer.seconds_elapsed() << " seconds." << endl;
			
			visualize_tree_structure_plan( mrkrMngr , 
										   Eigen::Vector2d( 0 , 0 ) , Eigen::Vector2d( 0.125 , 0.0 ) , Eigen::Vector2d( 0.0 , 0.125 ) , 
										   fullTree );
			
		}else{
			
			/***/ sep( "SIR Planner" );  /*************************************************************************************************/
		
			ValidationParams params = validation_defaults();
			
			testAsm->set_pose( origin_pose() );
			Eigen::MatrixXd bb /* ----------- */ = testAsm->calc_AABB();
			Eigen::Vector3d crnr1 /* -------- */ = bb.row(0);
			Eigen::Vector3d crnr2 /* -------- */ = bb.row(1);
			double /* -- */ width /* -------- */ = ( crnr2 - crnr1 ).norm() / 2.0 ,
							interval /* ----- */ = 0.0025 ,
							stuckInfluenceFactor = 1.5 ;
			size_t /* -- */ numSteps /* ----- */ = (size_t) ceil( width/interval );
			
			StopWatch algoTimer;
			
			AsmStateNode_ASP* fullTree = sub_ID_blockage_reduction_full_depth( testAsm , 
																			   0.33 , 0.33 , 0.33 ,
																			   interval , numSteps ,
																			   stuckInfluenceFactor ,
																			   0.85 ,
																			   params );
																			   
			cout << "Full sequence completed in " << algoTimer.seconds_elapsed() << " seconds." << endl;
			
			visualize_tree_structure_plan( mrkrMngr , 
										   Eigen::Vector2d( 0 , 0 ) , Eigen::Vector2d( 0.125 , 0.0 ) , Eigen::Vector2d( 0.0 , 0.125 ) , 
										   fullTree );
			
		}
		
	}
	
		
	/// ___ End Preliminary ________________________________________________________________________________________________________________
	
	// N-2. Notify
	ros_log( "[" + NODE_NAME + "] Init OK and about to run ..." , INFO );
	
	cerr << "Before loop!" << endl;
	
	size_t LOOPLIMIT = 10 ,
		   counter   =  0 ;
	PlayerFrame*  currFrame = nullptr;	
	bool /* -- */ LOOP	    = true;
	PlanPlayer player{ 20.0 , &recording };
	
	// N. Main loop
	while( ros::ok() ){ // While neither node nor ROS has been shut down
		
		/// == NODE WORK ===================================================================================================================

		//~ cerr << "Loop ";

		// Request frame from renderer

		if( 0 ){ // DISABLE FOR STATIC DISPLAY
			// A. Fetch frame
			currFrame = player.current_frame( LOOP );
			// B. Render object markers
			visualization_msgs::MarkerArray& markerArr = currFrame->mrkrMngr.get_arr();
			
			
			// C. Render the joint state
			jntCmd_Call( currFrame->get_q() );
		}

		// 5. Paint frame
		vis_arr.publish( markerArr );
		
		//~ if( counter >= LOOPLIMIT ){  break;  }  counter++;
		//~ break; // ONLY ONCE
		
		/// __ END WORK ____________________________________________________________________________________________________________________
		
		ros::spinOnce(); // - Process messages
		heartbeat.sleep(); // Sleep for remainder of period
	}
	
	cerr << "Outside loop!" << endl;
	
	// N+1. Notify  &&  Exit
	
	ros_log( "[" + NODE_NAME + "] Exit OK, Goodbye!" , INFO );
	
	return 0; // I guess everything turned out alright at the end!
}

// ___ End main ____________________________________________________________________________________________________________________________


/* === Spare Parts =========================================================================================================================

if( 0 ){
	std::vector<llin> movdID = { 1 };	
	std::vector<llin> refcID = { 2 };
	Eigen::MatrixXd foundConstraints = testAsm->asm_get_constraints_on_movd( movdID , refcID );
	Eigen::Vector3d curDir;
	Eigen::Vector3d endPnt;
	double markLen = 0.125;
	for( size_t i = 0 ; i < foundConstraints.rows() ; i++ ){
		curDir = foundConstraints.row(i);
		mrkrMngr.add_marker(
			get_straightPath_marker( origin_vec() , curDir , 
									 0.002 , 209/225.0 , 50/225.0 , 23/225.0 , 1.0 )
		);
	}
}

   ___ End Spare ___________________________________________________________________________________________________________________________
*/
