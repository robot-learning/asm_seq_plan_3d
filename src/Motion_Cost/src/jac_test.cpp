/*
jac_test.cpp
James Watson , 2018 June

% ROS Node %
Send joint angles and get a (flattened) jacobian back
Publises To ----> :
Subscribes To <-- :

Dependencies: ROS , Cpp_Helpers , ROS_Helpers
Template Version: 2018-06-06
*/

// === Init ================================================================================================================================

// ~~~ Includes ~~~
// ~~ ROS ~~
#include <ros/ros.h> // --- ROS , Publishers , Subscribers
#include <ros/package.h> // Where are we? // http://wiki.ros.org/Packages#C.2B-.2B-
// ~ ROS Messages ~
// #include "PACKAGE_NAME/MSG_TYPE.h"
#include <sensor_msgs/JointState.h>
// ~~ Local ~~
#include <Cpp_Helpers.h> // C++ Utilities and Shortcuts
#include <ROS_Helpers.h> // ROS Utilities and Shortcuts
#include <AsmSeqPlan.h> //- Identify grasp pairs , ASP
#include <comm_ASM.h> // -- Message Processing

// ___ End Init ____________________________________________________________________________________________________________________________


/// ######### Node Vars #########

string NODE_NAME = "JacobianTest";
int    RATE_HZ   = 100;
int    QUEUE_LEN = 100;

/// ********* End Vars *********


// === Program Functions & Classes ===

// == Manipulability ==





// __ End Manip __


// == Path Length / Time ==





// __ End Time __

// ___ End Functions & Classes ___


// === Program Vars ===


// ___ End Vars ___


// === main ================================================================================================================================

int main( int argc , char** argv ){ // Main takes the terminal command and flags that called it as arguments
	srand( time( 0 ) ); // Random seed based on the current clock time
	
	/// === Preliminary { Setup , Instantiation , Planning  } ==============================================================================



	/// ___ End Preliminary ________________________________________________________________________________________________________________
	
	// 0. Init ROS  &&  Register node
	ros::init( argc , argv , NODE_NAME );
	
	// 1. Fetch handle to this node
	ros::NodeHandle nodeHandle;
	
	// 2. Init node rate
	ros::Rate heartbeat( RATE_HZ );
	
	// 3. Set up subscribers and publishers
	
	// ~ Publishers ~
	// ros::Publisher PUBLISHER_OBJ = nodeHandle.advertise<CATEGORY_MSGS::MSG_TYPE>( "TOPIC_NAME" , QUEUE_LEN );
	
	// ~ Subscribers ~
	// ros::Subscriber SUBSCRIBER_OBJ = nodeHandle.subscribe( "TOPIC_NAME" , QUEUE_LEN , CALLBACK_FUNCTION );
	
	// ~ Service Servers ~
	// ros::ServiceServer SERVER_OBJ = nodeHandle.advertiseService( "SERVICE_NAME" , SERV_CALLBACK );
	
	// ~ Service Clients ~
	// ros::ServiceClient CLIENT_OBJ = nodeHandle.serviceClient<CATEGORY::SERVICE_TYPE>( "ADVERTISED_NAME" );
	ros::ServiceClient J_Client = nodeHandle.serviceClient<ll4ma_teleop::Joints2Jac>( "/robot_commander/get_Jacobian" );
	
	/// == PRE-LOOP WORK ===================================================================================================================
		
	ll4ma_teleop::Joints2Jac srvMsg;
	
	std::vector<double> jnts1 = { 0 , 0 , 0 , 0 , 0 , 0 , 0 };
	srvMsg.request.JointPos = jnts1;
	if( J_Client.call( srvMsg ) ){
		ros_log( "Got a Jacobian response!" , INFO );
		cout << extract_Jacobian( srvMsg ) << endl;
		cout << "Manipulability Score: " << manip_score_Yoshikawa85( extract_Jacobian( srvMsg ) ) << endl;
		cout << "Response Data , # Joints: " << srvMsg.response.numJoints << " , # Elements: " << srvMsg.response.flatJac.size() 
			 << " , Expected: " << ( srvMsg.response.numJoints * 6 ) << " , OK?: " 
			 << ( srvMsg.response.numJoints * 6 == srvMsg.response.flatJac.size() ) << endl;
	}else{
		ros_log( "There was NO Jacobian response!" , WARN );
	}
	
	std::vector<double> jnts2 = { M_PI/2.0 , M_PI/2.0 , M_PI/2.0 , M_PI/2.0 , M_PI/2.0 , M_PI/2.0 , M_PI/2.0 };
	srvMsg.request.JointPos = jnts2;
	if( J_Client.call( srvMsg ) ){
		ros_log( "Got a Jacobian response!" , INFO );
		cout << extract_Jacobian( srvMsg ) << endl;
		cout << "Manipulability Score: " << manip_score_Yoshikawa85( extract_Jacobian( srvMsg ) ) << endl;
		cout << "Response Data , # Joints: " << srvMsg.response.numJoints << " , # Elements: " << srvMsg.response.flatJac.size() 
			 << " , Expected: " << ( srvMsg.response.numJoints * 6 ) << " , OK?: " 
			 << ( srvMsg.response.numJoints * 6 == srvMsg.response.flatJac.size() ) << endl;
	}else{
		ros_log( "There was NO Jacobian response!" , WARN );
	}
		
	/// __ END PRE-LOOP ____________________________________________________________________________________________________________________
	
	// N-1. Notify
	ros_log( "[" + NODE_NAME + "] Init OK and about to run ..." , INFO );
	
	// N. Main loop
	while( ros::ok() ){ // While neither node nor ROS has been shut down
		
		/// == NODE WORK ===================================================================================================================
		
		break; // NOT EVEN ONCE
		
		/// __ END WORK ____________________________________________________________________________________________________________________
		
		ros::spinOnce(); // - Process messages
		heartbeat.sleep(); // Sleep for remainder of period
	}
	
	// N+1. Notify  &&  Exit
	
	ros_log( "[" + NODE_NAME + "] Exit OK, Goodbye!" , INFO );
	
	return 0; // I guess everything turned out alright at the end!
}

// ___ End main ____________________________________________________________________________________________________________________________


/* === Spare Parts =========================================================================================================================



   ___ End Spare ___________________________________________________________________________________________________________________________
*/
