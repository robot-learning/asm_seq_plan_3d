/*
jiggle_test.cpp
James Watson , 2018 June

% ROS Node %
Test assembly problems for correctness
Publises To ----> :
Subscribes To <-- :

Dependencies: ROS , Cpp_Helpers , ROS_Helpers
Template Version: 2018-06-06
*/

// === Init ================================================================================================================================

// ~~~ Includes ~~~
// ~~ ROS ~~
#include <ros/ros.h> // -------------- ROS , Publishers , Subscribers
#include <ros/package.h> // ------------------ Where are we? // http://wiki.ros.org/Packages#C.2B-.2B-
// ~ ROS Messages ~
#include "motion_cost/qFromPose.h"
#include "motion_cost/IKrequest.h"
#include "motion_cost/IKresponse.h"
#include "geometry_msgs/Pose.h"
#include "sensor_msgs/JointState.h"
#include <ll4ma_teleop/Joints2Jac.h>
#include <ll4ma_teleop/LinkPoses.h>
#include <ll4ma_teleop/FngrPoses.h>
// ~~ Local ~~
#include <Cpp_Helpers.h> // - C++ Utilities and Shortcuts
#include <ROS_Helpers.h> // - ROS Utilities and Shortcuts
#include <Model_Factory.h> // Assemblies to plan on
#include <AsmSeqPlan.h> // -- Identify grasp pairs , ASP
#include <comm_ASM.h> // ---- Messages and visualization
#include <Motion_Planning.h>
#include <Methods.h>

// ___ End Init ____________________________________________________________________________________________________________________________


/// ######### Node Vars #########

string /* -- */ NODE_NAME  = "NDBG_Repair";
int /* ----- */ RATE_HZ    = 30;
int /* ----- */ QUEUE_LEN  = 30;
Eigen::Vector3d ROBOT_BASE = Eigen::Vector3d( 0.55 , 0 , 1.20 ); // FUTURE: NEED A WAY TO LOAD THIS FROM A PARAM SERVER

/// ********* End Vars *********


// === Program Functions & Classes ===

Assembly_ASM* bolt_test( std::vector<llin>& partIDs ){
    // A radio-looking thing that implies sub-assemblies in its design , Ability to select a subset of parts
    // NOTE: This function does not enforce connectivity
    
    bool SHOWDEBUG = false; // if( SHOWDEBUG ){  cout << "" << endl;  }
    
    if( SHOWDEBUG ){  cout << "Init ..." << endl;  }
    
    Incrementer assign_part_ID{ 0 }; // Instantiate a functor that will assign a unique number to each part
    Incrementer assign_asm_ID{ 0 }; //- Instantiate a functor that will assign a unique number to each assembly
	
	double SML_Z_CRCTN = 999.7 * EPSILON; // Small Z correction
	
	if( SHOWDEBUG ){  cout << "Using small Z correction: " << SML_Z_CRCTN << endl;  }
	
	if( SHOWDEBUG ){  cout << "Allocate ..." << endl;  }
	
	Assembly_ASM* testAsm = new Assembly_ASM( assign_asm_ID() );
	
	if( SHOWDEBUG ){  cout << "Build paths ..." << endl;  }
	
	
	// ~~ 0. Create file paths ~~
	
	// ~ Fetch the folder to the STL files ~
	string pkgPath = ros::package::getPath( "motion_cost" );
	string resourcePath = pkgPath + "/Resources/Assembly_CAD/CylTest/";
	
	// ~ Create full paths to STLs ~
	std::vector<string> fNames;
/* 0*/ fNames.push_back( resourcePath + "Base_SCL.stl" );      		Eigen::Vector3d base_shift{  0.001077 ,  0.001077 , -0.005    };
/* 1*/ fNames.push_back( resourcePath + "BoltWideTight_SCL.stl" );  Eigen::Vector3d BWT_shift{   0.0      ,  0.0      , -0.016801 };
/* 2*/ fNames.push_back( resourcePath + "BoltWideLoose_SCL.stl" );  Eigen::Vector3d BWL_shift{   0.005    , -0.0175   , -0.017592 };
/* 3*/ fNames.push_back( resourcePath + "BoltThinTight_SCL.stl" );  Eigen::Vector3d BTT_shift{   0.0      ,  0.0      , -0.014683 };
/* 4*/ fNames.push_back( resourcePath + "BoltThinLoose_SCL.stl" );  Eigen::Vector3d BTL_shift{   0.0      ,  0.0      , -0.014925 };

    if( SHOWDEBUG ){  cout << "Create parts ..." << endl;  }

	// ~ Create parts ~
	size_t fLen = fNames.size();
	std::vector<Part_ASM> testParts;
	for( size_t i = 0 ; i < fLen ; i++ ){
		if( SHOWDEBUG ){  cout << fNames[i];  }
		testParts.push_back( Part_ASM{   assign_part_ID() , fNames[i]   } );
		if( SHOWDEBUG ){  cout << "\tPart created!" << endl;  }
	}
	if( SHOWDEBUG ){  cout << "Added " << testParts.size() << " parts." << endl;  }
	
	// 2. Create an assembly , add parts
	
	

	
if( is_arg_in_vector( (llin)0 , partIDs ) ){
/* 0*/ testAsm->add_part_w_pose( testParts[ 0] , Pose_ASP{ -base_shift , no_turn_quat() } ); // Base
}
if( is_arg_in_vector( (llin)1 , partIDs ) ){
/* 1*/ testAsm->add_part_w_pose( testParts[ 1] , Pose_ASP{ -BWT_shift + Eigen::Vector3d(  0.005 ,  0.005  , -0.000 ) , 
															no_turn_quat() } ); // Bolt Wide Tight
															//~ Eigen::AngleAxisd( M_PI / 52.0 , Eigen::Vector3d::UnitZ() ) * no_turn_quat() } ); // Bolt Wide Tight
}
if( is_arg_in_vector( (llin)2 , partIDs ) ){
/* 2*/ testAsm->add_part_w_pose( testParts[ 2] , Pose_ASP{ -BWL_shift + Eigen::Vector3d(  0.010 , -0.0125 , -0.000 ) , 
															no_turn_quat() } ); // Bolt Wide Loose
															//~ Eigen::AngleAxisd( M_PI / 52.0 , Eigen::Vector3d::UnitZ() ) * no_turn_quat() } ); // Bolt Wide Loose
}
if( is_arg_in_vector( (llin)3 , partIDs ) ){
/* 3*/ testAsm->add_part_w_pose( testParts[ 3] , Pose_ASP{ -BTT_shift + Eigen::Vector3d( -0.010 , -0.010  , -0.000 ) , 
															no_turn_quat() } ); // Bolt Thin Tight
															//~ Eigen::AngleAxisd( M_PI / 52.0 , Eigen::Vector3d::UnitZ() ) * no_turn_quat() } ); // Bolt Thin Tight
}
if( is_arg_in_vector( (llin)4 , partIDs ) ){
/* 4*/ testAsm->add_part_w_pose( testParts[ 4] , Pose_ASP{ -BTL_shift + Eigen::Vector3d( -0.010 , -0.010  , -0.000 ) , 
															no_turn_quat() } ); // Bolt Thin Loose
															//~ Eigen::AngleAxisd( M_PI / 52.0 , Eigen::Vector3d::UnitZ() ) * no_turn_quat() } ); // Bolt Thin Loose
}

    if( SHOWDEBUG ){  cout << "Recalc geo ..." << endl;  }

	// 3. Determine the local freedom of each part (NDBG)
	testAsm->recalc_geo();
		
	if( SHOWDEBUG ){  cout << "Return ..." << endl;  }
	
	return testAsm;
}

// ___ End Functions & Classes ___


// === Program Vars ===

bool CONNECT_TO_ARM = true;
//~ PlanRecording recording;

// ___ End Vars ___


// === main ================================================================================================================================

int main( int argc , char** argv ){ // Main takes the terminal command and flags that called it as arguments
	srand( time( 0 ) ); // Random seed based on the current clock time
	
	// 0. Init ROS  &&  Register node
	ros::init( argc , argv , NODE_NAME );
	
	// 1. Fetch handle to this node
	ros::NodeHandle nodeHandle;
	
	// 2. Init node rate
	ros::Rate heartbeat( RATE_HZ );
	
	// 3. Set up subscribers and publishers
	
	// ~ Publishers ~
	//~ ros::Publisher joint_cmd_pub = nodeHandle.advertise<sensor_msgs::JointState>( "/lbr4_teleop/joint_cmd" , QUEUE_LEN );
	//~ send_joint_command jntCmd_Call{ joint_cmd_pub };
	
	ros::Publisher vis_arr       = nodeHandle.advertise<visualization_msgs::MarkerArray>( "visualization_marker_array" , QUEUE_LEN );
	
	// ~ Subscribers ~
	// ros::Subscriber SUBSCRIBER_OBJ = nodeHandle.subscribe( "TOPIC_NAME" , QUEUE_LEN , CALLBACK_FUNCTION );
	
	// ~ Service Servers ~
	// ros::ServiceServer SERVER_OBJ = nodeHandle.advertiseService( "SERVICE_NAME" , SERV_CALLBACK );
	
	// ~ Service Clients ~
	
	//~ ros::ServiceClient  IK_Client = nodeHandle.serviceClient<motion_cost::qFromPose>( "q_from_pose" );
	//~ request_IK_solution IK_clientCall{ IK_Client };
	
	//~ ros::ServiceClient  J_Client = nodeHandle.serviceClient<ll4ma_teleop::Joints2Jac>( "/robot_commander/get_Jacobian" );
	//~ request_Jacobian    J_ClientCall{ J_Client };
	
	//~ ros::ServiceClient  FK_Client = nodeHandle.serviceClient<ll4ma_teleop::LinkPoses>( "/robot_commander/get_links_FK" );
	//~ request_FK_poses    FK_ClientCall{ FK_Client };  // { 1 , 2 , 3 , 4 , 5 , 6 , 7 , Grasp Target }

	//~ ros::ServiceClient  FNG_Client = nodeHandle.serviceClient<ll4ma_teleop::FngrPoses>( "/robot_commander/get_fingr_FK" );
	//~ request_FNG_poses   FNG_ClientCall{ FNG_Client };
	
	
	// N-1. Animation Init 
	srand( ( time( 0 ) % 1 ) * 1000.0 + getpid() ); // Random seed based on the current clock time
	RViz_MarkerManager mrkrMngr{};
	visualization_msgs::MarkerArray& markerArr = mrkrMngr.get_arr();

	// ~ Rviz Axes ~
	if(1){
		mrkrMngr.add_marker( get_axes_marker( 0.5 , 0.0025 ) );
	}
	
	/// === Preliminary { Setup , Instantiation , Planning  } ==============================================================================

	if( 1 ){
		std::vector<llin> allParts = { 0 , 4 };
		Assembly_ASM* boltAsm = bolt_test( allParts );
		boltAsm->set_pose( origin_pose() );
		boltAsm->set_marker_opacity( 0.65 );
		boltAsm->load_part_mrkrs_into_mngr( mrkrMngr );
		
		cout << "About to enhance ...";
		boltAsm->enhance_NDBG( 0.0015 );
		cout << "Complete!" << endl;
		boltAsm->populate_liaison_graph_from_NDBG();
		
		boltAsm->report_liaison_and_self_collision_state();
		
		std::vector<size_t> movdDex = { 1 };	
		std::vector<size_t> refcDex = { 0 };
		Eigen::MatrixXd foundConstraints = boltAsm->asm_get_constraints_on_movd( movdDex , refcDex );
		Eigen::Vector3d curDir;
		Eigen::Vector3d endPnt;
		double markLen = 0.25;
		
		for( size_t i = 0 ; i < foundConstraints.rows() ; i++ ){
			curDir = foundConstraints.row(i);
			mrkrMngr.add_marker(
				get_straightPath_marker( origin_vec() , curDir , 
										 0.002 , 209/225.0 , 50/225.0 , 23/225.0 , 1.0 )
			);
		}
	}else{
	
		Assembly_ASM* testAsm = simple_phone();
		testAsm->set_pose( origin_pose() );
		testAsm->set_marker_opacity( 0.65 );
		testAsm->load_part_mrkrs_into_mngr( mrkrMngr );
		
		cout << "About to enhance ...";
		testAsm->enhance_NDBG( 0.0015 );
		cout << "Complete!" << endl;
		testAsm->populate_liaison_graph_from_NDBG();
			
		testAsm->report_liaison_and_self_collision_state();
		
	}
		
	/// ___ End Preliminary ________________________________________________________________________________________________________________
	
	// N-2. Notify
	ros_log( "[" + NODE_NAME + "] Init OK and about to run ..." , INFO );
	
	cerr << "Before loop!" << endl;
	size_t loopCounter =  0 ,
		   LOOPLIMIT   = 10 ;
	
	// N. Main loop
	while( ros::ok() ){ // While neither node nor ROS has been shut down
		
		/// == NODE WORK ===================================================================================================================

		//~ cerr << "Loop ";

		// Request frame from renderer

		// A. Fetch frame
		//~ currFrame = player.current_frame( LOOP );
		// B. Render object markers
		//~ visualization_msgs::MarkerArray& markerArr = currFrame->mrkrMngr.get_arr();
		
		
		// COMING SOON PHASE 3
		//~ // C. Render the joint state
		//~ jntCmd_Call( currFrame->get_q() );

		// 5. Paint frame
		vis_arr.publish( markerArr );
		
		if( loopCounter >= LOOPLIMIT ){  break;  }  loopCounter++;
		//~ break; // ONLY ONCE
		
		/// __ END WORK ____________________________________________________________________________________________________________________
		
		ros::spinOnce(); // - Process messages
		heartbeat.sleep(); // Sleep for remainder of period
	}
	
	cerr << "Outside loop!" << endl;
	
	// N+1. Notify  &&  Exit
	
	ros_log( "[" + NODE_NAME + "] Exit OK, Goodbye!" , INFO );
	
	return 0; // I guess everything turned out alright at the end!
}

// ___ End main ____________________________________________________________________________________________________________________________


/* === Spare Parts =========================================================================================================================



   ___ End Spare ___________________________________________________________________________________________________________________________
*/
