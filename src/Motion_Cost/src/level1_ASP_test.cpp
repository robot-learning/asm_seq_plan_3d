/*
level1_ASP_test.cpp
James Watson , 2018 June

% ROS Node %
A ONE-LINE DESCRIPTION OF THE NODE
Publises To ----> :
Subscribes To <-- :

Dependencies: ROS , Cpp_Helpers , ROS_Helpers
Template Version: 2018-06-06
*/

// === Init ================================================================================================================================

// ~~~ Includes ~~~
// ~~ ROS ~~
#include <ros/ros.h> // -------------- ROS , Publishers , Subscribers
// ~ ROS Messages ~
#include "motion_cost/qFromPose.h"
#include "motion_cost/IKrequest.h"
#include "motion_cost/IKresponse.h"
#include "geometry_msgs/Pose.h"
#include "sensor_msgs/JointState.h"
// ~~ Local ~~
#include <Cpp_Helpers.h> // - C++ Utilities and Shortcuts
#include <ROS_Helpers.h> // - ROS Utilities and Shortcuts
#include <Model_Factory.h> // Assemblies to plan on
#include <AsmSeqPlan.h> // -- Identify grasp pairs , ASP

// ___ End Init ____________________________________________________________________________________________________________________________


/// ######### Node Vars #########

string NODE_NAME = "Replay_Test";
int    RATE_HZ   = 100;
int    QUEUE_LEN = 100;

std::vector<string> ALL_JOINT_NAMES = { "lbr4_j0" , "lbr4_j1" , "lbr4_j2" , "lbr4_j3" , "lbr4_j4" , "lbr4_j5" , "lbr4_j6" , 
										"r_gripper_l_finger_joint" , "r_gripper_r_finger_joint" };
size_t NUM_ALL_JOINTS = ALL_JOINT_NAMES.size();

/// ********* End Vars *********


// === Program Functions & Classes ===

void pack_joint_and_gripper_states_into_msg( std::vector<double>& armState , // ---- Joint positions of arm
											 std::vector<double>& fingState , // --- Joint positions of gripper
											 sensor_msgs::JointState& stateMsg ){ // Load state data here
	// Load arm joint and gripper joint states into the message parameter
	std::vector<double> totalPos = vec_copy( armState );
	extend_vec_with( totalPos , fingState );
	stateMsg.name     = vec_copy( ALL_JOINT_NAMES );
	stateMsg.position = totalPos;
}

void display_sequence_without_animation( AsmStateNode_ASP* rootNode , RViz_MarkerManager& nodeMrkrMngr ,
										 Eigen::Vector2d planStart , Eigen::Vector2d actionIncr , Eigen::Vector2d remvOffset ){
	// Diagnose a disassembly sequence for a single level
	
	bool SHOWDEBUG = true;
	
	Eigen::Vector2d   planPoint = planStart;
	AsmStateNode_ASP* currNode  = rootNode;
	Assembly_ASM*     stateAsm;
	Assembly_ASM*     removAsm;
	size_t stateSup = 0 ,
		   removSup = 0 ,
		   count    = 0 ;
	Pose_ASP stateSave;
	Pose_ASP removSave;
	
	// 1. While the state is not null
	while( currNode ){
		stateAsm = currNode->assembly;  stateSup = stateAsm->get_support();  stateSave = stateAsm->get_pose();
		removAsm = currNode->removed;   
		if( removAsm ){  removSup = removAsm->get_support();  removSave = removAsm->get_pose();  }
		// 2. Set the assembly down on its support
		stateAsm->set_lab_pose_for_support_index( stateSup , planPoint );
		// 3. Set the removed down on its support, offset
		if( removAsm )
			removAsm->set_lab_pose_for_support_index( removSup , planPoint + remvOffset );
		// 4. Load markers
		stateAsm->load_part_mrkrs_into_mngr( nodeMrkrMngr );
		if( removAsm )
			removAsm->load_part_mrkrs_into_mngr( nodeMrkrMngr );
		// 5. Advance setdown location
		planPoint += actionIncr;
		// 6. Advance node
		if( SHOWDEBUG ){  cout << "About to advance to the next node ..." << endl;  }
		currNode = currNode->get_next();
		count++;
	}
	cout << "Processed " << count << " nodes." << endl;
}

void display_sequence_without_animation_ALTERNATE( AsmStateNode_ASP* rootNode , RViz_MarkerManager& nodeMrkrMngr ,
												   Eigen::Vector2d planStart , Eigen::Vector2d actionIncr , Eigen::Vector2d remvOffset ){
	// Diagnose a disassembly sequence for a single level
	
	bool SHOWDEBUG = true;
	
	Eigen::Vector2d   planPoint = planStart;
	Eigen::Vector3d   ofstPoint;
	AsmStateNode_ASP* currNode  = rootNode;
	Assembly_ASM*     stateAsm;
	Assembly_ASM*     stateLast = nullptr;
	Assembly_ASM*     removAsm;
	Assembly_ASM*     removLast = nullptr;
	size_t stateSup = 0 ,
		   removSup = 0 ,
		   count    = 0 ;
	Pose_ASP stateSave;
	Pose_ASP removSave;
	Pose_ASP offstPose;
	
	// 1. While the state is not null
	while( currNode ){
		stateAsm = currNode->assembly;  stateSup = stateAsm->get_support();  stateSave = stateAsm->get_pose();
		removAsm = currNode->removed;   
		size_t trajLen = currNode->seqPoses.size();
		
		if( SHOWDEBUG ){  
			cout << "Found a state assembly: _ " << ( stateAsm ? "Yes" : "No" ) << endl;  
			cout << "Found a removed assembly: " << ( removAsm ? "Yes" : "No" ) << endl;  
			cout << "Trajectory Length: ______ " << trajLen << endl;
		}
		
		// 2. For each REMOVE action
		if( removAsm ){
			
			if( trajLen > 0 ){
			
				// A. Place ASM in the specified pose , Offset according to the step in sequence
				stateSave = stateAsm->get_pose();
				ofstPoint = Eigen::Vector3d( (double) planPoint(0) , (double) planPoint(1) , 0 );
				stateAsm->set_pose(
					pose_from_position_orient( 
						stateSave.position + ofstPoint, 
						stateSave.orientation 
					)
				);
				stateAsm->load_part_mrkrs_into_mngr( nodeMrkrMngr );
				stateAsm->set_pose( stateSave );
				// B. Place the removed sub at the end of its trajectory , Offset according to the step in sequence
				size_t last_i = trajLen - 1;
				
				if( SHOWDEBUG ){  cout << "The last pose in the sequence is " << currNode->seqPoses[ last_i ] << endl;  }
				
				removAsm->set_pose( currNode->seqPoses[ last_i ] ); // don't have to reset this
				removAsm->load_part_mrkrs_into_mngr( nodeMrkrMngr );
				
			}else if( SHOWDEBUG ){  
				cout << "ERROR: Remove operation without a trajectory!" << endl;
			}
		// 3. For each ROTATE action	
		}else{
			// A. Put initial pose in line
			if( stateLast ){
				stateSave = stateLast->get_pose();
				ofstPoint = Eigen::Vector3d( (double) planPoint(0) , (double) planPoint(1) , 0 );
				stateLast->set_pose(
					pose_from_position_orient( 
						stateSave.position + ofstPoint, 
						stateSave.orientation 
					)
				);
				stateLast->load_part_mrkrs_into_mngr( nodeMrkrMngr );
				stateLast->set_pose( stateSave );
			}
			// B. Put final pose in offset
			stateSave = stateAsm->get_pose();
			ofstPoint = Eigen::Vector3d( ( (double) planPoint(0) + (double) remvOffset(0) ) , ( (double) planPoint(1) + (double) remvOffset(1) ) , 0 );
			stateAsm->set_pose(
				pose_from_position_orient( 
					stateSave.position + ofstPoint, 
					stateSave.orientation 
				)
			);
			stateAsm->load_part_mrkrs_into_mngr( nodeMrkrMngr );
			stateAsm->set_pose( stateSave );
		}
		
		stateLast = stateAsm;
		removLast = removAsm;
		
		// X. Advance offset location
		planPoint += actionIncr;
		
		if( SHOWDEBUG ){  cout << "About to advance to the next node ..." << endl;  }
		currNode = currNode->get_next();
		count++;
		
		if( SHOWDEBUG ){  cout << endl;  }
	}
	if( SHOWDEBUG ){  cout << "Processed " << count << " nodes." << endl;  }
	
}

PlayerFrame spam_frame(){
	// Return a frame that is just cubes
	PlayerFrame rtnFrame;
	rtnFrame.mrkrMngr.spam_cubes();
	return rtnFrame;
}

// ___ End Functions & Classes ___


// === Program Vars ===

bool simViz    = true  , 
	 staticViz = false ;

// ___ End Vars ___


// === main ================================================================================================================================

int main( int argc , char** argv ){ // Main takes the terminal command and flags that called it as arguments
	srand( time( 0 ) ); // Random seed based on the current clock time
	
	// 0. Init ROS  &&  Register node
	ros::init( argc , argv , NODE_NAME );
	
	// 1. Fetch handle to this node
	ros::NodeHandle nodeHandle;
	
	// 2. Init node rate
	ros::Rate heartbeat( RATE_HZ );
	
	// 3. Set up subscribers and publishers
	
	// ~ Publishers ~
	ros::Publisher joint_cmd_pub = nodeHandle.advertise<sensor_msgs::JointState>( "/lbr4_teleop/joint_cmd" , QUEUE_LEN );
	ros::Publisher vis_arr       = nodeHandle.advertise<visualization_msgs::MarkerArray>( "visualization_marker_array" , QUEUE_LEN );
	
	// ~ Subscribers ~
	// ros::Subscriber SUBSCRIBER_OBJ = nodeHandle.subscribe( "TOPIC_NAME" , QUEUE_LEN , CALLBACK_FUNCTION );
	
	// ~ Service Servers ~
	// ros::ServiceServer SERVER_OBJ = nodeHandle.advertiseService( "SERVICE_NAME" , SERV_CALLBACK );
	
	// ~ Service Clients ~
	if( simViz )
		ros::ServiceClient IK_Client = nodeHandle.serviceClient<motion_cost::qFromPose>( "q_from_pose" );
	
	/// === Preliminary { Setup , Instantiation , Planning  } ==============================================================================

	// ~~ I. Planning ~~

	//  0. Init vars
	
	size_t befSupport =   0 , 
		   numSteps   = 100 ;
	
	double removeDist =  0.070 , 
		   floorXWidth = 1.000 ,
		   floorYLngth = 1.000 ,
		   gripMaxSep  = 0.080 ; // 8cm max separation
	//~ bool LOOP = true;
	
	//~ Eigen::Vector3d floorCenter{ 0.795 , 0.0 , 0.59 }; // Table center
	Eigen::Vector3d floorCenter{ 0.9 , 0.0 , 0.59 }; // Table center

	bool PROCESSCUBE = false;
	
	if( PROCESSCUBE ){
		Assembly_ASM* simpleCube  = simple_cube();
		AsmStateNode_ASP* cubeRoot = Belhadj2017_full_depth( simpleCube , 
														 0.4 , 0.2 , 0.4 , 
														 0.8 , 10 );
		AsmStateNode_ASP* cubeLevelSeq;
		if( true ){
			cubeLevelSeq = precedence_layer_root_to_action_sequence( cubeRoot , floorCenter , 
																	 removeDist , numSteps ,
																	 floorXWidth , floorYLngth );
		}else{
			cubeLevelSeq = precedence_layer_root_to_action_sequence_ALTERNATE( cubeRoot , floorCenter , 
																			   removeDist , numSteps ,
																			   floorXWidth , floorYLngth );
		}
		sep( "Look at level action sequence: Cube" );
		inspect_level_ASP( cubeLevelSeq );
	}

	// ~~~ SIMPLE PHONE PLANNING ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

	//  1. Instantiate ASM
	
	Assembly_ASM* simplePhone = simple_phone();
	
	//  2. Disassemble
	AsmStateNode_ASP* phoneRoot = Belhadj2017_full_depth( simplePhone , 
														  0.4 , 0.2 , 0.4 , 
														  0.8 , 10 );
										
	// ~~ AA. Layer Demo ~~
	AsmStateNode_ASP* phoneLevelSeq;
	if( false ){
		phoneLevelSeq = precedence_layer_root_to_action_sequence( phoneRoot , floorCenter , 
																  removeDist , numSteps ,
																  floorXWidth , floorYLngth );
		sep( "Look at level action sequence: Phone" );
		inspect_level_ASP( phoneLevelSeq );
	}else if( false ){
		phoneLevelSeq = precedence_layer_root_to_action_sequence_ALTERNATE( phoneRoot , floorCenter , 
																			removeDist , numSteps ,
																			floorXWidth , floorYLngth );
																			
		sep( "Look at level action sequence: Phone" );
		inspect_level_ASP( phoneLevelSeq );
	}else if( false ){
		sep( "Planning ONE Action Only: Phone" );
		size_t edgeToPlan 
					//~ = 0 , // Edge 0: Base and antenna , Probably better as a base than an asm, also trapped on support 0
					= 1 , // Edge 1: Remove OK!
					//~ = 2 , // Edge 2: Sub collides with other parts, There is precedence within the layer! , 
							  // URL : https://bitbucket.org/robot-learning/asm_seq_plan_3d/issues/1/precedence-within-belhadj-layer
			   initSupprt = 0 ;
		//~ phoneLevelSeq = precedence_layer_root_plan_one_action( phoneRoot , floorCenter , 
															   //~ edgeToPlan , initSupprt ,
														       //~ removeDist , numSteps ,
														       //~ floorXWidth , floorYLngth );
														     
		Assembly_ASM* totalBeforeAction = phoneRoot->assembly;
		Assembly_ASM* removedSub        = (*phoneRoot)[ edgeToPlan ]->assembly;
														       
		AsmStateSuccessCode stateCode = plan_straightline_REMOVE_action( totalBeforeAction , removedSub , 
																		 floorCenter , initSupprt ,
																		 removeDist , numSteps ,
																		 floorXWidth , floorYLngth );
		cout << "Attempted straightline action plan , Success?: " << stateCode.creationDetail.success << endl
			 << "\tCode: " << stateCode.creationDetail.code << " meaning " << stateCode.creationDetail.desc << endl;
			 
		phoneLevelSeq = stateCode.stateNode;
		
		sep( "Look at level action sequence: Phone" );
		inspect_level_ASP( phoneLevelSeq );
		
	}else{
		AsmStateSuccessCode aspCode = precedence_layer_ASP_simple_straightline( phoneRoot , floorCenter , 
																				removeDist , numSteps ,
																				floorXWidth , floorYLngth );
																				
		cout << "Attempted disasm sequence plan , Success?: " << aspCode.creationDetail.success << endl
			 << "\tCode: " << aspCode.creationDetail.code << " meaning " << aspCode.creationDetail.desc << endl << endl;
			 
		sep( "Look at level action sequence: Phone" );
		inspect_level_ASP( aspCode.stateNode );
		phoneLevelSeq = aspCode.stateNode;
	}
	
										
	// ... END PHONE ASP ...................................................................................................................
	
	
	// ~~ II. Sequence Display ~~
	
	RViz_MarkerManager mrkrMngr;
	
	double rotationDwellTime =  2.0; // 2018-06-15: Dwell time to hold rotation , There is not yet a planner for this
	double playerFramePerSec = 30.0;   
	size_t numRotDwellFrames = round( rotationDwellTime * playerFramePerSec );
	std::vector<double> zeroArm = { 0.0 , 0.0 , 0.0 , 0.0 , 0.0 , 0.0 , 0.0 };
	std::vector<double> zeroFng = { 0.0 , 0.0 };
	
	sep( "Animate Phone Level 1 Sequence" );
	
	PlanRecording record;
	
	AsmStateNode_ASP* currNode = phoneLevelSeq;
	
	size_t count = 0;
	
	if( simViz ){
		
		bool VERBOSE = false;
	
		//  1. For every action
		while( currNode ){
			//  A. Determine if there is an action from the last state
			if( currNode->count_edges() > 0 ){
				
				cout << "There is an action to process ..." << endl;
				
				//  2. Determine whether it is a rotate or a remove action  &&  3. If it is a remove action
				if( currNode->edgeActions[0].action == ACT_REMOVE ){
					
					cout << "Rendering a REMOVE action ..." << endl;
					
					currNode = (*currNode)[0]; // Advance to the next state
					
					cout << "\tCurrent assembly contains: " << currNode->assembly->get_part_ID_vec() << endl;
					cout << "\tRemoved sub contains ___ : " << currNode->removed->get_part_ID_vec()  << endl;
					
					//  4. For each pose in sequence
					for( size_t i = 0 ; i < currNode->seqPoses.size() ; i++ ){
						PlayerFrame frame; // Instantiate frame 
						//  5. Place the removed sub in the pose
						currNode->removed->set_pose( currNode->seqPoses[i] );
						//  6. Load the removed sub markers
						currNode->removed->load_part_mrkrs_into_mngr( frame.mrkrMngr );
						//  7. Load the current sub markers (these are static)
						currNode->assembly->set_lab_pose_for_index_3D( currNode->assembly->get_support() , floorCenter );
						currNode->assembly->load_part_mrkrs_into_mngr( frame.mrkrMngr );
						//  8. Load zero joint angles
						sensor_msgs::JointState temp;
						pack_joint_and_gripper_states_into_msg( zeroArm , // Joint positions of arm
																zeroFng , // Joint positions of gripper
																temp ); // - Load state data here
						record.push_back( frame );
					}
					
					record.push_back( spam_frame() ); // Overwrite extra markers with a spam frame
					
				//  9. If it is a rotate action
				}else if( currNode->edgeActions[0].action == ACT_ROTATE ){
					
					cout << "Rendering a ROTATE action ..." << endl;
					
					currNode = (*currNode)[0]; // Advance to the next state
					
					cout << "\tCurrent assembly contains: " << currNode->assembly->get_part_ID_vec() << endl;
					
					cout << "Generating frames ..." << endl;
					
					// 10. For the number of dwell frames
					for( size_t j = 0 ; j < numRotDwellFrames ; j++ ){
						
						if( VERBOSE ){  cout << "Frame " << j+1 << " of " << numRotDwellFrames << " ..." << endl;  }
						
						PlayerFrame frame; // Instantiate frame 
						// 11. Load the markers from the rotated sub (these are static)
						currNode->assembly->set_lab_pose_for_index_3D( currNode->assembly->get_support() , floorCenter );
						currNode->assembly->load_part_mrkrs_into_mngr( frame.mrkrMngr );
						// 12. Load zero joint angles
						sensor_msgs::JointState temp;
						pack_joint_and_gripper_states_into_msg( zeroArm , // Joint positions of arm
																zeroFng , // ------------ Joint positions of gripper
																temp ); // -------------- Load state data here
						record.push_back( frame );
					}
					
					if( VERBOSE ){  cout << "Frames generated! Outout a spam frame ..." << endl;  }
					
					record.push_back( spam_frame() ); // Overwrite extra markers with a spam frame
					
					if( VERBOSE ){  cout << "Cubes were spammed!" << endl;  }
					
				}else{  cout << "SEQUENCE ERROR: Undefined action!" << endl;  }
			}else{  
				cout << "SEQUENCE END" << endl;
				currNode = nullptr;  
			}
			count++;
		}
		
	}else if( staticViz ){
		
		Eigen::Vector2d planStart{ 0 , 0 };
		Eigen::Vector2d actionIncr{ 0.4 , 0.0 };
		Eigen::Vector2d remvOffset{ 0.0 , 0.4 };
		
		cout << "About to place diagnostic models ..." << endl;
		
		if( false ){
			display_sequence_without_animation( phoneLevelSeq , mrkrMngr ,
												planStart , actionIncr , remvOffset );
		}else{
			display_sequence_without_animation_ALTERNATE( phoneLevelSeq , mrkrMngr ,
														  planStart , actionIncr , remvOffset );
		}
		cout << "Diagnostic models placed!" << endl;
	}else{
		cout << "Graphics have been disabled for this test." << endl;
	}
	
	
	
	// 12. Instantiate playback (start timer)  &&  Load recording into playback
	PlanPlayer player{ playerFramePerSec , record };
	PlayerFrame* currFrame = nullptr;
	bool LOOP = true;
	

	/// ___ End Preliminary ________________________________________________________________________________________________________________

	// N-1. Notify
	ros_log( "[" + NODE_NAME + "] Init OK and about to run ..." , INFO );
	
	// N. Main loop
	while( ros::ok() ){ // While neither node nor ROS has been shut down
		
		/// == NODE WORK ===================================================================================================================
		
		size_t waitCount = 0  , 
			   waitLimit = 10 ;
		
		// Wait for a subscriber to start looking for this topic
		while (  ( vis_arr.getNumSubscribers() < 1 )  &&  ( waitCount <= waitLimit )  ){
			if ( !ros::ok() ){ return 0; }
			printf( "." );
			ROS_WARN_ONCE( "Please create a subscriber to the marker" );
			sleep( 1 );
			cout << ".";
			waitCount++;
			if( !( simViz || staticViz ) ){  break;  } // Skip rendering if no rendering mode is set
		} 
        
        
        if( simViz ){
			// A. Fetch frame
			currFrame = player.current_frame( LOOP );
			// B. Render object markers
			visualization_msgs::MarkerArray& markerArr = currFrame->mrkrMngr.get_arr();
			vis_arr.publish( markerArr );
			// C. Render the joint state
			joint_cmd_pub.publish( currFrame->jnt_state_ );
		}else if( staticViz ){
			visualization_msgs::MarkerArray& markerArr = mrkrMngr.get_arr();
			vis_arr.publish( markerArr );
		}else{
			break;
		}
		
		/// __ END WORK ____________________________________________________________________________________________________________________
		
		ros::spinOnce(); // - Process messages
		heartbeat.sleep(); // Sleep for remainder of period
	}
	
	// N+1. Notify  &&  Exit
	
	ros_log( "[" + NODE_NAME + "] Exit OK, Goodbye!" , INFO );
	
	return 0; // I guess everything turned out alright at the end!
}

// ___ End main ____________________________________________________________________________________________________________________________


/* === Spare Parts =========================================================================================================================



   ___ End Spare ___________________________________________________________________________________________________________________________
*/
