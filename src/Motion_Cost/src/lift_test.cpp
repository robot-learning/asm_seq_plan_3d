/*
lift_test.cpp
James Watson , 2018 June

% ROS Node %
A ONE-LINE DESCRIPTION OF THE NODE
Publises To ----> :
Subscribes To <-- :

Dependencies: ROS , Cpp_Helpers , ROS_Helpers
Template Version: 2018-06-06
*/

// === Init ================================================================================================================================

// ~~~ Includes ~~~
// ~~ ROS ~~
#include <ros/ros.h> // -------------- ROS , Publishers , Subscribers
// ~ ROS Messages ~
#include "motion_cost/qFromPose.h"
#include "motion_cost/IKrequest.h"
#include "motion_cost/IKresponse.h"
#include "geometry_msgs/Pose.h"
#include "sensor_msgs/JointState.h"
// ~~ Local ~~
#include <Cpp_Helpers.h> // - C++ Utilities and Shortcuts
#include <ROS_Helpers.h> // - ROS Utilities and Shortcuts
#include <Model_Factory.h> // Assemblies to plan on
#include <AsmSeqPlan.h> // -- Identify grasp pairs , ASP

// ___ End Init ____________________________________________________________________________________________________________________________


/// ######### Node Vars #########

string NODE_NAME = "Replay_Test";
int    RATE_HZ   = 100;
int    QUEUE_LEN = 100;

std::vector<string> ALL_JOINT_NAMES = { "lbr4_j0" , "lbr4_j1" , "lbr4_j2" , "lbr4_j3" , "lbr4_j4" , "lbr4_j5" , "lbr4_j6" , 
										"r_gripper_l_finger_joint" , "r_gripper_r_finger_joint" };
size_t NUM_ALL_JOINTS = ALL_JOINT_NAMES.size();

/// ********* End Vars *********


// === Program Functions & Classes ===

void pack_joint_and_gripper_states_into_msg( std::vector<double>& armState , // ---- Joint positions of arm
											 std::vector<double>& fingState , // --- Joint positions of gripper
											 sensor_msgs::JointState& stateMsg ){ // Load state data here
	// Load arm joint and gripper joint states into the message parameter
	std::vector<double> totalPos = vec_copy( armState );
	extend_vec_with( totalPos , fingState );
	stateMsg.name     = vec_copy( ALL_JOINT_NAMES );
	stateMsg.position = totalPos;
}

// ___ End Functions & Classes ___


// === Program Vars ===



// ___ End Vars ___


// === main ================================================================================================================================

int main( int argc , char** argv ){ // Main takes the terminal command and flags that called it as arguments
	srand( time( 0 ) ); // Random seed based on the current clock time
	
	// 0. Init ROS  &&  Register node
	ros::init( argc , argv , NODE_NAME );
	
	// 1. Fetch handle to this node
	ros::NodeHandle nodeHandle;
	
	// 2. Init node rate
	ros::Rate heartbeat( RATE_HZ );
	
	// 3. Set up subscribers and publishers
	
	// ~ Publishers ~
	ros::Publisher joint_cmd_pub = nodeHandle.advertise<sensor_msgs::JointState>( "/lbr4_teleop/joint_cmd" , QUEUE_LEN );
	ros::Publisher vis_arr       = nodeHandle.advertise<visualization_msgs::MarkerArray>( "visualization_marker_array" , QUEUE_LEN );
	
	// ~ Subscribers ~
	// ros::Subscriber SUBSCRIBER_OBJ = nodeHandle.subscribe( "TOPIC_NAME" , QUEUE_LEN , CALLBACK_FUNCTION );
	
	// ~ Service Servers ~
	// ros::ServiceServer SERVER_OBJ = nodeHandle.advertiseService( "SERVICE_NAME" , SERV_CALLBACK );
	
	// ~ Service Clients ~
	ros::ServiceClient IK_Client = nodeHandle.serviceClient<motion_cost::qFromPose>( "q_from_pose" );
	
	/// === Preliminary { Setup , Instantiation , Planning  } ==============================================================================

	// ~~ I. Planning ~~

	//  0. Init vars
	
	size_t befSupport =   0 , 
		   numSteps   = 100 ;
	
	double removeDist  = 0.070 , 
		   floorXWidth = 1.000 ,
		   floorYLngth = 1.000 ,
		   gripMaxSep  = 0.080 ; // 8cm max separation
	
	//~ Eigen::Vector3d floorCenter{ 0.795 , 0.0 , 0.59 }; // Table center
	Eigen::Vector3d floorCenter{ 0.9 , 0.0 , 0.59 }; // Table center

	//  1. Instantiate ASM
	Assembly_ASM* simpleCube  = simple_cube();
	Assembly_ASM* simplePhone = simple_phone();
	
	//  2. Disassemble
	AsmStateNode_ASP* cubeRoot = Belhadj2017_full_depth( simpleCube , 
														 0.4 , 0.2 , 0.4 , 
														 0.8 , 10 );
	AsmStateNode_ASP* phoneRoot = Belhadj2017_full_depth( simplePhone , 
														  0.4 , 0.2 , 0.4 , 
														  0.8 , 10 );
										
	// ~~ AA. Layer Demo ~~
	AsmStateNode_ASP* cubeLevelSeq = precedence_layer_root_to_action_sequence( cubeRoot , floorCenter , 
																			   removeDist , numSteps ,
																			   floorXWidth , floorYLngth );
	AsmStateNode_ASP* phoneLevelSeq = precedence_layer_root_to_action_sequence( phoneRoot , floorCenter , 
																			    removeDist , numSteps ,
																			    floorXWidth , floorYLngth );
	sep( "Look at level action sequence: Cube" );
	inspect_level_ASP( cubeLevelSeq );
	sep( "Look at level action sequence: Phone" );
	inspect_level_ASP( phoneLevelSeq );
										
	// 3. Simulate separation in the specified pose
	AsmStateNode_ASP* before = cubeRoot;
	size_t numEdges = cubeRoot->count_edges();
	AsmStateNode_ASP* after;
	// Fetch the successor node that has the cube in it
	for( size_t i = 0 ; i < numEdges ; i++ ){  if( (*cubeRoot)[i]->has_part_ID(1) ){  after = (*cubeRoot)[i];  }  }
	
	
	
	Pose_ASP befPose = before->assembly->get_lab_pose_for_index_3D( befSupport , floorCenter );
	
	std::vector<Pose_ASP> liftPlan = simulate_disasm_step_straight( before , after , 
																	befPose , befSupport , 
																	removeDist , numSteps ,
																	floorCenter , floorXWidth , floorYLngth );
															  
	cout << endl << "Completed lift plan with " << liftPlan.size() << " steps!" << endl << endl;
	
	// 4. Plan a grasp
	
	Eigen::Vector3d approach = Eigen::Vector3d(  1.0 ,  0.0 ,  0.0 );
	Eigen::Vector3d perpAxis = Eigen::Vector3d(  0.0 ,  0.0 ,  1.0 );
	
	before->assembly->set_lab_pose_for_index_3D( befSupport , floorCenter );
	
	RayHits graspPairs = get_opposite_grasp_pairs_for_ASM_and_direction( before->assembly , 
																		 approach , 
																		 perpAxis , 2.0 ,
																		 0.01 );
	
	Eigen::Vector3d hiPoint = floorCenter + Eigen::Vector3d(  0.0 ,  0.0 ,  1.0 ); 
																		
	Eigen::MatrixXd chosenPair = closest_pair_in_hits_to( graspPairs , hiPoint ); // Find the closest grasp pair to the given point
	
	ParallelGraspState chosenGrasp = gripper_pose_from_grasp_pair( chosenPair , perpAxis , gripMaxSep ); 
	
	cout << "Generated a grasp target:" << endl
		 << "\tFeasible?: __ " << chosenGrasp.feasible << endl
		 << "\tPose: _______ " << chosenGrasp.graspTarget << endl
		 << "\tFinger state: " << chosenGrasp.q_fingers << endl << endl;
		 
	Pose_ASP relativeTarget = chosenGrasp.graspTarget / befPose;
	
	cout << "Relative grasp pose: _ " << relativeTarget << endl
		 << "Recover absolute pose: " << liftPlan[0] * relativeTarget << endl; // yes, This works!
		 
	// 5. Attempt to get a solution for ever pose in the sequence
	
	size_t maxAttempts = 50 , // ------------ Max number of times to attempt IK for a point 
		   maxRetries  = 25 , // ------------ Max number of times to retry IK when the solution is not close to the previous step
		   attempts    =  0 ,
		   retries     =  0 ,
		   len         = liftPlan.size() ; // Number of items in the motion plan
	bool   isClose     = false , // --------- Is this solution close to the last one?
		   isValid     = false , // --------- Did IK find a solution?
		   wasValid    = false ; // --------- Was there an IK solution for the previous step?
	double jointDiff   = 0.0d , // ---------- Distance between this config and the last
		   jointCRIT   = 0.2617d; // -------- Cutoff for what is considered close , 15deg
		   
	Pose_ASP /* -------- */ currPose; // -- Desired gripper pose (ASP)
	geometry_msgs::Pose     currPoseMSG; // Desired gripper pose (ROS)
	sensor_msgs::JointState seedMsg; // --- Seed joint state for the next request
	
	std::vector<sensor_msgs::JointState> allSolns; // - Vector of all solutions, valid or not
	std::vector<bool> /* ------------ */ solnsValid; // Flags for whether each solution is valid
	motion_cost::qFromPose /* ------- */ srvMsg; // --- Message for communication with the solver
	
	for( size_t i = 0 ; i < len ; i++ ){
		
		// A. Calc the absolute pose
		currPose    = liftPlan[i] * relativeTarget;
		currPoseMSG = ROS_Pose_from_Pose_ASP( currPose );
		srvMsg.request.request.worldPose = currPoseMSG;
		
		// B. Set seed
		if( wasValid && i > 0 ){
			seedMsg.position = srvMsg.response.response.qSoln.position;
		}else{
			seedMsg.position = randrange_vec( (double) -M_PI , (double) M_PI , 7 ); 
		}
		srvMsg.request.request.seed = seedMsg;
		
		// B. Request a solution
		attempts = 0;
	    isValid  = false;
	    // C. If no solution found, attempt again until solution returned or the attempt limit is reached
		while(  !isValid  &&  ( attempts < maxAttempts )  ){
			
			if( IK_Client.call( srvMsg ) ){
				ros_log( "Response returned!"    , INFO );
				cout << "Solution: " << srvMsg.response.response.qSoln.position;
				isValid = srvMsg.response.response.valid;
				if( isValid ){
					cout << "Valid! , Increment" << endl;
					// Store the good solution
					sensor_msgs::JointState temp;
					temp.position = srvMsg.response.response.qSoln.position;
					allSolns.push_back( temp );
					solnsValid.push_back( true );
				}else{
					cout << "Invalid! , Retry" << endl; 
				}
				
			}else{
				ros_log( "NO response returned!" , WARN ); 
			}
			attempts++;
		}
		
		// D. Check for closeness with the previous step
		if( isValid ){  
			sep( "Solved!" , 3 , '*' );  
			
			if( i > 0 ){
				// E. If it is not close to the last, retry with perturbations until a close solution is obtained or the retry limit is reached
				retries  = 0;
				std::vector<double> newSeed;
				while(  !isClose  &&  ( retries < maxRetries )  ){
					
					newSeed = perturb_vec( allSolns[i-1].position , 0.0872 );
					
					if( IK_Client.call( srvMsg ) ){
						ros_log( "Response returned!"    , INFO );
						cout << "Solution: " << srvMsg.response.response.qSoln.position;
						isValid = srvMsg.response.response.valid;
						if( isValid ){
							cout << "Valid! , Increment" << endl;
							// Store the good solution
							sensor_msgs::JointState temp;
							temp.position = srvMsg.response.response.qSoln.position;
							allSolns[i] = temp;
							solnsValid[i] = true;
							
							jointDiff = vec_L2_diff( allSolns[i].position , allSolns[i-1].position );
							isClose = jointDiff <= jointCRIT;
							
						}else{
							cout << "Invalid! , Retry" << endl; 
						}
						
					}else{
						ros_log( "NO response returned!" , WARN ); 
					}
					retries++;
				}
			}
			
		}else{ 
			sep( "FAILED!" , 3 , '!' );  
			sensor_msgs::JointState temp;
			temp.position = vec_nan( 7 );
			allSolns.push_back( temp );
			solnsValid.push_back( false );
		}
		
	}
	
	// 6. Print results
	for( size_t i = 0 ; i < len ; i++ ){
		cout << "Step " << i + 1 << " , Valid?: " << solnsValid[i] << " , Solution: " << allSolns[i].position << endl;
	}
	
	// 7. Record
	
	// ~~ II. Recording ~~
	
	Assembly_ASM* balance = Assembly_A_minus_B( before->assembly , after->assembly );
	balance->set_pose( befPose );
	
	std::vector<double> fingPos = { 0.02 , 0.02 }; // From the test, need a way to load programmatically
	
	//  A. Instantiate a recording
	PlanRecording record;
	//  6. For every traj point
	for( size_t i = 0 ; i < len ; i++ ){
		//  7. Instantiate frame 
		PlayerFrame frame;
		
		//  8. Set the pose
		//~ Eigen::Vector3d pos = trajPts.row(i);
		after->assembly->set_pose( liftPlan[i] );
		//  9. Load markers into the frame's manager
		after->assembly->load_part_mrkrs_into_mngr( frame.mrkrMngr );
		balance->load_part_mrkrs_into_mngr( frame.mrkrMngr );
		
		// 10. Load a jointstate message into the frame
		sensor_msgs::JointState temp;
		pack_joint_and_gripper_states_into_msg( allSolns[i].position , // Joint positions of arm
												fingPos , // ------------ Joint positions of gripper
												temp ); // -------------- Load state data here
		frame.jnt_state_ = temp;
		// 11. Load frame into recording
		record.push_back( frame );
	}
	// 12. Instantiate playback (start timer)  &&  Load recording into playback
	PlanPlayer player{ 30 , record };
	PlayerFrame* currFrame = nullptr;
	bool LOOP = true;
	
	
	// ~ Animation Vars ~
	srand( ( time( 0 ) % 1 ) * 1000.0 + getpid() ); // Random seed based on the current clock time
	
	Eigen::Vector2d offsetStep{ 0.30 , 0.00 };
	Eigen::Vector2d asmPlnStep{ 0.00 , 0.40 };
	
	// Set up visualization management
	RViz_MarkerManager mrkrMngr{};
	visualization_msgs::MarkerArray& markerArr = mrkrMngr.get_arr();
	
	
	
	// ~ Visualize Results ~
	
	if( true ){
	
		cout << "About to set pose ..." << endl;
		simpleCube->set_pose( origin_pose() );
		cout << "About to load markers ..." << endl;
		simpleCube->load_part_mrkrs_into_mngr( mrkrMngr );
		
		visualize_tree_structure_plan( mrkrMngr , 
									   Eigen::Vector2d( 0.0 , 0.3 ) , Eigen::Vector2d( 0.0 , 0.3 ) , Eigen::Vector2d( 0.3 , 0.0 ) ,
									   cubeRoot );
		
	}else{
		
		
		
	}

	// ~~ II. Recording ~~
	
	

	/// ___ End Preliminary ________________________________________________________________________________________________________________

	// N-1. Notify
	ros_log( "[" + NODE_NAME + "] Init OK and about to run ..." , INFO );
	
	// N. Main loop
	while( ros::ok() ){ // While neither node nor ROS has been shut down
		
		/// == NODE WORK ===================================================================================================================
		
		// Wait for a subscriber to start looking for this topic
		while ( vis_arr.getNumSubscribers() < 1 ){
			if ( !ros::ok() ){ return 0; }
			printf( "." );
			ROS_WARN_ONCE( "Please create a subscriber to the marker" );
			sleep( 1 );
			cout << ".";
		} 
        
        // A. Fetch frame
		currFrame = player.current_frame( LOOP );
		// B. Render object markers
		visualization_msgs::MarkerArray& markerArr = currFrame->mrkrMngr.get_arr();
		vis_arr.publish( markerArr );
		// C. Render the joint state
		joint_cmd_pub.publish( currFrame->jnt_state_ );
		
		/// __ END WORK ____________________________________________________________________________________________________________________
		
		ros::spinOnce(); // - Process messages
		heartbeat.sleep(); // Sleep for remainder of period
	}
	
	// N+1. Notify  &&  Exit
	
	ros_log( "[" + NODE_NAME + "] Exit OK, Goodbye!" , INFO );
	
	return 0; // I guess everything turned out alright at the end!
}

// ___ End main ____________________________________________________________________________________________________________________________


/* === Spare Parts =========================================================================================================================



   ___ End Spare ___________________________________________________________________________________________________________________________
*/
