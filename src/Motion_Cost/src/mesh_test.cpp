/*
mesh_test.cpp
James Watson , 2017 October
Test integration of the ASP_3D lib --> ROS , Show markers and move them around

Dependencies:
Template Version: 2017-09-23
*/

// === Init ================================================================================================================================

// ~~~ Includes ~~~
// ~~ Standard ( Common includes not already in Cpp_Helpers.h ) ~~ 
//#include <iomanip> // - Output precision and printing
//#include <exception> // error handling
//#include <stdexcept> // library provided error types like 'std::range_error'
//#include <tuple> // --- standard tuple datatype
//#include <cstddef> // - alias types names:  size_t, ptrdiff_t
//#include <cstring> // - C Strings: strcomp 
//#include <algorithm> // sort, search, min, max, sequence operations

// ~~ ROS ~~
// ~ Core ~
#include <ros/ros.h> // ---------------------------- ROS presides over all
#include <ros/package.h> // Where are we? // http://wiki.ros.org/Packages#C.2B-.2B-
// ~ RViz ~
#include <visualization_msgs/Marker.h> // ---------- Display geometry
#include <visualization_msgs/MarkerArray.h> // ----- RViz marker array

// ~~ Local ~~ 
#include <Cpp_Helpers.h> // Shortcuts and Aliases , and Functions of general use for C++ programming
#include <ASP_3D.h>

// ___ End Init ____________________________________________________________________________________________________________________________


// === Program Functions & Classes ===


// ___ End Functions & Classes ___


// === Program Vars ===

// ~~ Display ~~
int updateHz = 30;
// ~~ Bookkeepping ~~
Incrementer assign_part_ID{ 0 }; // Instantiate a functor that will assign a unique number to each part

// ___ End Vars ___


// === main ================================================================================================================================

int main( int argc , char** argv ){
	
	srand( time( 0 ) ); // Random seed based on the current clock time
	
	
	
	// == Problem Setup ==
	
	// 0. Create file paths
	string pkgPath = ros::package::getPath("motion_cost");
	string EL_fileName  = pkgPath + "/Resources/EL_SCLD.stl";
	string CEE_fileName = pkgPath + "/Resources/CEE_SCLD.stl";
	cout << "Path to EL : " << EL_fileName  << endl;
	cout << "Path to CEE: " << CEE_fileName << endl;
	
	// 1. Create two part objects
	Part_ASM EL_part{  assign_part_ID() , EL_fileName  };
	Part_ASM CEE_part{ assign_part_ID() , CEE_fileName };
	
	// 2. set the poses for the parts
	Eigen::Vector3d posEL;		posEL   <<  0.2d   ,  0.2d   ,  0.2d   ;
	Eigen::Quaterniond quatEL{              0.0d   ,  0.0d   ,  0.707d , -0.707d };
	EL_part.set_pose( posEL , quatEL );
	Eigen::Vector3d posCEE;		posCEE  <<  0.0d   ,  0.0d   ,  0.0d   ;
	Eigen::Quaterniond quatCEE{             1.0d   ,  0.0d   ,  0.0d   ,  0.0d   };
	CEE_part.set_pose( posCEE , quatCEE );
	sep( "EL" );
	EL_part.status_report();
	
	sep( "CEE" );
	CEE_part.status_report();
	
	// 3. Create two mesh markers , Load the mesh markers with the triangle data from the parts
	RViz_MeshMarker EL_mrkr{ EL_part };		
	RViz_MeshMarker CEE_mrkr{ CEE_part };	
	
	cout << "Color after instantiation" << endl;
	EL_mrkr.print_color();
	CEE_mrkr.print_color();
	
	EL_mrkr.set_color(  0.0f , 0.0f , 1.0f , 1.0f ); 
	CEE_mrkr.set_color( 0.0f , 1.0f , 0.0f , 1.0f );
	
	cout << "Color after change" << endl;
	EL_mrkr.print_color();
	CEE_mrkr.print_color();
	
	cout << "Marker Poses" << endl;
	cout << "EL:  "; EL_mrkr.print_pose();
	cout << "CEE: "; CEE_mrkr.print_pose();
	
	
	// 4. Add the mesh markers to the mesh marker manager
	RViz_MarkerManager mrkrMngr{};
	mrkrMngr.add_marker( EL_mrkr  );
	mrkrMngr.add_marker( CEE_mrkr );
	
	// __ End Setup __
	
	
	// == ROS Loop ==
	
	// = ROS Start =
	
	// 5. Start ROS
	srand( ( time( 0 ) % 1 ) * 1000.0 + getpid() ); // Random seed based on the current clock time
	
	ros::init( argc , argv , "mesh_test_cpp" );
	
	
	// ros::Publisher vis_pub = node_handle.advertise<visualization_msgs::Marker>(       "visualization_marker"       , 0 );
	ros::NodeHandle node_handle;
	ros::Publisher vis_arr = node_handle.advertise<visualization_msgs::MarkerArray> ( "visualization_marker_array" , 100 );
	
	ros::Rate r( updateHz ); // 30Hz refresh rate
	
	// _ End Start _
	
	visualization_msgs::MarkerArray& markerArr = mrkrMngr.get_arr();
	
	float t    = 0.0f  ,
	      incr = 0.07f ;
	
	// 6. Set the animation loop and display
	while ( ros::ok() ){ // while roscore is running
        // cout << ".";

		// Wait for a subscriber to start looking for this topic
		while ( vis_arr.getNumSubscribers() < 1 ){
			if ( !ros::ok() ){ return 0; }
			printf( "." );
			ROS_WARN_ONCE( "Please create a subscriber to the marker" );
			sleep( 1 );
			cout << ".";
		} 
		
		CEE_mrkr.set_pose( 0.0 , 0.0 , cos( t ) , 1.0 , 0.0 , 0.0 , 0.0 );
        mrkrMngr.rebuild_mrkr_array();
        markerArr = mrkrMngr.get_arr();
        
        vis_arr.publish( markerArr );
        
        t += incr;

		r.sleep();
	}
	
	// __ End ROS __
	
	
	return 0; // I guess everything turned out alright at the end!
}

// ___ End main ____________________________________________________________________________________________________________________________


/* === Spare Parts =========================================================================================================================



   ___ End Spare ___________________________________________________________________________________________________________________________
*/
