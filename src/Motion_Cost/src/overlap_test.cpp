/*
simple_phone.cpp
James Watson , 2018 February
Test integration of the ASP_3D lib --> ROS , Show markers and move them around

Dependencies: Cpp_Helpers , ROS , Eigen , RAPID
Template Version: 2017-09-23
*/

// === Init ================================================================================================================================

// ~~~ Includes ~~~
// ~~ ROS ~~
// ~ Core ~
#include <ros/ros.h> // --- ROS presides over all
#include <ros/package.h> // Where are we? // http://wiki.ros.org/Packages#C.2B-.2B-
// ~~ Includes ~~
#include <Eigen/Core> // ---- The living heart of Eigen
#include <Eigen/Dense> // --- Cross Product , etc.
#include <Eigen/Geometry> //- Quaternion , etc
// ~ RViz ~
#include <visualization_msgs/Marker.h> // ---- Display geometry
#include <visualization_msgs/MarkerArray.h> // RViz marker array
// ~~ Local ~~ 
#include <Cpp_Helpers.h> // Shortcuts and Aliases , and Functions of general use for C++ programming
#include <ASP_3D.h> // ---- Assembly geometry
#include <AsmSeqPlan.h> //- Graph planning


// using namespace quickhull;

// ___ End Init ____________________________________________________________________________________________________________________________

// === Program Vars ===

string projectOuputDir = "/home/jwatson/output/";

// ___ End Vars ___



// === main ================================================================================================================================

int main( int argc , char** argv ){
	
	srand( time( 0 ) ); // Random seed based on the current clock time
	
	sep( "Check Segment Collision" );
	
	Segment2D_ASP segA{ Eigen::Vector2d{ 0 , 4 } , Eigen::Vector2d{ 4 , 0 } };
	Segment2D_ASP segB{ Eigen::Vector2d{ 4 , 4 } , Eigen::Vector2d{ 5 , 5 } };
	Segment2D_ASP segC{ Eigen::Vector2d{ 2 , 2 } , Eigen::Vector2d{ 3 , 3 } };
	Segment2D_ASP segD{ Eigen::Vector2d{ 0 , 2 } , Eigen::Vector2d{ 2 , 4 } };
	Segment2D_ASP segE{ Eigen::Vector2d{ 8 , 8 } , Eigen::Vector2d{ 8 , 8 } }; // Nonsense
	Segment2D_ASP segF{ Eigen::Vector2d{ 0 , 0 } , Eigen::Vector2d{ 2 , 0 } };
	Segment2D_ASP segG{ Eigen::Vector2d{ 4 , 0 } , Eigen::Vector2d{ 6 , 0 } };
	
	cout << "A and B intersect?: " << intersect_seg_2D( segA , segB , true ) << endl; // Correct: False // Actual: 0
	cout << "A and C intersect?: " << intersect_seg_2D( segA , segC , true ) << endl; // Correct: True  // Actual: 1
	cout << "A and D intersect?: " << intersect_seg_2D( segA , segD , true ) << endl; // Correct: True  // Actual: 1
	cout << "A and E intersect?: " << intersect_seg_2D( segA , segE , true ) << endl; // Correct: ????  // Actual: 0
	cout << "F and G intersect?: " << intersect_seg_2D( segF , segG , true ) << endl; // Correct: False // Actual: 0
	
	sep( "Check Triangle Overlap" );
	
	Eigen::MatrixXd triA( 3 , 3 );
	triA << 4 , 0 , 0 ,
			2 , 4 , 0 ,
			0 , 0 , 0 ;
	
	Eigen::MatrixXd triB( 3 , 3 );
	triB << 4 , 0 , 0 ,
			0 , 0 , 0 ,
			2 , 0 , 4 ;
	
	Eigen::MatrixXd triC( 3 , 3 );
	triC << 4 , 0 , 4 ,
			0 , 0 , 4 ,
			2 , 2 , 4 ;
	
	Eigen::MatrixXd triD( 3 , 3 );
	triD << 4 , 4 , 0 ,
			2 , 0 , 0 ,
			0 , 4 , 0 ;
			
	double angleCrit = 0.0349;
	double distcCrit = 0.001;
	cout << "Triangles A and A adjacent?: " << are_those_triangles_adjacent( triA , triA , angleCrit , distcCrit ) << endl; // 0
	cout << "Triangles A and B adjacent?: " << are_those_triangles_adjacent( triA , triB , angleCrit , distcCrit ) << endl; // 0
	cout << "Triangles A and C adjacent?: " << are_those_triangles_adjacent( triA , triC , angleCrit , distcCrit ) << endl; // 0
	cout << "Triangles A and D adjacent?: " << are_those_triangles_adjacent( triA , triD , angleCrit , distcCrit ) << endl; // 1
	
	// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
	
	sep( "Check Polygon Overlap" );
	
	cout << "Reading output data ..." << endl << endl;
	
	std::vector<string> allFiles = list_dir( projectOuputDir ); // Fetch all the directory entries
	std::vector<std::vector<std::vector<double>>> linNums;
	
	string modeSep = "~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~";
	string line;
	bool   created = true;
	
	for( size_t i = 0 ; i < allFiles.size() ; i ++ ){
		if( allFiles[i].length() > 2 ){ // If the entry is not '.' or '..'
			
			// 1. Fetch the lines of the file
			std::vector<string> lines = readlines( projectOuputDir + allFiles[i] );
			linNums.clear();
			std::vector<std::vector<double>> temp;  linNums.push_back( temp );  created = true;
			cout << "File: " << projectOuputDir + allFiles[i] << " , Got " << lines.size() << " lines." << endl;
			
			size_t len       = lines.size() , 
				   polyCount = 0            ;
			bool mode2 = false;
			
			
			for( size_t j = 0 ; j < len ; j++ ){
				line = lines[j];
				// 2. Discard everything before the flat polygons
				if( !mode2 ){  
					if( str_has_sub( line , modeSep ) ){  
						mode2 = true;  
						//~ cout << "Entered Mode 2!" << endl;
					}  
				}else{
					// 3. For each line
					// 4. If the line contains data
					if( line.length() > 2 ){  
						linNums[ polyCount ].push_back( tokenize_to_dbbl_w_separator( line , ',' ) );  
						created = false;
					}else{
						if( !created ){
							polyCount++;
							std::vector<std::vector<double>> temp;
							linNums.push_back( temp );
							created = true;
						}
					}
				}
			}
			
			for( size_t j = 0 ; j < linNums.size() ; j++ ){
				print_vec_vec( linNums[j] );
				cout << linNums[j].size() << " rows" << endl;
				cout << endl << endl;
			}
			
			cout << "linNums: " << linNums.size() << endl;
			cout << "linNums: " << linNums.size() << " , " << linNums[0].size() << " , " << linNums[0][0].size() <<  endl;
			
			cout << "About to load vectors ..." << endl;
			
			// ASSUME: File has only 2 flat polys and we have correctly loaded them
			Eigen::MatrixXd poly1 = load_vec_vec_into_matx( linNums[0] );
			Eigen::MatrixXd poly2 = load_vec_vec_into_matx( linNums[1] );
			
			cout << "About to perform collision check ..." << endl;
			
			cout << "Do the polygons in this file collide?: " << polygon_collide_2D( poly1 , poly2 ) << endl;
			
		}
	}
	
	//~ cout << atof( "2.5" ) << endl;
	
	return 0; // I guess everything turned out alright at the end!
}

// ___ End main ____________________________________________________________________________________________________________________________


/* === Spare Parts =========================================================================================================================


	
   ___ End Spare ___________________________________________________________________________________________________________________________
*/

