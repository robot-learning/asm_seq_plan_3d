/*
overlap_triangle.cpp
James Watson , 2018 April
Test integration of the ASP_3D lib --> ROS , Show markers and move them around

Dependencies: Cpp_Helpers , ROS , Eigen , RAPID
Template Version: 2017-09-23
*/

// === Init ================================================================================================================================

// ~~~ Includes ~~~
// ~~ ROS ~~
// ~ Core ~
#include <ros/ros.h> // --- ROS presides over all
#include <ros/package.h> // Where are we? // http://wiki.ros.org/Packages#C.2B-.2B-
// ~~ Includes ~~
#include <Eigen/Core> // ---- The living heart of Eigen
#include <Eigen/Dense> // --- Cross Product , etc.
#include <Eigen/Geometry> //- Quaternion , etc
// ~ RViz ~
#include <visualization_msgs/Marker.h> // ---- Display geometry
#include <visualization_msgs/MarkerArray.h> // RViz marker array
// ~~ Local ~~ 
#include <Cpp_Helpers.h> // Shortcuts and Aliases , and Functions of general use for C++ programming
#include <ASP_3D.h> // ---- Assembly geometry
#include <AsmSeqPlan.h> //- Graph planning


// using namespace quickhull;

// ___ End Init ____________________________________________________________________________________________________________________________

// === Program Vars ===

string projectOuputDir = "/home/jwatson/output/";

// ___ End Vars ___



// === main ================================================================================================================================

int main( int argc , char** argv ){
	
	srand( time( 0 ) ); // Random seed based on the current clock time
	
	Eigen::MatrixXd triA = Eigen::MatrixXd::Zero( 3 , 2 );
	triA << 0 , 0 ,
			4 , 0 ,
			0 , 4 ;
	
	Eigen::MatrixXd triB = Eigen::MatrixXd::Zero( 3 , 2 );
	triB << 0 , 0 ,
			4 , 0 ,
			4 , 4 ;
	
	sep( "Check Triangle Area" );
	cout << "Area of Triangle A: " << tri_area_2D( triA ) << endl;
	cout << "Area of Triangle B: " << tri_area_2D( triB ) << endl;
	
	sep( "Check Triangle Overlap" );
	size_t lvl = 20; // 5% overestimate at level 20 subdivision
	cout << "Overlapping Area Triangles A and B: " << overlapping_tri_area( triA , triB , lvl ) << endl;
	cout << "Overlapping Area Triangles B and A: " << overlapping_tri_area( triB , triA , lvl ) << endl;
	cout << "Overlapping Area Triangles A and A: " << overlapping_tri_area( triA , triA , lvl ) << endl;
	
	//~ cout << atof( "2.5" ) << endl;
	
	return 0; // I guess everything turned out alright at the end!
}

// ___ End main ____________________________________________________________________________________________________________________________


/* === Spare Parts =========================================================================================================================


	
   ___ End Spare ___________________________________________________________________________________________________________________________
*/

