/*
part_collide.cpp
James Watson , 2018 February
Test integration of the ASP_3D lib --> ROS , Show markers and move them around

Dependencies: Cpp_Helpers , ROS , Eigen , RAPID
Template Version: 2017-09-23
*/

// === Init ================================================================================================================================

// ~~~ Includes ~~~
// ~~ Standard ( Common includes not already in Cpp_Helpers.h ) ~~ 
//#include <iomanip> // - Output precision and printing
//#include <exception> // error handling
//#include <stdexcept> // library provided error types like 'std::range_error'
//#include <tuple> // --- standard tuple datatype
//#include <cstddef> // - alias types names:  size_t, ptrdiff_t
//#include <cstring> // - C Strings: strcomp 
//#include <algorithm> // sort, search, min, max, sequence operations

// ~~ ROS ~~
// ~ Core ~
#include <ros/ros.h> // --- ROS presides over all
#include <ros/package.h> // Where are we? // http://wiki.ros.org/Packages#C.2B-.2B-
// ~~ Includes ~~
#include <Eigen/Core> // ---- The living heart of Eigen
#include <Eigen/Dense> // --- Cross Product , etc.
#include <Eigen/Geometry> //- Quaternion , etc
// ~ RViz ~
#include <visualization_msgs/Marker.h> // ---- Display geometry
#include <visualization_msgs/MarkerArray.h> // RViz marker array
// ~~ Local ~~ 
#include <Cpp_Helpers.h> // Shortcuts and Aliases , and Functions of general use for C++ programming
#include <ASP_3D.h> // ---- Assembly planning


// using namespace quickhull;

// ___ End Init ____________________________________________________________________________________________________________________________


// === Program Functions & Classes ===

void load_RAPID_model( RAPID_model* rapidModel , Eigen::MatrixXd V , Eigen::MatrixXi F ){
	// Load all of the points into 'rapidModel' , Which stores points in the C style
	int counter = 0; // Triangle ID counter
	int len     = F.rows();
	double p0[3], p1[3], p2[3]; // R3 points
	// 1. Prepare the model
	rapidModel->BeginModel();
	// 2. For each f_i
	for( int i = 0 ; i < len ; i++ ){
		// 3. For each dimension of v_{0,1,2}
		for( int j = 0 ; j < 3 ; j++ ){
			// 4. Load the point into the array
			p0[j] = V( F(i,0) , j );
			p1[j] = V( F(i,1) , j );
			p2[j] = V( F(i,2) , j );
		}
		// 5. Load the facet, and give it a unique number (unique to the model, does not have to be unique globally)
		rapidModel->AddTri( p0 , p1 , p2 , counter );
		counter++;
	}
	// 6. End model input
	rapidModel->EndModel();
}

void load_RAPID_transform( double T[3] , double R[3][3] , Pose_ASP partPose ){
	// Extract the pose information 'partPose' and load it into 'T' and 'R' , as RAPID expects
	for( size_t i = 0 ; i < 3 ; i++ ){
		T[i] = partPose.homogMatx( i , 3 ); // Transfer the Pose_ASP translation to the RAPID translation
		for( size_t j = 0 ; j < 3 ; j++ ){
			R[i][j] = partPose.homogMatx( i , j );
		}
	}
}

// ___ End Functions & Classes ___


// === Program Vars ===
//     EL       | CEE
double T1[3]    , T2[3]    ;
double R1[3][3] , R2[3][3] ;

// ___ End Vars ___


// === main ================================================================================================================================

int main( int argc , char** argv ){
	
	srand( time( 0 ) ); // Random seed based on the current clock time
	
	// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
	
	sep( "Part Collision Test" );
	
	Incrementer assign_part_ID{ 0 }; // Instantiate a functor that will assign a unique number to each part
	
	// 0. Create file paths
	string pkgPath = ros::package::getPath( "motion_cost" );
	string EL_fileName  = pkgPath + "/Resources/EL_SCLD.stl";
	string CEE_fileName = pkgPath + "/Resources/CEE_SCLD.stl";
	string CUBE_fileName = pkgPath + "/Resources/CUBE.stl";
	cout << "Path to EL : " << EL_fileName  << endl;
	cout << "Path to CEE: " << CEE_fileName << endl;
	
	// 1. Create part object(s)
	Part_ASM EL_part{  assign_part_ID() , EL_fileName  };
	Part_ASM CEE_part{ assign_part_ID() , CEE_fileName };
	Part_ASM CUBE_part{ assign_part_ID() , CUBE_fileName };
	
	// 2. set the poses for the part(s)
	// Cube
	Eigen::Vector3d posCUBE;  posCUBE <<  0.0d   ,  0.0d   ,  0.0d;
	Eigen::Quaterniond quatCUBE{             1.0d   ,  0.0d   ,  0.0d   ,  0.0d   };
	CUBE_part.set_pose( posCUBE , quatCUBE );
	// EL
	// Eigen::Vector3d    posEL{ 0.2 * ( 0.25 - 0.075 ) , 0.0 , 0.2 * 0.125 };		
	Eigen::Vector3d    posEL{ 0.2 * ( 0.25 - 0.075 ) , 0.0 , 0.2 * 0.125 + 0.15 };		
	// Eigen::Quaterniond quatEL{              0.000 ,  0.000 ,  0.707 , -0.707 };
	Eigen::Quaterniond quatEL{               1.0d   ,  0.0d   ,  0.0d   ,  0.0d };
	EL_part.set_pose( posEL , quatEL );
	// CEE
	Eigen::Vector3d posCEE;		posCEE  <<  0.0d   ,  0.0d   ,  0.0d   ;
	Eigen::Quaterniond quatCEE{             1.0d   ,  0.0d   ,  0.0d   ,  0.0d   };
	CEE_part.set_pose( posCEE , quatCEE );
	
	
	
	// 3. Set up collisions
	
	cout << "Loading RAPID models ... ";
		
	// A. Instantiate RAPID models
	RAPID_model *EL_RAPID  = new RAPID_model;
	RAPID_model *CEE_RAPID = new RAPID_model;
	// B. Load RAPID models
	load_RAPID_model( EL_RAPID  , EL_part.get_dense_vertices()  , EL_part.get_dense_facets()  );
	load_RAPID_model( CEE_RAPID , CEE_part.get_dense_vertices() , CEE_part.get_dense_facets() );
	
	cout << "COMPLETE!" << endl;
	
	
	// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
	
	sep( "Visualizing Collisions" );
	
	EL_part.set_part_mrkr_color(  0.0f , 1.0f , 0.0f );
	CEE_part.set_part_mrkr_color( 0.0f , 0.0f , 1.0f );
	
	Eigen::Quaterniond deltaEX{ 0.999 , 0.052 , 0.000 , 0.000 }; // Turn by this much every frame
	Pose_ASP temp;
	double tempColor[4];
	
	// == ROS Loop ==
	
	// = ROS Start =
	
	
	// 5. Start ROS
	srand( ( time( 0 ) % 1 ) * 1000.0 + getpid() ); // Random seed based on the current clock time and process
	
	ros::init( argc , argv , "part_collide_cpp" );
	
	
	// ros::Publisher vis_pub = node_handle.advertise<visualization_msgs::Marker>(       "visualization_marker"       , 0 );
	ros::NodeHandle node_handle;
	ros::Publisher vis_arr = node_handle.advertise<visualization_msgs::MarkerArray> ( "visualization_marker_array" , 100 );
	int updateHz = 30;
	ros::Rate r( updateHz ); // 30Hz refresh rate
	
	RViz_MarkerManager mrkrMngr{};
	
	// Load axes into the manager
	mrkrMngr.add_marker( get_axes_marker( 5 , 0.01f ) );
	
	// Load models into the manager
	EL_part.load_part_mrkrs_into_mngr(  mrkrMngr );
	CEE_part.load_part_mrkrs_into_mngr( mrkrMngr );
	
	// Fetch the markers for publishing
	visualization_msgs::MarkerArray& markerArr = mrkrMngr.get_arr();
	
	
	// 7. Set the animation loop and display
	while ( ros::ok() ){ // while roscore is running

		// Wait for a subscriber to start looking for this topic
		while ( vis_arr.getNumSubscribers() < 1 ){
			if ( !ros::ok() ){ return 1; }
			ROS_WARN_ONCE( "Please create a subscriber to the marker" );
			cout << ".";
			sleep( 1 );
		} 
		
		// ~ Animation or Other Pose Changes ~
		temp = EL_part.get_pose();
		temp.orientation = temp.orientation * deltaEX;
		temp.orientation.normalize();
		EL_part.set_pose( temp );
        
        // ~~ Collision Check ~~
        
        // 3. If there is a collision
        if( EL_part.collides_with( CEE_part ) ){
			EL_part.set_part_mrkr_color( 1.0f , 0.0f , 0.0f ); // Color the part red to indicate collision
		}else{
			EL_part.set_part_mrkr_color( 0.0f , 1.0f , 0.0f ); // Otherwise restore green color
		}
        
        // Update markers and publish
        markerArr = mrkrMngr.rebuild_mrkr_array();
        vis_arr.publish( markerArr );
        
        // Rest and Loop
		r.sleep();
	}
	
	cout << markerArr.markers.size() << " markers in the array on exit." << endl;
	
	// __ End ROS __
	
	return 0; // I guess everything turned out alright at the end!
}

/// ___ END VISUAL _________________________________________________________________________________________________________________________

// ___ End main ____________________________________________________________________________________________________________________________


/* === Spare Parts =========================================================================================================================

		//~ // 1. Load the appropriate pose structures
        //~ load_RAPID_transform( T1 , R1 , EL_part.get_pose_w_matx()  );
        //~ load_RAPID_transform( T2 , R2 , CEE_part.get_pose_w_matx() );
        //~ // 2. Check for collision
        //~ RAPID_Collide( R1 , T1 , EL_RAPID , R2 , T2 , CEE_RAPID , RAPID_FIRST_CONTACT ); // Shortcut on first triangle collision

~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

	//~ // 3. Calc supports
	
	//~ // ~~ EL ~~
	sep( "CUBE Supports" , 2 , '*' );
	CUBE_part.determine_support_stability();
	CUBE_part.report_support_stability();
	cout << endl;
	
	sep( "CEE Supports" , 2 , '*' );
	CEE_part.determine_support_stability();
	CEE_part.report_support_stability();
	cout << endl;
	
	sep( "EL Supports" , 2 , '*' );
	EL_part.determine_support_stability();
	EL_part.report_support_stability();
	cout << endl;

~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

#include "RAPID.H"
#include <math.h>
#include <stdio.h>

#define LISTS 1

main()
{
  // First, get a couple of RAPID_models in which to put our models

  RAPID_model *b1 = new RAPID_model;
  RAPID_model *b2 = new RAPID_model;
  
  // Then, load the models with triangles.  The following loads each 
  // with a torus of 2*n1*n2 triangles.

  fprintf(stderr, "loading tris into RAPID_model objects...");  fflush(stderr);
  
  double a = 1.0;  // major radius of the tori
  double b = 0.2;  // minor radius of the tori

  int n1 = 50;     // tori will have n1*n2*2 triangles each
  int n2 = 50;

  int uc, vc;
  int count = 0;
  
  for(uc=0; uc<n1; uc++)
    for(vc=0; vc<n2; vc++)
      {
        double u1 = (2.0*M_PI*uc) / n1; 
        double u2 = (2.0*M_PI*(uc+1)) / n1; 
        double v1 = (2.0*M_PI*vc) / n2; 
        double v2 = (2.0*M_PI*(vc+1)) / n2; 

        double p1[3], p2[3], p3[3], p4[3];

        p1[0] = (a - b * cos(v1)) * cos(u1);
        p2[0] = (a - b * cos(v1)) * cos(u2);
        p3[0] = (a - b * cos(v2)) * cos(u1);
        p4[0] = (a - b * cos(v2)) * cos(u2);
        p1[1] = (a - b * cos(v1)) * sin(u1);
        p2[1] = (a - b * cos(v1)) * sin(u2);
        p3[1] = (a - b * cos(v2)) * sin(u1);
        p4[1] = (a - b * cos(v2)) * sin(u2);
        p1[2] = b * sin(v1);
        p2[2] = b * sin(v1);
        p3[2] = b * sin(v2);
        p4[2] = b * sin(v2);

        b1->AddTri(p1, p2, p3, count);
        b1->AddTri(p4, p2, p3, count+1);
        b2->AddTri(p1, p2, p3, count);
        b2->AddTri(p4, p2, p3, count+1);

        count += 2;
      }
  fprintf(stderr, "done\n");  fflush(stderr);
  fprintf(stderr, "Tori have %d triangles each.\n", count);
  fprintf(stderr, "building hierarchies...");  fflush(stderr);
  b1->EndModel();
  b2->EndModel();
  fprintf(stderr, "done.\n"); fflush(stderr); 
  
  // Now we are free to call the interference detection routine.
  // But first, construct the transformations which define the placement
  // of our two hierarchies in world space:

  // This placement causes them to overlap a large amount.

  double R1[3][3], R2[3][3], T1[3], T2[3];
  
  R1[0][0] = R1[1][1] = R1[2][2] = 1.0; // Object 1 orientation , 3 x 3 rotation matrix
  R1[0][1] = R1[1][0] = R1[2][0] = 0.0;
  R1[0][2] = R1[1][2] = R1[2][1] = 0.0;

  R2[0][0] = R2[1][1] = R2[2][2] = 1.0; // Object 2 orientation , 3 x 3 rotation matrix
  R2[0][1] = R2[1][0] = R2[2][0] = 0.0;
  R2[0][2] = R2[1][2] = R2[2][1] = 0.0;
  
  T1[0] = 1.0;  T1[1] = 0.0; T1[2] = 0.0; // Object 1 translation , 3 x 1 vector
  T2[0] = 0.0;  T2[1] = 0.0; T2[2] = 0.0; // Object 2 translation , 3 x 1 vector

  // Now we can perform a collision query:

  RAPID_Collide(R1, T1, b1, R2, T2, b2, RAPID_ALL_CONTACTS);

  // Looking at the report, we can see where all the contacts were, and
  // also how many tests were necessary:

  printf("All contacts between overlapping tori:\n");
  
  printf("Num box tests: %d\n", RAPID_num_box_tests);
  printf("Num contact pairs: %d\n", RAPID_num_contacts);
#if LISTS
  int i;
  for(i=0; i<RAPID_num_contacts; i++)
    {
      printf("\t contact %4d: tri %4d and tri %4d\n", 
             i, RAPID_contact[i].id1, RAPID_contact[i].id2);
    }
#endif

  // Notice the RAPID_ALL_CONTACTS flag we used in the call to collide().
  // The alternative is to use the RAPID_FIRST_CONTACT flag, instead,
  // so that the collide routine searches for contacts until it locates
  // the first one.  It takes many many fewer tests to locate a single
  // contact this way.

  RAPID_Collide(R1, T1, b1, R2, T2, b2, RAPID_FIRST_CONTACT);

  printf("First contact between overlapping tori:\n");
  
  printf("Num box tests: %d\n", RAPID_num_box_tests);
  printf("Num contact pairs: %d\n", RAPID_num_contacts);
#if LISTS
  for(i=0; i<RAPID_num_contacts; i++)
    {
      printf("\t contact %4d: tri %4d and tri %4d\n", 
             i, RAPID_contact[i].id1, RAPID_contact[i].id2);
    }
#endif

  // By rotating one of them around the x-axis 90 degrees, they 
  // are now interlocked, but not quite touching.

  R1[0][0] = 1.0;  R1[0][1] = 0.0;  R1[0][2] = 0.0;
  R1[1][0] = 0.0;  R1[1][1] = 0.0;  R1[1][2] =-1.0;
  R1[2][0] = 0.0;  R1[2][1] = 1.0;  R1[2][2] = 0.0;
  
  RAPID_Collide(R1, T1, b1, R2, T2, b2, RAPID_FIRST_CONTACT);

  printf("No contact between interlocked but nontouching tori:\n");
  
  printf("Num box tests: %d\n", RAPID_num_box_tests);
  printf("Num contact pairs: %d\n", RAPID_num_contacts);
#if LISTS
  for(i=0; i<RAPID_num_contacts; i++)
    {
      printf("\t contact %4d: tri %4d and tri %4d\n", 
             i, RAPID_contact[i].id1, RAPID_contact[i].id2);
    }
#endif

  // By moving one of the tori closer to the other, they
  // almost touch.  This is the case that requires a lot
  // of work wiht methods which use bounding boxes of limited
  // aspect ratio.  Oriented bounding boxes are more efficient
  // at determining noncontact than spheres, octree, or axis-aligned
  // bounding boxes for scenarios like this.  In this case, the interlocked
  // tori are separated by 0.0001 at their closest point.


  T1[0] = 1.5999;
  
  RAPID_Collide(R1, T1, b1, R2, T2, b2, RAPID_FIRST_CONTACT);

  printf("Many tests required for interlocked but almost touching tori:\n");
  
  printf("Num box tests: %d\n", RAPID_num_box_tests);
  printf("Num contact pairs: %d\n", RAPID_num_contacts);
#if LISTS
  for(i=0; i<RAPID_num_contacts; i++)
    {
      printf("\t contact %4d: tri %4d and tri %4d\n", 
             i, RAPID_contact[i].id1, RAPID_contact[i].id2);
    }
#endif

  return 0;  
}
   ___ End Spare ___________________________________________________________________________________________________________________________
*/
