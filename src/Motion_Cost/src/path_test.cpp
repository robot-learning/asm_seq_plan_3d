/*
level_exec.cpp
James Watson , 2018 June

% ROS Node %
Plan and execute one precedence level of a disassembly plan
Publises To ----> :
Subscribes To <-- :

Dependencies: ROS , Cpp_Helpers , ROS_Helpers
Template Version: 2018-06-06
*/

// === Init ================================================================================================================================

// ~~~ Includes ~~~
// ~~ ROS ~~
#include <ros/ros.h> // -------------- ROS , Publishers , Subscribers
#include <ros/package.h> // ------------------ Where are we? // http://wiki.ros.org/Packages#C.2B-.2B-
// ~ ROS Messages ~
#include "motion_cost/qFromPose.h"
#include "motion_cost/IKrequest.h"
#include "motion_cost/IKresponse.h"
#include "geometry_msgs/Pose.h"
#include "sensor_msgs/JointState.h"
#include <ll4ma_teleop/Joints2Jac.h>
#include <ll4ma_teleop/LinkPoses.h>
#include <ll4ma_teleop/FngrPoses.h>
// ~~ Local ~~
#include <Cpp_Helpers.h> // - C++ Utilities and Shortcuts
#include <ROS_Helpers.h> // - ROS Utilities and Shortcuts
#include <Model_Factory.h> // Assemblies to plan on
#include <AsmSeqPlan.h> // -- Identify grasp pairs , ASP
#include <comm_ASM.h> // ---- Messages and visualization
#include <Motion_Planning.h>

// ___ End Init ____________________________________________________________________________________________________________________________


/// ######### Node Vars #########

string /* -- */ NODE_NAME  = "Path_Test";
int /* ----- */ RATE_HZ    = 30;
int /* ----- */ QUEUE_LEN  = 30;
Eigen::Vector3d ROBOT_BASE = Eigen::Vector3d( 0.55 , 0 , 1.20 ); // FUTURE: NEED A WAY TO LOAD THIS FROM A PARAM SERVER

/// ********* End Vars *********


// === Program Functions & Classes ===

struct DummyCollision{
	bool operator()( const Pose_ASP& pose ){  return false;  }
} always_collsn_free;

void load_RRT_soln_into_mrkrMngr( RViz_MarkerManager& mngr , 
								  const std::vector<std::pair<Pose_ASP,Pose_ASP>>& allEdges , 
								  const std::vector<Pose_ASP>& solnPath , 
								  double epsilonSize = 0.1 ){
									  
	bool SHOWDEBUG = true; // if( SHOWDEBUG ){  cout << "" << endl;  }
									  
	// 1. Paint all of the edges
	size_t lenEdge  = allEdges.size() , 
		   lenSoln  = solnPath.size() ;
	float  edgeThic = epsilonSize / 50.0 ,
		   edgeAlfa = 0.500 ;
	float  solnThic = edgeThic * 2 ,
		   solnAlfa = 1.000 ;
	std::vector<float> edgeColor = { 0.0 , 0.0 , 1.0 };
	std::vector<float> solnColor = { 0.0 , 1.0 , 0.0 };
	std::pair<Pose_ASP,Pose_ASP> currEdge;
	Pose_ASP pose1;  Pose_ASP pose2;
	
	if( SHOWDEBUG ){  cout << "About to render edges ..." << endl;  }
	
	// A. For each edge
	for( size_t i = 0 ; i < lenEdge ; i++ ){
		// B. Get a straight-path marker
		currEdge = allEdges[i];
		// C. Add it to the manager
		mngr.add_marker( 
			get_straightPath_marker( (std::get<0>(currEdge)).position , (std::get<1>(currEdge)).position , 
									 edgeThic , edgeColor[0] , edgeColor[1] , edgeColor[2] , edgeAlfa )
		);
	}
	
	if( SHOWDEBUG ){  cout << "About to render solution ..." << endl;  }
	
	// 2. Paint the solution path
	if( lenSoln > 0 ){
		// A. Fetch the first point
		pose1 = solnPath[0];
		// B. For each following point
		for( size_t j = 1 ; j < lenSoln ; j++ ){
			// C. Paint a straight-path from the last point to this point
			pose2 = solnPath[j];
			mngr.add_marker( 
				get_straightPath_marker( pose1.position , pose2.position , 
										 solnThic , solnColor[0] , solnColor[1] , solnColor[2] , solnAlfa )
			);
			pose1 = pose2;
		}
	}else if( SHOWDEBUG ){  cout << "There was no solution to render!" << endl;  }
	if( SHOWDEBUG ){  cout << "Rendering complete!" << endl;  }
}

// 3. Set up the collision checker
struct AsmCollision{
	Assembly_ASM* movd;
	Assembly_ASM* refc;
	bool operator()( const Pose_ASP& pose ){  
		
		bool SHOWDEBUG = true  , 
			 collidesw = false ;
		
		movd->set_pose( pose );
		
		collidesw = movd->collides_with( *refc );
		
		if( SHOWDEBUG && collidesw ){  cout << "COLLISION" << endl;  }
		
		return collidesw;
	}
};

// ___ End Functions & Classes ___


// === Program Vars ===

bool CONNECT_TO_ARM = false ,
     NO_OBSTRUCTION = false ,
     WITH_OBSTRUCTN = true  ;

// ___ End Vars ___


// === main ================================================================================================================================

int main( int argc , char** argv ){ // Main takes the terminal command and flags that called it as arguments
	srand( time( 0 ) ); // Random seed based on the current clock time
	
	// 0. Init ROS  &&  Register node
	ros::init( argc , argv , NODE_NAME );
	
	// 1. Fetch handle to this node
	ros::NodeHandle nodeHandle;
	
	// 2. Init node rate
	ros::Rate heartbeat( RATE_HZ );
	
	// 3. Set up subscribers and publishers
	
	// ~ Publishers ~
	if( CONNECT_TO_ARM ){
		ros::Publisher joint_cmd_pub = nodeHandle.advertise<sensor_msgs::JointState>( "/lbr4_teleop/joint_cmd" , QUEUE_LEN );
		send_joint_command jntCmd_Call{ joint_cmd_pub };
	}
	
	ros::Publisher vis_arr       = nodeHandle.advertise<visualization_msgs::MarkerArray>( "visualization_marker_array" , QUEUE_LEN );
	
	// ~ Subscribers ~
	// ros::Subscriber SUBSCRIBER_OBJ = nodeHandle.subscribe( "TOPIC_NAME" , QUEUE_LEN , CALLBACK_FUNCTION );
	
	// ~ Service Servers ~
	// ros::ServiceServer SERVER_OBJ = nodeHandle.advertiseService( "SERVICE_NAME" , SERV_CALLBACK );
	
	// ~ Service Clients ~
	
	if( CONNECT_TO_ARM ){
		ros::ServiceClient  IK_Client = nodeHandle.serviceClient<motion_cost::qFromPose>( "q_from_pose" );
		request_IK_solution IK_clientCall{ IK_Client };
		
		ros::ServiceClient  J_Client = nodeHandle.serviceClient<ll4ma_teleop::Joints2Jac>( "/robot_commander/get_Jacobian" );
		request_Jacobian    J_ClientCall{ J_Client };
		
		ros::ServiceClient  FK_Client = nodeHandle.serviceClient<ll4ma_teleop::LinkPoses>( "/robot_commander/get_links_FK" );
		request_FK_poses    FK_ClientCall{ FK_Client };

		ros::ServiceClient  FNG_Client = nodeHandle.serviceClient<ll4ma_teleop::FngrPoses>( "/robot_commander/get_fingr_FK" );
		request_FNG_poses   FNG_ClientCall{ FNG_Client };
	}
	
	
	// N-1. Animation Init 
	srand( ( time( 0 ) % 1 ) * 1000.0 + getpid() ); // Random seed based on the current clock time
	RViz_MarkerManager mrkrMngr{};
	visualization_msgs::MarkerArray& markerArr = mrkrMngr.get_arr();
	
	
	/// === Preliminary { Setup , Instantiation , Planning  } ==============================================================================

	if( NO_OBSTRUCTION ){
		// I. RRT without obstruction

		cout << "Init vars ..." << endl;
		
		size_t /* -- */ pK          = 1500;
		double /* -- */ pEpsilon    = 0.05;
		double /* -- */ pBiasProb   = 0.15;
		Eigen::Matrix<double, 2, 3> pXbounds = Eigen::MatrixXd::Zero( 2 , 3 );  
		Pose_ASP /*- */ pStart      = Pose_ASP{ Eigen::Vector3d( 0.5 , 0.5 , 0.1 ) , no_turn_quat() };
		Pose_ASP /*- */ pGoal       = Pose_ASP{ Eigen::Vector3d( 0.5 , 0.5 , 0.9 ) , no_turn_quat() };
		double /* -- */ pWeightPosn = 1.0;
		double /* -- */ pWeightOrnt = 1.0;
		double /* -- */ nghbrRadius = pEpsilon * 2.5;
		pXbounds.row(0) = Eigen::Vector3d( 0 , 0 , 0 );  
		pXbounds.row(1) = Eigen::Vector3d( 1 , 1 , 1 );
		
		cout << "Constructed a bounds matrix:" << endl << pXbounds << endl;
		
		cout << "Init solver ..." << endl;
		RRT_Solver<DummyCollision> rrtSolver{ pK , pEpsilon , pBiasProb , pXbounds , always_collsn_free ,
											  pStart , pGoal , pWeightPosn , pWeightOrnt };
		
		StopWatch algoTimer;				
						
		cout << "Run solver on problem without obstructions ..." << endl;		
		cout << "From: " << pStart << " , To: " << pGoal << endl;
		std::vector<Pose_ASP> easySoln = rrtSolver.build_rrt_star( pStart , pGoal , nghbrRadius , 0 );
		cout << "Solution contains " << easySoln.size() << " poses" << endl 
			 << algoTimer.seconds_elapsed() << " seconds elapsed" << endl;
			 
		if( easySoln.size() > 0 ){
			cout << "Solution Path:" << endl;
			for( size_t i = 0 ; i < easySoln.size() ; i++ ){
				cout << easySoln[i] << endl;
			}
		}
		
		cout << "There are " << rrtSolver.allTrees[0]->rootNode->get_distal_edges().size() << " edges in the solver at exit" << endl;
		
		load_RRT_soln_into_mrkrMngr( mrkrMngr , 
									 rrtSolver.allTrees[0]->rootNode->get_distal_edges() , 
									 easySoln , 
									 pEpsilon );
	}
					
					
	if( WITH_OBSTRUCTN ){
		// II. RRT WITH obstruction
		
		Eigen::Vector3d floorCenter{ 0.0 , 0.0 , 0.0 };
		
		// 1. Create the parts and mark the offset
		Assembly_ASM* obstrCube = simple_cube_obstr();
		obstrCube->set_lab_pose_for_index_3D( 0 , floorCenter );
		Pose_ASP setDownPose = obstrCube->get_pose();
		//~ obstrCube->load_part_mrkrs_into_mngr( mrkrMngr );
		
		std::vector<llin> obstID = { 0 };
		std::vector<llin> cubeID = { 1 };
		
		// 2. Split the asm into two parts
		Assembly_ASM* obst = obstrCube->sub_from_spec( obstID , (size_t)0 );  obst->set_pose( setDownPose );
		Assembly_ASM* cube = obstrCube->sub_from_spec( cubeID , (size_t)0 );  cube->set_pose( setDownPose );
		
		obst->load_part_mrkrs_into_mngr( mrkrMngr );
		cube->load_part_mrkrs_into_mngr( mrkrMngr );
		
		//~ return 1;
		
		// 3. Set up the collision checker
		AsmCollision cubeCollide{ cube , obst };
		
		size_t /* -- */ pK          = 2000;
		double /* -- */ pEpsilon    = 0.005;
		double /* -- */ pBiasProb   = 0.15;
		Eigen::MatrixXd pXbounds    = Eigen::MatrixXd::Zero( 2 , 3 );  
		Pose_ASP /*- */ pStart      = setDownPose; 
		Pose_ASP /*- */ pGoal       = Pose_ASP{ setDownPose.position + Eigen::Vector3d( 0.0 , 0.0 , 0.5 ) , setDownPose.orientation };
		double /* -- */ pWeightPosn = 1.0;
		double /* -- */ pWeightOrnt = 1.0;
		double /* -- */ nghbrRadius = pEpsilon * 2.5;
		pXbounds.row(0) = Eigen::Vector3d( -0.5 , -0.5 , 0.0 );  
		pXbounds.row(1) = Eigen::Vector3d(  0.5 ,  0.5 , 1.0 );
		
		cout << "Init solver ..." << endl;
		RRT_Solver<AsmCollision> rrtSolver{ pK , pEpsilon , pBiasProb , pXbounds , cubeCollide ,
											pStart , pGoal , pWeightPosn , pWeightOrnt };
											
		StopWatch algoTimer;				
						
		cout << "Run solver on problem without obstructions ..." << endl;		
		cout << "From: " << pStart << " , To: " << pGoal << endl;
		std::vector<Pose_ASP> hardSoln = rrtSolver.build_rrt_star( pStart , pGoal , nghbrRadius , 0 );
		cout << "Solution contains " << hardSoln.size() << " poses" << endl 
			 << algoTimer.seconds_elapsed() << " seconds elapsed" << endl;
			 
		if( hardSoln.size() > 0 ){
			cout << "Solution Path:" << endl;
			for( size_t i = 0 ; i < hardSoln.size() ; i++ ){
				cout << hardSoln[i] << endl;
			}
		}
		
		cout << "There are " << rrtSolver.allTrees[0]->rootNode->get_distal_edges().size() << " edges in the solver at exit" << endl;
		
		cout << "About to render results ... " << endl;
		
		load_RRT_soln_into_mrkrMngr( mrkrMngr , 
									 rrtSolver.allTrees[0]->rootNode->get_distal_edges() , 
									 hardSoln , 
									 pEpsilon );
									 
		cout << "Rendered!" << endl;
		
		// III. Verify solution
		if( hardSoln.size() > 0 ){
			sep( "Verify Solution" );
			bool collidesw = false;
			
			// 1. For each pose
			for( size_t i = 0 ; i < hardSoln.size() ; i++ ){
				// 2. Set the mover to the pose
				cube->set_pose( hardSoln[i] );
				// 3. Check collision
				collidesw = cube->collides_with( *obst );
				if( collidesw ){
					cout << "COLLISION at step " << i+1 << endl;
					break;
				}
				// 4. Render mover
				cube->load_part_mrkrs_into_mngr( mrkrMngr );
			}
			cout << "Verification complete! OK?: " << !collidesw << endl;
		}
		
	}
	
	/// ___ End Preliminary ________________________________________________________________________________________________________________
	
	// N-2. Notify
	ros_log( "[" + NODE_NAME + "] Init OK and about to run ..." , INFO );
	
	// N. Main loop
	while( ros::ok() ){ // While neither node nor ROS has been shut down
		
		/// == NODE WORK ===================================================================================================================
		
		
		if( CONNECT_TO_ARM ){	
			// 4. Update markers
			mrkrMngr.rebuild_mrkr_array();
			
		}
		
		// 5. Paint frame
		vis_arr.publish( markerArr );
		
		
		//~ break; // ONLY ONCE
		
		/// __ END WORK ____________________________________________________________________________________________________________________
		
		ros::spinOnce(); // - Process messages
		heartbeat.sleep(); // Sleep for remainder of period
	}
	
	// N+1. Notify  &&  Exit
	
	ros_log( "[" + NODE_NAME + "] Exit OK, Goodbye!" , INFO );
	
	return 0; // I guess everything turned out alright at the end!
}

// ___ End main ____________________________________________________________________________________________________________________________


/* === Spare Parts =========================================================================================================================



   ___ End Spare ___________________________________________________________________________________________________________________________
*/
