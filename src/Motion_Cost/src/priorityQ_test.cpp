/*
priorityQ_test.cpp
James Watson , 2018 March
Test the standard priority queue and standard pair
RESULT: The TOP element of the priority queue has the LARGEST value

Dependencies:
Template Version: 2017-09-23
*/

/* C++ Checklist 
1. [Y] Appropriate #include */

// === Init ================================================================================================================================

// ~~~ Includes ~~~
// ~~ Standard ( Common includes not already in Cpp_Helpers.h ) ~~ 
//#include <iomanip> // - Output precision and printing
//#include <exception> // error handling
//#include <stdexcept> // library provided error types like 'std::range_error'
//#include <tuple> // --- standard tuple datatype
//#include <cstddef> // - alias types names:  size_t, ptrdiff_t
//#include <cstring> // - C Strings: strcomp 
//#include <algorithm> // sort, search, min, max, sequence operations

// ~~ Special ~~

// ~~ Local ~~ 
#include <Cpp_Helpers.h> // Shortcuts and Aliases , and Functions of general use for C++ programming

// ___ End Init ____________________________________________________________________________________________________________________________


// === Program Functions & Classes ===

//~ bool priorityQ_less_than( std::pair<double,string>& op1 , std::pair<double,string>& op2 ){
	//~ return std::get<0>( op1 ) < std::get<0>( op2 );
//~ }

struct priorityQ_less_than {
	bool operator()( const std::pair<double,string>& op1 , const std::pair<double,string>& op2 ) const{
		return std::get<0>( op1 ) < std::get<0>( op2 );
	}
};

// ___ End Functions & Classes ___


// === Program Vars ===

// ~~ Stack Vars ~~

// ~~ Heap Structs ~~

// ___ End Vars ___


// === main ================================================================================================================================

int main( int argc , char** argv ){ // Main takes the terminal command and flags that called it as arguments
	
	srand( time( 0 ) ); // Random seed based on the current clock time
	//~ sep( "I EXIST!" ); // Silly output test
	
	// Instantiate the priority queue
	std::priority_queue< std::pair< double , string >                , 
                         std::vector< std::pair< double , string > > ,
                         priorityQ_less_than                         > frontier;
                    
	std::pair< double , string > temp;
	temp = std::make_pair( 3.0 ,  "MOST"   );  frontier.push( temp ); // Push the elements out of order
	temp = std::make_pair( 1.0 ,  "LEAST"  );  frontier.push( temp );
	temp = std::make_pair( 2.0 ,  "MIDDLE" );  frontier.push( temp );
	
	
	//~ for( size_t i = 0 ; i < 3 ; i++ ){
	while( !frontier.empty() ){
		temp = frontier.top(); // Retrieve the top element
		cout << "Popped: <" << std::get<0>( temp ) << " , " << std::get<1>( temp ) << " >" << endl;
		frontier.pop(); // Pop the top element
	}
	cout << "Frontier empty!" << endl;
	
	return 0; // I guess everything turned out alright at the end!
}

// ___ End main ____________________________________________________________________________________________________________________________


/* === Spare Parts =========================================================================================================================



   ___ End Spare ___________________________________________________________________________________________________________________________
*/
