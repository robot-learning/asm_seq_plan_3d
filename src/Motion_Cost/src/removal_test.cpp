/*
removal_test.cpp
James Watson , 2018 February
Test integration of the ASP_3D lib --> ROS , Show markers and move them around

Dependencies: Cpp_Helpers , ROS , Eigen , RAPID
Template Version: 2017-09-23
*/

// === Init ================================================================================================================================

// ~~~ Includes ~~~
// ~~ ROS ~~
// ~ Core ~
#include <ros/ros.h> // --- ROS presides over all
#include <ros/package.h> // Where are we? // http://wiki.ros.org/Packages#C.2B-.2B-
// ~~ Includes ~~
#include <Eigen/Core> // ---- The living heart of Eigen
#include <Eigen/Dense> // --- Cross Product , etc.
#include <Eigen/Geometry> //- Quaternion , etc
// ~ RViz ~
#include <visualization_msgs/Marker.h> // ---- Display geometry
#include <visualization_msgs/MarkerArray.h> // RViz marker array
// ~~ Local ~~ 
#include <Cpp_Helpers.h> // Shortcuts and Aliases , and Functions of general use for C++ programming
#include <ASP_3D.h> // ---- Assembly planning


// using namespace quickhull;

// ___ End Init ____________________________________________________________________________________________________________________________


// === Program Functions & Classes ===

void load_RAPID_model( RAPID_model* rapidModel , Eigen::MatrixXd V , Eigen::MatrixXi F ){
	// Load all of the points into 'rapidModel' , Which stores points in the C style
	int counter = 0; // Triangle ID counter
	int len     = F.rows();
	double p0[3], p1[3], p2[3]; // R3 points
	// 1. Prepare the model
	rapidModel->BeginModel();
	// 2. For each f_i
	for( int i = 0 ; i < len ; i++ ){
		// 3. For each dimension of v_{0,1,2}
		for( int j = 0 ; j < 3 ; j++ ){
			// 4. Load the point into the array
			p0[j] = V( F(i,0) , j );
			p1[j] = V( F(i,1) , j );
			p2[j] = V( F(i,2) , j );
		}
		// 5. Load the facet, and give it a unique number (unique to the model, does not have to be unique globally)
		rapidModel->AddTri( p0 , p1 , p2 , counter );
		counter++;
	}
	// 6. End model input
	rapidModel->EndModel();
}

void load_RAPID_transform( double T[3] , double R[3][3] , Pose_ASP partPose ){
	// Extract the pose information 'partPose' and load it into 'T' and 'R' , as RAPID expects
	for( size_t i = 0 ; i < 3 ; i++ ){
		T[i] = partPose.homogMatx( i , 3 ); // Transfer the Pose_ASP translation to the RAPID translation
		for( size_t j = 0 ; j < 3 ; j++ ){
			R[i][j] = partPose.homogMatx( i , j );
		}
	}
}

// ___ End Functions & Classes ___


// === Program Vars ===

Incrementer assign_part_ID{ 0 }; // Instantiate a functor that will assign a unique number to each part
Incrementer assign_asm_ID{ 0 }; //- Instantiate a functor that will assign a unique number to each assembly

// ___ End Vars ___


// === main ================================================================================================================================

int main( int argc , char** argv ){
	
	srand( time( 0 ) ); // Random seed based on the current clock time
	
	// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
	
	sep( "Remove Part From Asm" );
	
	// 0. Create file paths
	string pkgPath = ros::package::getPath( "motion_cost" );
	string EL_fileName  = pkgPath + "/Resources/EL_SCLD.stl";
	string CEE_fileName = pkgPath + "/Resources/CEE_SCLD.stl";
	string CUBE_fileName = pkgPath + "/Resources/CUBE.stl";
	cout << "Path to EL : " << EL_fileName  << endl;
	cout << "Path to CEE: " << CEE_fileName << endl;
	
	// 1. Create part object(s)
	Part_ASM EL_part{   assign_part_ID() , EL_fileName   }; // 0 // Moved part
	Part_ASM CEE_part{  assign_part_ID() , CEE_fileName  }; // 1 // Reference part
	Part_ASM CUBE_part{ assign_part_ID() , CUBE_fileName }; // 2 // Not used
		
	std::vector< Polygon_ASM > EL_faces  = EL_part.get_all_faces();
	cout << "EL has  " << EL_faces.size()  << " faces." << endl;
	std::vector< Polygon_ASM > CEE_faces = CEE_part.get_all_faces();
	cout << "CEE has " << CEE_faces.size() << " faces." << endl;	
		
	// 2. Create an assembly , add parts
	Assembly_ASM testAsm{ assign_asm_ID() };
	testAsm.add_part_w_pose( EL_part   , Pose_ASP{ Eigen::Vector3d(  0.2 * ( 0.25 - 0.075 ) , 0.0 , 0.2 * 0.125 ) , 
												   Eigen::Quaterniond(  0.000 ,  0.000 ,  0.707 , -0.707 ) } );
	testAsm.add_part_w_pose( CEE_part  , Pose_ASP{ Eigen::Vector3d(  0.0 ,  0.0 ,  0.0 ) , 
												   Eigen::Quaterniond(  1.000 ,  0.000 ,  0.000 ,  0.000 ) } );
	testAsm.set_pose( origin_pose() ); // Don't forget to set the asm pose!
	//~ testAsm.set_pose( Pose_ASP{ Eigen::Vector3d( 0 , 0 , 1 ) , Eigen::Quaterniond( 0.924 , 0.383 , 0.000 , 0.000 ) } );
	// 3. Determine the local freedom of each part (NDBG)
	testAsm.calc_NDBG();
	
	interference_result result;
	std::vector<size_t> queryParts;      queryParts.push_back(     0 ); // EL
	std::vector<size_t> referenceParts;  referenceParts.push_back( 1 ); // CEE
	cout << "Assembly NDBG Queries in the Lab Frame..." << endl;
	
	result = testAsm.query_asm_NDBG( queryParts , referenceParts , 
									   Eigen::Vector3d(  1.0 ,  0.0 ,  0.0 ) );
	cout << "Attempting the remove the query part via (  1.0 ,  0.0 ,  0.0 ) , result: " << result.result << " , "
		 << result.interferingPartIDs << endl;
		 
	result = testAsm.query_asm_NDBG( queryParts , referenceParts , 
									   Eigen::Vector3d(  1.0 ,  0.0 ,  1.0 ) );
	cout << "Attempting the remove the query part via (  1.0 ,  0.0 ,  1.0 ) , result: " << result.result << " , "
		 << result.interferingPartIDs << endl;
	
	result = testAsm.query_asm_NDBG( queryParts , referenceParts , 
									   Eigen::Vector3d(  0.0 ,  0.0 ,  1.0 ) );
	cout << "Attempting the remove the query part via (  0.0 ,  0.0 ,  1.0 ) , result: " << result.result << " , "
		 << result.interferingPartIDs << endl;
		 
	result = testAsm.query_asm_NDBG( queryParts , referenceParts , 
									   Eigen::Vector3d( -1.0 ,  0.0 ,  1.0 ) );
	cout << "Attempting the remove the query part via ( -1.0 ,  0.0 ,  1.0 ) , result: " << result.result << " , "
		 << result.interferingPartIDs << endl;
		 
	result = testAsm.query_asm_NDBG( queryParts , referenceParts , 
									   Eigen::Vector3d( -1.0 ,  0.0 ,  0.0 ) );
	cout << "Attempting the remove the query part via ( -1.0 ,  0.0 ,  0.0 ) , result: " << result.result << " , "
		 << result.interferingPartIDs << endl;
		 
	result = testAsm.query_asm_NDBG( queryParts , referenceParts , 
									   Eigen::Vector3d( -1.0 ,  0.0 , -1.0 ) );
	cout << "Attempting the remove the query part via ( -1.0 ,  0.0 , -1.0 ) , result: " << result.result << " , "
		 << result.interferingPartIDs << endl;
		 
	result = testAsm.query_asm_NDBG( queryParts , referenceParts , 
									   Eigen::Vector3d(  0.0 ,  0.0 , -1.0 ) );
	cout << "Attempting the remove the query part via (  0.0 ,  0.0 , -1.0 ) , result: " << result.result << " , "
		 << result.interferingPartIDs << endl;
		 
	result = testAsm.query_asm_NDBG( queryParts , referenceParts , 
									   Eigen::Vector3d(  1.0 ,  0.0 , -1.0 ) );
	cout << "Attempting the remove the query part via (  1.0 ,  0.0 , -1.0 ) , result: " << result.result << " , "
		 << result.interferingPartIDs << endl;
		 
	Eigen::Vector3d suggestDirLab = testAsm.asm_center_of_freedom_direction( queryParts , referenceParts );
	cout << "Suggested removal direction: " << suggestDirLab << endl;
	
	
	cout << endl << "Now rotate the entire assembly and repeat the process ..." << endl << endl;
	
	testAsm.set_pose( Pose_ASP{ Eigen::Vector3d(  0.0 ,  0.0 ,  0.0 ) , 
							    Eigen::Quaterniond(  0.000 ,  0.000 ,  0.000 ,  1.000 ) } );
							    
	result = testAsm.query_asm_NDBG( queryParts , referenceParts , 
									   Eigen::Vector3d(  1.0 ,  0.0 ,  0.0 ) );
	cout << "Attempting the remove the query part via (  1.0 ,  0.0 ,  0.0 ) , result: " << result.result << " , "
		 << result.interferingPartIDs << endl;
		 
	result = testAsm.query_asm_NDBG( queryParts , referenceParts , 
									   Eigen::Vector3d(  1.0 ,  0.0 ,  1.0 ) );
	cout << "Attempting the remove the query part via (  1.0 ,  0.0 ,  1.0 ) , result: " << result.result << " , "
		 << result.interferingPartIDs << endl;
	
	result = testAsm.query_asm_NDBG( queryParts , referenceParts , 
									   Eigen::Vector3d(  0.0 ,  0.0 ,  1.0 ) );
	cout << "Attempting the remove the query part via (  0.0 ,  0.0 ,  1.0 ) , result: " << result.result << " , "
		 << result.interferingPartIDs << endl;
		 
	result = testAsm.query_asm_NDBG( queryParts , referenceParts , 
									   Eigen::Vector3d( -1.0 ,  0.0 ,  1.0 ) );
	cout << "Attempting the remove the query part via ( -1.0 ,  0.0 ,  1.0 ) , result: " << result.result << " , "
		 << result.interferingPartIDs << endl;
		 
	result = testAsm.query_asm_NDBG( queryParts , referenceParts , 
									   Eigen::Vector3d( -1.0 ,  0.0 ,  0.0 ) );
	cout << "Attempting the remove the query part via ( -1.0 ,  0.0 ,  0.0 ) , result: " << result.result << " , "
		 << result.interferingPartIDs << endl;
		 
	result = testAsm.query_asm_NDBG( queryParts , referenceParts , 
									   Eigen::Vector3d( -1.0 ,  0.0 , -1.0 ) );
	cout << "Attempting the remove the query part via ( -1.0 ,  0.0 , -1.0 ) , result: " << result.result << " , "
		 << result.interferingPartIDs << endl;
		 
	result = testAsm.query_asm_NDBG( queryParts , referenceParts , 
									   Eigen::Vector3d(  0.0 ,  0.0 , -1.0 ) );
	cout << "Attempting the remove the query part via (  0.0 ,  0.0 , -1.0 ) , result: " << result.result << " , "
		 << result.interferingPartIDs << endl;
		 
	result = testAsm.query_asm_NDBG( queryParts , referenceParts , 
									   Eigen::Vector3d(  1.0 ,  0.0 , -1.0 ) );
	cout << "Attempting the remove the query part via (  1.0 ,  0.0 , -1.0 ) , result: " << result.result << " , "
		 << result.interferingPartIDs << endl;
		 
	suggestDirLab = testAsm.asm_center_of_freedom_direction( queryParts , referenceParts );
	cout << "Suggested removal direction: " << suggestDirLab << endl;
	
	cout << "Simulation success lifting 0 from 1 along (  0.0 ,  0.0 ,  1.0 ): "
		 << testAsm.test_translation_sub_removal( queryParts , referenceParts , 
												  Eigen::Vector3d(  0.0 ,  0.0 ,  1.0 ) , 0.5 , 20 )
		 << endl;
		 
	cout << "Simulation success lifting 0 from 1 along (  0.0 ,  1.0 ,  0.0 ): "
		 << testAsm.test_translation_sub_removal( queryParts , referenceParts , 
												  Eigen::Vector3d(  0.0 ,  1.0 ,  0.0 ) , 0.5 , 20 )
		 << endl;
	
	cout << endl;
	
	// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
	
	sep( "Validating Removal with Stability Considerations" );
	
	std::vector<llin> remainingPartsID;  remainingPartsID.push_back( 1 );  // CEE will stay where it is
	
	SupportHull CEE_hull = testAsm.determine_subasm_stability( remainingPartsID );
	
	cout << "Found " << CEE_hull.supports.size() << " supports" << endl;
	cout << "COM located at " << CEE_hull.COM << endl;
	cout << "Convex hull has " << CEE_hull.hullVFN.F.rows() << " facets" << endl;
	cout << "Calculated stability quality for " << CEE_hull.spptEdgeDist.size() << " supports" << endl;
	cout << "Stability reported " << CEE_hull.stableFace/*.size()*/ << endl;
	cout << "Total volume of contained parts " << CEE_hull.volume << endl;
	
	// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
	
	sep( "Visualizing Removal" );
	
	std::vector<llin> movParts;  movParts.push_back( 0 ); // EL
	std::vector<llin> refParts;  refParts.push_back( 1 ); // CEE
	
	Eigen::Vector3d remDir;
	Pose_ASP remPartPose;
	
	testAsm.determine_support_stability();
	testAsm.calc_NDBG();
	std::vector<llin> NDBG_PartID_Lookup = testAsm.get_NDBG_PartID_Lookup();
	cout << "NDBG_PartID_Lookup: " << NDBG_PartID_Lookup << endl;
	
	SuccessCode actionValid;
	
	/// === VISUALIZATION ==================================================================================================================
	
	// == ROS Loop ==
	
	// = ROS Start =
	
	// 5. Start ROS
	// ~ Animation Vars ~
	srand( ( time( 0 ) % 1 ) * 1000.0 + getpid() ); // Random seed based on the current clock time
	int updateHz = 30;
	//~ Eigen::Quaterniond deltaQuat{ 0.990 , 0.059 , 0.064 , 0.108 }; // Turn by this much every frame
	Eigen::Quaterniond deltaEX{ 0.999 , 0.052 , 0.000 , 0.000 }; // Turn by this much every frame
	Eigen::Quaterniond deltaEY{ 0.999 , 0.000 , 0.052 , 0.000 }; // Turn by this much every frame
	Eigen::Quaterniond deltaEZ{ 0.999 , 0.000 , 0.000 , 0.052 }; // Turn by this much every frame
	Pose_ASP temp;
	
	double remLen = 1.0d;
	Eigen::Vector3d endPnt;
	
	float validColor[4] = { 0.0 , 1.0 , 0.0 , 1.0 };
	float faildColor[4] = { 1.0 , 0.0 , 0.0 , 1.0 };
	float markrColor[4];
	
	// Connect to ROS
	ros::init( argc , argv , "adjacency_test_cpp" );
	// Set up a node with refresh rate
	ros::NodeHandle node_handle;
	ros::Publisher vis_arr = node_handle.advertise<visualization_msgs::MarkerArray> ( "visualization_marker_array" , 100 );
	ros::Rate r( updateHz ); // 30Hz refresh rate
	
	// Set up visualization management
	RViz_MarkerManager mrkrMngr{};
	visualization_msgs::MarkerArray& markerArr = mrkrMngr.get_arr();
	
	
	std::vector<bool> stabilityVec = testAsm.get_stability_determination();
	size_t len = stabilityVec.size();
	cout << "There are " << len << " supports for this assembly" << endl;
	Eigen::Vector2d offsetStep{ 0.30 , 0.00 };
	Eigen::Vector2d putdown_XY{ 0.00 , 0.00 };
	
	// ~ Coordinate Axes ~
	mrkrMngr.add_marker( get_axes_marker( 5 , 0.01f ) );
	
	// 1. For each of the possible supports
	for( size_t i = 0 ; i < len ; i++ ){

		// 2. Determine if the support is stable
		if( stabilityVec[i] ){
			
			cout << "~ Support " << i << " ~" << endl;
			
			// 3. Calc the offset
			putdown_XY += offsetStep;
			
			// 4. Set the putdown pose
			testAsm.set_pose( testAsm.get_lab_pose_for_support_index( i , putdown_XY ) );
			
			// 5. For each of the faces , create a marker and load them into the marker manager , cycling through colors
			testAsm.load_face_mrkrs_into_mngr( mrkrMngr );
			
			// 6. Obtain the prefered removal direction
			remDir = testAsm.asm_center_of_freedom_direction( movParts , refParts );
			cout << "\t" << "Suggested removal direction: " << remDir << endl;
			
			// 7. Validate removal including floor in the preferred direction
			actionValid = testAsm.validate_removal_staightline_stability( movParts , refParts , remDir , i );
			cout << "\t" << "Success in suggested dir: " << actionValid.success << " because " << actionValid.desc << endl 
			     << endl << endl;
			
			// 8. Display path and color success
			remPartPose = testAsm.get_lab_pose_w_ID( movParts[0] );
			if( actionValid.success ){  memcpy( markrColor , validColor , sizeof(markrColor) );  }
			else{  memcpy( markrColor , faildColor , sizeof(markrColor) );  }
			
			mrkrMngr.add_marker( get_straigtPath_marker( remPartPose.position , remPartPose.position + remDir * remLen , 
														 0.01f , markrColor[0] , markrColor[1] , markrColor[2] , markrColor[3] ) );
			
		}
	}
	
	
	
	
	// 6. Set the animation loop and display
	while ( ros::ok() ){ // while roscore is running
        // cout << ".";

		// Wait for a subscriber to start looking for this topic
		while ( vis_arr.getNumSubscribers() < 1 ){
			if ( !ros::ok() ){ return 0; }
			printf( "." );
			ROS_WARN_ONCE( "Please create a subscriber to the marker" );
			sleep( 1 );
			cout << ".";
		} 
        
        vis_arr.publish( markerArr );

		r.sleep();
	}
	
	
	/// ___ END VISUAL _____________________________________________________________________________________________________________________
	
	return 0; // I guess everything turned out alright at the end!
}

// ___ End main ____________________________________________________________________________________________________________________________


/* === Spare Parts =========================================================================================================================


~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

		//~ // 1. Load the appropriate pose structures
        //~ load_RAPID_transform( T1 , R1 , EL_part.get_pose_w_matx()  );
        //~ load_RAPID_transform( T2 , R2 , CEE_part.get_pose_w_matx() );
        //~ // 2. Check for collision
        //~ RAPID_Collide( R1 , T1 , EL_RAPID , R2 , T2 , CEE_RAPID , RAPID_FIRST_CONTACT ); // Shortcut on first triangle collision

~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

	//~ // 3. Calc supports
	
	//~ // ~~ EL ~~
	sep( "CUBE Supports" , 2 , '*' );
	CUBE_part.determine_support_stability();
	CUBE_part.report_support_stability();
	cout << endl;
	
	sep( "CEE Supports" , 2 , '*' );
	CEE_part.determine_support_stability();
	CEE_part.report_support_stability();
	cout << endl;
	
	sep( "EL Supports" , 2 , '*' );
	EL_part.determine_support_stability();
	EL_part.report_support_stability();
	cout << endl;
	
   ___ End Spare ___________________________________________________________________________________________________________________________
*/
