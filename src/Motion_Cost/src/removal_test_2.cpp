/*
removal_test_2.cpp
James Watson , 2018 July

% ROS Node %
Remove a sub with RRT*, considering the arm
Publises To ----> :
Subscribes To <-- :

Dependencies: ROS , Cpp_Helpers , ROS_Helpers
Template Version: 2018-06-06
*/

// === Init ================================================================================================================================

// ~~~ Includes ~~~
// ~~ ROS ~~
#include <ros/ros.h> // -------------- ROS , Publishers , Subscribers
#include <ros/package.h> // ------------------ Where are we? // http://wiki.ros.org/Packages#C.2B-.2B-
// ~ ROS Messages ~
#include "motion_cost/qFromPose.h"
#include "motion_cost/IKrequest.h"
#include "motion_cost/IKresponse.h"
#include "geometry_msgs/Pose.h"
#include "sensor_msgs/JointState.h"
#include <ll4ma_teleop/Joints2Jac.h>
#include <ll4ma_teleop/LinkPoses.h>
#include <ll4ma_teleop/FngrPoses.h>
// ~~ Local ~~
#include <Cpp_Helpers.h> // - C++ Utilities and Shortcuts
#include <ROS_Helpers.h> // - ROS Utilities and Shortcuts
#include <Model_Factory.h> // Assemblies to plan on
#include <AsmSeqPlan.h> // -- Identify grasp pairs , ASP
#include <comm_ASM.h> // ---- Messages and visualization
#include <Motion_Planning.h>

// ___ End Init ____________________________________________________________________________________________________________________________


/// ######### Node Vars #########

string /* -- */ NODE_NAME  = "Path_Test_w_Collision";
int /* ----- */ RATE_HZ    = 30;
int /* ----- */ QUEUE_LEN  = 30;

/// ********* End Vars *********


/// ######### Environment Vars #########

Eigen::Vector3d ROBOT_BASE = Eigen::Vector3d( 0.55 , 0 , 1.20 ); // FUTURE: NEED A WAY TO LOAD THIS FROM A PARAM SERVER
Eigen::Vector3d floorCenter{ 0.9 , 0.0 , 0.59 }; // Table center

///     ********* End Env *********

// === Program Functions & Classes ===

DummyCollision always_collsn_free;



// ___ End Functions & Classes ___


// === Program Vars ===

bool CONNECT_TO_ARM = true  ,
     NO_OBSTRUCTION = false ,
     WITH_OBSTRUCTN = true  ;

// ___ End Vars ___


// === main ================================================================================================================================

int main( int argc , char** argv ){ // Main takes the terminal command and flags that called it as arguments
	srand( time( 0 ) ); // Random seed based on the current clock time
	
	// 0. Init ROS  &&  Register node
	ros::init( argc , argv , NODE_NAME );
	
	// 1. Fetch handle to this node
	ros::NodeHandle nodeHandle;
	
	// 2. Init node rate
	ros::Rate heartbeat( RATE_HZ );
	
	// 3. Set up subscribers and publishers
	
	// ~ Publishers ~
	if( CONNECT_TO_ARM ){
		ros::Publisher joint_cmd_pub = nodeHandle.advertise<sensor_msgs::JointState>( "/lbr4_teleop/joint_cmd" , QUEUE_LEN );
		send_joint_command jntCmd_Call{ joint_cmd_pub };
	}
	
	ros::Publisher vis_arr       = nodeHandle.advertise<visualization_msgs::MarkerArray>( "visualization_marker_array" , QUEUE_LEN );
	
	// ~ Subscribers ~
	// ros::Subscriber SUBSCRIBER_OBJ = nodeHandle.subscribe( "TOPIC_NAME" , QUEUE_LEN , CALLBACK_FUNCTION );
	
	// ~ Service Servers ~
	// ros::ServiceServer SERVER_OBJ = nodeHandle.advertiseService( "SERVICE_NAME" , SERV_CALLBACK );
	
	// ~ Service Clients ~
	
	//~ if( CONNECT_TO_ARM ){
	ros::ServiceClient  IK_Client = nodeHandle.serviceClient<motion_cost::qFromPose>( "q_from_pose" );
	request_IK_solution IK_clientCall{ IK_Client };
	
	ros::ServiceClient  J_Client = nodeHandle.serviceClient<ll4ma_teleop::Joints2Jac>( "/robot_commander/get_Jacobian" );
	request_Jacobian    J_ClientCall{ J_Client };
	
	ros::ServiceClient  FK_Client = nodeHandle.serviceClient<ll4ma_teleop::LinkPoses>( "/robot_commander/get_links_FK" );
	request_FK_poses    FK_ClientCall{ FK_Client };

	ros::ServiceClient  FNG_Client = nodeHandle.serviceClient<ll4ma_teleop::FngrPoses>( "/robot_commander/get_fingr_FK" );
	request_FNG_poses   FNG_ClientCall{ FNG_Client };
	//~ }
	
	
	// N-1. Animation Init 
	srand( ( time( 0 ) % 1 ) * 1000.0 + getpid() ); // Random seed based on the current clock time
	RViz_MarkerManager mrkrMngr{};
	visualization_msgs::MarkerArray& markerArr = mrkrMngr.get_arr();
	
	
	/// === Preliminary { Setup , Instantiation , Planning  } ==============================================================================

	cout << "About to create assemblies ..." << endl;

	//  0. Set up the model without bridges or antenna
	std::vector<llin> partIDs = {  0 ,  9 ,  4 ,  5 , 10 ,  6 ,  7 , 11 ,  8  };
	Assembly_ASM* chipsOnlyPhone = simple_phone( partIDs );
	//  A. Situate in the setdown pose
	chipsOnlyPhone->set_lab_pose_for_index_3D( 0 , floorCenter );
	Pose_ASP initPose = chipsOnlyPhone->get_pose();
	//  B. Create sub for reference
	std::vector<llin> refcIDs = {  0 , 10 ,  6 ,  7 , 11 ,  8  };
	Assembly_ASM* refcAsm = chipsOnlyPhone->sub_from_spec( refcIDs );
	refcAsm->set_pose( initPose );
	//  C. Create sub for moved
	std::vector<llin> movdIDs = {  9 ,  4 ,  5 };
	Assembly_ASM* movdAsm = chipsOnlyPhone->sub_from_spec( movdIDs );
	movdAsm->set_pose( initPose );

	cout << "About to generate grasp pairs ..." << endl;

	//  1. Populate grasp pairs
	RayHits graspPairs = tile_ASM_grasps_orthogonal( movdAsm , 
													 Eigen::Vector3d::UnitX() , Eigen::Vector3d::UnitY() , Eigen::Vector3d::UnitZ() , 
													 0.5 , 0.005 );
	cout << "Got " << graspPairs.enter.rows() << " grasp pairs" << endl;
	//  2. Obtain a graph pair ranking
	std::vector<ParallelGraspState> rankedPairs = rank_grasp_pairs( graspPairs , 
																    Eigen::Vector3d::UnitZ() , floorCenter + Eigen::Vector3d( 0 , 0 , 0.25 ) , 
																	M_PI / 8 );
	size_t numRanked = rankedPairs.size() , 
		   winDex    = 0 ; // Winning index
	Pose_ASP winPose = err_pose();
	cout << "Ranked " << numRanked << " grasp states" << endl;
	//  3. Select a collision-free grasp
	ParallelGraspState currGrasp;
	bool graspFound = false ,
		 collision  = true  ;
	
	hand_collision_model handModel;  handModel.init();
	//  4. For each grasp in decreasing order
	for( size_t i = 0 ; i < numRanked ; i++ ){
		cout << i << " , ";
		currGrasp = rankedPairs[i];
		// A. Place the collision model according to the grasp state
		handModel.set_finger_state( currGrasp.q_fingers );
		handModel.set_pose( currGrasp.graspTarget );
		// B. Check collision
		collision = refcAsm->collides_with( handModel.LBR4hand );
		// C. If there is no collision , break
		if( !collision ){  
			winDex     = i;
			graspFound = true;
			winPose    = currGrasp.graspTarget;
			break;  
		}
		// D. else there was a collision, continue
	}
	cout << endl << "Was there a collision-free pose?: " << graspFound << " , Winning Pose: " << winPose << endl;
	if( 1 ){
		refcAsm->load_part_mrkrs_into_mngr( mrkrMngr );
		movdAsm->load_part_mrkrs_into_mngr( mrkrMngr );
		handModel.set_finger_state( rankedPairs[ winDex ].q_fingers );
		handModel.set_pose( winPose );
		handModel.load_part_mrkrs_into_mngr( mrkrMngr );
	}
	
	Pose_ASP relHandPose = winPose / movdAsm->get_pose();
	
	Pose_ASP endPose{ movdAsm->get_pose().position + Eigen::Vector3d( 0 , 0 , 0.05 ) , movdAsm->get_pose().orientation };
	
	//  9. Check IK feasible beginning and end
		//  A. Begin
		std::vector<double> jointSeed = {0,0,0,0,0,0,0};
		size_t attemptLimit = 25;
		std::vector<double> q1 = IK_clientCall( winPose , jointSeed , attemptLimit );
		cout << "Solution for path begin: " << q1 << endl;
		//  B. End
		//~ Pose_ASP endPose{ winPose.position + Eigen::Vector3d( 0 , 0 , 0.05 ) , winPose.orientation };
		std::vector<double> q2 = IK_clientCall( endPose * relHandPose , jointSeed , attemptLimit );
		cout << "Solution for path end: " << q2 << endl;
		// 10. If not feasible , send back the grasp chooser
		
	
	
	OpCollision liftCollide{ movdAsm , refcAsm , &handModel , relHandPose };
		
	size_t /* -- */ pK          = 5000;
	double /* -- */ pEpsilon    = 0.005;
	double /* -- */ pBiasProb   = 0.25;
	Eigen::Matrix<double, 2, 3> pXbounds = Eigen::MatrixXd::Zero( 2 , 3 );  
	Pose_ASP /*- */ pStart      = movdAsm->get_pose(); // winPose;// Pose_ASP{ Eigen::Vector3d( 0.5 , 0.5 , 0.1 ) , no_turn_quat() };
	Pose_ASP /*- */ pGoal       = endPose;// Pose_ASP{ Eigen::Vector3d( 0.5 , 0.5 , 0.9 ) , no_turn_quat() };
	double /* -- */ pWeightPosn = 1.0;
	double /* -- */ pWeightOrnt = 1.0;
	double /* -- */ nghbrRadius = pEpsilon * 2.5;
	pXbounds.row(0) = pStart.position + Eigen::Vector3d( -0.1 , -0.1 , 0   );  
	pXbounds.row(1) = pGoal.position  + Eigen::Vector3d(  0.1 , 0.1  , 0.2 );
	
	if( 1 ){ // TROUBLESHOOTING
		cout << "Does the problem start in collision?: " << liftCollide( pStart ) << endl;
		cout << "Begin Pose: " << pStart << endl;
		cout << "End Pose: _ " << pGoal  << endl;
		cout << "Bounds:" << endl << pXbounds  << endl;
	}
		
	if( 1 ){
		
		// 11. While number of attempts <= limit
		// 12. RRT* with gripper model
		
		cout << "Init solver ..." << endl;
		RRT_Solver<OpCollision> rrtSolver{ pK , pEpsilon , pBiasProb , pXbounds , liftCollide ,
										   pStart , pGoal , pWeightPosn , pWeightOrnt };
											
		StopWatch algoTimer;				
						
		cout << "Run solver on problem without obstructions ..." << endl;		
		cout << "From: " << pStart << " , To: " << pGoal << endl;
		std::vector<Pose_ASP> hardSoln = rrtSolver.build_rrt_star( pStart , pGoal , nghbrRadius , 0 );
		cout << "Solution contains " << hardSoln.size() << " poses" << endl 
			 << algoTimer.seconds_elapsed() << " seconds elapsed" << endl;
			 
		if( hardSoln.size() > 0 ){
			cout << "Solution Path:" << endl;
			for( size_t i = 0 ; i < hardSoln.size() ; i++ ){
				cout << hardSoln[i] << endl;
			}
		}
		
		cout << "There are " << rrtSolver.allTrees[0]->rootNode->get_distal_edges().size() << " edges in the solver at exit" << endl;
		
		// 13. IK for every pose ( smoothing later? )
		if( hardSoln.size() > 0 ){
			std::vector<double> soln = q1;
			
			for( size_t i = 0 ; i < hardSoln.size() ; i++ ){
				if( !is_err( soln ) ){  jointSeed = soln;  }
				else{  jointSeed = {0,0,0,0,0,0,0};  }
				
				soln = IK_clientCall( hardSoln[i] * relHandPose , jointSeed , attemptLimit );
				
				if( !is_err( soln ) ){  cout << "Solution Found!: " << soln << endl;  }
				else{  cout << "Solution FAILED!: " << endl;  }
			}	
			
		}
		
		if( 0 ){
			cout << "About to render results ... " << endl;
			
			load_RRT_soln_into_mrkrMngr( mrkrMngr , 
										 rrtSolver.allTrees[0]->rootNode->get_distal_edges() , 
										 hardSoln , 
										 pEpsilon );
										 
			cout << "Rendered!" << endl;
		}
	}
	
		
		
	
	/// ___ End Preliminary ________________________________________________________________________________________________________________
	
	// N-2. Notify
	ros_log( "[" + NODE_NAME + "] Init OK and about to run ..." , INFO );
	
	// N. Main loop
	while( ros::ok() ){ // While neither node nor ROS has been shut down
		
		/// == NODE WORK ===================================================================================================================
		
		
		if( CONNECT_TO_ARM ){	
			// 4. Update markers
			mrkrMngr.rebuild_mrkr_array();
			
		}
		
		// 5. Paint frame
		vis_arr.publish( markerArr );
		
		
		//~ break; // ONLY ONCE
		
		/// __ END WORK ____________________________________________________________________________________________________________________
		
		ros::spinOnce(); // - Process messages
		heartbeat.sleep(); // Sleep for remainder of period
	}
	
	// N+1. Notify  &&  Exit
	
	ros_log( "[" + NODE_NAME + "] Exit OK, Goodbye!" , INFO );
	
	return 0; // I guess everything turned out alright at the end!
}

// ___ End main ____________________________________________________________________________________________________________________________


/* === Spare Parts =========================================================================================================================



   ___ End Spare ___________________________________________________________________________________________________________________________
*/
