/*
ROS_NODE_TEMPLATE.cpp
James Watson , YYYY MONTHNAME

% ROS Node %
A ONE-LINE DESCRIPTION OF THE NODE
Publises To ----> :
Subscribes To <-- :

Dependencies: ROS , Cpp_Helpers , ROS_Helpers
Template Version: 2018-06-06
*/

// === Init ================================================================================================================================

// ~~~ Includes ~~~
// ~~ ROS ~~
#include <ros/ros.h> // -------------- ROS , Publishers , Subscribers
// ~ ROS Messages ~
#include "motion_cost/qFromPose.h"
#include "motion_cost/IKrequest.h"
#include "motion_cost/IKresponse.h"
#include "geometry_msgs/Pose.h"
#include "sensor_msgs/JointState.h"
// ~~ Local ~~
#include <Cpp_Helpers.h> // - C++ Utilities and Shortcuts
#include <ROS_Helpers.h> // - ROS Utilities and Shortcuts
#include <Model_Factory.h> // Assemblies to plan on
#include <AsmSeqPlan.h> // -- Identify grasp pairs , ASP

// ___ End Init ____________________________________________________________________________________________________________________________


/// ######### Node Vars #########

string NODE_NAME = "Replay_Test";
int    RATE_HZ   = 100;
int    QUEUE_LEN = 100;

std::vector<string> ALL_JOINT_NAMES = { "lbr4_j0" , "lbr4_j1" , "lbr4_j2" , "lbr4_j3" , "lbr4_j4" , "lbr4_j5" , "lbr4_j6" , 
										"r_gripper_l_finger_joint" , "r_gripper_r_finger_joint" };
size_t NUM_ALL_JOINTS = ALL_JOINT_NAMES.size();

/// ********* End Vars *********


// === Program Functions & Classes ===

void pack_joint_and_gripper_states_into_msg( std::vector<double>& armState , // ---- Joint positions of arm
											 std::vector<double>& fingState , // --- Joint positions of gripper
											 sensor_msgs::JointState& stateMsg ){ // Load state data here
	// Load arm joint and gripper joint states into the message parameter
	std::vector<double> totalPos = vec_copy( armState );
	extend_vec_with( totalPos , fingState );
	stateMsg.name     = vec_copy( ALL_JOINT_NAMES );
	stateMsg.position = totalPos;
}

// ___ End Functions & Classes ___


// === Program Vars ===



// ___ End Vars ___


// === main ================================================================================================================================

int main( int argc , char** argv ){ // Main takes the terminal command and flags that called it as arguments
	srand( time( 0 ) ); // Random seed based on the current clock time
	
	// 0. Init ROS  &&  Register node
	ros::init( argc , argv , NODE_NAME );
	
	// 1. Fetch handle to this node
	ros::NodeHandle nodeHandle;
	
	// 2. Init node rate
	ros::Rate heartbeat( RATE_HZ );
	
	// 3. Set up subscribers and publishers
	
	// ~ Publishers ~
	ros::Publisher joint_cmd_pub = nodeHandle.advertise<sensor_msgs::JointState>( "/lbr4_teleop/joint_cmd" , QUEUE_LEN );
	ros::Publisher vis_arr       = nodeHandle.advertise<visualization_msgs::MarkerArray>( "visualization_marker_array" , QUEUE_LEN );
	
	// ~ Subscribers ~
	// ros::Subscriber SUBSCRIBER_OBJ = nodeHandle.subscribe( "TOPIC_NAME" , QUEUE_LEN , CALLBACK_FUNCTION );
	
	// ~ Service Servers ~
	// ros::ServiceServer SERVER_OBJ = nodeHandle.advertiseService( "SERVICE_NAME" , SERV_CALLBACK );
	
	// ~ Service Clients ~
	ros::ServiceClient IK_Client = nodeHandle.serviceClient<motion_cost::qFromPose>( "q_from_pose" );
	
	/// === Preliminary { Setup , Instantiation , Planning  } ==============================================================================

	// ~~ I. Planning ~~

	//  1. Instantiate ASM
	Assembly_ASM* simplePhone = simple_phone();
	simplePhone->set_pose( origin_pose() );
	cout << "Assembly has " << simplePhone->how_many_parts() << " parts" << endl;
	//  2. Get grasp points and choose one pair
	Eigen::Vector3d approach = Eigen::Vector3d(  0.0 ,  1.0 ,  0.0 );
	Eigen::Vector3d perpAxis = Eigen::Vector3d(  0.0 ,  0.0 ,  1.0 );
	RayHits graspPnts = get_opposite_grasp_pairs_for_ASM_and_direction( simplePhone , 
																		approach , 
																		perpAxis , 2.0 ,
																		0.01 );
	cout << "Are there hits? " << graspPnts.anyHits << " , How many? " << graspPnts.enter.rows() << endl;
	Eigen::Vector3d query( 0 , 0 , 4 ); 
	Eigen::MatrixXd chosenPair = closest_pair_in_hits_to( graspPnts , query );
	cout << "Chosen pair: " << endl << chosenPair << endl;
	//  3. Get relative pose for grasp
	Eigen::Vector3d origin = get_average_V( chosenPair );
	Eigen::Vector3d zBasis( 0 , 0 , 1 );
	Eigen::Vector3d A      = chosenPair.row(0);
	Eigen::Vector3d B      = chosenPair.row(1);
	Eigen::Vector3d xBasis = ( B - A ).normalized();
	Eigen::Vector3d yBasis = zBasis.cross( xBasis ).normalized();
	Pose_ASP graspPose     = pose_from_origin_bases( origin , xBasis , yBasis , zBasis );
	Pose_ASP relGrasp      = graspPose / simplePhone->get_pose();
	//  4. Generate traj
	size_t N = 50;
	cout << "Arc Angle: " << 2*M_PI << endl;
	Eigen::MatrixXd trajPts = circle_arc_3D( Eigen::Vector3d( 0.0 , 0.0 , 1.0 ) , // --- axis
											 Eigen::Vector3d( 0.795 , 0.0 , 0.59  ) , // center
											 0.305 , // -------------------------------- radius
											 Eigen::Vector3d( 1.0 , 0.0 , 0.0 ) , // --- theta = 0 direction
											 2*M_PI , // ------------------------------- arc angle
											 N ); // ----------------------------------- Number of points on arc
	cout << "Trajectory: " << endl << trajPts << endl;
	
	// Declare message outside so that we preserve the seed state
	motion_cost::qFromPose srvMsg;
	srvMsg.response.response.valid = 0; // Init false
	
	//  5. Check IK for all traj points  &&  Report
	
	size_t i = 0;
	unsigned char isValid = 0;
	std::vector<sensor_msgs::JointState> allSolns;
	
	while( i < N ){
	//~ for( size_t i = 0 ; i < N ; i++ ){
		// A. Build a request message 
		// Pose
		srvMsg.request.request.worldPose.position.x = trajPts( i , 0 );
		srvMsg.request.request.worldPose.position.y = trajPts( i , 1 );
		srvMsg.request.request.worldPose.position.z = trajPts( i , 2 );
		srvMsg.request.request.worldPose.orientation.w =  1;
		srvMsg.request.request.worldPose.orientation.x =  0.0;
		srvMsg.request.request.worldPose.orientation.y =  0.0;
		srvMsg.request.request.worldPose.orientation.z =  0.0;
		// Seed JointState
		sensor_msgs::JointState seedMsg;
		
		cout << "Previous valid?: " << static_cast<unsigned>( srvMsg.response.response.valid ) << endl;
		
		isValid = static_cast<unsigned>( srvMsg.response.response.valid );
		
		if( isValid && i > 0 ){
			seedMsg.position = srvMsg.response.response.qSoln.position;
		}else{
			//~ seedMsg.position = vec_dbbl_zeros( 7 ); 
			seedMsg.position = randrange_vec( -M_PI ,  M_PI , 7 ); 
		}
		srvMsg.request.request.seed = seedMsg;
		
		// B. Send Request
		if( IK_Client.call( srvMsg ) ){
			ros_log( "Response returned!"    , INFO );
			cout << "Solution: " << srvMsg.response.response.qSoln.position;
			isValid = static_cast<unsigned>( srvMsg.response.response.valid );
			if( isValid ){
				cout << "Valid! , Increment" << endl;
				
				// Store the good solution
				sensor_msgs::JointState temp;
				temp.position = srvMsg.response.response.qSoln.position;
				allSolns.push_back( temp );
				
				i++;
			}else{
				cout << "Invalid! , Retry" << endl; // Might be a good idea to prevent infinite retries!
			}
			
		}else{
			ros_log( "NO response returned!" , WARN );
		}
		ros::spinOnce(); // - Process messages
		heartbeat.sleep(); // Sleep for remainder of period
	}

	// ~~ II. Recording ~~
	
	std::vector<double> fingPos = { 0.04 , 0.04 };
	
	//  A. Instantiate a recording
	PlanRecording record;
	//  6. For every traj point
	for( size_t i = 0 ; i < N ; i++ ){
		//  7. Instantiate frame 
		PlayerFrame frame;
		//  8. Set the pose
		Eigen::Vector3d pos = trajPts.row(i);
		simplePhone->set_pose( pose_from_position_orient( pos , no_turn_quat() ) );
		//  9. Load markers into the frame's manager
		simplePhone->load_part_mrkrs_into_mngr( frame.mrkrMngr );
		// 10. Load a jointstate message into the frame
		sensor_msgs::JointState temp;
		pack_joint_and_gripper_states_into_msg( allSolns[i].position , // Joint positions of arm
												fingPos , // ------------ Joint positions of gripper
												temp ); // -------------- Load state data here
		frame.jnt_state_ = temp;
		// 11. Load frame into recording
		record.push_back( frame );
	}
	// 12. Instantiate playback (start timer)  &&  Load recording into playback
	PlanPlayer player{ 30 , record };
	PlayerFrame* currFrame = nullptr;
	bool LOOP = true;

	/// ___ End Preliminary ________________________________________________________________________________________________________________

	// N-1. Notify
	ros_log( "[" + NODE_NAME + "] Init OK and about to run ..." , INFO );
	
	// N. Main loop
	while( ros::ok() ){ // While neither node nor ROS has been shut down
		
		/// == NODE WORK ===================================================================================================================
		
		// A. Fetch frame
		currFrame = player.current_frame( LOOP );
		// B. Render object markers
		visualization_msgs::MarkerArray& markerArr = currFrame->mrkrMngr.get_arr();
		vis_arr.publish( markerArr );
		// C. Render the joint state
		joint_cmd_pub.publish( currFrame->jnt_state_ );
		
		/// __ END WORK ____________________________________________________________________________________________________________________
		
		ros::spinOnce(); // - Process messages
		heartbeat.sleep(); // Sleep for remainder of period
	}
	
	// N+1. Notify  &&  Exit
	
	ros_log( "[" + NODE_NAME + "] Exit OK, Goodbye!" , INFO );
	
	return 0; // I guess everything turned out alright at the end!
}

// ___ End main ____________________________________________________________________________________________________________________________


/* === Spare Parts =========================================================================================================================



   ___ End Spare ___________________________________________________________________________________________________________________________
*/
