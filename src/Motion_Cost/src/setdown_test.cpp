/*
integration01.cpp
James Watson , 2018 June

% ROS Node %
Test assembly problems for correctness
Publises To ----> :
Subscribes To <-- :

Dependencies: ROS , Cpp_Helpers , ROS_Helpers
Template Version: 2018-06-06
*/

// === Init ================================================================================================================================

// ~~~ Includes ~~~
// ~~ ROS ~~
#include <ros/ros.h> // -------------- ROS , Publishers , Subscribers
#include <ros/package.h> // ------------------ Where are we? // http://wiki.ros.org/Packages#C.2B-.2B-
// ~ ROS Messages ~
#include "motion_cost/qFromPose.h"
#include "motion_cost/IKrequest.h"
#include "motion_cost/IKresponse.h"
#include "geometry_msgs/Pose.h"
#include "sensor_msgs/JointState.h"
#include <ll4ma_teleop/Joints2Jac.h>
#include <ll4ma_teleop/LinkPoses.h>
#include <ll4ma_teleop/FngrPoses.h>
// ~~ Local ~~
#include <Cpp_Helpers.h> // - C++ Utilities and Shortcuts
#include <ROS_Helpers.h> // - ROS Utilities and Shortcuts
#include <Model_Factory.h> // Assemblies to plan on
#include <AsmSeqPlan.h> // -- Identify grasp pairs , ASP
#include <comm_ASM.h> // ---- Messages and visualization
#include <Motion_Planning.h>
#include <Methods.h>

// ___ End Init ____________________________________________________________________________________________________________________________


/// ######### Node Vars #########

string /* -- */ NODE_NAME  = "integration02";
int /* ----- */ RATE_HZ    = 30;
int /* ----- */ QUEUE_LEN  = 30;
Eigen::Vector3d ROBOT_BASE = Eigen::Vector3d( 0.55 , 0 , 1.20 ); // FUTURE: NEED A WAY TO LOAD THIS FROM A PARAM SERVER

/// ********* End Vars *********


// === Program Functions & Classes ===



// ___ End Functions & Classes ___


// === Program Vars ===

bool CONNECT_TO_ARM = true;
//~ PlanRecording recording;

// ___ End Vars ___


// === main ================================================================================================================================

int main( int argc , char** argv ){ // Main takes the terminal command and flags that called it as arguments
	srand( time( 0 ) ); // Random seed based on the current clock time
	
	// 0. Init ROS  &&  Register node
	ros::init( argc , argv , NODE_NAME );
	
	// 1. Fetch handle to this node
	ros::NodeHandle nodeHandle;
	
	// 2. Init node rate
	ros::Rate heartbeat( RATE_HZ );
	
	// 3. Set up subscribers and publishers
	
	// ~ Publishers ~
	ros::Publisher joint_cmd_pub = nodeHandle.advertise<sensor_msgs::JointState>( "/lbr4_teleop/joint_cmd" , QUEUE_LEN );
	send_joint_command jntCmd_Call{ joint_cmd_pub };
	
	ros::Publisher vis_arr       = nodeHandle.advertise<visualization_msgs::MarkerArray>( "visualization_marker_array" , QUEUE_LEN );
	
	// ~ Subscribers ~
	// ros::Subscriber SUBSCRIBER_OBJ = nodeHandle.subscribe( "TOPIC_NAME" , QUEUE_LEN , CALLBACK_FUNCTION );
	
	// ~ Service Servers ~
	// ros::ServiceServer SERVER_OBJ = nodeHandle.advertiseService( "SERVICE_NAME" , SERV_CALLBACK );
	
	// ~ Service Clients ~
	
	ros::ServiceClient  IK_Client = nodeHandle.serviceClient<motion_cost::qFromPose>( "q_from_pose" );
	request_IK_solution IK_clientCall{ IK_Client };
	
	//~ ros::ServiceClient  J_Client = nodeHandle.serviceClient<ll4ma_teleop::Joints2Jac>( "/robot_commander/get_Jacobian" );
	//~ request_Jacobian    J_ClientCall{ J_Client };
	
	//~ ros::ServiceClient  FK_Client = nodeHandle.serviceClient<ll4ma_teleop::LinkPoses>( "/robot_commander/get_links_FK" );
	//~ request_FK_poses    FK_ClientCall{ FK_Client };  // { 1 , 2 , 3 , 4 , 5 , 6 , 7 , Grasp Target }

	//~ ros::ServiceClient  FNG_Client = nodeHandle.serviceClient<ll4ma_teleop::FngrPoses>( "/robot_commander/get_fingr_FK" );
	//~ request_FNG_poses   FNG_ClientCall{ FNG_Client };
	
	
	// N-1. Animation Init 
	srand( ( time( 0 ) % 1 ) * 1000.0 + getpid() ); // Random seed based on the current clock time
	RViz_MarkerManager mrkrMngr{};
	visualization_msgs::MarkerArray& markerArr = mrkrMngr.get_arr();
	
	
	/// === Preliminary { Setup , Instantiation , Planning  } ==============================================================================

	std::vector<llin> subPrt = { 11 ,  8 ,  3 };
	Assembly_ASM* subAsm = simple_phone( subPrt );
	
	
	
	size_t numSpprts = subAsm->how_many_supports();
	
	cout << "The constructed sub has " << numSpprts << " supports" << endl;
	
	Eigen::Vector2d loc{ 0,0 };
	Eigen::Vector2d shift{ 0 , 0.075 };
	
	Eigen::Vector3d loc3{ 0,0,0.05 };
	Eigen::Vector3d shift3{ 0 , 0.075 , 0 };
	
	for( size_t i = 0 ; i < numSpprts ; i++ ){
		
		subAsm->set_pose(  
			Pose_ASP{
				Eigen::Vector3d( 1,1,1.05 ),
				Eigen::Quaterniond( 0.707 , 0 , 0.707 , 0 )
			}
		);
		
		
		if(0){
			subAsm->set_lab_pose_for_support_index( i  , loc , true );
			subAsm->load_part_mrkrs_into_mngr( mrkrMngr );
			cout << "AABB for Rest on Support " << i << endl
				 << AABB( subAsm->get_lab_VFN() ) << endl
				 << "Away from floor?: " << yesno( abs( AABB( subAsm->get_lab_VFN() )(0,2) ) > EPSILON ) << endl << endl;
			loc += shift;
		}else{
			subAsm->set_lab_pose_for_index_3D( i  , loc3 , true );
			subAsm->load_part_mrkrs_into_mngr( mrkrMngr );
			cout << "AABB for Rest on Support " << i << endl
				 << AABB( subAsm->get_lab_VFN() ) << endl
				 << "Away from floor?: " << yesno( abs( AABB( subAsm->get_lab_VFN() )(0,2) - loc3(2) ) > EPSILON ) << endl << endl;
			loc3 += shift3;
		}
	}
		
	/// ___ End Preliminary ________________________________________________________________________________________________________________
	
	// N-2. Notify
	ros_log( "[" + NODE_NAME + "] Init OK and about to run ..." , INFO );
	
	cerr << "Before loop!" << endl;
	size_t loopCounter =  0 ,
		   LOOPLIMIT   = 10 ;
	
	// N. Main loop
	while( ros::ok() ){ // While neither node nor ROS has been shut down
		
		/// == NODE WORK ===================================================================================================================

		//~ cerr << "Loop ";

		// Request frame from renderer

		// A. Fetch frame
		//~ currFrame = player.current_frame( LOOP );
		// B. Render object markers
		//~ visualization_msgs::MarkerArray& markerArr = currFrame->mrkrMngr.get_arr();
		
		
		// COMING SOON PHASE 3
		//~ // C. Render the joint state
		//~ jntCmd_Call( currFrame->get_q() );

		// 5. Paint frame
		vis_arr.publish( markerArr );
		
		if( loopCounter >= LOOPLIMIT ){  break;  }  loopCounter++;
		//~ break; // ONLY ONCE
		
		/// __ END WORK ____________________________________________________________________________________________________________________
		
		ros::spinOnce(); // - Process messages
		heartbeat.sleep(); // Sleep for remainder of period
	}
	
	cerr << "Outside loop!" << endl;
	
	// N+1. Notify  &&  Exit
	
	ros_log( "[" + NODE_NAME + "] Exit OK, Goodbye!" , INFO );
	
	return 0; // I guess everything turned out alright at the end!
}

// ___ End main ____________________________________________________________________________________________________________________________


/* === Spare Parts =========================================================================================================================



   ___ End Spare ___________________________________________________________________________________________________________________________
*/
