/*
simple_phone.cpp
James Watson , 2018 February
Test integration of the ASP_3D lib --> ROS , Show markers and move them around

Dependencies: Cpp_Helpers , ROS , Eigen , RAPID
Template Version: 2017-09-23
*/

// === Init ================================================================================================================================

// ~~~ Includes ~~~
// ~~ ROS ~~
// ~ Core ~
#include <ros/ros.h> // --- ROS presides over all
#include <ros/package.h> // Where are we? // http://wiki.ros.org/Packages#C.2B-.2B-
// ~~ Includes ~~
#include <Eigen/Core> // ---- The living heart of Eigen
#include <Eigen/Dense> // --- Cross Product , etc.
#include <Eigen/Geometry> //- Quaternion , etc
// ~ RViz ~
#include <visualization_msgs/Marker.h> // ---- Display geometry
#include <visualization_msgs/MarkerArray.h> // RViz marker array
// ~~ Local ~~ 
#include <Cpp_Helpers.h> // Shortcuts and Aliases , and Functions of general use for C++ programming
#include <ASP_3D.h> // ---- Assembly geometry
#include <AsmSeqPlan.h> //- Graph planning
#include <Model_Factory.h> // Assemblies to plan on
#include <Methods.h>

// using namespace quickhull;

// ___ End Init ____________________________________________________________________________________________________________________________

// === Program Vars ===

Incrementer assign_part_ID{ 0 }; // Instantiate a functor that will assign a unique number to each part
Incrementer assign_asm_ID{ 0 }; //- Instantiate a functor that will assign a unique number to each assembly

// ___ End Vars ___


// === main ================================================================================================================================

int main( int argc , char** argv ){
	
	srand( time( 0 ) ); // Random seed based on the current clock time
	
	// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
	
	sep( "Create the Simple Phone" );
	
	// ~~ 0. Create file paths ~~
	// ~ Fetch the folder to the STL files ~
	string pkgPath = ros::package::getPath( "motion_cost" );
	string resourcePath = pkgPath + "/Resources/Assembly_CAD/SimplePhone/";
	// ~ Create full paths to STLs ~
	std::vector<string> fNames;
/* 0*/ fNames.push_back( resourcePath + "body_SCL.stl" );      Eigen::Vector3d body_shift{    -3.92054e-05 ,  9.47465e-05 , -0.00919433 };
/* 1*/ fNames.push_back( resourcePath + "antenna_SCL.stl" );   Eigen::Vector3d antenna_shift{  0.005       , -0.0175      , -0.00375    };
/* 2*/ fNames.push_back( resourcePath + "bridgeAB_SCL.stl" );  Eigen::Vector3d bridgeAB_shift{ 0.0         ,  0.0         , -0.0025     };
/* 3*/ fNames.push_back( resourcePath + "bridgeBC_SCL.stl" );  Eigen::Vector3d bridgeBC_shift{ 0.0         ,  0.0         , -0.0025     };
/* 4*/ fNames.push_back( resourcePath + "chipA1_SCL.stl" );    Eigen::Vector3d chipA1_shift{   0.0         ,  0.0         , -0.0025     };
/* 5*/ fNames.push_back( resourcePath + "chipA1_SCL.stl" );    
/* 6*/ fNames.push_back( resourcePath + "chipB1_SCL.stl" );    Eigen::Vector3d chipB1_shift{  -0.002       ,  0.00666667  , -0.0025     };
/* 7*/ fNames.push_back( resourcePath + "chipB2_SCL.stl" );    Eigen::Vector3d chipB2_shift{   0.00371429  ,  0.0114286   , -0.0025     };
/* 8*/ fNames.push_back( resourcePath + "chipC1_SCL.stl" );    Eigen::Vector3d chipC1_shift{   0.0         , -0.00697957  , -0.0025     };
/* 9*/ fNames.push_back( resourcePath + "moduleA_SCL.stl" );   Eigen::Vector3d moduleA_shift{  0.0         ,  0.000178571 , -0.00446429 };
/*10*/ fNames.push_back( resourcePath + "moduleB_SCL.stl" );   Eigen::Vector3d moduleB_shift{  7.01754e-05 ,  0.0         , -0.00453947 };
/*11*/ fNames.push_back( resourcePath + "moduleC_SCL.stl" );   Eigen::Vector3d moduleC_shift{  0.0         ,  1.39405e-05 , -0.00428903 };
/*12*/ fNames.push_back( resourcePath + "panel_SCL.stl" );     Eigen::Vector3d panel_shift{    0.0         , 0.0          , -0.00125    };
	// ~ Create parts ~
	size_t fLen = fNames.size();
	std::vector<Part_ASM> phoneParts;
	for( size_t i = 0 ; i < fLen ; i++ ){
		cout << fNames[i] << endl;
		phoneParts.push_back( Part_ASM{   assign_part_ID() , fNames[i]   } );
	}
	cout << "Added " << phoneParts.size() << " parts." << endl;
		
	
		
	// 2. Create an assembly , add parts
	Assembly_ASM simplePhone{ assign_asm_ID() };
	// ~ body ~
/* 0*/ simplePhone.add_part_w_pose( phoneParts[ 0] , Pose_ASP{ -body_shift , no_turn_quat() } ); // body
/*10*/ simplePhone.add_part_w_pose( phoneParts[10] , Pose_ASP{ -moduleB_shift  + Eigen::Vector3d(  0.0000 ,  0.00000 ,  0.0025 ) , // moduleB
		                                                     no_turn_quat()                                       } ); 
/* 9*/ simplePhone.add_part_w_pose( phoneParts[ 9] , Pose_ASP{ -moduleA_shift  + Eigen::Vector3d(  0.0000 ,  0.04000 ,  0.0025 ) , // moduleA
		                                                     no_turn_quat()                                       } ); 
/*11*/ simplePhone.add_part_w_pose( phoneParts[11] , Pose_ASP{ -moduleC_shift  + Eigen::Vector3d(  0.0000 , -0.04000 ,  0.0025 ) , // moduleC
		                                                     no_turn_quat()                                       } ); 

if( true ){ // Disable all but a known connected subset
	/* 4*/ simplePhone.add_part_w_pose( phoneParts[ 4] , Pose_ASP{ -chipA1_shift   + Eigen::Vector3d(  0.0000 ,  0.04875 ,  0.0100 ) , // chipA1
																 no_turn_quat()                                       } ); 
	/* 5*/ simplePhone.add_part_w_pose( phoneParts[ 5] , Pose_ASP{ -chipA1_shift   + Eigen::Vector3d(  0.0000 ,  0.03875 ,  0.0100 ) , // chipA1
																 no_turn_quat()                                       } ); 
	/* 2*/ simplePhone.add_part_w_pose( phoneParts[ 2] , Pose_ASP{ -bridgeAB_shift + Eigen::Vector3d(  0.0000 ,  0.02250 ,  0.0100 ) , // bridgeAB
																 no_turn_quat()                                       } ); 
	/* 6*/ simplePhone.add_part_w_pose( phoneParts[ 6] , Pose_ASP{ -chipB1_shift   + Eigen::Vector3d( -0.0075 ,  0.01000 ,  0.0100 ) , // chipB1
																 no_turn_quat()                                       } ); 
	/* 7*/ simplePhone.add_part_w_pose( phoneParts[ 7] , Pose_ASP{ -chipB2_shift   + Eigen::Vector3d(  0.0075 ,  0.01000 ,  0.0100 ) , // chipB2
																 no_turn_quat()                                       } ); 
	/* 3*/ simplePhone.add_part_w_pose( phoneParts[ 3] , Pose_ASP{ -bridgeBC_shift + Eigen::Vector3d(  0.0000 , -0.02500 ,  0.0100 ) , // bridgeBC
																 no_turn_quat()                                       } ); 
	/* 8*/ simplePhone.add_part_w_pose( phoneParts[ 8] , Pose_ASP{ -chipC1_shift   + Eigen::Vector3d(  0.0000 , -0.04750 ,  0.0100 ) , // chipC1
																 no_turn_quat()                                       } ); 
	/* 1*/ simplePhone.add_part_w_pose( phoneParts[ 1] , Pose_ASP{ -antenna_shift  + Eigen::Vector3d( -0.0200 ,  0.05250 ,  0.0050 ) , // antenna
																 no_turn_quat()                                       } ); 
}
	
	// 3. Determine the local freedom of each part (NDBG)
	simplePhone.recalc_geo();
	
	// 4. Print a report
	simplePhone.assembly_report();
	
	
	sep( "Create the Liaison Graph" );
	simplePhone.populate_liaison_graph_from_NDBG();
	print_vec_vec( simplePhone.get_liaison_graph() );
	
	sep( "Part ID List" );
	cout << simplePhone.get_part_ID_vec() << endl;
	
	sep( "Query the NDBG b/n each pair in a known connected subgraph" );
	std::vector<llin> knownConnctd = simplePhone.get_part_ID_vec();
	size_t len = knownConnctd.size();
	for( size_t i = 0 ; i < len - 1 ; i++ ){
		for( size_t j = i+1 ; j < len ; j++ ){
			std::vector<llin> movd;  movd.push_back( knownConnctd[i] );
			std::vector<llin> rfrc;  movd.push_back( knownConnctd[j] );
			cout << "Number of constraints at " << knownConnctd[i] << " , " << knownConnctd[j]
				 << ": \t" << simplePhone.how_many_constraints( knownConnctd[i] , knownConnctd[j] ) << endl;
		}
	}
	
	
	sep( "Test Components of Competing Methods" );
	//~ cout << vec3d_random() << endl;
	//~ cout << vec3d_rand_corners( Eigen::Vector3d( 0,0,0 ) , Eigen::Vector3d( 10,10,10 ) ) << endl;
	//~ Eigen::MatrixXd corners = Eigen::MatrixXd::Zero( 2 , 3 );
	//~ cout << sample_from_AABB( 10 , corners ) << endl;
	
	// TEST: 'vec_vec_top_populate' , OK!
	std::vector<std::vector<size_t>> vv;
	vec_vec_top_populate( vv , 10 ); // TESTED , OK!
	cout << vec_vec_len( vv ) << endl;
	
	// TEST: 'closest_point_to_sq' , OK!
	Eigen::Vector3d queryPnt{ 0,0,0 };
	Eigen::MatrixXd points = Eigen::MatrixXd::Zero( 4 , 3 );
	points << 0 , 0 , 3 ,
			  1 , 0 , 0 , // Should be the closest
			  0 , 2 , 0 ,
			  3 , 3 , 3 ;
	IndexDbblResult closest = closest_point_to_sq( points , queryPnt ); // TESTED , OK!
	cout << "Closest is index " << closest.index << " at a squared distance of " << closest.measure << endl;
	
	
	Eigen::MatrixXd vertices = Eigen::MatrixXd::Zero( 8*3 , 3 );
	//          Cube 1
	vertices <<  0 ,  0 ,  0 ,
				10 ,  0 ,  0 ,
				10 , 10 ,  0 ,
				 0 , 10 ,  0 ,
				 0 ,  0 , 10 ,
				10 ,  0 , 10 ,
				10 , 10 , 10 ,
				 0 , 10 , 10 ,
	//          Cube 2			
				20 ,  0 ,  0 ,
				30 ,  0 ,  0 ,
				30 , 10 ,  0 ,
				20 , 10 ,  0 ,
				20 ,  0 , 10 ,
				30 ,  0 , 10 ,
				30 , 10 , 10 ,
				20 , 10 , 10 ,
	//          Cube 3
				 0 , 20 ,  0 ,
				10 , 20 ,  0 ,
				10 , 30 ,  0 ,
				 0 , 30 ,  0 ,
				 0 , 20 , 10 ,
				10 , 20 , 10 ,
				10 , 30 , 10 ,
				 0 , 30 , 10 ;
	std::vector<size_t> c1dex = vec_range( (size_t) 0 , (size_t) 7 );
	std::vector<size_t> c2dex = vec_range( (size_t) 8 , (size_t)15 );
	std::vector<size_t> c3dex = vec_range( (size_t)16 , (size_t)23 );
	std::vector<size_t> oneOnly;  oneOnly.push_back( 1 );
	cout << "Average point of Cube 1: " << selected_average_V( vertices , c1dex   ) << endl;
	cout << "Average point of Cube 2: " << selected_average_V( vertices , c2dex   ) << endl;
	cout << "Average point of Cube 3: " << selected_average_V( vertices , c3dex   ) << endl;
	cout << "Average of one point: __ " << selected_average_V( vertices , oneOnly ) << endl;
	
	if( true ){
		Eigen::MatrixXd cube1 = Eigen::MatrixXd::Zero( 2 , 3 );		cube1 <<  0 ,  0 ,  0 , 10 , 10 , 10;
		Eigen::MatrixXd cube2 = Eigen::MatrixXd::Zero( 2 , 3 );		cube2 <<  5 , -5 ,  5 , 15 ,  5 , 15;
		Eigen::MatrixXd cube3 = Eigen::MatrixXd::Zero( 2 , 3 );		cube3 << -5 ,  5 ,  5 ,  5 , 15 , 15;
		
		Eigen::MatrixXd manyPoints = Eigen::MatrixXd::Zero( 300 , 3 );
		manyPoints.block<100,3>(  0,0) = sample_from_AABB( 100 , cube1 );
		manyPoints.block<100,3>(100,0) = sample_from_AABB( 100 , cube2 );
		manyPoints.block<100,3>(200,0) = sample_from_AABB( 100 , cube3 );
		
		PointsClassfd clusters =  kMeans_clusters_3D( 3 , manyPoints );
		cout << clusters.points << endl;
		cout << clusters.assignments << endl;
	}
	
	cout << endl << "Test Subgraph Connectivity , " << "Part 1: Connected Subgraph" << endl;
	std::vector<std::vector<bool>> testLiaison = bool_false_vec_vec( 16 );
	add_undir_edge( testLiaison ,  5 ,  6 );
	add_undir_edge( testLiaison ,  6 , 10 );
	add_undir_edge( testLiaison , 10 ,  9 );
	add_undir_edge( testLiaison ,  9 ,  5 );
	print_vec_vec( testLiaison );
	std::vector<size_t> subG;  subG.push_back(  5 );  subG.push_back(  6 );  subG.push_back(  9 );  subG.push_back( 10 );
	cout << "Is the subgraph connected? : " << is_subgraph_connected( testLiaison , subG ) << endl;
	
	cout << endl << "Test Subgraph Connectivity , " << "Part 2: Unconnected Subgraph" << endl;
	testLiaison = bool_false_vec_vec( 16 );
	add_undir_edge( testLiaison ,  5 ,  6 );
	add_undir_edge( testLiaison , 10 ,  9 );
	print_vec_vec( testLiaison );
	cout << "Is the subgraph connected? : " << is_subgraph_connected( testLiaison , subG ) << endl;
	
	cout << endl << "Identify Connected Subgraphs , " << "Part 1: Partially connected Subgraph" << endl;
	std::vector<std::vector<size_t>> subSubs = connected_subgraphs_in_subgraph( testLiaison , subG );
	print_vec_vec( subSubs );
	
	cout << endl << "Identify Connected Subgraphs , " << "Part 2: Connected Subgraph" << endl;
	testLiaison = bool_false_vec_vec( 16 );
	add_undir_edge( testLiaison ,  5 ,  6 );
	add_undir_edge( testLiaison ,  6 , 10 );
	add_undir_edge( testLiaison , 10 ,  9 );
	add_undir_edge( testLiaison ,  9 ,  5 );
	subSubs = connected_subgraphs_in_subgraph( testLiaison , subG );
	print_vec_vec( subSubs );
	
	cout << endl << "Identify Connected Subgraphs , " << "Part 3: Unconnected Subgraph" << endl;
	testLiaison = bool_false_vec_vec( 16 );
	subSubs = connected_subgraphs_in_subgraph( testLiaison , subG );
	print_vec_vec( subSubs );
	
	std::vector< std::vector< llin > > moratoLvl1;
	if( true ){
		std::vector< std::vector< llin > > clusterVec = kMeans_part_clusters_ID( &simplePhone , 2 );
		cout << endl << "k-Means from Part Centers" << endl;
		print_vec_vec( clusterVec );
		cout << "Level 1 Clustering of the Simple Phone" << endl;
		moratoLvl1 = cluster_one_level_disasm( &simplePhone );
		print_vec_vec( moratoLvl1 );
	}
	
	// ~~~ MORATO 2013 MOD TEST ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
	
	sep( "Investigate Morato Results" );
	
	Assembly_ASM* phoneAlt = simple_phone();
	
	//~ AsmStateNode_ASP* rootNodeMoratoModFull = Morato2013Modified_full_depth( &simplePhone );
	AsmStateNode_ASP* rootNodeMoratoModFull = Morato2013Modified_full_depth( phoneAlt );
	
	AsmStateNode_ASP* currNode = nullptr;
	size_t lenFirst = rootNodeMoratoModFull->outgoingEdges.size() , 
		   currLen  = 0                                           ;
	
	cout << "Full depth level 1 has " << lenFirst << " edges." << endl;
	for( size_t i = 0 ; i < lenFirst ; i++ ){
		currNode = rootNodeMoratoModFull->outgoingEdges[i];
		cout << "Node " << i + 1 << " has " << currNode->outgoingEdges.size() << " outgoing edges." << endl;
	}
	
	
	
	// ... END MORATO TEST .................................................................................................................
	
	sep( "Test 2D Poly Collision" );
	Eigen::MatrixXd sqrA = Eigen::MatrixXd::Zero(4,2);
	sqrA<< 0 , 0 ,
		   4 , 0 , 
		   4 , 4 ,
		   0 , 4 ;
	
	Eigen::MatrixXd sqrB = Eigen::MatrixXd::Zero(4,2);
	sqrB<< 3 , 3 ,
		   7 , 3 , 
		   7 , 7 ,
		   3 , 7 ;
	
	Eigen::MatrixXd sqrC = Eigen::MatrixXd::Zero(4,2);
	sqrC<< 1 , 1 ,
		   3 , 1 , 
		   3 , 3 ,
		   1 , 3 ;
	
	Eigen::MatrixXd sqrD = Eigen::MatrixXd::Zero(4,2);
	sqrD<< 5 , 5 ,
		   7 , 5 , 
		   7 , 7 ,
		   5 , 7 ;
	
	cout << "Do Squares A and B collide?: " << polygon_collide_2D( sqrA , sqrB ) << endl;
	cout << "Do Squares A and C collide?: " << polygon_collide_2D( sqrA , sqrC ) << endl;	   
	cout << "Do Squares A and D collide?: " << polygon_collide_2D( sqrA , sqrD ) << endl;	   
	
	/// === VISUALIZATION ==================================================================================================================
	
	// == ROS Loop ==
	
	// = ROS Start =
	
	// 5. Start ROS
	
	// ~ Animation Vars ~
	srand( ( time( 0 ) % 1 ) * 1000.0 + getpid() ); // Random seed based on the current clock time
	int updateHz = 30;
	Eigen::Vector2d offsetStep{ 0.30 , 0.00 };
	Eigen::Vector2d asmPlnStep{ 0.00 , 0.40 };
	
	// Set up visualization management
	RViz_MarkerManager mrkrMngr{};
	visualization_msgs::MarkerArray& markerArr = mrkrMngr.get_arr();
	
	// ~ Visualize Results ~
	if( false ){
		simplePhone.set_pose( origin_pose() );
		simplePhone.load_part_mrkrs_into_mngr( mrkrMngr );
	}else{
		if( false ){
			visualize_Morato2013_Level_1( mrkrMngr , 
										  Eigen::Vector2d( 0.0 , 0.0 ) , Eigen::Vector2d( 0.0 , 0.3 ) , Eigen::Vector2d( 0.1 , 0.0 ) , 
										  &simplePhone , moratoLvl1 );
		}else{
			visualize_tree_structure_plan( mrkrMngr , 
										   Eigen::Vector2d( 0.0 , 0.0 ) , Eigen::Vector2d( 0.0 , 0.3 ) , Eigen::Vector2d( 0.1 , 0.0 ) ,
										   rootNodeMoratoModFull );
		}
	}
		
	
	// std::vector<SuccessCode> vizChk = mrkrMngr.diagnose_markers_OK();
	
	// Connect to ROS
	ros::init( argc , argv , "graph_test_cpp" );
	// Set up a node with refresh rate
	ros::NodeHandle node_handle;
	ros::Publisher vis_arr = node_handle.advertise<visualization_msgs::MarkerArray> ( "visualization_marker_array" , 100 );
	ros::Rate r( updateHz ); // 30Hz refresh rate
	
	// 6. Set the animation loop and display
	while ( ros::ok() ){ // while roscore is running
        // cout << ".";

		// Wait for a subscriber to start looking for this topic
		while ( vis_arr.getNumSubscribers() < 1 ){
			if ( !ros::ok() ){ return 0; }
			printf( "." );
			ROS_WARN_ONCE( "Please create a subscriber to the marker" );
			sleep( 1 );
			cout << ".";
		} 
        
        vis_arr.publish( markerArr );

		r.sleep();
	}
	
	/// ___ END VISUALIZATION ______________________________________________________________________________________________________________
	
	return 0; // I guess everything turned out alright at the end!
}

// ___ End main ____________________________________________________________________________________________________________________________


/* === Spare Parts =========================================================================================================================


	
   ___ End Spare ___________________________________________________________________________________________________________________________
*/

