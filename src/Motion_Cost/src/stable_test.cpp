/*
stable_test.cpp
James Watson , 2018 January
Test integration of the ASP_3D lib --> ROS , Show markers and move them around

Dependencies: Cpp_Helpers , ROS , Eigen
Template Version: 2017-09-23
*/

// === Init ================================================================================================================================

// ~~~ Includes ~~~
// ~~ Standard ( Common includes not already in Cpp_Helpers.h ) ~~ 
//#include <iomanip> // - Output precision and printing
//#include <exception> // error handling
//#include <stdexcept> // library provided error types like 'std::range_error'
//#include <tuple> // --- standard tuple datatype
//#include <cstddef> // - alias types names:  size_t, ptrdiff_t
//#include <cstring> // - C Strings: strcomp 
//#include <algorithm> // sort, search, min, max, sequence operations

// ~~ ROS ~~
// ~ Core ~
#include <ros/ros.h> // --- ROS presides over all
#include <ros/package.h> // Where are we? // http://wiki.ros.org/Packages#C.2B-.2B-
// ~~ Includes ~~
#include <Eigen/Core> // ---- The living heart of Eigen
#include <Eigen/Dense> // --- Cross Product , etc.
#include <Eigen/Geometry> //- Quaternion , etc
// ~ RViz ~
#include <visualization_msgs/Marker.h> // ---- Display geometry
#include <visualization_msgs/MarkerArray.h> // RViz marker array
// ~~ Local ~~ 
// #include <Cpp_Helpers.h> // Shortcuts and Aliases , and Functions of general use for C++ programming
#include <ASP_3D.h> // ---- Assembly planning
#include <QuickHull.hpp> // Convex hull

// using namespace quickhull;

// ___ End Init ____________________________________________________________________________________________________________________________


// === Program Functions & Classes ===



// ___ End Functions & Classes ___


// === Program Vars ===

quickhull::QuickHull<double> qh; // hull object to be returned
std::vector<quickhull::Vector3<double>> pointCloud; // Collection of points to form a hull around

// ___ End Vars ___


// === main ================================================================================================================================

int main( int argc , char** argv ){
	
	srand( time( 0 ) ); // Random seed based on the current clock time
	
	// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
	
	sep( "Helper Function Test" );
	
	// ~~~ Distance to 2D Line Segment ~~~
	
	std::vector<Segment2D_ASP> segTestList;
	
	segTestList.push_back( Segment2D_ASP{ Eigen::Vector2d( 0.0 , 0.0 ) , Eigen::Vector2d( 4.0 , 0.0 ) } );
	segTestList.push_back( Segment2D_ASP{ Eigen::Vector2d( 4.0 , 0.0 ) , Eigen::Vector2d( 0.0 , 0.0 ) } );
	
	segTestList.push_back( Segment2D_ASP{ Eigen::Vector2d( 4.0 , 0.0 ) , Eigen::Vector2d( 4.0 , 4.0 ) } );
	segTestList.push_back( Segment2D_ASP{ Eigen::Vector2d( 4.0 , 4.0 ) , Eigen::Vector2d( 4.0 , 0.0 ) } );
	
	segTestList.push_back( Segment2D_ASP{ Eigen::Vector2d( 4.0 , 4.0 ) , Eigen::Vector2d( 0.0 , 4.0 ) } );
	segTestList.push_back( Segment2D_ASP{ Eigen::Vector2d( 0.0 , 4.0 ) , Eigen::Vector2d( 4.0 , 4.0 ) } );
	
	segTestList.push_back( Segment2D_ASP{ Eigen::Vector2d( 0.0 , 4.0 ) , Eigen::Vector2d( 0.0 , 0.0 ) } );
	segTestList.push_back( Segment2D_ASP{ Eigen::Vector2d( 0.0 , 0.0 ) , Eigen::Vector2d( 0.0 , 4.0 ) } );
	
	Eigen::Vector2d queryPnt{ 2.5 , 3.0 };
	
	size_t numElem = segTestList.size();
	
	for( size_t i = 0 ; i < numElem ; i++ ){
		
		cout << "Distance between " << segTestList[i] << " and ";  print_vec2d_inline( queryPnt );  
		cout << " : " << d_point_to_segment_2D( queryPnt , segTestList[i] ) << endl;
		
	}
	
	// ~~~ Winding Number ~~~
	
	Eigen::MatrixXd polygon1 = Eigen::MatrixXd::Zero( 5 , 2 );
	Eigen::MatrixXd polygon2 = Eigen::MatrixXd::Zero( 5 , 2 );
	Eigen::MatrixXd polygon3 = Eigen::MatrixXd::Zero( 5 , 2 );
	Eigen::MatrixXd polygon4 = Eigen::MatrixXd::Zero( 5 , 2 );
	
	Eigen::Vector2d insidePnt{ 2.5 , 3.0 };
	Eigen::Vector2d outsidPnt{ 5.5 , 3.0 };
	Eigen::Vector2d colinrPnt{ 4.0 , 1.0 };
	
	polygon1.row(0) = Eigen::Vector2d( 0.0 , 0.0 ); // CCW
	polygon1.row(1) = Eigen::Vector2d( 4.0 , 0.0 );
	polygon1.row(2) = Eigen::Vector2d( 4.0 , 4.0 );
	polygon1.row(3) = Eigen::Vector2d( 0.0 , 4.0 );
	polygon1.row(4) = Eigen::Vector2d( 0.0 , 0.0 );
	
	polygon2.row(0) = Eigen::Vector2d( 0.0 , 0.0 ); //  CW
	polygon2.row(1) = Eigen::Vector2d( 0.0 , 4.0 );
	polygon2.row(2) = Eigen::Vector2d( 4.0 , 4.0 );
	polygon2.row(3) = Eigen::Vector2d( 4.0 , 0.0 );
	polygon2.row(4) = Eigen::Vector2d( 0.0 , 0.0 );
	
	cout << "Inside CCW  : " << winding_num( insidePnt , polygon1 ) << endl;
	cout << "Outside CCW : " << winding_num( outsidPnt , polygon1 ) << endl;
	cout << "Inside CW   : " << winding_num( insidePnt , polygon2 ) << endl;
	cout << "Outside CW  : " << winding_num( outsidPnt , polygon2 ) << endl;
	cout << "Colinear CCW: " << winding_num( colinrPnt , polygon1 ) << endl;
	cout << "Colinear CW : " << winding_num( colinrPnt , polygon2 ) << endl;
	
	Eigen::Vector2d insideFPE{ 23.5702 , 6.4837e-13 };
	Eigen::Vector2d insideZro{ 23.5702 , 0.0        };
	
	polygon3.row(0) = Eigen::Vector2d(  23.5702 ,  70.7107      ); // Close to a stop on X+
	polygon3.row(1) = Eigen::Vector2d( -47.1405 ,   7.10543e-15 );
	polygon3.row(2) = Eigen::Vector2d(  23.5702 , -70.7107      );
	polygon3.row(3) = Eigen::Vector2d(  94.2809 ,   1.42109e-14 );
	polygon3.row(4) = Eigen::Vector2d(  23.5702 ,  70.7107      );
	
	polygon4.row(0) = Eigen::Vector2d(  23.5702 ,  70.7107 ); // Stop on X+
	polygon4.row(1) = Eigen::Vector2d( -47.1405 ,   0.0    );
	polygon4.row(2) = Eigen::Vector2d(  23.5702 , -70.7107 );
	polygon4.row(3) = Eigen::Vector2d(  94.2809 ,   0.0    );
	polygon4.row(4) = Eigen::Vector2d(  23.5702 ,  70.7107 );
	
	cout << "Inside Y~0 , Poly Close to a stop on X+ : " << winding_num( insideFPE , polygon3 ) << endl;
	cout << "Inside Y=0 , Poly Close to a stop on X+ : " << winding_num( insideZro , polygon3 ) << endl;
	cout << "Inside Y~0 , Poly            Stop on X+ : " << winding_num( insideFPE , polygon4 ) << endl;
	cout << "Inside Y=0 , Poly            Stop on X+ : " << winding_num( insideZro , polygon4 ) << endl;
	
	// ~~~ Pose Representation ~~~
	
	Eigen::Vector3d position{ 1.0 , 2.0 , 3.0 };
	Eigen::Vector3d axis{     0.0 , 0.0 , 1.0 };
	double angle = M_PI / 2.0;
	Eigen::Quaterniond orientation; orientation = Eigen::AngleAxis<double>( angle , axis );
	Pose_ASP examplePose = pose_from_position_orient( position , orientation );
	cout << "Constructed a Pose" << endl
		 << "Position: __ "   << examplePose.position    << endl
		 << "Orientation: "   << examplePose.orientation << endl
		 << "Matrix:" << endl << examplePose.homogMatx   << endl;
	
	// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
	
	sep( "Part Stability Test" );
	
	Incrementer assign_part_ID{ 0 }; // Instantiate a functor that will assign a unique number to each part
	
	// 0. Create file paths
	string pkgPath = ros::package::getPath( "motion_cost" );
	string EL_fileName  = pkgPath + "/Resources/EL_SCLD.stl";
	string CEE_fileName = pkgPath + "/Resources/CEE_SCLD.stl";
	string CUBE_fileName = pkgPath + "/Resources/CUBE.stl";
	cout << "Path to EL : " << EL_fileName  << endl;
	cout << "Path to CEE: " << CEE_fileName << endl;
	
	// 1. Create part object(s)
	Part_ASM EL_part{  assign_part_ID() , EL_fileName  };
	Part_ASM CEE_part{ assign_part_ID() , CEE_fileName };
	Part_ASM CUBE_part{ assign_part_ID() , CUBE_fileName };
	
	// 2. set the poses for the part(s)
	Eigen::Vector3d posEL;		
	Eigen::Vector3d posCUBE;  posCUBE <<  0.0d   ,  0.0d   ,  0.0d;
	if( true ){  posEL   <<  0.2d   ,  0.2d   ,  0.2d   ;  } 
	else{        posEL   <<  0.6d   ,  0.6d   ,  0.6d   ;  }
	Eigen::Quaterniond quatEL{              0.0d   ,  0.0d   ,  0.707d , -0.707d };
	EL_part.set_pose( posEL , quatEL );
	Eigen::Vector3d posCEE;		posCEE  <<  0.0d   ,  0.0d   ,  0.0d   ;
	Eigen::Quaterniond quatCEE{             1.0d   ,  0.0d   ,  0.0d   ,  0.0d   };
	Eigen::Quaterniond quatCUBE{             1.0d   ,  0.0d   ,  0.0d   ,  0.0d   };
	CEE_part.set_pose( posCEE , quatCEE );
	CUBE_part.set_pose( posCUBE , quatCUBE );
	
	//~ // 3. Calc supports
	
	//~ // ~~ EL ~~
	sep( "CUBE Supports" , 2 , '*' );
	CUBE_part.determine_support_stability();
	CUBE_part.report_support_stability();
	cout << endl;
	
	sep( "CEE Supports" , 2 , '*' );
	CEE_part.determine_support_stability();
	CEE_part.report_support_stability();
	cout << endl;
	
	sep( "EL Supports" , 2 , '*' );
	EL_part.determine_support_stability();
	EL_part.report_support_stability();
	cout << endl;
	
	// ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
	
	sep( "Visualizing Stability" );
	
	// == ROS Loop ==
	
	// = ROS Start =
	
	
	// 5. Start ROS
	srand( ( time( 0 ) % 1 ) * 1000.0 + getpid() ); // Random seed based on the current clock time
	
	ros::init( argc , argv , "stable_test_cpp" );
	
	
	// ros::Publisher vis_pub = node_handle.advertise<visualization_msgs::Marker>(       "visualization_marker"       , 0 );
	ros::NodeHandle node_handle;
	ros::Publisher vis_arr = node_handle.advertise<visualization_msgs::MarkerArray> ( "visualization_marker_array" , 100 );
	int updateHz = 30;
	ros::Rate r( updateHz ); // 30Hz refresh rate
	
	RViz_MarkerManager mrkrMngr{};
	
	std::vector<bool> stabilityVec = EL_part.get_stability_determination();
	size_t len = stabilityVec.size();
	Eigen::Vector2d offsetStep{ 0.30 , 0.00 };
	Eigen::Vector2d putdown_XY{ 0.00 , 0.00 };
	
	// 1. For each of the possible supports
	for( size_t i = 0 ; i < len ; i++ ){

		// 2. Determine if the support is stable
		if( stabilityVec[i] ){
			
			// 3. Calc the offset
			putdown_XY += offsetStep;
			
			// 4. Set the putdown pose
			EL_part.set_pose( EL_part.get_lab_pose_for_support_index( i , putdown_XY ) );
			
			// 5. Calc positions of the face vertices
			EL_part.calc_all_faces();
			
			// 6. For each of the faces , create a marker and load them into the marker manager , cycling through colors
			EL_part.load_face_mrkrs_into_mngr( mrkrMngr );
		}
	}
	
	// Fetch the markers for publishing
	visualization_msgs::MarkerArray& markerArr = mrkrMngr.get_arr();
	
	// 7. Set the animation loop and display
	while ( ros::ok() ){ // while roscore is running
        // cout << ".";

		// Wait for a subscriber to start looking for this topic
		while ( vis_arr.getNumSubscribers() < 1 ){
			if ( !ros::ok() ){ return 0; }
			printf( "." );
			ROS_WARN_ONCE( "Please create a subscriber to the marker" );
			sleep( 1 );
			cout << ".";
		} 
        
        // markerArr = mrkrMngr.rebuild_mrkr_array();
        
        vis_arr.publish( markerArr );

		r.sleep();
	}
	
	// __ End ROS __
	
	return 0; // I guess everything turned out alright at the end!
}

/// ___ END VISUAL _________________________________________________________________________________________________________________________

// ___ End main ____________________________________________________________________________________________________________________________


/* === Spare Parts =========================================================================================================================



   ___ End Spare ___________________________________________________________________________________________________________________________
*/
