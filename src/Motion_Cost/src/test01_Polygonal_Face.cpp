#include <ctime> // -- NOT SURE WHO IS USING
#include <cstdlib> //- NOT SURE WHO IS USING
#include <iostream> // NOT SURE WHO IS USING

#include <Cpp_Helpers.h> // ----------------- Functions of general use
#include <ASP_3D.h> // ---------------------- Assembly Planning Lib

#include <ros/ros.h> // --------------------- ROS presides over all
#include <ros/package.h> // ----------------- Where are we? // http://wiki.ros.org/Packages#C.2B-.2B-

#include <igl/read_triangle_mesh.h> // ------ URL , Load STL : https://github.com/libigl/libigl/issues/273
#include <igl/readOFF.h> // ----------------- load OFF ( BUNNY )
#include <igl/remove_duplicate_vertices.h> // More compact representation of the model
#include <igl/per_face_normals.h> // -------- Needed for agglomerating facets into faces
#include <igl/viewer/Viewer.h> // ----------- Display mesh
#include "tutorial_shared_path.h" // -------- Path to all of the example models

#include <nanoflann.hpp> // ----------------- kd-tree NN search



using namespace std; // Standard linux libraries namespace
using namespace igl; // igl namespace 
using namespace Eigen;
using namespace nanoflann;

// Dimensionality set at compile-time: Explicit selection of the distance metric: L1 , L2 , or L2_Simple
// typedef nanoflann::KDTreeEigenMatrixAdaptor< Eigen::Matrix< double , Dynamic , Dynamic > , nanoflann::metric_L1 >  kd_tree_t;
typedef nanoflann::KDTreeEigenMatrixAdaptor< Eigen::Matrix< double , Dynamic , Dynamic > , nanoflann::metric_L2 >  kd_tree_t;
// typedef nanoflann::KDTreeEigenMatrixAdaptor< Eigen::Matrix< double , Dynamic , Dynamic > , nanoflann::metric_L2_Simple >  kd_tree_t;

Incrementer assign_part_ID{ 0 }; // Instantiate a functor that will assign a unique number to each part

int main( int argc , char *argv[] ){
	
	// Polygon_ASM face{};

	ASP_init_colors(); // Needed to make Rainbow work
	Rainbow faceColors{};

	bool BUNNY = false;
	
	// 0. Output Test
	sep( "Test 01: Polygonal Faces" );
	//~ cout << "Created a polygon with " << face.get_len() << " points!" << endl
		 //~ << "Center: " << endl << face.get_center() << endl
		 //~ << "Normal: " << endl << face.get_norm()   << endl ;
	
	// 1. Load model
	// Announce the file that is being worked on
	
	Eigen::MatrixXd OV;
	Eigen::MatrixXi OF;
	
	// string filename = "../Resources/EL.stl"; // Path to the mesh to load 
	string pkgPath = ros::package::getPath("motion_cost");
	cout << "Found the package path for 'motion_cost' : " << pkgPath << endl;
	string filename = pkgPath + "/Resources/EL.stl";
	cout << "Constructed the full STL path : " << filename << endl;
	
	if( BUNNY ){ // Load the BUNNY
		// Load a mesh in OFF format
		igl::readOFF( TUTORIAL_SHARED_PATH "/bunny.off" , OV , OF );
	} else { // load the block
		// string filename = "../Resources/EL.stl"; // Path to the mesh to load
		cout << "  'working on'  = " << filename << endl;
		igl::read_triangle_mesh( filename , OV , OF );
	}
	
	cout << " original  mesh containts " << OV.rows() << " vertices and " << OF.rows() << " faces" << endl;
	
	// 2. Reduce points
	Eigen::MatrixXd V;
	Eigen::MatrixXi F , SVI , SVJ;
	igl::remove_duplicate_vertices( OV , OF , 0.0 , V , SVI , SVJ , F );
	cout << " after removing duplicates, mesh containts " << V.rows() << " vertices and " << F.rows() << " faces" << endl;
	
	// 3. List facets
	cout << "Facets:" << endl << F << endl; // These are indices of V that comprise the facets
	
	// 4. Get normals
	/* Surface normals are a basic quantity necessary for rendering a surface. There are a variety of ways to compute and store 
	 * normals on a triangle mesh. Example 201 demonstrates how to compute and visualize normals with libigl. */
	Eigen::MatrixXd N_faces;
	igl::per_face_normals( V , F , N_faces ); // Compute per-face normals
	
	cout << "Facet Normals:" << endl << N_faces << endl;
	
	// 5. Get facet centers
	Eigen::MatrixXd centers = get_facet_centers( V , F );
	cout << "Facet Centers:" << endl << centers << endl;
	
	// 6. Build a kd-tree from the facet centers
	kd_tree_t centerNNqueryTree( centers , 10 /* max leaf */ );
	centerNNqueryTree.index->buildIndex();
		
	// 5. Agglomerate Faces
	
	// Create vectors to represent faces
	vector<vector<size_t>>  faceLists; //-- vectors of facet indices that comprise faces
	vector< Eigen::Vector3d , Eigen::aligned_allocator< Eigen::Vector3d > > faceNormals; // normals of the faces
	
	size_t i         = 0        , 
		   j         = 0        ,
		   k         = 0        ,
		   numRows   = F.rows() , 
		   currFacet = 0        ,
		   lastFacet = 0        ,
		   addCount  = 0        ,
		   notCount  = 0        ;
		   
	list<size_t> unusedFacets = lst_range( (size_t) 0 , numRows - 1 ); // Get a list of facet indices that have not been clustered
	list<size_t>::iterator find_it; // Iterator for list searches
	
	// ~ kd-tree params and vars ~
	size_t clusterLimit = 750; // the maximum number of facets that will be considered, per cluster
	vector<size_t>                  ret_indexes( clusterLimit ); // Indices returned by the NN search
	vector<double>                  out_dists( clusterLimit ); // distances returned by the NN search
	nanoflann::KNNResultSet<double> resultSet( clusterLimit ); // nanoflann result object
	std::vector<double> query_pt(3);
	
	// ~ Cluster Criteria ~
	double CRITRN_SEP = 1.0d; //---- Separation criterion , mm
	double CRITRN_ANG = 0.17453d; // Angle criterion      , rad
	double diffSep;
	double diffAng;
	bool   match;
	size_t avgLen = 0;
	Eigen::Vector3d scratchRow;
	
	// currFacet = unusedFacets.front(); // get first item from the list // Order does not specifically matter // it will be removed soon
	
	j = 0;
	
	printf( "DEBUG: About to start the loop with %4zi unused facets \n" , unusedFacets.size() );
	
	sep( "Wan / Harada Clustering" );
	
	while( unusedFacets.size() > 0 ){ // While there are unused facets
		
		
		// 5.1. Pop facet
		printf( "DEBUG: About to pop a facet ... \n" );
		currFacet = unusedFacets.front();
		unusedFacets.pop_front();
		
		// 5.2. Nucleate face
		// Create a new face group and add the facet
		printf( "DEBUG: About to nucleate face ... \n" );
		vector<size_t> temp;
		temp.push_back( currFacet ); 
		faceLists.push_back( temp ); // Add the new group to the face vector
		faceNormals.push_back( N_faces.row( currFacet ) ); // Add the face normal
		
		printf( "DEBUG: About to search neighbors ... \n" );
		// 5.3. Get NN
		for( i = 0 ; i < 3 ; i++){ query_pt[i] = centers( currFacet , i ); }  // Assign to the query point
		// Get the 'clusterLimit' nearest neighbors to current facet
		resultSet.init( &ret_indexes[0] , &out_dists[0] ); // Point the kd-tree at the response arrays to fill
		centerNNqueryTree.index->findNeighbors( resultSet , &query_pt[0] , nanoflann::SearchParams( 10 ) );
		
		printf( "DEBUG: About to cluster ... \n" );
		// 5.4. For each NN , Try to grow the cluster for as long as we are meeting the criteria
		//      The idea is that if we spread out among NN , we will extend until the edge contiguously
		for( i = 0 ; i < clusterLimit ; i++ ){
			// printf( "\tNew current facet ... \n" );
			currFacet = ret_indexes[i];
			// printf( "\tSearching ... \n" );
			// 5.4.1. If unused
			find_it = find( unusedFacets.begin( ) , unusedFacets.end() , currFacet );
			if( find_it != unusedFacets.end() ){
				// 5.4.1.1. Test criteria
				// Calculate the perpendicular separation distance between the facet center and the nucleus plane
				diffSep = dist_to_plane( N_faces.row( currFacet ) , centers.row( faceLists[j][0] ) , centers.row( currFacet ) );
				// Calculate the angle between the normals
				diffAng = angle_between( N_faces.row( currFacet ) , faceNormals[j] );
				// 5.4.1.2. Criteria PASS
				if( diffSep <= CRITRN_SEP && diffAng <= CRITRN_ANG ){
					// 5.4.1.2.1. Pop facet
					unusedFacets.remove( currFacet );
					// 5.4.1.2.2. Add facet
					// Add the facet to the group
					faceLists[j].push_back( currFacet );
					// Calculate a new average normal
					scratchRow = N_faces.row( currFacet );
					faceNormals[j] = ( faceNormals[j] * (double) faceNormals.size() + scratchRow ) / ( faceNormals.size() + 1 );
					faceNormals[j].normalize();
				} else { break; } // 5.4.1.3. Criteria FAIL : break
				// 5.4.1.4. Max neighbors : NO OP , This is automatically enforced by the for loop
			} // 5.4.2. Used: NO OP 
		}
		j++; // Advance to the next cluster
		printf( "DEBUG: There are %4zi unused facets remaining \n" , unusedFacets.size() );
	}
	
	printf( "DEBUG: Located %4zi faces \n" , faceNormals.size() );
	
	int I = 0 ,
		J = 0 ;
	
	// Print out the adjacency list to see what it looks like
	vector<vector<int>> adj_lst = get_adjacency_list( F );
	for( I = 0 ; I < adj_lst.size() ; I++ ){
		for( J = 0 ; J < adj_lst[I].size() ; J++ ){ printf( "%5i" , adj_lst[I][J] ); } printf( "\n" );
	} // Looks nice! , 3 neighbors per facet as expected
	
	
	sep( "Adjacency Clustering" );
	
	// !! RESET !!
	unusedFacets.clear();
	unusedFacets = lst_range( (size_t) 0 , numRows - 1 ); // Get a list of facet indices that have not been clustered
	
	faceLists.clear();
	
	faceNormals.clear();
	
	vector<vector<int>>  faceLists2; //-- vectors of facet indices that comprise faces
	
	// adj_lst.clear();
	
	FaceIndices rtnStruct = faces_from_facets( V , F , N_faces , CRITRN_ANG );
	
	faceLists2  = rtnStruct.faceLists;
	faceNormals = rtnStruct.faceNormals;
	
	Polygon_ASM facePoly;
	
	printf( "DEBUG: Located %4zi faces \n" , faceNormals.size() );
	
	vector<int> faceRow;
	
	
	
	for( I = 0 ; I < faceLists2.size() ; I++ ){
		for( J = 0 ; j < faceLists2[I].size() ; J++ ){ printf( "%5i" , faceLists2[I][J] ); } printf( "\n" );
		
		//~ Polygon_ASM::Polygon_ASM( Eigen::MatrixXd& , 
								  //~ Eigen::MatrixXi& , 
								  //~ __gnu_cxx::__alloc_traits<std::allocator<std::vector<long unsigned int> > >::value_type& , 
								  //~ Eigen::Vector3d )
		
		faceRow = faceLists2[I];
		
		// Create a poly from each face
		facePoly = Polygon_ASM( V , 
								F , 
								faceRow , // faceLists2[i] , 
								Vec3D_from_aligned_stdvec( faceNormals , I ) );
	}
	
	
	printf( "Adjacency List \n" );
	// Print out the adjacency list to see what it looks like
	for( i = 0 ; i < adj_lst.size() ; i++ ){
		for( j = 0 ; j < adj_lst[i].size() ; j++ ){ printf( "%5i" , adj_lst[i][j] ); } printf( "\n" );
	} 
	
	// cout << _RED << endl;
	
	cout << "DEBUG: V has " << V.rows() << " rows" << endl;
	Eigen::MatrixXd C = Eigen::MatrixXd::Zero( V.rows() , 3 );
	
	cout << "DEBUG: About to assign colors ..." << endl;
	for( i = 0 ; i < faceLists.size() ; i++ ){ // For each face
		for( j = 0 ; j < faceLists[i].size() ; j++ ){ // For each facet in the face
			faceColors.advance();
			for( k = 0 ; k < 3 ; k++ ){ // For each vertex of the facet
				cout << "\tAssigning color to vertex " << F( faceLists[i][j] , k ) << endl;
				cout << "Color: " << endl << faceColors.get_color() << endl;
				C.row( F( faceLists[i][j] , k ) ) = faceColors.get_color();
			}
		}
	}
	
	cout << "Vertex Colors" << endl;
	
	// In order to have colored the faces individually, each face has to have its own vertices.  When the vertices are shared,
	// then the colors will blend together from the vertices.  Right now this coloring is not critical , leave it on the side for now
	
	for( i = 0 ; i < C.rows() ; i++ ){ // For each face
		printf( " %5.3f %5.3f %5.3f \n" , C( i , 0 ) , C( i , 1 ) , C( i , 2 ) );
	}
	
	// ~~~~~~~~~~~ Part Test ~~~~~~~~~~~
	Part_ASM EL_part( assign_part_ID() , 
					  filename , 
				      OV , OF , 
				       V ,  F , 
				      faceLists2 ,
				      faceNormals );
	
	
	// Plot the mesh
	igl::viewer::Viewer viewer;
	viewer.data.set_mesh( V , F );
	
	// Add per-vertex colors
	viewer.data.set_colors( C );

	// Launch the viewer
	viewer.launch();
	
	return 0; // Everything turned out OK in the end
}
