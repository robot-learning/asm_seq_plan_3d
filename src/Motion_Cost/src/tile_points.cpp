/*
tile_points.cpp
James Watson , 2018 May
Test integration of the ASP_3D lib --> ROS , Show markers and move them around

Dependencies: Cpp_Helpers , ROS , Eigen , RAPID
Template Version: 2017-09-23
*/

// === Init ================================================================================================================================

// ~~~ Includes ~~~
// ~~ ROS ~~
// ~ Core ~
#include <ros/ros.h> // --- ROS presides over all
#include <ros/package.h> // Where are we? // http://wiki.ros.org/Packages#C.2B-.2B-
// ~~ Includes ~~
#include <Eigen/Core> // ---- The living heart of Eigen
#include <Eigen/Dense> // --- Cross Product , etc.
#include <Eigen/Geometry> //- Quaternion , etc
// ~ RViz ~
#include <visualization_msgs/Marker.h> // ---- Display geometry
#include <visualization_msgs/MarkerArray.h> // RViz marker array
// ~~ Local ~~ 
#include <Cpp_Helpers.h> // Shortcuts and Aliases , and Functions of general use for C++ programming
#include <ASP_3D.h> // ---- Assembly geometry
#include <AsmSeqPlan.h> //- Graph planning


// using namespace quickhull;

// ___ End Init ____________________________________________________________________________________________________________________________

/// === PREBUILT ASSEMBLIES ================================================================================================================

Incrementer assign_part_ID{ 0 }; // Instantiate a functor that will assign a unique number to each part
Incrementer assign_asm_ID{ 0 }; //- Instantiate a functor that will assign a unique number to each assembly

Assembly_ASM* simple_phone(){
	
	double SML_Z_CRCTN = 999.7 * EPSILON; // Small Z correction
	
	cout << "Using small Z correction: " << SML_Z_CRCTN << endl;
	
	Assembly_ASM* simplePhone = new Assembly_ASM( assign_asm_ID() );
	
	// ~~ 0. Create file paths ~~
	// ~ Fetch the folder to the STL files ~
	string pkgPath = ros::package::getPath( "motion_cost" );
	string resourcePath = pkgPath + "/Resources/Assembly_CAD/SimplePhone/";
	// ~ Create full paths to STLs ~
	std::vector<string> fNames;
/* 0*/ fNames.push_back( resourcePath + "body_SCL.stl" );      Eigen::Vector3d body_shift{    -3.92054e-05 ,  9.47465e-05 , -0.00919433 };
/* 1*/ fNames.push_back( resourcePath + "antenna_SCL.stl" );   Eigen::Vector3d antenna_shift{  0.005       , -0.0175      , -0.00375    };
/* 2*/ fNames.push_back( resourcePath + "bridgeAB_SCL.stl" );  Eigen::Vector3d bridgeAB_shift{ 0.0         ,  0.0         , -0.0025     };
/* 3*/ fNames.push_back( resourcePath + "bridgeBC_SCL.stl" );  Eigen::Vector3d bridgeBC_shift{ 0.0         ,  0.0         , -0.0025     };
/* 4*/ fNames.push_back( resourcePath + "chipA1_SCL.stl" );    Eigen::Vector3d chipA1_shift{   0.0         ,  0.0         , -0.0025     };
/* 5*/ fNames.push_back( resourcePath + "chipA1_SCL.stl" );    
/* 6*/ fNames.push_back( resourcePath + "chipB1_SCL.stl" );    Eigen::Vector3d chipB1_shift{  -0.002       ,  0.00666667  , -0.0025     };
/* 7*/ fNames.push_back( resourcePath + "chipB2_SCL.stl" );    Eigen::Vector3d chipB2_shift{   0.00371429  ,  0.0114286   , -0.0025     };
/* 8*/ fNames.push_back( resourcePath + "chipC1_SCL.stl" );    Eigen::Vector3d chipC1_shift{   0.0         , -0.00697957  , -0.0025     };
/* 9*/ fNames.push_back( resourcePath + "moduleA_SCL.stl" );   Eigen::Vector3d moduleA_shift{  0.0         ,  0.000178571 , -0.00446429 };
/*10*/ fNames.push_back( resourcePath + "moduleB_SCL.stl" );   Eigen::Vector3d moduleB_shift{  7.01754e-05 ,  0.0         , -0.00453947 };
/*11*/ fNames.push_back( resourcePath + "moduleC_SCL.stl" );   Eigen::Vector3d moduleC_shift{  0.0         ,  1.39405e-05 , -0.00428903 };
/*12*/ fNames.push_back( resourcePath + "panel_SCL.stl" );     Eigen::Vector3d panel_shift{    0.0         , 0.0          , -0.00125    };
	// ~ Create parts ~
	size_t fLen = fNames.size();
	std::vector<Part_ASM> phoneParts;
	for( size_t i = 0 ; i < fLen ; i++ ){
		//~ cout << fNames[i] << endl;
		phoneParts.push_back( Part_ASM{   assign_part_ID() , fNames[i]   } );
	}
	//~ cout << "Added " << phoneParts.size() << " parts." << endl;
	
	// 2. Create an assembly , add parts
	
	bool ACTIVATE_ALL = false;
	
	// ~~ Body ~~
if( true || ACTIVATE_ALL ){
/* 0*/ simplePhone->add_part_w_pose( phoneParts[ 0] , Pose_ASP{ -body_shift , no_turn_quat() } ); // body
}

	// ~~ Module A ~~
if( true || ACTIVATE_ALL ){
	/* 9*/ simplePhone->add_part_w_pose( phoneParts[ 9] , Pose_ASP{ -moduleA_shift  + Eigen::Vector3d(  0.0000 ,  0.04000 ,  0.0025 + SML_Z_CRCTN ) , // moduleA
																	 no_turn_quat()                                       } ); 
	/* 4*/ simplePhone->add_part_w_pose( phoneParts[ 4] , Pose_ASP{ -chipA1_shift   + Eigen::Vector3d(  0.0000 ,  0.04875 ,  0.0100 + SML_Z_CRCTN * 3 ) , // chipA1
																	 no_turn_quat()                                       } ); 
	/* 5*/ simplePhone->add_part_w_pose( phoneParts[ 5] , Pose_ASP{ -chipA1_shift   + Eigen::Vector3d(  0.0000 ,  0.03875 ,  0.0100 + SML_Z_CRCTN * 3 ) , // chipA1
																	 no_turn_quat()                                       } ); 
}

	// ~~ Module B ~~
if( true || ACTIVATE_ALL ){
	
	if( true || ACTIVATE_ALL )
	/*10*/ simplePhone->add_part_w_pose( phoneParts[10] , Pose_ASP{ -moduleB_shift  + Eigen::Vector3d(  0.0000 ,  0.00000 ,  0.0025  ) , // moduleB
																 no_turn_quat()                                       } ); 
	if( true || ACTIVATE_ALL )
	/* 6*/ simplePhone->add_part_w_pose( phoneParts[ 6] , Pose_ASP{ -chipB1_shift   + Eigen::Vector3d( -0.0075 ,  0.01000 ,  0.0100   ) , // chipB1
																 no_turn_quat()                                       } ); 
	if( true || ACTIVATE_ALL )
	/* 7*/ simplePhone->add_part_w_pose( phoneParts[ 7] , Pose_ASP{ -chipB2_shift   + Eigen::Vector3d(  0.0075 ,  0.01000 ,  0.0100   ) , // chipB2
																 no_turn_quat()                                       } ); 
}

	// ~~ Module C ~~
if( true || ACTIVATE_ALL ){
	/*11*/ simplePhone->add_part_w_pose( phoneParts[11] , Pose_ASP{ -moduleC_shift  + Eigen::Vector3d(  0.0000 , -0.04000 ,  0.0025 ) , // moduleC
		                                                     no_turn_quat()                                       } ); 
	/* 8*/ simplePhone->add_part_w_pose( phoneParts[ 8] , Pose_ASP{ -chipC1_shift   + Eigen::Vector3d(  0.0000 , -0.04750 ,  0.0100 ) , // chipC1
																 no_turn_quat()                                       } ); 
}

	// ~~ Misc & Bridges ~~
if( true || ACTIVATE_ALL ){ 
	/* 2*/ simplePhone->add_part_w_pose( phoneParts[ 2] , Pose_ASP{ -bridgeAB_shift + Eigen::Vector3d(  0.0000 ,  0.02250 ,  0.0100 + SML_Z_CRCTN ) , // bridgeAB
																 no_turn_quat()                                       } ); 
	/* 3*/ simplePhone->add_part_w_pose( phoneParts[ 3] , Pose_ASP{ -bridgeBC_shift + Eigen::Vector3d(  0.0000 , -0.02500 ,  0.0100 + SML_Z_CRCTN ) , // bridgeBC
																 no_turn_quat()                                       } ); 
	/* 1*/ simplePhone->add_part_w_pose( phoneParts[ 1] , Pose_ASP{ -antenna_shift  + Eigen::Vector3d( -0.0200 ,  0.05250 ,  0.0050 + SML_Z_CRCTN ) , // antenna
																 no_turn_quat()                                       } ); 
}
	
	// 3. Determine the local freedom of each part (NDBG)
	simplePhone->recalc_geo();
		
	return simplePhone;
}

Assembly_ASM* simple_cube(){
	
	Assembly_ASM* simpleCube = new Assembly_ASM( assign_asm_ID() );
	
	// ~~ 0. Create file paths ~~
	// ~ Fetch the folder to the STL files ~
	string pkgPath = ros::package::getPath( "motion_cost" );
	string resourcePath = pkgPath + "/Resources/Assembly_CAD/SimpleCube/";
	
	// ~ Create full paths to STLs ~
	std::vector<string> fNames;
	fNames.push_back( resourcePath + "PocketPlate.stl" );	Eigen::Vector3d pcktShift{ -1.5 , -1.5 , -0.242647 }; 
	fNames.push_back( resourcePath + "MeterCube.stl" ); 	Eigen::Vector3d cubeShift{ -0.5 , -0.5 , -0.5      }; 
	
	// ~ Create parts ~
	size_t fLen = fNames.size();
	std::vector<Part_ASM> cubeParts;
	for( size_t i = 0 ; i < fLen ; i++ ){
		cout << fNames[i] << endl;
		cubeParts.push_back( Part_ASM{   assign_part_ID() , fNames[i]   } );
	}
	cout << "Added " << cubeParts.size() << " parts." << endl;
	
	simpleCube->add_part_w_pose( cubeParts[ 0] , Pose_ASP{ -pcktShift + Eigen::Vector3d(  0.0000 ,  0.0000 ,  0.0000 ) , // Pocket Plate
																 no_turn_quat()                                        } ); 
	simpleCube->add_part_w_pose( cubeParts[ 1] , Pose_ASP{ -cubeShift + Eigen::Vector3d(  1.0000 ,  1.0000 ,  0.2500 ) , // Meter Cube
																 no_turn_quat()                                        } ); 
	simpleCube->recalc_geo();
	return simpleCube;
}

/// ___ END PREBUILT _______________________________________________________________________________________________________________________

// === Program Vars ===

string projectOuputDir = "/home/jwatson/output/";

// ___ End Vars ___



// === main ================================================================================================================================

int main( int argc , char** argv ){
	
	srand( time( 0 ) ); // Random seed based on the current clock time
	
	
	/// == Planning ==
	
	bool RAY1 = true  ,
	     RAY2 = true  ;
	
	// 0. Get dirs from env
	string pkgPath = ros::package::getPath( "motion_cost" );
	string resourcePath = pkgPath + "/Resources/Assembly_CAD/SimpleCube/";
	
	// 1. Create a part and pose it in the lab frame
	
	Part_ASM cubePart{ assign_part_ID() , resourcePath + "MeterCube.stl" };
	cubePart.set_pose( origin_pose() );
	
	// 2. Create a ray that won't pass through a tri boundary
	Eigen::Vector3d nudge( -0.5 , -0.5 , -0.5 );
	// ~ Ray 1 ~
	Eigen::Vector3d ray1org(  0.25 ,  0.35 ,  2.00 );  ray1org += nudge;
	Eigen::Vector3d ray1dir(  0.00 ,  0.00 , -1.00 );
	// ~ Ray 2 ~
	Eigen::Vector3d ray2org(  0.75 ,  0.85 , -1.00 );  ray2org += nudge;
	Eigen::Vector3d ray2dir(  0.00 ,  0.00 ,  1.00 );
	
	// 3. Fetch the lab VFN
	TriMeshVFN_ASP  cubeMesh = cubePart.get_lab_VFN();
	Eigen::MatrixXd meshBox  = AABB( cubeMesh );
	
	// 4. Request intersections , print
	sep( "Cube VFN" );
	cout << "V: " << endl
		 << cubeMesh.V << endl
		 << "F: " << endl
		 << cubeMesh.F << endl
		 << "N: " << endl
		 << cubeMesh.N << endl;
	
	if( RAY1 ){
		// ~ Ray 1 ~
		
		cout << "Ray 1 about to intersect ... " << endl;
		
		RayHits ray1hits = ray_intersect_VFN( ray1org , ray1dir , cubeMesh );
		
		sep( "Ray 1 Intersection" );
		cout << "Entries: " << endl
			 << ray1hits.enter << endl
			 << "Exits: " << endl
			 << ray1hits.exit << endl; 
			 
		cout << "About to collide with AABB ..." << endl;
			 
		cout << "Intersects AABB at: " << ray_intersect_AABB( ray1org , ray1dir , meshBox ) << endl;
			 
		cout << "Complete!" << endl;
	 }
	
	if( RAY2 ){
		// ~ Ray 2 ~
		
		cout << "Ray 2 about to intersect ... " << endl;
		
		RayHits ray2hits = ray_intersect_VFN( ray2org , ray2dir , cubeMesh );
		
		sep( "Ray 2 Intersection" );
		cout << "Entries: " << endl
			 << ray2hits.enter << endl
			 << "Exits: " << endl
			 << ray2hits.exit << endl; 
			 
		cout << "About to collide with AABB ..." << endl;
			 
		cout << "Intersects AABB at: " << ray_intersect_AABB( ray2org , ray2dir , meshBox ) << endl;
		
		cout << "Complete!" << endl;
	}
	
	sep( "Cube Assembly Intersections" );
	
	cout << "About to create ASM ... " << endl;
	
	Assembly_ASM* cubePlat = simple_cube();
	cubePlat->set_pose( origin_pose() );
	
	cout << "About to tile with points ... " << endl;
	
	Eigen::Vector3d approach = Eigen::Vector3d(  1.0 ,  1.0 ,  0.0 );
	Eigen::Vector3d perpAxis = Eigen::Vector3d(  0.0 ,  0.0 ,  1.0 );
	
	RayHits graspPnts = get_opposite_grasp_pairs_for_ASM_and_direction( cubePlat , 
																		approach , 
																		perpAxis , 2.0 ,
																		0.25 );
	
	cout << "About to print points ... " << endl;
	
	cout << "Entries:"      << endl 
		 << graspPnts.enter.rows() << endl
		 << "Exits:"        << endl 
		 << graspPnts.exit.rows()  << endl;
		 
	cout << "Entry-Exit Pairs" << endl;
	Eigen::Vector3d entr;  Eigen::Vector3d exit;
	double nAng , xAng;
	for( size_t i = 0 ; i < graspPnts.enter.rows() ; i++ ){
		entr = graspPnts.enter.row(i);
		nAng = graspPnts.n_Metric(i,0);
		exit = graspPnts.exit.row(i);
		xAng = graspPnts.x_Metric(i,0);
		cout << "Enter: " << entr << " , Angle: " << nAng << " , Exit: " << exit <<  " , Angle: " << xAng << endl;
	}
	
	/// __ End Planning __
	
	
	/// === VISUALIZATION ==================================================================================================================
	
	// == ROS Loop ==
	
	// = ROS Start =
	
	// 5. Start ROS
	
	// ~ Animation Vars ~
	srand( ( time( 0 ) % 1 ) * 1000.0 + getpid() ); // Random seed based on the current clock time
	int updateHz = 30;
	Eigen::Vector2d offsetStep{ 0.30 , 0.00 };
	Eigen::Vector2d asmPlnStep{ 0.00 , 0.40 };
	
	// Set up visualization management
	RViz_MarkerManager mrkrMngr{};
	visualization_msgs::MarkerArray& markerArr = mrkrMngr.get_arr();
	
	
	/// == LOAD MARKERS HERE ===
		
	cubePlat->load_part_mrkrs_into_mngr( mrkrMngr );
	
	Eigen::Vector3d bgn;
	Eigen::Vector3d end;
	double segLen   = 0.45;
	float  lineThic = 0.02;
	
	for( size_t i = 0 ; i < graspPnts.enter.rows() ; i++ ){
		
		// Add the entry marker
		entr = graspPnts.enter.row(i);
		bgn  = entr - approach * segLen;
		end  = entr;
		mrkrMngr.add_marker(  
			get_straigtPath_marker( bgn , end , 
									lineThic , 0.0 , 1.0 , 0.0 , 1.0 )
		);
		
		// Add the exit marker
		exit = graspPnts.exit.row(i);
		bgn  = exit + approach * segLen;
		end  = exit;
		mrkrMngr.add_marker(  
			get_straigtPath_marker( bgn , end , 
									lineThic , 0.0 , 1.0 , 0.0 , 1.0 )
		);
	}

	/// __ END MARKER LOADING __
		
	
	// std::vector<SuccessCode> vizChk = mrkrMngr.diagnose_markers_OK();
	
	// Connect to ROS
	ros::init( argc , argv , "graph_test_cpp" );
	// Set up a node with refresh rate
	ros::NodeHandle node_handle; // Constructor needs no params
	ros::Publisher vis_arr = node_handle.advertise<visualization_msgs::MarkerArray> ( "visualization_marker_array" , 100 );
	ros::Rate r( updateHz ); // 30Hz refresh rate
	
	// 6. Set the animation loop and display
	while ( ros::ok() ){ // while roscore is running
        // cout << ".";

		// Wait for a subscriber to start looking for this topic
		while ( vis_arr.getNumSubscribers() < 1 ){
			if ( !ros::ok() ){ return 0; }
			printf( "." );
			ROS_WARN_ONCE( "Please create a subscriber to the marker" );
			sleep( 1 );
			cout << ".";
		} 
		
		/// == UPDATE / ANIMATE MARKERS HERE ===
	
		
	
		/// __ END UPDATE / ANIMATE __	
        
        vis_arr.publish( markerArr );

		r.sleep();
	}
	
	/// ___ END VISUALIZATION ______________________________________________________________________________________________________________
	
	return 0; // I guess everything turned out alright at the end!
}

// ___ End main ____________________________________________________________________________________________________________________________


/* === Spare Parts =========================================================================================================================


	
   ___ End Spare ___________________________________________________________________________________________________________________________
*/

