cmake_minimum_required( VERSION 2.8.3 )
project( simple_render )

## Add support for C++11, supported in ROS Kinetic and newer
add_definitions(-std=c++11)

# === Project Variables ===

message( STATUS "CMAKE_MODULE_PATH: ${CMAKE_MODULE_PATH}" )

# ___ End Variables ___


# === Required Packages ===

## Find catkin macros and libraries
## if COMPONENTS list like find_package(catkin REQUIRED COMPONENTS xyz)
## is used, also find other catkin packages
find_package(catkin REQUIRED COMPONENTS
  cmake_modules
  roscpp
  std_msgs
  tf2
  tf2_ros # This is separate from vanilla tf2!!
  visualization_msgs
  # fcl # The finder is supposed to get this
  # FCL REQUIRED # Is there a difference between this and the above?
  # Eigen3 REQUIRED # 2017-07-28
)

# ___ End Packages ___

## Uncomment this if the package has a setup.py. This macro ensures
## modules and global scripts declared therein get installed
## See http://ros.org/doc/api/catkin/html/user_guide/setup_dot_py.html
# catkin_python_setup()

################################################
## Declare ROS messages, services and actions ##
################################################

# add_message_files( FILES Message1.msg Message2.msg ) ## Generate messages in the 'msg' folder


# add_service_files( FILES Service1.srv Service2.srv ) ## Generate services in the 'srv' folder


# add_action_files( FILES Action1.action Action2.action ) ## Generate actions in the 'action' folder

## Generate added messages and services with any dependencies listed here
# generate_messages( DEPENDENCIES std_msgs visualization_msgs ) 

################################################
## Declare ROS dynamic reconfigure parameters ##
################################################

## Generate dynamic reconfigure parameters in the 'cfg' folder
# generate_dynamic_reconfigure_options( cfg/DynReconf1.cfg cfg/DynReconf2.cfg ) 

###################################
## catkin specific configuration ##
###################################

catkin_package(
#  INCLUDE_DIRS include
#  LIBRARIES geo_devel
#  CATKIN_DEPENDS cmake_modules roscpp std_msgs tf2 visualization_msgs
#  DEPENDS system_lib
)

###########
## Build ##
###########

# set( Eigen3_INCLUDE_DIRS "/usr/include/eigen3" )

## Specify additional locations of header files
## Your package locations should be listed before other locations
# include_directories(include)
include_directories(
        # include/
        ${catkin_INCLUDE_DIRS}
        # ${LIBIGL_HEADER_PATH}
        # ${FCL_HEADER_PATH}
        # ${FCL_INCLUDE_DIRS}
        # ${Eigen_INCLUDE_DIRS} # 2017-05-NN
        # ${Eigen3_INCLUDE_DIRS} # 2017-07-29
)

# message( STATUS "Eigen_INCLUDE_DIRS:  ${Eigen_INCLUDE_DIRS}" )
# message( STATUS "Eigen3_INCLUDE_DIRS: ${Eigen3_INCLUDE_DIRS}" )

# ~~~ RViz Test ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

add_executable( basic_shapes basic_shapes.cpp )

target_link_libraries( basic_shapes ${catkin_LIBRARIES} )

# ~~~ STL Test ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

add_executable( STL_test STL_test.cpp )

target_link_libraries( STL_test
        ${catkin_LIBRARIES}
        # ${FCL_LIBRARIES}
)

# ~~~ Triangle Test ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

add_executable( tri_test tri_test.cpp )

target_link_libraries( tri_test
        ${catkin_LIBRARIES}
        # ${FCL_LIBRARIES}
)
