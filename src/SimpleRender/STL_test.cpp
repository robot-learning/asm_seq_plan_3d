
/// http://wiki.ros.org/rviz/DisplayTypes/Marker#The_Marker_Message

// ~~ Standard ~~
#include <stdio.h>

// ~~ ROS ~~
// ~ Core ~
#include <ros/ros.h> // ---------------------------- ROS presides over all
// ~ RViz ~
#include <visualization_msgs/Marker.h> // ---------- Display geometry
#include <visualization_msgs/MarkerArray.h> // ----- RViz marker array
// ~ TF2 Transforms ~
#include <tf2_ros/static_transform_broadcaster.h> // Static transforms , intended to be persistent
#include <geometry_msgs/TransformStamped.h> // ----- Messages to send to the broadcaster
#include <tf2/LinearMath/Quaternion.h> // ---------- for creating poses to pass the the broadcaster
#include <tf2_ros/transform_listener.h> // --------- To make the process of receiving transformations easier

int updateHz = 30;

int main( int argc , char** argv ){
	
	srand( ( time( 0 ) % 1 ) * 1000.0 + getpid() ); // Random seed based on the current clock time
	
	ros::init( argc , argv , "marker_move" );
	ros::Rate r( updateHz ); // 30Hz refresh rate
	
	// ros::Publisher vis_pub = node_handle.advertise<visualization_msgs::Marker>(       "visualization_marker"       , 0 );
	ros::NodeHandle node_handle;
	ros::Publisher vis_arr = node_handle.advertise<visualization_msgs::MarkerArray> ( "visualization_marker_array" , 100 );
	
	// https://answers.ros.org/question/263715/fixed-frame-map-does-not-exist/?answer=263742#post-id-263742	
	//~ tf2_ros::Buffer tfBuffer; // Buffer to hold queried transforms
	//~ tf2_ros::StaticTransformBroadcaster broadcaster();
    //~ tf2_ros::TransformListener* tfListener = new tf2_ros::TransformListener( tfBuffer );

    // === MARKER SPEC =====================================================================================================================
    
    // ~~~~ GUIDE ~~~~
    /**
    1. Instantiate individual markers
    2. Every marker must have its own 'id'
    3. M
    **/
    
    // ~~~~ AXES ~~~~
	float axsLen = 5.0;
	float axesPts[3][3] = { { axsLen , 0.0    , 0.0    } ,   
							{ 0.0    , axsLen , 0.0    } ,   
							{ 0.0    , 0.0    , axsLen } };
	
	float axesClrs[3][3] = { { 1.0 , 0.0 , 0.0 } ,   
							 { 0.0 , 1.0 , 0.0 } ,   
							 { 0.0 , 0.0 , 1.0 } };
	
	visualization_msgs::Marker axes;
	axes.header.frame_id = "world";
	axes.header.stamp = ros::Time();
	axes.ns = "my_namespace";
	axes.id = 0;
	axes.type = visualization_msgs::Marker::LINE_LIST;
	axes.action = visualization_msgs::Marker::ADD;
	axes.pose.position.x = 0.0;
	axes.pose.position.y = 0.0;
	axes.pose.position.z = 0.0;
	axes.pose.orientation.x = 0.0;
	axes.pose.orientation.y = 0.0;
	axes.pose.orientation.z = 0.0;
	axes.pose.orientation.w = 1.0;
	
	axes.scale.x = 0.01; // <-- Line Thickness
	
	axes.color.r = 1.0;
	axes.color.a = 1.0;
	for( int i = 0 ; i < 3 ; ++i ){
		// Set Points
		geometry_msgs::Point p1, p2;
		p1.x = 0;				p1.y = 0;				p1.z = 0;
		p2.x = axesPts[i][0];	p2.y = axesPts[i][1];	p2.z = axesPts[i][2];
		axes.points.push_back( p1 );
		axes.points.push_back( p2 );
		// Set Colors
		std_msgs::ColorRGBA c;
		
		c.r = axesClrs[i][0];	c.g = axesClrs[i][1];	c.b = axesClrs[i][2];
		c.a = 1.0;

		axes.colors.push_back( c );
		axes.colors.push_back( c );
	}
    
    float partScale = 1.0;
    
    // ~~~~ CEE ~~~~
    
    visualization_msgs::Marker   marker; // Instantiate a marker message
    
	marker.header.frame_id    = "world";
	// marker.header.stamp       = ros::Time();
	marker.header.stamp       = ros::Time::now();
	marker.ns                 = "my_namespace";
	marker.id                 = 1;
	// marker.type               = visualization_msgs::Marker::SPHERE;
	marker.type               = visualization_msgs::Marker::MESH_RESOURCE;
	marker.action             = visualization_msgs::Marker::ADD;
	marker.lifetime           = ros::Duration();
	
	marker.pose.position.x    = 0.0; // <-- Position
	marker.pose.position.y    = 0.0;
	marker.pose.position.z    = 0.0;
	
	marker.pose.orientation.w = 1.0;
	marker.pose.orientation.x = 0.0; // <-- Orientation
	marker.pose.orientation.y = 0.0;
	marker.pose.orientation.z = 0.0;
	
	marker.scale.x            = partScale; // <-- Scale
	marker.scale.y            = partScale;
	marker.scale.z            = partScale;
	
	marker.color.r            = 0.0; // <-- Color
	marker.color.g            = 1.0;
	marker.color.b            = 0.0;
	marker.color.a            = 1.0; // Don't forget to set the alpha!
	
	// geometry_msgs::TransformStamped poseAbso;
	
	if( true ){ // only if using a MESH_RESOURCE marker type:
		marker.mesh_resource = "package://simple_render/CEE_SCLD.stl";
		// marker.mesh_resource = "package://simple_render/CEE.stl";
		
	}
	
	
	
	// ~~~~ EL ~~~~
	
	visualization_msgs::Marker   marker2; // Instantiate a marker message
    
	marker2.header.frame_id    = "world";
	// marker.header.stamp       = ros::Time();
	marker2.header.stamp       = ros::Time::now();
	marker2.ns                 = "my_namespace";
	marker2.id                 = 2;
	// marker.type               = visualization_msgs::Marker::SPHERE;
	marker2.type               = visualization_msgs::Marker::MESH_RESOURCE;
	marker2.action             = visualization_msgs::Marker::ADD;
	marker2.lifetime           = ros::Duration();
	
	marker2.pose.position.x    = 0.2; // <-- Position
	marker2.pose.position.y    = 0.2;
	marker2.pose.position.z    = 0.2;
	
	marker2.pose.orientation.w =  0.0;
	marker2.pose.orientation.x =  0.0; // <-- Orientation
	marker2.pose.orientation.y =  0.707;
	marker2.pose.orientation.z = -0.707;
	
	marker2.scale.x            = partScale; // <-- Scale
	marker2.scale.y            = partScale;
	marker2.scale.z            = partScale;
	
	marker2.color.r            = 0.0; // <-- Color
	marker2.color.g            = 0.0;
	marker2.color.b            = 1.0;
	marker2.color.a            = 1.0; // Don't forget to set the alpha!
	
	if( true ){ // only if using a MESH_RESOURCE marker type:
		marker2.mesh_resource = "package://simple_render/EL_SCLD.stl";
		// marker2.mesh_resource = "package://simple_render/EL.stl";
	}
	
	// ~~~~ ARRAY ~~~~
	
    visualization_msgs::MarkerArray ma; 
    
    ma.markers.push_back( axes    );
    ma.markers.push_back( marker  );
    ma.markers.push_back( marker2 );
    
    // ___ END MARKER ______________________________________________________________________________________________________________________

	while ( ros::ok() ){ // while roscore is running
        // cout << ".";

		// Wait for a subscriber to start looking for this topic
		while ( vis_arr.getNumSubscribers() < 1 ){
			if ( !ros::ok() ){ return 0; }
			printf( "." );
			ROS_WARN_ONCE( "Please create a subscriber to the marker" );
			sleep( 1 );
		} // printf( "\n" );

        //~ vis_pub.publish( marker  ); // Send the marker to the visualization
        //~ vis_pub.publish( marker2 ); // Send the marker to the visualization
        
        vis_arr.publish( ma );

		r.sleep();
	}
	
	// delete tfListener;
	
	return 0;

}
