
/// http://wiki.ros.org/rviz/DisplayTypes/Marker#The_Marker_Message

// ~~ Standard ~~
#include <stdio.h>

// ~~ ROS : Transforms & RVIZ ~~
#include <ros/ros.h> // ---------------------------- ROS presides over all
#include <visualization_msgs/Marker.h> // ---------- Display geometry
#include <tf2_ros/static_transform_broadcaster.h> // Static transforms , intended to be persistent
#include <geometry_msgs/TransformStamped.h> // ----- Messages to send to the broadcaster
#include <tf2/LinearMath/Quaternion.h> // ---------- for creating poses to pass the the broadcaster
#include <tf2_ros/transform_listener.h> // --------- To make the process of receiving transformations easier

int updateHz = 30;

int32_t x_pos , y_pos , z_pos;

int main( int argc, char** argv ){
	
	srand( ( time( 0 ) % 1 ) * 1000.0 + getpid() ); // Random seed based on the current clock time
	
	ros::init( argc , argv , "marker_move" );
	ros::NodeHandle node_handle;
	ros::Rate r( updateHz ); // 30Hz refresh rate
	
	ros::Publisher vis_pub = node_handle.advertise<visualization_msgs::Marker>( "visualization_marker" , 0 );
	
	// https://answers.ros.org/question/263715/fixed-frame-map-does-not-exist/?answer=263742#post-id-263742	
	tf2_ros::Buffer tfBuffer; // Buffer to hold queried transforms
	tf2_ros::StaticTransformBroadcaster broadcaster();
    tf2_ros::TransformListener* tfListener = new tf2_ros::TransformListener( tfBuffer );

    // === MARKER SPEC =====================================================================================================================
    
	visualization_msgs::Marker marker;
	marker.header.frame_id = "world";
	marker.header.stamp = ros::Time();
	marker.ns = "marker_test_triangle_list";
	marker.id = 0;
	marker.type = visualization_msgs::Marker::TRIANGLE_LIST;
	marker.action = visualization_msgs::Marker::ADD;
	marker.pose.position.x = 0.0;
	marker.pose.position.y = 0.0;
	marker.pose.position.z = 0.0;
	marker.pose.orientation.x = 0.0;
	marker.pose.orientation.y = 0.0;
	marker.pose.orientation.z = 0.0;
	marker.pose.orientation.w = 1.0;
	marker.pose.position.x = x_pos;
	marker.scale.x = 1.0;
	marker.scale.y = 1.0;
	marker.scale.z = 1.0;
	
	marker.color.r = 0.0;
	marker.color.g = 1.0;
	marker.color.b = 0.0;
	marker.color.a = 1.0;
	
	/// THIS IS IMPORTANT FOR RENDERING MESHES IN RVIZ
	
	for( int x = 0 ; x < 10 ; ++x ){
		for( int y = 0 ; y < 10 ; ++y ){
			for( int z = 0 ; z < 10 ; ++z ){
				
				geometry_msgs::Point p;
				p.x = x * 0.1f;
				p.y = y * 0.1f;
				p.z = z * 0.1f;

				geometry_msgs::Point p2 = p;
				p2.x = p.x + 0.05;

				geometry_msgs::Point p3 = p;
				p3.x = p2.x;
				p3.z = p.z + 0.05;

				marker.points.push_back(p);
				marker.points.push_back(p2);
				marker.points.push_back(p3);

				std_msgs::ColorRGBA c;
				c.r = x * 0.1;
				c.g = y * 0.1;
				c.b = z * 0.1;
				c.a = 1.0;     // Don't forget to set the alpha!
				marker.colors.push_back(c); // <-- This is vertex shading
				marker.colors.push_back(c); //     One color for each vertex , to be blended within the triangle
				marker.colors.push_back(c);
			}
		}
	}    
	
	
    
    // ___ END MARKER ______________________________________________________________________________________________________________________

	while ( ros::ok() ){ // while roscore is running
        // cout << ".";

		if( false ){
			// Wait for a subscriber to start looking for this topic
			while ( vis_pub.getNumSubscribers() < 1 ){
				if ( !ros::ok() ){ return 0; }
				printf( "." );
				ROS_WARN_ONCE( "Please create a subscriber to the marker" );
				sleep( 1 );
			} // printf( "\n" );
		}

        vis_pub.publish( marker ); // Send the marker to the visualization

		r.sleep();
	}
	
	delete tfListener;
	
	return 0;

}
