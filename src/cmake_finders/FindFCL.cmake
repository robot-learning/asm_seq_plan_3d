# Copyright (c) 2015-2016, Humanoid Lab, Georgia Tech Research Corporation
# Copyright (c) 2015-2017, Graphics Lab, Georgia Tech Research Corporation
# Copyright (c) 2016-2017, Personal Robotics Lab, Carnegie Mellon University
# This file is provided under the "BSD-style" License

# Find FCL
#
# This sets the following variables:
# FCL_FOUND
# FCL_INCLUDE_DIRS
# FCL_LIBRARIES
# FCL_VERSION

message( STATUS " =============== FindFCL.cmake - RUN AND TROUBLESHOOT =============== " )

# find_package(PkgConfig QUIET) # 2017-07-28
find_package( PkgConfig ) # 2017-07-29 # What if I don't want to be quiet?

# Check to see if pkgconfig is installed.
# pkg_check_modules(PC_FCL fcl QUIET) # 2017-07-28
pkg_check_modules( PC_FCL fcl ) # 2017-07-29 # What if I don't want to be quiet?

set( INCLUDE_LOCATION "LOCAL" ) # Allowed Values: { "LOCAL" , "NONLOCAL" }

# !! DEBUG !!
message( STATUS "FindFCL.cmake sets the following variables:" )
message( STATUS "INCLUDE_LOCATION ----- ${INCLUDE_LOCATION}" )
message( STATUS "FCL_FOUND ------------ ${FCL_FOUND}" )
message( STATUS "FCL_INCLUDE_DIRS ----- ${FCL_INCLUDE_DIRS}" )
message( STATUS "FCL_LIBRARIES -------- ${FCL_LIBRARIES}" )
message( STATUS "FCL_VERSION ---------- ${FCL_VERSION}" )
message( STATUS "PC_FCL_INCLUDEDIR ---- ${PC_FCL_INCLUDEDIR}" )
message( STATUS "FCL_HEADER_PATH ------ ${FCL_HEADER_PATH}" )
message( STATUS "Other variables:" )
message( STATUS "CMAKE_INSTALL_PREFIX - ${CMAKE_INSTALL_PREFIX}" )
message( STATUS "MSVC ----------------- ${MSVC} < Likely never true" )
message( STATUS " " )

if( ${INCLUDE_LOCATION} MATCHES "NONLOCAL" )

    set( PC_FCL_INCLUDEDIR "/usr/include" )

    # find_path
    # https://cmake.org/cmake/help/v3.0/command/find_path.html
    # This command is used to find a directory containing the named file.
    # If the file in a directory is found the result is stored in the variable and the search will not be repeated
    # unless the variable is cleared.

    # Include directories
    find_path(FCL_INCLUDE_DIRS
            NAMES fcl/collision.h
            HINTS ${PC_FCL_INCLUDEDIR}
            # PATHS "${CMAKE_INSTALL_PREFIX}/include" # 2017-07-28 # This is not where the headers are!
            PATHS "/usr/include" # 2017-07-29
    )

    # Libraries
    if(MSVC)
        set(FCL_LIBRARIES optimized fcl debug fcld)
    else()
        find_library(FCL_LIBRARIES
                NAMES fcl
                HINTS ${PC_FCL_LIBDIR})
    endif()

elseif( ${INCLUDE_LOCATION} MATCHES "LOCAL" )

    set( PC_FCL_INCLUDEDIR /usr/local/include/fcl )
    set( FCL_HEADER_PATH   /usr/local/include/fcl )

    # find_path
    # https://cmake.org/cmake/help/v3.0/command/find_path.html
    # This command is used to find a directory containing the named file.
    # If the file in a directory is found the result is stored in the variable and the search will not be repeated
    # unless the variable is cleared.

    # Include directories
    find_path(FCL_INCLUDE_DIRS
            # NAMES fcl/collision.h # 2017-07-28
            NAMES fcl/fcl.h # 2017-07-29
            PATHS /usr/local/include # 2017-07-29
                  /usr/local/include/fcl
    )

    # Libraries
    if(MSVC)
        set(FCL_LIBRARIES optimized fcl debug fcld)
    else()
        find_library(FCL_LIBRARIES
                NAMES fcl
                HINTS ${PC_FCL_LIBDIR})
    endif()

else() # Passed a param other than allowed!
    message( STATUS "INCLUDE_LOCATION ${INCLUDE_LOCATION} is not recognized!" )
endif()



# Version
set(FCL_VERSION ${PC_FCL_VERSION})

# Set (NAME)_FOUND if all the variables and the version are satisfied.
include( FindPackageHandleStandardArgs )
find_package_handle_standard_args(FCL
    FAIL_MESSAGE  DEFAULT_MSG
    REQUIRED_VARS FCL_INCLUDE_DIRS FCL_LIBRARIES
    VERSION_VAR   FCL_VERSION
)

# !! DEBUG !!
message( STATUS " " )
message( STATUS "FindFCL.cmake should have set the following variables:" )
message( STATUS "INCLUDE_LOCATION ----- ${INCLUDE_LOCATION}" )
message( STATUS "FCL_FOUND ------------ ${FCL_FOUND}" )
message( STATUS "FCL_INCLUDE_DIRS ----- ${FCL_INCLUDE_DIRS}" )
message( STATUS "FCL_LIBRARIES -------- ${FCL_LIBRARIES}" )
message( STATUS "FCL_VERSION ---------- ${FCL_VERSION}" )
message( STATUS "PC_FCL_INCLUDEDIR ---- ${PC_FCL_INCLUDEDIR}" )
message( STATUS "FCL_HEADER_PATH ------ ${FCL_HEADER_PATH}" )
message( STATUS "Other variables:" )
message( STATUS "CMAKE_INSTALL_PREFIX - ${CMAKE_INSTALL_PREFIX}" )
message( STATUS "MSVC ----------------- ${MSVC} < Likely never true" )

message( STATUS " =============== End FindFCL.cmake =============== " )