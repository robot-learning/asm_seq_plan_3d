/***********
 * ASP_Geo.h
 * James Watson , 2017 May
 * 3D Geometry for Assembly Sequence Planning , Header File
 ***********/

/* ~~ LOG ~~
 * 2017-05-29: * Making all the pose params pointers so that the results of constructors can be passed directly to the constructor
 *             * Changing to tf2 , as it has a static (persistent) transform available
 * */

#ifndef ASPGEO_H // Use the 'ifndef' block to prevent the header from being copied into the source more than once
#define ASPGEO_H

// === Includes ============================================================================================================================

// ~~ Standard Libraries ~~
#include <math.h> // - cos
#include <stdio.h> //- printf
#include <iostream> // output for days
const float PI = 3.14159265; // should be in cmath M_PI

// ~~ Local Libraries ~~
#include "Cpp_Helpers.h"

// ~~ ROS : Transforms & RVIZ ~~
#include <ros/ros.h> // ---------------------------- ROS presides over all
#include <visualization_msgs/Marker.h> // ---------- Display geometry
#include <tf2_ros/static_transform_broadcaster.h> // Static transforms , intended to be persistent
#include <geometry_msgs/TransformStamped.h> // ----- Messages to send to the broadcaster
#include <tf2/LinearMath/Quaternion.h> // ---------- for creating poses to pass the the broadcaster
#include <tf2_ros/transform_listener.h> // --------- To make the process of receiving transformations easier


// ~~ LIBIGL : Polyhedral Geometry ~~
// #include <igl/cotmatrix.h> // Test of libigl

// ~~ Eigen : Maths! ~~
#include <Eigen/Core> // ------------------------ The living heart of Eigen
#include <Eigen/Geometry> // -------------------- Quaternions
typedef Eigen::Matrix< double , 3 , 1 > Vec3E; // Eigen aliases // This is row major
typedef Eigen::Quaternion<double> QuatE;

// ~~ FCL : Collision Detection ~~
// #include <fcl/BV/AABB.h> // 2017-07-28 , I guess BOX needs this now?
#include <fcl/math/bv/AABB.h> // 2017-07-29 , I guess BOX needs this now?
// #include <fcl/shape/geometric_shapes.h> // 2017-07-28
#include <fcl/geometry/collision_geometry.h>
// #include <fcl/shape/geometric_shapes_utility.h> // 2017-07-28
// #include <fcl/narrowphase/narrowphase.h> // 2017-07-28
// #include <fcl/collision.h> // 2017-07-28
#include <fcl/narrowphase/collision.h> // 2017-07-29
#include <fcl/narrowphase/detail/gjk_solver_libccd.h> // 2017-07-30
#include "fcl/narrowphase/detail/gjk_solver_indep.h" // 2017-08-17
#include "fcl/narrowphase/detail/traversal/collision_node.h" // 2017-08-17
#include <fcl/common/deprecated.h> // 2017-07-30 // TODO: Comment out if the above works

// ~~ Dubious Includes ~~
#include <fcl/narrowphase/detail/convexity_based_algorithm/gjk_libccd-inl.h>

using namespace fcl;
// https://github.com/flexible-collision-library/fcl/issues/133

// === End Include =========================================================================================================================

// == Utility Functions ==

Vec3E postn_from_transform( geometry_msgs::TransformStamped pose ); // Get the position from a stamped transform message
QuatE orntn_from_transform( geometry_msgs::TransformStamped pose ); // Get the orientation from a stamped transform message

// == End Utility ==

// ~~ Forward Declarations ~~
class ASP_Shape; // Let ASP_Shape know that there will be an ASP_World defined!

// == Utility Structs ==

struct ASP_Pair_Shape{ // Just holds two shape pointers
    ASP_Shape* one;
    ASP_Shape* two;
};

struct pose_cheap{ // Just holds the position / orientation // Meant as an intermediate between lib representations
    float pX;  float pY;  float pZ; // ---------- position
    float oW;  float oX;  float oY;  float oZ; // orientation
};

// == End Structs ==

// === class ASP_World ====================================================================================================================

// == ASP Ultilities ==

long int index_by_partID( vector<ASP_Shape*>& partVec , string partIDkey ); // Return the index of a part that matches 'partIDkey' or return -1 if DNE
long int index_by_ptr( vector<ASP_Shape*>& partVec , ASP_Shape* partAddr ); // Return the index of a part that matches 'partAddr' or return -1 if DNE

// == End Utilities ==

/* ~~ NOTES ~~
 * > For now , letting the world manage all of the absolute transforms , will move these to 'Shape as the need arises
 * */

// What does this class need?

/* > The ability to add and remove parts
 * > The ability to remove parts by name (ID)
 * */

class ASP_World{ // This is a class to manage drawing ASP objects
    friend ASP_Shape;
public:
    // ~ Constructors and Destructors ~
    ASP_World(); // Default Constructor
    ~ASP_World(); // Destructor

    // ~ Transforms ~
    void broadcast( geometry_msgs::TransformStamped& msg ); // Broadcast a transform for all shapes to see
    geometry_msgs::TransformStamped get_abso_pose( string targetPart ); // Get the absolute pose of a 'targetPart'
    void set_mrkr_abso(); // Tell all the parts to place their markers in their abolute positions

    // ~ Bookkeepping ~
    bool partID_exists( string partIDkey ); // Return true if 'partIDkey' exists in the problem , otherwise return false
    bool add_part( ASP_Shape* nuPart ); // Add a part to the problem , return success
    bool rem_part( ASP_Shape* rmPart ); // Remove a part from the problem , return success
    bool rem_partID( string rmPartID ); // Remove a part from the problem by its 'partID' , return success
    void print_all_partID(); // Just print all the partIDs for debugging purposes

    // ~~ Markers ~~
    void attach_mrkr_publisher( ros::Publisher& node_mrkr_pub ); // Attach an RVIZ publisher associated with a ROS node
    void publish_markers(); // Publish all the part markers in the problem
    void pub_mrkrs_anim( float frameRateHz ); // Publish all the part markers in the problem with a set expiration

    // ~~ Collisions ~~
    vector<ASP_Pair_Shape> get_all_collisions(); // Preform a collision check on all possible pairs of shapes
    void reset_collision_flags(); // Set the collision flag for all shapes to false
    void paint_collisions(); // Ask all of the parts to change colors based on their collision states

protected:
    string worldFrame; // The name of the world frame
    vector<ASP_Shape*> allShapes; // All the individual parts in this problem
    tf2_ros::StaticTransformBroadcaster broadcaster; // This can be used for persistent transforms, and you don't have
                                                     // to continually update them in order for them to be valid
                                                     // There should only be one per simulation
    tf2_ros::Buffer tfBuffer; // Buffer to hold queried transforms
    tf2_ros::TransformListener* tfListener; //( tfBuffer ); // Listener for transform queries
    ros::Publisher marker_pub; // Publisher for marker messages

    // ~~ Collision Setup ~~
    // ~ Query ~
    // GJKSolver_libccd solver; // 2017-07-NN

    // detail::GJKSolver_libccd<S> solver; // What is <S> ? // 2017-08-17a
    detail::GJKSolver_libccd<double> solver;  // 2017-08-17b

    // CollisionRequest request; // 2017-07-NN

    // ~ Result ~
    // CollisionResult result; // 2017-07-NN
    bool result; // 2017-08-17
    // Vec3f contact_points; // 2017-07-NN
    // vector<ContactPoint<S>>* contact_points; // 2017-08-17a
    vector<ContactPoint<double>>* contact_points; // 2017-08-17b
    // FCL_REAL penetration_depth; // 2017-07-NN
    // Vec3f normal; // 2017-07-NN

};

// === End ASP_World ======================================================================================================================



// === class ASP_Shape ====================================================================================================================

/* ~~ NOTES ~~
 * - The visual pose will be the absolute pose
 * - The tf2 pose will be the relative pose */

class ASP_Shape{
    friend ASP_World;
public:
	// ~~ Constructors ~~
	ASP_Shape(); // Default constructor // [ ] Re-write
	// ASP_Shape( string parentFrame , Vec3E* position , QuatE* orientation ); // Constructor with position and orientation // [ ] Re-write
//    ASP_Shape( string parentFrame , Vec3E* position , QuatE* orientation , // [ ] Re-write
//               float Xscale , float Yscale , float Zscale ); // position , orientation , scales
//	ASP_Shape( const ASP_Shape& other ); // Copy Constructor // [ ] Re-write
//	// ~~ Destructor ~~
//	~ASP_Shape();

	// ~~ Getters ~~

    unsigned long long get_num(); // Get the number assigned to this object at instantiation
    string get_ID(); // ------------ Get the (hopefully) unique string identifier for this object
    geometry_msgs::TransformStamped get_abso_pose(); // Get the absolute pose of a the part


    // ~ Collision Getters ~
    // Box& get_collsn_geo(); // Return a pointer to the collision geometry // 2017-07-NN
    Box<double>& get_collsn_geo(); // Return a pointer to the collision geometry // 2017-08-17
    // Transform3f& get_collsn_pose(); // Get the pose of the collision geometry // 2017-07-NN
    Transform3<double>& get_collsn_pose(); // Get the pose of the collision geometry

//	Box* get_collsn_geo(); // Return a pointer to the collision geometry
//	Transform3f& get_collsn_pose(); // Get the pose of the collision geometry
//    geometry_msgs::TransformStamped& get_abso_pose(); // Get the pose of the object in the world

    // ~~ Setters ~~

    // ~ Pose Setters ~
    void set_rela( float pX , float pY , float pZ , float oW , float oX , float oY , float oZ ); // Relative pose
    void set_rela_zero();
    void set_mrkr_abso(); // Set the marker to the absolute pose in the world

    // ~ Relationship Setters ~
    void set_world( ASP_World* wrdlManager ); // Assign a world to this object
    void unset_world(); // Remove the current world reference
    void set_parent( ASP_Shape* other );
    void add_child( ASP_Shape* other );  // Add a child shape whose reference frame will be relative to this shape
    void rmv_child( ASP_Shape* other ); // Remove child matching the address passed
    void rmv_child_partID( string rmvID ); // Remove child matching the 'partID'

    // ~ Marker Setters ~
	void set_color( float R , float G , float B , float alpha = 1.0 ); // Set the marker color RGBA , default alpah=1 opaque
    void rand_color(); // Set the marker to a random color
    void set_alpha( float alpha ); // Set the opacity for this shape
	void set_scale( float Xscale , float Yscale , float Zscale ); // Set the scale of the marker -- 1x1x1 here means 1m on a side
	void set_scale( float uniformScale ); // Set all the dimensions of the marker to 'uniformScale'
    void color_if_collide( float cR , float cG , float cB ); // Set the marker to a temporary RGB color if it is in collision

// What does this class need?

// > Name
// > ID #
// > Idenification of frames
//   - part
//   - parent
//   - world
// > A representation of the relative pose in the parent frame
// > Pointers to child objects , Tree will be the mechanism for relative transforms
// > A reference to a world manager object
// > A Way to tell the world manager object the relative poses of parts
// > Visualization Geometry
// > Collision Geometry

protected:
    // ~~ Member Variables ~~
    // ~ Bookkeeping ~
    string className = "ASP_Shape"; // ------------------------ Class label
    unsigned long long partNum; // ---------------------------- Unique Numerical identifier
    string partID; // ----------------------------------------- Unique String identifier
    // ~ Marker ~
	visualization_msgs::Marker visMarker; // ----------------- RViz marker
	uint32_t markerType = visualization_msgs::Marker::CUBE; // RViz marker type (CUBE default)
    float Rbase; float Gbase; float Bbase; // ---------------- Store color values to restore after changes
    // ~ FCL collision geometry ~
    // Box* collsnBox; // --------------------------------------- Collision geometry // 2017-07-NN
    Box<double>* collsnBox; // --------------------------------------- Collision geometry // 2017-08-17
    // Transform3f collsnTF; // --------------------------------- Pose of the collision geometry // 2017-07-NN
    Transform3<double> collsnTF; // --------------------------------- Pose of the collision geometry // 2017-08-17
    bool inCollsn = false; // -------------------------------- Flag for whether this particular shape is in collision
    // ~ Pose ~
    geometry_msgs::TransformStamped poseRela; // ------------- Reference frame for the shape , relative
    geometry_msgs::TransformStamped poseAbso; // ------------- Reference frame for the shape , absolute

    // ~ Reference Frames (tf2) ~
    // NOTE: The convention is to make the partFrame the same as the partID
    string partFrame; // ------------------------------------- Name of the frame that is this part's relative pose
    string parentFrame; // ----------------------------------- Name of the frame containing this part
    string worldFrame; // ------------------------------------ Name of the world frame that contains all parts

    vector<ASP_Shape*> childShapes; // Shapes that are attached to this shape

    ASP_World* worldPtr = nullptr; // ------------------------ Reference to the world that contains all parts
    // This needs to be a pointer so that it can be checked for null

    // ~~ Class Variables ~~    // REMEMBER TO INIT CLASS VARIABLES IN MAIN
    static unsigned long count; // --------------------------- Count of objects instantiated

    // ~~ Private Functions ~~
    void _broadcast_tf(); // Broadcast the the transform to the world manager object
    void _set_mrkr_pose( float pX , float pY , float pZ , float oW , float oX , float oY , float oZ ); // Set the marker to the absolute pose in the world ( Internal )
};

// === End ASP_Shape ======================================================================================================================





// == class ASP_asm ==

/*

class ASP_asm{ // Contains all the parts of an assembly
public:

protected:

};

*/

// == End ASP_asm ==

// === End Assembly Geometry ===

#endif
