/***********
 * ASP_Geo.h
 * James Watson , 2017 May
 * 3D Geometry for Assembly Sequence Planning , Source File
 ***********/

#include "ASP_Geo.h"
#include "Cpp_Helpers.h"

// == Utility Functions ==

Vec3E postn_from_transform( geometry_msgs::TransformStamped pose ){ // Get the position from a stamped transform message
    return Vec3E{ pose.transform.translation.x ,
                  pose.transform.translation.y ,
                  pose.transform.translation.z };
}

QuatE orntn_from_transform( geometry_msgs::TransformStamped pose ){ // Get the orientation from a stamped transform message
    return QuatE{ pose.transform.rotation.w ,
                  pose.transform.rotation.x ,
                  pose.transform.rotation.y ,
                  pose.transform.rotation.z };
}

// == End Utility ==

// === class ASP_World ====================================================================================================================

// == ASP Utilities ==

long int index_by_partID( vector<ASP_Shape*>& partVec , string partIDkey ){
    // Return the index of a part that matches 'partIDkey' or -1 if DNE
    for( int i = 0 ; i < partVec.size() ; i++ ){
        if( partVec[i]->get_ID() == partIDkey ){ return i; }
    }
    // If we made it through the loop without finding a part , then it does not exist in the problem , return false
    return -1;
}

long int index_by_ptr( vector<ASP_Shape*>& partVec , ASP_Shape* partAddr ){
    // Return the index of a part that matches 'partIDkey' or -1 if DNE
    for( int i = 0 ; i < partVec.size() ; i++ ){
        if( partVec[i] == partAddr ){ return i; }
    }
    // If we made it through the loop without finding a part , then it does not exist in the problem , return false
    return -1;
}

// == End Utilities ==

ASP_World::ASP_World(  ) { // Constructor with the marker publisher for RViz

    // ~ 1. Set up transforms ~
    worldFrame = "world";
    broadcaster = tf2_ros::StaticTransformBroadcaster();
    tfListener = new tf2_ros::TransformListener( tfBuffer ); //;
}

ASP_World::~ASP_World(){ // Destructor
    // 'allShapes' is a vector and will call 'delete' on all of its pointers?
    delete tfListener;
}

// == Transforms ==

void ASP_World::broadcast( geometry_msgs::TransformStamped& msg ){ // Broadcast a transform for all shapes to see
    broadcaster.sendTransform( msg );
}

geometry_msgs::TransformStamped ASP_World::get_abso_pose( string targetPart ){ // Get the absolute pose of a 'targetPart'
    try{ // Cache the pose if it is successful
        return tfBuffer.lookupTransform(
                worldFrame , // We want the transform to this frame (target frame) ...
                targetPart , // ... from this frame (source frame).
                ros::Time::now(), // The time at which we want to transform.
                                  // Providing ros::Time::now() will get us the transform for the current instant
                                  // Providing ros::Time(0) will just get us the latest available transform.
                ros::Duration( 3.0 ) // Wait for up to 3 seconds for the transform
        ); //   ^-- It's a good idea to wait for transforms if you want to be listening for them at the very beginning of the program
           //       and you don't care if the listener blocks execution for just a bit
        // return poseAbso;
    }
    catch( tf2::TransformException &ex ) {
        cout << "ASP_World::get_abso_pose : Could not retrieve the desired transform!" << endl;
        return geometry_msgs::TransformStamped();
    }
}

void ASP_World::set_mrkr_abso(){ // Tell all the parts to place their markers in their abolute positions
    for( int i = 0 ; i < allShapes.size() ; i++ ){ allShapes[i]->set_mrkr_abso(); } // For each shape , set the absolute position
}

// == End Transform ==


// == Bookkeepping ==

bool ASP_World::partID_exists( string partIDkey ){ // Return true if 'partIDkey' exists in the problem , otherwise return false
    for( int i = 0 ; i < allShapes.size() ; i++ ){
        if( allShapes[i]->partID == partIDkey ){ return true; }
    }
    // If we made it through the loop without finding a part , then it does not exist in the problem , return false
    return false;
}

bool ASP_World::add_part( ASP_Shape* nuPart ){ // Add a part to the problem , return success
    allShapes.push_back( nuPart ); // 1. Add the part to the world
    nuPart->set_world( this ); // 2. Assign world
    return true; // Not doing any error checking at this time
}

bool ASP_World::rem_part( ASP_Shape* rmPart ){ // Remove a part from the problem , return success
    rmPart->unset_world(); // 1. De-assign
    long int index = index_by_ptr( allShapes , rmPart ); // Fetch the index of the key
    if( index > -1 ){
        allShapes.erase( allShapes.begin() + index ); // NOTE: This is inefficient but leaving it here because not expecting to
        return true; //                                        erase parts after planning starts
    } else { return false; }
}

bool ASP_World::rem_partID( string rmPartID ){ // Remove a part from the problem by its 'partID' , return success
    long int index = index_by_partID( allShapes , rmPartID ); // Fetch the index of the key
    if( index > -1 ){
        allShapes[ index ]->unset_world();
        allShapes.erase( allShapes.begin() + index ); // NOTE: This is inefficient but leaving it here because not expecting to
        return true; //                                        erase parts after planning starts for one problem
    } else { return false; }
}

void ASP_World::print_all_partID(){ // Just print all the partIDs for debugging purposes
    for( size_t i = 0 ; i < allShapes.size() ; i++ ){ cout << allShapes[i]->partID << endl; }
}

// == End Bookkeepping ==

// == Markers ==

void ASP_World::attach_mrkr_publisher( ros::Publisher& node_mrkr_pub ){ // Attach an RVIZ publisher associated with a ROS node
    cout << "DEBUG , ASP_World::attach_mrkr_publisher : Attaching the marker publisher!" << endl;
    marker_pub = node_mrkr_pub;
}

void ASP_World::publish_markers(){ // Publish all the part markers in the problem
    for( size_t i = 0 ; i < allShapes.size() ; i++ ) {
        cout << "DEBUG , ASP_World::publish_markers : Attempting to publish a marker for " << allShapes[i]->partID << endl;
        marker_pub.publish( allShapes[i]->visMarker );
    }
}

void ASP_World::pub_mrkrs_anim( float frameRateHz ){ // Publish all the part markers in the problem with a set expiration
    float period = ( 1.0 / frameRateHz ) * 1.1; // Calc the period from the framerate
    for( size_t i = 0 ; i < allShapes.size() ; i++ ) {
        allShapes[i]->visMarker.lifetime = ros::Duration( period ); // Make sure that the marker has a set expiration
        marker_pub.publish( allShapes[i]->visMarker ); // Now publish marker , hoping it will disappear before the next frame
    }
}

// == End Markers ==

// == Collisions ==

void ASP_World::reset_collision_flags(){ // Set the collision flag for all shapes to false
    for( size_t i = 0 ; i < allShapes.size() ; i++ ){ allShapes[i]->inCollsn = false; }
}

vector<ASP_Pair_Shape> ASP_World::get_all_collisions(){ // Preform a collision check on all possible pairs of shapes
    // NOTE: This function assumes that all of the absolute poses have been calculated and cached!
    bool res; // per-pair collision result
    vector<ASP_Pair_Shape> rtnCollisions; // All of the the pair-wise collisions for this frame for this problem
    reset_collision_flags(); // Reset the shape collisions so that collisions do not persist across checks
    for( size_t i = 0 ; i < ( allShapes.size() - 1 ) ; i++ ){ // For each shape except the last one
        for( size_t j = i + 1 ; j < allShapes.size() ; j++ ){ // for each shape between i and the end
            // Perform a collision check between shape i and shape j
            // 1. Get the collision geometry
            // 2. Get the absolute poses
            // 3. Pass the geometry information to the solver and obtain a result
            res = solver.shapeIntersect(
                    allShapes[i]->get_collsn_geo()  ,
                    allShapes[i]->get_collsn_pose() ,
                    allShapes[j]->get_collsn_geo()  ,
                    allShapes[j]->get_collsn_pose() ,
                    // &contact_points , &penetration_depth , &normal // Not currently doing anything with these // 2017-07-NN
                    contact_points
            );
            // 4. If the solver found a collision , add the collision to the pair
            if( res ){
                rtnCollisions.push_back( ASP_Pair_Shape{ allShapes[i] , allShapes[j] } ); // Store the collision pair
                allShapes[i]->inCollsn = true;  allShapes[j]->inCollsn = true; // Let the shapes know they are in collision
            }
        }
    }
    // Return collisions , if there were no collisions this frame the vector will be empty
    return rtnCollisions;
}

void ASP_World::paint_collisions(){ // Ask all of the parts to change colors based on their collision states
    for( size_t i = 0 ; i < allShapes.size() ; i++ ){ allShapes[i]->color_if_collide( 1 , 0 , 0 ); }
}

// == End Collisions ==

// === End ASP_World ======================================================================================================================


// === class ASP_Shape ====================================================================================================================



ASP_Shape::ASP_Shape(){ // Default constructor

    // 0. Increment counter and assign a unique ID
    count++; partNum = count; partID = className + to_string( partNum );

    cout << " 1. Set the relative pose" << endl;
    partFrame   = partID; // The part frame should be the same as the partID
    parentFrame = "world"; // Default to "world
    worldFrame  = "world";  // Default to "world
    set_rela_zero(); // Default relative pose is the zero pose

    cout << " 2. Set the world connection to null" << endl;
    worldPtr = nullptr;

    cout << " 3. Instantiate the marker" << endl;
    visMarker = visualization_msgs::Marker();
    visMarker.header.frame_id = "map"; // Set the frame ID and timestamp.  See the TF tutorials for information on these.
    visMarker.ns = "ASP"; // Set the namespace and id for this marker.  This serves to create a unique ID
    visMarker.id = (int)partNum; // Any marker sent with the same namespace and id will overwrite the old one
    visMarker.type = markerType; // Set the marker type.
    visMarker.action = visualization_msgs::Marker::ADD; // Set the marker action.  Options are ADD, DELETE, and new in ROS Indigo:
                                                        // 3 (DELETEALL) , ADD will repaint the marker

    cout << " 4. Instantiate the collision box" << endl; // This needs to be done before scale is set!
    // Set up the collision geometry to match the marker geometry
    // collsnBox = new Box<float>( Vec3f( visMarker.scale.x , visMarker.scale.y , visMarker.scale.z ) ); // 2017-07-NN
    collsnBox = new Box<double>( Vector3<double>( visMarker.scale.x , visMarker.scale.y , visMarker.scale.z ) ); // 2017-08-17
    collsnTF.setIdentity();

    cout << " 5. Set up the marker // Default marker attributes are a 1x1x1 cube with a random color at the origin" << endl;
    cout << "DEBUG : Setting color!" << endl;
    set_color( 255.0/255.0 , 255.0/255.0 , 255.0/255.0 );
    cout << "DEBUG : Setting scale!" << endl;
    set_scale( 1 );


}

// ASP_Shape::~ASP_Shape(){  } // No dynamic objects

// ~~~~~~~~~~ Setters ~~~~~~~~~~

// == Pose Setters ==

void ASP_Shape::set_rela( float pX , float pY , float pZ , float oW , float oX , float oY , float oZ ){
    // Set the Relative Pose for this shape
    poseRela.header.stamp = ros::Time::now();
    // Relationship
    /* From */ poseRela.header.frame_id = parentFrame;
    cout << "ASP_Shape::set_rela , DEBUG: The parent frame is " << poseRela.header.frame_id << endl;
    /* To   */ poseRela.child_frame_id  = partFrame;
    cout << "ASP_Shape::set_rela , DEBUG: The part frame is   " << poseRela.child_frame_id << endl;
    // Position
    poseRela.transform.translation.x = pX;
    poseRela.transform.translation.y = pY;
    poseRela.transform.translation.z = pZ;
    // Orientation
    poseRela.transform.rotation.w = oW;
    poseRela.transform.rotation.x = oX;
    poseRela.transform.rotation.y = oY;
    poseRela.transform.rotation.z = oZ;
    // Set
    _broadcast_tf();
}

void ASP_Shape::set_rela_zero(){ set_rela( 0.0f , 0.0f , 0.0f , 1.0f , 0.0f , 0.0f , 0.0f ); }  // Set the relative pose to Zero

void ASP_Shape::set_mrkr_abso(){ // Set the marker to the absolute pose in the world
    // 1. Get and store the absolute pose for this shape
    poseAbso = get_abso_pose(); // Get the absolute pose of a the part // This caches the absolute pose
    // 2. Set the marker pose to the absolute pose
    _set_mrkr_pose( // Set the marker pose to the absolute pose obtained from the transform
        poseAbso.transform.translation.x , poseAbso.transform.translation.y , poseAbso.transform.translation.z ,
        poseAbso.transform.rotation.w , poseAbso.transform.rotation.x , poseAbso.transform.rotation.y , poseAbso.transform.rotation.z
    );
    // 3. Set the collision geometry to the absolute pose // TODO: Consider making this a separate function
    // collsnTF.setTranslation( Vec3f( poseAbso.transform.translation.x , // 2017-07-NN
    // collsnTF.setTranslation( Vector3<float>( poseAbso.transform.translation.x , // 2017-08-17a
    collsnTF.translation() = Vector3<double>( poseAbso.transform.translation.x , // 2017-08-17b
                                             poseAbso.transform.translation.y ,
                                             // poseAbso.transform.translation.z ) ); // 2017-07-NN
                                             poseAbso.transform.translation.z );
    // http://gamma.cs.unc.edu/FCL/fcl_docs/webpage/generated/transform_8h-source.html#l00123
    // collsnTF.setQuatRotation( Quaternion3f( poseAbso.transform.rotation.w ,    // W // 2017-07-NN
    // collsnTF.setQuatRotation( Quaternion<float>( poseAbso.transform.rotation.w ,    // W // 2017-08-17a
    collsnTF.linear() = Quaternion<double>( poseAbso.transform.rotation.w ,    // W // 2017-08-17b
                                                 poseAbso.transform.rotation.x ,    // X
                                                 poseAbso.transform.rotation.y ,    // Y
                                                 // poseAbso.transform.rotation.z ) ); // Z // 2017-07-NN
                                                 poseAbso.transform.rotation.z ).toRotationMatrix(); // Z // 2017-08-17
}

// == End Pose ==

// == Marker Setters ==

void ASP_Shape::_set_mrkr_pose( float pX , float pY , float pZ , float oW , float oX , float oY , float oZ ){
    // Set the marker to the absolute pose in the world ( Internal )
    visMarker.pose.position.x    = pX; // Set the pose of the marker.
    visMarker.pose.position.y    = pY; // This is a full 6DOF pose relative to the frame/time specified in the header
    visMarker.pose.position.z    = pZ;
    visMarker.pose.orientation.w = oW;
    visMarker.pose.orientation.x = oX;
    visMarker.pose.orientation.y = oY;
    visMarker.pose.orientation.z = oZ;
}

void ASP_Shape::set_color( float R , float G , float B , float alpha ){ // Set the marker color RGBA , default alpah=1 opaque
    visMarker.color.r = R;
    visMarker.color.g = G;
    visMarker.color.b = B;
    Rbase = R;
    Gbase = G;
    Bbase = B;
    visMarker.color.a = alpha;
}

void ASP_Shape::rand_color(){ // Set the marker to a random color
    set_color( rand_float() , rand_float() , rand_float() , 1.0f ); // Set the color -- be sure to set alpha to something non-zero!
}

void ASP_Shape::set_alpha( float alpha ){ // Set the opacity for this shape
    visMarker.color.a = alpha;
}

void ASP_Shape::color_if_collide( float cR , float cG , float cB ){ // Set the marker to a temporary RGB color if it is in collision
    if( inCollsn ){ // If the shape is in collision , set the marker to the specified collision color
        visMarker.color.r = cR;
        visMarker.color.g = cG;
        visMarker.color.b = cB;
    } else { // else the shape is not in collision , set the marker back to the base color
        visMarker.color.r = Rbase;
        visMarker.color.g = Gbase;
        visMarker.color.b = Bbase;
    }
}

void ASP_Shape::set_scale( float Xscale , float Yscale , float Zscale ){
    visMarker.scale.x = Xscale; // Set the scale of the marker -- 1x1x1 here means 1m on a side
    visMarker.scale.y = Yscale;
    visMarker.scale.z = Zscale;
    if( collsnBox ){ // Delete the present collision box , if it exists
        cout << "DEBUG , ASP_Shape::set_scale : Deleting the old 'collsnBox'" << endl;
        delete collsnBox;
    }
    cout << "DEBUG , ASP_Shape::set_scale : Create a new 'collsnBox'" << endl;
    //collsnBox = new Box( Vec3f( visMarker.scale.x , visMarker.scale.y , visMarker.scale.z ) ); // Create new collision geo // 2017-07-NN
    collsnBox = new Box<double>( Vector3<double>( visMarker.scale.x , visMarker.scale.y , visMarker.scale.z ) ); // Create new collision geo
}

void ASP_Shape::set_scale( float uniformScale ){ // Set all the dimensions of the marker to 'uniformScale'
    set_scale( uniformScale , uniformScale , uniformScale );
}

// == End Marker ==

// == Relationship Setters ==

void ASP_Shape::set_world( ASP_World* wrdlManager ){ // Assign a world to this object
    worldPtr = wrdlManager; // Set the pointer
    worldFrame = worldPtr->worldFrame;
}

void ASP_Shape::unset_world(){ // Remove the current world reference
    worldPtr = nullptr; // Set the pointer
    worldFrame = "world";
}

void ASP_Shape::set_parent( ASP_Shape* other ){ // This is the same operation as 'add_child' , but from the other direction
    other->childShapes.push_back( this ); // Add this shape to the list of child shapes
    parentFrame = other->partFrame; // Make sure that the child's frame is relative to this
    poseRela.header.frame_id = other->partFrame;
    _broadcast_tf();
} // NOTE: At this time it is the client's responsibility to add parts to the world

void ASP_Shape::add_child( ASP_Shape* other ){ // Add a child shape whose reference frame will be relative to this shape
    childShapes.push_back( other ); // Add the 'other' shape to the list of child shapes
    other->parentFrame = partFrame; // Make sure that the child's frame is relative to this
    other->poseRela.header.frame_id = partFrame;
    other->_broadcast_tf();
} // NOTE: At this time it is the client's responsibility to add parts to the world

void ASP_Shape::rmv_child( ASP_Shape* other ){ // Remove child matching the address passed
    long int index = index_by_ptr( childShapes , other ); // Fetch the index of the key
    if( index > -1 ){
        childShapes.erase( childShapes.begin() + index ); // NOTE: This is inefficient but leaving it here because not expecting to
    } //                                                           erase parts after planning starts
    other->parentFrame = worldFrame; // Make sure that the child's frame is relative to this
    other->poseRela.header.frame_id = worldFrame;
    other->_broadcast_tf();
}

void ASP_Shape::rmv_child_partID( string rmvID ){ // Remove child matching the 'partID'
    long int index = index_by_partID( childShapes , rmvID ); // Fetch the index of the key
    if( index > -1 ){
        childShapes[ index ]->parentFrame = worldFrame; // Make sure that the child's frame is relative to this
        childShapes[ index ]->poseRela.header.frame_id = worldFrame;
        childShapes.erase( childShapes.begin() + index ); // NOTE: This is inefficient but leaving it here because not expecting to
    } //                                                           erase parts after planning starts for one problem
}

// == End Relationship ==


// ~~~~~~~~~~ Getters ~~~~~~~~~~

// ~~ ID Getters ~~
unsigned long long ASP_Shape::get_num(){ return partNum; } // Get the number assigned to this object at instantiation
string ASP_Shape::get_ID(){ return partID; }; // Get the (hopefully) unique string identifier for this object

// ~~ Transform Getters ~~
void ASP_Shape::_broadcast_tf(){ // If there is a world pointer assigned , then broadcast the relative pose
    if( worldPtr ){ // the null pointer is implicitly converted into boolean false while non-null pointers are converted into true // // https://stackoverflow.com/a/17772117/893511
        cout << "ASP_Shape::_broadcast_tf , DEBUG: Broadcasting transform!" << endl;
        worldPtr->broadcast( poseRela );
    } else {// else there is no world pointer assigned , no action
        cout << "ASP_Shape::_broadcast_tf , DEBUG: There is no world object to manage transforms!" << endl;
    }
}

geometry_msgs::TransformStamped ASP_Shape::get_abso_pose(){ // Get the absolute pose of a the part
    if( worldPtr ){ // the null pointer is implicitly converted into boolean false while non-null pointers are converted into true // // https://stackoverflow.com/a/17772117/893511
        cout << "ASP_Shape::get_abso_pose , DEBUG: Getting an absolute pose for " << partID << endl;
        return worldPtr->get_abso_pose( partID );
    } else { // else there is no world pointer assigned , return a dummy transform
        cout << "ASP_Shape::get_abso_pose , DEBUG: There is no world object to manage transforms!" << endl;
        return geometry_msgs::TransformStamped{};
    }
}

// ~~ Collision Getters ~~
Box<double>& ASP_Shape::get_collsn_geo(){ return *collsnBox; } // Return a pointer to the collision geometry
Transform3<double>& ASP_Shape::get_collsn_pose(){ return collsnTF; } // Get the pose of the collision geometry

// === End ASP_Shape ======================================================================================================================