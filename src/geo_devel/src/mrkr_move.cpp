#include "ASP_Geo.h"
#include "Cpp_Helpers.h" // The IFNDEF should take care of repeat imports

// == Marker Definitions ==

int updateHz = 30;

float mrkr_x = 0.0;
float mrkr_y = 0.0;
float mrkr_r = 4.0;
unsigned long long mrkr_counter = 0;
float orbit_Hz = 0.25;
float spinAngle = 0.0;
float spin_Hz = 0.35;

// tf::Quaternion* cubeSpin = new tf::Quaternion(); // http://docs.ros.org/indigo/api/tf/html/c++/classtf_1_1Quaternion.html
Vec3E cubeSpin{ 1 , 1 , 1 };

/*
// This must be a reference in order to modify ------v                      v-- Must be passed as a pointer , DOCS DON'T SAY WHY
void marker_orbit_advance( visualization_msgs::Marker& mrkr , tf::Quaternion* turn , Transform3f& collTF ){ // Advance the marker 
	mrkr.pose.position.x = mrkr_r * cos( orbit_Hz * 2 * PI * (float)mrkr_counter / (float)updateHz );
	mrkr.pose.position.y = mrkr_r * sin( orbit_Hz * 2 * PI * (float)mrkr_counter / (float)updateHz );
	mrkr_counter++;
	cout << "X: " << mrkr.pose.position.x << "  , Y: " << mrkr.pose.position.y << endl;
	turn->setRotation( spinAxis , spin_Hz * 2 * PI * (float)mrkr_counter / (float)updateHz );
	mrkr.pose.orientation.x = turn->getX();
	mrkr.pose.orientation.y = turn->getY();
	mrkr.pose.orientation.z = turn->getZ();
	mrkr.pose.orientation.w = turn->getW();
	collTF.setIdentity();
	collTF.setTranslation( Vec3f( mrkr.pose.position.x , mrkr.pose.position.y , mrkr.pose.position.z ) );
	// http://gamma.cs.unc.edu/FCL/fcl_docs/webpage/generated/transform_8h-source.html#l00123
	collTF.setQuatRotation( Quaternion3f( mrkr.pose.orientation.w ,    // W 
		                                  mrkr.pose.orientation.x ,    // X
		                                  mrkr.pose.orientation.y ,    // Y
		                                  mrkr.pose.orientation.z ) ); // Z
}
*/

void marker_orbit_advance( ASP_Shape& mrkr , Vec3E& axis ){ // Advance the marker
    Vec3E pos{
            mrkr_r * cos( orbit_Hz * 2 * PI * (double)mrkr_counter / (double)updateHz ) ,
            mrkr_r * sin( orbit_Hz * 2 * PI * (double)mrkr_counter / (double)updateHz ) ,
            0
    };
    mrkr.set_position( pos );
    mrkr_counter++;

    axis.normalize(); // 'AngleAxis' expects the vector to be normalized
    //        v-- Assignment conversion does not work! , URL: https://forum.kde.org/viewtopic.php?f=74&t=95213
    QuatE rot = QuatE( Eigen::AngleAxisd(
            spin_Hz * 2 * PI * (double)mrkr_counter / (double)updateHz  ,
            axis
    ) );
    mrkr.set_orientation( rot );
}

// == End Marker ==

// == Collision Setup ==

GJKSolver_libccd solver;
CollisionRequest request;
CollisionResult result;

Vec3f contact_points;
FCL_REAL penetration_depth;
Vec3f normal;



// ~ Troubleshooting ~
Box* exampleBox1;
Box* exampleBox2;
Transform3f tf1; // Transform3f();
Transform3f tf2;

// Create some shapes
ASP_Shape orbitor{};
ASP_Shape stationary{};

//         x   y   z
Vec3E pos1{ 4 , 0 , 0 };
Vec3E pos2{ 0 , 4 , 0 };
//         w   x   y   z
QuatE orn{ 1 , 0 , 0 , 0 };

// == End Collision ==

int main( int argc, char** argv ){
	
	srand( ( time( 0 ) % 1 ) * 1000.0 + getpid() ); // Random seed based on the current clock time
	
	ros::init( argc , argv , "marker_move" );
	ros::NodeHandle n;
	ros::Rate r( updateHz ); // 30Hz refresh rate
	
	ros::Publisher marker_pub = n.advertise<visualization_msgs::Marker>( "visualization_marker" , 1 );

    stationary.set_pose( pos1 , orn );
    orbitor.set_pose( pos2 , orn );

	while ( ros::ok() ){ // while roscore is running
        // cout << ".";

		// Publish the marker
		while ( marker_pub.getNumSubscribers() < 1 ){
			if ( !ros::ok() ){ return 0; }
			ROS_WARN_ONCE( "Please create a subscriber to the marker" );
			sleep( 1 );
		}

        // Display the markers
        marker_pub.publish( orbitor.get_marker() );
        marker_pub.publish( stationary.get_marker() );

        // marker.lifetime = ros::Duration();
        marker_orbit_advance( orbitor , cubeSpin );

        cout << "Box 1 Geo: " << orbitor.get_collsn_geo()  << endl;
        cout << "Box 1 tf:  " << &orbitor.get_collsn_pose() << endl;
        cout << "Box 2 Geo: " << stationary.get_collsn_geo()  << endl;
        cout << "Box 2 tf:  " << &stationary.get_collsn_pose() << endl;

        // ~~ Troubleshoot Collision Detection ~~
        exampleBox1 = new Box( Vec3f( 1 , 1 , 1 ) ); // These are pointers
        exampleBox2 = new Box( Vec3f( 1 , 1 , 1 ) );
        tf1 = Transform3f(); // These are not pointers
        tf2 = Transform3f();
        tf1.setIdentity();
        tf2.setIdentity();

        bool res = solver.shapeIntersect(
                *orbitor.get_collsn_geo() , // *exampleBox1 , // orbitor.get_collsn_geo() ,
                orbitor.get_collsn_pose() ,
                *stationary.get_collsn_geo() , // *exampleBox2 , // stationary.get_collsn_geo() ,
                stationary.get_collsn_pose() ,
                &contact_points , &penetration_depth , &normal
        );


        cout << "contact points: " << contact_points << endl;
        cout << "pen depth: " << penetration_depth << endl;
        cout << "normal: " << normal << endl;
        cout << "result: " << res << endl;
        // contact points: (0.7 -0.107143 -0.142857)
        // pen depth: 0.1
        // normal: (-1 -0 -0)
        // result: 1



        if( penetration_depth > 0.001 ){
            orbitor.set_color( 1.0f , 0.0f , 0.0f , 0.75 );
        } else {
            orbitor.set_color( 0.0f , 1.0f , 0.0f , 0.75 );
        }

		r.sleep();
	}

}
