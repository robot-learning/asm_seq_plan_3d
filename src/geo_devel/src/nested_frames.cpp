/***********
nested_frames.cpp
James Watson , 2017 April
Testing Nested Frames with TF
***********/

#include "ASP_Geo.h"
#include "Cpp_Helpers.h" // The IFNDEF should take care of repeat imports
#include <tf/transform_broadcaster.h> // Broadcasters for nested frames // TODO: MOVE THIS TO "ASP_Geo.h"
                                                                        // This will break "mrkr_move.cpp"

// FIXME: Refactor Node as a class , Like you are supposed to per ROS conventions

// == Model Setup ==

// ~~ Components ~~
// Create some shapes
Vec3E basePos{ 0 , 0 , 0 };
QuatE baseQuat{ 1 , 0 , 0 , 0 };
Vec3E shldrPos{ 0 , 0 , 0.625f };
QuatE shldrQuat{ 1 , 0 , 0 , 0 };

ASP_Shape base{     basePos , baseQuat , 2.0f , 2.0f , 0.125f };
ASP_Shape shoulder{ shldrPos , shldrQuat , 1.0f , 1.0f , 1.0f };

// ~~ Model Tree ~~
tf::Transform shoulder_tf; // Create a new transform from the base to the shoulder
tf::TransformBroadcaster tfbr; // Create a transformer broadcaster to propagate nested frame coordinates

// == End Model ==

// == Animation Setup ==

unsigned long long loopCount = 0; // Counter for advancing the animation
int updateHz = 30; // Updates per second
int rotateHz = 0.5; // Rotations per second

//TODO: Need to establish a way to automatically set the relative poses of part and assembly frames

void rotate_shoulder( tf::Transform& transform ){ // Rotate the shoulder about the world Z axis , then set the shape pose
    tf::Quaternion rotQuat{
            tf::Vector3( 0 , 0 , 1.0f ) ,
            (float)rotateHz * 2 * PI * (float)loopCount / (float)updateHz
    };
    transform.setRotation( rotQuat );
    loopCount++;
}

// == End Animation ==


int main( int argc, char** argv ){

    // ~~ Node Init ~~
    ros::init( argc , argv , "nestef_frames" );
    ros::NodeHandle n;
    ros::Rate r( updateHz ); // 30Hz refresh rate
    ros::Publisher marker_pub = n.advertise<visualization_msgs::Marker>( "visualization_marker" , 1 );



    // ~~ Node Run ~~
    while ( ros::ok() ){ // while roscore is running

        // Wait for RViz to subscribe to the marker topic
        while ( marker_pub.getNumSubscribers() < 1 ){
            if ( !ros::ok() ){ return 0; }
            ROS_WARN_ONCE( "Please create a subscriber to the marker" );
            sleep( 1 );
        }



        // ~ Display the markers ~
        marker_pub.publish( base.get_marker() );
        marker_pub.publish( shoulder.get_marker() );

        r.sleep();
    }

    return 0;
}