/***********
test_01_mrkr.cpp
James Watson , 2017 April
Migrating ASP geometry to tf2
***********/

#include "ASP_Geo.h"
#include "Cpp_Helpers.h" // The IFNDEF should take care of repeat imports
// #include <tf/transform_broadcaster.h> // Broadcasters for nested frames // TODO: MOVE THIS TO "ASP_Geo.h"
// This will break "mrkr_move.cpp"
// #include <tf2_ros/static_transform_broadcaster.h> // Static transforms , intended to be persistent

// ~~ Node Vars ~~
unsigned long long loopCount = 0; // Counter for advancing the animation
int updateHz = 24; // Updates per second , in this case it is our target FPS

// ~~ Class Vars ~~
unsigned long ASP_Shape::count = 0;
// tf2_ros::StaticTransformBroadcaster ASP_World::broadcaster{}; // Only have to assign here if it is static

// == Animation Setup ==
Vec3E cubeSpin{ 1 , 1 , 1 };
unsigned long long mrkr_counter = 0;
float mrkr_r = 4.0;
float orbit_Hz = 0.25;
float spinAngle = 0.0;
float spin_Hz = 0.35;

void marker_orbit_advance( ASP_Shape& mrkr , Vec3E& axis , float radius ){ // Advance the marker
    Vec3E pos{
            radius * cos( orbit_Hz * 2 * PI * (double)mrkr_counter / (double)updateHz ) ,
            radius * sin( orbit_Hz * 2 * PI * (double)mrkr_counter / (double)updateHz ) ,
            0
    };
    //        v-- Assignment conversion does not work! , URL: https://forum.kde.org/viewtopic.php?f=74&t=95213
    QuatE rot = QuatE( Eigen::AngleAxisd(
            spin_Hz * 2 * PI * (double)mrkr_counter / (double)updateHz  ,
            axis
    ) );

    mrkr.set_rela( pos.x() , pos.y() , pos.z() ,
                   rot.w() , rot.x() , rot.y() , rot.z() );

    mrkr_counter++;
}

// == End Animation ==

// == Verification ==

Vec3E p1{}; // Hold the center of the 1st shape
Vec3E p2{}; // Hold the center of the 2nd shape
Vec3E p3{}; // Hold the center of the 3rd shape
// bool removed = false; // Flag for whether we have removed a shape from the sim

// == End Verify ==

// ==== Main Loop ========================================================================================================================

int main( int argc , char** argv ){

    // ~~ Node Init ~~
    ros::init( argc , argv , "test_01_mrkr" );
    ros::NodeHandle n;
    ros::Rate r( updateHz ); // 30Hz refresh rate
    ros::Publisher marker_pub = n.advertise<visualization_msgs::Marker>( "visualization_marker" , 1 );

    // ~~ Other Setup ~~
    cubeSpin.normalize(); // 'AngleAxis' expects the vector to be normalized

    // ~~ World Test ~~
    ASP_World theWorld{};
    theWorld.attach_mrkr_publisher( marker_pub );

    // ~~ Shape Test ~~
    ASP_Shape theShape{};
    cout << "The shape number is: " << theShape.get_num() << endl;
    cout << "The shape ID is:     " << theShape.get_ID() << endl;
    string thePartName = theShape.get_ID();
    theWorld.add_part( &theShape );
    theShape.set_rela( 1 , 1 , 1 , 1 , 0 , 0 , 0 );
    theShape.set_color( 255.0/255.0 , 183.0/255.0 , 76.0/255.0 );
    cout << "Pose of the created part:" << endl << theWorld.get_abso_pose( thePartName ) << endl;

    // ~~ Relative Frames Test ~~
    ASP_Shape chldPart{};
    theWorld.add_part( &chldPart );
    theShape.add_child( &chldPart );
    chldPart.set_rela( 0 , 0 , 1.25 , 1 , 0 , 0 , 0 );
    chldPart.set_color( 102.0/255.0 , 198.0/255.0 , 255.0/255.0 );

    ASP_Shape orbtPart{};
    theWorld.add_part( &orbtPart );
    chldPart.add_child( &orbtPart );
    orbtPart.set_rela( 0 , 0 , 1 , 1 , 0 , 0 , 0 ); // Not really needed
    orbtPart.set_color( 124.0/255.0 , 255.0/255.0 , 122.0/255.0 ); // 124, 255, 122

    // ~~ Collision Test ~~
    ASP_Shape towrPart{};
    theWorld.add_part( &towrPart );
    // chldPart.add_child( &orbtPart );
    towrPart.set_rela( 0 , -4 , 2 , 1 , 0 , 0 , 0 );
    towrPart.set_color( 199.0/255.0 , 165.0/255.0 , 212.0/255.0 ); // 124, 255, 122
    towrPart.set_scale( 1 , 1 , 4 );


    // ~ Add / Remove Test ~
    if( false ) {
        for (int i = 0; i < 10; i++) { // Add a number of anonymous shapes
            theWorld.add_part(new ASP_Shape());
        }

        // ASP_Shape9

        theWorld.rem_part(&theShape);
        theWorld.rem_partID("ASP_Shape9");
    }


    theWorld.print_all_partID();

    // ~~ Node Run ~~
    while ( ros::ok() ){ // while roscore is running

        // ~ Animation ~
        marker_orbit_advance( theShape , cubeSpin , 4.0 );
        marker_orbit_advance( orbtPart , cubeSpin , 1.5 );
        cout << "DEBUG: About to set marker positions!" << endl;
        // Compute absolute positions and publish the markers
        theWorld.set_mrkr_abso(); // Set all the markers to their absolute positions
        theWorld.publish_markers(); // Send all of the marker messages
        // theWorld.pub_mrkrs_anim( updateHz ); // Send all of the marker messages with expiration
        // Setting an expiration on markers did not cause the middle object to render with regularity and furthermore caused
        // flicker for the objects that *were* rendering consistently

        cout << "DEBUG: About to fetch positions!" << endl;
        // Verify that the transforms are working as intended
        p1 = postn_from_transform( theShape.get_abso_pose() );
        p2 = postn_from_transform( chldPart.get_abso_pose() );
        p3 = postn_from_transform( orbtPart.get_abso_pose() );

        cout << "Dist p1 -> p2: " << (p1 - p2).norm() << endl;
        cout << "Dist p2 -> p3: " << (p2 - p3).norm() << endl;
        cout << "Dist p1 -> p3: " << (p1 - p3).norm() << endl; // These are staying the same , it's RViz that's dumb!

        if( false ) {
            // After 10 seconds , remove the innermost frame (orbit shape)
            if (loopCount / updateHz > 10) {
                cout << "DEBUG , removing child part!" << endl;
                //chldPart.rmv_child( &orbtPart );
                chldPart.rmv_child_partID(orbtPart.get_ID());
            }
        }

        // Handle Collisions
        cout << "DEBUG : About to fetch collisions!" << endl;
        theWorld.get_all_collisions(); // Set all of the collision flags
        theWorld.paint_collisions();
        cout << "DEBUG : Got collisions!" << endl;

        // Wrap-up
        loopCount++;
        cout << "Loop " << loopCount << endl;
        r.sleep();

        // break; // Uncomment to run only one loop
    }

    return 0; // I guess everything turned out alright at the end!
}

// ==== End Main =========================================================================================================================