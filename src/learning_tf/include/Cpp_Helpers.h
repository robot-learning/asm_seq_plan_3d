#pragma once

/***********  
Cpp_Helpers.h
James Watson , 2017 March
Functions of general use for C++ programming
***********/

#ifndef CPPHELPERS_H
#define CPPHELPERS_H

#include <string> // string manipulation
#include <vector> // standard vector datatype
#include <iostream> // - standard input and output, istream
#include <limits> // number limits of data types, limit on 'cin.ignore'
#include <string> // --- string manipulation
#include <cassert> // -- input/condition verification
#include <vector> // --- Standard vector , the friendly array
#include <sys/stat.h> // File status
#include <fstream> // -- Reading files
#include <sstream> // -- Text streams
#include <stdlib.h> // - srand, rand 
#include <time.h> // --- time , for getting sys time and seeding random numbers
#include <map> // ------ dictionaries , for the object registry
//#define NDEBUG // uncomment to disable assert()
#include <cassert> // input/condition verification
#include <cmath> // abs, min/max, trig, hyperbolic, power, exp, error, rounding

// == Shortcuts and Aliases ==
using std::cout; // --------------- output to terminal
using std::endl; // --------------- newline
using std::cin; // ---------------- input from terminal
using std::string; 
using std::to_string; // ---------- string conversion // Needs C++11
using std::vector; // ------------- Vectors , naturally
using usll = unsigned long long; // big ints          // Needs C++11
// == End Shortcuts ==

/* These are very short functions, but putting them in a separate CPP anyway because .H files are text substitution and including in 
several places (likely) will define the functions multiple times, which will raise errors. */

// == Debug Tools ==
void assert_report(bool assertion, string report); // Reporting wrapper for 'assert'
void sep_dbg(); // Print a separator for debug information
void sep( string title = "" , size_t width = 6 , char dingbat = '=' ); // Print a separating title card for debug 
void newline();// print a new line
// == End Debug ==

// == Math Tools ==
float rand_float(); // Note that this is not exactly uniformly distributed
int randrange( int end );
int randrange( int bgn , int end );
// == End Math ==

// == File Tools ==
bool file_exists( const string& fName );  // Return true if the file exists , otherwise return false
std::vector<string> readlines( string path ); // Return all the lines of text file as a string vector
// == End File ==

// == String Tools ==
void remove_all( string& rawStr , char keyChar ); // Destructively remove all instances of 'keyChar' from 'rawStr'
// Destructively remove all newlines from 'rawStr'
void strip_newlines( string& rawStr );
string strip_after_dot( string fName ); // Return a copy of 'fName' with the first period and all following characters removed
// == End String ==

// == Container Tools ==

template<typename T> // NOTE: ONLY COMPILES IF THE ENTIRE DEFINITION IS IN HEADER , CANNOT SEPARATE .H/.CPP FOR UNKNOWN REASON
std::ostream& operator<<(std::ostream& os, const std::vector<T>& vec) { // ostream '<<' operator for vectors
	// NOTE: This function assumes that the ostream '<<' operator for T has already been defined
	os << "[ ";
	for (size_t i = 0; i < vec.size(); i++) {
		os << vec[i];
		if (i + 1 < vec.size()) { os << ", "; }
	}
	os << " ]";
	return os; // You must return a reference to the stream!
}

// == End Container ==

#endif

/* == Useful Parts =========================================================================================================================

// ~~ cout << operator ~~
std::ostream& operator<<(std::ostream& os, const Gene& vec) {
	std::vector<float> codons = vec.copy_codons();
	os << "[ ";
	for (size_t i = 0; i < codons.size(); i++) {
		os << codons[i];
		if (i + 1 < codons.size()) { os << ", "; }
	}
	os << " ]";
	return os; // You must return a reference to the stream!
}

// ~~ Function Object ~~
struct myclass {
  bool operator() (int i,int j) { return (i<j);}
} myobject; 

  == End Parts ==  */