/*
 * set_once_tf.cpp
 * Set the transforms once and see what happens when you query them , does it just hold the last relationship?
 * */

#include <cstdio>
#include "Cpp_Helpers.h"

#include <ros/ros.h>
// #include <tf/transform_datatypes.h>
// #include <tf/transform_broadcaster.h>
// #include <tf/transform_listener.h> // Intended to make receiving transform messages easier
#include <tf2_ros/static_transform_broadcaster.h> // Static transforms , intended to be persistent
#include <geometry_msgs/TransformStamped.h> // Messages to send to the broadcaster
#include <tf2/LinearMath/Quaternion.h> // for creating poses to pass the the broadcaster
#include <tf2_ros/transform_listener.h> // To make the process of receiving transformations easier



void fetch_transform( tf2_ros::Buffer& tfBuffer , string parent , string child , geometry_msgs::TransformStamped& transformStamped ){
    // Get the transformation from 'parent' to 'child' and load it into 'transformStamped'
    try{
        transformStamped = tfBuffer.lookupTransform(
                parent , // We want the transform to this frame (target frame) ...
                child , // ... from this frame (source frame).
                ros::Time(0) // The time at which we want to transform.
                // Providing ros::Time(0) will just get us the latest available transform.
        );      // Duration before timeout. ( optional , default=ros::Duration(0.0) )
    }
    catch( tf2::TransformException &ex ) {
        ROS_WARN( "%s" , ex.what() );
        ros::Duration( 1.0 ).sleep();
    }

}

int main( int argc , char** argv ){

    ros::init( argc , argv , "my_tf_broadcaster" );
    ros::NodeHandle node;

    // ~~ Broadcaster ~~
    // tf::TransformBroadcaster br;
    static tf2_ros::StaticTransformBroadcaster static_broadcaster; /* This can be used for persistent transforms, and you don't have
                                                                    * to continually update them in order for them to be valid */

    // ~~ Transforms ~~
    // tf::Transform trans1; // Create a new transform
    // tf::Transform trans2;
    // tf::Transform trans3;

    // ~ Transform 1 , one-->two ~
    geometry_msgs::TransformStamped trans1;
    trans1.header.stamp = ros::Time::now();
    trans1.header.frame_id = "one"; // Relationship
    trans1.child_frame_id = "two";
    trans1.transform.translation.x = 1.0f; // Position
    trans1.transform.translation.y = 0.0f;
    trans1.transform.translation.z = 0.0f;
    tf2::Quaternion quat;
    quat.setRPY( 0.0f , 0.0f , 0.0f );
    trans1.transform.rotation.w = quat.w(); // Orientation
    trans1.transform.rotation.x = quat.x();
    trans1.transform.rotation.y = quat.y();
    trans1.transform.rotation.z = quat.z();
    static_broadcaster.sendTransform( trans1 );

    // ~ Transform 2 , two-->three ~
    geometry_msgs::TransformStamped trans2;
    trans2.header.stamp = ros::Time::now();
    trans2.header.frame_id = "two"; // Relationship
    trans2.child_frame_id = "three";
    trans2.transform.translation.x = 0.0f; // Position
    trans2.transform.translation.y = 1.0f;
    trans2.transform.translation.z = 0.0f;
    quat.setRPY( 0.0f , 0.0f , 0.0f );
    trans2.transform.rotation.w = quat.w(); // Orientation
    trans2.transform.rotation.x = quat.x();
    trans2.transform.rotation.y = quat.y();
    trans2.transform.rotation.z = quat.z();
    static_broadcaster.sendTransform( trans2 );

    // ~ Transform 3 , three-->four ~
    geometry_msgs::TransformStamped trans3;
    trans3.header.stamp = ros::Time::now();
    trans3.header.frame_id = "three"; // Relationship
    trans3.child_frame_id = "four";
    trans3.transform.translation.x = 0.0f; // Position
    trans3.transform.translation.y = 0.0f;
    trans3.transform.translation.z = 1.0f;
    quat.setRPY( 0.0f , 0.0f , 0.0f );
    trans3.transform.rotation.w = quat.w(); // Orientation
    trans3.transform.rotation.x = quat.x();
    trans3.transform.rotation.y = quat.y();
    trans3.transform.rotation.z = quat.z();
    static_broadcaster.sendTransform( trans3 );

    // tf::TransformListener listener; // Transforms picked up by this listener will expire!

    /*
    // Estasbish three relative relationships between frames
    trans1.setOrigin( tf::Vector3( 1.0f , 0.0f , 0.0f ) );
    trans2.setOrigin( tf::Vector3( 0.0f , 1.0f , 0.0f ) );
    trans3.setOrigin( tf::Vector3( 0.0f , 0.0f , 1.0f ) );

    ros::Rate rate( 10.0 );

    // Set up all the relationships between the nested frames
    br.sendTransform( tf::StampedTransform( trans1 ,        // Setting up the transform is all that is needed to create the
                                            ros::Time::now() , // relationship between frames. You do not have to instantiate the
                                            "one" ,            // frames individually, just the relationships. ROS/TF will
                                            "two" ) );         // manage the tree of transforms. You cannot access the frames
                                                               // themselves, only their relationships.

    br.sendTransform( tf::StampedTransform( trans2 , ros::Time::now() , "two" , "three" ) );
    br.sendTransform( tf::StampedTransform( trans3 , ros::Time::now() , "three" , "four" ) );
    */

    tf2_ros::Buffer tfBuffer;
    tf2_ros::TransformListener tfListener(tfBuffer); /*  Once the listener is created, it starts receiving
    * tf2 transformations over the wire, and buffers them for up to 10 seconds. The TransformListener object should be scoped to persist,
    * otherwise it's cache will be unable to fill and almost every query will fail. A common method is to make the
    * TransformListener object a member variable of a class. */

    ros::Rate rate( 10.0 ); // 10Hz

    unsigned long long loopCount = 0;

    while ( node.ok() ){

        loopCount++;

        /*
        tf::StampedTransform transform; // Set up a transform to hold the relationships between frames

        try{
            listener.lookupTransform( "one" , // from this frame
                                      "two" , // to this frame // "/turtle1"
                                      ros::Time( 0 ) , // Get the latest available transform
                                      transform ); // response object that will store the resulting transform
        }
        catch ( tf::TransformException &ex ) {
            ROS_ERROR( "%s" , ex.what() );
            ros::Duration( 1.0 ).sleep();
            continue;
        }
        cout << "Transform 1: " << transform.getOrigin() << endl;
        */

        geometry_msgs::TransformStamped transformStamped;

        cout << endl << "Loop: " << loopCount << endl;

        fetch_transform( tfBuffer , "one" , "two" , transformStamped );
        cout << "Transform 1: " << transformStamped.transform.translation << " " <<
                                   transformStamped.transform.rotation    << endl;

        fetch_transform( tfBuffer , "two" , "three" , transformStamped );
        cout << "Transform 2: " << transformStamped.transform.translation << " " <<
                                   transformStamped.transform.rotation    << endl;

        fetch_transform( tfBuffer , "three" , "four" , transformStamped );
        cout << "Transform 3: " << transformStamped.transform.translation << " " <<
             transformStamped.transform.rotation    << endl;

        fetch_transform( tfBuffer , "one" , "four" , transformStamped );
        cout << "one-->four: " << transformStamped.transform.translation << " " <<
             transformStamped.transform.rotation    << endl;

        rate.sleep();

    }
    return 0;
};

/* ~~ Looking at Transforms ~~
 * $>> rosrun tf view_frames        // Generate a PDF of the frame tree
 * $>> evince frames.pdf            // Look at the pdf you creates
 * $>> rosrun tf tf_echo one two    // To monitor the transform from 'one' to 'two' in the terminal
 * */