/*
 * simplest_tf.cpp
 * The minimum necessary to represent the relative poses between frames
 * */

#include <ros/ros.h>
#include <tf/transform_datatypes.h>
#include <tf/transform_broadcaster.h>

int main( int argc , char** argv ){
    ros::init( argc , argv , "my_tf_broadcaster" );
    ros::NodeHandle node;

    tf::TransformBroadcaster br;
    tf::Transform transform; // Create a new transform

    ros::Rate rate( 10.0 );
    while ( node.ok() ){
        // Set a relative pose for the transform

        // static
        // transform.setOrigin( tf::Vector3( 0.0 , 2.0 , 0.0 ) );
        // transform.setRotation( tf::Quaternion( 0 , 0 , 0 , 1 ) );

        // dynamic , move in a circle around the user turtle
        transform.setOrigin( tf::Vector3( 2.0 * sin( ros::Time::now().toSec() ) , 2.0 * cos( ros::Time::now().toSec() ) , 0.0 ) );
        transform.setRotation( tf::Quaternion( 0 , 0 , 0 , 1 ) );

        /* Here we create a new transform, from the parent turtle1 to the new child carrot1.
         * The carrot1 frame is 2 meters offset to the left from the turtle1 frame.  */
        br.sendTransform( tf::StampedTransform( transform ,        // Setting up the transform is all that is needed to create the
                                                ros::Time::now() , // relationship between frames. You do not have to instantiate the
                                                "one" ,            // frames individually, just the relationships. ROS/TF will
                                                "two" ) );         // manage the tree of transforms. You cannot access the frames
        rate.sleep();                                              // themselves, only their relationships.
    }
    return 0;
};

/* ~~ Looking at Transforms ~~
 * $>> rosrun tf view_frames        // Generate a PDF of the frame tree
 * $>> evince frames.pdf            // Look at the pdf you creates
 * $>> rosrun tf tf_echo one two    // To monitor the transform from 'one' to 'two' in the terminal
 * */