#include <ros/ros.h> // ROS headers
#include <tf/transform_broadcaster.h> // TF transformers and publishers
#include <turtlesim/Pose.h> // ROS turtle environment

std::string turtle_name;

void poseCallback( const turtlesim::PoseConstPtr& msg ){
    static tf::TransformBroadcaster br; // Used to send transformations
    tf::Transform transform; // Create a tf transform
    // Load the 3D transform with 2D pose data from the turtle
    transform.setOrigin( tf::Vector3( msg->x , msg->y , 0.0 ) );
    tf::Quaternion q;
    q.setRPY( 0 , 0 , msg->theta );
    transform.setRotation( q );
    br.sendTransform( // Create and send the transform , the transform has 4 arguments
            tf::StampedTransform( transform , // the transform itself
                                  ros::Time::now() , // a timestamp
                                  "world" , // the parent frame of the link we are creating
                                  turtle_name ) // the child frame of the link we are creating
    );
}
/*
 *     ^                           ^
 *     |                           |
 *     +-->  ~~~~ transform ~~~~>  +-->
 *     parent    (broadcaster)      child
 *
 * */


int main( int argc , char** argv ){
    ros::init( argc , argv , "my_tf_broadcaster" );
    if ( argc != 2 ){ ROS_ERROR( "need turtle name as argument" ); return -1; };
    turtle_name = argv[1];

    ros::NodeHandle node;
    ros::Subscriber sub = node.subscribe( turtle_name + "/pose" , 10 , &poseCallback );

    ros::spin();
    return 0;
};

