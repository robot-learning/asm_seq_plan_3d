#include <ros/ros.h> // ROS , naturally
#include <tf/transform_listener.h> // Intended to make receiving transform messages easier
#include <geometry_msgs/Twist.h> // Geometry , needed to receive transforms
#include <turtlesim/Spawn.h> // ROS turtle environment

int main( int argc , char** argv ){
    ros::init( argc , argv , "my_tf_listener" );

    ros::NodeHandle node;

    ros::service::waitForService( "spawn" );
    ros::ServiceClient add_turtle = node.serviceClient<turtlesim::Spawn>( "spawn" );
    turtlesim::Spawn srv;
    add_turtle.call(srv);

    ros::Publisher turtle_vel = node.advertise<geometry_msgs::Twist>( "turtle2/cmd_vel" , 10 );

    tf::TransformListener listener; // Once this is created , it starts receiving transformations , and stores them for up to 10s
    /* The TransformListener object should be scoped to persist otherwise it's cache will be unable to fill and
     * almost every query will fail. A common method is to make the TransformListener object a member variable of a class.  */

    ros::Rate rate( 10.0 );
    while ( node.ok() ){
        tf::StampedTransform transform;
        try{
            listener.lookupTransform( "/turtle2" , // from this frame
                                      "/carrot1" , // to this frame // "/turtle1"
                                      ros::Time( 0 ) , // Get the latest available transform
                                      transform ); // response object that will store the resulting transform
        }
        catch ( tf::TransformException &ex ) {
            ROS_ERROR( "%s" , ex.what() );
            ros::Duration( 1.0 ).sleep();
            continue;
        }

        geometry_msgs::Twist vel_msg; // Create an object to hold the CPU turtle velocity
        // Calc the velocity for the CPU turtle
        vel_msg.angular.z = 4.0 * atan2( transform.getOrigin().y() ,
                                         transform.getOrigin().x() );
        vel_msg.linear.x = 0.5 * sqrt( pow( transform.getOrigin().x() , 2 ) +
                                       pow( transform.getOrigin().y() , 2 ) );
        turtle_vel.publish( vel_msg );

        rate.sleep();
    }
    return 0;
};