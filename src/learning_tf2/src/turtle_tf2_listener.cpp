#include <ros/ros.h>
#include <tf2_ros/transform_listener.h> // To make the process of receiving transformations easier
#include <geometry_msgs/TransformStamped.h>
#include <geometry_msgs/Twist.h>
#include <turtlesim/Spawn.h>

int main( int argc , char** argv ){
    ros::init( argc , argv , "my_tf2_listener" );

    ros::NodeHandle node;

    ros::service::waitForService( "spawn" );
    ros::ServiceClient spawner =
            node.serviceClient<turtlesim::Spawn>( "spawn" );
    turtlesim::Spawn turtle;
    turtle.request.x = 4;
    turtle.request.y = 2;
    turtle.request.theta = 0;
    turtle.request.name = "turtle2";
    spawner.call(turtle);

    ros::Publisher turtle_vel = node.advertise<geometry_msgs::Twist>( "turtle2/cmd_vel" , 10 );

    tf2_ros::Buffer tfBuffer;
    tf2_ros::TransformListener tfListener(tfBuffer); /*  Once the listener is created, it starts receiving
    * tf2 transformations over the wire, and buffers them for up to 10 seconds. The TransformListener object should be scoped to persist,
    * otherwise it's cache will be unable to fill and almost every query will fail. A common method is to make the
    * TransformListener object a member variable of a class. */

    ros::Rate rate( 10.0 ); // 10Hz

    while( node.ok() ){ // While ROS is running

        // URL , Waiting for a transform that doesn't exist yet : http://permalink.gmane.org/gmane.science.robotics.ros.user/6526
        geometry_msgs::TransformStamped transformStamped;
        try{
            transformStamped = tfBuffer.lookupTransform(
                    "turtle2" , // We want the transform to this frame (target frame) ...
                    "turtle1" , // ... from this frame (source frame).
                    ros::Time::now(), // The time at which we want to transform.
                                     // Providing ros::Time::now() will get us the transform for the current instant
                                     // Providing ros::Time(0) will just get us the latest available transform.
                    ros::Duration( 3.0 ) // Wait for up to 3 seconds for the transform
            );      // Duration before timeout. ( optional , default=ros::Duration(0.0) )
        }
        catch( tf2::TransformException &ex ) {
            ROS_WARN( "%s" , ex.what() );
            ros::Duration( 1.0 ).sleep();
            continue;
        }

        geometry_msgs::Twist vel_msg;

        vel_msg.angular.z = 4.0 * atan2( transformStamped.transform.translation.y ,
                                         transformStamped.transform.translation.x);
        vel_msg.linear.x = 0.5 * sqrt( pow( transformStamped.transform.translation.x , 2 ) +
                                       pow( transformStamped.transform.translation.y , 2 ) );
        turtle_vel.publish(vel_msg);

        rate.sleep();
    }
    return 0;
};