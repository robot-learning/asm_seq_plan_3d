// === Includes ============================================================================================================================

// ~~ Standard Libraries ~~
#include <math.h>       /* cos */
#include <stdio.h>      /* printf */
#include <iostream> // output for days
#define PI 3.14159265

// ~~ Local Libraries ~~
#include "Cpp_Helpers.h"

// ~~ ROS : Transforms & RVIZ ~~
#include <ros/ros.h>
#include <visualization_msgs/Marker.h>
#include <tf/transform_datatypes.h> // Quaternions
// #include <tf/transform_broadcaster.h> // Broadcasters for nested frames


// ~~ LIBIGL : Polyhedral Geometry ~~
// #include <igl/cotmatrix.h> // Test of libigl

// ~~ Eigen : Maths! ~~
#include <Eigen/Core> // --- The living heart of Eigen
#include <Eigen/Geometry> // Quaternions
// typedef Eigen::Matrix< double , 3 , 1 , Eigen::RowMajor > Vec3E; // Eigen aliases
typedef Eigen::Matrix< double , 3 , 1 > Vec3E; // Eigen aliases // This is row major
typedef Eigen::Quaternion<double> QuatE;

// ~~ FCL : Collision Detection ~~ // ALL THE FILES HAVE CHANGED!
// #include <fcl/shape/geometric_shapes.h>
// #include <fcl/shape/geometric_shapes_utility.h>
// #include <fcl/narrowphase/narrowphase.h>
// #include <fcl/collision.h>
// using namespace fcl;
// https://github.com/flexible-collision-library/fcl/issues/133

// === End Include =========================================================================================================================


// === Assembly Geometry Classes ===


// == class ASP_Shape ==

class ASP_Shape{
public:
	ASP_Shape(); // Default constructor
	ASP_Shape( Vec3E& position , QuatE& orientation ); // Constructor with position and orientation
    ASP_Shape( Vec3E& position , QuatE& orientation , float Xscale , float Yscale , float Zscale ); // position , orientation , scales
	~ASP_Shape(); // Destructor
	ASP_Shape( const ASP_Shape& other );
	void set_pose( Vec3E& position , QuatE& orientation ); // Set the pose to a new position and orientation
	void set_position( Vec3E& position );
	void set_orientation( QuatE& orientation );
	void set_color( float R , float G , float B , float alpha );
	void set_scale( float Xscale , float Yscale , float Zscale );
	void set_scale( float uniformScale );
    const visualization_msgs::Marker& get_marker();
	Box* get_collsn_geo(); // Return a pointer to the collision geometry
	Transform3f& get_collsn_pose(); // Get the pose of the collision geometry
	
protected:
	visualization_msgs::Marker visMarker; // ----------------- RViz marker
	uint32_t markerType = visualization_msgs::Marker::CUBE; // RViz marker type (CUBE default)
	Box* collsnBox; // --------------------------------------- FCL collision geometry
	Transform3f tf; // --------------------------------------- Reference frame for the shape
    string name = "ASP_Shape";
    static unsigned long count;
    // static ObjectRegistry& registry;
};

// == End ASP_Shape ==


// == class ASP_World ==

class ASP_World{ // This is a class to manage drawing ASP objects
public:
    ASP_World( ros::Publisher& mrkrPub ); // Constructor with the marker publisher for RViz
    bool add_model( ASP_Shape* nuShape );

protected:
    Transform3f tf; // Base frame for the
    vector<ASP_Shape> allShapes;
};

// == End ASP_World ==


// === End Assembly Geometry ===
