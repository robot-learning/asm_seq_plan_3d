#include "ASP_Geo.h"
#include "Cpp_Helpers.h"

// Init static vars
unsigned long ASP_Shape::count = 0;
// ObjectRegistry& ASP_Shape::registry = objectRegistry;

// == class ASP_World ==

ASP_World::ASP_World( ros::Publisher& mrkrPub ) { // Constructor with the marker publisher for RViz


}

bool ASP_World::add_model( ASP_Shape* nuShape ){ // Add a model to the world


}


// == End ASP_World ==

// === class ASP_Shape ===

ASP_Shape::ASP_Shape(){ // Default constructor
    visMarker = visualization_msgs::Marker(  );
    visMarker.header.frame_id = "map"; // Set the frame ID and timestamp.  See the TF tutorials for information on these.
    visMarker.ns = "collision_test"; // Set the namespace and id for this marker.  This serves to create a unique ID
    count++; visMarker.id = (int)count; // Any marker sent with the same namespace and id will overwrite the old one
    visMarker.type = markerType; // Set the marker type.
    visMarker.action = visualization_msgs::Marker::ADD; // Set the marker action.  Options are ADD, DELETE, and new in ROS Indigo: 3 (DELETEALL)
    // ADD will repaint the marker
    visMarker.pose.position.x = 0; // Set the pose of the marker.  This is a full 6DOF pose relative to the frame/time specified in the header
    visMarker.pose.position.y = 0;
    visMarker.pose.position.z = 0;
    visMarker.pose.orientation.w = 1.0;
    visMarker.pose.orientation.x = 0.0;
    visMarker.pose.orientation.y = 0.0;
    visMarker.pose.orientation.z = 0.0;
    visMarker.scale.x = 1.0; // Set the scale of the marker -- 1x1x1 here means 1m on a side
    visMarker.scale.y = 1.0;
    visMarker.scale.z = 1.0;
    visMarker.color.r = rand_float(); // Set the color -- be sure to set alpha to something non-zero!
    visMarker.color.g = rand_float();
    visMarker.color.b = rand_float();
    cout << rand_float() << " " << rand_float() << " " << rand_float() << endl;
    visMarker.color.a = 1.0f;

    collsnBox = new Box( Vec3f( visMarker.scale.x , visMarker.scale.y , visMarker.scale.z ) );

    // Collision mesh
    tf = Transform3f();
    tf.setIdentity();
    tf.setTranslation( Vec3f( visMarker.pose.position.x , visMarker.pose.position.y , visMarker.pose.position.z ) );
    // http://gamma.cs.unc.edu/FCL/fcl_docs/webpage/generated/transform_8h-source.html#l00123
    tf.setQuatRotation( Quaternion3f( visMarker.pose.orientation.w ,    // W
                                      visMarker.pose.orientation.x ,    // X
                                      visMarker.pose.orientation.y ,    // Y
                                      visMarker.pose.orientation.z ) ); // Z
}

ASP_Shape::ASP_Shape( Vec3E& position , QuatE& orientation ){ // Constructor with position and orientation
    visMarker = visualization_msgs::Marker(  );
    visMarker.header.frame_id = "map"; // Set the frame ID and timestamp.  See the TF tutorials for information on these.
    visMarker.ns = "collision_test"; // Set the namespace and id for this marker.  This serves to create a unique ID
    count++; visMarker.id = (int)count; // Any marker sent with the same namespace and id will overwrite the old one
    visMarker.type = markerType; // Set the marker type.
    visMarker.action = visualization_msgs::Marker::ADD; // Set the marker action.  Options are ADD, DELETE, and new in ROS Indigo: 3 (DELETEALL)
    // ADD will repaint the marker
    visMarker.pose.position.x = position.x(); // Set the pose of the marker.  This is a full 6DOF pose relative to the frame/time specified in the header
    visMarker.pose.position.y = position.y();
    visMarker.pose.position.z = position.z();
    visMarker.pose.orientation.w = orientation.w();
    visMarker.pose.orientation.x = orientation.x();
    visMarker.pose.orientation.y = orientation.y();
    visMarker.pose.orientation.z = orientation.z();
    visMarker.scale.x = 1.0; // Set the scale of the marker -- 1x1x1 here means 1m on a side
    visMarker.scale.y = 1.0;
    visMarker.scale.z = 1.0;
    visMarker.color.r = rand_float(); // Set the color -- be sure to set alpha to something non-zero!
    visMarker.color.g = rand_float();
    visMarker.color.b = rand_float();
    cout << rand_float() << " " << rand_float() << " " << rand_float() << endl;
    visMarker.color.a = 1.0f;

    collsnBox = new Box( Vec3f( visMarker.scale.x , visMarker.scale.y , visMarker.scale.z ) );

    // Collision mesh
    tf = Transform3f();
    tf.setIdentity();
    tf.setTranslation( Vec3f( visMarker.pose.position.x , visMarker.pose.position.y , visMarker.pose.position.z ) );
    // http://gamma.cs.unc.edu/FCL/fcl_docs/webpage/generated/transform_8h-source.html#l00123
    tf.setQuatRotation( Quaternion3f( visMarker.pose.orientation.w ,    // W
                                      visMarker.pose.orientation.x ,    // X
                                      visMarker.pose.orientation.y ,    // Y
                                      visMarker.pose.orientation.z ) ); // Z
}

ASP_Shape::ASP_Shape( Vec3E& position , QuatE& orientation , float Xscale , float Yscale , float Zscale )
        : ASP_Shape( position , orientation ){ // Constructor with position  , orientation , scales
    set_scale( Xscale , Yscale , Zscale );

}

ASP_Shape::~ASP_Shape(){ /* No dynamic objects */ }

ASP_Shape::ASP_Shape( const ASP_Shape& other ){ // Copy constructor
    visMarker = visualization_msgs::Marker(  );
    visMarker.header.frame_id = other.visMarker.header.frame_id; // Set the frame ID and timestamp.  See the TF tutorials for information on these.
    visMarker.ns = "collision_test"; // Set the namespace and id for this marker.  This serves to create a unique ID
    count++; visMarker.id = (int)count; // Any marker sent with the same namespace and id will overwrite the old one
    visMarker.type = markerType; // Set the marker type.
    visMarker.action = visualization_msgs::Marker::ADD; // Set the marker action.  Options are ADD, DELETE, and new in ROS Indigo: 3 (DELETEALL)
    // ADD will repaint the marker
    visMarker.pose.position.x = other.visMarker.pose.position.x; // Set the pose of the marker.  This is a full 6DOF pose relative to the frame/time specified in the header
    visMarker.pose.position.y = other.visMarker.pose.position.y;
    visMarker.pose.position.z = other.visMarker.pose.position.z;
    visMarker.pose.orientation.w = other.visMarker.pose.orientation.w;
    visMarker.pose.orientation.x = other.visMarker.pose.orientation.x;
    visMarker.pose.orientation.y = other.visMarker.pose.orientation.y;
    visMarker.pose.orientation.z = other.visMarker.pose.orientation.z;
    visMarker.scale.x = other.visMarker.scale.x; // Set the scale of the marker -- 1x1x1 here means 1m on a side
    visMarker.scale.y = other.visMarker.scale.y;
    visMarker.scale.z = other.visMarker.scale.z;
    visMarker.color.r = other.visMarker.color.r; // Set the color -- be sure to set alpha to something non-zero!
    visMarker.color.g = other.visMarker.color.g;
    visMarker.color.b = other.visMarker.color.b;
    visMarker.color.a = other.visMarker.color.a;

    collsnBox = new Box( Vec3f( visMarker.scale.x , visMarker.scale.y , visMarker.scale.z ) );

    // Collision mesh
    tf = Transform3f();
    tf.setIdentity();
    tf.setTranslation( Vec3f( visMarker.pose.position.x , visMarker.pose.position.y , visMarker.pose.position.z ) );
    // http://gamma.cs.unc.edu/FCL/fcl_docs/webpage/generated/transform_8h-source.html#l00123
    tf.setQuatRotation( Quaternion3f( visMarker.pose.orientation.w ,    // W
                                      visMarker.pose.orientation.x ,    // X
                                      visMarker.pose.orientation.y ,    // Y
                                      visMarker.pose.orientation.z ) ); // Z
}

void ASP_Shape::set_pose( Vec3E& position , QuatE& orientation ) { // Set the pose to a new position and orientation
    visMarker.pose.position.x = position.x(); // Set the pose of the marker.  This is a full 6DOF pose relative to the frame/time specified in the header
    visMarker.pose.position.y = position.y();
    visMarker.pose.position.z = position.z();
    tf.setTranslation( Vec3f( visMarker.pose.position.x , visMarker.pose.position.y , visMarker.pose.position.z ) );
    visMarker.pose.orientation.w = orientation.w();
    visMarker.pose.orientation.x = orientation.x();
    visMarker.pose.orientation.y = orientation.y();
    visMarker.pose.orientation.z = orientation.z();
    tf.setQuatRotation( Quaternion3f( visMarker.pose.orientation.w ,    // W
                                      visMarker.pose.orientation.x ,    // X
                                      visMarker.pose.orientation.y ,    // Y
                                      visMarker.pose.orientation.z ) ); // Z
}

void ASP_Shape::set_position( Vec3E& position ){
    visMarker.pose.position.x = position.x(); // Set the pose of the marker.  This is a full 6DOF pose relative to the frame/time specified in the header
    visMarker.pose.position.y = position.y();
    visMarker.pose.position.z = position.z();
    tf.setTranslation( Vec3f( visMarker.pose.position.x , visMarker.pose.position.y , visMarker.pose.position.z ) );
}

void ASP_Shape::set_orientation( QuatE& orientation ){
    visMarker.pose.orientation.w = orientation.w();
    visMarker.pose.orientation.x = orientation.x();
    visMarker.pose.orientation.y = orientation.y();
    visMarker.pose.orientation.z = orientation.z();
    tf.setQuatRotation( Quaternion3f( visMarker.pose.orientation.w ,    // W
                                      visMarker.pose.orientation.x ,    // X
                                      visMarker.pose.orientation.y ,    // Y
                                      visMarker.pose.orientation.z ) ); // Z
}

void ASP_Shape::set_color( float R , float G , float B , float alpha ){
    visMarker.color.r = R;
    visMarker.color.g = G;
    visMarker.color.b = B;
    visMarker.color.a = alpha;
}

void ASP_Shape::set_scale( float Xscale , float Yscale , float Zscale ){
    visMarker.scale.x = Xscale; // Set the scale of the marker -- 1x1x1 here means 1m on a side
    visMarker.scale.y = Yscale;
    visMarker.scale.z = Zscale;
    if( collsnBox != nullptr ){ delete collsnBox; } // Delete the present collision box , if it exists
    collsnBox = new Box( Vec3f( visMarker.scale.x , visMarker.scale.y , visMarker.scale.z ) ); // Create new collision geo
}

void ASP_Shape::set_scale( float uniformScale ){
    visMarker.scale.x = uniformScale; // Set the scale of the marker -- 1x1x1 here means 1m on a side
    visMarker.scale.y = uniformScale;
    visMarker.scale.z = uniformScale;
}

const visualization_msgs::Marker& ASP_Shape::get_marker(){
    visMarker.header.stamp = ros::Time::now(); // NOTE: ROS must be running in order for this to work!
    return visMarker;
}

Box* ASP_Shape::get_collsn_geo(){ return collsnBox; } // Return a pointer to the collision geometry

Transform3f& ASP_Shape::get_collsn_pose(){ return tf; } // Get the pose of the collision geometry

// === End ASP_Shape ===