# coding=utf-8
"""
This is a test of commanding the joint angles of the default 7-DOF fixed manipulator in V-REP from Python.
The program is set to command the joints to follow a predefined path.
"""

try:
    import vrep
except:
    print ('--------------------------------------------------------------')
    print ('"vrep.py" could not be imported. This means very probably that')
    print ('either "vrep.py" or the remoteApi library could not be found.')
    print ('Make sure both are in the same folder as this file,')
    print ('or appropriately adjust the file "vrep.py"')
    print ('--------------------------------------------------------------')
    print ('')

import time
import sys
import math

print('---Program Start---')

# Attempt a connection to V-REP
print ('Attempting to connect to V-REP remote API server\n')
vrep.simxFinish(-1) # just in case, close all opened connections
clientID=vrep.simxStart('127.0.0.1',19999,True,True,5000,5) # Connect to V-REP
if clientID!=-1:
    print ('Connection Successful!')

    # Proceed with program commands

    # Create a list of joint angles to pass through on each joint (move to +90, then to -90, and back to 0
    jointAngles = range(0,90)
    jointAngles.extend(range(90,-90, -1))
    jointAngles.extend(range(-90,1))
    # Convert to radians
    radAngles = []
    for angle in jointAngles:
        radAngles.append(math.radians(angle))
    jointAngles = radAngles

    # Create a list of the object names of each of the joints
    jointNames = ['redundantRob_joint1',
                  'redundantRob_joint2',
                  'redundantRob_joint3',
                  'redundantRob_joint4',
                  'redundantRob_joint5',
                  'redundantRob_joint6',
                  'redundantRob_joint7']

    # Obtain the object handles for each of the joints of the robot
    jointHandles = []
    for objectName in jointNames:
        returnCode, handle = vrep.simxGetObjectHandle(clientID, objectName, vrep.simx_opmode_blocking)
        if returnCode != 0:
            sys.exit('Failed to obtain manipulator joint handles')
        jointHandles.append(handle)
    print('Manipulator joint handles obtained')

    # Command each of the joints to sweep the specified joint angles
    # Note that the joints are set to force/torque control and therefore, we must use
    # simxSetJointTargetPositon and the robot will not collide with itself
    print('Running Animation')
    frameRate = 0.01
    for handle in jointHandles:
        for pos in jointAngles:
            vrep.simxSetJointTargetPosition(clientID, handle, pos, vrep.simx_opmode_oneshot)
            time.sleep(frameRate)
    print('Animation Complete')

    # Close the connection to V-REP
    vrep.simxFinish(clientID)
else:
    print ('Connection Failed')

print('---Program End---')