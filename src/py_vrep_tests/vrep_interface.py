#!/usr/bin/env python
import rospy
from vrep_api import vrep


_RUN_RATE = 100
_CLIENT_ID = 19997

class VREPInterface:

    def __init__(self, node_name=None, client_id=_CLIENT_ID, rate=_RUN_RATE):
        if node_name is not None:
            rospy.init_node(node_name)
        self.rate = rate
        self.client_id = vrep.simxStart('127.0.0.1', client_id, True, True, 500, 5)
        vrep.simxSetFloatingParameter(self.client_id, vrep.sim_floatparam_simulation_time_step,
                                      1.0 / self.rate, vrep.simx_opmode_oneshot)

    def start_simulation(self):
        vrep.simxStartSimulation(self.client_id, vrep.simx_opmode_blocking)
        rospy.sleep(1)

    def stop_simulation(self):
        vrep.simxStopSimulation(self.client_id, vrep.simx_opmode_blocking)
        vrep_done = vrep.simxGetPingTime(self.client_id) # blocking call to ensure completion
        if vrep_done:
            vrep.simxFinish(self.client_id)


if __name__=='__main__':
    pass
