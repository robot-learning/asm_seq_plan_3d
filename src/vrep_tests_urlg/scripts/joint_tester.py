#!/usr/bin/env python
import rospy
import numpy as np
from sensor_msgs.msg import JointState
from vrep_api import VREPInterface

       
def test_LBR4_position_control():
    rospy.init_node('lbr4_control_test')
    run_rate = rospy.Rate(100)
    joint_cmd_pub = rospy.Publisher('lbr4/joint_cmd', JointState, queue_size=5)
    vrep = VREPInterface()
    vrep.start_simulation()
    js = JointState()
    js.name = ['lbr4_joint_0', 'lbr4_joint_1', 'lbr4_joint_2', 'lbr4_joint_3',
               'lbr4_joint_4', 'lbr4_joint_5', 'lbr4_joint_6']
    js.position = [0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0]
    while js.position[0] < np.pi / 2.0:
        joint_cmd_pub.publish(js)
        js.position = [p + 0.01 for p in js.position]
        run_rate.sleep()
    rospy.sleep(1)
    while js.position[0] > 0:
        joint_cmd_pub.publish(js)
        js.position = [p - 0.01 for p in js.position]
        run_rate.sleep()            
    vrep.stop_simulation()


if __name__=='__main__':
    test_LBR4_position_control()
