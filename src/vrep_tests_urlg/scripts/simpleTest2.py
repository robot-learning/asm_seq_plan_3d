# import vrep_api as vrep # This is Adam's module
import vrep
import numpy as np

vrep.simxFinish( -1 ) # close open connections

# connect to VREP server                  v-- Connection Port , This number may change <-- look at the console that you started V-REP from
client_id = vrep.simxStart( '127.0.0.1' , 19997 , True , True , 500 , 5 )

if client_id != -1:
    # --------------------------- Setup Simulation ---------------------------#
    # ensure simulation moves in step with controller
    vrep.simxSynchronous( client_id , True )

    if 0: # Set to 1 to get the handles to a list of objects by name
        	# get V-REP handles corresponding to the robot joints
        	joint_names = [] # populate with the joint names of your robot
        	joint_handles = [
        		vrep.simxGetObjectHandle(client_id, name, vrep.simx_opmode_blocking)[1]
        		for name in joint_names
        	]

    # set the simulation timestep
    dt = 0.01
    vrep.ximxSetFloatingParameter( client_id , vrep.sim_floatparam_simulation_time_step , dt , vrep.simx_opmode_oneshot )

    # start the simulation environment
    vrep.simxStartSimulation( client_id , vrep.simx_opmode_blocking )


    # --------------------------- Run Simulation --------------------------- #

    # execute simulation forever
    while True: # add simulation stopping condition


        print "ADD YOUR SIMULATION CODE HERE"


        pass

else:
    print 'Connection failed. Could not connect to remote API server.'
